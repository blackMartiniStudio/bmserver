﻿using BMNet;
using System;

namespace RelayServer
{
	public class RelayRoomManager
	{
		private Int32 uidCreateRoom = 0;

		public RGameRoom GetRelayRoom()
		{
			RGameRoom room = new RGameRoom( ++uidCreateRoom );
			room.Create( (Int32)NetCommon.UDP_BASE_PORT + uidCreateRoom );

			return room;
		}
	}
}