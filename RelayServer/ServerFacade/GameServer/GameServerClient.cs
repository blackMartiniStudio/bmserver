﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;
using BMServerCommonLib;
using CSCommonLib;

namespace RelayServer
{
	public class GameServerClient : NetClientProxy
	{
		private Regulator regTestSender;
		private static Int32 SendID = 0;

		public GameServerClient( BMNetClientDesc _kDesc ) : base( _kDesc )
		{
			regTestSender = new Regulator( 1000 );
		}

		internal bool StartNetwork()
		{
			if( ResultTable.SUCCESS != CreateSocket( AddressFamily.InterNetwork, Desc.ServerIp, Desc.ServerPort, false, false ) )
				return false;

			if( ResultTable.SUCCESS != Connect() )
				return false;

			return true;
		}

		public override void OnResInitializeToServer( PacketS2SResInitialize kPkt )
		{
			base.OnResInitializeToServer( kPkt );

			regTestSender.Start();
		}

		protected override void OnUpdate()
		{

			if( IsConnected() )
			{
				if( regTestSender.IsUpdate() )
				{
					SendPacketTest();
				}
			}

			base.OnUpdate();
		}

		private void SendPacketTest()
		{
			++SendID;
			BMCommand kCmd = NewCommand( (Int32)SPacketCommandEnum.CMD_PacketS2SSendTest );
			kCmd.SetPacket( new PacketS2SSendTest( RSSys.INST.kRelayServer.NetServer.Desc.ServerName, SendID ) );

			SendCommand( kCmd );
		}
	}
}
