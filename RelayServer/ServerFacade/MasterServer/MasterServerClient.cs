﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMDK;
using CSCommonLib;
using BMServerCommonLib;

namespace RelayServer
{
	public class MasterServerClient
	{
		public NetClientProxy NetClient { get; private set; }
		private Regulator regSender;
		private static Int32 SendID = 0;

		public MasterServerClient()
		{
			NetClient = new NetClientProxy( new BMNetClientDesc() { KeepAlive = true } );
			regSender = new Regulator( 1000 );
		}

		internal void Connect()
		{
			BMNetClientDesc desc = NetClient.Desc;
			NetClient.CreateSocket( AddressFamily.InterNetwork, desc.ServerIp, desc.ServerPort, false, false );
			NetClient.Connect();
			regSender.Start();
		}

		internal void Update()
		{
			NetClient.Update();

			if( NetClient.IsConnected() )
			{
				if( regSender.IsUpdate() )
				{
					SendPacketTest();
				}
			}
		}

		private void SendPacketTest()
		{
			++SendID;
			BMCommand kCmd = NetClient.NewCommand( (Int32)SPacketCommandEnum.CMD_PacketS2SSendTest );
			kCmd.SetPacket( new PacketS2SSendTest( RSSys.INST.kRelayServer.NetServer.Desc.ServerName, SendID ) );

			NetClient.SendCommand( kCmd );
		}
	}
}
