﻿using System;
using System.Windows.Forms;
using BMDK;

namespace RelayServer
{

	internal static class Program
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault( false );
			Application.Run( new RelayServerForm() );
		}
	}

	/*
	internal class ProgramConsole
	{

		private static void Main( string[] args )
		{
			RelayServerApplication m_kApp = new RelayServerApplication();
			LogViewer logViewer = new LogViewer();
			logViewer.Init();

			Logger.SetLogWindow( logViewer );


			string ProcessName = ProcessHelper.GetProcessName();
			Logger.Start( ProcessName, typeof( ProgramConsole ) );
			

			if( false == m_kApp.Create( ProcessName ) || false == m_kApp.Start() )
			{
				m_kApp.Destroy();
			}
		}
	}
	*/
}