﻿using BMNet;
using CSCommonLib;
using System;
using System.Linq;
using System.Net;

namespace RelayServer
{
	public class RCommandHandlerCommon : BMCommandHandler
	{
		private static Int32 nRecvCount = 0;

		public RCommandHandlerCommon( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketC2STest, OnTetstPacket );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketC2RClientAddressInfo, OnClientAddressInfo );
		}

		private BMCommandResult OnClientAddressInfo( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			RNetServer kServer = RCommandGroup.NetServer( _kHandler );

			PacketC2RClientAddressInfo pkt = _kCommand.Packet as PacketC2RClientAddressInfo;

			IPEndPoint clientAddress = new IPEndPoint( IPAddress.Parse( pkt.ip ), pkt.port );

			var addresses = RSSys.INST.kRelayServer.GameRoom.EntryPlayerAddress;
			int EntryCount = 0;

			bool isFind = false;
			foreach( EndPoint address in addresses )
			{
				EntryCount++;
				if( address.ToString() == clientAddress.ToString() )
				{
					isFind = true;
					break;
				}
			}

			if( isFind == false )
			{
				RSSys.INST.kRelayServer.GameRoom.EntryPlayerAddress.Add( clientAddress );
				EntryCount = RSSys.INST.kRelayServer.GameRoom.EntryPlayerAddress.Count;
			}

			BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketR2CClientAddressInfo, _kCommand.SendererUID );
			PacketR2CClientAddressInfo kSendCmd = new PacketR2CClientAddressInfo( (int)BMCommandResult.SUCCESS, pkt.ip, pkt.port, EntryCount );

			Console.WriteLine( " OnClientAddressInfo ip = {0}, port = {1}, EntryID = {2}", pkt.ip, pkt.port, EntryCount );

			kNewCommand.SetPacket( kSendCmd );

			kServer.Post( kNewCommand );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnTetstPacket( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketC2STest kCmd = _kCommand.Packet as PacketC2STest;

			RNetServer kServer = RCommandGroup.NetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketS2CTest, _kCommand.SendererUID );
			PacketS2CTest kSendCmd = new PacketS2CTest( (int)BMCommandResult.SUCCESS, ++nRecvCount, kCmd.nClientSendCount );

			Console.WriteLine( " CommandID = {0}, nClientSendCount = {1}, ServerRecvCount = {2}", _kCommand.ID, kCmd.nClientSendCount, nRecvCount );

			kNewCommand.SetPacket( kSendCmd );

			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}
	}
}