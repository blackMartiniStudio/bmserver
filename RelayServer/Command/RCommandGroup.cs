﻿using BMNet;

namespace RelayServer
{
	public class RCommandGroup : ICommandGroup
	{
		public RCommandGroup( BMCommandCommunicator _kCommunicator )
		{
			RCommandHandlerCommon kCommon = new RCommandHandlerCommon( _kCommunicator );
		}

		static public RNetServer NetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as RNetServer;
		}
	}
}