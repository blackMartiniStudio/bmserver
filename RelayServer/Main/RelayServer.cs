﻿using BMDK;
using BMNet;
using BMServerCommonLib;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace RelayServer
{
	// 실제 서버 메인 클래스
	public class RelayServer : GBaseServer
	{
		public RNetServer NetServer { get; private set; }
		public GameServerClient GSClient { get; private set; }

		public RGameRoom GameRoom { get; private set; }

		public RelayServer()
		{
		}

		protected override bool OnCreate()
		{
			if( false == CreateNetwork( RSMgr.INST.Config.Info ) )
				return false;

			if( false == CreateClientNetwork( RSMgr.INST.Config.ConnetionInfo ) )
				return false;

			if( false == StartNetwork() )
				return false;

			TestCreateRelayRoom();

			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			try
			{
				// 네트워크 업데이트 ( recv 처리 )
				NetServer?.Update();
				GSClient?.Update();

				// DB 처리

				// 로직 업데이트 처리
				// frameRate 처리
			}
			catch( Exception /*ex*/ )
			{
			}

			return true;
		}

		protected override void OnDestroy()
		{
		}

		public bool CreateNetwork( ServerInfo info )
		{
			BMNetServerDesc kDesc = NetSettingHelper.CreateNetServerDesc( info );
			NetServer = new RNetServer( kDesc );

			return true;

			// List<IPAddress> liIps = NetworkHelper.GetIPv4AddressList();
			// return m_kNetServer.Start( AddressFamily.InterNetwork, kDesc.ListenPort, kDesc.NoDelay, kDesc.SocketPoolSize, kDesc.SendPendingLimitCount, kDesc.PublicIP );
		}

		private Boolean CreateClientNetwork( ServerConnectionInfo connetionInfo )
		{
			BMNetClientDesc kDesc = NetSettingHelper.CreateNetClientDesc( connetionInfo, NetServer.Desc.ServerName );
			kDesc.KeepAlive = true;

			GSClient = new GameServerClient( kDesc );

			return true;
		}

		private bool StartNetwork()
		{
			if( false == StartNetworkServer() )
				return false;

			if( false == StartNetworkClient() )
				return false;

			return true;
		}

		private bool StartNetworkServer()
		{
			BMNetServerDesc desc = NetServer.Desc;
			return NetServer.Start( AddressFamily.InterNetwork, desc.ListenPort, desc.NoDelay, desc.SocketPoolSize, desc.SendPendingLimitCount, desc.PublicIP );
		}


		private bool StartNetworkClient()
		{
			return GSClient.StartNetwork();
		}
		
		private void TestCreateRelayRoom()
		{
			GameRoom = RSMgr.INST.kRelayRoomManager.GetRelayRoom();
			GameRoom.Run();
		}
	}
}