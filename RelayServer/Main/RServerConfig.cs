﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMServerCommonLib;
using CSCommonLib;

namespace RelayServer
{
	public class RServerConfig : ServerConfig
	{
		public ServerInfo Info;
		public ServerConnectionInfo ConnetionInfo;

		public RServerConfig()
		{
		}

		public override bool Load( string filePath )
		{
			JsonFileLoader fileLoader = new JsonFileLoader( filePath );
			if( false == fileLoader.TryGetRead( ServerInfo.Key, out Info ) )
				return false;

			if( false == fileLoader.TryGetRead( ServerConnectionInfo.Key, out ConnetionInfo ) )
				return false;

			return true;
		}
	}
}
