﻿using System;
using BMServerCommonLib;

namespace RelayServer
{
	public class RelayServerApplication : ServerApplication
	{
		protected override bool OnCreate()
		{
			// 전역 클래스 Instance
			RSSys.INST.Create();
			RSMgr.INST.Create();

			// 리소스 초기화
			if( false == Initialize() )
				return false;

			// 게임서버 초기화
			if( false == RSSys.INST.kRelayServer.Create( ProcessName ) )
				return false;

			// DB 초기화

			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			UpdateConsoleKeyInput();
			return RSSys.INST.kRelayServer.Update( _elapsed );
		}

		protected override void OnDestroy()
		{
			RSSys.INST.kRelayServer.Destroy();
		}

		private bool Initialize()
		{
			// 로거 초기화

			// 리소스 초기화
			if( false == RSMgr.INST.Config.Load( "Config.json" ) )
				return false;

			// Net 관련 초기화

			return true;
		}

		private void UpdateConsoleKeyInput()
		{
			// 			ConsoleKeyInfo key = Console.ReadKey();
			// 			String strKeyChar = key.KeyChar.ToString();
			// 			if( 0 == String.Compare( strKeyChar, "Y", true ) )
			// 			{
			// 				int a = 0;
			// 			}
		}
	}
}