﻿using BMNet;
using CSCommonLib;

namespace RelayServer
{
	public class RNetServer : BMNetServer
	{
		private ICommandGroup kCommandGroup;

		public RNetServer( BMNetServerDesc _kDesc ) : base( _kDesc )
		{
			kCommandGroup = new RCommandGroup( this );
		}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new CSCommandBuilder();
		}
	}
}