﻿namespace RelayServer
{
	public class RSMgr : RGlobalManager
	{
		#region [Simple Singleton]

		private static RSMgr _inst = new RSMgr();

		public static RSMgr INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			kRelayRoomManager = new RelayRoomManager();
			Config = new RServerConfig();
		}

		public virtual void Destroy()
		{
		}
	}
}