﻿namespace RelayServer
{
	public class RSSys : RGlobalSystem
	{
		#region [Simple Singleton]

		private static RSSys _inst = new RSSys();

		public static RSSys INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			kRelayServer = new RelayServer();
		}

		public virtual void Destroy()
		{
		}
	}
}