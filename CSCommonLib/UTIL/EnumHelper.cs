﻿using System;

namespace CSCommonLib
{
	public class EnumHelper
	{
		public static TEnum[] GetEnumValues<TEnum>()
		where TEnum : struct
		{
			return (TEnum[])Enum.GetValues( typeof( TEnum ) );
		}
	}
}