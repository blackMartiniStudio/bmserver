﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CSCommonLib
{
	public class JsonFileLoader
	{
		public JObject JObj { get; private set; }

		public JsonFileLoader( string filePath )
		{
// 			string text = File.ReadAllText( filePath );
// 			JObject JObj = JObject.Parse( text );

			using( StreamReader file = File.OpenText( filePath ) )
			using( JsonTextReader reader = new JsonTextReader( file ) )
			{
				JObj = ( JObject)JToken.ReadFrom( reader );
			}
		}

		public bool TryGetRead<T>( string key, out T readData ) where T : class
		{
			readData = null;
			if( JObj == null )
				return false;

			JToken token = JObj[key];
			if( token == null )
				return false;

			readData = JsonConvert.DeserializeObject<T>( token.ToString() );
			if( readData == null )
				return false;

			return true;
		}
	}
}
