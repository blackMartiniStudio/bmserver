﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSCommonLib
{
	public class CSRoomInfo
	{
		public Int16 MatchType { get; protected set; }
		public Int16 GameModeType { get; protected set; }
		public byte Capacity { get; protected set; }

		public CSRoomInfo()
		{

		}

		public CSRoomInfo( Int16 matchType, Int16 gameModeType, byte capacity )
		{
			MatchType = matchType;
			GameModeType = gameModeType;
			Capacity = capacity;
		}

		public CSRoomInfo( TDCreateRoomInfo roomInfo )
		{
			MatchType = roomInfo.MatchType;
			GameModeType = roomInfo.GameModeType;
			Capacity = roomInfo.Capacity;
		}

		public CSRoomInfo( TDRoomInfo roomInfo )
		{
			MatchType = roomInfo.MatchType;
			GameModeType = roomInfo.GameModeType;
			Capacity = roomInfo.Capacity;
		}

	}
}
