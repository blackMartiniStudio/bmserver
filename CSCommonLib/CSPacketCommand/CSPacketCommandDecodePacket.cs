﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSCommonLib
{
public sealed partial class CSPacketCommandCoder
{
	static public object Decode(int cmd, byte[] bodyBuffer)
	{
		switch (cmd)
		{
			case (int)CSPacketCommandEnum.CMD_PacketC2STest:
				return (object)CSPacketCommandDecoder.DecodePacketC2STest(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketS2CTest:
				return (object)CSPacketCommandDecoder.DecodePacketS2CTest(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketC2RClientAddressInfo:
				return (object)CSPacketCommandDecoder.DecodePacketC2RClientAddressInfo(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketR2CClientAddressInfo:
				return (object)CSPacketCommandDecoder.DecodePacketR2CClientAddressInfo(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketC2MGetRoomList:
				return null;
			case (int)CSPacketCommandEnum.CMD_PacketM2CGetRoomList:
				return (object)CSPacketCommandDecoder.DecodePacketM2CGetRoomList(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketC2MGetRoomInfo:
				return null;
			case (int)CSPacketCommandEnum.CMD_PacketM2CGetRoomInfo:
				return (object)CSPacketCommandDecoder.DecodePacketM2CGetRoomInfo(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketC2MCreateRoom:
				return (object)CSPacketCommandDecoder.DecodePacketC2MCreateRoom(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketM2CCreateRoom:
				return (object)CSPacketCommandDecoder.DecodePacketM2CCreateRoom(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketC2REnterRoom:
				return (object)CSPacketCommandDecoder.DecodePacketC2REnterRoom(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketR2CEnterRoom:
				return (object)CSPacketCommandDecoder.DecodePacketR2CEnterRoom(bodyBuffer);
			case (int)CSPacketCommandEnum.CMD_PacketC2RLeaveRoom:
				return null;
			case (int)CSPacketCommandEnum.CMD_PacketR2CLeaveRoom:
				return (object)CSPacketCommandDecoder.DecodePacketR2CLeaveRoom(bodyBuffer);
			default:
				break;
		}
		return null;
	}
}
}
