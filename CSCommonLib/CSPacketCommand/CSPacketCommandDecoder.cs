﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CSCommonLib
{
	public sealed partial class CSPacketCommandDecoder
	{

		static public PacketC2STest DecodePacketC2STest(byte[] bodyBuffer)
		{
			PacketC2STest retValue = new PacketC2STest();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketS2CTest DecodePacketS2CTest(byte[] bodyBuffer)
		{
			PacketS2CTest retValue = new PacketS2CTest();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketC2RClientAddressInfo DecodePacketC2RClientAddressInfo(byte[] bodyBuffer)
		{
			PacketC2RClientAddressInfo retValue = new PacketC2RClientAddressInfo();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketR2CClientAddressInfo DecodePacketR2CClientAddressInfo(byte[] bodyBuffer)
		{
			PacketR2CClientAddressInfo retValue = new PacketR2CClientAddressInfo();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketC2MGetRoomList DecodePacketC2MGetRoomList(byte[] bodyBuffer)
		{
			PacketC2MGetRoomList retValue = new PacketC2MGetRoomList();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketM2CGetRoomList DecodePacketM2CGetRoomList(byte[] bodyBuffer)
		{
			PacketM2CGetRoomList retValue = new PacketM2CGetRoomList();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketC2MGetRoomInfo DecodePacketC2MGetRoomInfo(byte[] bodyBuffer)
		{
			PacketC2MGetRoomInfo retValue = new PacketC2MGetRoomInfo();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketM2CGetRoomInfo DecodePacketM2CGetRoomInfo(byte[] bodyBuffer)
		{
			PacketM2CGetRoomInfo retValue = new PacketM2CGetRoomInfo();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketC2MCreateRoom DecodePacketC2MCreateRoom(byte[] bodyBuffer)
		{
			PacketC2MCreateRoom retValue = new PacketC2MCreateRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketM2CCreateRoom DecodePacketM2CCreateRoom(byte[] bodyBuffer)
		{
			PacketM2CCreateRoom retValue = new PacketM2CCreateRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketC2REnterRoom DecodePacketC2REnterRoom(byte[] bodyBuffer)
		{
			PacketC2REnterRoom retValue = new PacketC2REnterRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketR2CEnterRoom DecodePacketR2CEnterRoom(byte[] bodyBuffer)
		{
			PacketR2CEnterRoom retValue = new PacketR2CEnterRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketC2RLeaveRoom DecodePacketC2RLeaveRoom(byte[] bodyBuffer)
		{
			PacketC2RLeaveRoom retValue = new PacketC2RLeaveRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketR2CLeaveRoom DecodePacketR2CLeaveRoom(byte[] bodyBuffer)
		{
			PacketR2CLeaveRoom retValue = new PacketR2CLeaveRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}
	}
}
