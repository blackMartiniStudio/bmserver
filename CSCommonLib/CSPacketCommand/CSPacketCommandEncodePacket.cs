﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSCommonLib
{
public sealed partial class CSPacketCommandCoder
{
	static public byte[] Encode(int cmd, object packetClass, out bool isCrypt)
	{
		switch (cmd)
		{
			case (int)CSPacketCommandEnum.CMD_PacketC2STest:
				return CSPacketCommandEncoder.EncodePacketC2STest((PacketC2STest)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketS2CTest:
				return CSPacketCommandEncoder.EncodePacketS2CTest((PacketS2CTest)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketC2RClientAddressInfo:
				return CSPacketCommandEncoder.EncodePacketC2RClientAddressInfo((PacketC2RClientAddressInfo)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketR2CClientAddressInfo:
				return CSPacketCommandEncoder.EncodePacketR2CClientAddressInfo((PacketR2CClientAddressInfo)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketC2MGetRoomList:
				isCrypt = false;
				return null;
			case (int)CSPacketCommandEnum.CMD_PacketM2CGetRoomList:
				return CSPacketCommandEncoder.EncodePacketM2CGetRoomList((PacketM2CGetRoomList)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketC2MGetRoomInfo:
				isCrypt = false;
				return null;
			case (int)CSPacketCommandEnum.CMD_PacketM2CGetRoomInfo:
				return CSPacketCommandEncoder.EncodePacketM2CGetRoomInfo((PacketM2CGetRoomInfo)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketC2MCreateRoom:
				return CSPacketCommandEncoder.EncodePacketC2MCreateRoom((PacketC2MCreateRoom)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketM2CCreateRoom:
				return CSPacketCommandEncoder.EncodePacketM2CCreateRoom((PacketM2CCreateRoom)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketC2REnterRoom:
				return CSPacketCommandEncoder.EncodePacketC2REnterRoom((PacketC2REnterRoom)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketR2CEnterRoom:
				return CSPacketCommandEncoder.EncodePacketR2CEnterRoom((PacketR2CEnterRoom)packetClass, out isCrypt);
			case (int)CSPacketCommandEnum.CMD_PacketC2RLeaveRoom:
				isCrypt = false;
				return null;
			case (int)CSPacketCommandEnum.CMD_PacketR2CLeaveRoom:
				return CSPacketCommandEncoder.EncodePacketR2CLeaveRoom((PacketR2CLeaveRoom)packetClass, out isCrypt);
			default:
				break;
		}
		isCrypt = false;
		return null;
	}
}
}
