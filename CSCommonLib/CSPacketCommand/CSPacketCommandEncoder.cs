﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CSCommonLib
{
	public sealed partial class CSPacketCommandEncoder
	{

		static public byte[] EncodePacketC2STest(PacketC2STest pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketS2CTest(PacketS2CTest pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketC2RClientAddressInfo(PacketC2RClientAddressInfo pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketR2CClientAddressInfo(PacketR2CClientAddressInfo pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketC2MGetRoomList(PacketC2MGetRoomList pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketM2CGetRoomList(PacketM2CGetRoomList pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketC2MGetRoomInfo(PacketC2MGetRoomInfo pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketM2CGetRoomInfo(PacketM2CGetRoomInfo pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketC2MCreateRoom(PacketC2MCreateRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketM2CCreateRoom(PacketM2CCreateRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketC2REnterRoom(PacketC2REnterRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketR2CEnterRoom(PacketR2CEnterRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketC2RLeaveRoom(PacketC2RLeaveRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketR2CLeaveRoom(PacketR2CLeaveRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}
	}
}
