﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace CSCommonLib
{

public class TDRoomInfo
{
	public Int32 RoomID;
	public Int16 MatchType;
	public Int16 GameModeType;
	public byte Capacity;
	public TDRoomInfo() {}
	public TDRoomInfo(Int32 p_RoomID, Int16 p_MatchType, Int16 p_GameModeType, byte p_Capacity)
	{
		this.RoomID = p_RoomID;
		this.MatchType = p_MatchType;
		this.GameModeType = p_GameModeType;
		this.Capacity = p_Capacity;
	}
	public TDRoomInfo(TDRoomInfo p_TDRoomInfo)
	{
		this.RoomID = p_TDRoomInfo.RoomID;
		this.MatchType = p_TDRoomInfo.MatchType;
		this.GameModeType = p_TDRoomInfo.GameModeType;
		this.Capacity = p_TDRoomInfo.Capacity;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(RoomID);
		output.Write(MatchType);
		output.Write(GameModeType);
		output.Write(Capacity);
	}

	public void StreamDecoding(BinaryReader src)
	{
		RoomID = src.ReadInt32();
		MatchType = src.ReadInt16();
		GameModeType = src.ReadInt16();
		Capacity = src.ReadByte();
	}
}

public class TDRoomList
{
	public TDRoomInfo[] arRoomInfos;
	public TDRoomList() {}
	public TDRoomList(TDRoomInfo[] p_arRoomInfos)
	{
		this.arRoomInfos = new TDRoomInfo[p_arRoomInfos.Length];
		for (int i = 0; i < (int)p_arRoomInfos.Length; i++)
		{
			this.arRoomInfos[i] = new TDRoomInfo(p_arRoomInfos[i]);
		}
	}
	public TDRoomList(TDRoomList p_TDRoomList)
	{
		this.arRoomInfos = new TDRoomInfo[p_TDRoomList.arRoomInfos.Length];
		for (int i = 0; i < (int)p_TDRoomList.arRoomInfos.Length; i++)
		{
			this.arRoomInfos[i] = new TDRoomInfo(p_TDRoomList.arRoomInfos[i]);
		}
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write((int)arRoomInfos.Length);
		for (int i = 0; i < (int)arRoomInfos.Length; i++)
		{
		if (arRoomInfos[i] == null)
			arRoomInfos[i] = new TDRoomInfo();
		arRoomInfos[i].StreamEncoding(output);
		}
	}

	public void StreamDecoding(BinaryReader src)
	{
		int arRoomInfosLength = src.ReadInt32();
		arRoomInfos = new TDRoomInfo[arRoomInfosLength];
		for (int i = 0; i < (int)arRoomInfos.Length; i++)
		{
			arRoomInfos[i] = new TDRoomInfo();
			arRoomInfos[i].StreamDecoding(src);
		}
	}
}

public class TDCreateRoomInfo
{
	public Int16 MatchType;
	public Int16 GameModeType;
	public byte Capacity;
	public TDCreateRoomInfo() {}
	public TDCreateRoomInfo(Int16 p_MatchType, Int16 p_GameModeType, byte p_Capacity)
	{
		this.MatchType = p_MatchType;
		this.GameModeType = p_GameModeType;
		this.Capacity = p_Capacity;
	}
	public TDCreateRoomInfo(TDCreateRoomInfo p_TDCreateRoomInfo)
	{
		this.MatchType = p_TDCreateRoomInfo.MatchType;
		this.GameModeType = p_TDCreateRoomInfo.GameModeType;
		this.Capacity = p_TDCreateRoomInfo.Capacity;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(MatchType);
		output.Write(GameModeType);
		output.Write(Capacity);
	}

	public void StreamDecoding(BinaryReader src)
	{
		MatchType = src.ReadInt16();
		GameModeType = src.ReadInt16();
		Capacity = src.ReadByte();
	}
}
}
