﻿using BMNet;
using System;

namespace CSCommonLib
{
	public class CSCommandBuilder : BMCommandBuilder
	{
		protected override byte[] HeaderEncode( Int32 _cmdID, UInt16 packetOption, UInt32 dataCRC, Int32 bodyLength, out bool notUsingCrypt )
		{
			return PacketCommandEncoder.EncodeC2SPacketHeader( new C2SPacketHeader( _cmdID, packetOption, dataCRC, bodyLength ), out notUsingCrypt );
		}

		protected override byte[] BodyEncode( BMCommand _kCmd, out bool isCrypt )
		{
			Byte[] body = base.BodyEncode( _kCmd, out isCrypt );
			if( body != null )
				return body;

			return CSPacketCommandCoder.Encode( _kCmd.ID, _kCmd.Packet, out isCrypt );
		}

		protected override C2SPacketHeader HeaderDecode( byte[] _arBuffer, Int32 _nOffset )
		{
			return base.HeaderDecode( _arBuffer, _nOffset );
		}

		protected override Object BodyDecod( int cmd, byte[] bodyBuffer )
		{
			Object body = base.BodyDecod( cmd, bodyBuffer );
			if( body != null )
				return body;

			return CSPacketCommandCoder.Decode( cmd, bodyBuffer );
		}
	}
}