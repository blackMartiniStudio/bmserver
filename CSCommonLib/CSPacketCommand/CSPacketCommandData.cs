﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace CSCommonLib
{

public class PacketC2STest
{
	public Int32 nClientSendID;
	public Int32 nClientSendCount;
	public PacketC2STest() {}
	public PacketC2STest(Int32 p_nClientSendID, Int32 p_nClientSendCount)
	{
		this.nClientSendID = p_nClientSendID;
		this.nClientSendCount = p_nClientSendCount;
	}
	public PacketC2STest(PacketC2STest p_PacketC2STest)
	{
		this.nClientSendID = p_PacketC2STest.nClientSendID;
		this.nClientSendCount = p_PacketC2STest.nClientSendCount;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(nClientSendID);
		output.Write(nClientSendCount);
	}

	public void StreamDecoding(BinaryReader src)
	{
		nClientSendID = src.ReadInt32();
		nClientSendCount = src.ReadInt32();
	}
}

public class PacketS2CTest
{
	public Int32 Result;
	public Int32 nServerRecvID;
	public Int32 nClientSendID;
	public PacketS2CTest() {}
	public PacketS2CTest(Int32 p_Result, Int32 p_nServerRecvID, Int32 p_nClientSendID)
	{
		this.Result = p_Result;
		this.nServerRecvID = p_nServerRecvID;
		this.nClientSendID = p_nClientSendID;
	}
	public PacketS2CTest(PacketS2CTest p_PacketS2CTest)
	{
		this.Result = p_PacketS2CTest.Result;
		this.nServerRecvID = p_PacketS2CTest.nServerRecvID;
		this.nClientSendID = p_PacketS2CTest.nClientSendID;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		output.Write(nServerRecvID);
		output.Write(nClientSendID);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		nServerRecvID = src.ReadInt32();
		nClientSendID = src.ReadInt32();
	}
}

public class PacketC2RClientAddressInfo
{
	public string ip;
	public Int32 port;
	public PacketC2RClientAddressInfo() {}
	public PacketC2RClientAddressInfo(string p_ip, Int32 p_port)
	{
		this.ip = p_ip;
		this.port = p_port;
	}
	public PacketC2RClientAddressInfo(PacketC2RClientAddressInfo p_PacketC2RClientAddressInfo)
	{
		this.ip = p_PacketC2RClientAddressInfo.ip;
		this.port = p_PacketC2RClientAddressInfo.port;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (ip == null)
			ip = "";
		byte[] sip = Encoding.UTF8.GetBytes(ip);
		output.Write(sip.Length);
		output.Write(sip);
		output.Write(port);
	}

	public void StreamDecoding(BinaryReader src)
	{
		int lip = src.ReadInt32();
		byte[] sip = new byte[lip];
		src.Read(sip, 0, lip);
		ip = Encoding.UTF8.GetString(sip);
		port = src.ReadInt32();
	}
}

public class PacketR2CClientAddressInfo
{
	public Int32 Result;
	public string ip;
	public Int32 port;
	public int enterID;
	public PacketR2CClientAddressInfo() {}
	public PacketR2CClientAddressInfo(Int32 p_Result, string p_ip, Int32 p_port, int p_enterID)
	{
		this.Result = p_Result;
		this.ip = p_ip;
		this.port = p_port;
		this.enterID = p_enterID;
	}
	public PacketR2CClientAddressInfo(PacketR2CClientAddressInfo p_PacketR2CClientAddressInfo)
	{
		this.Result = p_PacketR2CClientAddressInfo.Result;
		this.ip = p_PacketR2CClientAddressInfo.ip;
		this.port = p_PacketR2CClientAddressInfo.port;
		this.enterID = p_PacketR2CClientAddressInfo.enterID;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		if (ip == null)
			ip = "";
		byte[] sip = Encoding.UTF8.GetBytes(ip);
		output.Write(sip.Length);
		output.Write(sip);
		output.Write(port);
		output.Write(enterID);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		int lip = src.ReadInt32();
		byte[] sip = new byte[lip];
		src.Read(sip, 0, lip);
		ip = Encoding.UTF8.GetString(sip);
		port = src.ReadInt32();
		enterID = src.ReadInt32();
	}
}

public class PacketC2MGetRoomList
{
	public PacketC2MGetRoomList() {}
	public PacketC2MGetRoomList(PacketC2MGetRoomList p_PacketC2MGetRoomList)
	{
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
	}

	public void StreamDecoding(BinaryReader src)
	{
	}
}

public class PacketM2CGetRoomList
{
	public Int32 Result;
	public TDRoomList RoomList;
	public PacketM2CGetRoomList() {}
	public PacketM2CGetRoomList(Int32 p_Result, TDRoomList p_RoomList)
	{
		this.Result = p_Result;
		this.RoomList = new TDRoomList(p_RoomList);
	}
	public PacketM2CGetRoomList(PacketM2CGetRoomList p_PacketM2CGetRoomList)
	{
		this.Result = p_PacketM2CGetRoomList.Result;
		this.RoomList = new TDRoomList(p_PacketM2CGetRoomList.RoomList);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		if (RoomList == null)
			RoomList = new TDRoomList();
		RoomList.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		RoomList = new TDRoomList();
		RoomList.StreamDecoding(src);
	}
}

public class PacketC2MGetRoomInfo
{
	public PacketC2MGetRoomInfo() {}
	public PacketC2MGetRoomInfo(PacketC2MGetRoomInfo p_PacketC2MGetRoomInfo)
	{
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
	}

	public void StreamDecoding(BinaryReader src)
	{
	}
}

public class PacketM2CGetRoomInfo
{
	public Int32 Result;
	public TDRoomInfo RoomInfo;
	public PacketM2CGetRoomInfo() {}
	public PacketM2CGetRoomInfo(Int32 p_Result, TDRoomInfo p_RoomInfo)
	{
		this.Result = p_Result;
		this.RoomInfo = new TDRoomInfo(p_RoomInfo);
	}
	public PacketM2CGetRoomInfo(PacketM2CGetRoomInfo p_PacketM2CGetRoomInfo)
	{
		this.Result = p_PacketM2CGetRoomInfo.Result;
		this.RoomInfo = new TDRoomInfo(p_PacketM2CGetRoomInfo.RoomInfo);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		if (RoomInfo == null)
			RoomInfo = new TDRoomInfo();
		RoomInfo.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		RoomInfo = new TDRoomInfo();
		RoomInfo.StreamDecoding(src);
	}
}

public class PacketC2MCreateRoom
{
	public TDCreateRoomInfo CreateInfo;
	public PacketC2MCreateRoom() {}
	public PacketC2MCreateRoom(TDCreateRoomInfo p_CreateInfo)
	{
		this.CreateInfo = new TDCreateRoomInfo(p_CreateInfo);
	}
	public PacketC2MCreateRoom(PacketC2MCreateRoom p_PacketC2MCreateRoom)
	{
		this.CreateInfo = new TDCreateRoomInfo(p_PacketC2MCreateRoom.CreateInfo);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (CreateInfo == null)
			CreateInfo = new TDCreateRoomInfo();
		CreateInfo.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		CreateInfo = new TDCreateRoomInfo();
		CreateInfo.StreamDecoding(src);
	}
}

public class PacketM2CCreateRoom
{
	public Int32 Result;
	public TDRoomInfo RoomInfo;
	public PacketM2CCreateRoom() {}
	public PacketM2CCreateRoom(Int32 p_Result, TDRoomInfo p_RoomInfo)
	{
		this.Result = p_Result;
		this.RoomInfo = new TDRoomInfo(p_RoomInfo);
	}
	public PacketM2CCreateRoom(PacketM2CCreateRoom p_PacketM2CCreateRoom)
	{
		this.Result = p_PacketM2CCreateRoom.Result;
		this.RoomInfo = new TDRoomInfo(p_PacketM2CCreateRoom.RoomInfo);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		if (RoomInfo == null)
			RoomInfo = new TDRoomInfo();
		RoomInfo.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		RoomInfo = new TDRoomInfo();
		RoomInfo.StreamDecoding(src);
	}
}

public class PacketC2REnterRoom
{
	public Int32 RoomID;
	public PacketC2REnterRoom() {}
	public PacketC2REnterRoom(Int32 p_RoomID)
	{
		this.RoomID = p_RoomID;
	}
	public PacketC2REnterRoom(PacketC2REnterRoom p_PacketC2REnterRoom)
	{
		this.RoomID = p_PacketC2REnterRoom.RoomID;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(RoomID);
	}

	public void StreamDecoding(BinaryReader src)
	{
		RoomID = src.ReadInt32();
	}
}

public class PacketR2CEnterRoom
{
	public Int32 Result;
	public Int32 RoomID;
	public byte EntrySlotID;
	public bool IsMaster;
	public PacketR2CEnterRoom() {}
	public PacketR2CEnterRoom(Int32 p_Result, Int32 p_RoomID, byte p_EntrySlotID, bool p_IsMaster)
	{
		this.Result = p_Result;
		this.RoomID = p_RoomID;
		this.EntrySlotID = p_EntrySlotID;
		this.IsMaster = p_IsMaster;
	}
	public PacketR2CEnterRoom(PacketR2CEnterRoom p_PacketR2CEnterRoom)
	{
		this.Result = p_PacketR2CEnterRoom.Result;
		this.RoomID = p_PacketR2CEnterRoom.RoomID;
		this.EntrySlotID = p_PacketR2CEnterRoom.EntrySlotID;
		this.IsMaster = p_PacketR2CEnterRoom.IsMaster;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		output.Write(RoomID);
		output.Write(EntrySlotID);
		output.Write(IsMaster);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		RoomID = src.ReadInt32();
		EntrySlotID = src.ReadByte();
		IsMaster = src.ReadBoolean();
	}
}

public class PacketC2RLeaveRoom
{
	public PacketC2RLeaveRoom() {}
	public PacketC2RLeaveRoom(PacketC2RLeaveRoom p_PacketC2RLeaveRoom)
	{
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
	}

	public void StreamDecoding(BinaryReader src)
	{
	}
}

public class PacketR2CLeaveRoom
{
	public Int32 Result;
	public PacketR2CLeaveRoom() {}
	public PacketR2CLeaveRoom(Int32 p_Result)
	{
		this.Result = p_Result;
	}
	public PacketR2CLeaveRoom(PacketR2CLeaveRoom p_PacketR2CLeaveRoom)
	{
		this.Result = p_PacketR2CLeaveRoom.Result;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
	}
}
}
