﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSCommonLib
{
public enum CSPacketCommandEnum
{
	CMD_NONE,
	CMD_PacketC2STest = 10001,
	CMD_PacketS2CTest,
	CMD_PacketC2RClientAddressInfo,
	CMD_PacketR2CClientAddressInfo,
	CMD_PacketC2MGetRoomList,
	CMD_PacketM2CGetRoomList,
	CMD_PacketC2MGetRoomInfo,
	CMD_PacketM2CGetRoomInfo,
	CMD_PacketC2MCreateRoom,
	CMD_PacketM2CCreateRoom,
	CMD_PacketC2REnterRoom,
	CMD_PacketR2CEnterRoom,
	CMD_PacketC2RLeaveRoom,
	CMD_PacketR2CLeaveRoom,
	CMD_MAX
}
}
