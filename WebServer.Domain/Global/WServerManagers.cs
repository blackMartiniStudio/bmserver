﻿using WebServer.Domain.CouchBase;

namespace WebServer.Domain.Global
{
	public class WSMgr : WGlobalManager
	{
		#region [Simple Singleton]

		private static WSMgr _inst = new WSMgr();

		public static WSMgr INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			m_kCouchbaseMgr = new CouchbaseManager();
		}

		public virtual void Destroy()
		{
		}
	}
}