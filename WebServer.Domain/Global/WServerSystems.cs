﻿namespace WebServer.Domain.Global
{
	public class WSSys : WGlobalSystem
	{
		#region [Simple Singleton]

		private static WSSys _inst = new WSSys();

		public static WSSys INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
		}

		public virtual void Destroy()
		{
		}
	}
}