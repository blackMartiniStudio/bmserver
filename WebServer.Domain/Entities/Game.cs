﻿using Newtonsoft.Json;
using System;

namespace WebServer.Domain.Entities
{
	[JsonObject]
	public class GameResult
	{
		[JsonProperty( PropertyName = "result")]
		public String result;
	}

	[JsonObject]
	public class TradeMoney
	{
		[JsonProperty( PropertyName = "fromAID")]
		public Int64 fromAID;

		[JsonProperty( PropertyName = "toAID")]
		public Int64 toAID;

		[JsonProperty( PropertyName = "money")]
		public Int32 money;
	}

}
