﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase.Transaction;

namespace WebServer.Domain.Entities
{
	[JsonObject]
	public class PlayerData  : TransactionObject
	{
		[JsonProperty( PropertyName = "aid")]
		public String aid;

		[JsonProperty( PropertyName = "id")]
		public String id;

		[JsonProperty( PropertyName = "money")]
		public Int32 money;
	}
}
