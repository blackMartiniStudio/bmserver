﻿using Newtonsoft.Json;
using System;

namespace WebServer.Domain.Entities
{
	[JsonObject]
	public class LoginInfo
	{
		[JsonProperty( PropertyName = "id")]
		public String id;

		[JsonProperty( PropertyName = "pwd" )]
		public String pwd;
	}

	[JsonObject]
	public class CreateAccountInfo
	{
		[JsonProperty( PropertyName = "id")]
		public String id;

		[JsonProperty( PropertyName = "pwd" )]
		public String pwd;

		[JsonProperty( PropertyName = "pid" )]
		public String pid;
	}
	
	[JsonObject]
	public class AccountInfo
	{
		[JsonProperty( PropertyName = "name")]
		public String name;

		[JsonProperty( PropertyName = "aid" )]
		public String aid;

		[JsonProperty( PropertyName = "pid" )]
		public String pid;
	}

	[JsonObject]
	public class UpdateName
	{
		[JsonProperty( PropertyName = "aid")]
		public String aid;

		[JsonProperty( PropertyName = "name")]
		public String name;
	}


}