﻿namespace WebServer.Domain.Redis
{
	public class RedisKey
	{
		public static string Session( string sid )
		{
			return "session.sid:" + sid;
		}

		public static string AId { get { return "aid"; } }
	}
}