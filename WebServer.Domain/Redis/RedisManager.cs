﻿using BMDK;
using System;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace WebServer.Domain.Redis
{
	public class RedisManager : TSingleton<RedisManager>
	{
		public RedisManager()
		{
			InitClientsManager();
		}

		public async Task<string> Test()
		{

			return await RunCommand<string>( () => 
			{
				return client.GetValue( "foo" );
			},
			"");			
		}

		public async Task<string> SIDToAID( string sid )
		{
			return await RunCommand<string>( () =>
			{
				return client.GetValueFromHash( RedisKey.Session( sid ), RedisKey.AId );
			},
			"0");
		}

		public async Task SignIn( string sid, string aid )
		{
			await RunCommand( () =>
			{
				using( var trans = client.CreateTransaction() )
				{
					trans.QueueCommand( r => r.SetEntryInHash( RedisKey.Session( sid ), RedisKey.AId, aid ) );
					trans.QueueCommand( r => r.ExpireEntryIn( RedisKey.Session( sid ), TimeSpan.FromHours(1) ) );
					trans.Commit();
				}					
			});
		}

		public async Task SignOut( string sid )
		{
			await RunCommand( () =>
			{
				client.Remove( RedisKey.Session( sid ) );
			});
		}

		private async Task RunCommand( Action function )
		{
			await Task.Run( () =>
			{
				int retries = 0;

				while( true )
				{
					try
					{
						function();
						return;
					}
					catch( Exception ex )
					{
						Console.WriteLine( ex );

						Task.Delay( retryDelay );
						InitClientsManager();
						if( ++retries == maxRetries )
							return;
					}
				}
			} );
		}

		private async Task<TResult> RunCommand<TResult>( Func<TResult> function, TResult defaultResult )
		{
			return await Task<TResult>.Run( () =>
			{
				int retries = 0;

				while( true )
				{
					try
					{
						return function();
					}
					catch( Exception ex )
					{
						Console.WriteLine( ex );

						Task.Delay( retryDelay );
						InitClientsManager();
						if( ++retries == maxRetries )
							return defaultResult;
					}
				}
			} );
		}

		private void InitClientsManager()
		{
			clientsManager = new PooledRedisClientManager( "redis.am7qvw.ng.0001.apn2.cache.amazonaws.com:6379" );
		}

		private IRedisClientsManager clientsManager = null;

		private IRedisClient client { get { return clientsManager.GetClient(); } }

		private const int maxRetries = 5;
		private const int retryDelay = 100;
	}
}