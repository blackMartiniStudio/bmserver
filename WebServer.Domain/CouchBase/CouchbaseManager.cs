﻿using Couchbase;
using Couchbase.Configuration.Client;
using Couchbase.Core;
using System;
using System.Collections.Generic;
using WebServer.Domain.CouchBase.StroageHelper;

namespace WebServer.Domain.CouchBase
{
	public class CouchbaseManager
	{
		Dictionary<BucketType, CouchBaseStorage>     m_dicBuckets;
		public CouchbaseManager()
		{
			m_dicBuckets = new Dictionary<BucketType, CouchBaseStorage>();
		}

		public void Initialize()
		{
			ClientConfiguration config = new ClientConfiguration();
			config.BucketConfigs.Clear();

			ClusterHelper.Initialize( new ClientConfiguration
			{
				Servers = new List<Uri>
				{
					new Uri("http://127.0.0.1:8091/pools"),
				}
			} );

			IBucket kDefault = ClusterHelper.GetBucket( "default" );
			m_dicBuckets.Add( BucketType.DEFAULT, new CBAccountStorage( kDefault ) );

			IBucket kAccount = ClusterHelper.GetBucket( "FA_SUB" );
			m_dicBuckets.Add( BucketType.ACCOUNT, new CBAccountStorage( kAccount ) );

			IBucket kGame = ClusterHelper.GetBucket( "FA_MAIN" );
			m_dicBuckets.Add( BucketType.GAME, new CBGameStorage( kGame ) );
		}

		public void Init()
		{
			ClientConfiguration config = GetConfiguration();
			var cluster = new Cluster( config );
			ClusterHelper.Initialize( config );

			var bucket = cluster.OpenBucket( "default" );
			var bucket2 = cluster.OpenBucket( "FA_MAIN" );
			var bucket3 = cluster.OpenBucket( "FA_SUB" );

			IBucket bucket4 = ClusterHelper.GetBucket( CouchBaseConfigHelper.INST.Bucket );
		}

		public void Initial()
		{
			ClientConfiguration config = new ClientConfiguration();
			config.BucketConfigs.Clear();

			config.Servers = new List<Uri>( new Uri[] { new Uri( CouchBaseConfigHelper.INST.Server ) } );
			config.ViewRequestTimeout = 45000;
			config.UseSsl = false;
			config.BucketConfigs.Add(
				"Main",
				new BucketConfiguration
				{
					BucketName = CouchBaseConfigHelper.INST.Bucket,
					Username = CouchBaseConfigHelper.INST.User,
					Password = CouchBaseConfigHelper.INST.Password,
					DefaultOperationLifespan = 2000,
					PoolConfiguration = new PoolConfiguration
					{
						MaxSize = 10,
						MinSize = 5,
						SendTimeout = 12000
					}
				} );

			var cluster = new Cluster( config );
			var bucket = cluster.OpenBucket( "default" );
			var bucket2 = cluster.OpenBucket( "FA_MAIN" );
			var bucket3 = cluster.OpenBucket( "FA_SUB" );

			ClusterHelper.Initialize( config );
			//IBucket bucket = ClusterHelper.GetBucket( "default" );			
		}

		private ClientConfiguration GetConfiguration()
		{
			var config = new ClientConfiguration
			{
				Servers = new List<Uri>
				{
					new Uri("http://127.0.0.1:8091/"),
				},
				//UseSsl = true,
				UseSsl = false,
				DefaultOperationLifespan = 1000,
				BucketConfigs = new Dictionary<string, BucketConfiguration>
				{
					{
						"Main",
						new BucketConfiguration
						{
							BucketName = CouchBaseConfigHelper.INST.Bucket,
							Username = CouchBaseConfigHelper.INST.User,
							Password = CouchBaseConfigHelper.INST.Password,
							UseSsl = false,
							DefaultOperationLifespan = 2000,
							PoolConfiguration = new PoolConfiguration
							{
							MaxSize = 10,
							MinSize = 5,
							SendTimeout = 12000
							}
						}
					}
				}
			};

			return config;
		}

		public IBucket Bucket( string bucketName )
		{
			return ClusterHelper.GetBucket( bucketName );
		}

		public bool TryGetStorage<T> ( BucketType _eType, out T _kStorage ) where T : class
		{
			_kStorage = default( T );

			CouchBaseStorage kObj;
			if( false == m_dicBuckets.TryGetValue( _eType, out kObj ) )
				return false;

			_kStorage = kObj as T;
			if( null == _kStorage )
				return false;

			return true;
		}

		public bool FailOver()
		{
			ClusterHelper.Initialize( new ClientConfiguration
			{
				Servers = new List<Uri>
				{
					new Uri("http://127.0.0.1:8091/controller/failOver"),
					new Uri("http://127.0.0.1:8091/controller/failOver")
				}
			} );

			return true;
		}
	}
}