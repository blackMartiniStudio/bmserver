﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.Entities;

namespace WebServer.Domain.CouchBase.Transaction
{

	[JsonObject]
	public class TSTradeMoney : TransactionData<PlayerData, PlayerData>
	{
		[JsonProperty( PropertyName = "tradeData")]
		public TradeMoney       m_kTradeMoney;

		public TSTradeMoney( TradeMoney _kTradeMoney)
		{
			m_kTradeMoney = _kTradeMoney;
			type = "trade";
		}

		public override String FromKey()
		{
			return KeyMakerHelper.PlayerKey( m_kTradeMoney.fromAID.ToString() );
		}

		public override String ToKey()
		{
			return KeyMakerHelper.PlayerKey( m_kTradeMoney.toAID.ToString() );
		}

		public override bool ExecuteFrom( PlayerData model )
		{
			model.money -= m_kTradeMoney.money;
			model.transactionList.Add( id );

			return true;
		}
		public override bool ExecuteTo( PlayerData model )
		{
			model.money += m_kTradeMoney.money;
			model.transactionList.Add( id );

			return true;
		}

		public override bool CleanFrom( PlayerData model )
		{
			model.transactionList.Remove( id );
			return true;
		}

		public override bool CleanTo( PlayerData model )
		{
			model.transactionList.Remove( id );
			return true;
		}

		public override bool RollbackFrom( PlayerData model )
		{
			model.money += m_kTradeMoney.money;
			model.transactionList.Remove( id );

			return true;
		}

		public override bool RollbackTo( PlayerData model )
		{
			model.money -= m_kTradeMoney.money;
			model.transactionList.Remove( id );

			return true;
		}


		public override bool CheckRollbackFrom( PlayerData model )
		{
			return model.transactionList.Contains( id );
		}

		public override bool CheckRollbackTo( PlayerData model )
		{
			return model.transactionList.Contains( id );
		}

	}
}
