﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer.Domain.CouchBase.Transaction
{
	[JsonObject]
	public class TransactionObject
	{
		[JsonProperty( PropertyName = "transactionList")]
		public List<UInt64> transactionList = new List<UInt64>();
	}

	[JsonObject]
	public abstract class TransactionData<FromObj, ToObj>
	{
		[JsonProperty( PropertyName = "id")]
		public UInt64 id = 0;
		[JsonProperty( PropertyName = "type")]
		public String type = "";
		[JsonProperty( PropertyName = "state")]
		public String state = "init";
		[JsonProperty( PropertyName = "retry")]
		public Int16 retry = 0;

		public abstract String FromKey();
		public abstract String ToKey();
		public abstract bool ExecuteFrom( FromObj model );
		public abstract bool ExecuteTo( ToObj model );
		public abstract bool CleanFrom( FromObj model );
		public abstract bool CleanTo( ToObj model );
		public abstract bool RollbackFrom( FromObj model );
		public abstract bool RollbackTo( ToObj model );
		public abstract bool CheckRollbackFrom( FromObj model );
		public abstract bool CheckRollbackTo( ToObj model );
	}
}
