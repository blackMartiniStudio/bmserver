﻿using Couchbase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.Entities;
using BMDK;

namespace WebServer.Domain.CouchBase.Transaction
{
	public enum TransactionStatus
	{
		CANCEL		= -2,
		ROLLBACK	= -1,
		INIT		= 0,
		PENDING,
		COMMIT,
		DONE,
	}

	public enum TransationDefine
	{
		RETRY = 5,
	}

	public class Transaction<Storage, TSData, FromObj, ToObj>
		where Storage	: IStorageHelper
		where TSData	: TransactionData<FromObj, ToObj>
		where FromObj	: TransactionObject
		where ToObj		: TransactionObject
	{

		private static readonly Int16 s_MaxRetry = (Int16)TransationDefine.RETRY;

		private Storage m_kStorage;
		private TSData m_kTSData;
		private TSData m_kTSDocRef;

		public TSData TransactionDoc
		{
			get { return m_kTSDocRef; }
		}

		public Transaction( Storage _storage, TSData _data )
		{
			m_kStorage = _storage;
			m_kTSData = _data;
		}

		public async Task<String> Execute()
		{
			try
			{
				await UpdateInit();
				await UpdateTransactionState( "pending" );
				await UpdateExecute();
				await UpdateTransactionState( "commit" );
				await UpdateClean();
				await UpdateTransactionState( "done" );
			}
			catch( Exception ex )
			{
				//  로그
				BMLogger.ExceptionLog( ex );
				await RollBack();
				await UpdateTransactionState( "cancelled" );
			}
			finally
			{
				UpdateResult();
			}

			return m_kTSData.state;
		}

		private async Task UpdateInit()
		{
			var transactionID = await GetTransationIDAsync();
			m_kTSData.id = transactionID.Value;

			var result = await CreateTransactionDocAsync( m_kTSData );
			if( false == result.Success || result.Exception != null )
			{
				throw new CouchbaseException();
			}
		}

		private async Task UpdateTransactionState( String _state )
		{
			var result = await m_kStorage.GetAsync( KeyMakerHelper.TransationKey( m_kTSData.id ) );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			m_kTSDocRef = JsonConvert.DeserializeObject<TSData>( result.Value.ToString() );
			m_kTSDocRef.state = _state;

			result = await m_kStorage.ReplaceAsync( KeyMakerHelper.TransationKey( m_kTSDocRef.id ), m_kTSDocRef, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await UpdateTransactionState( _state );
					return;
				}

				throw new CouchbaseException();

			}
		}

		private async Task UpdateExecute()
		{
			await UpdateExecuteFrom();
			await UpdateExecuteTo();
		}

		private async Task UpdateExecuteFrom()
		{
			String fromKey = m_kTSDocRef.FromKey();
			var result = await m_kStorage.GetAsync( fromKey );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			FromObj model = JsonConvert.DeserializeObject<FromObj>( result.Value.ToString() );
			m_kTSDocRef.ExecuteFrom( model );

			result = await m_kStorage.ReplaceAsync( fromKey, model, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await UpdateExecuteFrom();
					return;
				}

				throw new CouchbaseException();
			}
		}

		private async Task UpdateExecuteTo()
		{
			String toKey = m_kTSDocRef.ToKey();
			var result = await m_kStorage.GetAsync( toKey );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			ToObj model = JsonConvert.DeserializeObject<ToObj>( result.Value.ToString() );
			m_kTSDocRef.ExecuteTo( model );

			result = await m_kStorage.ReplaceAsync( toKey, model, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await UpdateExecuteTo();
					return;
				}

				throw new CouchbaseException();
			}
		}

		private async Task UpdateClean()
		{
			await UpdateCleanFrom();
			await UpdateCleanTo();
		}

		private async Task UpdateCleanFrom()
		{
			String fromKey = m_kTSDocRef.FromKey();
			var result = await m_kStorage.GetAsync( fromKey );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			FromObj model = JsonConvert.DeserializeObject<FromObj>( result.Value.ToString() );
			m_kTSDocRef.CleanFrom( model );

			result = await m_kStorage.ReplaceAsync( fromKey, model, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await UpdateCleanFrom();
					return;
				}

				throw new CouchbaseException();
			}
		}

		private async Task UpdateCleanTo()
		{
			String toKey = m_kTSDocRef.ToKey();
			var result = await m_kStorage.GetAsync( toKey );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			ToObj model = JsonConvert.DeserializeObject<ToObj>( result.Value.ToString() );
			m_kTSDocRef.CleanTo( model );

			result = await m_kStorage.ReplaceAsync( toKey, model, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await UpdateCleanTo();
					return;
				}

				throw new CouchbaseException();
			}
		}

		private void UpdateResult()
		{
			if( null == m_kTSDocRef )
			{
				m_kTSData.state = "fail";
			}
				

			switch( m_kTSDocRef.state )
			{
				case "done":
					m_kTSData.state = "success";
					m_kTSDocRef.state = "success";
					break;
				default:
					m_kTSData.state = "fail";
					m_kTSDocRef.state = "fail";
					break;
			}
		}


		private async Task RollBack()
		{
			await RollBackFrom();
			await RollBackTo();
		}

		private async Task RollBackFrom()
		{
			if( null == m_kTSDocRef )
				return;

			String fromKey = m_kTSDocRef.FromKey();
			var result = await m_kStorage.GetAsync( fromKey );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			FromObj model = JsonConvert.DeserializeObject<FromObj>( result.Value.ToString() );
			if( false == m_kTSDocRef.CheckRollbackFrom( model ) )
				return;

			m_kTSDocRef.RollbackFrom( model );

			result = await m_kStorage.ReplaceAsync( fromKey, model, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await RollBackFrom();
					return;
				}

				throw new CouchbaseException();
			}
		}

		private async Task RollBackTo()
		{
			if( null == m_kTSDocRef )
				return;

			String toKey = m_kTSDocRef.ToKey();
			var result = await m_kStorage.GetAsync( toKey );
			if( false == result.Success || result.Exception != null || result.Value == null )
			{
				throw new CouchbaseException();
			}

			ToObj model = JsonConvert.DeserializeObject<ToObj>( result.Value.ToString() );
			if( false == m_kTSDocRef.CheckRollbackTo( model ) )
				return;

			m_kTSDocRef.RollbackTo( model );

			result = await m_kStorage.ReplaceAsync( toKey, model, result.Cas );
			if( false == result.Success || result.Exception != null )
			{
				//  여기서 재 시도를 할지 말지를 생각해 보자
				if( result.Status == Couchbase.IO.ResponseStatus.KeyExists && m_kTSDocRef.retry < s_MaxRetry )
				{
					m_kTSDocRef.retry++;
					await RollBackTo();
					return;
				}

				throw new CouchbaseException();
			}
		}

		public Task<IOperationResult<UInt64>> GetTransationIDAsync()
		{
			return m_kStorage.IncrementAsync( "TransactionID", 1, 1 );
		}

		protected Task<IOperationResult<dynamic>> CreateTransactionDocAsync( TransactionData<FromObj, ToObj> _kTranData )
		{
			return m_kStorage.UpsertAsync( KeyMakerHelper.TransationKey( _kTranData.id ), _kTranData );
		}
	}
}
