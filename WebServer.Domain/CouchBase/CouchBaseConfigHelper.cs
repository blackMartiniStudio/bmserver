﻿namespace WebServer.Domain.CouchBase
{
	public class CouchBaseConfigHelper
	{
		public CouchBaseConfigHelper()
		{
		}

		private static CouchBaseConfigHelper instance = null;

		public static CouchBaseConfigHelper INST
		{
			get { if( instance == null ) { instance = new CouchBaseConfigHelper(); } return instance; }
		}

		public string Bucket
		{
			get
			{
				return "default";
			}
		}

		public string Server
		{
			get
			{
				return "http://127.0.0.1:8091/";
			}
		}

		public string Password
		{
			get
			{
				return "tnraud12737";
			}
		}

		public string User
		{
			get
			{
				return "Administrator";
			}
		}

		public string JWTTokenSecret
		{
			get
			{
				return "secret_token";
			}
		}
	}
}