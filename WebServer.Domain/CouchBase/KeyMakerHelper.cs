﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer.Domain.CouchBase
{
	public class KeyMakerHelper
	{
		static String PlayerPrefix = "Player:";

		static public String PlayerKey( String id )
		{
			return PlayerPrefix + id;
		}

		static public String TransationKey( UInt64 id )
		{
			return "Transaction:" + id;
		}
	}
}
