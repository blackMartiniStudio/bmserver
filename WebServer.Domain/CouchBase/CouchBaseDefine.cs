﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer.Domain.CouchBase
{
	public enum BucketType
	{
		DEFAULT = 0,
		ACCOUNT = 1,
		GAME	= 2,
	}
}
