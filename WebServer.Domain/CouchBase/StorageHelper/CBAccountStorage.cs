﻿using Couchbase;
using Couchbase.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer.Domain.Entities;

namespace WebServer.Domain.CouchBase.StroageHelper
{
	public class CBAccountStorage : CouchBaseStorage
	{
		static String StorageName = "Account.";
		static String ProfilePrefix = "Profile:";


		public CBAccountStorage( IBucket _kBucket ) : base(_kBucket)
		{

		}

		public override String GetStorageName()
		{
			return StorageName;
		}


		public String GetProfileKey( String id )
		{
			return ProfilePrefix + id;
		}

		public async Task<Boolean> CreateUserAccount( LoginInfo loginInfo )
		{
			var reAidGen = await Bucket.IncrementAsync( "IDGenerator", 1, 100000000001 );
			if( !reAidGen.Success || reAidGen.Exception != null )
				return false;

			Object model = new { id = loginInfo.id, pwd = loginInfo.pwd, aid = reAidGen.Value };
			// 				String key = CouchBaseConfigHelper.INST.JWTTokenSecret;
			// 				string jsonToken = JsonWebToken.Encode( payload, key, JwtHashAlgorithm.HS256 );

			var reUpsert = await UpsertAsync( GetProfileKey( loginInfo.id ), model );

			if( !reUpsert.Success || reUpsert.Exception != null )
				return false;

			return true;
		}
	}
}
