﻿using Couchbase;
using Couchbase.Core;
using Couchbase.N1QL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer.Domain.CouchBase.StroageHelper
{
	public interface IStorageHelper
	{
		IBucket Bucket
		{
			get;
		}

		Task<IQueryResult<dynamic>> ExecuteQuery( IQueryRequest query );
		Task<IQueryResult<dynamic>> ExecuteQuery( string query );
		string ExecuteQuery( string query, Formatting format );
		string ExecuteQuery( IQueryRequest query, Formatting format );
		Task<IOperationResult<dynamic>> GetAsync( string id );
		Task<IOperationResult<dynamic>> UpsertAsync( string id, object model );
		Task<IOperationResult<dynamic>> ReplaceAsync( string key, object value );
		Task<IOperationResult<dynamic>> ReplaceAsync( string key, object value, UInt64 cas );
		Task<bool> ExistsAsync( string id );
		Task<IOperationResult<UInt64>> IncrementAsync( String v1, UInt64 v2, UInt64 v3 );
		String GetStorageName();
	}

}
