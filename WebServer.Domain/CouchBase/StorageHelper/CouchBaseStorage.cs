﻿using Couchbase;
using Couchbase.Core;
using Couchbase.N1QL;
using Newtonsoft.Json;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase.StroageHelper;
using System;

namespace WebServer.Domain.CouchBase
{
	public abstract class CouchBaseStorage : IStorageHelper
	{
		protected IBucket m_kBucket;


		public IBucket Bucket
		{
			get { return m_kBucket;  }
		}

		public CouchBaseStorage( IBucket _kBucket )
		{
			m_kBucket = _kBucket;
		}
		
		public virtual Task<IQueryResult<dynamic>> ExecuteQuery( IQueryRequest query )
		{
			return m_kBucket.QueryAsync<dynamic>( query );
		}
		
		public virtual Task<IQueryResult<dynamic>> ExecuteQuery( string query )
		{
			return ExecuteQuery( new QueryRequest( query ) );
		}

		public virtual string ExecuteQuery( string query, Formatting format )
		{
			return JsonConvert.SerializeObject( ExecuteQuery( query ), format );
		}

		public virtual string ExecuteQuery( IQueryRequest query, Formatting format )
		{
			return JsonConvert.SerializeObject( ExecuteQuery( query ), format );
		}

		public virtual Task<IOperationResult<dynamic>> GetAsync( string id )
		{
			return m_kBucket.GetAsync<dynamic>( id );
		}

		public virtual Task<IOperationResult<dynamic>> UpsertAsync( string id, object model ) 
		{
			return m_kBucket.UpsertAsync<dynamic>( id, model );
		}

		public virtual Task<IOperationResult<dynamic>> ReplaceAsync( string key, object value )
		{
			return m_kBucket.ReplaceAsync<dynamic>( key, value );
		}

		public virtual Task<IOperationResult<dynamic>> ReplaceAsync( string key, object value, UInt64 cas )
		{
			return m_kBucket.ReplaceAsync<dynamic>( key, value, cas );
		}

		public virtual Task<bool> ExistsAsync( string id )
		{
			return m_kBucket.ExistsAsync( id );
		}

		public virtual Task<IOperationResult<UInt64>> IncrementAsync( String v1, UInt64 v2, UInt64 v3 )
		{
			return m_kBucket.IncrementAsync( v1, v2, v3 );
		}

		public abstract String GetStorageName();
	}
}