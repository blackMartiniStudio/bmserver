﻿using Couchbase.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebServer.Domain.CouchBase.StroageHelper
{
	public class CBGameStorage : CouchBaseStorage
	{
		public CBGameStorage( IBucket _kBucket ) : base(_kBucket)
		{

		}

		public override String GetStorageName()
		{
			return "Game::";
		}


	}
}
