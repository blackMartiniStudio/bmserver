﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMServerCommonLib;

namespace GameServer
{
	public partial class RelayServerProxy : IProxyServer
	{
		public static Func<IProxyServer> CreateProxy()
		{
			return () => new RelayServerProxy();
		}

		public void Initialize()
		{
			
		}
	}
}
