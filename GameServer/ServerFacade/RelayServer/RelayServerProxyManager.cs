﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMServerCommonLib;

namespace GameServer
{
	public class RelayServerProxyManager : NetServerProxyManager
	{
		public Dictionary<UInt64, RelayServerProxy> DicServerProxy;

		public RelayServerProxyManager( BMNetServerDesc _kDesc, String ownerServerName ) 
			: base( _kDesc, ownerServerName )
		{
			DicServerProxy = new Dictionary<UInt64, RelayServerProxy>();
		}

		public override IProxyServer CreateProxyServer( UInt64 uid )
		{
			RelayServerProxy serverProxy;
			if( true == DicServerProxy.TryGetValue( uid, out serverProxy ) )
			{
				return serverProxy;
			}

			serverProxy = new RelayServerProxy();
			DicServerProxy.Add( uid, serverProxy );

			return serverProxy;
		}
	}
}
