﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;
using BMServerCommonLib;
using CSCommonLib;

namespace GameServer
{
	public class GMCommandHandler_Client_Room : SCommandHandler_Client
	{
		public GMCommandHandler_Client_Room( BMCommandCommunicator _CommandCommunicator ) 
			: base( _CommandCommunicator )
		{
			SetCmdHandler( (Int32)SPacketCommandEnum.CMD_PacketM2GCreateRoom, OnCreateRoom );
		}

		private BMCommandResult OnCreateRoom( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			ResultTable result = ResultTable.SUCCESS;
			NetClientProxy kClient = ToNetClient( _kHandler );
			PacketM2GCreateRoom kPkt = _kCommand.Packet as PacketM2GCreateRoom;

			BMLogger.DebugLog( "CommandID[{0}] ", ( (SPacketCommandEnum)_kCommand.ID ).ToString() );

			GRoom room;
			TDRoomInfo tdRoomInfo;
			if( false == GSMgr.INST.RoomManager.TryCreateRoom( kPkt.CreateRoomInfo, out room ) )
			{
				BMLogger.WarnLog( "TryCreateRoom Fail. MatchType[{0}], GameModeType[{1}], Capacity[{2}]"
					, kPkt.CreateRoomInfo.MatchType, kPkt.CreateRoomInfo.GameModeType, kPkt.CreateRoomInfo.Capacity );

				result = ResultTable.ROOM_CREATE_ERROR;
				tdRoomInfo = new TDRoomInfo();
			}

			tdRoomInfo = room.ToTDRoomInfo();

			BMLogger.InfoLog( "TryCreateRoom Success. RoomID[{0}] MatchType[{1}], GameModeType[{2}], Capacity[{3}]"
					, room.UIDRoom, room.RoomInfo.MatchType, room.RoomInfo.GameModeType, room.RoomInfo.Capacity );

			BMCommand cmd = kClient.NewCommand( (Int32)SPacketCommandEnum.CMD_PacketG2MCreateRoom );
			cmd.SetPacket( new PacketG2MCreateRoom( (int)result, tdRoomInfo ) );

			kClient.SendCommand( cmd );
			return BMCommandResult.SUCCESS;
		}

	}
}
