﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;

namespace GameServer
{
	public class GMCommandHandlerGroup
	{
		GMCommandHandler_Client_Room Room;
		public GMCommandHandlerGroup( BMCommandCommunicator _CommandCommunicator )
		{
			Room = new GMCommandHandler_Client_Room( _CommandCommunicator );
		}
	}
}
