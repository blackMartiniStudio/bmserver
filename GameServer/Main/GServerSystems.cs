﻿namespace GameServer
{
	public class GSSys : GGlobalSystem
	{
		#region [Simple Singleton]

		private static GSSys _inst = new GSSys();

		public static GSSys INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			GameServer = new GameServer();
		}

		public virtual void Destroy()
		{
		}
	}
}