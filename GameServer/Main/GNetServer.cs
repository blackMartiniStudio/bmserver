﻿using BMNet;
using CSCommonLib;

namespace GameServer
{
	public class GNetServer : BMNetServer
	{
		private ICommandGroup kCommandGroup;

		public GNetServer( BMNetServerDesc _kDesc ) : base( _kDesc )
		{
			kCommandGroup = new GCommandGroup( this );
		}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new CSCommandBuilder();
		}
	}
}