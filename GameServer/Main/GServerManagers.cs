﻿namespace GameServer
{
	public class GSMgr : GGlobalManager
	{
		#region [Simple Singleton]

		private static GSMgr _inst = new GSMgr();

		public static GSMgr INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			Config = new GServerConfig();
			RoomManager = new GRoomManager();
		}

		public virtual void Destroy()
		{
		}
	}
}