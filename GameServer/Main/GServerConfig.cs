﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMNet;
using BMServerCommonLib;
using CSCommonLib;
using Newtonsoft.Json;

namespace GameServer
{
	public class GServerConfig : ServerConfig
	{
		public ServerInfo Info;
		public ServerConnectionInfo ConnetionInfo;

		[JsonProperty("ProxyServerInfos")]
		public List<ServerInfo> ProxyServerInfos;

		public GServerConfig()
		{
		}

		public override bool Load( string filePath )
		{
			JsonFileLoader fileLoader = new JsonFileLoader( filePath );
			if( false == fileLoader.TryGetRead( ServerInfo.Key, out Info ) )
				return false;

			if( false == fileLoader.TryGetRead( ServerConnectionInfo.Key, out ConnetionInfo ) )
				return false;

			if( false == fileLoader.TryGetRead( "ProxyServerInfos", out ProxyServerInfos ) )
				return false;

			return true;
		}
	}
}
