﻿using BMDK;
using BMNet;
using BMServerCommonLib;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using CSCommonLib;

namespace GameServer
{
	// 실제 서버 메인 클래스
	public class GameServer : GBaseServer
	{
		public GNetServer NetServer { get; private set; }
		public MasterServerClient MasterClient { get; private set; }

		private Dictionary<string, NetServerProxyManager> DicProxyServer;

		public Int64 elapsedTick;

		public GameServer()
		{
			DicProxyServer = new Dictionary<string, NetServerProxyManager>();
		}

		protected override bool OnCreate()
		{
			if( false == CreateNetwork( GSMgr.INST.Config.Info ) )
				return false;

			if( false == CreateClientNetwork( GSMgr.INST.Config.ConnetionInfo ) )
				return false;

			if( false == CreateProxyGameServer( GSMgr.INST.Config.ProxyServerInfos ) )
				return false;

			if( false == StartNetwork() )
				return false;

			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			try
			{
				// 네트워크 업데이트 ( recv 처리 )
				NetServer?.Update();
				MasterClient?.Update();
				UpdateProxyServer();

				// 				if( MasterServerSocket?.IsSocketOpened() )
				// 				{
				// 					int a = 0;
				// 				}

				// DB 처리

				// 로직 업데이트 처리
				// frameRate 처리

			}
			catch( Exception /*ex*/ )
			{
			}

			return true;
		}

		private void UpdateProxyServer()
		{
			foreach( BMNetServer server in DicProxyServer.Values )
			{
				server.Update();
			}
		}

		protected override void OnDestroy()
		{
		}

		public bool CreateNetwork( ServerInfo info )
		{
			BMNetServerDesc kDesc = NetSettingHelper.CreateNetServerDesc( info );
			NetServer = new GNetServer( kDesc );

			return true;

			// List<IPAddress> liIps = NetworkHelper.GetIPv4AddressList();
			// return m_kNetServer.Start( AddressFamily.InterNetwork, kDesc.ListenPort, kDesc.NoDelay, kDesc.SocketPoolSize, kDesc.SendPendingLimitCount, kDesc.PublicIP );
		}

		private Boolean CreateClientNetwork( ServerConnectionInfo connetionInfo )
		{
			BMNetClientDesc kDesc = NetSettingHelper.CreateNetClientDesc( connetionInfo, NetServer.Desc.ServerName );
			kDesc.KeepAlive = true;

			MasterClient = new MasterServerClient( kDesc );

			return true;
		}


		private Boolean CreateProxyGameServer( List<ServerInfo> liProxyServer )
		{
			ProxyFactory factory = new ProxyFactory();
			foreach( ServerInfo info in liProxyServer )
			{
				BMNetServerDesc kDesc = NetSettingHelper.CreateNetServerDesc( info );
				NetServerProxyManager netServer = factory.CreateProxyServer( kDesc, NetServer.Desc.ServerName );
				if( netServer == null )
					return false;

				if( DicProxyServer.ContainsKey( kDesc.ServerName ) )
				{
					DicProxyServer[kDesc.ServerName] = netServer;
				}
				else
				{
					DicProxyServer.Add( kDesc.ServerName, netServer );
				}

			}


			return true;
		}

		private bool StartNetwork()
		{
			if( false == StartNetworkServer() )
				return false;

			if( false == StartNetworkClient() )
				return false;

			if( false == StartProxyGameServer() )
				return false;

			return true;
		}

		private bool StartNetworkServer()
		{
			BMNetServerDesc desc = NetServer.Desc;
			return NetServer.Start( AddressFamily.InterNetwork, desc.ListenPort, desc.NoDelay, desc.SocketPoolSize, desc.SendPendingLimitCount, desc.PublicIP );
		}


		private bool StartNetworkClient()
		{
			return MasterClient.StartNetwork();
		}

		private bool StartProxyGameServer()
		{
			foreach( BMNetServer server in DicProxyServer.Values )
			{
				BMNetServerDesc desc = server.Desc;
				if( false == server.Start( AddressFamily.InterNetwork, desc.ListenPort, desc.NoDelay, desc.SocketPoolSize, desc.SendPendingLimitCount, desc.PublicIP ) )
				{
					return false;
				}
			}

			return true;
		}

		public Func<IProxyServer> GetProxyServerCreateFunc( string ProxyServerName )
		{
			switch( ProxyServerName )
			{
				case "GameServer":
					return () => new GameServerProxy();
				case "RelayServer":
					return RelayServerProxy.CreateProxy();
				default:
					return () => new EmptyServerProxy();
			}
		}

	}
}
