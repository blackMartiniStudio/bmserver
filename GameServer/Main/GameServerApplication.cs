﻿using BMServerCommonLib;
using System;

namespace GameServer
{
	public class GameServerApplication : ServerApplication
	{

		protected override bool OnCreate()
		{
			// 전역 클래스 Instance
			GSSys.INST.Create();
			GSMgr.INST.Create();

			// 초기화
			if( false == Initialize() )
				return false;

			// 게임서버 초기화
			if( false == GSSys.INST.GameServer.Create( ProcessName ) )
				return false;

			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			UpdateConsoleKeyInput();
			return GSSys.INST.GameServer.Update( _elapsed );
		}

		protected override void OnDestroy()
		{
			GSSys.INST.GameServer.Destroy();
		}

		private bool Initialize()
		{
			// 로거 초기화

			// 리소스 초기화
			if( false == GSMgr.INST.Config.Load( "Config.json" ) )
				return false;

			// DB 초기화

			return true;
		}

		private void UpdateConsoleKeyInput()
		{
			// 			ConsoleKeyInfo key = Console.ReadKey();
			// 			String strKeyChar = key.KeyChar.ToString();
			// 			if( 0 == String.Compare( strKeyChar, "Y", true ) )
			// 			{
			// 				int a = 0;
			// 			}
		}
	}
}