﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;

namespace GameServer
{
	public class GRoomNetwork : BMUdpNetServer
	{
		private static Int32 nRecvCount = 0;

		public override void RecvCommand( Int32 bytesTransferred, Byte[] m_Buff, UdpPacketHeader kHaeder, EndPoint m_kPeerAddress )
		{
			nRecvCount++;
			m_setPeerAddress.Add( m_kPeerAddress );

			BMLogger.DebugLog( " CommandID = {0}, nRecvCount = {1}", kHaeder.cmdID, nRecvCount );
	
			Broadcast( m_Buff, bytesTransferred, m_kPeerAddress );
		}

		public override void Broadcast( Byte[] m_Buff, Int32 _nSize, EndPoint m_kPeerAddress )
		{
			foreach( EndPoint peer in m_setPeerAddress )
			{
				m_kReactor.Send( m_Buff, _nSize, peer );
			}
		}
	}
}
