﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using CSCommonLib;

namespace GameServer
{
	public class GRoom
	{
		public Int32 UIDRoom { get; set; }
		public static CSRoomInfo EmptyRoomInfo = new CSRoomInfo();
		public CSRoomInfo RoomInfo;
		public GRoomNetwork Net;

		public GRoom()
		{
			UIDRoom = 0;
			RoomInfo = EmptyRoomInfo;
			Net = new GRoomNetwork();
		}

		public void Initialize( CSRoomInfo roomInfo )
		{
			SetRoomInfo( roomInfo );
			Net.Create( UIDRoom + (int)NetCommon.UDP_BASE_PORT);
		}

		public void NetStart()
		{
			Net.Run();
		}

		internal void Destroy()
		{
			Net.Terminate();
		}

		private void SetRoomInfo( CSRoomInfo roomInfo )
		{
			RoomInfo = roomInfo;
		}
	
		public void Reset()
		{
			RoomInfo = EmptyRoomInfo;
		}
		
		public TDRoomInfo ToTDRoomInfo()
		{
			TDRoomInfo info = new TDRoomInfo();
			info.RoomID		= UIDRoom;
			info.MatchType	= RoomInfo.MatchType;
			info.GameModeType = RoomInfo.GameModeType;
			info.Capacity	= RoomInfo.Capacity;

			return info;
		}

		
	}
}
