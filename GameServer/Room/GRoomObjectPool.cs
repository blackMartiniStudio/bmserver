﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;

namespace GameServer
{
	public class GRoomObjectPool
	{
		private readonly Int32 m_nPoolSize = 1000;
		private ObjectPool<GRoom> m_RoomPool = new ObjectPool<GRoom>( () => new GRoom() );

		public GRoomObjectPool()
		{
			Int32 nInitPool = (Int32)( m_nPoolSize * 0.3 );//Math.Min( m_BufPool.MinimumSize, (Int32)(m_nPoolSize * 0.5));
			m_RoomPool.MinimumSize = m_nPoolSize;

			for( Int32 nId = 0; nId < nInitPool; ++nId )
			{
				m_RoomPool.Push( new GRoom() );
			}
		}

		public GRoom Get()
		{
			GRoom room = m_RoomPool.Pop();
			room.Reset();

			return room;
		}

		public void Restore( GRoom room )
		{
			m_RoomPool.Push( room );
		}

		public Int32 GetAvailableCount()
		{
			return m_RoomPool.AvailableCount;
		}
	}
}
