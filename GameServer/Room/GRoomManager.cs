﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using CSCommonLib;

namespace GameServer
{
	public class GRoomManager
	{
		public static int IncrementRoomUID { get; private set; }

		public GRoomObjectPool pool = new GRoomObjectPool();

		public Dictionary<Int32, GRoom> DicRoom;
		public GRoomManager()
		{
			IncrementRoomUID = 0;
			DicRoom = new Dictionary<int, GRoom>();
		}

		public bool TryCreateRoom( TDCreateRoomInfo createRoomInfo, out GRoom room )
		{
			room = pool.Get();
			if( room == null )
				return false;

			if( room.UIDRoom == 0 )
			{
				room.UIDRoom = GetRoomUID();
			}

			CSRoomInfo roomInfo = new CSRoomInfo( createRoomInfo );
			room.Initialize( roomInfo );

			if( DicRoom.ContainsKey( room.UIDRoom ) )
			{
				DicRoom.Remove( room.UIDRoom );
			}

			DicRoom.Add( room.UIDRoom, room );

			return true;
		}

		public bool DestroyRoom( GRoom room )
		{
			if( DicRoom.ContainsKey( room.UIDRoom ) )
			{
				DicRoom.Remove( room.UIDRoom );
			}

			room.Destroy();
			pool.Restore( room );
			return true;
		}

		public int GetRoomUID()
		{
			return ++IncrementRoomUID;
		}

	}
}
