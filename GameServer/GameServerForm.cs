﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using BMDK;
using BMServerCommonLib;

namespace GameServer
{
	public partial class GameServerForm : Form, ILogViewer
	{
		private GameServerApplication m_kApp = new GameServerApplication();

		// Delegate : 로그창에 로그를 남기기 위한 델리게이터
		public delegate void OnLogAdd( String logString );
		public OnLogAdd DelegateOnLogAdd;

		public GameServerForm()
		{
			InitializeComponent();

			Init();

			try
			{
				string ProcessName = ProcessHelper.GetProcessName();
				SLogger logger = new SLogger( ProcessName );
				logger.SetLogWindow( this );
				BMLogger.Initialize( logger );

				if( false == m_kApp.Create( ProcessName ) || false == m_kApp.Start() )
				{
					BMLogger.ErrLog("App Create or Start Fail!");
					m_kApp.Destroy();
					return;
				}
			}
			catch( Exception ex )
			{
				BMLogger.ExceptionLog( ex );
				m_kApp.Destroy();
			}

		}

		private void Init()
		{
			DelegateOnLogAdd = new OnLogAdd( AddToLogView );
		}

		public void AddToLogView( String logString )
		{
			if( listBox_mainWindow.InvokeRequired )
			{
				this.Invoke( DelegateOnLogAdd, logString );
				return;
			}

			if( listBox_mainWindow.Items.Count > 3000 )
			{
				listBox_mainWindow.Items.Clear();
			}

			string pushLog = DateTime.Now.ToString() + "\t" + logString;

			listBox_mainWindow.Items.Add( pushLog );

			if( checkBox_AutoScroll.Checked )
			{
				listBox_mainWindow.TopIndex = listBox_mainWindow.Items.Count - 1;
			}
			
		}
	}
}