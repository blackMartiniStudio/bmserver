﻿using BMNet;

namespace GameServer
{
	public class GCommandGroup : ICommandGroup
	{
		public GCommandGroup( BMCommandCommunicator _kCommunicator )
		{
			GCommandHandler_Common kCommon = new GCommandHandler_Common( _kCommunicator );
		}

		static public GNetServer NetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as GNetServer;
		}
	}
}