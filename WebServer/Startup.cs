﻿using Infrastructure.ExceptionIntercepts;
using Infrastructure.ExceptionIntercepts.Builder;
using Infrastructure.ExceptionIntercepts.ExceptionLoggers;
using Infrastructure.ExceptionInterceptsIntercepts.Handler;
using Microsoft.AspNet.Authentication.Cookies;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using WebServer.Domain.CouchBase;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.Global;
using WebServer.Models;

namespace WebServer
{
	public class Startup
	{
		public Startup( IHostingEnvironment env )
		{
			// Set up configuration sources.
			var builder = new ConfigurationBuilder()
				.AddJsonFile( "appsettings.json" );

			if( env.IsEnvironment( "Development" ) )
			{
				// This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
				builder.AddApplicationInsightsSettings( developerMode: true );
			}

			Configuration = builder.Build();

			WSSys.INST.Create();
			WSMgr.INST.Create();

			WSMgr.INST.m_kCouchbaseMgr.Init();
			//WSMgr.INST.m_kCouchbaseMgr.Initial();
		}

		public IConfigurationRoot Configuration { get; set; }

		// This method gets called by the runtime. Use this method to add services to the container
		public void ConfigureServices( IServiceCollection services )
		{
			// Add framework services.
			services.AddApplicationInsightsTelemetry( Configuration );

			services.AddExceptionInterceptManager();
			services.AddAuthorization();
			services.AddMvc();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
		public void Configure( IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory )
		{
			loggerFactory.AddConsole( Configuration.GetSection( "Logging" ) );
			loggerFactory.AddDebug();

			//app.UseExceptionHandler( "/Exception/Error" );

			app.UseIISPlatformHandler();
			app.UseApplicationInsightsRequestTelemetry();
			app.UseApplicationInsightsExceptionTelemetry();
			app.UseStaticFiles();

			app.UseCookieAuthentication( new CookieAuthenticationOptions
			{
				LoginPath = "/api/Account/Login",
				LogoutPath = "/api/Account/Logout",
				AccessDeniedPath = "/api/Account/Denied",
				AuthenticationScheme = CookieAuthenticationDefaults.AuthenticationScheme,
				AutomaticAuthenticate = true,
			//	AutomaticChallenge = true,
			} );

			app.UseExceptionInterceptManager( new ExceptionInterceptOptions() );
			app.AddExceptionInterceptHandler( new ExceptionInitializer( new ExceptionCategorizer() ) );
			app.AddExceptionInterceptHandler( new ExceptionJIRALogger() );
			app.AddExceptionInterceptHandler( new ExceptionDbLogger() );
			app.AddExceptionInterceptHandler( new ExceptionFinalizer() );
			app.UseMvc();

			app.UseMvcWithDefaultRoute();
		}

		// Entry point for the application.
		public static void Main( string[] args ) => WebApplication.Run<Startup>( args );
	}
}