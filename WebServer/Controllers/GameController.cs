﻿using Couchbase.Core;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.CouchBase.Transaction;
using WebServer.Domain.Entities;
using WebServer.Domain.Global;
using WebServer.Domain.Redis;

namespace WebServer.Controllers
{
	[Authorize]
	[Route( "api/[controller]/[action]" )]
	public class GameController : Controller
	{
		CBGameStorage    m_kStorage;

		public GameController()
		{
			WSMgr.INST.m_kCouchbaseMgr.TryGetStorage<CBGameStorage>( BucketType.GAME, out m_kStorage );
		}

		[HttpPost]
		public String Result( [FromBody]GameResult loginInfo )
		{
			return "aaa";
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<String> Trade( [FromBody]TradeMoney tradeMoney )
		{
			TSTradeMoney kTrade = new TSTradeMoney( tradeMoney );
			Transaction<CBGameStorage, TSTradeMoney, PlayerData, PlayerData> kTransaction = new Transaction<CBGameStorage, TSTradeMoney, PlayerData, PlayerData>( m_kStorage, kTrade );

			String strResult = await kTransaction.Execute();

			return strResult;
		}
    }
}
