﻿using Couchbase.Core;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.Entities;
using WebServer.Domain.Global;
using WebServer.Domain.Redis;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebServer.Controllers
{
	[Authorize]
	[Route( "api/[controller]/[action]" )]
	public class AccountController : Controller
	{
		CBAccountStorage    m_kStorage;
		public AccountController()
		{
			WSMgr.INST.m_kCouchbaseMgr.TryGetStorage<CBAccountStorage>( BucketType.ACCOUNT, out m_kStorage );
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<Object> Login( [FromBody]LoginInfo loginInfo )
		{
			try
			{

				var result = await m_kStorage.GetAsync( m_kStorage.GetProfileKey( loginInfo.id ) );
				if( false == result.Success || result.Status != Couchbase.IO.ResponseStatus.Success || result.Exception != null || result.Value == null )
				{
					return new { success = false };
				}

				// 					var jsonDecodedTokenString =
				// 						JsonWebToken
				// 						.Decode( result.Value, CouchBaseConfigHelper.INST.JWTTokenSecret, false );

				AccountInfo kAccountInfo = JsonConvert.DeserializeObject<AccountInfo>( result.Value.ToString() );
				var kAccount = JsonConvert.DeserializeAnonymousType( result.Value.ToString(), new { id = "", pwd = "", aid = "" } );
				if( kAccount.pwd != loginInfo.pwd )
				{
					return new { success = false };
				}

				var claims = new List<Claim>
					{
						new Claim("sub", kAccount.id),
						new Claim("name", kAccount.id),
						new Claim("email", kAccount.id + "@road99.co.kr"),
						new Claim("aid", kAccount.aid),
					};

				var id = new ClaimsIdentity( claims, "local", "name", "role" );
				await HttpContext.Authentication.SignInAsync( "Cookies", new ClaimsPrincipal( id ) );

				string sid = Guid.NewGuid().ToString();
				RedisManager.INST.SignIn( sid, kAccount.aid );

				return new { success = true, value = result.Value };
			}
			catch( Exception ex )
			{
				Console.WriteLine( ex );
				return new { success = false };
			}
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<Object> LoginCreate( [FromBody]LoginInfo loginInfo )
		{
			try
			{
				bool bExists = await m_kStorage.ExistsAsync( m_kStorage.GetProfileKey( loginInfo.id ) );
				if( bExists )
					throw new CouchbaseException( "No such user exists for login." );

				bool bCreate = await m_kStorage.CreateUserAccount( loginInfo );
				if( false == bCreate )
					throw new CouchbaseException( "CreateUserAccount Failed" );

				return await Login( loginInfo );
			}
			catch( CouchbaseException ex )
			{
				return new { success = false, value = ex.Message };
			}
		}

		public async Task<String> Logout()
		{
			await HttpContext.Authentication.SignOutAsync( "Cookies" );
			return "Log Off!";
		}

		public async Task<String> Denied()
		{
			return "Denied!";
		}

	}
}