﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.Global;

namespace WebServer.Controllers
{
	[Route( "api/[controller]/[action]" )]
	public class CBTestController : Controller
    {
		CBAccountStorage    m_kStorage;

		public CBTestController()
		{
			WSMgr.INST.m_kCouchbaseMgr.TryGetStorage<CBAccountStorage>( BucketType.ACCOUNT, out m_kStorage );
		}

		[HttpGet("{view}")]
		public async Task<String> ViewQueries( String view )
		{
			var query = m_kStorage.Bucket.CreateQuery( "Account", view, false );
			var queryResult = await m_kStorage.Bucket.QueryAsync<dynamic>( query );

			StringBuilder str = new StringBuilder();

			foreach( var raw in queryResult.Rows.OrderBy( m => m.Value["aid"]) )
			{
				str.AppendLine( raw.Value.ToString() );
			}

			return str.ToString();
		}

		[HttpGet]
		public String MultiGet()
		{
			var items = new List<String>
			{
				m_kStorage.GetProfileKey("kimjungmin"),
				m_kStorage.GetProfileKey("chdnjfwk"),
			};


			StringBuilder str = new StringBuilder();
			var multiGet = m_kStorage.Bucket.Get<dynamic>( items );
			foreach( var item in multiGet )
			{
				str.AppendLine( item.Value.Value.ToString() );
			}

			return str.ToString();
		}

	}
}
