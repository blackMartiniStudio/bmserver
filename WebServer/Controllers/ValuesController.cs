﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace WebServer.Controllers
{
	[Authorize]
	[Route( "api/[controller]" )]
	public class ValuesController : Controller
	{
		// GET: api/values
		[HttpGet]
		public IEnumerable<string> Get()
		{
			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		[HttpGet( "{id}" )]
		public Task<string> Get( int id )
		{
			if( id == 1 )
			{
				throw new UnauthorizedAccessException( "No such user exists for login." );
			}

			if( id == 2 )
			{
				throw new ValidationException( "Missing user name." );
			}

			throw new ArgumentNullException( nameof( id ) );
		}

		// POST api/values
		[HttpPost]
		public void Post( [FromBody]string value )
		{
		}

		// PUT api/values/5
		[HttpPut( "{id}" )]
		public void Put( int id, [FromBody]string value )
		{
		}

		// DELETE api/values/5
		[HttpDelete( "{id}" )]
		public void Delete( int id )
		{
		}
	}
}