// ASP.NET.Plus
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information. 

using System;
using Microsoft.AspNet.Http;
using Infrastructure.ExceptionIntercepts.Interfaces;

namespace Infrastructure.ExceptionIntercepts.Handler
{
	/// <summary>
	/// The exception context that holds the HttpContext and the actual
	/// unhandled exception that occurred in
	/// </summary>
	/// <seealso cref="Infrastructure.ExceptionIntercepts.ExceptionInterceptHandler.Interfaces.IExceptionInterceptContext" />    
	public class ExceptionInterceptContext : IExceptionInterceptContext
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public HttpContext Context { get; set; }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        public Exception Exception { get; set; }
    }
}
