﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace WebServer.Models
{
	public class ApplicationUser : IdentityUser
	{
	}

	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext()
		{
		}
	}

	public class ApplicationRole : IdentityRole
	{
	}
}