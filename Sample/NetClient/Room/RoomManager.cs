﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSCommonLib;

namespace NetClient
{
	public class RoomManager
	{
		public IGameRoom Room { get; private set; }

		internal void OnGetGameRoomInfo( TDRoomInfo roomInfo )
		{
			Room = new GameRoom();
			EnterRoom( roomInfo );
		}

		public bool EnterRoom( TDRoomInfo roomInfo )
		{
			return Global.NetMgr.EnterRoom( roomInfo );
		}

		public void OnEnterRoom( int roomID, byte slotID )
		{
			Room.OnEnterRoom( roomID, slotID );
		}

		public bool LeaveRoom()
		{
			return Global.NetMgr.LeaveRoom();
		}

		public void OnLeaveRoom()
		{
			Room.OnLeaveRoom();
			Release();
		}

		private void Release()
		{
			SetRoom( null );
		}

		public void SetRoom( GameRoom _room )
		{
			Room = _room;
		}

	}
}
