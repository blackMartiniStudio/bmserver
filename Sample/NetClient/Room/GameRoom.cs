﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using CSCommonLib;

public interface IGameRoom
{
	void OnEnterRoom( int roomID, byte slotID );
	
	void OnLeaveRoom();
	
}

public class EmptyRoom : IGameRoom
{
	public void OnEnterRoom( int roomID, byte slotID )
	{
	}

	public void OnLeaveRoom()
	{
	}
}

public class GameRoom : CSRoom, IGameRoom
{
	public Int32 RoomID { get; set; }
	public byte MySlotID { get; set; }

	
	public void OnEnterRoom( int roomID, byte slotID )
	{
		RoomID = roomID;
		MySlotID = slotID;
	}

	public void OnLeaveRoom()
	{

	}
}
