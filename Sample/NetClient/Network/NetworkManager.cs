﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using CSCommonLib;
using NetClient;

namespace NetClient
{
	public class NetworkManager
	{
		public XNetClientProxy ClientProxy { get; private set; }
		public XUdpNetClient UdpNet { get; private set; }

		public UdpClientInfo UdpLocalInfo { get; private set; }
		private Int32 UdpSendID = 0;

	

		public NetworkManager()
		{
			
		}

		public void CreatClient( ServerConnectionInfo info,  string ClientName )
		{
			BMNetClientDesc Desc = NetSettingHelper.CreateNetClientDesc( info, ClientName );
			Desc.KeepAlive = true;

			ClientProxy = new XNetClientProxy( Desc );
			ClientProxy.CreateSocket( AddressFamily.InterNetwork, Desc.ServerIp, Desc.ServerPort, false, true );
		}

		internal void Connect( bool isConnectionKeep = false )
		{
			ClientProxy.Connect( isConnectionKeep );

		}
		internal Boolean EnterRoom( TDRoomInfo roomInfo )
		{
			return false;
		}
		internal Boolean LeaveRoom()
		{
			return false;
		}

		public void Update()
		{
			ClientProxy.Update();
		}

			#region UDP Send Packet
		public void SendUdpPacket()
		{
			++UdpSendID;
			BMUdpCommand kCmd = UdpNet.NewCommand( UdpPacketEnum.CMD_UdpPacketPing, 1, 1 );
			kCmd.SetPacket( new UdpPacketPing( UdpSendID ) );

			UdpNet.PostCommand( kCmd );
		}

		public void SendUdpPingToPeer( EndPoint peerAddress )
		{
			BMUdpCommand kCmd = UdpNet.NewCommand( UdpPacketEnum.CMD_UdpPacketPing, 1, 1 );
			kCmd.SetPacket( new UdpPacketPing( UdpSendID ) );

			UdpNet.PostCommand( kCmd, peerAddress );
		}

		
		#endregion UDP Send Packet
	}
}