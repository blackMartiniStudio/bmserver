﻿using System.Collections.Generic;
using System.Net;
using BMNet;
using CSCommonLib;

namespace NetClient
{
	public class XNetClient : BMNetClient
	{
		public Dictionary<int, EndPoint> DicPlayAddress = new Dictionary<int, EndPoint>();

		private XCommandHandler_Common Common;
		private XCommandHandler_Room Room;

		public XNetClient( BMNetClientDesc _kDesc )
			: base( _kDesc )
		{
			m_bASyncSending = true;
			Common = new XCommandHandler_Common( this );
			Room = new XCommandHandler_Room( this );
		}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new CSCommandBuilder();
		}
	}
}