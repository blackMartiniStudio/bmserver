﻿using BMNet;

namespace NetClient
{
	public class XUdpNetClient : BMUdpNetClient
	{
		private XUdpCommandHandler kUdpCommandHandler;

		public XUdpNetClient()
		{
			kUdpCommandHandler = new XUdpCommandHandler( this );
		}
	}
}