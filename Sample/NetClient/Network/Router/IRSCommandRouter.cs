﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CSCommonLib;
using NetClient;

public interface IRelayServerCommandRouter
{
	#region TCP Send Packet
	void GetRoomList();
	void GetRoomInfo();
	void SendClientAddressInfo( UdpClientInfo udpInfo );
	
	void SendPacket();
	bool EnterRoom( TDRoomInfo roomInfo );
	bool LeaveRoom();
	void CreateRoom( TDCreateRoomInfo info );

	#endregion TCP Send Packet

}
