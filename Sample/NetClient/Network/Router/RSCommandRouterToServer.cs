﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using CSCommonLib;
using NetClient;

public class RSCommandRouterToServer : IRelayServerCommandRouter
{
	private Int32 SendID = 0;
	private XNetClientProxy clientProxy;


	public RSCommandRouterToServer( XNetClientProxy _clientProxy )
	{
		clientProxy = _clientProxy;
	}

	#region TCP Send Packet

	public void GetRoomList()
	{
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2MGetRoomList );
		//kCmd.SetPacket( new PacketC2MGetRoomList() );

		clientProxy.SendCommand( kCmd );
	}


	public void GetRoomInfo()
	{
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2MGetRoomInfo );
		clientProxy.SendCommand( kCmd );
	}

	public void CreateRoom( TDCreateRoomInfo info )
	{
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2MCreateRoom );
		kCmd.SetPacket( new PacketC2MCreateRoom( info ) );

		clientProxy.SendCommand( kCmd );
	}

	public void SendClientAddressInfo( UdpClientInfo udpInfo )
	{
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2RClientAddressInfo );
		kCmd.SetPacket( new PacketC2RClientAddressInfo( "192.168.119.16", udpInfo.LocalPort ) );

		clientProxy.SendCommand( kCmd );
	}


	public void SendPacket()
	{
		++SendID;
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2STest );
		kCmd.SetPacket( new PacketC2STest( SendID, SendID ) );

		clientProxy.SendCommand( kCmd );
	}

	public bool EnterRoom( TDRoomInfo roomInfo )
	{
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2REnterRoom );
		kCmd.SetPacket( new PacketC2REnterRoom( roomInfo.RoomID ) );
		clientProxy.SendCommand( kCmd );

		return true;
	}

	public bool LeaveRoom()
	{
		BMCommand kCmd = clientProxy.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketC2RLeaveRoom );
		kCmd.SetPacket( new PacketC2RLeaveRoom() );
		clientProxy.SendCommand( kCmd );

		return true;
	}
	#endregion TCP Send Packet


}
