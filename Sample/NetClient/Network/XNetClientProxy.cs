﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using CSCommonLib;

namespace NetClient
{
	public class XNetClientProxy : XNetClient
	{
		public IRelayServerCommandRouter CmdRouter;


		public XNetClientProxy( BMNetClientDesc _kDesc ) : base( _kDesc )
		{
			CmdRouter = new RSCommandRouterToStandalone();
		}

		public override void OnConnect()
		{
			base.OnConnect();

			CmdRouter = new RSCommandRouterToServer( this );
		}
		public override void Disconnect( string reasen = "" )
		{
			base.Disconnect( reasen );

			CmdRouter = new RSCommandRouterToStandalone();
		}

		public void SendClientAddressInfo( UdpClientInfo udpInfo )
		{
			CmdRouter.SendClientAddressInfo( udpInfo );
		}

		public void GetRoomList()
		{
			CmdRouter.GetRoomList();
		}

		public void GetooRmInfo()
		{
			CmdRouter.GetRoomInfo();
		}

		public void CreateRoom( Int16 matchType, Int16 gameModeType, byte capacity )
		{
			TDCreateRoomInfo info = new TDCreateRoomInfo( matchType, gameModeType, capacity );
			CmdRouter.CreateRoom( info );
		}

		public void SendPacket()
		{
			CmdRouter.SendPacket();
		}
		
	}
}
