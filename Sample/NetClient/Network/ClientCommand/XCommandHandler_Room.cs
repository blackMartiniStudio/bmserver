﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;
using CSCommonLib;

namespace NetClient
{
	public class XCommandHandler_Room : XCommandHandler
	{
		public XCommandHandler_Room( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetConnect, OnNetConnect );

			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketR2CClientAddressInfo, OnClientAddressInfo );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketM2CGetRoomList, OnGetRoomList );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketM2CGetRoomInfo, OnGetRoomInfo );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketM2CCreateRoom, OnCreateRoom );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketR2CEnterRoom, OnEnterRoom );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketR2CLeaveRoom, OnLeaveRoom );
		}

		private BMCommandResult OnNetConnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );
			kClient.OnConnect();
			Console.WriteLine( " OnLocalConnect " );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnNetRelayConnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketNetRelayConnect kPkt = _kCommand.Packet as PacketNetRelayConnect;
			kClient.OnReplyConnect( kPkt.tdNetRelayConnect.uidHost, kPkt.tdNetRelayConnect.uidAlloc, kPkt.tdNetRelayConnect.timeTick );

			Console.WriteLine( " OnNetRelayConnect : uidHost = {0}, uidAlloc = {1} ", kPkt.tdNetRelayConnect.uidHost, kPkt.tdNetRelayConnect.uidAlloc );

			return BMCommandResult.SUCCESS;
		}


		private BMCommandResult OnClientAddressInfo( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketR2CClientAddressInfo pkt = _kCommand.Packet as PacketR2CClientAddressInfo;

			EndPoint address = new IPEndPoint( IPAddress.Parse( pkt.ip ), pkt.port );

			Console.WriteLine( "OnClientAddressInfo : address = {0} ", address );

			if( kClient.DicPlayAddress.ContainsKey( pkt.enterID ) )
				kClient.DicPlayAddress[pkt.enterID] = address;
			else
				kClient.DicPlayAddress.Add( pkt.enterID, address );

			Global.NetMgr.SendUdpPingToPeer( address );

			return BMCommandResult.SUCCESS;
		}


		private BMCommandResult OnGetRoomList( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketM2CGetRoomList pkt = _kCommand.Packet as PacketM2CGetRoomList;
			BMLogger.DebugLog( "CommandID[{0}] : RoomListCount[{1}] ", ( (CSPacketCommandEnum)_kCommand.ID).ToString(), pkt.RoomList.arRoomInfos.Length );

			//Global.RoomMgr.OnGetGameRoomInfo( pkt.RoomInfo );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnGetRoomInfo( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketM2CGetRoomInfo pkt = _kCommand.Packet as PacketM2CGetRoomInfo;
			BMLogger.DebugLog( "OnGetRoomInfo : RoomID = {0} ", pkt.RoomInfo.RoomID );

			Global.RoomMgr.OnGetGameRoomInfo( pkt.RoomInfo );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnCreateRoom( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketM2CCreateRoom pkt = _kCommand.Packet as PacketM2CCreateRoom;
			BMLogger.DebugLog( "CommandID[{0}] - Result[{1}] ", ( (CSPacketCommandEnum)_kCommand.ID ).ToString(), ((ResultTable)pkt.Result).ToString() );

			return BMCommandResult.SUCCESS;
		}


		private BMCommandResult OnEnterRoom( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketR2CEnterRoom pkt = _kCommand.Packet as PacketR2CEnterRoom;
			BMLogger.DebugLog( "OnEnterRoom : RoomID = {0}, SlotID = {1}", pkt.RoomID, pkt.EntrySlotID );

			Global.RoomMgr.OnEnterRoom( pkt.RoomID, pkt.EntrySlotID );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnLeaveRoom( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketR2CEnterRoom pkt = _kCommand.Packet as PacketR2CEnterRoom;
			BMLogger.DebugLog( "OnLeaveRoom : Leave Result = {0}", ( (BMCommandResult)( pkt.Result ) ).ToString() );

			Global.RoomMgr.OnLeaveRoom();

			return BMCommandResult.SUCCESS;
		}
	}
}
