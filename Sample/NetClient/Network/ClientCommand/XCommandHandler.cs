﻿using BMNet;

namespace NetClient
{
	public class XCommandHandler : BMCommandHandler
	{
		public XCommandHandler( BMCommandCommunicator _CommandCommunicator )
			: base( _CommandCommunicator )
		{
		}

		protected XNetClient XNetClient( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as XNetClient;
		}
	}
}