﻿using BMNet;
using CSCommonLib;
using System;
using System.Net;
using BMDK;

namespace NetClient
{
	public class XCommandHandler_Common : XCommandHandler
	{
		public XCommandHandler_Common( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetConnect, OnNetConnect );
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetRelayConnect, OnNetRelayConnect );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketS2CTest, OnTetstPacket );
		}


		private BMCommandResult OnNetConnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );
			kClient.OnConnect();
			BMLogger.DebugLog( "OnLocalConnect " );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnNetRelayConnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			XNetClient kClient = XNetClient( _kHandler );

			PacketNetRelayConnect kPkt = _kCommand.Packet as PacketNetRelayConnect;
			kClient.OnReplyConnect( kPkt.tdNetRelayConnect.uidHost, kPkt.tdNetRelayConnect.uidAlloc, kPkt.tdNetRelayConnect.timeTick );

			BMLogger.DebugLog( "OnNetRelayConnect : uidHost = {0}, uidAlloc = {1} ", kPkt.tdNetRelayConnect.uidHost, kPkt.tdNetRelayConnect.uidAlloc );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnTetstPacket( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketS2CTest kCmd = _kCommand.Packet as PacketS2CTest;

			BMLogger.DebugLog( "CommandID = {0}, nServerRecvID = {1}, nClientSendID = {2}", _kCommand.ID, kCmd.nServerRecvID, kCmd.nClientSendID );

			if( kCmd.nServerRecvID != kCmd.nClientSendID )
			{
				//int a = 0;
			}

			return BMCommandResult.SUCCESS;
		}


	}
}