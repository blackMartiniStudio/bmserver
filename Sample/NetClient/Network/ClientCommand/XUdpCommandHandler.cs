﻿using BMNet;
using System;
using System.Net;

namespace NetClient
{
	public class XUdpCommandHandler
	{
		private BMUdpNetClient m_kCommunicator;

		public XUdpCommandHandler( BMUdpNetClient _Communicator )
		{
			m_kCommunicator = _Communicator;

			m_kCommunicator.SetCmdHandler( (Int32)UdpPacketEnum.CMD_UdpPacketPing, OnUdpPacketPing );
		}

		public BMCommandResult OnUdpPacketPing( BMUdpCommand _kCommand, EndPoint _kPeerAddress, IUdpCommandCommunicator _kHandler )
		{
			Console.WriteLine( " OnUdpPacketPing : FromID = {0}, ToID = {1} peerAddress = {2}", _kCommand.FromID, _kCommand.ToID, _kPeerAddress.ToString() );

			return BMCommandResult.SUCCESS;
		}
	}
}