﻿using BMNet;
using CSCommonLib;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using System.Threading;
using BMDK;

namespace NetClient
{
	public enum eKeyEvent
	{
		E_END = 1,
		E_CONNECT,
		E_SEND,
		E_BIND,
		E_UDP_SEND,
		E_GET_ROOM_LIST,
		E_GET_ROOM_INFO,
		E_CREATE_ROOM,
	}

	[JsonObject]
	public class RelayServerInfo
	{
		public static string Key = "RelayServerInfo";
		[JsonProperty("ServerIp")]
		public string ServerIp;
		[JsonProperty("ServerPort")]
		public int ServerPort;
	}

	public class UdpClientInfo
	{
		public static string Key = "UdpClientInfo";

		[JsonProperty("LocalPort")]
		public int LocalPort;
		[JsonProperty("ServerIp")]
		public string ServerIp;
		[JsonProperty("ServerPort")]
		public int ServerPort;
	}

	internal class Program
	{
		private static bool bRun = true;

		private static KeyInput keyInput;

		private static Object Lock = new object();
		private static List<eKeyEvent> liKeyEvent = new List<eKeyEvent>();

		private static ServerConnectionInfo connectionInfo;
		private static UdpClientInfo updInfo;

		private static void Main( string[] args )
		{
			keyInput = new KeyInput( KeyEvent );
			keyInput.Start();

			BMLogger.Initialize( new XConsoleLogger() );

			JsonFileLoader fileLoader = new JsonFileLoader( "Config.json" );
			
			if( false == fileLoader.TryGetRead( ServerConnectionInfo.Key, out connectionInfo ) )
				return;

			if( false == fileLoader.TryGetRead( UdpClientInfo.Key, out updInfo ) )
				return;

			Global.NetMgr.CreatClient( connectionInfo, "NetClient" );
			Global.NetMgr.Connect( true );

			while( bRun )
			{
				Global.NetMgr.Update();

				UpdateKeyEvent();
				Thread.Sleep( 10 );
			}
		}

		private static Int32 KeyEvent( String strKeyChar )
		{
			lock( Lock )
			{
				if( 0 == String.Compare( strKeyChar, "Q", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_END );
					keyInput.Stop();
				}
				else if( 0 == String.Compare( strKeyChar, "C", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_CONNECT );
				}
				else if( 0 == String.Compare( strKeyChar, "S", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_SEND );
				}
				else if( 0 == String.Compare( strKeyChar, "U", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_UDP_SEND );
				}
				else if( 0 == String.Compare( strKeyChar, "B", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_BIND );
				}
				else if( 0 == String.Compare( strKeyChar, "G", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_GET_ROOM_INFO );
				}
				else if( 0 == String.Compare( strKeyChar, "L", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_GET_ROOM_LIST );
				}
				else if( 0 == String.Compare( strKeyChar, "R", true ) )
				{
					liKeyEvent.Add( eKeyEvent.E_CREATE_ROOM );
				}
			}

			return 0;
		}
		private static void UpdateKeyEvent()
		{
			lock ( Lock )
			{
				foreach( eKeyEvent key in liKeyEvent )
				{
					switch( key )
					{
						case eKeyEvent.E_END:
							bRun = false;
							break;

						case eKeyEvent.E_CONNECT:
							// Global.NetMgr.Connect( true );
							break;

						case eKeyEvent.E_SEND:
							{
								for( int i = 0; i < 50; ++i )
								{
									Global.NetMgr.ClientProxy.SendPacket();
								}
							}
							break;

						case eKeyEvent.E_UDP_SEND:
							Global.NetMgr.SendUdpPacket();
							break;

						case eKeyEvent.E_BIND:
							Global.NetMgr.ClientProxy.SendClientAddressInfo( updInfo );
							break;

						case eKeyEvent.E_GET_ROOM_INFO:
							Global.NetMgr.ClientProxy.GetooRmInfo();
							break;
						case eKeyEvent.E_GET_ROOM_LIST:
							Global.NetMgr.ClientProxy.GetRoomList();
							break;
						case eKeyEvent.E_CREATE_ROOM:
							Global.NetMgr.ClientProxy.CreateRoom( 1, 2, 6 );
							break;
					}
				}

				liKeyEvent.Clear();
			}
		}

		
	}
}