﻿using System;
using System.Threading;

namespace NetClient
{
	public class KeyInput
	{
		private Thread kThread;
		private Func<String, Int32> CallbackFunc;

		private bool bRun = true;

		public KeyInput( Func<String, Int32> func )
		{
			CallbackFunc = func;
			kThread = new Thread( Update );
		}

		public void Start()
		{
			kThread.Start();
		}

		internal void Stop()
		{
			bRun = false;
			kThread.Join( 1000 );
		}

		private void Update()
		{
			while( bRun )
			{
				ConsoleKeyInfo key = Console.ReadKey( true );

				String strKeyChar = key.KeyChar.ToString();

				Console.WriteLine( "KeyInput = {0}", strKeyChar );

				CallbackFunc( strKeyChar );
			}
		}
	}
}