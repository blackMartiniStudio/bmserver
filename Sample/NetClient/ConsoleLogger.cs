﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;

namespace NetClient
{
	public class XConsoleLogger : IBMLogger
	{
		public void Debug( String text )
		{
			Console.WriteLine( text );
		}

		public void End()
		{
			throw new NotImplementedException();
		}

		public void Error( String text )
		{
			Console.WriteLine( text );
		}

		public void Fatal( String text )
		{
			Console.WriteLine( text );
		}

		public void Info( String text )
		{
			Console.WriteLine( text );
		}

		public void Start()
		{
			
		}

		public void Warning( String text )
		{
			Console.WriteLine( text );
		}
	}
}
