﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMDK;
using BMServerCommonLib;

namespace MasterServer
{
	public partial class MasterServerForm : Form, ILogViewer
	{
		private MasterServerApplication m_kApp = new MasterServerApplication();

		// Delegate : 로그창에 로그를 남기기 위한 델리게이터
		public delegate void OnLogAdd( String logString );
		public OnLogAdd DelegateOnLogAdd;
		public BMConcurrentQueue<string> LogQueue = new BMConcurrentQueue<string>();


		public MasterServerForm()
		{
			InitializeComponent();

			Init();

			try
			{

				string ProcessName = ProcessHelper.GetProcessName();
				SLogger logger = new SLogger( ProcessName );
				logger.SetLogWindow( this );
				BMLogger.Initialize( logger );


				if( false == m_kApp.Create( ProcessName ) || false == m_kApp.Start() )
				{
					BMLogger.ErrLog( "App Create or Start Fail!" );
					m_kApp.Destroy();
					return;
				}
			}
			catch( Exception ex )
			{
				BMLogger.ExceptionLog( ex );
				m_kApp.Destroy();
			}
		}

		private void Init()
		{
			DelegateOnLogAdd = new OnLogAdd( AddToLogView );

			UpdateViewTimer.Interval = 50;
			UpdateViewTimer.Start();
		}

		private void FormUpdate( object sender, EventArgs e )
		{
			LogListViewUpdate();
		}

		private void LogListViewUpdate()
		{
			string logString;
			while( LogQueue.TryDequeue( out logString ) )
			{
				if( listBox_mainWindow.InvokeRequired )
				{
					this.Invoke( DelegateOnLogAdd, logString );
					return;
				}

				if( listBox_mainWindow.Items.Count > 3000 )
				{
					listBox_mainWindow.Items.Clear();
				}

				string pushLog = DateTime.Now.ToString() + "\t" + logString;

				listBox_mainWindow.Items.Add( pushLog );

				if( checkBox_AutoScroll.Checked )
				{
					listBox_mainWindow.TopIndex = listBox_mainWindow.Items.Count - 1;
				}
			}
		}

		public void AddToLogView( String logString )
		{
			LogQueue.Enqueue( logString );
		}
	}
}
