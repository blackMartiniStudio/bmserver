﻿using BMAsyncDataBase;

namespace MasterServer
{
	public class MasterGlobalManager
	{
		protected MasterGlobalManager()
		{
		}

		public SqlDBManager		SqlDbManager;
		public MServerConfig	Config;
		public MRoomManager     RoomManager;
	}
}