﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMServerCommonLib;

namespace MasterServer
{
	public class ProxyFactory : IProxyFactory
	{
		public BMNetClient CreateProxyClient( BMNetClientDesc kDesc )
		{
			return new BMNetClient( kDesc );
		}

		public NetServerProxyManager CreateProxyServer( BMNetServerDesc kDesc, String ownerServerName )
		{
			switch( kDesc.ServerName )
			{
				case "GameServer":
					return new GameServerProxyManager( kDesc, ownerServerName );
				case "RelayServer":
					return new RelayServerProxyManager( kDesc, ownerServerName );
			}

			return null;
		}
	}
}
