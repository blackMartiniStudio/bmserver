﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMServerCommonLib;

namespace MasterServer
{
	public class GameServerProxyManager : NetServerProxyManager
	{
		public Dictionary<UInt64, GameServerProxy> DicServerProxy;
		public Func<IProxyServer> CreateProxyFunc;

		private MGCommandHandlerGroup CommandGroup;

		public GameServerProxyManager( BMNetServerDesc _kDesc, String ownerServerName ) 
			: base( _kDesc, ownerServerName )
		{
			DicServerProxy = new Dictionary<UInt64, GameServerProxy>();
			CommandGroup = new MGCommandHandlerGroup( this );
		}

		public override IProxyServer CreateProxyServer( UInt64 uid )
		{
			GameServerProxy serverProxy;
			if( true == DicServerProxy.TryGetValue( uid, out serverProxy ) )
			{
				return serverProxy;
			}

			serverProxy = new GameServerProxy( this, uid );
			DicServerProxy.Add( uid, serverProxy );

			return serverProxy;
		}

		public Int32 Count()
		{
			return DicServerProxy.Count;
		}

		internal bool TryGetServer( out GameServerProxy server )
		{
			server = default( GameServerProxy );

			if( 0 == DicServerProxy.Count )
				return false;

			server = DicServerProxy.Values.FirstOrDefault();
			if( server == null )
				return false;

			return true;
		}
	}
}
