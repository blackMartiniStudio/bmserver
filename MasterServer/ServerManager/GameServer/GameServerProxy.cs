﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMServerCommonLib;
using CSCommonLib;

namespace MasterServer
{
	public partial class GameServerProxy : IProxyServer
	{
		private GameServerProxyManager Server;
		private UInt64 UID;

		public GameServerProxy( GameServerProxyManager gameServerProxyManager, UInt64 uid )
		{
			Server = gameServerProxyManager;
			this.UID = uid;
		}

		internal void CreateRoom( TDCreateRoomInfo createInfo )
		{
			BMCommand kNewCommand = Server.NewCommand( (Int32)SPacketCommandEnum.CMD_PacketM2GCreateRoom, UID );
			PacketM2GCreateRoom pkt = new PacketM2GCreateRoom( createInfo );

			kNewCommand.SetPacket( pkt );
			Server.Post( kNewCommand );
		}
	}
}
