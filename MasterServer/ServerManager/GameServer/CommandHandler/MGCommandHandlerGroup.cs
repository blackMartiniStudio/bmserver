﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;
using BMServerCommonLib;

namespace MasterServer
{
	public class MGCommandHandlerGroup
	{
		MGCommandHandler_Room Room;
		public MGCommandHandlerGroup( BMCommandCommunicator _kCommunicator )
		{
			Room = new MGCommandHandler_Room( _kCommunicator );
		}

		static public NetServerProxyManager ToNetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as NetServerProxyManager;
		}
	}

	
}
