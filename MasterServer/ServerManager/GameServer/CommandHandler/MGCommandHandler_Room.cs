﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;
using BMServerCommonLib;

namespace MasterServer
{
	public class MGCommandHandler_Room : BMCommandHandler
	{
 
		public MGCommandHandler_Room( BMCommandCommunicator _CommandCommunicator )
			: base( _CommandCommunicator )
		{
			SetCmdHandler( (Int32)SPacketCommandEnum.CMD_PacketG2MCreateRoom, OnCreateRoom );
		}

		private BMCommandResult OnCreateRoom( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			NetServerProxyManager kServer = MGCommandHandlerGroup.ToNetServer( _kHandler );

			if( kServer == null )
				return BMCommandResult.FAIL;

			BMLogger.InfoLog( "CommandID = {0} ", ( (SPacketCommandEnum)_kCommand.ID ).ToString());

// 			BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketS2CTest, _kCommand.SendererUID );
// 			PacketS2CTest kSendCmd = new PacketS2CTest( (int)ResultTable.SUCCESS, ++nRecvCount, kCmd.nClientSendCount );
// 
// 			kNewCommand.SetPacket( kSendCmd );
// 
// 
// 			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}
	}
}
