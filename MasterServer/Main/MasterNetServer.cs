﻿using BMNet;
using CSCommonLib;

namespace MasterServer
{
	public class MasterNetServer : BMNetServer
	{
		private ICommandGroup kCommandGroup;

		public MasterNetServer( BMNetServerDesc _kDesc ) : base( _kDesc )
		{
			kCommandGroup = new MasterCommandGroup( this );
		}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new CSCommandBuilder();
		}
	}
}