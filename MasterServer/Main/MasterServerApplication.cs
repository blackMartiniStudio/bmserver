﻿using BMServerCommonLib;
using System;
using System.Configuration;
using BMAsyncDataBase;
using System.Collections.Generic;
using BMDK;
using System.Data.Common;
using System.Threading.Tasks;
using CSCommonLib;
using Newtonsoft.Json.Linq;

namespace MasterServer
{
	public class MasterServerApplication : ServerApplication
	{
		protected override bool OnCreate()
		{
			// 전역 클래스 Instance
			MasterGSys.INST.Create();
			MasterGMgr.INST.Create();

			// 리소스 초기화
			if( false == Initialize() )
				return false;

			// DB 초기화
			if( false == DBInitialize() )
				return false;

			// 게임서버 초기화
			if( false == MasterGSys.INST.Server.Create( ProcessName ) )
				return false;


			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			UpdateConsoleKeyInput();
			return MasterGSys.INST.Server.Update( _elapsed );
		}

		protected override void OnDestroy()
		{
			BMLogger.End();
			MasterGSys.INST.Server.Destroy();
		}

		private bool Initialize()
		{
			if( false == MasterGMgr.INST.Config.Load( "Config.json" ) )
				return false;
		
			return true;
		}

		private bool DBInitialize()
		{
            List<DBConnectionInfo> liDBConnInfo = new List<DBConnectionInfo>() { MasterGMgr.INST.Config.DbInfo };
            if (false == MasterGMgr.INST.SqlDbManager.Connect(liDBConnInfo, new SqlDBFactory(DBKind.MySQL)))
                return false;

            if (false == MasterGMgr.INST.SqlDbManager.Start())
                return false;

            return true;
		}

		private void UpdateConsoleKeyInput()
		{
			// 			ConsoleKeyInfo key = Console.ReadKey();
			// 			String strKeyChar = key.KeyChar.ToString();
			// 			if( 0 == String.Compare( strKeyChar, "Y", true ) )
			// 			{
			// 				int a = 0;
			// 			}
		}
	}
}