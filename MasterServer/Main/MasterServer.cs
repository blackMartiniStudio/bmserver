﻿using BMDK;
using BMNet;
using BMServerCommonLib;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using BMAsyncDataBase;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

namespace MasterServer
{
	// 실제 서버 메인 클래스
	public class MasterServer : GBaseServer
	{
		private MasterNetServer m_kNetServer;

		private Dictionary<string, NetServerProxyManager> DicProxyServer;

		public ServerConnectionInfo MyServerInfo { get; private set; }
		public List<ServerConnectionInfo> ServerConnectonInfos { get; set; }

		public MasterServer()
		{
			MyServerInfo = new ServerConnectionInfo();
			ServerConnectonInfos = new List<ServerConnectionInfo>();
			DicProxyServer = new Dictionary<string, NetServerProxyManager>();
		}

		protected override bool OnCreate()
		{
			if( false == CreateNetwork( MasterGMgr.INST.Config.Info) )
				return false;

			if( false == CreateProxyGameServer( MasterGMgr.INST.Config.ProxyServerInfos ) )
				return false;

            StartNetwork(new List<ServerConnectionInfo>());

            /*
			if( false == LoadServerConnectionInfo() )
				return false;
            */

            return true;
		}


		protected override bool OnUpdate( Int64 _elapsed )
		{
			try
			{
				// 네트워크 업데이트 ( recv 처리 )
				m_kNetServer?.Update();
				UpdateProxyServer();

				// DB 처리
				MasterGMgr.INST.SqlDbManager?.Update();


				// 로직 업데이트 처리

				// frameRate 처리
			}
			catch( Exception /*ex*/ )
			{
			}

			return true;
		}

		private void UpdateProxyServer()
		{
			foreach( BMNetServer server in  DicProxyServer.Values )
			{
				server.Update();
			}
		}

		protected override void OnDestroy()
		{
			MasterGMgr.INST.SqlDbManager.Stop();
		}

		private Boolean LoadServerConnectionInfo()
		{
			SqlMasterHandler handler;
			if( false == MasterGMgr.INST.SqlDbManager.TryGetHandler( DBType.master, out handler ) )
				return false;

			handler.LoadServerConnectionInfo();

			return true;
		}

		public bool CreateNetwork( ServerInfo info )
		{
			string jsonFormat = JsonConvert.SerializeObject( info );
			BMLogger.InfoLog( "Server Network Info : {0}", jsonFormat );
			BMNetServerDesc kDesc = NetSettingHelper.CreateNetServerDesc( info );
			m_kNetServer = new MasterNetServer( kDesc );
			return true;
		}

		private Boolean CreateProxyGameServer( List<ServerInfo> liProxyServer )
		{
			ProxyFactory factory = new ProxyFactory();
			foreach( ServerInfo info in liProxyServer )
			{
				BMNetServerDesc kDesc = NetSettingHelper.CreateNetServerDesc( info );
				NetServerProxyManager netServer = factory.CreateProxyServer( kDesc, m_kNetServer.Desc.ServerName );
				if( netServer == null )
					return false;

				string jsonFormat = JsonConvert.SerializeObject( info );
				BMLogger.InfoLog( "Proxy Server Network Info : {0}", jsonFormat );

				if( DicProxyServer.ContainsKey( kDesc.ServerName ) )
					DicProxyServer[kDesc.ServerName] = netServer;
				else
					DicProxyServer.Add( kDesc.ServerName, netServer );
			}
			
			return true;
		}

		internal void StartNetwork( List<ServerConnectionInfo> list )
		{
			ServerConnectonInfos = list;
			MyServerInfo = ServerConnectonInfos.Find( item => item.ServerName == ProcessName );

			StartNetworkServer();
			StartProxyGameServer();
		}

		private bool StartNetworkServer()
		{
			BMNetServerDesc desc = m_kNetServer.Desc;
			return m_kNetServer.Start( AddressFamily.InterNetwork, desc.ListenPort, desc.NoDelay, desc.SocketPoolSize, desc.SendPendingLimitCount, desc.PublicIP );
		}

		private bool StartProxyGameServer()
		{
			foreach( BMNetServer server in DicProxyServer.Values )
			{
				BMNetServerDesc desc = server.Desc;
				if( false == server.Start( AddressFamily.InterNetwork, desc.ListenPort, desc.NoDelay, desc.SocketPoolSize, desc.SendPendingLimitCount, desc.PublicIP ) )
				{
					return false;
				}
			}

			return true;
		}

		public Int32 GetListenPort()
		{
			if( MyServerInfo.Port == 0 )
				return 10000;

			return MyServerInfo.Port;
		}

		public string GetPublicIPv4()
		{
			List<IPAddress> liIps = NetworkHelper.GetIPv4AddressList();
			if( liIps.Count <= 0 && false == string.IsNullOrEmpty( MyServerInfo.Ip ) )
				return MyServerInfo.Ip;

			string publicIP = MyServerInfo.Ip;

			IPAddress find = liIps.Find( item => item.ToString() == MyServerInfo.Ip );
			return find.ToString();
		}

		public bool TryGetServerProxyManager<T>( out T manager ) where T : NetServerProxyManager
		{
			manager = default( T );

			NetServerProxyManager proxyManager;
			if( false == DicProxyServer.TryGetValue( "GameServer", out proxyManager ) )
				return false;

			manager = proxyManager as T;
			if( manager == null )
				return false;

			return true;
		}
	}
}