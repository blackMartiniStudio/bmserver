﻿using BMAsyncDataBase;

namespace MasterServer
{
	public class MasterGMgr : MasterGlobalManager
	{
		#region [Simple Singleton]

		private static MasterGMgr _inst = new MasterGMgr();

		public static MasterGMgr INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			SqlDbManager = new SqlDBManager( new DBThreadScheduler(8) );
			Config = new MServerConfig();
			RoomManager = new MRoomManager();
		}

		public virtual void Destroy()
		{
		}
	}
}