﻿namespace MasterServer
{
	public class MasterGSys : MasterGlobalSystem
	{
		#region [Simple Singleton]

		private static MasterGSys _inst = new MasterGSys();

		public static MasterGSys INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			Server = new MasterServer();
		}

		public virtual void Destroy()
		{
		}
	}
}