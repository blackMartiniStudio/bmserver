﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMAsyncDataBase;
using BMNet;
using BMServerCommonLib;
using CSCommonLib;
using Newtonsoft.Json;

namespace MasterServer
{

	public class MServerConfig : ServerConfig
	{
		public ServerInfo Info;

		[JsonProperty("ProxyServerInfos")]
		public List<ServerInfo> ProxyServerInfos;

		public DBConnectionInfo DbInfo;

		public MServerConfig()
		{
		}

		public override bool Load( string filePath )
		{
			JsonFileLoader fileLoader = new JsonFileLoader( filePath );

			if( false == fileLoader.TryGetRead( ServerInfo.Key, out Info ) )
				return false;

			if( false == fileLoader.TryGetRead( DBConnectionInfo.Key, out DbInfo ) )
				return false;

			if( false == fileLoader.TryGetRead( "ProxyServerInfos", out ProxyServerInfos ) )
				return false;

			return true;
		}
	}
}
