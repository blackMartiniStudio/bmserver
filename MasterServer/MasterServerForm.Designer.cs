﻿using System;
using System.Windows.Forms;

namespace MasterServer
{
	partial class MasterServerForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );

			m_kApp.Destroy();
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.listBox_mainWindow = new System.Windows.Forms.ListBox();
			this.Btn_ClearLog = new System.Windows.Forms.Button();
			this.checkBox_AutoScroll = new System.Windows.Forms.CheckBox();
			this.UpdateViewTimer = new Timer( this.components );
			this.SuspendLayout();
			// 
			// listBox_mainWindow
			// 
			this.listBox_mainWindow.FormattingEnabled = true;
			this.listBox_mainWindow.HorizontalScrollbar = true;
			this.listBox_mainWindow.ItemHeight = 12;
			this.listBox_mainWindow.Location = new System.Drawing.Point(3, 0);
			this.listBox_mainWindow.Name = "listBox_mainWindow";
			this.listBox_mainWindow.Size = new System.Drawing.Size(780, 424);
			this.listBox_mainWindow.TabIndex = 4;
			// 
			// Btn_ClearLog
			// 
			this.Btn_ClearLog.Location = new System.Drawing.Point(636, 443);
			this.Btn_ClearLog.Name = "Btn_ClearLog";
			this.Btn_ClearLog.Size = new System.Drawing.Size(122, 41);
			this.Btn_ClearLog.TabIndex = 7;
			this.Btn_ClearLog.Text = "Clear Log";
			this.Btn_ClearLog.UseVisualStyleBackColor = true;
			this.Btn_ClearLog.Click += new System.EventHandler(this.Btn_ClearLog_Click);
			// 
			// checkBox_AutoScroll
			// 
			this.checkBox_AutoScroll.AutoSize = true;
			this.checkBox_AutoScroll.Checked = true;
			this.checkBox_AutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox_AutoScroll.Location = new System.Drawing.Point(466, 466);
			this.checkBox_AutoScroll.Name = "checkBox_AutoScroll";
			this.checkBox_AutoScroll.Size = new System.Drawing.Size(85, 16);
			this.checkBox_AutoScroll.TabIndex = 16;
			this.checkBox_AutoScroll.Text = "Auto Scroll";
			this.checkBox_AutoScroll.UseVisualStyleBackColor = true;
			// 
			// UpdateTimer
			// 
			this.UpdateViewTimer.Tick += new System.EventHandler( this.FormUpdate );

			// 
			// MasterServerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(795, 506);
			this.Controls.Add(this.checkBox_AutoScroll);
			this.Controls.Add(this.Btn_ClearLog);
			this.Controls.Add(this.listBox_mainWindow);
			this.KeyPreview = true;
			this.Name = "MasterServerForm";
			this.Text = "MasterServer";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormKeyPress);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		private void FormMain_FormClosing( Object sender, FormClosingEventArgs e )
		{
			m_kApp.Destroy();
		}


		private void Btn_ClearLog_Click( Object sender, EventArgs e )
		{
			listBox_mainWindow.Items.Clear();
		}



		private void FormKeyPress( object sender, KeyPressEventArgs e )
		{
			String strKeyChar = e.KeyChar.ToString();

			if( 0 == String.Compare( strKeyChar, "Y", true ) )
			{
				//int a = 0;
			}
		}

		#endregion

		private ListBox listBox_mainWindow;
		private Button Btn_ClearLog;
		private CheckBox checkBox_AutoScroll;
		private Timer       UpdateViewTimer;
	}
}

