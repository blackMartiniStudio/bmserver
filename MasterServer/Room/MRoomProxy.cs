﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSCommonLib;

namespace MasterServer
{
	public class MRoomProxy
	{
		public int RoomID;
		public short MatchType;
		public short GameModeType;
		public byte Capacity;

		internal TDRoomInfo ToTDRoomInfo()
		{
			return new TDRoomInfo( RoomID, MatchType, GameModeType, Capacity );
		}
	}
}
