﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSCommonLib;

namespace MasterServer
{
	public class MRoomManager
	{
		Dictionary<Int32, MRoomProxy> DicRoom;

		public MRoomManager()
		{
			DicRoom = new Dictionary<Int32, MRoomProxy>();
		}

		internal List<MRoomProxy> GetRoomList()
		{
			return DicRoom.Values.ToList();
		}

		public TDRoomList ToTDRoomList()
		{
			TDRoomList tdRoomList = new TDRoomList();
			tdRoomList.arRoomInfos = DicRoom.Values.Select( item => item.ToTDRoomInfo() ).ToArray();

			return tdRoomList;
		}
	}
}
