﻿using BMNet;

namespace MasterServer
{
	public class MasterCommandGroup : ICommandGroup
	{
		MasterCommandHandler_Common common;
		MasterCommandHandler_Room room;
		public MasterCommandGroup( BMCommandCommunicator _kCommunicator )
		{
			common = new MasterCommandHandler_Common( _kCommunicator );
			room = new MasterCommandHandler_Room( _kCommunicator );
		}

		static public MasterNetServer NetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as MasterNetServer;
		}
	}
}