﻿using BMNet;
using CSCommonLib;
using System;
using System.Diagnostics;
using BMDK;
using BMServerCommonLib;

namespace MasterServer
{
	public class MasterCommandHandler_Room : BMCommandHandler
	{
		private static Int32 nRecvCount = 0;

		public MasterCommandHandler_Room( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketC2MGetRoomList, OnGetRoomList );
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketC2MCreateRoom, OnCreateRoom );
		}

		private BMCommandResult OnTetstPacket( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketC2STest kCmd = _kCommand.Packet as PacketC2STest;

			MasterNetServer kServer = MasterCommandGroup.NetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketS2CTest, _kCommand.SendererUID );
			PacketS2CTest kSendCmd = new PacketS2CTest( (int)ResultTable.SUCCESS, ++nRecvCount, kCmd.nClientSendCount );

			BMLogger.InfoLog( "CommandID = {0}, nClientSendCount = {1}, ServerRecvCount = {2}", ( (CSPacketCommandEnum)_kCommand.ID ).ToString(), kCmd.nClientSendCount, nRecvCount );
			
			kNewCommand.SetPacket( kSendCmd );

			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnGetRoomList( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			MasterNetServer kServer = MasterCommandGroup.NetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			BMLogger.DebugLog( "Recv CommandID = {0}", ((CSPacketCommandEnum)_kCommand.ID).ToString() );

			BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketM2CGetRoomList, _kCommand.SendererUID );
			TDRoomList tdRoomList = MasterGMgr.INST.RoomManager.ToTDRoomList();

			PacketM2CGetRoomList kSendCmd = new PacketM2CGetRoomList( (int)ResultTable.SUCCESS, tdRoomList );

			kNewCommand.SetPacket( kSendCmd );

			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}


		private BMCommandResult OnCreateRoom( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketC2MCreateRoom kCmd = _kCommand.Packet as PacketC2MCreateRoom;
			MasterNetServer kServer = MasterCommandGroup.NetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			BMLogger.DebugLog( "Recv CommandID = {0}, RoomInfo : Match[{1}], GameMode[{2}], Capacity[{3}] ", ( (CSPacketCommandEnum)_kCommand.ID ).ToString()
				, kCmd.CreateInfo.MatchType, kCmd.CreateInfo.GameModeType, kCmd.CreateInfo.Capacity );


			GameServerProxyManager proxyManager;
			GameServerProxy gameServer;
			if( false == MasterGSys.INST.Server.TryGetServerProxyManager( out proxyManager ) ||
				false == proxyManager.TryGetServer( out gameServer ))
			{
				BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketM2CCreateRoom, _kCommand.SendererUID );
				PacketM2CCreateRoom kSendCmd = new PacketM2CCreateRoom( (int)ResultTable.ROOM_CREATE_ERROR_DOES_NOT_EXIST_GAMESERVER, new TDRoomInfo() );

				kNewCommand.SetPacket( kSendCmd );
				kServer.Post( kNewCommand );

				return BMCommandResult.SUCCESS;
			}

			gameServer.CreateRoom( kCmd.CreateInfo );

			return BMCommandResult.SUCCESS; 
		}


	}
}