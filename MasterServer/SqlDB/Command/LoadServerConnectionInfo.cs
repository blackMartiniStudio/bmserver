﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using BMAsyncDataBase;
using BMNet;

namespace MasterServer
{
	public class LoadServerConnectionInfo : BMDBCommand
	{
		List<ServerConnectionInfo> list;

		public LoadServerConnectionInfo( SqlDBHandler _dbHandler ) : base( _dbHandler )
		{
			SpName = "sp_GetServerConnectionInfo";
		//	DBHandler.InParam( ParamList, "" )
		}

		public override void OnRead( DbDataReader reader )
		{
			if( false == IsSuccess() )
				return;

			list = new List<ServerConnectionInfo>();

			if( reader.HasRows )
			{
				while( reader.Read() )
				{
					ServerConnectionInfo info = new ServerConnectionInfo();
					info.ServerName = (string)reader["ServerName"];
					info.Ip = (string)reader["PublicIP"];
					info.Port = (Int32)reader["ListenPort"];

					list.Add( info );
				}
			}
		}

		public override void OnFinish()
		{
			if( false == IsSuccess() )
				return;

			MasterGSys.INST.Server.StartNetwork( list );
		}
	}
}
