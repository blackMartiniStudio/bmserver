﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMAsyncDataBase;

namespace MasterServer
{
	public class SqlDBFactory : ISqlDBFactory
	{
		public DBKind DBKind { get; private set; }

		public SqlDBFactory( DBKind _kind )
		{
			DBKind = _kind;
		}

		public ISQLGenerator CreateSqlGenerator( DBConnectionInfo info )
		{
			switch( DBKind )
			{
				case DBKind.MySQL:
					return new MySQLGenerator( info );
				case DBKind.MsSQL:
					return new MsSQLGenerator( info );
			}

			return null;
		}

		public SqlDBHandler CreateSqlHandler( DBType type, ISQLGenerator generator, DBThreadScheduler scheduler )
		{
			switch( type )
			{
				case DBType.master:
					return new SqlMasterHandler( generator, scheduler );
			}

			return null;
		}
	}
}
