﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMAsyncDataBase;
using BMDK;
using BMNet;

namespace MasterServer
{
	public class SqlMasterHandler : SqlDBHandler
	{
		private DBThreadScheduler Scheduler;
		public SqlMasterHandler( ISQLGenerator generator, DBThreadScheduler _scheduler ) : base( generator )
		{
			Scheduler = _scheduler;
		}

		public DBResultCode LoadServerConnectionInfo()
		{
			LoadServerConnectionInfo dbCmd = new LoadServerConnectionInfo( this );
			DBExecute execute = Scheduler.GetExecuter();
			execute.AttachCommand( dbCmd );

			Scheduler.EnqueJob( execute );

			return DBResultCode.SUCCESS;
		}

		internal DBResultCode LoadServerConnectionInfo( out List<ServerConnectionInfo> liServerInfo )
		{
			liServerInfo = new List<ServerConnectionInfo>();
			DBResultCode result = DBResultCode.SUCCESS;

			if( false == IsOpen() )
			{
				return DBResultCode.DB_ERROR;
			}

			using( DbConnection dbConn = CreateConnection() )
			{
				DbCommand cmd = CreateCommand( "sp_GetServerConnectionInfo", dbConn );

				DbDataReader reader = null;
				DataSet dataSet = new DataSet();
				try
				{
					dbConn.Open();
					reader = cmd.ExecuteReader();
					if( reader.HasRows )
					{
						while( reader.Read() )
						{
							ServerConnectionInfo info = new ServerConnectionInfo();
							info.ServerName = (string)reader["ServerName"];
							info.Ip = (string)reader["PublicIP"];
							info.Port = (Int32)reader["ListenPort"];

							liServerInfo.Add( info );
						}
					}
				}
				catch( Exception ex )
				{
					result = DBResultCode.DB_ERROR;
					BMLogger.ExceptionLog( ex );
				}
				finally
				{
					if( reader != null )
						reader.Close();
				}
			}

			return result;

		}
	}
}
