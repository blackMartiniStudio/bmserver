﻿namespace MatchingServer
{
	public class MSMgr : MGlobalManager
	{
		#region [Simple Singleton]

		private static MSMgr _inst = new MSMgr();

		public static MSMgr INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
		}

		public virtual void Destroy()
		{
		}
	}
}