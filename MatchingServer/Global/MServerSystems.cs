﻿namespace MatchingServer
{
	public class MSSys : MGlobalSystem
	{
		#region [Simple Singleton]

		private static MSSys _inst = new MSSys();

		public static MSSys INST
		{
			get { return _inst; }
		}

		#endregion [Simple Singleton]

		public virtual void Create()
		{
			kMatchingServer = new MatchingServer();
		}

		public virtual void Destroy()
		{
		}
	}
}