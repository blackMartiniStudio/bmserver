﻿using BMNet;
using CSCommonLib;
using System;

namespace MatchingServer
{
	public class GCommandHandlerCommon : BMCommandHandler
	{
		private static Int32 nRecvCount = 0;

		public GCommandHandlerCommon( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)CSPacketCommandEnum.CMD_PacketC2STest, OnTetstPacket );
		}

		private BMCommandResult OnTetstPacket( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketC2STest kCmd = _kCommand.Packet as PacketC2STest;

			MNetServer kServer = MCommandGroup.NetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			BMCommand kNewCommand = kServer.NewCommand( (Int32)CSPacketCommandEnum.CMD_PacketS2CTest, _kCommand.SendererUID );
			PacketS2CTest kSendCmd = new PacketS2CTest( (int)BMCommandResult.SUCCESS, ++nRecvCount, kCmd.nClientSendCount );

			Console.WriteLine( " CommandID = {0}, nClientSendCount = {1}, ServerRecvCount = {2}", _kCommand.ID, kCmd.nClientSendCount, nRecvCount );

			kNewCommand.SetPacket( kSendCmd );

			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}
	}
}