﻿using BMNet;

namespace MatchingServer
{
	public class MCommandGroup : ICommandGroup
	{
		public MCommandGroup( BMCommandCommunicator _kCommunicator )
		{
			GCommandHandlerCommon kCommon = new GCommandHandlerCommon( _kCommunicator );
		}

		static public MNetServer NetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as MNetServer;
		}
	}
}