﻿using BMDK;
using BMServerCommonLib;

namespace MatchingServer
{
	public class Stratup
	{
		private static void Main( string[] args )
		{
			MatchingServerApplication m_kApp = new MatchingServerApplication();

			string ProcessName = ProcessHelper.GetProcessName();
			SLogger logger = new SLogger( ProcessName );
			BMLogger.Initialize( logger );

			if( false == m_kApp.Create( ProcessName ) || false == m_kApp.Start() )
			{
				m_kApp.Destroy();
			}
		}
	}
}