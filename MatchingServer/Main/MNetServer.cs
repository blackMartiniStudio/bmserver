﻿using BMNet;
using CSCommonLib;

namespace MatchingServer
{
	public class MNetServer : BMNetServer
	{
		private ICommandGroup kCommandGroup;

		public MNetServer( BMNetServerDesc _kDesc ) : base( _kDesc )
		{
			kCommandGroup = new MCommandGroup( this );
		}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new CSCommandBuilder();
		}
	}
}