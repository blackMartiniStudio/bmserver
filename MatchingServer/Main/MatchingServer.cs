﻿using BMDK;
using BMNet;
using BMServerCommonLib;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace MatchingServer
{
	// 실제 서버 메인 클래스
	public class MatchingServer : GBaseServer
	{
		private MNetServer m_kNetServer;

		public MatchingServer()
		{
		}

		protected override bool OnCreate()
		{
			if( false == CreateNetwork() )
				return false;

			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			try
			{
				// 네트워크 업데이트 ( recv 처리 )
				m_kNetServer.Update();

				// DB 처리

				// 로직 업데이트 처리
				// frameRate 처리
			}
			catch( Exception /*ex*/ )
			{
			}

			return true;
		}

		protected override void OnDestroy()
		{
		}

		public bool CreateNetwork()
		{
			BMNetServerDesc kDesc = new BMNetServerDesc();
			m_kNetServer = new MNetServer( kDesc );

			List<IPAddress> liIps = NetworkHelper.GetIPv4AddressList();
			return m_kNetServer.Start( AddressFamily.InterNetwork, 10000, false, 3000, 100000, "" );
		}
	}
}