﻿using System;
using BMServerCommonLib;

namespace MatchingServer
{
	public class MatchingServerApplication : ConsoleApplication
	{
		protected override bool OnCreate()
		{
			// 전역 클래스 Instance
			MSSys.INST.Create();
			MSMgr.INST.Create();

			// 리소스 초기화
			if( false == Initialize() )
				return false;

			// 게임서버 초기화
			if( false == MSSys.INST.kMatchingServer.Create( ProcessName ) )
				return false;

			// DB 초기화

			return true;
		}

		protected override bool OnUpdate( Int64 _elapsed )
		{
			UpdateConsoleKeyInput();
			return MSSys.INST.kMatchingServer.Update( _elapsed );
		}

		protected override void OnDestroy()
		{
			MSSys.INST.kMatchingServer.Destroy();
		}

		private bool Initialize()
		{
			// 로거 초기화

			// 리소스 초기화

			// Net 관련 초기화

			return true;
		}

		private void UpdateConsoleKeyInput()
		{
			// 			ConsoleKeyInfo key = Console.ReadKey();
			// 			String strKeyChar = key.KeyChar.ToString();
			// 			if( 0 == String.Compare( strKeyChar, "Y", true ) )
			// 			{
			// 				int a = 0;
			// 			}
		}
	}
}