﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMAsyncDataBase
{
	public interface IDBExecute
	{
		void Execute();
		void Bind();
		void AttachCommand( BMDBCommand dbCom );
	}
}
