﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;

namespace BMAsyncDataBase
{
	public class ReadQueryExecute : DBExecute
	{
		public override void Bind()
		{

		}

		public override void Execute()
		{
			DBCommand.ResultCode = DBResultCode.NONE;

			DbCommand cmd = BeginExecute();
			if( cmd == null )
				throw new InvalidOperationException();

			using( DbConnection conn = DBCommand.DBHandler.CreateConnection() )
			{
				DbDataReader reader = null;
				try
				{
					cmd.Connection = conn;
					cmd.Connection.Open();

					reader = cmd.ExecuteReader() as DbDataReader;
					if( reader == null )
						throw new InvalidOperationException();

					DBCommand.ResultCode = DBResultCode.SUCCESS;
					OnRead( reader );
				}
				catch( System.IndexOutOfRangeException ior )
				{
					BMLogger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					BMLogger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					BMLogger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					if( cmd.Parameters.Contains( BMDBCommand.ParamResultName ) )
					{
						DBCommand.ResultCode = (DBResultCode)cmd.Parameters[BMDBCommand.ParamResultName].Value;
					}
					
					if( DBCommand.ResultCode != DBResultCode.SUCCESS )
						throw new InvalidOperationException( "Fail!!" );
				}
			}
		}

		private DbCommand BeginExecute()
		{
			// 열려 있는지 확인
			if( DBCommand.DBHandler.IsOpen() == false )
			{
				DBCommand.ResultCode = DBResultCode.DB_ERROR;
				return null;
			}

			Bind();
			// 커맨드 생성
			return DBCommand.DBHandler.CreateCommand( DBCommand.SpName, DBCommand.ParamList );
		}

		protected virtual void OnRead( DbDataReader reader )
		{
			DBCommand.OnRead( reader );
		}
	}
}
