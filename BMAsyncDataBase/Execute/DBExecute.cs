﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMAsyncDataBase
{
	public class DBExecute : IDBExecute
	{
		public BMDBCommand DBCommand { get; protected set; }

		public DBExecute()
		{
		}

		public virtual void Bind()
		{
		}

		public virtual void Execute()
		{
		}

		public void AttachCommand( BMDBCommand  dbCom )
		{
			DBCommand = dbCom;
		}
	}
}
