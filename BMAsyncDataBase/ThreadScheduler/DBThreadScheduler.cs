﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BMAsyncDataBase
{
	public class DBThreadScheduler : IDisposable
	{
		private readonly int MaxJob = 100000;
		private Object m_lockObj = new Object();
		private ConcurrentQueue<DBExecute>			m_actExcute;
		private BlockingCollection<DBExecute>		m_JobPool;
		private ConcurrentQueue<DBExecute>			m_ResultPool;
		private BlockingCollection<DBThreadWorker>	m_workerPool;
		private Thread								m_Thread;

		private bool m_disposed = false;

		public DBThreadScheduler( int nWorkSize )
		{
			m_actExcute = new ConcurrentQueue<DBExecute>();
			m_JobPool = new BlockingCollection<DBExecute>( MaxJob );
			m_ResultPool = new ConcurrentQueue<DBExecute>();
			m_workerPool = new BlockingCollection<DBThreadWorker>( nWorkSize );

			for( Int32 i = 0; i < nWorkSize; ++i )
			{
				var woker = new DBThreadWorker( this );
				while( m_workerPool.TryAdd( woker, System.Threading.Timeout.Infinite ) == false )
				{
					Thread.SpinWait( 10 );
				}

				woker.Start();
			}

			m_Thread = new Thread( this.OnWork );
		}

		public void Start()
		{
			if( m_Thread != null )
				m_Thread.Start();
		}

		internal void Stop()
		{
			Dispose();
			m_JobPool.CompleteAdding();
			m_workerPool.CompleteAdding();

		}

		private void OnWork()
		{
			while( m_JobPool.IsCompleted == false )
			{
				DBExecute req = null;
				if( m_JobPool.TryTake( out req, System.Threading.Timeout.Infinite ) == false )
					continue;

				DBThreadWorker worker = null;
				m_workerPool.TryTake( out worker, System.Threading.Timeout.Infinite );

				worker.Enque( req );
			}


			// worker 정리
			while( m_workerPool.IsCompleted == false )
			{
				DBThreadWorker worker = null;
				m_workerPool.TryTake( out worker, System.Threading.Timeout.Infinite );

				worker = null;
			}

			m_JobPool = null;
			m_workerPool = null;
		}

		public void EnqueJob( DBExecute execute )
		{
			bool success = false;
			while( true )
			{
				try
				{
					success = m_JobPool.TryAdd( execute, 1 );
				}
				catch( OperationCanceledException )
				{
					m_JobPool.CompleteAdding();
				}

				if( success == true )
				{
					break;
				}
				else
				{
					Thread.SpinWait( 1 );
				}
			}
		}

		public void Dispose()
		{
			bool waitForThreads = false;
			lock( m_lockObj )
			{
				if( m_disposed == false )
				{
					GC.SuppressFinalize( this );

					m_disposed = true;
					waitForThreads = true;
				}
			}

			if( waitForThreads )
			{
				foreach( var worker in this.m_workerPool )
				{
					worker.Stop();
					Thread.Sleep( 10 );
				}
			}
		}

		public DBThreadWorker GetWoker()
		{
			while( true )
			{
				DBThreadWorker worker = null;
				if( m_workerPool.TryTake( out worker, System.Threading.Timeout.Infinite ) == true )
				{
					return worker;
				}
				else
				{
					Thread.SpinWait( 10 );
				}
			}
		}

		public DBExecute GetExecuter()
		{
			DBExecute execute = null;

			if( m_actExcute.Count != 0 )
			{
				if( m_actExcute.TryDequeue( out execute ) == true )
				{
					return execute;
				}
			}

			return new ReadQueryExecute();
		}

		internal void ReturnToWokerPool( DBThreadWorker threadWorker  )
		{
			m_workerPool.TryAdd( threadWorker );
		}

		public void ReturnToExecutePool( DBExecute execute )
		{
			m_actExcute.Enqueue( execute );
		}

		public bool TryDequeResult( out DBExecute execute )
		{
			return m_ResultPool.TryDequeue( out execute );
		}

		public void EnqueResult( DBExecute execute )
		{
			m_ResultPool.Enqueue( execute );
		}
	}
}
