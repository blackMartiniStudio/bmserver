﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BMDK;

namespace BMAsyncDataBase
{
	public class DBThreadWorker
	{
		private Thread              m_thread;
		private DBThreadScheduler     m_scheduler;
		private BlockingCollection<DBExecute> m_execute;

		public DBThreadWorker( DBThreadScheduler _scheduler )
		{
			m_scheduler = _scheduler;
			m_execute = new BlockingCollection<DBExecute>();
			m_thread = new Thread( OnWork );
		}

		public Thread Start()
		{
			m_thread.Start();

			return m_thread;
		}

		public void OnWork()
		{
			while( m_execute.IsCompleted == false )
			{
				DBExecute exe;
				try
				{
					if( m_execute.TryTake( out exe, System.Threading.Timeout.Infinite ) == false )
					{
						if( m_execute.IsCompleted ) // thread terminate
							return;
					}

					exe.Execute();
					m_scheduler.EnqueResult( exe );
					m_scheduler.ReturnToWokerPool( this );
				}
				catch( Exception ex )
				{
					BMLogger.ExceptionLog( ex );
					return;
				}
			}

		}

		public void Enque( DBExecute act )
		{
			m_execute.TryAdd( act );
		}

		internal void Stop()
		{
			m_execute.CompleteAdding();
			m_scheduler = null;
			m_thread = null;
		}
	}
}
