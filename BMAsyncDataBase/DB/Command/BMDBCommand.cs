﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMAsyncDataBase
{
	public abstract class BMDBCommand : IBMDBCommand
	{
		public static readonly string ParamResultName = "out_Result";

		public SqlDBHandler DBHandler { get; protected set; }
		public List<DbParameter> ParamList { get; protected set; }
		public String SpName { get; protected set; }

		public DbDataReader DataReader { get; private set; }

		public DBResultCode ResultCode { get; set; }

		public BMDBCommand( SqlDBHandler _dbHandler )
		{
			DBHandler = _dbHandler;
			ParamList = new List<DbParameter>();
		}

		public abstract void OnRead( DbDataReader reader );

		public abstract void OnFinish();

		public virtual bool IsSuccess()
		{
			if( ResultCode != DBResultCode.SUCCESS )
				return false;

			return true;
		}
				
	}
}
