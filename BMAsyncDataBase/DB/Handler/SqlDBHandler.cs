﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;

namespace BMAsyncDataBase
{
	public abstract class SqlDBHandler
	{
		protected ISQLGenerator m_generator = null;

		public SqlDBHandler( ISQLGenerator generator )
		{
			Init( generator );
		}

		public bool Init( ISQLGenerator generator )
		{
			try
			{
				if( m_generator != null )
					throw new InvalidOperationException();

				if( generator.MainConnection.State != ConnectionState.Open )
					throw new InvalidOperationException();

				m_generator = generator;

			}
			catch( Exception ex )
			{
				BMLogger.ExceptionLog( ex );
				return false;
			}

			BMLogger.DebugLog( "Sql Manager 시작 - generator to {0}", m_generator.ToString() );
			return true;
		}

		public void CleanUp()
		{
			if( m_generator != null )
				m_generator.CleanUp();

			BMLogger.DebugLog( "Sql Manager 종료 - generator" );
		}

		public bool IsOpen()
		{
			if( m_generator.MainConnection == null )
				throw new InvalidOperationException( "Main Connection is Not Create" );

			try
			{
				if( m_generator.MainConnection.State == ConnectionState.Broken || m_generator.MainConnection.State == ConnectionState.Closed )
				{
					m_generator.MainConnection.Close();
					m_generator.MainConnection.Open();
				}
			}
			catch( System.Exception ex )
			{
				BMLogger.ExceptionLog( ex );
				return false;
			}

			return true;
		}

		public void Open()
		{
			try
			{
				m_generator.MainConnection.Close();
				m_generator.MainConnection.Open();
			}
			catch( System.Exception ex )
			{
				BMLogger.ExceptionLog( ex );
			}
		}

		public DbCommand CreateCommand( string spName, List<DbParameter> paramList )
		{
			DbCommand cmd = m_generator.CreateCommand( spName, paramList );
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddRange( paramList.ToArray() );
			return cmd;
		}

		public DbCommand CreateCommand( string spName, DbConnection dbConn )
		{
			DbCommand cmd = m_generator.CreateCommand( spName, dbConn );
			cmd.CommandType = CommandType.StoredProcedure;
			return cmd;
		}

		public DbCommand CreateCommand( string spName )
		{
			DbCommand cmd = m_generator.CreateCommand( spName );
			cmd.CommandType = CommandType.StoredProcedure;
			return cmd;
		}

		public DbConnection CreateConnection()
		{
			return m_generator.CreateConnection();
		}

		public DbDataAdapter CreateDataAdapter()
		{
			return m_generator.CreateDataAdapter();
		}

		#region [InParam]
		public DbParameter InParam( List<DbParameter> paramList, String paramName, bool value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}
		public DbParameter InParam( List<DbParameter> paramList, String paramName, Byte value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Int16 value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Int32 value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Int64 value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, DateTime value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, String value, ParameterDirection direction )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Byte[] value, ParameterDirection direction = ParameterDirection.Input )
		{
			return m_generator.InParam( paramList, paramName, value, direction );
		}
		#endregion
	}
}
