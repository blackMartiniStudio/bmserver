﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMAsyncDataBase
{
	public interface ISqlDBFactory
	{
		ISQLGenerator CreateSqlGenerator( DBConnectionInfo info );
		SqlDBHandler CreateSqlHandler( DBType type, ISQLGenerator generator, DBThreadScheduler scheduler );
	}
}
