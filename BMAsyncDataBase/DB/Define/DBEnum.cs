﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMAsyncDataBase
{
	public enum DBKind
	{
		MySQL = 1,
		MsSQL = 2,
	}

	public enum DBType
	{
		master = 1,
	}

	public enum DBResultCode
	{
		NONE = 0,
		SUCCESS = 1,

		ERROR_INDEX = 100000,
		DB_ERROR = ERROR_INDEX,
	}
}
