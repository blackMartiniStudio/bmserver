﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BMAsyncDataBase
{
	public class DBConnectionInfo
	{
		public static string Key = "DBConnectionInfo";

		[JsonProperty("ID")]
		public Int32	ID;
		[JsonProperty("IP")]
		public string	IP;
		[JsonProperty("Port")]
		public Int32    Port;
		[JsonProperty("User")]
		public string   User;
		[JsonProperty("Pw")]
		public string   Pw;
		[JsonProperty("DBName")]
		public string   DBName;
	}
}
