﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;

namespace BMAsyncDataBase
{
	public  class MsSQLGenerator : ISQLGenerator
	{
		private SqlConnectionStringBuilder m_dbConnStringBuilder;
		private SqlConnection m_mainConnection;

		public MsSQLGenerator( DBConnectionInfo info )
		{
			m_dbConnStringBuilder = new SqlConnectionStringBuilder();
			m_dbConnStringBuilder["Server"] = string.Format( "{0},{1}", info.IP, info.Port );
			m_dbConnStringBuilder["Database"] = info.DBName;
			m_dbConnStringBuilder["uid"] = info.User;
			m_dbConnStringBuilder["pwd"] = info.Pw;
			m_dbConnStringBuilder.Enlist = false;

		}
		public string ConnectionString
		{
			get { return m_dbConnStringBuilder.ConnectionString; }
		}

		public DbConnection MainConnection
		{
			get { return m_mainConnection; }
		}

		public override String ToString()
		{
			return base.ToString();
		}

		public DbConnection CreateConnection()
		{
			return new SqlConnection( m_dbConnStringBuilder.ConnectionString );
		}

		public DbDataAdapter CreateDataAdapter()
		{
			return new SqlDataAdapter();
		}

		public DbCommand CreateCommand( string spName, DbConnection dbConn )
		{
			return new SqlCommand( spName, (SqlConnection)dbConn );
		}

		public DbCommand CreateCommand( string spName )
		{
			return new SqlCommand( spName );
		}

		public DbCommand CreateCommand( string spName, List<DbParameter> paramList )
		{
			SqlCommand cmd = new SqlCommand( spName );
			cmd.Parameters.AddRange( paramList.ToArray() );
			return cmd;
		}


		public void Init()
		{
			try
			{
				m_mainConnection = new SqlConnection( m_dbConnStringBuilder.ConnectionString );
				m_mainConnection.Open();
			}
			catch( System.Exception ex )
			{
				BMLogger.ErrLog( "DB Open Error... connectionString:{0}", m_dbConnStringBuilder.ConnectionString );
				BMLogger.ExceptionLog( ex );
			}
		}

		public void CleanUp()
		{
			if( m_mainConnection != null )
				m_mainConnection.Close();

			m_mainConnection = null;
		}



		public DbParameter InParam( List<DbParameter> paramList, String paramName, bool value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.Bit );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}
		public DbParameter InParam( List<DbParameter> paramList, String paramName, Byte value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.TinyInt );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Int16 value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.SmallInt );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Int32 value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.Int );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, Int64 value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.BigInt );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, DateTime value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.DateTime );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		public DbParameter InParam( List<DbParameter> paramList, String paramName, String value, ParameterDirection direction )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.VarChar, value.Length );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}


		public DbParameter InParam( List<DbParameter> paramList, String paramName, Byte[] value, ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.Binary, value.Length );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}
	}
}
