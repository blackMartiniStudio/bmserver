﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;

namespace BMAsyncDataBase
{
	public class SqlDBManager
	{
		public DBKind Kind { get; private set; }
		public DBThreadScheduler ThreadScheduler { get; private set; }

		public SqlDBManager( DBThreadScheduler scheduler )
		{
			ThreadScheduler = scheduler;
		}

		public Dictionary<DBType, SqlDBHandler> DicHandler = new Dictionary<DBType, SqlDBHandler>();

		public bool Connect( List<DBConnectionInfo> dbConnectionInfo, ISqlDBFactory factory )
		{
			try
			{
				foreach( DBConnectionInfo info in dbConnectionInfo )
				{
					ISQLGenerator generator = factory.CreateSqlGenerator( info );
					if( generator == null  )
					{
						BMLogger.ErrLog( "SqlDBManager CreateSqlGenerator Fail" );
						return false;
					}

					generator.Init();

					DBType dbType;
					if( false == Enum.TryParse<DBType>( info.DBName, out dbType ) )
						return false;

					SqlDBHandler handler = factory.CreateSqlHandler( dbType, generator, ThreadScheduler );
					DicHandler.Add( dbType, handler );
				}
			}
			catch( Exception ex )
			{
				BMLogger.ErrLog( "SqlDBManager Connect Fail" );
				BMLogger.ExceptionLog( ex );

				return false;
			}

			return true;
		}


		public bool Start()
		{
			if( ThreadScheduler == null )
				return false;

			ThreadScheduler.Start();
			return true;
		}

		public void Stop()
		{
			if( ThreadScheduler != null )
				ThreadScheduler.Stop();

			ThreadScheduler = null;
		}

		private void Add( DBType type, SqlDBHandler handler )
		{
			DicHandler.Add( type, handler );
		}

		public bool TryGetHandler<T>( DBType type, out T dbhandler ) where T : SqlDBHandler
		{
			dbhandler = null;
			SqlDBHandler handler;
			if( false == DicHandler.TryGetValue( type, out handler ) )
				return false;

			dbhandler = handler as T;
			if( dbhandler == null )
				return false;

			return true;
		}

		public void Update()
		{
			DBExecute execute;
			while( ThreadScheduler.TryDequeResult( out execute ) )
			{
				execute.DBCommand.OnFinish();
				ThreadScheduler.ReturnToExecutePool( execute );
			}
		}
	}
}
