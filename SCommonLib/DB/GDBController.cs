﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCommonLib
{
    public class GDBController
    {
        private Dictionary<SQLDB, SqlHandler> m_kDicSqlHandler = new Dictionary<SQLDB, SqlHandler>();

        public Dictionary<SQLDB, SqlHandler> DicSqlHandler
        {
            get { return m_kDicSqlHandler; }
        }


        public SQLAccountHandler AccountDB
        {
            get { return (SQLAccountHandler)m_kDicSqlHandler[SQLDB.Account]; }
        }

        public SQLGameHandler GameDB
        {
            get { return (SQLGameHandler)m_kDicSqlHandler[SQLDB.Game]; }
        }

        public SQLSessionHandler SessionDB
        {
            get { return (SQLSessionHandler)m_kDicSqlHandler[SQLDB.Session]; }
        }

        public SQLSocialHandler SocialDB
        {
            get { return (SQLSocialHandler)m_kDicSqlHandler[SQLDB.Social]; }
        }

        public SQLToolHandler ToolDB
        {
            get { return (SQLToolHandler)m_kDicSqlHandler[SQLDB.Tool]; }
        }

        public SQLDataHandler DataDB
        {
            get { return (SQLDataHandler)m_kDicSqlHandler[SQLDB.Data]; }
        }

        public void Add(SQLDB _eDbType, SqlHandler _kSqlHandler)
        {
            m_kDicSqlHandler.Add(_eDbType, _kSqlHandler);
        }

        public bool Find(SQLDB _eDbType, out SqlHandler _kSqlHandler)
        {
            return m_kDicSqlHandler.TryGetValue(_eDbType, out _kSqlHandler);
        }

        static public void SendToResult(UInt32 clientID, Int64 userUID , S2SPacketEnum pktCmd, Object pktData)
        {
            KMDBCommand cmd = KMDBCommand.GetInitCommand(clientID, userUID, pktCmd, pktData);
            DBRequest.SendToLogic(cmd);
        }

        public virtual void QueryCall(KMDBCommand cmd)
        {

        }

        public bool CommonQueryCall(KMDBCommand cmd)
        {
            switch(cmd.PacketCmd)
            {
                case S2SPacketEnum.CMD_PacketDBRqLoadConfigData:
                    OnRecvPacketDBRqLoadConfigData(cmd.Data);
                    break;
            }

            return false;
        }

        private void OnRecvPacketDBRqLoadConfigData(Object pkt)
        {
            PacketMMRsReadConfigData kRsPacket = new PacketMMRsReadConfigData(false);
            if(true == ConfigDataDisposer.INSTANCE.ReloadRealTimeConfigData())
                kRsPacket.IsSuccess = true;

            ManagementManager.INSTANCE.SendPacketToManagementServer(S2SPacketEnum.CMD_PacketMMRsReadConfigData, kRsPacket);
        }
    }
}
