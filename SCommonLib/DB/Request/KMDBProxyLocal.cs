﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SCommonLib.DB;
using System.Threading;

namespace SCommonLib
{
    public delegate DBRequest DelegateDBRequest();
    abstract public class KMDBProxyLocal : IDatabaseProxy
    {
        public static Int64 m_elapsedTime = 0;
        public static Int64 m_totalCount = 0;

        // taskManager
        private KMDBManager         m_dbManager = null;
        private GDBController       m_kDBController;
        private DBConnectionInfo[]  m_kDBConnectionInfoArray;

        public GDBController kDBController
        {
            get { return m_kDBController; }
            set { m_kDBController = value; }
        }

        public KMDBProxyLocal(DBConnectionInfo[] _kDBConnectionInfoArray)
        {
            m_kDBConnectionInfoArray = _kDBConnectionInfoArray;
        }

        // build setting
		public virtual bool Init( Int32 WorkerPoolSize, Int32 WorkerPerThroughpu )
        {
            try
            {
                foreach(var dbInfo in m_kDBConnectionInfoArray)
                {
                    ISqlGenerator generator = new MsSqlGenerator(dbInfo.ip,
                                                                    dbInfo.port,
                                                                    dbInfo.dbName,
                                                                    dbInfo.user,
                                                                    dbInfo.pw);

                    generator.Init();
                    SqlManager dbManager = new SqlManager(generator);

                    SqlHandler kSqlHandler = null;
                    if(m_kDBController.Find((SQLDB)dbInfo.type, out kSqlHandler) == false)
                    {
                        if(false == MakeSqlHandler((SQLDB)dbInfo.type, out kSqlHandler))
                        {
                            return false;
                        }

                        kDBController.Add((SQLDB)dbInfo.type, kSqlHandler);
                    }

                    kSqlHandler.Add(dbInfo.id, dbManager);
                }
            }
            catch(Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

			m_dbManager = new KMDBManager( this.CreateRequest, WorkerPoolSize, WorkerPerThroughpu / WorkerPoolSize );
            m_dbManager.Activate();
            return true;
        }

        public virtual void CleanUp()
        {
            if (m_dbManager != null)
                m_dbManager.Deactivate();

            m_dbManager = null;

        }

        public virtual bool SendDBRequest(KMDBCommand cmd)
        {
            if (cmd == null)
                return false;

            DBRequest req = m_dbManager.GetDBRequest();
            if (req == null)
                return false;

			Interlocked.Increment( ref KMDBCommand.SendQuery );

            req.Attach(cmd);

            m_dbManager.Enque(req);
            return true;
        }


        public virtual DBRequest CreateRequest()
        {
            return new DBRequest(m_kDBController);
        }

        abstract public void OnDBResult(Int32 cmd, Object pkt);

        public bool MakeSqlHandler(SQLDB _eSqlDBType, out SqlHandler _kSqlHander)
        {
            _kSqlHander = null;

            switch(_eSqlDBType)
            {
                case SQLDB.Account:
                    _kSqlHander = new SQLAccountHandler();
                    break;
                case SQLDB.Data:
                    _kSqlHander = new SQLDataHandler();
                    break;
                case SQLDB.Game:
                    _kSqlHander =  new SQLGameHandler();
                    break;
                case SQLDB.Session:
                    _kSqlHander =  new SQLSessionHandler();
                    break;
                case SQLDB.Social:
                    _kSqlHander =  new SQLSocialHandler();
                    break;
                case SQLDB.Tool:
                    _kSqlHander =  new SQLToolHandler();
                    break;
                default:
                    return false;
            }

            return true;
        }
    }
}
