﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCommonLib
{
    public class KMDBProxyController : IDatabaseProxy
    {
        private IDatabaseProxy m_databaseProxy;
        public KMDBProxyController(IDatabaseProxy proxy)
        {
            m_databaseProxy = proxy;
        }


		public virtual bool Init( Int32 WorkerPoolSize, Int32 WorkerPerThroughput )
        {
			return m_databaseProxy.Init( WorkerPoolSize, WorkerPerThroughput );
        }

        public virtual void CleanUp()
        {
            if (m_databaseProxy != null)
                m_databaseProxy.CleanUp();

            m_databaseProxy = null;
        }

        public virtual bool SendDBRequest(KMDBCommand cmd)
        {
            return m_databaseProxy.SendDBRequest(cmd);
        }

        public DBRequest CreateRequest()
        {
            return m_databaseProxy.CreateRequest();
        }

        public void OnDBResult(Int32 cmd, Object pkt)
        {
            m_databaseProxy.OnDBResult(cmd, pkt);
        }

        
    }
}
