﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCommonLib
{
    public interface IDatabaseProxy
    {
		bool Init( Int32 WorkerPoolSize, Int32 WorkerPerThroughpu );
        void CleanUp();
        bool SendDBRequest(KMDBCommand cmd);
        DBRequest CreateRequest();

        void OnDBResult(Int32 cmd, Object pkt);

        
    }
}
