﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SCommonLib
{
    public class DBRequest : ActExecuteRequest
    {
        private GDBController m_kDBController;

        public DBRequest(GDBController _kDBController)
        {
            m_kDBController = _kDBController;
        }

        private KMDBCommand m_dbCmd = null;
        public bool Attach(KMDBCommand cmd)
        {
            m_dbCmd = cmd;
            return true;
        }

        public override Int32 OnExecute()
        {
            Int32 code = (Int32)m_dbCmd.PacketCmd;
            QueryCall(m_dbCmd);

            Interlocked.Increment(ref KMDBCommand.QueryTotalCount);

            KMLogic.INSTANCE.GetLogicEntry.RecallDBCmd(m_dbCmd);
            m_dbCmd = null;
            return code;

        }

        protected virtual void QueryCall(KMDBCommand cmd)
        {
			Int64 beginTick = DateTime.Now.Ticks;

			Interlocked.Increment( ref KMDBCommand.QueryCallCnt );
			Interlocked.Increment( ref KMDBCommand.QueryPending );
			m_kDBController.QueryCall( cmd );
			Interlocked.Decrement( ref KMDBCommand.QueryPending );

			if( LuaScriptManager.INSTANCE.m_DBQueryProfile )
			{
				DBQueryProfile.INSTANCE.AddTick( cmd, DateTime.Now.Ticks - beginTick );
			}
        }

        static public void SendToLogic(KMDBCommand cmd)
        {
            cmd.TypeID = KMCommand.Type.DB;
            KMLogicGateway.INSTANCE.AddCallback( cmd );
        }
    }
}
