﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCommonLib
{
    public class KMCommand
    {
        public enum Type
        {
            None = 0,
            Connect = 1,
            Data = 2,
            Timer = 3,
            Shouter = 4,
            Disconnect = 5,
            DB = 6,
            Proxy = 7,
        }


        static public Int64 LogicElapsedTime = 0;
        static public Int64 LogicCount = 0;
        public Int64 logicEnter = 0;

        private Type m_type = Type.Connect;




        private UInt32 m_clientID;
        public UInt32 ClientID
        {
            get { return m_clientID; }
            set { m_clientID = value; }
        }

        public Type TypeID
        {
            get { return m_type; }
            set { m_type = value; }
        }



        public virtual void Reset()
        {
            m_clientID = 0;
            m_type = Type.None;
        }
    }

    public class KMDBCommand : KMCommand
    {
        static public Int64 QueryTotalCount = 0;
        static public Int64 QueryPending = 0;
		static public Int64 QueryCallCnt = 0;
		static public Int64 SendQuery = 0;


        static public Int64 GetAppFriendInfoElapsed = 0;
        static public Int64 GetAppFriendInfoCount = 0;
        static public Int64 GetAppFreindSocialInfoElapsed = 0;
        static public Int64 GetAppFreindSocialInfoCount = 0;
        static public Int64 GameServerAuth= 0;
        static public Int64 GameServerAuthCount = 0;
        static public Int64 LoadInventory = 0;
        static public Int64 LoadInventoryCount = 0;
        static public Int64 GetSocialUserInfoElapsed = 0;
        static public Int64 GetSocialUserInfoCount = 0;
        


        private Int64 m_userUID;
        public Int64 UserUID
        {
            get { return m_userUID; }
        }

        public override void Reset()
        {
            m_userUID = 0;
            m_packetCmd = S2SPacketEnum.CMD_NONE;
            Data = null;

            base.Reset();
        }

        private S2SPacketEnum m_packetCmd;
        public S2SPacketEnum PacketCmd
        {
            get { return m_packetCmd; }
        }

        public Object Data { get; set; }
        public void Init(S2SPacketEnum pktCmd)
        {
            Reset();
            m_packetCmd = pktCmd;

        }



        static public KMDBCommand GetInitCommand(UInt32 clientID, Int64 userUID, S2SPacketEnum pktCmd, Object pktData)
        {
            KMDBCommand cmd = KMLogic.INSTANCE.GetLogicEntry.GetDBCommand();
            cmd.Init(pktCmd);


            cmd.ClientID = clientID;
            cmd.m_userUID = userUID;
            cmd.TypeID = KMCommand.Type.DB;
            cmd.Data = pktData;

            return cmd;
        }


    }


}
