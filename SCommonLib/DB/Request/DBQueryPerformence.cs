﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCommonLib
{
	public class DBQueryProfile : TSingleton<DBQueryProfile>
	{
		public Object kLock = new Object();
		public Dictionary<S2SPacketEnum, List<Int64>>	m_dicPerformence	= new Dictionary<S2SPacketEnum, List<Int64>>();
		public SortedList<S2SPacketEnum, Int64>			m_sliPrintPerf		= new SortedList<S2SPacketEnum, Int64>();

		public void AddTick( KMDBCommand cmd, Int64 _ReqTick )
		{
			lock( kLock )
			{
				List<Int64> liRequestTick;
				if( false == m_dicPerformence.TryGetValue( cmd.PacketCmd, out liRequestTick ) )
				{
					liRequestTick = new List<Int64>();
					m_dicPerformence.Add( cmd.PacketCmd, liRequestTick );
				}

				liRequestTick.Add( _ReqTick );
			}
		}

		public void Print()
		{
			Int32 nID = 0;
			Logger.InfoLog( "========== Query Performence Log ==============" );
			lock( kLock )
			{
				foreach( var kPerformence in m_dicPerformence.OrderByDescending( kItem => kItem.Value.Sum() / kItem.Value.Count ) )
				{
					nID++;
					Logger.InfoLog( "==> [{0}] : enum = {1}, AvgMS = {2}, SumMS = {3}, CallCnt = {4}",
						nID,
                        kPerformence.Key,
                        ( kPerformence.Value.Sum() / kPerformence.Value.Count ) / TimeSpan.TicksPerMillisecond,
                        kPerformence.Value.Sum() / TimeSpan.TicksPerMillisecond,
                        kPerformence.Value.Count );

                    if(nID > 5)
                        break;
				}
			}
			Logger.InfoLog( "===============================================" );

			Clear();
		}

		public void Clear()
		{
			lock(kLock)
			{
				m_dicPerformence.Clear();
			}
		}
	}
}
