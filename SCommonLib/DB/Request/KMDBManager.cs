﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;


namespace SCommonLib
{
    public class KMDBManager : IDBManager
    {
        private DelegateDBRequest   m_dbRequestMaker    = null;
        private KMDBScheduler       m_scheduler         = null;
        
        public KMDBManager(DelegateDBRequest delCreateDBRequest, Int32 maxPoolSize, Int32 workerThroughput)
        {
            m_dbRequestMaker = delCreateDBRequest;
            m_scheduler = new KMDBScheduler( delCreateDBRequest,  maxPoolSize, workerThroughput);
        }

        public DBRequest GetDBRequest()
        {
            return m_scheduler.GetDBRequest();
        }

        public void Activate()
        {
            if ( m_scheduler != null)
                m_scheduler.Activate();
        }

        public void Deactivate()
        {
            if (m_scheduler != null)
                m_scheduler.Deactivate();

            m_scheduler = null;
        }


        // thread pool 에 테스크를 던짐
        internal void Enque(DBRequest req)
        {
            if ( m_scheduler != null)
                m_scheduler.Enque(req);
        }

        
    }
}
