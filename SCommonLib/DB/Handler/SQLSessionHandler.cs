﻿using SCommonLib.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SCommonLib
{
	public class SQLSessionHandler : SqlHandler
	{
		public DbResultCode GetGameServerList( List<GameServerInfo> gameServerList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetLoginGameServerInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlDataReader result = null;
				try
				{
					// 실행직전 Open
					dbConn.Open();
					result = sqlCmd.ExecuteReader();

					if ( result.HasRows )
					{
						while ( result.Read() )
						{
							gameServerList.Add(
								new GameServerInfo(
									(UInt16)(Int16)result["ID"]
									, (String)result["ListenIP"]
									, (Int32)result["ListenPort"]
									, (Int32)result["Limit"]
									, (Int32)result["Weight"]
								) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();

					// 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GameServerAddress( Int32 _nServerGroupID, out String _strServerIP , out Int32 _nServerPort )
		{
			_strServerIP = String.Empty;
			_nServerPort = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetGameServerAddress" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramServerGroupID = sqlCmd.Parameters.Add( "@ServerGroupID", SqlDbType.Int );
                paramServerGroupID.Direction = ParameterDirection.Input;
                paramServerGroupID.Value = _nServerGroupID;

				SqlParameter paramNowTimeTick   = sqlCmd.Parameters.Add( "@NowTimeTick" , SqlDbType.BigInt );
				paramNowTimeTick.Direction = ParameterDirection.Input;
				paramNowTimeTick.Value = DateTime.Now.Ticks;

				SqlParameter paramMaxLiveTimeTick   = sqlCmd.Parameters.Add( "@MaxLiveTimeTick" , SqlDbType.BigInt );
				paramMaxLiveTimeTick.Direction = ParameterDirection.Input;
				paramMaxLiveTimeTick.Value = LuaScriptManager.INSTANCE.m_lLoadBallancingMaxLiveTime;

				SqlParameter paramGameServerIP  = sqlCmd.Parameters.Add( "@GameServerIP" , SqlDbType.NVarChar , 15 );
				paramGameServerIP.Direction = ParameterDirection.Output;
				paramGameServerIP.Value = String.Empty;

				SqlParameter paramGameServerPort = sqlCmd.Parameters.Add( "@GameServerPort" , SqlDbType.Int );
				paramGameServerPort.Direction = ParameterDirection.Output;
				paramGameServerPort.Value = 0;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
					{
						_strServerIP = (String)paramGameServerIP.Value;
						_nServerPort = (Int32)paramGameServerPort.Value;
					}
					else
					{
						Logger.ErrLog( " GameServerAddress Fail : Result = {0}, MaxLiveTimeTick = {1}" , result_code , LuaScriptManager.INSTANCE.m_lLoadBallancingMaxLiveTime );
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateWeight( UInt16 serverID , String listenIP , Int32 listenPort , Int16 targetValue )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateGameServerWeight" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramGameServerID = sqlCmd.Parameters.Add( "@serverID" , SqlDbType.SmallInt );
				paramGameServerID.Direction = ParameterDirection.Input;
				paramGameServerID.Value = (Int16)serverID;

				SqlParameter paramListenIP = sqlCmd.Parameters.Add( "@listenIP" , SqlDbType.VarChar , 15 );
				paramListenIP.Direction = ParameterDirection.Input;
				paramListenIP.Value = listenIP;

				SqlParameter paramListenPort = sqlCmd.Parameters.Add( "@listenPort" , SqlDbType.Int );
				paramListenPort.Direction = ParameterDirection.Input;
				paramListenPort.Value = listenPort;

				SqlParameter paramUpdateValue = sqlCmd.Parameters.Add( "@value" , SqlDbType.SmallInt );
				paramUpdateValue.Direction = ParameterDirection.Input;
				paramUpdateValue.Value = targetValue;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode AuthLoginGameServer( UInt16 serverID , String listenIP , Int32 listenPort )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GameServerVerify" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramGameServerID = sqlCmd.Parameters.Add( "@serverID" , SqlDbType.SmallInt );
				paramGameServerID.Direction = ParameterDirection.Input;
				paramGameServerID.Value = (Int16)serverID;

				SqlParameter paramListenIP = sqlCmd.Parameters.Add( "@listenIP" , SqlDbType.VarChar , 15 );
				paramListenIP.Direction = ParameterDirection.Input;
				paramListenIP.Value = listenIP;

				SqlParameter paramListenPort = sqlCmd.Parameters.Add( "@listenPort" , SqlDbType.Int );
				paramListenPort.Direction = ParameterDirection.Input;
				paramListenPort.Value = listenPort;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}


		public DbResultCode AllowAuthTicket( Int64 userUID , out Int64 authTicket , out UInt16 gameServerId )
		{
			authTicket = gameServerId = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_AllocAuthTicket" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramAuthTicket = sqlCmd.Parameters.Add( "@authTicket" , SqlDbType.BigInt );
				paramAuthTicket.Direction = ParameterDirection.Output;
				paramAuthTicket.Value = authTicket;

				SqlParameter paramGameServerID = sqlCmd.Parameters.Add( "@gameServerID" , SqlDbType.SmallInt );
				paramGameServerID.Direction = ParameterDirection.Output;
				paramGameServerID.Value = (Int16)gameServerId;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
					{
						authTicket = (Int64)paramAuthTicket.Value;
						gameServerId = (UInt16)(Int16)paramGameServerID.Value;
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateExpireSession( Int64 userUID , String platformID , UInt16 gameServerId )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateExpireSession" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramGameServerID = sqlCmd.Parameters.Add( "@serverID" , SqlDbType.SmallInt );
				paramGameServerID.Direction = ParameterDirection.Input;
				paramGameServerID.Value = (Int16)gameServerId;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode ConfirmAuth( Int64 authTicket , out Int64 userUID , out String platformID )
		{
			userUID = 0;
			platformID = string.Empty;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_ConfirmAuthTicket" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramAuthTicket = sqlCmd.Parameters.Add( "@authTicket" , SqlDbType.BigInt );
				paramAuthTicket.Direction = ParameterDirection.Input;
				paramAuthTicket.Value = authTicket;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Output;
				paramUserUID.Value = userUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Output;
				paramPlatformID.Value = platformID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
					{
						userUID = (Int64)paramUserUID.Value;
						platformID = (String)paramPlatformID.Value;
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode ConfirmMoveAuth( Int64 authTicket, out Int64 userUID, out String platformID )
		{
			userUID = 0;
			platformID = string.Empty;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_ConfirmMoveAuthTicket", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramAuthTicket = sqlCmd.Parameters.Add( "@authTicket", SqlDbType.BigInt );
				paramAuthTicket.Direction = ParameterDirection.Input;
				paramAuthTicket.Value = authTicket;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Output;
				paramUserUID.Value = userUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformID", SqlDbType.VarChar, 40 );
				paramPlatformID.Direction = ParameterDirection.Output;
				paramPlatformID.Value = platformID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if( result_code == DbResultCode.SUCCESS )
					{
						userUID = (Int64)paramUserUID.Value;
						platformID = (String)paramPlatformID.Value;
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateLoadBallancing( Int32 _nServerGroupID, Int32 _nServerID , String _strIP , Int32 _nPort , Int32 _nUserCount , Int32 _nMaxUserCount )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateLoadBallancing" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramServerGroupID = sqlCmd.Parameters.Add( "@ServerGroupID", SqlDbType.Int );
                paramServerGroupID.Direction = ParameterDirection.Input;
                paramServerGroupID.Value = _nServerGroupID;

				SqlParameter paramServerID = sqlCmd.Parameters.Add( "@ServerID" , SqlDbType.Int );
				paramServerID.Direction = ParameterDirection.Input;
				paramServerID.Value = _nServerID;

				SqlParameter paramServerIP = sqlCmd.Parameters.Add( "@ServerIP" , SqlDbType.NVarChar , 15 );
				paramServerIP.Direction = ParameterDirection.Input;
				paramServerIP.Value = _strIP;

				SqlParameter paramServerPort = sqlCmd.Parameters.Add( "@ServerPort" , SqlDbType.Int );
				paramServerPort.Direction = ParameterDirection.Input;
				paramServerPort.Value = _nPort;

				SqlParameter paramUserCount = sqlCmd.Parameters.Add( "@UserCount" , SqlDbType.Int );
				paramUserCount.Direction = ParameterDirection.Input;
				paramUserCount.Value = _nUserCount;

				SqlParameter paramMaxCount = sqlCmd.Parameters.Add( "@MaxCount" , SqlDbType.Int );
				paramMaxCount.Direction = ParameterDirection.Input;
				paramMaxCount.Value = _nMaxUserCount;

				SqlParameter paramPingTick = sqlCmd.Parameters.Add( "@PingTick" , SqlDbType.BigInt );
				paramPingTick.Direction = ParameterDirection.Input;
				paramPingTick.Value = DateTime.Now.Ticks;


				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "UpdateLoadBallancing Fail : ServerID = {0}, IP = {1}, Port = {2}, UserCount = {3}, MaxCount = {4}" , _nServerID , _strIP , _nPort , _nUserCount , LuaScriptManager.INSTANCE.m_lLoadBallancingMaxLiveTime );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateSessionInfo( Int64 userUID , UInt16 serverID , String serverIP , Int32 serverPort )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSessionInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramServerID = sqlCmd.Parameters.Add( "@serverID" , SqlDbType.SmallInt );
				paramServerID.Direction = ParameterDirection.Input;
				paramServerID.Value = (Int16)serverID;

				SqlParameter paramserverIP = sqlCmd.Parameters.Add( "@serverIP" , SqlDbType.VarChar , 15 );
				paramserverIP.Direction = ParameterDirection.Input;
				paramserverIP.Value = serverIP;

				SqlParameter paramserverPort = sqlCmd.Parameters.Add( "@serverPort" , SqlDbType.Int );
				paramserverPort.Direction = ParameterDirection.Input;
				paramserverPort.Value = serverPort;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateSessionLogin( Int64 userUID , Int32 serverGroupID, String deviceID , UInt16 serverID , String serverIP , Int32 serverPort , Int64 curTicks )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSessionLogin" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramServerGroupID = sqlCmd.Parameters.Add( "@serverGroupID", SqlDbType.Int );
				paramServerGroupID.Direction = ParameterDirection.Input;
				paramServerGroupID.Value = serverGroupID;

				SqlParameter DeviceID = sqlCmd.Parameters.Add( "@deviceID" , SqlDbType.VarChar , 50 );
				DeviceID.Direction = ParameterDirection.Input;
				DeviceID.Value = deviceID;

				SqlParameter paramServerID = sqlCmd.Parameters.Add( "@serverID" , SqlDbType.SmallInt );
				paramServerID.Direction = ParameterDirection.Input;
				paramServerID.Value = (Int16)serverID;

				SqlParameter paramserverIP = sqlCmd.Parameters.Add( "@serverIP" , SqlDbType.VarChar , 15 );
				paramserverIP.Direction = ParameterDirection.Input;
				paramserverIP.Value = serverIP;

				SqlParameter paramserverPort = sqlCmd.Parameters.Add( "@serverPort" , SqlDbType.Int );
				paramserverPort.Direction = ParameterDirection.Input;
				paramserverPort.Value = serverPort;

				SqlParameter paramCurTime = sqlCmd.Parameters.Add( "@curTime" , SqlDbType.DateTime );
				paramCurTime.Direction = ParameterDirection.Input;
				paramCurTime.Value = new DateTime( curTicks );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}


		public DbResultCode MovingTicketIssue( Int64 userUID , Int32 ServerGroupID, UInt16 serverID , out Int64 movingTicket , out String serverIP , out Int32 serverPort )
		{
			movingTicket = 0;
			serverIP = string.Empty;
			serverPort = 0;

			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_AllocMoveTicket", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramServerGroupID = sqlCmd.Parameters.Add( "@serverGroupID" , SqlDbType.Int );
				paramServerGroupID.Direction = ParameterDirection.Input;
				paramServerGroupID.Value = ServerGroupID;

				SqlParameter paramServerID = sqlCmd.Parameters.Add( "@serverID" , SqlDbType.SmallInt );
				paramServerID.Direction = ParameterDirection.Input;
				paramServerID.Value = (Int16)serverID;

				SqlParameter paramMovingTicket = sqlCmd.Parameters.Add( "@movingTicket" , SqlDbType.BigInt );
				paramMovingTicket.Direction = ParameterDirection.Output;
				paramMovingTicket.Value = movingTicket;

				SqlParameter paramServerIP = sqlCmd.Parameters.Add( "@serverIp" , SqlDbType.VarChar , 15 );
				paramServerIP.Direction = ParameterDirection.Output;
				paramServerIP.Value = serverIP;

				SqlParameter paramServerPort = sqlCmd.Parameters.Add( "@serverPort" , SqlDbType.Int );
				paramServerPort.Direction = ParameterDirection.Output;
				paramServerPort.Value = serverPort;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
					{
						movingTicket = (Int64)paramMovingTicket.Value;
						serverIP = (String)paramServerIP.Value;
						serverPort = (Int32)paramServerPort.Value;
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetEventList( List<DBEventInfo> dbEventInfo )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadEvent" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlDataReader result = null;
				try
				{
					// 실행직전 Open
					dbConn.Open();
					result = sqlCmd.ExecuteReader();
					if ( result.HasRows )
					{
						while ( result.Read() )
						{
							dbEventInfo.Add(
								new DBEventInfo(
									(Int32)result["EventUID"]
									, (bool)result["IsOpen"]
									, (Byte)result["TargetCondition"]
									, (Int32)result["TargetValue"]
									, ( (DateTime)result["StartTime"] ).Ticks
									, ( (DateTime)result["EndTime"] ).Ticks
									, (Int32)result["RewardItemID"]
								) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();

					// 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		//public DbResultCode MoveOutGameserver(Int64 userUID, UInt16 serverID, String serverIP, Int32 serverPort, out Int64 movingTicket)
		//{
		//    movingTicket = 0;
		//    DbResultCode result_code = DbResultCode.DB_ERROR;
		//    try
		//    {
		//        if (Manager.IsOpen() == false)
		//            Manager.Open();
		//    }
		//    catch (System.Exception ex)
		//    {
		//        Logger.ExceptionLog(ex);
		//        result_code = DbResultCode.DB_ERROR;
		//    }

		//    //#if _NATRACELOG
		//    //            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
		//    //#endif

		//    using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
		//    {
		//        SqlCommand sqlCmd = new SqlCommand("dbo.P_MoveOutGameServer", dbConn);
		//        sqlCmd.CommandType = CommandType.StoredProcedure;

		//        SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
		//        paramResult.Direction = ParameterDirection.ReturnValue;
		//        paramResult.Value = (Int32)DbResultCode.DB_ERROR;

		//        SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
		//        paramUserUID.Direction = ParameterDirection.Input;
		//        paramUserUID.Value = userUID;

		//        SqlParameter paramServerID = sqlCmd.Parameters.Add("@serverID", SqlDbType.SmallInt);
		//        paramServerID.Direction = ParameterDirection.Input;
		//        paramServerID.Value = (Int16)serverID;

		//        SqlParameter paramserverIP = sqlCmd.Parameters.Add("@serverIP", SqlDbType.VarChar, 15);
		//        paramserverIP.Direction = ParameterDirection.Input;
		//        paramserverIP.Value = serverIP;

		//        SqlParameter paramserverPort = sqlCmd.Parameters.Add("@serverPort", SqlDbType.Int);
		//        paramserverPort.Direction = ParameterDirection.Input;
		//        paramserverPort.Value = serverPort;

		//        SqlParameter paramMovingTicket = sqlCmd.Parameters.Add("@movingTicket", SqlDbType.BigInt);
		//        paramMovingTicket.Direction = ParameterDirection.Output;
		//        paramMovingTicket.Value = serverPort;

		//        try
		//        {
		//            // 실행직전 Open
		//            dbConn.Open();
		//            Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
		//            result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
		//            if (result_code == DbResultCode.SUCCESS)
		//            {
		//                movingTicket = (Int64)paramMovingTicket.Value;
		//            }
		//        }
		//        catch (System.IndexOutOfRangeException ior)
		//        {
		//            Logger.ExceptionLog(ior);
		//        }
		//        catch (System.InvalidOperationException ioe)
		//        {
		//            Logger.ExceptionLog(ioe);
		//        }
		//    }

		//    return result_code;
		//}

		//public DbResultCode ConfirmMovingUser(Int64 userUID, Int64 movingTicket)
		//{   
		//    DbResultCode result_code = DbResultCode.DB_ERROR;
		//    try
		//    {
		//        if (Manager.IsOpen() == false)
		//            Manager.Open();
		//    }
		//    catch (System.Exception ex)
		//    {
		//        Logger.ExceptionLog(ex);
		//        result_code = DbResultCode.DB_ERROR;
		//    }

		//    //#if _NATRACELOG
		//    //            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
		//    //#endif

		//    using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
		//    {
		//        SqlCommand sqlCmd = new SqlCommand("dbo.P_ConfirmMovingUser", dbConn);
		//        sqlCmd.CommandType = CommandType.StoredProcedure;

		//        SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
		//        paramResult.Direction = ParameterDirection.ReturnValue;
		//        paramResult.Value = (Int32)DbResultCode.DB_ERROR;

		//        SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
		//        paramUserUID.Direction = ParameterDirection.Input;
		//        paramUserUID.Value = userUID;

		//        SqlParameter paramMovingTicket = sqlCmd.Parameters.Add("@movingTicket", SqlDbType.BigInt);
		//        paramMovingTicket.Direction = ParameterDirection.Output;
		//        paramMovingTicket.Value = movingTicket;

		//        try
		//        {
		//            // 실행직전 Open
		//            dbConn.Open();
		//            Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
		//            result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
		//        }
		//        catch (System.IndexOutOfRangeException ior)
		//        {
		//            Logger.ExceptionLog(ior);
		//        }
		//        catch (System.InvalidOperationException ioe)
		//        {
		//            Logger.ExceptionLog(ioe);
		//        }
		//    }

		//    return result_code;
		//}
	}
}
