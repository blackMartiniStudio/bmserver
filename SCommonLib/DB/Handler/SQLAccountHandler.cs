﻿using SCommonLib.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SCommonLib
{
	public class SQLAccountHandler : SqlHandler
	{
		public DbResultCode AuthLoginZipi( String accountID , String accountPW , Byte provider , Int16 dbID ,
			out Byte accountState , out String blockTypeCd , out Int64 bolckExpireTime , out Int64 userUID )
		{
			userUID = -1;
			accountState = 0;
			bolckExpireTime = 0;
			blockTypeCd = String.Empty;

			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();

			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

#if _NATRACELOG
            Logger.DebugLog("DBAgent.AuthLoginZipi() accountID = {0}, accountPW = {1}", accountID, accountPW);
#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_AuthLoginZipi" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramAccountID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 40 );
				paramAccountID.Direction = ParameterDirection.Input;
				paramAccountID.Value = accountID;

				SqlParameter paramAccountPW = sqlCmd.Parameters.Add( "@PlatformPW" , SqlDbType.VarChar , 40 );
				paramAccountID.Direction = ParameterDirection.Input;
				paramAccountPW.Value = accountPW;

				SqlParameter paramDBID = sqlCmd.Parameters.Add( "@dbID" , SqlDbType.SmallInt );
				paramDBID.Direction = ParameterDirection.Input;
				paramDBID.Value = dbID;

				SqlParameter paramIsGuest = sqlCmd.Parameters.Add( "@isGuest" , SqlDbType.Bit );
				paramIsGuest.Direction = ParameterDirection.Input;
				paramIsGuest.Value = ( provider == (Byte)USER_PROVIDER.GUEST ? true : false );

				SqlParameter paramAccountState = sqlCmd.Parameters.Add( "@AccountState" , SqlDbType.TinyInt );
				paramAccountState.Direction = ParameterDirection.Output;
				paramAccountState.Value = accountState;

				SqlParameter paramBlockTypeCD = sqlCmd.Parameters.Add( "@blockTypeCD" , SqlDbType.Char , 3 );
				paramBlockTypeCD.Direction = ParameterDirection.Output;
				paramBlockTypeCD.Value = blockTypeCd;

				SqlParameter paramBlockExpireTime = sqlCmd.Parameters.Add( "@blockExpireTime" , SqlDbType.DateTime );
				paramBlockExpireTime.Direction = ParameterDirection.Output;
				paramBlockExpireTime.Value = bolckExpireTime;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Output;
				paramUserUID.Value = userUID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS || result_code == DbResultCode.DB_LOGIN_NEW_ACCOUNT )
					{
						userUID = (Int64)paramUserUID.Value;
						accountState = (Byte)paramAccountState.Value;
						blockTypeCd = (String)paramBlockTypeCD.Value;
						bolckExpireTime = ( (DateTime)paramBlockExpireTime.Value ).Ticks;
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}
		public DbResultCode AuthLoginKakao( String kakaoID , Byte provider , Int16 dbID ,
			out Byte accountState , out String blockTypeCd , out Int64 bolckExpireTime , out Int64 userUID )
		{
			userUID = -1;
			accountState = 0;
			bolckExpireTime = 0;
			blockTypeCd = String.Empty;

			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

#if _NATRACELOG
            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				using ( SqlCommand sqlCmd = new SqlCommand( "dbo.P_AuthLoginKakao" , dbConn ) )
				{
					sqlCmd.CommandType = CommandType.StoredProcedure;

					SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
					paramResult.Direction = ParameterDirection.ReturnValue;
					paramResult.Value = (Int32)DbResultCode.DB_ERROR;

					SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 40 );
					paramPlatformID.Direction = ParameterDirection.Input;
					paramPlatformID.Value = kakaoID;

					SqlParameter paramDBID = sqlCmd.Parameters.Add( "@dbID" , SqlDbType.SmallInt );
					paramDBID.Direction = ParameterDirection.Input;
					paramDBID.Value = dbID;

					SqlParameter paramAccountState = sqlCmd.Parameters.Add( "@AccountState" , SqlDbType.TinyInt );
					paramAccountState.Direction = ParameterDirection.Output;
					paramAccountState.Value = accountState;

					SqlParameter paramBlockTypeCD = sqlCmd.Parameters.Add( "@blockTypeCD" , SqlDbType.Char , 3 );
					paramBlockTypeCD.Direction = ParameterDirection.Output;
					paramBlockTypeCD.Value = blockTypeCd;

					SqlParameter paramBlockExpireTime = sqlCmd.Parameters.Add( "@blockExpireTime" , SqlDbType.DateTime );
					paramBlockExpireTime.Direction = ParameterDirection.Output;
					paramBlockExpireTime.Value = bolckExpireTime;

					SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
					paramUserUID.Direction = ParameterDirection.Output;
					paramUserUID.Value = userUID;

					try
					{
						// 실행직전 Open
						dbConn.Open();
						Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

						result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
						if ( result_code == DbResultCode.SUCCESS || result_code == DbResultCode.DB_LOGIN_NEW_ACCOUNT )
						{
							userUID = (Int64)paramUserUID.Value;
							accountState = (Byte)paramAccountState.Value;
							blockTypeCd = (String)paramBlockTypeCD.Value;
							bolckExpireTime = ( (DateTime)paramBlockExpireTime.Value ).Ticks;
						}
					}
					catch ( System.IndexOutOfRangeException ior )
					{
						Logger.ExceptionLog( ior );
					}
					catch ( System.InvalidOperationException ioe )
					{
						Logger.ExceptionLog( ioe );
					}
					catch ( Exception ex )
					{
						Logger.ExceptionLog( ex );
					}
				}

			}

			return result_code;
		}

		public DbResultCode AuthLoginIwplay( String kakaoID , Byte provider , Int16 dbID ,
		   out Byte accountState , out String blockTypeCd , out Int64 bolckExpireTime , out Int64 userUID )
		{
			userUID = -1;
			accountState = 0;
			bolckExpireTime = 0;
			blockTypeCd = String.Empty;

			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

#if _NATRACELOG
            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				using ( SqlCommand sqlCmd = new SqlCommand( "dbo.P_AuthLoginIwplay" , dbConn ) )
				{
					sqlCmd.CommandType = CommandType.StoredProcedure;

					SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
					paramResult.Direction = ParameterDirection.ReturnValue;
					paramResult.Value = (Int32)DbResultCode.DB_ERROR;

					SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 40 );
					paramPlatformID.Direction = ParameterDirection.Input;
					paramPlatformID.Value = kakaoID;

					SqlParameter paramDBID = sqlCmd.Parameters.Add( "@dbID" , SqlDbType.SmallInt );
					paramDBID.Direction = ParameterDirection.Input;
					paramDBID.Value = dbID;

					SqlParameter paramAccountState = sqlCmd.Parameters.Add( "@AccountState" , SqlDbType.TinyInt );
					paramAccountState.Direction = ParameterDirection.Output;
					paramAccountState.Value = accountState;

					SqlParameter paramBlockTypeCD = sqlCmd.Parameters.Add( "@blockTypeCD" , SqlDbType.Char , 3 );
					paramBlockTypeCD.Direction = ParameterDirection.Output;
					paramBlockTypeCD.Value = blockTypeCd;

					SqlParameter paramBlockExpireTime = sqlCmd.Parameters.Add( "@blockExpireTime" , SqlDbType.DateTime );
					paramBlockExpireTime.Direction = ParameterDirection.Output;
					paramBlockExpireTime.Value = bolckExpireTime;

					SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
					paramUserUID.Direction = ParameterDirection.Output;
					paramUserUID.Value = userUID;

					try
					{
						// 실행직전 Open
						dbConn.Open();
						Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

						result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
						if ( result_code == DbResultCode.SUCCESS || result_code == DbResultCode.DB_LOGIN_NEW_ACCOUNT )
						{
							userUID = (Int64)paramUserUID.Value;
							accountState = (Byte)paramAccountState.Value;
							blockTypeCd = (String)paramBlockTypeCD.Value;
							bolckExpireTime = ( (DateTime)paramBlockExpireTime.Value ).Ticks;
						}
					}
					catch ( System.IndexOutOfRangeException ior )
					{
						Logger.ExceptionLog( ior );
					}
					catch ( System.InvalidOperationException ioe )
					{
						Logger.ExceptionLog( ioe );
					}
					catch ( Exception ex )
					{
						Logger.ExceptionLog( ex );
					}
				}

			}

			return result_code;
		}
        public ClientErrorMessage RestoreAccount( String strPlatformID, ref Int64 _lUserUID, ref String strPublisherID )
		{
			ClientErrorMessage result_code = ClientErrorMessage.ERROR_BASE;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_RestoreAccount" , dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult		= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction			= ParameterDirection.ReturnValue;

				SqlParameter paramPlatformID	= sqlCmd.Parameters.Add( "@in_PlatformID", SqlDbType.VarChar, 40 );
				paramPlatformID.Direction		= ParameterDirection.Input;
				paramPlatformID.Value			= strPlatformID;

                SqlParameter paramUserUID	    = sqlCmd.Parameters.Add( "@out_UserUID", SqlDbType.BigInt );
                paramUserUID.Direction          = ParameterDirection.Output;
                paramUserUID.Value              = _lUserUID;

                SqlParameter paramPublisherID   = sqlCmd.Parameters.Add( "@out_PublisherID", SqlDbType.NVarChar, 40 );
                paramPublisherID.Direction      = ParameterDirection.Output;
                paramPublisherID.Value          = strPublisherID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                    if( ClientErrorMessage.SUCCESS == result_code )
                    {
                        _lUserUID       = Convert.ToInt64( paramUserUID.Value );
                        strPublisherID  = Convert.ToString( paramPublisherID.Value );
                    }
                        
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode WithdrawalAccount( Int64 userUID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			//#if _NATRACELOG
			//            Logger.DebugLog("DBAgent.AuthLoginZipi() kakaoID = {0}, provider = {1}", kakaoID, provider);
			//#endif

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_WithdrawalAccount" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@in_UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetUserUIDFromPlatformID( String strPlatformID , out Int64 userUID )
		{
			userUID = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_FindUser" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = strPlatformID;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Output;
				paramUserUID.Value = userUID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code == DbResultCode.SUCCESS )
						userUID = (Int64)paramUserUID.Value;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetWhiteListKey( out bool _bWhiteListKey )
		{
			_bWhiteListKey = false;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
                if(Manager.IsOpen() == false)
                    Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetWhiteListKey", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramIsWhiteList = sqlCmd.Parameters.Add( "@IsWhiteList", SqlDbType.Bit );
				paramIsWhiteList.Direction = ParameterDirection.Output;
				paramIsWhiteList.Value = _bWhiteListKey;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

					if ( result_code == DbResultCode.SUCCESS )
						_bWhiteListKey = ( bool )paramIsWhiteList.Value;
					else
						Logger.ErrLog( "GetWhiteListKey fail. resultCode = {0}", result_code );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode CheckWhiteList( String _strPlatformID, out bool _bLogin )
		{
			_bLogin = false;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
                if(Manager.IsOpen() == false)
                    Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_CheckWhiteList", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID", SqlDbType.NVarChar, 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = _strPlatformID;

				SqlParameter paramLogin = sqlCmd.Parameters.Add( "@Login", SqlDbType.Bit );
				paramLogin.Direction = ParameterDirection.Output;
				paramLogin.Value = _bLogin;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

					if ( result_code == DbResultCode.SUCCESS )
						_bLogin = ( bool )paramLogin.Value;
					else
						Logger.ErrLog( "CheckWhiteList fail. resultCode = {0}, PlatformID = {1}", result_code, _strPlatformID );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public ClientErrorMessage AuthPlatformLogin( Byte publisherType, String strPlatformID, String strPublisherID,
			out USER_ACCOUNT_STATE eState, out String strBlockType, out Int64 ulBlockExpireTicks, out Int64 uidPlayer, out bool bCreate )
		{
			uidPlayer			= -1;
			eState				= USER_ACCOUNT_STATE.ACTIVE;
			ulBlockExpireTicks	= 0;
			strBlockType		= String.Empty;

			ClientErrorMessage result_code = ClientErrorMessage.ERROR_BASE;

			try
			{
                if(false == Manager.IsOpen())
                    Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_PlatformLogin", dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramPlatformType	= sqlCmd.Parameters.Add( "@in_PublisherType", SqlDbType.TinyInt );
				paramPlatformType.Direction		= ParameterDirection.Input;
				paramPlatformType.Value			= publisherType;

				SqlParameter paramPlatformID	= sqlCmd.Parameters.Add( "@in_PlatformID", SqlDbType.NVarChar, 40 );
				paramPlatformID.Direction		= ParameterDirection.Input;
				paramPlatformID.Value			= strPlatformID;

				SqlParameter paramPublisherID	= sqlCmd.Parameters.Add( "@in_PublisherID", SqlDbType.NVarChar, 40 );
				paramPublisherID.Direction		= ParameterDirection.Input;
				paramPublisherID.Value			= strPublisherID;

				SqlParameter paramCreate	= sqlCmd.Parameters.Add( "@out_Create", SqlDbType.Bit );
				paramCreate.Direction		= ParameterDirection.Output;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction		= ParameterDirection.ReturnValue;

				SqlDataReader reader = null;
				try
				{
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if( reader.HasRows )
					{
						while( reader.Read() )
						{
							uidPlayer			= (Int64)reader["UserUID"];
							ulBlockExpireTicks	= ( (DateTime)reader["BlockExpireTime"] ).Ticks;
							strBlockType		= (String)reader["BlockTypeCD"];
							eState				= (USER_ACCOUNT_STATE)((Byte)reader["State"]);
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					bCreate		= Convert.ToBoolean( paramCreate.Value );
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GetNonHumanUserPublisherIDList( Byte _byPublisherType, DateTime _kHumanUserCheckDate, ref List<String> _kPublisherIDList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
                if(Manager.IsOpen() == false)
                    Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetNonHumanUserPublisherIDList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramPublisherType = sqlCmd.Parameters.Add("@PublisherType", SqlDbType.TinyInt);
                paramPublisherType.Direction = ParameterDirection.Input;
                paramPublisherType.Value = _byPublisherType;

                SqlParameter paramCheckHumanDate= sqlCmd.Parameters.Add("@CheckHumanDate", SqlDbType.DateTime);
                paramCheckHumanDate.Direction = ParameterDirection.Input;
                paramCheckHumanDate.Value = _kHumanUserCheckDate;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(true == reader.Read())
                        {
                            _kPublisherIDList.Add((String)reader["PublisherID"]);
                        }
                    }

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_GetNonHumanUserPublisherIDList is Fail retCode: {0}", result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode GetUserPublisherID(Int64 _lUserUID, ref String _strPublisherID)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetPublisherID", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult   = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction      = ParameterDirection.ReturnValue;
                kParamResult.Value          = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID  = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction     = ParameterDirection.Input;
                kParamUserUID.Value         = _lUserUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(true == reader.Read())
                        {
                            _strPublisherID = (String)reader["PublisherID"];
                        }
                    }
                        

                    reader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetPublisherID is Fail UserUID: {0} retCode: {1}", _lUserUID, eDbResultCode);
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode GetUserPublisherIDList(DataTable _kTargetUserUIDList, Byte _byPublisherType, ref List<String> _kPubshlierIDList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetPublisherIDList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@TargetUserUIDList", SqlDbType.Structured);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _kTargetUserUIDList;

                SqlParameter paramPublisherType = sqlCmd.Parameters.Add("@PublisherType", SqlDbType.TinyInt);
                paramPublisherType.Direction = ParameterDirection.Input;
                paramPublisherType.Value = _byPublisherType;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(true == reader.Read())
                        {
                            _kPubshlierIDList.Add((String)reader["PublisherID"]);
                        }
                    }

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_GetPublisherID is Fail retCode: {0}", result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

		public DbResultCode LoadNMSGHashValues( out String[] HashValues)
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			List<String> HasgValuesList = new List<string>();
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
	}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOGIN_Load_Token_Hash", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 Open
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( true == reader.Read() )
						{
							HasgValuesList.Add( ( String )reader["HashVaule"] );
						}
					}

					reader.Close();
					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( DbResultCode.SUCCESS != result_code )
					{
						Logger.ErrLog( "P_GetPublisherID is Fail retCode: {0}", result_code );
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			HashValues = HasgValuesList.ToArray();

			return result_code;
		}
	}
}
