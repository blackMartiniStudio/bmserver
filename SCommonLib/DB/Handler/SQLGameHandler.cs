﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using SCommonLib.DB;
using System.Text;
using System.Data.Common;
using KMNetClass;
using KMCSCommonLib;


namespace SCommonLib
{
    public class SQLGameHandler : SqlHandler
    {
        public static Int64 m_elapsedTime = 0;
        public static Int64 m_count = 0;

        public SqlManager GetDB( Int64 userUID)
        {
            Int16 selectDB = (Int16)(userUID / DEFAULT_INFO.BASE_USERUID_OFFSET);
            if (m_dicSqlManager.ContainsKey(selectDB) == false)
            {
                StringBuilder log = new StringBuilder();
                Logger.CallerInfoWrite(ref log);
                Logger.WarnLog("GetDB Incorrect UserUID {0} {1}", userUID, log.ToString());
                
                selectDB = m_dicSqlManager.Keys.First();
            }

            return m_dicSqlManager[selectDB];
        }

        public DbResultCode MakeUnit(Int64 userUID, String platformId, Int32 defaultCharcter, Int32 _nStartGameMoney, Int32 _nStartCash, 
            Int16 _sStartEnergy, Int32 _nStartMagicPoint, DataTable _kItemInfoList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if ( sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_MakeUnit", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction   = ParameterDirection.ReturnValue;
                paramResult.Value       = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction  = ParameterDirection.Input;
                paramUserUID.Value      = userUID;

                SqlParameter paramUniID = sqlCmd.Parameters.Add("@platformId", SqlDbType.VarChar, 40);
                paramUniID.Direction    = ParameterDirection.Input;
                paramUniID.Value        = platformId;

				SqlParameter paramDefaultCharacter = sqlCmd.Parameters.Add( "@DefaultCharacter", SqlDbType.Int );
				paramDefaultCharacter.Direction = ParameterDirection.Input;
				paramDefaultCharacter.Value     = defaultCharcter;

                SqlParameter paramStartGameMoney = sqlCmd.Parameters.Add("@StartGameMoney", SqlDbType.Int);
                paramStartGameMoney.Direction   = ParameterDirection.Input;
                paramStartGameMoney.Value       = _nStartGameMoney;

                SqlParameter paramStartCash = sqlCmd.Parameters.Add("@StartCash", SqlDbType.Int);
                paramStartCash.Direction        = ParameterDirection.Input;
                paramStartCash.Value            = _nStartCash;

                SqlParameter paramStartEnergy = sqlCmd.Parameters.Add("@StartEnergy", SqlDbType.SmallInt);
                paramStartEnergy.Direction      = ParameterDirection.Input;
                paramStartEnergy.Value          = _sStartEnergy;

                SqlParameter paramStartMagicPoint = sqlCmd.Parameters.Add("@StartMagicPoint", SqlDbType.Int);
                paramStartMagicPoint.Direction  = ParameterDirection.Input;
                paramStartMagicPoint.Value      = _nStartMagicPoint;

                SqlParameter kParamItemInfoList = sqlCmd.Parameters.Add("@ItemInfoList", SqlDbType.Structured);
                kParamItemInfoList.Direction    = ParameterDirection.Input;
                kParamItemInfoList.Value        = _kItemInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (DbResultCode.SUCCESS != result_code)
                    {
                        if (result_code == DbResultCode.DB_UNIT_DUP_PLATFORMID_FAIL)
                        {
#if DEBUG
                            Logger.DebugLog("P_MakeUnit, Duplication platformId uID = {0}, platformId ={1}", userUID, platformId);
#endif
                            return result_code;
                        }
                        else if (result_code == DbResultCode.DB_UNIT_INSERT_FAIL)
                        {
#if DEBUG
                            Logger.DebugLog("P_MakeUnit, Insert fail uID = {0}, platformId ={1}", userUID, platformId);
#endif
                        }
                        else
                            throw new InvalidOperationException("Make Unit Fail DB Result = " + result_code.ToString() + " uID = " + userUID);
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }
            return result_code;
        }

        public DbResultCode LoadUnit(Int64 userUID, ref DBUserInfo userInfo, ref DBPlayInfo playInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadUnit", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();

                    if (result.HasRows)
                    {
                        if (result.Read())
                        {
                            userInfo.platformId				    = (String)result["PlatformID"];
                            userInfo.nickname				    = (String)result["Nickname"];
                            //userInfo.selectCharacter		    = (Int32)result["SelectCharacter"];
							userInfo.grade					    = (Byte)result["Grade"];
                            userInfo.exp					    = (Int32)result["Exp"];
                            userInfo.level					    = (Byte)result["Level"];
                            userInfo.silver					    = (Int32)result["Gold"];
                            userInfo.star					    = (Int32)result["Star"];
                            userInfo.flower					    = (Int16)result["Flower"];
                            userInfo.magicPoint				    = (Int32)result["MagicPoint"];
                            userInfo.lastFlowerTick			    = (Int64)result["LastFlowerDate"];
                            userInfo.tutorialStep			    = (Byte)result["TutorialStep"];
                            userInfo.regDate				    = ((DateTime)result["RegDate"]).Ticks;
                            userInfo.GachaItemSlot			    = (Int64)result["GachaItemSlot"];
                            userInfo.countGacha				    = (Byte)result["GachaCount"];
                            userInfo.SpeedModeGacha			    = (Int32)result["GachaSpeedItemSlot"];
                            userInfo.SpeedModeGachaCount	    = (Byte)result["GachaSpeedItemCount"];
                            userInfo.isBuy			            = (bool)result["IsBuy"];
                            userInfo.TotalUseCash	            = (Int32)result["TotalUseCash"];
							userInfo.TotalAcqGameMoney			= (Int32)result["TotalAcqGameMoney"];

							playInfo.ItemModeEloPoint	        = (Int32)result["ItemModeEloPoint"];
							playInfo.SpeedModeEloPoint	        = (Int32)result["SpeedModeEloPoint"];
							playInfo.CareerRecentlyStage		= (Int32)result["CareerRecentlyStage"];
							playInfo.CareerRecentlyStageClear	= (bool)result["CareerRecentlyStageClear"];
							playInfo.ArriveStageNum				= (Int32)result["ArriveStageNum"];

                        }
                    }

                    result.Close();

                    // 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(sqlCmd.Parameters["@Result"].Value);
                    if(DbResultCode.SUCCESS != result_code && DbResultCode.DB_ERROR_COMMON_CAN_NOT_FIND_ROW_DATA != result_code )
                        Logger.ErrLog( "OnRecvPacketDBRqUserInfo @ GameDB.LoadUnit = Result : {0}", result_code.ToString( "F" ) );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateUnitInfo(Int64 userUID, DBUserInfo userInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateUnit", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramNickname = sqlCmd.Parameters.Add("@nickname", SqlDbType.NVarChar, (int)MAXLENGTH.NICKNAME);
                paramNickname.Direction = ParameterDirection.Input;
                paramNickname.Value = userInfo.nickname;

                SqlParameter paramLevel = sqlCmd.Parameters.Add("@level", SqlDbType.TinyInt);
                paramLevel.Direction = ParameterDirection.Input;
                paramLevel.Value = userInfo.level;

                SqlParameter paramExp = sqlCmd.Parameters.Add("@exp", SqlDbType.Int);
                paramExp.Direction = ParameterDirection.Input;
                paramExp.Value = userInfo.exp;

                SqlParameter paramGold = sqlCmd.Parameters.Add("@gold", SqlDbType.Int);
                paramGold.Direction = ParameterDirection.Input;
                paramGold.Value = userInfo.silver;

                SqlParameter paramStar = sqlCmd.Parameters.Add("@star", SqlDbType.Int);
                paramStar.Direction = ParameterDirection.Input;
                paramStar.Value = userInfo.star;

                SqlParameter paramFlower = sqlCmd.Parameters.Add("@flower", SqlDbType.SmallInt);
                paramFlower.Direction = ParameterDirection.Input;
                paramFlower.Value = userInfo.flower;

                SqlParameter paramMagicPoint = sqlCmd.Parameters.Add("@magicPoint", SqlDbType.Int);
                paramMagicPoint.Direction = ParameterDirection.Input;
                paramMagicPoint.Value = userInfo.magicPoint;

                SqlParameter paramLastFlowerDate = sqlCmd.Parameters.Add("@lastFlowerDate", SqlDbType.BigInt);
                paramLastFlowerDate.Direction = ParameterDirection.Input;
                paramLastFlowerDate.Value = userInfo.lastFlowerTick;

                SqlParameter paramTutorialStep = sqlCmd.Parameters.Add("@tutorialStep", SqlDbType.TinyInt);
                paramTutorialStep.Direction = ParameterDirection.Input;
                paramTutorialStep.Value = userInfo.tutorialStep;

                SqlParameter paramIsBuy = sqlCmd.Parameters.Add("@isBuy", SqlDbType.Bit);
                paramIsBuy.Direction = ParameterDirection.Input;
                paramIsBuy.Value = userInfo.isBuy;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateUserNickName(Int64 userUID, String name)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateUserName", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramNickname = sqlCmd.Parameters.Add( "@nickname", SqlDbType.NVarChar, ( int )MAXLENGTH.NICKNAME );
                paramNickname.Direction = ParameterDirection.Input;
                paramNickname.Value = name;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode RegistUserNickName(Int64 userUID, String name)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_RegistNickName", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramNickname = sqlCmd.Parameters.Add( "@nickname", SqlDbType.NVarChar, ( int )MAXLENGTH.NICKNAME );
                paramNickname.Direction = ParameterDirection.Input;
                paramNickname.Value = name;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode ProfileImgChange(Int64 userUID, String ImgUrl, Int32 ImgNum)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("P_ProfileImageChange", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramImgURL = sqlCmd.Parameters.Add( "@imgurl", SqlDbType.NVarChar, ( int )MAXLENGTH.PROFILEURL );
                paramImgURL.Direction = ParameterDirection.Input;
                paramImgURL.Value = ImgUrl;

                SqlParameter paramImgNum = sqlCmd.Parameters.Add("@imgnum", SqlDbType.Int);
                paramImgNum.Direction = ParameterDirection.Input;
                paramImgNum.Value = ImgNum;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode NoticeChange(Int64 _lUserUID, String _strNotice)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_NoticeChange", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult   = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                kParamResult.Direction      = ParameterDirection.ReturnValue;
                kParamResult.Value          = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID  = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                kParamUserUID.Direction     = ParameterDirection.Input;
                kParamUserUID.Value         = _lUserUID;

                SqlParameter kParamNotice   = sqlCmd.Parameters.Add( "@notice", SqlDbType.NVarChar, ( int )MAXLENGTH.SOCIALINFO_NOTICE );
                kParamNotice.Direction      = ParameterDirection.Input;
                kParamNotice.Value          = _strNotice;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(kParamResult.Value);
                    if( DbResultCode.SUCCESS != result_code )
                        Logger.ErrLog( "P_NoticeChange is Fail UserUID: {0}, ResultCode: {1}", _lUserUID, result_code );
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

		public DbResultCode UpdateProfileURL(Int64 _lUserUID, String _strProfileURL)
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;

			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lUserUID);
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateProfileURL", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _lUserUID;

				SqlParameter paramProfileURL = sqlCmd.Parameters.Add("@ProfileURL", SqlDbType.NVarChar, 128);
				paramProfileURL.Direction = ParameterDirection.Input;

				try
				{ 
					if( null != _strProfileURL && _strProfileURL.Length > 0 )
					{
						paramProfileURL.Value =  _strProfileURL;
					}
					else
					{
						paramProfileURL.Value = "";
					}
				}
				catch
				{
					paramProfileURL.Value = "";
				}

				SqlDataReader result = null;
				try
				{
					// 실행직전 Open
					dbConn.Open();
					result = sqlCmd.ExecuteReader();
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog(ior);
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog(ioe);
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog(ex);
				}
				finally
				{
					result.Close();

					// 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog("P_UpdateProfileURL Fail : UserUID = {0}, retCode = {1}", _lUserUID, ( (DbResultCode)result_code ).ToString());
				}
			}

			return result_code;
		}

        public DbResultCode GetUserSocialInfo(Int64 userUID, ref DBSocialInfo SocialInfo, ref Byte InviteFriendCount)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserSocialInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();

                    if (result.HasRows)
                    {
                        if (result.Read())
                        {
                            SocialInfo.LoginTime			= ((DateTime)result["LoginTime"]).Ticks;
                            SocialInfo.LogoutTime			= ((DateTime)result["LogoutTime"]).Ticks;
                            SocialInfo.ProfileImgURL		= (String)result["PhotoImageURL"];
                            SocialInfo.ProfileImgIndex		= (Int32)result["BaseProfileNumber"];
                            SocialInfo.Notice				= (String)result["Notice"];
                            SocialInfo.VipGrade				= (Byte)result["VIPGrade"];
                            SocialInfo.VIPPoint				= (float)(double)result["VIPPoint"];
                            SocialInfo.VIPShop.LastBuyDate	= ((DateTime)result["LastBuyDate"]).Ticks;
                            SocialInfo.VIPShop.DeductDate	= ((DateTime)result["DeductDate"]).Ticks;
                            SocialInfo.VIPShop.RefreshTime	= ((DateTime)result["RefreshTime"]).Ticks;
                            InviteFriendCount				= (Byte)result["PromotionalInviteCnt"];
                            SocialInfo.CurrentScore			= (Int32)result["CurrentRankScore"];
                            SocialInfo.MsgOptionKakao       = (bool)result["MsgOptionKakao"];
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();

                    // 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "GetUserSocialInfo Fail : UserUID = {0}, retCode = {1}", userUID, ( ( DbResultCode )result_code ).ToString() );
                }
            }

            return result_code;
        }

        public DbResultCode GetUserVIPShopInfo( Int64 userUID, ref DBVIPSaleItem[] _aUserSaleItemList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetUserVIPShopItemList", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();

                    List<DBVIPSaleItem> listSaleItem = new List<DBVIPSaleItem>();

                    if ( result.HasRows )
                    {
                        while ( result.Read() )
                        {
                            DBVIPSaleItem kSaleItem = new DBVIPSaleItem( ( Int32 )result["SaleID"],
                                                                         ( bool )result["Blind"] );
                            listSaleItem.Add( kSaleItem );
                        }
                    }
                    _aUserSaleItemList = listSaleItem.ToArray();
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    result.Close();

                    // 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "GetUserVIPShopInfo Fail : UserUID = {0}, retCode = {1}", userUID, result_code );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateVIPDecrease( Int64 userUID, Byte _byVIPGrade, float _fVIPPoint, DateTime _dDeductDate )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateVIP_Decrease", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramVIPGrade = sqlCmd.Parameters.Add( "@VIPGrade", SqlDbType.TinyInt );
                paramVIPGrade.Direction = ParameterDirection.Input;
                paramVIPGrade.Value = _byVIPGrade;

                SqlParameter paramVIPPoint = sqlCmd.Parameters.Add( "@VIPPoint", SqlDbType.Float );
                paramVIPPoint.Direction = ParameterDirection.Input;
                paramVIPPoint.Value = _fVIPPoint;

                SqlParameter paramDeductDate = sqlCmd.Parameters.Add( "@DeductDate", SqlDbType.DateTime );
                paramDeductDate.Direction = ParameterDirection.Input;
                paramDeductDate.Value = _dDeductDate;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateVIPDecrease Fail : UserID = {0}, VIPGrade = {1}, VIPPoint = {2}", userUID, _byVIPGrade, _fVIPPoint );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateVIPIncrease( Int64 userUID, Byte _byVIPGrade, float _fVIPPoint, Int64 _lRefreshTime, Int64 _lLastBuyDate, out bool _bFirstEnterVIP )
        {
            _bFirstEnterVIP = false;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateVIP_Increase", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramVIPGrade = sqlCmd.Parameters.Add( "@VIPGrade", SqlDbType.TinyInt );
                paramVIPGrade.Direction = ParameterDirection.Input;
                paramVIPGrade.Value = _byVIPGrade;

                SqlParameter paramVIPPoint = sqlCmd.Parameters.Add( "@VIPPoint", SqlDbType.Float );
                paramVIPPoint.Direction = ParameterDirection.Input;
                paramVIPPoint.Value = _fVIPPoint;

                SqlParameter paramLastBuyDate = sqlCmd.Parameters.Add( "@LastBuyDate", SqlDbType.DateTime );
                paramLastBuyDate.Direction = ParameterDirection.Input;
                paramLastBuyDate.Value = new DateTime( _lLastBuyDate );

                SqlParameter paramRefreshTime = sqlCmd.Parameters.Add( "@RefreashTime", SqlDbType.DateTime );
                paramRefreshTime.Direction = ParameterDirection.Input;
                paramRefreshTime.Value = new DateTime( _lRefreshTime );

                SqlParameter paramFirstEnterVIP = sqlCmd.Parameters.Add( "@FirstEnterVIP", SqlDbType.Bit );
                paramFirstEnterVIP.Direction = ParameterDirection.Output;
                paramFirstEnterVIP.Value = _bFirstEnterVIP;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateVIPIncrease Fail : UserID = {0}, VIPGrade = {1}, VIPPoint = {2}, RefreshTime = {3}, LastBuyDate = {4}", userUID, _byVIPGrade, _fVIPPoint, new DateTime( _lRefreshTime ).ToString(), new DateTime( _lLastBuyDate ).ToString() );
                    else
                        _bFirstEnterVIP = ( bool )paramFirstEnterVIP.Value;
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateUserVIPShopItem(Int64 userUID, Int32 _nMaxShowItemCount, DataTable _dtSaleIDList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateUserVIPShopItemList", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = userUID;

                SqlParameter paramMaxShopItemCount = sqlCmd.Parameters.Add( "@MaxShowItemCount", SqlDbType.Int );
                paramMaxShopItemCount.Direction = ParameterDirection.Input;
                paramMaxShopItemCount.Value = _nMaxShowItemCount;

                SqlParameter paramItemList = sqlCmd.Parameters.Add( "@UserVIPShopItemList", SqlDbType.Structured );
                paramItemList.Direction = ParameterDirection.Input;
                paramItemList.Value = _dtSaleIDList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateUserVIPShopItem Fail : retCode = {0}, UserID = {1}, MaxShowCount = {2}, ItemListCount = {3}", result_code, userUID, _nMaxShowItemCount, _dtSaleIDList.Rows.Count );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode BuyElementItem( Int64 _nUserID, Int32 _nItemID, DataTable _kRecipeElementList, Int32 _nBillType ,Int32 _nMoney, Int32 _nSaleID )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _nUserID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_BuyElementItem", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = _nUserID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add( "@ItemID", SqlDbType.Int );
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = _nItemID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add( "@RecipeElementList", SqlDbType.Structured );
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kRecipeElementList;

                SqlParameter paramBillType = sqlCmd.Parameters.Add( "@BillType", SqlDbType.Int );
                paramBillType.Direction = ParameterDirection.Input;
                paramBillType.Value = _nBillType;

                SqlParameter paramMoney = sqlCmd.Parameters.Add( "@UserMoney", SqlDbType.Int );
                paramMoney.Direction = ParameterDirection.Input;
                paramMoney.Value = _nMoney;

                SqlParameter paramSaleID = sqlCmd.Parameters.Add( "@SaleID", SqlDbType.Int );
                paramSaleID.Direction = ParameterDirection.Input;
                paramSaleID.Value = _nSaleID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "BuyElementItem Fail : retCode = {0}, UserID = {1}, ItemID = {2}, BillType = {3}, UserMoney = {4}, SaleID = {5}", result_code, _nUserID, _nItemID, _nBillType, _nMoney, _nSaleID );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode BuyRecipeItem( Int64 _nUserID, Int64 _lItemUID, Int32 _nItemId, Int32 _nUpgradeLevel,
                                            Int32 _nQuantity, bool _bIsPeriod,
                                            Int32 _nBillType, Int32 _nMoney, Int32 _nSaleID )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _nUserID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_BuyRecipeItem", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = _nUserID;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add( "@ItemUID", SqlDbType.BigInt );
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = _lItemUID;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add( "@UpgradeLevel", SqlDbType.TinyInt );
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = _nUpgradeLevel;

                SqlParameter paramQuantity = sqlCmd.Parameters.Add( "@Quantity", SqlDbType.Int );
                paramQuantity.Direction = ParameterDirection.Input;
                paramQuantity.Value = _nQuantity;

                SqlParameter paramIsPeriod = sqlCmd.Parameters.Add( "@IsPeriod", SqlDbType.Bit );
                paramIsPeriod.Direction = ParameterDirection.Input;
                paramIsPeriod.Value = _bIsPeriod;

                SqlParameter paramItemID = sqlCmd.Parameters.Add( "@ItemID", SqlDbType.Int );
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = _nItemId;

                SqlParameter paramBillType = sqlCmd.Parameters.Add( "@BillType", SqlDbType.Int );
                paramBillType.Direction = ParameterDirection.Input;
                paramBillType.Value = _nBillType;

                SqlParameter paramMoney = sqlCmd.Parameters.Add( "@UserMoney", SqlDbType.Int );
                paramMoney.Direction = ParameterDirection.Input;
                paramMoney.Value = _nMoney;

                SqlParameter paramSaleID = sqlCmd.Parameters.Add( "@SaleID", SqlDbType.Int );
                paramSaleID.Direction = ParameterDirection.Input;
                paramSaleID.Value = _nSaleID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "BuyRecipeItem Fail : retCode = {0}, UserID = {1}, ItemUID = {2}, ItemID = {3}, UpgradeLevel = {4}, Quantity = {5}, IsPeriod = {6}, BillType = {7}, UserMoney = {8}, SaleID = {9}",
                                        result_code, _nUserID, _lItemUID, _nItemId, _nUpgradeLevel, _nQuantity, _bIsPeriod, _nBillType, _nMoney, _nSaleID );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode PopVIPItemList( Int64 _nUserUID, Int32 _nSaleID )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _nUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_PopUserVIPSaleID", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = _nUserUID;

                SqlParameter paramSaleID = sqlCmd.Parameters.Add( "@SaleID", SqlDbType.Int );
                paramSaleID.Direction = ParameterDirection.Input;
                paramSaleID.Value = _nSaleID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "PopVIPItemList Fail : retCode = {0}, UserID = {1}, SaleID = {2}", result_code, _nUserUID, _nSaleID );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateUserRefreshVIPShopInfoToUse( Int64 userUID, Int32 _nMaxShowItemCount, DataTable _dtSaleIDList, Int64 _lRefreshTime )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateUserRefreshVIPShopInfoToUse", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = userUID;

                SqlParameter paramMaxShopItemCount = sqlCmd.Parameters.Add( "@MaxShowItemCount", SqlDbType.Int );
                paramMaxShopItemCount.Direction = ParameterDirection.Input;
                paramMaxShopItemCount.Value = _nMaxShowItemCount;

                SqlParameter paramSaleIDList = sqlCmd.Parameters.Add( "@UserVIPShopItemList", SqlDbType.Structured );
                paramSaleIDList.Direction = ParameterDirection.Input;
                paramSaleIDList.Value = _dtSaleIDList;

                SqlParameter paramRefreshTime = sqlCmd.Parameters.Add( "@RefreshTime", SqlDbType.DateTime );
                paramRefreshTime.Direction = ParameterDirection.Input;
                paramRefreshTime.Value = new DateTime( _lRefreshTime );

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateUserRefreshVIPShopInfoToUse Fail : retCode = {0}, UserID = {1}, MaxShowCount = {2}, ItemListCount = {3}, RefreshTime = {4}", result_code, userUID, _nMaxShowItemCount, _dtSaleIDList.Rows.Count, new DateTime( _lRefreshTime ).ToString() );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateUserRefreshVIPShopInfoToBuy( Int64 userUID, Int32 _nMaxShowItemCount, DataTable _dtSaleIDList, Int64 _lRefreshTime, Int32 _nCalculatedUserGold )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateUserRefreshVIPShopInfoToBuy", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = userUID;

                SqlParameter paramMaxShopItemCount = sqlCmd.Parameters.Add( "@MaxShowItemCount", SqlDbType.Int );
                paramMaxShopItemCount.Direction = ParameterDirection.Input;
                paramMaxShopItemCount.Value = _nMaxShowItemCount;

                SqlParameter paramSaleIDList = sqlCmd.Parameters.Add( "@UserVIPShopItemList", SqlDbType.Structured );
                paramSaleIDList.Direction = ParameterDirection.Input;
                paramSaleIDList.Value = _dtSaleIDList;

                SqlParameter paramRefreshTime = sqlCmd.Parameters.Add( "@RefreshTime", SqlDbType.DateTime );
                paramRefreshTime.Direction = ParameterDirection.Input;
                paramRefreshTime.Value = new DateTime( _lRefreshTime );

                SqlParameter paramUserGold = sqlCmd.Parameters.Add( "@UserGold", SqlDbType.Int );
                paramUserGold.Direction = ParameterDirection.Input;
                paramUserGold.Value = _nCalculatedUserGold;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateUserRefreshVIPShopInfoToBuy Fail : retCode = {0}, UserID = {1}, MaxShowCount = {2}, ItemListCount = {3}, RefreshTime = {4}, UserGold = {5}", result_code, userUID, _nMaxShowItemCount, _dtSaleIDList.Rows.Count, new DateTime( _lRefreshTime ).ToString(), _nCalculatedUserGold );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode VIPUpgradeLog( Int64 userUID, Byte _byVIPGrade )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_VIPUpgradeLog", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramVIPGrade = sqlCmd.Parameters.Add( "@VIPGrade", SqlDbType.TinyInt );
                paramVIPGrade.Direction = ParameterDirection.Input;
                paramVIPGrade.Value = _byVIPGrade;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "VIPUpgradeLog Fail : UserID = {0}, VIPGrade = {1}", userUID, _byVIPGrade );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode GetFriendRelationList(Int64 userUID, out DBFriendState[] _kFriendArray)
        {
            List<DBFriendState> kFriendList = new List<DBFriendState>();
            DbResultCode        result_code = DbResultCode.DB_ERROR;

            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader result = null;
            
                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();

                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            DBFriendState kFriendState  = new DBFriendState();
                            kFriendState.UserUID        = (Int64)result["UserUID"];
                            kFriendState.FriendUID      = (Int64)result["FriendUserUID"];
                            kFriendState.FriendRelation = (Byte)result["FriendRelation"];
                            kFriendList.Add(kFriendState);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();

                    // 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS == result_code)
                    {
                        _kFriendArray = kFriendList.ToArray();
                    }
                    else
                    {
                        Logger.ErrLog("P_GetFriendList is Fail UserUID: {0}", userUID);
                        _kFriendArray = new DBFriendState[0];
                    }
                }
            }

            return result_code;
        }

        public DbResultCode UpdateSelectCharacter(Int64 userUID, Int32 selectCharacter)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateSelectCharacter", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramSelectCharacter = sqlCmd.Parameters.Add("@selectCharacter", SqlDbType.Int);
                paramSelectCharacter.Direction = ParameterDirection.Input;
                paramSelectCharacter.Value = selectCharacter;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }
            return result_code;
        }

        public DbResultCode LoadDBItem(Int64 userUID, List<DBUserItem> items)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Int64 itemUID                   = (Int64)reader["ItemUID"];
                            Int32 itemID                    = (Int32)reader["ItemID"];
                            Int32 attachItemID              = (Int32)reader["AttachItemID"];
                            bool isEquip                    = (bool)reader["IsEquip"];
                            Int32 quantity                  = (Int32)reader["Quantity"];
                            Byte upgradeLevel               = (Byte)reader["ItemLevel"];
                            Byte mainTag                    = GlobalHelper.GetMainTag( itemID );//(byte)reader["MainTag"];
                            Byte subTag                     = GlobalHelper.GetSubTag( itemID );//(byte)reader["SubTag"];
                            DateTime regDate                = (DateTime)reader["RegDate"];
                            DateTime UpgradeCompleteDate    = DateTime.FromBinary( 0 );//(DateTime)reader["UpgradeCompleteDate"];
                            bool isOpenSkill                = true;// (bool)reader["isOpenSkill"];
                            DateTime UpgradeStartDate       = DateTime.FromBinary(0);         // 초기화
							bool isPeriod                   = (bool)reader["isPeriod"];
							DateTime ExpirationDate         = (DateTime)reader["ExpirationDate"];
                            bool isUsable                   = true;
                            Int32 partsSkillID				= (Int32)reader["SkillID"];
                            Byte partsSkillLevel			= (byte)reader["SkillLevel"];
							DateTime completeDate			= (DateTime)reader["SkillUpgradeCompleteDate"];
                            Int32 ExpPoint                  = (Int32)reader["ExpPoint"];
                                                    
                            // 만료 된 아이템이 장착 중일 경우 만료 로직을 타야한다.
                            if (false == isEquip)
                                isUsable = DateTime.Now <= ExpirationDate ? true : false;

                            DBUserItem new_item = new DBUserItem(   itemUID, 
                                                                    itemID,
                                                                    attachItemID, 
                                                                    isEquip, 
                                                                    upgradeLevel, 
                                                                    0, 
                                                                    mainTag, 
                                                                    subTag,
                                                                    ExpPoint,
                                                                    quantity,
                                                                    UpgradeCompleteDate.Ticks,
                                                                    regDate.Ticks,
                                                                    isOpenSkill,
                                                                    UpgradeStartDate.Ticks,
																	isPeriod,
																	ExpirationDate.Ticks,
                                                                    isUsable,
                                                                    new DBPropertyInfo[0],
                                                                    new DBSkillInfo( partsSkillID, partsSkillLevel, completeDate.Ticks ) );

                            items.Add(new_item);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
            }

            return result_code;
        }

        public DbResultCode LoadDBPartsSkill( Int64 userUID, DataTable _PartsList, List<DBPartsSkill> _PartsSkillList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadPartsSkill", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramPartsUIDList = sqlCmd.Parameters.Add( "@PartsUIDList", SqlDbType.Structured );
                paramPartsUIDList.Direction = ParameterDirection.Input;
                paramPartsUIDList.Value = _PartsList;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            Int64 itemUID   = ( Int64 )reader["ItemUID"];
                            Int32 skillID   = ( Int32 )reader["SkillID"];
                            Byte skillLevel = (Byte)reader["SkillLevel"];
                            DateTime completeTime = (DateTime)reader["UpgradeCompleteDate"];

                            DBPartsSkill new_item = new DBPartsSkill( itemUID, skillID, skillLevel, completeTime.Ticks );

                            _PartsSkillList.Add( new_item );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( null != reader )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                        if ( result_code != DbResultCode.SUCCESS )
                            Logger.ErrLog( "LoadDBPartsSkill Fail : codeResult = {0}", ( Int32 )result_code );
                    }
                    else
                        result_code = DbResultCode.DB_ERROR;
                }
            }

            return result_code;
        }

        public DbResultCode LoadItemPropertyMastery(Int64 lUserUID, DataTable kCharacterItemUIDList, Dictionary<Int64, List<DBPropertyInfo>> kDicPropertyInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEM_LoadPropertyMastery", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramCharacterItemUIDList = sqlCmd.Parameters.Add("@CharacterItemUIDList", SqlDbType.Structured);
                paramCharacterItemUIDList.Direction = ParameterDirection.Input;
                paramCharacterItemUIDList.Value = kCharacterItemUIDList;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Int64 lCharacterItemUID = (Int64)reader["CharacterItemUID"];
                            Int32 nPropertyID       = (Int32)reader["PropertyID"];
                            Int16 sMasteryExp       = (Int16)reader["MasteryExp"];
                            Byte byMasteryLevel     = (Byte)reader["MasteryLevel"];

                            if (false == kDicPropertyInfo.ContainsKey(lCharacterItemUID))
                                kDicPropertyInfo.Add(lCharacterItemUID, new List<DBPropertyInfo>());

                            kDicPropertyInfo[lCharacterItemUID].Add(new DBPropertyInfo(nPropertyID, sMasteryExp, byMasteryLevel));
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    if (null != reader)
                    {
                        reader.Close();
                        result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                        if (result_code != DbResultCode.SUCCESS)
                            Logger.ErrLog("LoadItemPropertyMastery Fail : codeResult = {0}", (Int32)result_code);
                    }
                    else
                        result_code = DbResultCode.DB_ERROR;
                }
            }

            return result_code;
        }

        public DbResultCode UpdateFlower(Int64 userUID, Int16 flower, Int64 flowerTicks)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateFlower", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramFlower = sqlCmd.Parameters.Add("@flower", SqlDbType.SmallInt);
                paramFlower.Direction = ParameterDirection.Input;
                paramFlower.Value = flower;

                SqlParameter paramLastFlowerDate = sqlCmd.Parameters.Add("@lastFlowerDate", SqlDbType.BigInt);
                paramLastFlowerDate.Direction = ParameterDirection.Input;
                paramLastFlowerDate.Value = flowerTicks;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    //if (result_code != DbResultCode.SUCCESS)
                    //    Logger.ErrLog("UpdateFlower Fail UserUID = {0}", userUID);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

		public DbResultCode UpdateGold( Int64 userUID, Int32 currGold )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateRara", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramGold = sqlCmd.Parameters.Add("@gold", SqlDbType.Int);
                paramGold.Direction = ParameterDirection.Input;
                paramGold.Value = currGold;


                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(sqlCmd.Parameters["@Result"].Value);

                    if (result_code != DbResultCode.SUCCESS)
						Logger.ErrLog( "UpdateGold Fail UserUID = {0} rara = {1}", userUID, currGold );

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateStar(Int64 userUID, Int32 currStar)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateStar", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramStar = sqlCmd.Parameters.Add("@star", SqlDbType.Int);
                paramStar.Direction = ParameterDirection.Input;
                paramStar.Value = currStar;


                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateMagicPoint(Int64 userUID, Int32 currMagicPoint)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateMagicPoint", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramMagicPoint = sqlCmd.Parameters.Add("@magicPoint", SqlDbType.Int);
                paramMagicPoint.Direction = ParameterDirection.Input;
                paramMagicPoint.Value = currMagicPoint;


                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateGoodsInfo( Int64 _lUserUID, Byte _byGoodsType, Int32 _nUpdateValue, Byte _byCause )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.USER_UpdateGoods", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult       = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                kParamResult.Direction          = ParameterDirection.ReturnValue;
                kParamResult.Value              = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID      = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                kParamUserUID.Direction         = ParameterDirection.Input;
                kParamUserUID.Value             = _lUserUID;

                SqlParameter kParamGoodsType    = sqlCmd.Parameters.Add( "@GoodsType", SqlDbType.TinyInt );
                kParamGoodsType.Direction       = ParameterDirection.Input;
                kParamGoodsType.Value           = _byGoodsType;

                SqlParameter kParamUpdateValue  = sqlCmd.Parameters.Add( "@QuantityVariation", SqlDbType.Int );
                kParamUpdateValue.Direction     = ParameterDirection.Input;
                kParamUpdateValue.Value         = _nUpdateValue;

                SqlParameter kParamCause        = sqlCmd.Parameters.Add( "@ChangeCause", SqlDbType.TinyInt );
                kParamCause.Direction           = ParameterDirection.Input;
                kParamCause.Value               = _byCause;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32( kParamResult.Value );
                    if(DbResultCode.SUCCESS != result_code)
                        Logger.ErrLog( "USER_UpdateGoods is Fail. resultCode = {0}, UserUID: {1}, GoodsType: {2}, UpdateValue: {3}, Cause: {4}", result_code, _lUserUID, (AttachmentType)_byGoodsType, _nUpdateValue, _byCause );
                    return result_code;
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateEnergyInfo( Int64 _UserUID, Int32 _UpdateValue, Int64 _LastFlowerTick )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _UserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.USER_UpdateEnergy", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                kParamUserUID.Direction = ParameterDirection.Input;
                kParamUserUID.Value = _UserUID;

                SqlParameter kParamUpdateValue = sqlCmd.Parameters.Add( "@UpdateValue", SqlDbType.Int );
                kParamUpdateValue.Direction = ParameterDirection.Input;
                kParamUpdateValue.Value = _UpdateValue;

                SqlParameter kParamLastFlowerTick = sqlCmd.Parameters.Add( "@LastFlowerTick", SqlDbType.BigInt );
                kParamLastFlowerTick.Direction = ParameterDirection.Input;
                kParamLastFlowerTick.Value = _LastFlowerTick;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32( kParamResult.Value );
                    if( DbResultCode.SUCCESS != result_code )
                        Logger.ErrLog( "USER_UpdateGoods is Fail. resultCode = {0}, UserUID: {1}, UpdateValue: {2}, LastFlowerTick: {3}", result_code, _UserUID, _UpdateValue, _LastFlowerTick );
                    return result_code;
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode UpgradeItem(Int64 userUID, Int64 targetItemUid, Int32 targetItemID, Byte targetItemGrade)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_UpgradeItem", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd, "@Result");

                DBDecorator.InParam(paramList, "@userUID", userUID);
                DBDecorator.InParam(paramList, "@targetItemUid", targetItemUid);
                DBDecorator.InParam(paramList, "@targetItemID", targetItemID);
                DBDecorator.InParam(paramList, "@targetGrade", targetItemGrade);

                result_code = DBDecorator.Execute(cmd);
            }

            return result_code;
        }

        public DbResultCode UpgradeItemBySilver( Int64 userUID, Int64 targetItemUid, Int32 targetItemID, Byte targetItemGrade, Byte targetAffectGrade, Int64 UpgradeCompleteTicks, DataTable deleteProductItem )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_ITEM_SetUpgradeCompleteDate", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd, "@Result");

                DBDecorator.InParam(paramList, "@itemUID", targetItemUid);
                DBDecorator.InParam(paramList, "@userUID", userUID);
                DBDecorator.InParam(paramList, "@UpgradeCompleteDate", DateTime.FromBinary(UpgradeCompleteTicks));

                SqlParameter paramDeleteElementList = cmd.Parameters.Add( "@deleteElementItem", SqlDbType.Structured );
                paramDeleteElementList.Direction = ParameterDirection.Input;
                paramDeleteElementList.Value = deleteProductItem;

                result_code = DBDecorator.Execute(cmd);
            }

            return result_code;
        }

        public DbResultCode UpgradeItemUsingElementItem(Int64 userUID, Int64 targetItemUid, Int32 targetItemID, Byte targetItemGrade, DataTable deleteProductItem)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {

                SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpgradeItemUsingElementItem", dbConn );
                SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

                DBDecorator.InParam( paramList, "@userUID", userUID );
                DBDecorator.InParam( paramList, "@targetItemUid", targetItemUid );
                DBDecorator.InParam( paramList, "@targetItemID", targetItemID );
                DBDecorator.InParam( paramList, "@targetGrade", targetItemGrade );

                SqlParameter paramDeleteElementList = cmd.Parameters.Add( "@deleteElementItem", SqlDbType.Structured );
                paramDeleteElementList.Direction = ParameterDirection.Input;
                paramDeleteElementList.Value = deleteProductItem;

                result_code = DBDecorator.Execute( cmd );
            }

            return result_code;
        }

        public DbResultCode OpenHiddenSkill( Int64 userUID, Int64 targetItemUid, Int32 skillID, Int32 userGold, out Int32 prevSkillID )
        {
			prevSkillID = 0;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {

                SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpdatePartsSkill", dbConn );
                SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

                DBDecorator.InParam( paramList, "@UserID", userUID );
                DBDecorator.InParam( paramList, "@TargetItemUID", targetItemUid );
                DBDecorator.InParam( paramList, "@SkillID", skillID );
                DBDecorator.InParam( paramList, "@UserGold", userGold );
				DBDecorator.InParam( paramList, "@PrevSkillID", 0, ParameterDirection.Output );

                result_code = DBDecorator.Execute( cmd );
                if( result_code != DbResultCode.SUCCESS )
                    Logger.ErrLog( "OpenHiddenSkill Fail : codeResult = {0}", (Int32)result_code );
                else
                    prevSkillID = (int)paramList["@PrevSkillID"].Value;
            }

            return result_code;
        }

        public DbResultCode UpgradePartsSkill( Int64 UserUID, Int64 ItemUID, Int32 SkillID, Byte SkillGrade, DataTable deleteElement )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( UserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpgradePartsSkillByStar", dbConn );
                SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

                DBDecorator.InParam( paramList, "@userUID", UserUID );
                DBDecorator.InParam( paramList, "@targetItemUID", ItemUID );
                DBDecorator.InParam( paramList, "@skillID", SkillID );
                DBDecorator.InParam( paramList, "@skillLevel", SkillGrade );

                SqlParameter paramDeleteElementList = cmd.Parameters.Add( "@deleteElementItem", SqlDbType.Structured );
                paramDeleteElementList.Direction = ParameterDirection.Input;
                paramDeleteElementList.Value = deleteElement;

                result_code = DBDecorator.Execute( cmd );
            }

            return result_code;
        }

        public ClientErrorMessage UpgradePartsSkillImmediately( Int64 UserUID, Int64 ItemUID, Int32 SkillID, Byte SkillGrade )
        {
			ClientErrorMessage result_code = ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( UserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpgradePartsSkillImmediately", dbConn );
                SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

                DBDecorator.InParam( paramList, "@userUID",			UserUID		);
                DBDecorator.InParam( paramList, "@targetItemUID",	ItemUID		);
                DBDecorator.InParam( paramList, "@skillID",			SkillID		);
                DBDecorator.InParam( paramList, "@skillLevel",		SkillGrade	);

				result_code = (ClientErrorMessage)DBDecorator.Execute( cmd );
            }

            return result_code;
        }

        public DbResultCode UpgradePartsSkillBySilver( Int64 UserUID, Int64 ItemUID, Int32 ItemID, Int32 skillID, Byte SkillGrade, Int64 UpgradeCompleteTicks, DataTable deleteProductItem )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( UserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpgradePartsSkillSetCompleteTick", dbConn );
                SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

                DBDecorator.InParam( paramList, "@UserUID", UserUID );
                DBDecorator.InParam( paramList, "@TargetItemUID", ItemUID );
                DBDecorator.InParam( paramList, "@SkillID", skillID );
                DBDecorator.InParam( paramList, "@SkillLevel", SkillGrade );
                DBDecorator.InParam( paramList, "@UpgradeCompleteDate", DateTime.FromBinary( UpgradeCompleteTicks ) );
                SqlParameter paramDeleteElementList = cmd.Parameters.Add( "@deleteElementItem", SqlDbType.Structured );
                paramDeleteElementList.Direction = ParameterDirection.Input;
                paramDeleteElementList.Value = deleteProductItem;

                result_code = DBDecorator.Execute( cmd );
            }

            return result_code;
        }

        public DbResultCode NewPostCheck(Int64 userUID, out bool isNewPost)
        {
            isNewPost = false;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_NewPostCheck", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramCurrTime = sqlCmd.Parameters.Add("@currTime", SqlDbType.DateTime);
                paramCurrTime.Direction = ParameterDirection.Input;
                paramCurrTime.Value = DateTime.Now;

                SqlParameter paramIsNew = sqlCmd.Parameters.Add("isNew", SqlDbType.Bit);
                paramIsNew.Direction = ParameterDirection.Output;
                paramIsNew.Value = userUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.SUCCESS)
                        isNewPost = (bool)paramIsNew.Value;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode DeletePostBox( Int64 userUID, List<DBDeletePost> listDeletePost )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( userUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_DeletePost", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            Int64 postUID       = ( Int64 )reader["PostUID"];
                            Int64 attach        = ( Int64 )reader["Attach"];
                            bool isTakeAttach   = ( bool )reader["IsTakeAttach"];

                            listDeletePost.Add( new DBDeletePost( postUID, attach, isTakeAttach) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    reader.Close();
                    result_code = ( DbResultCode )Convert.ToInt32( sqlCmd.Parameters["@ret"].Value );
                }
            }

            return result_code;
        }

        public DbResultCode LoadPostBoxList(Int64 _lUserUID, DateTime _kLastCheckDate, out List<DBNewPostBox> _kPostBoxList)
        {
            _kPostBoxList = new List<DBNewPostBox>();

            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadPostBoxList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult       = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction          = ParameterDirection.ReturnValue;
                kParamResult.Value              = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID      = sqlCmd.Parameters.Add("@UserUID ", SqlDbType.BigInt);
                kParamUserUID.Direction         = ParameterDirection.Input;
                kParamUserUID.Value             = _lUserUID;

                SqlParameter kParamCurrentDate  = sqlCmd.Parameters.Add("@LastCheckDate ", SqlDbType.DateTime);
                kParamCurrentDate.Direction     = ParameterDirection.Input;
                kParamCurrentDate.Value         = _kLastCheckDate;

                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            Int64           lPostUID        = (Int64)reader["PostUID"];
                            Int64           lSendUserUID    = (Int64)reader["SendUserUID"];
                            Int32           nAttachItemID   = (Int32)reader["AttachItemID"];
                            Int32           nAttachQuantity = (Int32)reader["AttachItemQuantity"];
                            Byte            byPostType      = (Byte)reader["PostType"];
                            Byte            byMessageType   = (Byte)reader["MessageType"];
                            String          strPostMessage  = (String)reader["PostMessage"];
							bool			IsDel			= (bool)reader["IsDel"];
                            DateTime        kExpirationDate = (DateTime)reader["ExpirationDate"];
							DBNewPostBox    kDbPostBox      = new DBNewPostBox( lPostUID, byPostType, byMessageType, lSendUserUID, nAttachItemID, nAttachQuantity, strPostMessage, IsDel, false, kExpirationDate.Ticks ); 
                            _kPostBoxList.Add( kDbPostBox );
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(sqlCmd.Parameters["@Result"].Value);

                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_LoadPostBoxList is Fail UserUID: {0}, ResultCode: {1}", _lUserUID, eDbResultCode);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode RemovePostBoxList(Int64 _lUserUID, DataTable _kRemovePostUIDList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd = new SqlCommand("dbo.P_RemovePostBox", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult                   = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction                      = ParameterDirection.ReturnValue;
                kParamResult.Value                          = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamExpirationPostUIDList    = kSqlCmd.Parameters.Add("@RemovePostUIDList", SqlDbType.Structured);
                kParamExpirationPostUIDList.Direction       = ParameterDirection.Input;
                kParamExpirationPostUIDList.Value           = _kRemovePostUIDList;

                Int32 nExecuteLine = 0;
                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    nExecuteLine = kSqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
				finally
				{
					eDbResultCode = (DbResultCode)Convert.ToInt32( kSqlCmd.Parameters["@Result"].Value );

					if( DbResultCode.SUCCESS != eDbResultCode )
						Logger.ErrLog( "P_RemovePostBox is Fail UserUID: {0}, Count:{1}, ResultCode: {2}", _lUserUID, _kRemovePostUIDList.Rows.Count, eDbResultCode );
				}
            }

            return eDbResultCode;
        }

		public DbResultCode AddPostBox( Int64 _lUserUID, DBNewPostBox _kPostBox, out Int64 _lPostUID, out bool _IsDel )
		{
			_lPostUID = 0;
			_IsDel = false;

			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.P_AddPostBox", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_ReceiveUserUID",		_lUserUID										);
				DBDecorator.InParam( paramList, "@in_SendUserUID",			_kPostBox.SendUserUID							);
				DBDecorator.InParam( paramList, "@in_AttachItemID",			_kPostBox.AttachItemID							);
				DBDecorator.InParam( paramList, "@in_AttachItemQuantity",	_kPostBox.AttachItemQuantity					);
				DBDecorator.InParam( paramList, "@in_PostType",				_kPostBox.PostType								);
				DBDecorator.InParam( paramList, "@in_PostMessageType",		_kPostBox.MessageType							);
				DBDecorator.InParam( paramList, "@in_PostMessage",			_kPostBox.PostMessage							);
				DBDecorator.InParam( paramList, "@in_IsDel",				_kPostBox.IsDel									);
				DBDecorator.InParam( paramList, "@in_ExpirationDate",		new DateTime( _kPostBox.ExpirationDateTick )	);

				SqlParameter paramPostUID	= DBDecorator.InParam( paramList, "@out_PostUID",	_lPostUID, ParameterDirection.Output	);
				SqlParameter paramIsDel		= DBDecorator.InParam( paramList, "@out_IsDel",		_IsDel, ParameterDirection.Output		);

				result_code = DBDecorator.Execute( sqlCmd );
				if( result_code == DbResultCode.SUCCESS )
				{
					_lPostUID	= Convert.ToInt64( paramPostUID.Value );
					_IsDel		= Convert.ToBoolean( paramIsDel.Value );
				}
			}

			return result_code;
		}

        public DbResultCode AllGoodsPostTakeAttachment(Int64 _lUserUID, DataTable _kRemovePostUIDList, Byte _byGoodsType, Int32 _nGoodsQuantity, Byte _byChangeCause)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd = new SqlCommand("dbo.P_AllGoodsPostTakeAttachment", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult               = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction                  = ParameterDirection.ReturnValue;
                kParamResult.Value                      = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID              = kSqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction                 = ParameterDirection.Input;
                kParamUserUID.Value                     = _lUserUID;

                SqlParameter kParamRemovePostUIDList    = kSqlCmd.Parameters.Add("@RemovePostUIDList", SqlDbType.Structured);
                kParamRemovePostUIDList.Direction       = ParameterDirection.Input;
                kParamRemovePostUIDList.Value           = _kRemovePostUIDList;

                SqlParameter kParamGoodsType            = kSqlCmd.Parameters.Add("@GoodsType", SqlDbType.TinyInt);
                kParamGoodsType.Direction               = ParameterDirection.Input;
                kParamGoodsType.Value                   = _byGoodsType;

                SqlParameter kParamGoodsQuantity        = kSqlCmd.Parameters.Add("@QuantityVariation", SqlDbType.Int);
                kParamGoodsQuantity.Direction           = ParameterDirection.Input;
                kParamGoodsQuantity.Value               = _nGoodsQuantity;

                SqlParameter kParamChangeCause          = kSqlCmd.Parameters.Add("@ChangeCause", SqlDbType.TinyInt);
                kParamChangeCause.Direction             = ParameterDirection.Input;
                kParamChangeCause.Value                 = _byChangeCause;

                Int32 nExecuteLine = 0;
                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    nExecuteLine = kSqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kSqlCmd.Parameters["@Result"].Value);

                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_AllGoodsPostTakeAttachment is Fail UserUID: {0}, ResultCode: {1}", _lUserUID, eDbResultCode);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode AllItemPostTakeAttachment(Int64 _lUserUID, DataTable _kRemovePostUIDList, DataTable _kItemInfoList, 
            DataTable _kPropertyInfoList, DataTable _kItemModifyPlanList, Byte _nGoodsType, Int32 _nGoodsQuantity)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd = new SqlCommand("dbo.P_AllItemPostTakeAttachment", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult               = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction                  = ParameterDirection.ReturnValue;
                kParamResult.Value                      = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID              = kSqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction                 = ParameterDirection.Input;
                kParamUserUID.Value                     = _lUserUID;

                SqlParameter kParamRemovePostUIDList    = kSqlCmd.Parameters.Add("@RemovePostUIDList", SqlDbType.Structured);
                kParamRemovePostUIDList.Direction       = ParameterDirection.Input;
                kParamRemovePostUIDList.Value           = _kRemovePostUIDList;

                SqlParameter kParamItemInfoList         = kSqlCmd.Parameters.Add("@ItemInfoList", SqlDbType.Structured);
                kParamItemInfoList.Direction            = ParameterDirection.Input;
                kParamItemInfoList.Value                = _kItemInfoList;

                SqlParameter kParamPropertyInfoList     = kSqlCmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                kParamPropertyInfoList.Direction        = ParameterDirection.Input;
                kParamPropertyInfoList.Value            = _kPropertyInfoList;

                SqlParameter kParamQuantityItemInfoList = kSqlCmd.Parameters.Add("@QuantitiyItemInfoList", SqlDbType.Structured);
                kParamQuantityItemInfoList.Direction    = ParameterDirection.Input;
                kParamQuantityItemInfoList.Value        = _kItemModifyPlanList;

                SqlParameter kParamGoodsType            = kSqlCmd.Parameters.Add("@GoodsType", SqlDbType.TinyInt);
                kParamGoodsType.Direction               = ParameterDirection.Input;
                kParamGoodsType.Value                   = _nGoodsType;

                SqlParameter kParamGoodsQuantity        = kSqlCmd.Parameters.Add("@GoodsQuantity", SqlDbType.Int);
                kParamGoodsQuantity.Direction           = ParameterDirection.Input;
                kParamGoodsQuantity.Value               = _nGoodsQuantity;

                SqlParameter kParamChangeCause          = kSqlCmd.Parameters.Add("@ChangeCause", SqlDbType.TinyInt);
                kParamChangeCause.Direction             = ParameterDirection.Input;
                kParamChangeCause.Value                 = (Byte)GoodsChangeCauseTemp.IncreaseGoodsByPostBox;

                Int32 nExecuteLine = 0;
                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    nExecuteLine = kSqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kSqlCmd.Parameters["@Result"].Value);

                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_AllItemPostTakeAttachment is Fail UserUID: {0}, ResultCode: {1}", _lUserUID, eDbResultCode);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode UpdatePost(Int64 userUID, Int64 postUID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdatePost", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramPostUID = sqlCmd.Parameters.Add("@postUID", SqlDbType.BigInt);
                paramPostUID.Direction = ParameterDirection.Input;
                paramPostUID.Value = postUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("UpDatePostBox Fail UserUID = {0}, PostUID = {1}", userUID, postUID);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode AddPost( Int64 fromUserUID, Byte type, Int64 toUserUID, Int64 attach, String info, Int64 endTicks, out Int64 PostUID, List<DBDeletePost> listDeletePost )
        {
			PostUID = 0;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = GetDB(toUserUID);

            try
            {
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_AddPost", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = toUserUID;

                SqlParameter paramType = sqlCmd.Parameters.Add("@type", SqlDbType.TinyInt);
                paramType.Direction = ParameterDirection.Input;
                paramType.Value = type;

                SqlParameter paramSenderUserUID = sqlCmd.Parameters.Add("@senderUserUID", SqlDbType.BigInt);
                paramSenderUserUID.Direction = ParameterDirection.Input;
                paramSenderUserUID.Value = fromUserUID;

                SqlParameter paramAttach = sqlCmd.Parameters.Add("@attach", SqlDbType.BigInt);
                paramAttach.Direction = ParameterDirection.Input;
                paramAttach.Value = attach;

                SqlParameter paramInfo = sqlCmd.Parameters.Add("@info", SqlDbType.NVarChar, (int)MAXLENGTH.POST_INFO);
                paramInfo.Direction = ParameterDirection.Input;
                paramInfo.Value = info;

                SqlParameter paramCurrTime = sqlCmd.Parameters.Add("@currTime", SqlDbType.DateTime);
                paramCurrTime.Direction = ParameterDirection.Input;
                paramCurrTime.Value = DateTime.Now;

                SqlParameter paramEndTime = sqlCmd.Parameters.Add("@endTime", SqlDbType.DateTime);
                paramEndTime.Direction = ParameterDirection.Input;
                paramEndTime.Value = new DateTime(endTicks);

				SqlParameter paramPostUID = sqlCmd.Parameters.Add( "@PostUID" , SqlDbType.BigInt );
				paramPostUID.Direction = ParameterDirection.Output;
				paramPostUID.Value = 0;


                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            Int64 lPostUID      = ( Int64 )reader["PostUID"];
                            Int64 lAttach       = ( Int64 )reader["Attach"];
                            bool isTakeAttach   = ( bool )reader["IsTakeAttach"];

                            listDeletePost.Add( new DBDeletePost( lPostUID, lAttach, isTakeAttach ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    reader.Close();
                    result_code = ( DbResultCode )Convert.ToInt32( sqlCmd.Parameters["@ret"].Value );

                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "AddPost Fail UserUID = {0}, type={1}, senderUserUID={2}", toUserUID, type, fromUserUID );
                    }
                    else
                    {
                        PostUID = ( Int64 )paramPostUID.Value;
                    }
                }
            }

            return result_code;
        }

        public DbResultCode PostTakeAttachment(Int64 userUID, Int64 postUID, bool isTakeAttach, bool isDel)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdatePostDelAttachment", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramPostUID = sqlCmd.Parameters.Add("@postUID", SqlDbType.BigInt);
                paramPostUID.Direction = ParameterDirection.Input;
                paramPostUID.Value = postUID;

                SqlParameter paramIsTakeAttach = sqlCmd.Parameters.Add("@isTakeAttach", SqlDbType.Bit);
                paramIsTakeAttach.Direction = ParameterDirection.Input;
                paramIsTakeAttach.Value = isTakeAttach;

                SqlParameter paramIsDel = sqlCmd.Parameters.Add("@isDel", SqlDbType.Bit);
                paramIsDel.Direction = ParameterDirection.Input;
                paramIsDel.Value = isDel;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("PostTakeAttachment Fail userUID = {0}, postUID={1}, isDel={2}", userUID, postUID, isDel);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode PostAllTakeAttachment(Int64 userUID, String postsUID, out Int64[] attList)
        {
            attList = null;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdatePostAllDelAttachment", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramPostsUID = sqlCmd.Parameters.Add("@postsUID", SqlDbType.VarChar, -1);
                paramPostsUID.Direction = ParameterDirection.Input;
                paramPostsUID.Value = postsUID;

                List<Int64> attachmentList = new List<Int64>();
                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                            attachmentList.Add((Int64)reader["postUID"]);
                    }

                    
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("PostAllTakeAttachment Fail userUID = {0}", userUID);

                    attList = attachmentList.ToArray();
                }
            }

            return result_code;
        }

        public DbResultCode GetFriendPlayerInfo(Int64 friendUID,
                                                ref Int32 selectCharacter,
                                                ref String nickName,
                                                ref Byte characterLevel,
                                                ref Int32 wearPartID,
                                                ref DBItemLook[] dbItemsLook)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(friendUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendPlyerInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramFriendUID = sqlCmd.Parameters.Add("@FriendUID", SqlDbType.BigInt);
                paramFriendUID.Direction = ParameterDirection.Input;
                paramFriendUID.Value = friendUID;

                SqlParameter paramSelectCharacter = sqlCmd.Parameters.Add("@SelectCharacter", SqlDbType.Int);
                paramSelectCharacter.Direction = ParameterDirection.Output;
                paramSelectCharacter.Value = selectCharacter;

                SqlParameter paramNickName = sqlCmd.Parameters.Add("@NickName", SqlDbType.VarChar, 30);
                paramNickName.Direction = ParameterDirection.Output;
                paramNickName.Value = nickName;

                SqlParameter paramCharacterLevel = sqlCmd.Parameters.Add("@CharacterLevel", SqlDbType.TinyInt);
                paramCharacterLevel.Direction = ParameterDirection.Output;
                paramCharacterLevel.Value = characterLevel;

                SqlParameter paramWearPartID = sqlCmd.Parameters.Add("@WearPartID", SqlDbType.Int);
                paramWearPartID.Direction = ParameterDirection.Output;
                paramWearPartID.Value = wearPartID;

                List<DBItemLook> itemLookList = new List<DBItemLook>();
                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            itemLookList.Add(new DBItemLook((Int32)reader["ItemID"],
                                                             (Byte)reader["UpgradeLevel"],
                                                             0
                                                            ));
                        }
                    }
                    dbItemsLook = itemLookList.ToArray();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.SUCCESS)
                    {
                        selectCharacter = (Int32)paramSelectCharacter.Value;
                        nickName = (String)paramNickName.Value;
                        characterLevel = (Byte)paramCharacterLevel.Value;
                        wearPartID = (Int32)paramWearPartID.Value;
                    }
                }
            }

            return result_code;
        }

        public DbResultCode GetTotalInviteCount(Int64 userUID, out Byte toDayInviteCount, out Int32 totalInviteCount)
        {
            toDayInviteCount = 0;
            totalInviteCount = 0;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetTotalInviteCount", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramFriendUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramFriendUID.Direction = ParameterDirection.Input;
                paramFriendUID.Value = userUID;

                SqlParameter paramToDayInviteCount = sqlCmd.Parameters.Add("@ToDayInviteCount", SqlDbType.TinyInt);
                paramToDayInviteCount.Direction = ParameterDirection.Output;
                paramToDayInviteCount.Value = toDayInviteCount;

                SqlParameter paramTotalInviteCount = sqlCmd.Parameters.Add("@TotalInviteCount", SqlDbType.Int);
                paramTotalInviteCount.Direction = ParameterDirection.Output;
                paramTotalInviteCount.Value = totalInviteCount;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.SUCCESS)
                    {
                        toDayInviteCount = (Byte)paramToDayInviteCount.Value;
                        totalInviteCount = (Int32)paramTotalInviteCount.Value;
                    }
                    else
                        Logger.ErrLog( "P_GetTotalInviteCount is Fail UserUID: {0}, ResultCode: {1}", userUID, result_code );

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode SendRefreshFatigueGoodsToFriend(Int64 nFromUserUID, Int64 nToUserUID, Byte byPostType, Byte byGoodsType, Int32 nGoodsQty, String strPostMessage, Int64 nPostEndTick,
            Byte bySendTermDay, out Int64 lPostUID)
        {
			lPostUID = 0;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(nFromUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.USER_SendRefreshFatigueGoods", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@FromUserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = nFromUserUID;

                SqlParameter paramFriendUserUID = sqlCmd.Parameters.Add("@ToUserUID", SqlDbType.BigInt);
                paramFriendUserUID.Direction = ParameterDirection.Input;
                paramFriendUserUID.Value = nToUserUID;

                SqlParameter paramPostType = sqlCmd.Parameters.Add("@PostType", SqlDbType.TinyInt);
                paramPostType.Direction = ParameterDirection.Input;
                paramPostType.Value = byPostType;

                SqlParameter paramGoodsType     = sqlCmd.Parameters.Add("@GoodsType", SqlDbType.TinyInt);
                paramGoodsType.Direction        = ParameterDirection.Input;
                paramGoodsType.Value            = byGoodsType;

                SqlParameter paramGoodsQty      = sqlCmd.Parameters.Add("@GoodsQuantity", SqlDbType.Int);
                paramGoodsQty.Direction         = ParameterDirection.Input;
                paramGoodsQty.Value             = nGoodsQty;

                SqlParameter paramPostMessage = sqlCmd.Parameters.Add("@PostMessage", SqlDbType.NVarChar, 50);
                paramPostMessage.Direction = ParameterDirection.Input;
				paramPostMessage.Value = strPostMessage;

                SqlParameter paramPostEndTime = sqlCmd.Parameters.Add("@PostEndTime", SqlDbType.DateTime);
                paramPostEndTime.Direction = ParameterDirection.Input;
                paramPostEndTime.Value = DateTime.FromBinary(nPostEndTick);

                SqlParameter parambSendTermDay = sqlCmd.Parameters.Add("@SendTermDay", SqlDbType.TinyInt);
                parambSendTermDay.Direction = ParameterDirection.Input;
                parambSendTermDay.Value = bySendTermDay;

				SqlParameter paramPostUID = sqlCmd.Parameters.Add( "@PostUID" , SqlDbType.BigInt );
				paramPostUID.Direction = ParameterDirection.Output;
				paramPostUID.Value = 0;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if ( DbResultCode.SUCCESS == result_code )
					{
						lPostUID = (Int64)paramPostUID.Value;
					}
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    result_code = DbResultCode.DB_ERROR;
                }
            }

            return result_code;
        }

		public DbResultCode SendRefreshFatiguqGoodsToFriends( Int64 _lFromUserUID, DataTable _kToUserUIDList, Byte _byPostType,
			String _strPostMessage, Int64 _lPostEndTick, out Int64[] _kPoustUIDArray, Byte _bySendTermDay, Byte _byGoodsType, Int32 _nGoodsQuantity, Byte _byChangeCause )
		{
			_kPoustUIDArray = new Int64[0];

			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lFromUserUID);
				if(sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}
			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.USER_SendRefreshFatigueGoodsToList", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@FromUserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _lFromUserUID;

				SqlParameter paramFriendUserUID = sqlCmd.Parameters.Add("@ToUserUIDList", SqlDbType.Structured);
				paramFriendUserUID.Direction = ParameterDirection.Input;
				paramFriendUserUID.Value = _kToUserUIDList;

				SqlParameter paramPostType = sqlCmd.Parameters.Add("@PostType", SqlDbType.TinyInt);
				paramPostType.Direction = ParameterDirection.Input;
				paramPostType.Value = _byPostType;

				SqlParameter paramPostMessage = sqlCmd.Parameters.Add("@PostMessage", SqlDbType.NVarChar, 50);
				paramPostMessage.Direction = ParameterDirection.Input;
				paramPostMessage.Value = _strPostMessage;

				SqlParameter paramPostEndTime = sqlCmd.Parameters.Add("@PostEndTime", SqlDbType.DateTime);
				paramPostEndTime.Direction = ParameterDirection.Input;
				paramPostEndTime.Value = DateTime.FromBinary(_lPostEndTick);

				SqlParameter parambSendTermDay = sqlCmd.Parameters.Add("@SendTermDay", SqlDbType.TinyInt);
				parambSendTermDay.Direction = ParameterDirection.Input;
				parambSendTermDay.Value = _bySendTermDay;

				SqlParameter paramGoodsType = sqlCmd.Parameters.Add("@GoodsType", SqlDbType.TinyInt);
				paramGoodsType.Direction = ParameterDirection.Input;
				paramGoodsType.Value = _byGoodsType;

				SqlParameter paramGoodsQuantity = sqlCmd.Parameters.Add("@GoodsQuantity", SqlDbType.Int);
				paramGoodsQuantity.Direction = ParameterDirection.Input;
				paramGoodsQuantity.Value = _nGoodsQuantity;

				SqlParameter paramChangeCause = sqlCmd.Parameters.Add("@ChangeCause", SqlDbType.TinyInt);
				paramChangeCause.Direction = ParameterDirection.Input;
				paramChangeCause.Value = _byChangeCause;

				SqlDataReader result = null;

				List<Int64> kPostUIDList = new List<Int64>();

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					result = sqlCmd.ExecuteReader();
					if(result.HasRows)
					{
						while(result.Read())
						{
							kPostUIDList.Add((Int64)result["PostUID"]);
						}
					}
				}
				catch(Exception ex)
				{
					Logger.ExceptionLog(ex);
					result_code = DbResultCode.DB_ERROR;
				}
				finally
				{
					if(null != result)
					{
						result.Close();
					}

					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS == result_code)
					{
						_kPoustUIDArray = kPostUIDList.ToArray();
					}
					else
					{
						Logger.ErrLog("USER_SendRefreshFatigueGoodsToList is Fail UserUID: {0}, Result_Code: {1}", _lFromUserUID, result_code);
					}
				}
			}

			return result_code;
		}

        public DbResultCode LoadOption(Int64 userUID, out DBOptionInfo Info)
        {
            Info = new DBOptionInfo(false, false);

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserOption", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();

                    if (result.HasRows)
                    {
                        if (result.Read())
                        {
                            Info.FriendRequest      = (bool)result["FriendRequestOption"];
                            Info.ProfileImageOpen   = (bool)result["ProfileImageOption"];
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if( DbResultCode.SUCCESS != result_code )
                        Logger.ErrLog( "P_GetUserOption is Fail UserUID: {0}, ResultCode: {1}", userUID, result_code );
                }
            }

            return result_code;
        }


        public DbResultCode GetVIPGrade(Int64 UserUID, out Byte _byVIPGrade)
        {
            _byVIPGrade = 0;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( UserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }

            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetVIPGrade", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramVIPGrade = sqlCmd.Parameters.Add( "@VIPGrade", SqlDbType.TinyInt );
                paramVIPGrade.Direction = ParameterDirection.Output;
                paramVIPGrade.Value = 0;

                try
                {
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code == DbResultCode.SUCCESS )
                    {
                        _byVIPGrade = ( Byte )paramVIPGrade.Value;
                    }
                    else
                        Logger.ErrLog( "P_GetVIPGrade is Fail UserUID: {0}, ResultCode: {1}", UserUID, result_code );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode GetRaceInfo(Int64 UserUID, ref List<DBRaceInfo> RaceInfoList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserRaceInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            DBRaceInfo kRaceInfo                    = new DBRaceInfo();
                            kRaceInfo.GameMode = (Byte)reader["GameMode"];
                            kRaceInfo.RaceIndex = (Int64)reader["RaceIndex"];

                            kRaceInfo.PlayInfo = new DBRacePlayInfo();
                            kRaceInfo.PlayInfo.WinCount = (Int32)reader["WinCount"];
                            kRaceInfo.PlayInfo.DrawCount = (Int32)reader["DrawCount"];
                            kRaceInfo.PlayInfo.LoseCount = (Int32)reader["LoseCount"];
                            kRaceInfo.PlayInfo.PlaySeconds = (Int32)reader["PlaySeconds"];
                            kRaceInfo.PlayInfo.WinningStreak = (Int16)reader["WinningStreak"];
                            kRaceInfo.PlayInfo.LoseStreak = (Int16)reader["LoseStreak"];
                            kRaceInfo.PlayInfo.LastPlayDateTicks = ((DateTime)reader["LastPlayDate"]).Ticks;
                            kRaceInfo.PlayInfo.LastWinDateTicks = ((DateTime)reader["LastWinDate"]).Ticks;

                            kRaceInfo.EloInfo = new DBRaceEloInfo();
                            kRaceInfo.EloInfo.EloPoint = (Int32)reader["EloPoint"];
                            kRaceInfo.EloInfo.HighEloPoint = (Int32)reader["HighEloPoint"];
                            kRaceInfo.EloInfo.StressPoint = (Byte)reader["StressPoint"];
                            kRaceInfo.EloInfo.StressApplyCount = (Byte)reader["StressApplyCount"];
                            kRaceInfo.EloInfo.NextPenaltyDateTicks = ((DateTime)reader["NextPenaltyDate"]).Ticks;

                            RaceInfoList.Add(kRaceInfo);
                        }
                    }

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("P_GetUserRaceInfo is Fail UserUID: {0}, retCode: {1}", UserUID, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode GetCareerFavorInfo(Int64 _lUserUID, out Int32 _nAccpetFavorCount, out Int64 _lEpisodeOpenDate, out Int16 _sCurrentEpisodeNumber)
        {
            _nAccpetFavorCount      = 0;
            _lEpisodeOpenDate       = 0;
            _sCurrentEpisodeNumber  = 0;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CAREER_GetCareerFavorInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramAcceptFavorCount = sqlCmd.Parameters.Add("@AcceptFavorCount", SqlDbType.Int);
                paramAcceptFavorCount.Direction = ParameterDirection.Output;
                paramAcceptFavorCount.Value = _nAccpetFavorCount;

                SqlParameter paramEpisodeOpenDate = sqlCmd.Parameters.Add("@EpisodeOpenDate", SqlDbType.DateTime);
                paramEpisodeOpenDate.Direction = ParameterDirection.Output;
                paramEpisodeOpenDate.Value = _lEpisodeOpenDate;

                SqlParameter paramCurrentEpisodeNumber = sqlCmd.Parameters.Add("@CurrentEpisodeNumber", SqlDbType.SmallInt);
                paramCurrentEpisodeNumber.Direction = ParameterDirection.Output;
                paramCurrentEpisodeNumber.Value = _sCurrentEpisodeNumber;

                try
                {
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code == DbResultCode.SUCCESS)
                    {
                        _nAccpetFavorCount      = Convert.ToInt32(paramAcceptFavorCount.Value);
                        _lEpisodeOpenDate       = Convert.ToDateTime(paramEpisodeOpenDate.Value).Ticks;
                        _sCurrentEpisodeNumber  = Convert.ToInt16(paramCurrentEpisodeNumber.Value);
                    }
                    else
                    {
                        Logger.ErrLog("CAREER_GetAcceptFavorCount is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }
        
        public DbResultCode UpdateTicketCount(Int64 UserUID, Byte ChannelType, Int32 Quantity)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CHANNEL_UpdateTicketCnt", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramChannelType = sqlCmd.Parameters.Add("@ChannelType", SqlDbType.TinyInt);
                paramChannelType.Direction = ParameterDirection.Input;
                paramChannelType.Value = ChannelType;

                SqlParameter paramQuantity = sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int);
                paramQuantity.Direction = ParameterDirection.Input;
                paramQuantity.Value = Quantity;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)paramResult.Value;
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode GetAppFriendsUID(Int64 userUID, List<Int64> friendsUID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetAppFriendsUID", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();

                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            friendsUID.Add((Int64)result["FriendUID"]);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();

                    // 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
            }

            return result_code;
        }

		public ClientErrorMessage GetAttendanceInfo( Int64 _lUserUID, ref DBAttendanceInfo _kInfo )
        {
			ClientErrorMessage result_code = ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.P_GetUserAttendanceInfo", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult	= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction		= ParameterDirection.ReturnValue;


                SqlParameter paramUserUID	= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction		= ParameterDirection.Input;
                paramUserUID.Value			= _lUserUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
							_kInfo.UserType						= (Byte)reader["UserType"];
							_kInfo.TotalAttendanceCount			= (Byte)reader["TotalAttendanceCount"];
							_kInfo.ContinuousAttendanceCount	= (Int16)reader["ContinuousAttendanceCount"];
							_kInfo.LastActiveTicks				= ((DateTime)reader["AttendanceTime"]).Ticks;
                        }
                    }

                    reader.Close();
                    result_code = (ClientErrorMessage)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public ClientErrorMessage UpdateAttendaceInfo(Int64 _lUserUID, DBAttendanceInfo _kInfo)
        {
			ClientErrorMessage result_code = ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.P_UpdateUserAttendanceInfo", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult	= sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction		= ParameterDirection.ReturnValue;
                paramResult.Value			= (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID	= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction		= ParameterDirection.Input;
                paramUserUID.Value			= _lUserUID;

                SqlParameter paramUserType	= sqlCmd.Parameters.Add("@UserType", SqlDbType.TinyInt);
                paramUserType.Direction		= ParameterDirection.Input;
                paramUserType.Value			= _kInfo.UserType;

                SqlParameter paramTotalAttendanceCount		= sqlCmd.Parameters.Add("@TotalAttendanceCount", SqlDbType.TinyInt);
                paramTotalAttendanceCount.Direction			= ParameterDirection.Input;
                paramTotalAttendanceCount.Value				= _kInfo.TotalAttendanceCount;

                SqlParameter paramContinousAttendanceCount	= sqlCmd.Parameters.Add("@ContinuousAttendanceCount", SqlDbType.SmallInt);
                paramContinousAttendanceCount.Direction		= ParameterDirection.Input;
                paramContinousAttendanceCount.Value			= _kInfo.ContinuousAttendanceCount;

				SqlParameter paramAttendanceTime			= sqlCmd.Parameters.Add( "@AttendanceTime", SqlDbType.DateTime );
				paramAttendanceTime.Direction				= ParameterDirection.Input;
				paramAttendanceTime.Value					= new DateTime(_kInfo.LastActiveTicks);

                try
                {
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

		public ClientErrorMessage UpdateSevenAttendance( Int64 _uiUserUID, Byte _byMaxCount, Int64 _uiUpdateTicks, ref DBSevenAttendanceInfo _AttendanceInfo )
		{
			ClientErrorMessage result_code = ClientErrorMessage.ERROR_NORMAL;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( _uiUserUID );
				if( false == sqlMgr.IsOpen() )
				{
					sqlMgr.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_UpdateSevenAttendance", dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction		= ParameterDirection.ReturnValue;

				SqlParameter paramUserUID	= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
				paramUserUID.Direction		= ParameterDirection.Input;
				paramUserUID.Value			= _uiUserUID;

				SqlParameter paramMaxCount	= sqlCmd.Parameters.Add( "@in_MaxCount", SqlDbType.TinyInt );
				paramMaxCount.Direction		= ParameterDirection.Input;
				paramMaxCount.Value			= _byMaxCount;

				SqlParameter paramNow		= sqlCmd.Parameters.Add( "@in_DateTime", SqlDbType.DateTime );
				paramNow.Direction			= ParameterDirection.Input;
				paramNow.Value				= new DateTime( _uiUpdateTicks );

				SqlDataReader reader = null;
				try
				{
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if( reader.HasRows )
					{
						while( reader.Read() )
						{
							_AttendanceInfo.AttendanceCount = Convert.ToByte( reader["AttendanceCount"] );
							_AttendanceInfo.LastActiveTicks = Convert.ToDateTime( reader["AttendanceTime"] ).Ticks;
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

        public DbResultCode UpdateCumulationInfo(Int64 userUID, Int32 score, Int16 winCount, Int16 playCount, Int32 gold)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateCumulationInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramScore = sqlCmd.Parameters.Add("@score", SqlDbType.Int);
                paramScore.Direction = ParameterDirection.Input;
                paramScore.Value = score;

                SqlParameter paramWinCount = sqlCmd.Parameters.Add("@win", SqlDbType.TinyInt);
                paramWinCount.Direction = ParameterDirection.Input;
                paramWinCount.Value = winCount;

                SqlParameter paramPlayCount = sqlCmd.Parameters.Add("@play", SqlDbType.TinyInt);
                paramPlayCount.Direction = ParameterDirection.Input;
                paramPlayCount.Value = playCount;

                SqlParameter paramGold = sqlCmd.Parameters.Add("@gold", SqlDbType.Int);
                paramGold.Direction = ParameterDirection.Input;
                paramGold.Value = playCount;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)paramResult.Value;
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateTutorialStep(Int64 userUID, Byte step)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateTutorialStep", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramStep = sqlCmd.Parameters.Add("@step", SqlDbType.TinyInt);
                paramStep.Direction = ParameterDirection.Input;
                paramStep.Value = step;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("UpdateTutorialStep Fail UserUID={0}, step={1}", userUID, step);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpgradeBaseCharacter(Int64 userUID, Byte selectCharacter, Byte level)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateBaseCharacterUpgrade", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramSelectCharacter = sqlCmd.Parameters.Add("@selectCharacter", SqlDbType.TinyInt);
                paramSelectCharacter.Direction = ParameterDirection.Input;
                paramSelectCharacter.Value = selectCharacter;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add("@upgradeLevel", SqlDbType.TinyInt);
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = level;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }

                return result_code;
            }
        }

        public DataTable GetPropertyIDTable(Int64 _lItemUID, Int32 nItemID)
        {
            ItemInfo itemInfo = null;
            if (false == ConfigDataGlobal.INSTANCE.ItemDataManager.GetItem(nItemID, out itemInfo))
                return null;

            ItemInfo.ItemKind MainTag = ItemScriptManager.GetItemMainTag(nItemID);
            if (ItemInfo.ItemKind.E_CHARACTER != MainTag)
                return null;

            Int32[] AffectIDArray = itemInfo.GetAffectIDArray();
			if( 0 == AffectIDArray.Length || 0 == AffectIDArray[0] )
                return null;

            DataTable AffectIDList = new DataTable();
            AffectIDList.Columns.Add("CharacterItemUID",    typeof(Int64));
            AffectIDList.Columns.Add("PropertyID",          typeof(Int32));
            AffectIDList.Columns.Add("MasteryExp",          typeof(Int16));
            AffectIDList.Columns.Add("MasteryLevel",        typeof(Byte));
            for (Int32 i = 0; i < AffectIDArray.Length; ++i)
                AffectIDList.Rows.Add(_lItemUID, AffectIDArray[i]);

            return AffectIDList;
        }

        public DbResultCode InsertItem(Int64 lUserUID, Int64 lItemUID, Int32 nItemID, Int32 nAttachItemID, bool bIsEquip, 
            Int32 nQuantity, Int32 nUpgradeLevel, bool bIsPeriod, DateTime kExpirationDate, Int32 partsSkillID)
        {
            DataTable PropertyIDTable = GetPropertyIDTable(lItemUID, nItemID);
            if(null != PropertyIDTable)
            {
                return InsertItemWithProperty(lUserUID, lItemUID, nItemID, nAttachItemID, bIsEquip, nQuantity, nUpgradeLevel, 
                    bIsPeriod, kExpirationDate, PropertyIDTable);
            }

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEM_Acquire", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = lUserUID;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@ItemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = lItemUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@ItemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = nItemID;

                SqlParameter paramAttachItemID = sqlCmd.Parameters.Add("@AttachItemID", SqlDbType.Int);
                paramAttachItemID.Direction = ParameterDirection.Input;
                paramAttachItemID.Value = nAttachItemID;

                SqlParameter paramIsEquip = sqlCmd.Parameters.Add("@IsEquip", SqlDbType.Bit);
                paramIsEquip.Direction = ParameterDirection.Input;
                paramIsEquip.Value = bIsEquip;

                SqlParameter paramQuantity = sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int);
                paramQuantity.Direction = ParameterDirection.Input;
                paramQuantity.Value = nQuantity;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add("@UpgradeLevel", SqlDbType.TinyInt);
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = nUpgradeLevel;

                SqlParameter paramIsPeriod = sqlCmd.Parameters.Add("@IsPeriod", SqlDbType.Bit);
                paramIsPeriod.Direction = ParameterDirection.Input;
                paramIsPeriod.Value = bIsPeriod;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = kExpirationDate;

                SqlParameter paramPartsSkillID = sqlCmd.Parameters.Add( "@PartsSkillID", SqlDbType.Int );
                paramPartsSkillID.Direction = ParameterDirection.Input;
                paramPartsSkillID.Value = partsSkillID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("InsertItem Fail : UserUID = {0} ItemUID = {1}, ItemID = {2}, AttachItemID = {3}, IsEquip = {4}, Quantity = {5}, UpgradeLevel = {6}, IsPeriod = {7}, ExpirationData = {8}, Result_Code: {9}",
                                                         lUserUID, lItemUID, nItemID, nAttachItemID, bIsEquip, nQuantity, nUpgradeLevel, bIsPeriod, kExpirationDate, result_code );

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

		public DbResultCode GachaGuageBonus( Int64 _UserUID, DataTable _ItemList, Byte _BillType )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( _UserUID );
				if ( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_InsertGachaGuageBonusItems", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUnitUID.Direction = ParameterDirection.Input;
				paramUnitUID.Value = _UserUID;

				SqlParameter paramItemList = sqlCmd.Parameters.Add( "@ItemInfoList", SqlDbType.Structured );
				paramItemList.Direction = ParameterDirection.Input;
				paramItemList.Value = _ItemList;

                SqlParameter paramBillType = sqlCmd.Parameters.Add( "@BillType", SqlDbType.TinyInt );
                paramBillType.Direction = ParameterDirection.Input;
                paramBillType.Value = _BillType;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "GachaGuageBonus Fail : UserUID = {0} , ItemListCount = {1}, BonusGachaType = {2}, Result_Code: {3}",
                                                         _UserUID, _ItemList.Rows.Count, (Int32)_BillType, result_code );

					return result_code;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return DbResultCode.SUCCESS;
		}

		public DbResultCode UpdateCareerDropReward( Int64 _UserUID, DataTable _ItemList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( _UserUID );
				if ( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.ITEM_AcquireList", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUnitUID.Direction = ParameterDirection.Input;
				paramUnitUID.Value = _UserUID;

				SqlParameter paramItemList = sqlCmd.Parameters.Add( "@ItemInfoList", SqlDbType.Structured );
				paramItemList.Direction = ParameterDirection.Input;
				paramItemList.Value = _ItemList;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "UpdateCareerDropReward Fail : UserUID = {0} , ItemListCount = {1} Result_Code: {2}", _UserUID, _ItemList.Rows.Count, result_code );

					return result_code;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return DbResultCode.SUCCESS;
		}

        public DbResultCode InsertItemWithProperty(Int64 lUserUID, Int64 lItemUID, Int32 nItemID, Int32 nAttachItemID, bool bIsEquip,
            Int32 nQuantity, Int32 nUpgradeLevel, bool bIsPeriod, DateTime kExpirationDate, DataTable kPropertyIDList)
        {
            DataTable PropertyIDTable = GetPropertyIDTable(lItemUID, nItemID);
            if (null != PropertyIDTable)
            {

            }

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEM_AcquireWithProperty", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = lUserUID;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@ItemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = lItemUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@ItemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = nItemID;

                SqlParameter paramAttachItemID = sqlCmd.Parameters.Add("@AttachItemID", SqlDbType.Int);
                paramAttachItemID.Direction = ParameterDirection.Input;
                paramAttachItemID.Value = nAttachItemID;

                SqlParameter paramIsEquip = sqlCmd.Parameters.Add("@IsEquip", SqlDbType.Bit);
                paramIsEquip.Direction = ParameterDirection.Input;
                paramIsEquip.Value = bIsEquip;

                SqlParameter paramQuantity = sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int);
                paramQuantity.Direction = ParameterDirection.Input;
                paramQuantity.Value = nQuantity;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add("@UpgradeLevel", SqlDbType.TinyInt);
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = nUpgradeLevel;

                SqlParameter paramIsPeriod = sqlCmd.Parameters.Add("@IsPeriod", SqlDbType.Bit);
                paramIsPeriod.Direction = ParameterDirection.Input;
                paramIsPeriod.Value = bIsPeriod;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = kExpirationDate;

                SqlParameter paramPropertyIDList = sqlCmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                paramPropertyIDList.Direction = ParameterDirection.Input;
                paramPropertyIDList.Value = kPropertyIDList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("InsertItemWithProperty Fail UserUID = {0} itemID = {1}", lUserUID, nItemID);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

		public DbResultCode UpdatePeriodItem( Int64 userUID, Int64 itemUID, Int64 UpdateExpirationDateTick )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( userUID );
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdatePeriodItem", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@userUID", SqlDbType.BigInt );
				paramUnitUID.Direction = ParameterDirection.Input;
				paramUnitUID.Value = userUID;

				SqlParameter paramItemUID = sqlCmd.Parameters.Add( "@itemUID", SqlDbType.BigInt );
				paramItemUID.Direction = ParameterDirection.Input;
				paramItemUID.Value = itemUID;

				SqlParameter paramExpirationDate = sqlCmd.Parameters.Add( "@UpdateExpirationDate", SqlDbType.DateTime );
				paramExpirationDate.Direction = ParameterDirection.Input;
				paramExpirationDate.Value = new DateTime( UpdateExpirationDateTick );


				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "UpdatePeriodItem Fail UserUID = {0} itemUID = {1}", userUID, itemUID );

					return result_code;
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return DbResultCode.SUCCESS;
		}

		public DbResultCode ItemDelete( Int64 userUID, Int64 itemUID, Int16 _Cause )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( userUID );
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_ItemDelete", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@userUID", SqlDbType.BigInt );
				paramUnitUID.Direction = ParameterDirection.Input;
				paramUnitUID.Value = userUID;

				SqlParameter paramItemUID = sqlCmd.Parameters.Add( "@itemUID", SqlDbType.BigInt );
				paramItemUID.Direction = ParameterDirection.Input;
				paramItemUID.Value = itemUID;

				SqlParameter paramCause = sqlCmd.Parameters.Add( "@cause", SqlDbType.SmallInt );
				paramCause.Direction = ParameterDirection.Input;
				paramCause.Value = _Cause;


				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "ItemDelete Fail UserUID = {0} itemUID = {1}", userUID, itemUID );

					return result_code;
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return DbResultCode.SUCCESS;
		}

		public ClientErrorMessage UpdateAchievement( Int64 UserUID, DBAchievement _kAchievementInfo )
        {
			ClientErrorMessage resultCode = ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return resultCode;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.ACHIEVEMENT_Update", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult		= sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction			= ParameterDirection.ReturnValue;

				SqlParameter paramUnitUID		= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
                paramUnitUID.Direction			= ParameterDirection.Input;
                paramUnitUID.Value				= UserUID;

				SqlParameter paramGoalID		= sqlCmd.Parameters.Add( "@in_GoalID", SqlDbType.Int );
                paramGoalID.Direction			= ParameterDirection.Input;
				paramGoalID.Value				= _kAchievementInfo.GoalID;

				SqlParameter paramGainedCount	= sqlCmd.Parameters.Add( "@in_ResetValue", SqlDbType.Int );
                paramGainedCount.Direction		= ParameterDirection.Input;
				paramGainedCount.Value			= _kAchievementInfo.Value;

                try
                {
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
					resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
					if( resultCode != ClientErrorMessage.SUCCESS )
                    {
						Logger.ErrLog( "ACHIEVEMENT_Update is Fail UserUID: {0}, GoalID: {1}, result_code: {2}", UserUID, _kAchievementInfo.GoalID, _kAchievementInfo.Value );
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

			return resultCode;
        }

		public DBAchievement[] AcquireAchievement( Int64 UserUID, DBAchievement[] _kAcquireAchievementInfo )
		{
			List<DBAchievement>	liUserAchievementInfo = new List<DBAchievement>();
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( UserUID );
				if( false == sqlMgr.IsOpen() )
					sqlMgr.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return new DBAchievement[0];
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.ACHIEVEMENT_Acquire", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction		= ParameterDirection.ReturnValue;

				SqlParameter paramUnitUID	= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
				paramUnitUID.Direction		= ParameterDirection.Input;
				paramUnitUID.Value			= UserUID;

				SqlParameter paramGoalID	= sqlCmd.Parameters.Add( "@in_GoalID", SqlDbType.Int );
				paramGoalID.Direction		= ParameterDirection.Input;

				SqlParameter paramAcquireValue	= sqlCmd.Parameters.Add( "@in_AcquireValue", SqlDbType.Int );
				paramAcquireValue.Direction		= ParameterDirection.Input;

				SqlParameter paramCurrentValue	= sqlCmd.Parameters.Add( "@out_CurrentValue", SqlDbType.Int );
				paramCurrentValue.Direction		= ParameterDirection.Output;

				dbConn.Open();
				for( Int32 i = 0; i < _kAcquireAchievementInfo.Length; ++i )
				{
					paramGoalID.Value		= _kAcquireAchievementInfo[i].GoalID;
					paramAcquireValue.Value = _kAcquireAchievementInfo[i].Value;
					try
					{
						sqlCmd.ExecuteNonQuery();
						ClientErrorMessage resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
						if( resultCode != ClientErrorMessage.SUCCESS )
						{
							Logger.ErrLog( "AcquireAchievement is Update Fail UserUID:{0}, GoalID:{1}, result_code:{2}", UserUID, _kAcquireAchievementInfo[i].GoalID, resultCode );
						}
						else
						{
							liUserAchievementInfo.Add( new DBAchievement( _kAcquireAchievementInfo[i].GoalID, Convert.ToInt32( paramCurrentValue.Value ) ) );
						}
					}
					catch( Exception ex )
					{
						Logger.ExceptionLog( ex );
						return new DBAchievement[0];
					}
				}
			}

			return liUserAchievementInfo.ToArray();
		}

        public DbResultCode GetAchievement(Int64 UserUID, out List<DBAchievement> _kAchievementList)
        {
            _kAchievementList = new List<DBAchievement>();

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ACHIEVEMENT_GetList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
							_kAchievementList.Add( new DBAchievement( (Int32)reader["GoalID"], (Int32)reader["GainedCount"] ) );
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
            }

            return result_code;
        }

        public DbResultCode LoadMissionList(Int64 _lUserUID, out List<DBMissionInfo> _kDBMissionInfoList)
        {
            _kDBMissionInfoList = new List<DBMissionInfo>();

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadMissionList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult   = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                kParamResult.Direction      = ParameterDirection.ReturnValue;
                kParamResult.Value          = (Int32)DbResultCode.SUCCESS;

                SqlParameter kParamUserUID  = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction     = ParameterDirection.Input;
                kParamUserUID.Value         = _lUserUID;

                SqlDataReader kReader       = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    kReader = sqlCmd.ExecuteReader();
                    if(kReader.HasRows)
                    {
                        while(kReader.Read())
                        {
                            DBMissionInfo kDBMissionInfo        = new DBMissionInfo();
                            kDBMissionInfo.MissionID            = (Int16)kReader["MissionID"];
							kDBMissionInfo.GainedCount			= (Int32)kReader["GainedCount"];
                            kDBMissionInfo.IsComplete           = (bool)kReader["IsComplete"];
							kDBMissionInfo.ResetDateTick		= ( (DateTime)kReader["ResetDate"] ).Ticks;

                            _kDBMissionInfoList.Add(kDBMissionInfo);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    kReader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(kParamResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_LoadMissionList is Fail UserUID: {0}", _lUserUID);
                    }
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GetAppFreindSocialInfo(Int64 userUID, String friendsUID, List<DBAppFriendsScoreMsgInfo> appFriendMsgList)
        {

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetAppFriendSocialInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramFriendsUID = sqlCmd.Parameters.Add("@FriendsUID", SqlDbType.VarChar, -1);
                paramFriendsUID.Direction = ParameterDirection.Input;
                paramFriendsUID.Value = friendsUID;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            appFriendMsgList.Add(new DBAppFriendsScoreMsgInfo(
                                (Int64)result["FriendUID"]
                                , ((DateTime)result["MsgGiftDate"]).Ticks
                                , ((DateTime)result["MsgPesterDate"]).Ticks
                                , ((DateTime)result["MsgFlowerDate"]).Ticks
                                ));
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
            }

            return result_code;
        }

		public ClientErrorMessage AddFriend( Int64 UserUID, Int64 FriendUID, Int32 RequestMaxCount )
        {
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return ClientErrorMessage.ERROR_NORMAL;
            }

			ClientErrorMessage result_code = ClientErrorMessage.ERROR_NORMAL;
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_AddFriend", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult		= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction			= ParameterDirection.ReturnValue;
				paramResult.Value				= (Int32)ClientErrorMessage.ERROR_NORMAL;

                SqlParameter paramUserUID		= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction			= ParameterDirection.Input;
                paramUserUID.Value				= UserUID;

                SqlParameter paramFriendsUID	= sqlCmd.Parameters.Add("@FriendUID", SqlDbType.BigInt);
                paramFriendsUID.Direction		= ParameterDirection.Input;
                paramFriendsUID.Value			= FriendUID;

                SqlParameter paramMaxCount		= sqlCmd.Parameters.Add("@MaxFriendCount", SqlDbType.Int);
                paramMaxCount.Direction			= ParameterDirection.Input;
                paramMaxCount.Value				= RequestMaxCount;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
            }

            return result_code;
        }

        public ClientErrorMessage ResponseFriend(Int64 UserUID, Int64 FriendUID, bool IsAccept, Int32 FriendMaxCount)
        {
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return ClientErrorMessage.ERROR_NORMAL;
            }

			ClientErrorMessage result_code = ClientErrorMessage.ERROR_NORMAL;
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.P_ResponseFriend", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult		= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction			= ParameterDirection.ReturnValue;
				paramResult.Value				= (Int32)ClientErrorMessage.ERROR_NORMAL;


                SqlParameter paramUserUID		= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction			= ParameterDirection.Input;
                paramUserUID.Value				= UserUID;

                SqlParameter paramFriendsUID	= sqlCmd.Parameters.Add("@FriendUID", SqlDbType.BigInt);
                paramFriendsUID.Direction		= ParameterDirection.Input;
                paramFriendsUID.Value			= FriendUID;

                SqlParameter paramIsAccept		= sqlCmd.Parameters.Add("@IsAceept", SqlDbType.Bit);
                paramIsAccept.Direction			= ParameterDirection.Input;
                paramIsAccept.Value				= IsAccept;

                SqlParameter paramMaxCount		= sqlCmd.Parameters.Add("@MaxFriendCount", SqlDbType.Int);
                paramMaxCount.Direction			= ParameterDirection.Input;
                paramMaxCount.Value				= FriendMaxCount;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
            }

            return result_code;
        }

		public ClientErrorMessage RemoveRequestFriend( Int64 UserUID, Int64 FriendUID )
        {
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return ClientErrorMessage.ERROR_NORMAL;
            }

			ClientErrorMessage result_code	= ClientErrorMessage.ERROR_NORMAL;
            using (SqlConnection dbConn		= Manager.CreateConnection() as SqlConnection)
            {
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_RemoveRequestFriend", dbConn );
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult		= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction			= ParameterDirection.ReturnValue;
				paramResult.Value				= (Int32)ClientErrorMessage.ERROR_NORMAL;


                SqlParameter paramUserUID		= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction			= ParameterDirection.Input;
                paramUserUID.Value				= UserUID;

                SqlParameter paramFriendsUID	= sqlCmd.Parameters.Add("@FriendUID", SqlDbType.BigInt);
                paramFriendsUID.Direction		= ParameterDirection.Input;
                paramFriendsUID.Value			= FriendUID;

                SqlDataReader result = null;
                try
                {
                    dbConn.Open();
                    result = sqlCmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
            }

            return result_code;
        }

		public ClientErrorMessage RemoveFriend( Int64 UserUID, Int64 FriendUID )
		{
			try
			{
				if( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return ClientErrorMessage.ERROR_NORMAL;
			}

			ClientErrorMessage result_code	= ClientErrorMessage.ERROR_NORMAL;
			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_RemoveFriend", dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult		= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction			= ParameterDirection.ReturnValue;
				paramResult.Value				= (Int32)ClientErrorMessage.ERROR_NORMAL;


				SqlParameter paramUserUID		= sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction			= ParameterDirection.Input;
				paramUserUID.Value				= UserUID;

				SqlParameter paramFriendsUID	= sqlCmd.Parameters.Add( "@FriendUID", SqlDbType.BigInt );
				paramFriendsUID.Direction		= ParameterDirection.Input;
				paramFriendsUID.Value			= FriendUID;
					
				SqlDataReader result = null;
				try
				{
					dbConn.Open();
					result = sqlCmd.ExecuteReader();
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}
        public DbResultCode GetKakaoFriendsInfo(Int64 _lUserUID, DataTable _kPlatformIDArray, out Int64[] _kFriendUIDArray)
        {
            List<Int64> kFriendUIDList = new List<Int64>();

            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUIDListByPlatformIDList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformIDList = sqlCmd.Parameters.Add("@FriendPlatformIDList", SqlDbType.Structured);
                paramPlatformIDList.Direction = ParameterDirection.Input;
                paramPlatformIDList.Value = _kPlatformIDArray;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            kFriendUIDList.Add((Int64)reader["FriendUID"]);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code == DbResultCode.SUCCESS)
                    {
                        _kFriendUIDArray = kFriendUIDList.ToArray();
                    }
                    else
                    {
                        Logger.ErrLog("P_GetUIDListByPlatformIDList is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                        for (Int32 i = 0; i < _kPlatformIDArray.Rows.Count; ++i )
                        {
                            Logger.ErrLog("Friend PlatformID: {0}", _kPlatformIDArray.Rows[i]);
                        }

                        _kFriendUIDArray = new Int64[0];
                    }
                }
            }

            return result_code;
        }

        public DbResultCode RemoveFriendList(Int64 _lUserUID, DataTable _kFriendUIDArray)
        {
            List<Int64> kFriendUIDList = new List<Int64>();

            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_RemoveKakaoFriendInGameFriend", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramPlatformIDList = sqlCmd.Parameters.Add("@FriendUIDList", SqlDbType.Structured);
                paramPlatformIDList.Direction = ParameterDirection.Input;
                paramPlatformIDList.Value = _kFriendUIDArray;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
						Logger.ErrLog( "P_RemoveKakaoFriendInGameFriend is Fail UserUID: {0}, Result = {1}", _lUserUID, result_code );
                    }
                }
            }

            return result_code;
        }

        public DbResultCode GetFriendInfoByName(String _strName, out DBUserSocialInfo _kSocialInfo, out List<DBItemLook> _kEquipItemList, out bool _bIsProfileImageOption)
        {
            _kSocialInfo              = GlobalHelper.InitDBUserSocialInfo(0);
            _kEquipItemList           = new List<DBItemLook>();
            _bIsProfileImageOption  = false;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendByName", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult                = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction                   = ParameterDirection.ReturnValue;
                paramResult.Value                       = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramName                  = sqlCmd.Parameters.Add( "@nickname", SqlDbType.NVarChar, ( int )MAXLENGTH.NICKNAME );
                paramName.Direction                     = ParameterDirection.Input;
                paramName.Value                         = _strName;

                SqlParameter paramCharacterID           = sqlCmd.Parameters.Add("@CharacterID", SqlDbType.Int);
                paramCharacterID.Direction              = ParameterDirection.Output;
                paramCharacterID.Value                  = _kSocialInfo.EquipInfo.CharacterID;

                SqlParameter paramMasteryLevel          = sqlCmd.Parameters.Add("@MasteryLevel", SqlDbType.TinyInt);
                paramMasteryLevel.Direction             = ParameterDirection.Output;
                paramMasteryLevel.Value                 = _kSocialInfo.EquipInfo.MasteryLevel;

                SqlParameter paramProfileImageOption    = sqlCmd.Parameters.Add("@ProfileImageOption", SqlDbType.Bit);
                paramProfileImageOption.Direction       = ParameterDirection.Output;
                paramProfileImageOption.Value           = _bIsProfileImageOption;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
							_kSocialInfo.UserInfo.UserUID				= (Int64)reader["UserUID"];
							_kSocialInfo.UserInfo.Name					= (String)reader["Nickname"];
							_kSocialInfo.UserInfo.Level					= (Byte)reader["Level"];
							_kSocialInfo.UserInfo.ExpPoint				= (Int32)reader["Exp"];
							_kSocialInfo.UserInfo.TotalGainSilver		= (Int32)reader["TotalAcqGameMoney"];
							_kSocialInfo.UserInfo.TotalUseGold			= (Int32)reader["TotalUseCash"];
							_kSocialInfo.ProfileInfo.profileImgURL		= (String)reader["PhotoImageURL"];
							_kSocialInfo.ProfileInfo.profileImgIndex	= (Int32)reader["BaseProfileNumber"];
							_kSocialInfo.ProfileInfo.Notice				= (String)reader["Notice"];
							_kSocialInfo.UserInfo.VipGrade				= (Byte)reader["VIPGrade"];
							_kSocialInfo.UserInfo.VIPPoint				= (float)(double)reader["VIPPoint"];
							_kSocialInfo.RaceInfo.WinCount				= (Int32)reader["WinCount"];
							_kSocialInfo.RaceInfo.PlayCount				= (Int32)reader["PlayCount"];
							_kSocialInfo.RaceInfo.ItemModeCount			= (Int32)reader["ItemModeCount"];
							_kSocialInfo.RaceInfo.SpeedModeCount		= (Int32)reader["SpeedModeCount"];
                            _kEquipItemList.Add(new DBItemLook((Int32)reader["ItemID"], (Byte)reader["UpgradeLevel"], (Int32)reader["AttachItemID"]));
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code == DbResultCode.SUCCESS)
                    {
                        _kSocialInfo.EquipInfo.CharacterID  = (Int32)paramCharacterID.Value;
                        _kSocialInfo.EquipInfo.MasteryLevel = (Byte)paramMasteryLevel.Value;
                        _bIsProfileImageOption              = (bool)paramProfileImageOption.Value;
                    }
                    else if(result_code == DbResultCode.DB_ERROR_FRIEND_NOT_FOUND_BY_NICKNAME)
                    {
                        Logger.DebugLog("GetFriendInfoByName Not Found Nickname: {0}", _strName);
                    }
                    else
                    {
                        Logger.ErrLog("GetFriendInfoByName Fail Ninkname: {0}, retCode: {1}", _strName, result_code);
                    }
                }
            }

            return result_code;
        }

        public DbResultCode GetUserInfo(Int64 UserUID, out DBUserSocialInfo SocialInfo, out List<DBItemLook> ItemList, out bool _bProfileImageOption)
        {
            SocialInfo              = GlobalHelper.InitDBUserSocialInfo(UserUID);
            ItemList                = new List<DBItemLook>();
            _bProfileImageOption    = false;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult                = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction                   = ParameterDirection.ReturnValue;
                paramResult.Value                       = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID               = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction                  = ParameterDirection.Input;
                paramUserUID.Value                      = UserUID;

                SqlParameter paramCharacterID           = sqlCmd.Parameters.Add("@CharacterID", SqlDbType.Int);
                paramCharacterID.Direction              = ParameterDirection.Output;
                paramCharacterID.Value                  = SocialInfo.EquipInfo.CharacterID;

                SqlParameter paramMasteryLevel          = sqlCmd.Parameters.Add("@MasteryLevel", SqlDbType.TinyInt);
                paramMasteryLevel.Direction             = ParameterDirection.Output;
                paramMasteryLevel.Value                 = SocialInfo.EquipInfo.MasteryLevel;

                SqlParameter paramProfileImageOption    = sqlCmd.Parameters.Add("@ProfileImageOption", SqlDbType.Bit);
                paramProfileImageOption.Direction       = ParameterDirection.Output;
                paramProfileImageOption.Value           = _bProfileImageOption;

                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            SocialInfo.UserInfo.UserUID             = (Int64)reader["UserUID"];
                            SocialInfo.UserInfo.Name                = (String)reader["Nickname"];
                            SocialInfo.UserInfo.Level               = (Byte)reader["Level"];
                            SocialInfo.UserInfo.ExpPoint            = (Int32)reader["Exp"];
                            SocialInfo.ProfileInfo.profileImgURL    = (String)reader["PhotoImageURL"];
                            SocialInfo.ProfileInfo.profileImgIndex  = (Int32)reader["BaseProfileNumber"];
                            SocialInfo.ProfileInfo.Notice           = (String)reader["Notice"];
                            SocialInfo.UserInfo.VipGrade            = (Byte)reader["VIPGrade"];
                            SocialInfo.UserInfo.VIPPoint            = (float)(double)reader["VIPPoint"];
                            SocialInfo.RaceInfo.WinCount            = (Int32)reader["WinCount"];
                            SocialInfo.RaceInfo.PlayCount           = (Int32)reader["PlayCount"];
                            SocialInfo.RaceInfo.ItemModeCount       = (Int32)reader["ItemModeCount"];
                            SocialInfo.RaceInfo.SpeedModeCount      = (Int32)reader["SpeedModeCount"];
                            SocialInfo.UserInfo.TotalGainSilver     = (Int32)reader["TotalAcqGameMoney"];
                            SocialInfo.UserInfo.TotalUseGold        = (Int32)reader["TotalUseCash"];
							SocialInfo.RaceInfo.ItemModeEloPoint = (Int32)reader["ItemModeEloPoint"];
							SocialInfo.RaceInfo.SpeedModeEloPoint = (Int32)reader["SpeedModeEloPoint"];
                            ItemList.Add(new DBItemLook((Int32)reader["ItemID"], (Byte)reader["UpgradeLevel"], (Int32)reader["AttachItemID"]));
                        }
                    }

                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code == DbResultCode.SUCCESS)
                    {
                        SocialInfo.EquipInfo.CharacterID    = (Int32)paramCharacterID.Value;
                        SocialInfo.EquipInfo.MasteryLevel   = (Byte)paramMasteryLevel.Value;
                        _bProfileImageOption                = (bool)paramProfileImageOption.Value;
                    }
                    else
                    {
                        Logger.ErrLog("GetUserInfo Fail UserUID: {0}, retCode: {1}", UserUID, result_code);
                    }
                }
            }

            return result_code;
        }

        public DbResultCode SetFriendRequestOption(Int64 UserUID, bool IsAccept)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_SetFriendOption", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramBoolReqeust = sqlCmd.Parameters.Add("@IsAccept", SqlDbType.Bit);
                paramBoolReqeust.Direction = ParameterDirection.Input;
                paramBoolReqeust.Value = IsAccept;

                SqlDataReader reader = null;
                List<DBUserSocialInfo> SocialInfoList = new List<DBUserSocialInfo>();

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
            }

            return result_code;
        }

		public ClientErrorMessage GetFriendInfoList( Int64 UserUID, Byte Type, out DBListSocialInfo[] FriendInfoArray, ref bool Request )
        {
            FriendInfoArray = new DBListSocialInfo[0];

            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                    sqlMgr.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return ClientErrorMessage.ERROR_BASE;
            }

			ClientErrorMessage result_code = ClientErrorMessage.ERROR_BASE;
            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.P_GetFriendInfoList", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult	= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction		= ParameterDirection.ReturnValue;
                paramResult.Value			= (Int32)ClientErrorMessage.ERROR_BASE;

                SqlParameter paramUserUID	= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction		= ParameterDirection.Input;
                paramUserUID.Value			= UserUID;

                SqlParameter paramType		= sqlCmd.Parameters.Add("@Type", SqlDbType.TinyInt);
                paramType.Direction			= ParameterDirection.Input;
                paramType.Value				= Type;

				SqlParameter paramRequest	= sqlCmd.Parameters.Add( "@RequestFlag", SqlDbType.Bit );
				paramRequest.Direction		= ParameterDirection.Output;

                SqlDataReader result = null;
                List<DBListSocialInfo> FriendInfoList = new List<DBListSocialInfo>();

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            DBListSocialInfo SocialInfo     = new DBListSocialInfo();
                            SocialInfo.UserUID              = (Int64)result["UserUID"];
                            SocialInfo.name                 = (String)result["Nickname"];
                            SocialInfo.level                = (Byte)result["Level"];
							SocialInfo.profileImgURL	    = (String)result["PhotoImageURL"];
                            SocialInfo.profileImgIndex      = (Int32)result["BaseProfileNumber"];
                            SocialInfo.VIPGrade             = (Byte)result["VipGrade"];
                            SocialInfo.logoutTicks          = ((DateTime)result["LogoutTime"]).Ticks;
                            SocialInfo.LastRefreshFatigueGoodsSentTime = ((DateTime)result["LastRefreshFatigueGoodsSentTime"]).Ticks;
                            SocialInfo.PlatformID           = String.Empty;
                            SocialInfo.ProfileImageOption   = (bool)result["ProfileImageOption"];
                            SocialInfo.KakaoMsgOption       = (bool)result["MsgOptionKakao"];
                            FriendInfoList.Add(SocialInfo);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );

					if( result_code == ClientErrorMessage.SUCCESS )
					{
						Request = Convert.ToBoolean( paramRequest.Value );
                        FriendInfoArray = FriendInfoList.ToArray();
                    }
					else
						Logger.ErrLog( "GetFriendInfoList Fail. retCode = {0}, UserUID = {1}, Type = {2}", result_code, UserUID, Type );
                }
            }

            return result_code;
        }

        public DbResultCode GetKakaoFriendInfoList(Int64 _lUserUID, out DBListSocialInfo[] _kFriendInfoArray, DataTable _kKakaoFriendUIDList)
        {
            _kFriendInfoArray = new DBListSocialInfo[0];

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetKakaoFriendInfoList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramFriendUIDList = sqlCmd.Parameters.Add("@FriendUIDList", SqlDbType.Structured);
                paramFriendUIDList.Direction = ParameterDirection.Input;
                paramFriendUIDList.Value = _kKakaoFriendUIDList;

                SqlDataReader result = null;
                List<DBListSocialInfo> FriendInfoList = new List<DBListSocialInfo>();

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            DBListSocialInfo SocialInfo = new DBListSocialInfo();
                            SocialInfo.UserUID          = (Int64)result["UserUID"];
                            SocialInfo.name             = (String)result["Nickname"];
                            SocialInfo.level            = (Byte)result["Level"];
							SocialInfo.profileImgURL	= (String)result["PhotoImageURL"];
                            SocialInfo.profileImgIndex  = (Int32)result["BaseProfileNumber"];
                            SocialInfo.VIPGrade         = (Byte)result["VipGrade"];
                            SocialInfo.logoutTicks      = ((DateTime)result["LogoutTime"]).Ticks;
                            SocialInfo.LastRefreshFatigueGoodsSentTime = ((DateTime)result["LastRefreshFatigueGoodsSentTime"]).Ticks;
                            SocialInfo.PlatformID       = (String)result["PlatformID"];
                            SocialInfo.KakaoMsgOption   = (bool)result["MsgOptionKakao"];
                            FriendInfoList.Add(SocialInfo);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code == DbResultCode.SUCCESS)
                    {
                        _kFriendInfoArray = FriendInfoList.ToArray();
                    }

                    else
                    {
                        Logger.ErrLog("P_GetKakaoFriendInfoList is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                        _kFriendInfoArray = new DBListSocialInfo[0];
                    }
                }
            }

            return result_code;
        }

		public DbResultCode GetFriendAndInviteCount(Int64 UserUID, out Int16 _nFriendCnt, out Int16 _nInviteCnt)
        {
			_nFriendCnt = 0;
			_nInviteCnt = 0;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendAndInviteCnt", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramFriendCnt = sqlCmd.Parameters.Add("@FriendCnt", SqlDbType.SmallInt);
                paramFriendCnt.Direction = ParameterDirection.Output;
                paramFriendCnt.Value = (Int16)_nFriendCnt;

                SqlParameter paramInviteCnt = sqlCmd.Parameters.Add("@InviteCnt", SqlDbType.SmallInt);
                paramInviteCnt.Direction = ParameterDirection.Output;
                paramInviteCnt.Value = (Int16)_nInviteCnt;

				try
                {
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if( result_code == DbResultCode.SUCCESS )
					{
						_nFriendCnt = (Int16)paramFriendCnt.Value;
						_nInviteCnt = (Int16)paramInviteCnt.Value;
					}
                    
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
               
            }

            return result_code;
        }

		public DbResultCode AddTaiwanIwReceipt(Int64 lUserUID, Int64 lOrderID, Int64 lPlayerID, Int32 nSaleID, int nCashIncrAmount, byte byRegistedReceiptStatus)
		{
			DbResultCode kDbResult = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = this.GetDB(lUserUID);
				if (sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}
			catch (System.Exception ex)
			{
				Logger.ErrLog("KMCenter.SQLGameHandler.RegistReceipt fail > could not open sql manager");
				Logger.ExceptionLog(ex);
				kDbResult = DbResultCode.DB_ERROR;
				return kDbResult;
			}

			using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.PAYMENT_AddTaiwanIwReceipt", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramOrderID = sqlCmd.Parameters.Add("@OrderID", SqlDbType.BigInt);
				paramOrderID.Direction = ParameterDirection.Input;
				paramOrderID.Value = lOrderID;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = lUserUID;

				SqlParameter paramPlayerID = sqlCmd.Parameters.Add("@PlayerID", SqlDbType.BigInt);
				paramPlayerID.Direction = ParameterDirection.Input;
				paramPlayerID.Value = lPlayerID;

				SqlParameter paramInAppShopID = sqlCmd.Parameters.Add("@InAppShopID", SqlDbType.Int);
				paramInAppShopID.Direction = ParameterDirection.Input;
				paramInAppShopID.Value = nSaleID;

				SqlParameter paramCashIncrAmount = sqlCmd.Parameters.Add("@ModifyAmount", SqlDbType.Int);
				paramCashIncrAmount.Direction = ParameterDirection.Input;
				paramCashIncrAmount.Value = nCashIncrAmount;

				SqlParameter paramRegistedStatus = sqlCmd.Parameters.Add("@RegistedStatus", SqlDbType.TinyInt);
				paramRegistedStatus.Direction = ParameterDirection.Input;
				paramRegistedStatus.Value = byRegistedReceiptStatus;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					kDbResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
				}
				catch (System.Exception ex)
				{
					Logger.ExceptionLog(ex);
					kDbResult = DbResultCode.DB_ERROR;
				}
			}

			return kDbResult;
		}

		public DbResultCode SignTaiwanIwReceipt(Int64 lUserUID, Int64 lOrderID, Int64 lPlayerID, Int32 nSaleID, byte byRegistedReceiptStatus, byte bySignedReceiptStatus, String strTransactionID, String strBuyingID, String strAppItemID)
		{
			DbResultCode kDbResult = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = this.GetDB(lUserUID);
				if (sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}
			catch (System.Exception ex)
			{
				Logger.ErrLog("KMCenter.SQLGameHandler.SignTaiwanIwReceipt fail > could not open sql manager");
				Logger.ExceptionLog(ex);
				kDbResult = DbResultCode.DB_ERROR;
				return kDbResult;
			}

			using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.PAYMENT_SignTaiwanIwReceipt", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramOrderID = sqlCmd.Parameters.Add("@OrderID", SqlDbType.BigInt);
				paramOrderID.Direction = ParameterDirection.Input;
				paramOrderID.Value = lOrderID;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = lUserUID;

				SqlParameter paramPlayerID = sqlCmd.Parameters.Add("@PlayerID", SqlDbType.BigInt);
				paramPlayerID.Direction = ParameterDirection.Input;
				paramPlayerID.Value = lPlayerID;

				SqlParameter paramInAppShopID = sqlCmd.Parameters.Add("@InAppShopID", SqlDbType.Int);
				paramInAppShopID.Direction = ParameterDirection.Input;
				paramInAppShopID.Value = nSaleID;

				SqlParameter paramRegistedStatus = sqlCmd.Parameters.Add("@RegistedStatus", SqlDbType.TinyInt);
				paramRegistedStatus.Direction = ParameterDirection.Input;
				paramRegistedStatus.Value = byRegistedReceiptStatus;

				SqlParameter paramSignedStatus = sqlCmd.Parameters.Add("@SignedStatus", SqlDbType.TinyInt);
				paramSignedStatus.Direction = ParameterDirection.Input;
				paramSignedStatus.Value = bySignedReceiptStatus;

				SqlParameter paramTransactionID = sqlCmd.Parameters.Add("@TransactionID", SqlDbType.VarChar);
				paramTransactionID.Direction = ParameterDirection.Input;
				paramTransactionID.Value = strTransactionID;

				SqlParameter paramBuyingID = sqlCmd.Parameters.Add("@BID", SqlDbType.VarChar);
				paramBuyingID.Direction = ParameterDirection.Input;
				paramBuyingID.Value = strBuyingID;

				SqlParameter paramAppItemID = sqlCmd.Parameters.Add("@AppItemID", SqlDbType.VarChar);
				paramAppItemID.Direction = ParameterDirection.Input;
				paramAppItemID.Value = strAppItemID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					kDbResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
				}
				catch (System.Exception ex)
				{
					Logger.ExceptionLog(ex);
					kDbResult = DbResultCode.DB_ERROR;
				}
			}

			return kDbResult;
		}

		public DbResultCode ExpireTaiwanIwReceipt(Int64 lUserUID, Int64 lOrderID, Int64 lPlayerID, Int32 nSaleID, int nCashIncrAmount, byte byGoodsChangeCause, byte bySignedReceiptStatus, byte byExpiredReceiptStatus)
		{
			DbResultCode kDbResult = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = this.GetDB(lUserUID);
				if (sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}
			catch (System.Exception ex)
			{
				Logger.ErrLog("KMCenter.SQLGameHandler.ExpireTaiwanIwReceipt fail > could not open sql manager");
				Logger.ExceptionLog(ex);
				kDbResult = DbResultCode.DB_ERROR;
				return kDbResult;
			}

			using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.PAYMENT_ExpireTaiwanIwReceipt", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramOrderID = sqlCmd.Parameters.Add("@OrderID", SqlDbType.BigInt);
				paramOrderID.Direction = ParameterDirection.Input;
				paramOrderID.Value = lOrderID;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = lUserUID;

				SqlParameter paramPlayerID = sqlCmd.Parameters.Add("@PlayerID", SqlDbType.BigInt);
				paramPlayerID.Direction = ParameterDirection.Input;
				paramPlayerID.Value = lPlayerID;

				SqlParameter paramInAppShopID = sqlCmd.Parameters.Add("@InAppShopID", SqlDbType.Int);
				paramInAppShopID.Direction = ParameterDirection.Input;
				paramInAppShopID.Value = nSaleID;

				SqlParameter paramQuantity = sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int);
				paramQuantity.Direction = ParameterDirection.Input;
				paramQuantity.Value = nCashIncrAmount;

				SqlParameter paramGoodsChangeCause = sqlCmd.Parameters.Add("@GoodsChangeCause", SqlDbType.TinyInt);
				paramGoodsChangeCause.Direction = ParameterDirection.Input;
				paramGoodsChangeCause.Value = byGoodsChangeCause;

				SqlParameter paramSignedStatus = sqlCmd.Parameters.Add("@SignedStatus", SqlDbType.TinyInt);
				paramSignedStatus.Direction = ParameterDirection.Input;
				paramSignedStatus.Value = bySignedReceiptStatus;

				SqlParameter paramExpiredStatus = sqlCmd.Parameters.Add("@ExpiredStatus", SqlDbType.TinyInt);
				paramExpiredStatus.Direction = ParameterDirection.Input;
				paramExpiredStatus.Value = byExpiredReceiptStatus;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					kDbResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
				}
				catch (System.Exception ex)
				{
					Logger.ExceptionLog(ex);
					kDbResult = DbResultCode.DB_ERROR;
				}
			}

			return kDbResult;
		}

        public DbResultCode IsNewRequestCheck(Int64 UserUID, out bool IsNewRequest)
        {
            IsNewRequest = false;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_IsNewRequest", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramNewRequest = sqlCmd.Parameters.Add("@NewRequest", SqlDbType.Bit);
                paramNewRequest.Direction = ParameterDirection.Output;
                paramNewRequest.Value = IsNewRequest;

                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if (result_code == DbResultCode.SUCCESS)
                    {
                        IsNewRequest = (bool)paramNewRequest.Value;
                    }

                }
            }

            return result_code;
        }

        public DbResultCode GetTimeAttackRecord(Int64 UserUID, out DBTimeAttackRecord[] RecordArray)
        {
            RecordArray = new DBTimeAttackRecord[0];

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetTimeAttackRecord", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlDataReader result = null;
                List<DBTimeAttackRecord> RecordList = new List<DBTimeAttackRecord>();

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            DBTimeAttackRecord Record = new DBTimeAttackRecord();
                            Record.EquipInfo = new DBSocialEquipInfo(0, 0, 0, 0, 0, 0, 0, 0, 0);
                            Record.TrackID = (Int32)result["TrackID"];
                            Record.RaceRecordTime = (float)result["BestRaceRecord"];
                            Record.EquipInfo.CharacterID = (Int32)result["CharacterID"];
                            Record.EquipInfo.carBodyItemID= (Int32)result["BodyItemID"];
                            Record.EquipInfo.carBodyUpgradeLevel = (Byte)result["BodyUpgradeLevel"];
                            Record.EquipInfo.carSpoilerItemID = (Int32)result["SpoilerItemID"];
                            Record.EquipInfo.carSpoilerUpgradeLevel = (Byte)result["SpoilerUpgradeLevel"];
                            Record.EquipInfo.carWheelItemID= (Int32)result["WheelItemID"];
                            Record.EquipInfo.carWheelUpgradeLevel = (Byte)result["WheelUpgradeLevel"];
                            RecordList.Add(Record);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if ( result_code == DbResultCode.SUCCESS )
                    {
                        RecordArray = RecordList.ToArray();
                    }
                    else
                        Logger.ErrLog( "GetTimeAttackRecord Fail : UserUID = {0}, retCode = {1}", UserUID, result_code );
                }
            }

            return result_code;
        }

        public DbResultCode UpdateTimeAttackRecord(Int64 UserUID, Int32 TrackID, float RaceTime, DBSocialEquipInfo EquipInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateTimeAttackRecord", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramTrackID = sqlCmd.Parameters.Add("@TrackID", SqlDbType.Int);
                paramTrackID.Direction = ParameterDirection.Input;
                paramTrackID.Value = TrackID;

                SqlParameter paramRaceTime = sqlCmd.Parameters.Add("@RaceTime", SqlDbType.Float);
                paramRaceTime.Direction = ParameterDirection.Input;
                paramRaceTime.Value = RaceTime;

                SqlParameter paramCharacterID = sqlCmd.Parameters.Add("@CharacterID", SqlDbType.Int);
                paramCharacterID.Direction = ParameterDirection.Input;
                paramCharacterID.Value = EquipInfo.CharacterID;

                SqlParameter paramBodyItemID = sqlCmd.Parameters.Add("@BodyItemID", SqlDbType.Int);
                paramBodyItemID.Direction = ParameterDirection.Input;
                paramBodyItemID.Value = EquipInfo.carBodyItemID;

                SqlParameter paramBodyUpgradeLevel = sqlCmd.Parameters.Add("@BodyUpgradeLevel", SqlDbType.TinyInt);
                paramBodyUpgradeLevel.Direction = ParameterDirection.Input;
                paramBodyUpgradeLevel.Value = EquipInfo.carBodyUpgradeLevel;

                SqlParameter paramSpoilerItemID = sqlCmd.Parameters.Add("@SpoilerItemID", SqlDbType.Int);
                paramSpoilerItemID.Direction = ParameterDirection.Input;
                paramSpoilerItemID.Value = EquipInfo.carSpoilerItemID;

                SqlParameter paramSpoilerUpgradeLevel = sqlCmd.Parameters.Add("@SpoilerUpgradeLevel", SqlDbType.TinyInt);
                paramSpoilerUpgradeLevel.Direction = ParameterDirection.Input;
                paramSpoilerUpgradeLevel.Value = EquipInfo.carSpoilerUpgradeLevel;

                SqlParameter paramWheelItemID = sqlCmd.Parameters.Add("@WheelItemID", SqlDbType.Int);
                paramWheelItemID.Direction = ParameterDirection.Input;
                paramWheelItemID.Value = EquipInfo.carWheelItemID;

                SqlParameter paramWheelUpgradeLevel = sqlCmd.Parameters.Add("@WheelUpgradeLevel", SqlDbType.TinyInt);
                paramWheelUpgradeLevel.Direction = ParameterDirection.Input;
                paramWheelUpgradeLevel.Value = EquipInfo.carWheelUpgradeLevel;

                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if (result.HasRows)
                    {
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
            }

            return result_code;
        }

        public DbResultCode GetTimeAttackRank( ref Int32 _nResult, Int64 _nUserUID, Int32 _nTrackID, Int32 _nMaxCount, ref List<KMTimeAttackRecord> _listTimeAttack )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _nUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }

            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.GetTimeAttackRecordRank", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                // Input
                SqlParameter paramTrackID = sqlCmd.Parameters.Add( "@TrackID", SqlDbType.Int );
                paramTrackID.Direction = ParameterDirection.Input;
                paramTrackID.Value = _nTrackID;

                SqlParameter paramMaxCount = sqlCmd.Parameters.Add( "@MaxCount", SqlDbType.Int );
                paramMaxCount.Direction = ParameterDirection.Input;
                paramMaxCount.Value = _nMaxCount;

     
                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if ( result.HasRows )
                    {
                        while (result.Read())
                        {
                            _listTimeAttack.Add( new KMTimeAttackRecord( new KMTimeAttackResult( ( String )result["Nickname"]
                                                                                               , ( float )result["BestRaceRecord"] )
                                                                       , new KMSocialEquipInfo( ( Int32 )result["CharacterID"]
                                                                                              , ( Int32 )result["BodyItemID"]
                                                                                              , ( Byte )result["BodyUpgradeLevel"]
                                                                                              , ( Int32 )result["SpoilerItemID"]
                                                                                              , ( Byte )result["SpoilerUpgradeLevel"]
                                                                                              , ( Int32 )result["WheelItemID"]
                                                                                              , ( Byte )result["WheelUpgradeLevel"]
                                                                                              , 0 
                                                                                              , 0)
                                                                        )
                                               );
                                                                                              
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( result != null)
                    {
                        result.Close();
                        _nResult = Convert.ToInt32( paramResult.Value );
                        if ( _nResult != ( Int32 )ServerErrorMessage.SUCCESS )
                        {
                            Logger.ErrLog( "SQL GetTimeAttackRank fail  codeResult : {0}", _nResult );
                            result_code = DbResultCode.DB_ERROR;
                        }
                        else
                            result_code = DbResultCode.SUCCESS;
                    }
                    else
                        result_code = DbResultCode.DB_ERROR;
                }
            }

            return result_code;
        }

        public DbResultCode UpdateTimeAttackRank(ref Int32 _nResult, Int64 _nUserUID, Int32 _nTrackID, float _fRackTime, Int32 _nMaxCount, ref List<DBTimeAttackResult> _listTimeAttack)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _nUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }

            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.UpdateTimeAttackRecordRank", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                // Input
                SqlParameter paramUserID = sqlCmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUserID.Direction = ParameterDirection.Input;
                paramUserID.Value = _nUserUID;

                SqlParameter paramTrackID = sqlCmd.Parameters.Add( "@TrackID", SqlDbType.Int );
                paramTrackID.Direction = ParameterDirection.Input;
                paramTrackID.Value = _nTrackID;

                SqlParameter paramTimeRecord = sqlCmd.Parameters.Add( "@TimeRecord", SqlDbType.Float );
                paramTimeRecord.Direction = ParameterDirection.Input;
                paramTimeRecord.Value = _fRackTime;

                SqlParameter paramMaxCount = sqlCmd.Parameters.Add( "@MaxCount", SqlDbType.Int );
                paramMaxCount.Direction = ParameterDirection.Input;
                paramMaxCount.Value = _nMaxCount;


                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if ( result.HasRows )
                    {
                        while ( result.Read() )
                        {
                            _listTimeAttack.Add(new DBTimeAttackResult((String)result["Nickname"], ( float )result["BestRaceRecord"], (Int64)result["UserUID"]));
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( result != null )
                    {
                        result.Close();
                        _nResult = Convert.ToInt32( paramResult.Value );
                        if ( _nResult == ( Int32 )ServerErrorMessage.SUCCESS )
                            result_code = DbResultCode.SUCCESS;
                        else
                        {
                            Logger.ErrLog( "SQL UpdateTimeAttackRank fail  codeResult : {0}", _nResult );
                            result_code = DbResultCode.DB_ERROR;
                        }
                    }
                    else
                        result_code = DbResultCode.DB_ERROR;
                }
            }

            return result_code;
        }

        public DbResultCode GetTimeAttackRankSimple( ref Int32 _nResult, Int64 _nUserUID, Int32 _nTrackID, Int32 _nMaxCount, ref List<DBTimeAttackResult> _listTimeAttack )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _nUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }

            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.GetTimeAttackRankSimple", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                // Input
                SqlParameter paramTrackID = sqlCmd.Parameters.Add( "@TrackID", SqlDbType.Int );
                paramTrackID.Direction = ParameterDirection.Input;
                paramTrackID.Value = _nTrackID;

                SqlParameter paramMaxCount = sqlCmd.Parameters.Add( "@MaxCount", SqlDbType.Int );
                paramMaxCount.Direction = ParameterDirection.Input;
                paramMaxCount.Value = _nMaxCount;

                SqlDataReader result = null;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();

                    result = sqlCmd.ExecuteReader();
                    if ( result.HasRows )
                    {
                        while ( result.Read() )
                        {
                            _listTimeAttack.Add(new DBTimeAttackResult((String)result["Nickname"], (float)result["BestRaceRecord"], (Int64)result["UserUID"]));
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( result != null )
                    {
                        result.Close();
                        _nResult = Convert.ToInt32( paramResult.Value );
                        if ( _nResult == ( Int32 )ServerErrorMessage.SUCCESS )
                            result_code = DbResultCode.SUCCESS;
                        else
                        {
                            Logger.ErrLog( "SQL GetTimeAttackRankSimple fail  codeResult : {0}", _nResult );
                            result_code = DbResultCode.DB_ERROR;
                        }
                    }
                    else
                        result_code = DbResultCode.DB_ERROR;
                }
            }

            return result_code;
        }


		public ClientErrorMessage UpdateTotalUseCash( Int64 UserUID, Int32 UseCash )
        {
			ClientErrorMessage resultCode = ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( UserUID );
				if( false == sqlMgr.IsOpen() )
					sqlMgr.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return resultCode;
			}

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_UpdateTotalUseCash", dbConn );
                sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction		= ParameterDirection.ReturnValue;
                paramResult.Value			= (Int32)DbResultCode.DB_ERROR;


				SqlParameter paramUserUID	= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
                paramUserUID.Direction		= ParameterDirection.Input;
                paramUserUID.Value			= UserUID;

				SqlParameter paramUseGold	= sqlCmd.Parameters.Add( "@in_UseCash", SqlDbType.Int );
                paramUseGold.Direction		= ParameterDirection.Input;
                paramUseGold.Value			= UseCash;

                try
                {
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

			return resultCode;
        }

		public ClientErrorMessage UpdateTotalAcquireGameMoney( Int64 UserUID, Int32 AcquireGameMoney )
        {
			ClientErrorMessage resultCode = ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( UserUID );
				if( false == sqlMgr.IsOpen() )
					sqlMgr.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return resultCode;
			}

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_UpdateTotalAcquireGameMoney", dbConn );
                sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult		= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction			= ParameterDirection.ReturnValue;
                paramResult.Value				= (Int32)DbResultCode.DB_ERROR;


				SqlParameter paramUserUID		= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
                paramUserUID.Direction			= ParameterDirection.Input;
                paramUserUID.Value				= UserUID;

				SqlParameter paramAcqGameMoney	= sqlCmd.Parameters.Add( "@in_AcquireGameMoney", SqlDbType.Int );
				paramAcqGameMoney.Direction		= ParameterDirection.Input;
				paramAcqGameMoney.Value			= AcquireGameMoney;

                try
                {
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

			return resultCode;
        }

        public DbResultCode UpdateEquipItem(Int64 _lUserUID, DataTable _kEquipItemUpdateList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }
            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEM_UpdateEquipItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@EquipItemUpdateList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kEquipItemUpdateList;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if(result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("ITEM_UpdateEquipItem is Fail UserUID = {0} ResultCode = {1}", _lUserUID, result_code);
                }
            }
            return result_code;
        }

        public DbResultCode UpdateGachaItemSlot(Int64 userUID, Int64 gachaItem, Byte gachaCount)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateGachaItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramGachaItem = sqlCmd.Parameters.Add("@gachaItem", SqlDbType.BigInt);
                paramGachaItem.Direction = ParameterDirection.Input;
                paramGachaItem.Value = gachaItem;

                SqlParameter paramGachaCount = sqlCmd.Parameters.Add("@gachaCount", SqlDbType.TinyInt);
                paramGachaCount.Direction = ParameterDirection.Input;
                paramGachaCount.Value = gachaCount;


                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }
            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateSpeedModeGachaItemSlot(Int64 userUID, Int32 gachaItem, Byte gachaCount)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateSpeedModeGachaItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramGachaItem = sqlCmd.Parameters.Add("@gachaItem", SqlDbType.Int);
                paramGachaItem.Direction = ParameterDirection.Input;
                paramGachaItem.Value = gachaItem;

                SqlParameter paramGachaCount = sqlCmd.Parameters.Add("@gachaCount", SqlDbType.TinyInt);
                paramGachaCount.Direction = ParameterDirection.Input;
                paramGachaCount.Value = gachaCount;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }
            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateInviteFriend(Int64 userUID, out Byte toDayInviteCount, out Int32 totalInviteCount)
        {
            toDayInviteCount = 0;
            totalInviteCount = 0;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateInviteFriend", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramToDayInviteCount = sqlCmd.Parameters.Add("@ToDayInviteCount", SqlDbType.TinyInt);
                paramToDayInviteCount.Direction = ParameterDirection.Output;
                paramToDayInviteCount.Value = toDayInviteCount;

                SqlParameter paramTotalInviteCount = sqlCmd.Parameters.Add("@TotalInviteCount", SqlDbType.Int);
                paramTotalInviteCount.Direction = ParameterDirection.Output;
                paramTotalInviteCount.Value = totalInviteCount;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)paramResult.Value;
                    if (result_code == DbResultCode.SUCCESS)
                    {
                        toDayInviteCount = (Byte)paramToDayInviteCount.Value;
                        totalInviteCount = (Int32)paramTotalInviteCount.Value;
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UserFirstBuy(Int64 userUID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateIsFirstBuy", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode EnterGame(Int64 userUID, String platformID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_EnterGame", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = userUID;

                SqlParameter paramPlatformID = sqlCmd.Parameters.Add("@platformID", SqlDbType.VarChar, 40);
                paramPlatformID.Direction = ParameterDirection.Input;
                paramPlatformID.Value = platformID;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateItemForAttachItem(Int64 userUID, Int64 itemUID, Int32 attachItemID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(userUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateItemForAttachItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = userUID;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@itemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = itemUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@attachItemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = attachItemID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("P_UpdateItemForAttachItem Fail UserUID = {0} itemUID = {1} attachItemID = {2}", userUID, itemUID, attachItemID);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateEquip(Int64 userUID, Int64 itemUID, bool isEquip)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_UpdateEquipItem", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd);

                DBDecorator.InParam(paramList, "@userUID", userUID);
                DBDecorator.InParam(paramList, "@itemUID", itemUID);
                DBDecorator.InParam(paramList, "@isEquip", isEquip);
                result_code = DBDecorator.Execute(cmd);
            }
            return DbResultCode.SUCCESS;
        }

        public DbResultCode LoadDeliveryItem(Int64 userUID, out List<DBDeliveryItem> deliveryItemList)
        {
            deliveryItemList = new List<DBDeliveryItem>();
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_LoadDeliveryItem", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd);

                DBDecorator.InParam(paramList, "@userUID", userUID);
                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            DBDeliveryItem item = new DBDeliveryItem()
                            {
                                deliveryID = (Int64)reader["ID"],
                                itemUID = (Int64)reader["ItemUID"],
                                endDate = ((DateTime)reader["EndDate"]).Ticks
                            };

                            deliveryItemList.Add(item);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramList[0].Value);
                }
            }
            return result_code;
        }

        public DbResultCode DeliveryItem(Int64 userUID, Int64 itemUID, Int32 itemID, Byte nextLevel, Int64 endDateTime, out Int64 deliveryID)
        {
            deliveryID = 0;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_DeliveryItem", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd);

                DBDecorator.InParam(paramList, "@targetItemUID", itemUID);
                DBDecorator.InParam(paramList, "@userUID", userUID);
                DBDecorator.InParam(paramList, "@nextLevel", nextLevel);
                DBDecorator.InParam(paramList, "@endDateTime", new DateTime(endDateTime));
                var paramDeliveryID = DBDecorator.InParam(paramList, "@deliveryID", deliveryID);
                result_code = DBDecorator.Execute(cmd);
                if(result_code == DbResultCode.SUCCESS)
                    deliveryID = Convert.ToInt64(paramDeliveryID.Value);
            }
            return result_code;
        }

        public DbResultCode ImmidiateUpgradeItem(long userUID, Int64 deliveryID, Int64 itemUID, out Byte nextLevel)
        {
            nextLevel = 0;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_ImmidiateUpgradeItem", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd);

                DBDecorator.InParam(paramList, "@userUID",      userUID);
                DBDecorator.InParam(paramList, "@deliveryID",   deliveryID);
                DBDecorator.InParam(paramList, "@itemUID",      itemUID);
                
                var paramNextLevel = DBDecorator.InParam(paramList, "@nextLevel", nextLevel, ParameterDirection.Output);
                result_code = DBDecorator.Execute(cmd);
                if (result_code == DbResultCode.SUCCESS)
                    nextLevel = Convert.ToByte(paramNextLevel.Value);
                
            }
            return result_code;
        }

        public DbResultCode ReceptUpgradeItem(long userUID,  Int64 deliveryID, Int64 itemUID, out Byte nextLevel, out DateTime endDateTime)
        {
            nextLevel = 0;
            endDateTime = DateTime.Now;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {

                SqlCommand cmd = DBDecorator.CreateCommand("dbo.P_ReceptUpgradeItem", dbConn);
                SqlParameterCollection paramList = DBDecorator.Return(cmd);

                DBDecorator.InParam(paramList, "@userUID", userUID);
                DBDecorator.InParam(paramList, "@deliveryID", deliveryID);
                DBDecorator.InParam(paramList, "@itemUID", itemUID);

                var paramNextLevel = DBDecorator.InParam(paramList, "@nextLevel", nextLevel, ParameterDirection.Output);
                var paramEndDateTime = DBDecorator.InParam(paramList, "@endDateTime", nextLevel, ParameterDirection.Output);
                result_code = DBDecorator.Execute(cmd);
                if (result_code == DbResultCode.SUCCESS)
                {
                    nextLevel = Convert.ToByte(paramNextLevel.Value);
                    endDateTime = Convert.ToDateTime(paramEndDateTime.Value);
                }
            }
            return result_code;
        }

        public DbResultCode RankingWeeklyRewardCheck( Int64 _nUserID, ref Int32 _nNewPost )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.RankingWeeklyRewardCheck", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Reture
                    SqlParameter paramResult    = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction       = ParameterDirection.ReturnValue;
                    paramResult.Value           = ( Int32 )DbResultCode.DB_ERROR;

                    // InPut
                    SqlParameter paramUserID    = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                    paramUserID.Direction       = ParameterDirection.Input;
                    paramUserID.Value           = _nUserID;

                    SqlParameter paramPostType  = cmd.Parameters.Add( "@PostType", SqlDbType.TinyInt );
                    paramPostType.Direction     = ParameterDirection.Input;
                    paramPostType.Value         = KMPostType.REWARD;

                    // OutPut
                    SqlParameter paramNewPost = cmd.Parameters.Add( "@NewPost", SqlDbType.Int );
                    paramNewPost.Direction = ParameterDirection.Output;
                    paramNewPost.Value = _nNewPost;

                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "RankingWeeklyRewardCheck Fail UserID = {0}, ResultCode : " + ( ( DbResultCode )result_code ).ToString(), _nUserID );
                        return result_code;
                    }

                    _nNewPost = Convert.ToInt32( paramNewPost.Value );

                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode RankingMonthlyRewardCheck( Int64 _nUserID, ref Int32 _nNewPost )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.RankingMonthlyRewardCheck", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Reture
                    SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    // InPut
                    SqlParameter paramUserID = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                    paramUserID.Direction = ParameterDirection.Input;
                    paramUserID.Value = _nUserID;

                    SqlParameter paramPostType = cmd.Parameters.Add( "@PostType", SqlDbType.TinyInt );
                    paramPostType.Direction = ParameterDirection.Input;
                    paramPostType.Value = KMPostType.REWARD;

                    // OutPut
                    SqlParameter paramNewPost = cmd.Parameters.Add( "@NewPost", SqlDbType.Int );
                    paramNewPost.Direction = ParameterDirection.Output;
                    paramNewPost.Value = _nNewPost;

                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "RankingMonthlyRewardCheck Fail UserID = {0}, ResultCode : " + ( ( DbResultCode )result_code ).ToString(), _nUserID );
                        return result_code;
                    }

                    _nNewPost = Convert.ToInt32( paramNewPost.Value );

                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode RankingCalculate( Int32 _nCollectPeriodType )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.RankingCalculate", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Reture
                    SqlParameter paramResult            = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction               = ParameterDirection.ReturnValue;
                    paramResult.Value                   = ( Int32 )DbResultCode.DB_ERROR;

                    // InPut
                    SqlParameter paramCollectPeriodType = cmd.Parameters.Add( "@CollectPeriodType", SqlDbType.Int );
                    paramCollectPeriodType.Direction    = ParameterDirection.Input;
                    paramCollectPeriodType.Value        = _nCollectPeriodType;

                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "RankingCalculateToDB Fail CollectPeriodType = {0}, ResultCode : " + (( DbResultCode )result_code).ToString(), _nCollectPeriodType );
                        return result_code;
                    }
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode RewardUpdateToDB()
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.RewardTableUpdate", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Reture
                    SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "RewardUpdateToDB Fail CollectPeriodType : codeResult = {0}", ( Int32 )result_code );
                        return result_code;
                    }
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode RankElementPointUpdateWeekly( Int64 _nUserUid, Int32 _nElementType, float _nElementPoint, Int64 _nWeekTime,
            ref float _nElementValue, ref String _strNickName, ref Byte _nUserLevel, ref Byte _nVIPGrade, ref String _strProfileURL, ref int _nProfileNumber, ref int _nRetCode )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.RankRecordUpdate_Weekly", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Input
                    SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    SqlParameter paramUnitUID = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                    paramUnitUID.Direction = ParameterDirection.Input;
                    paramUnitUID.Value = _nUserUid;

                    SqlParameter paramElementType = cmd.Parameters.Add( "@RankElementType", SqlDbType.Int );
                    paramElementType.Direction = ParameterDirection.Input;
                    paramElementType.Value = _nElementType;

                    SqlParameter paramElementPoint = cmd.Parameters.Add( "@RankElementPoint", SqlDbType.Float );
                    paramElementPoint.Direction = ParameterDirection.Input;
                    paramElementPoint.Value = _nElementPoint;

                    SqlParameter paramWeekTime = cmd.Parameters.Add( "@WeekTime", SqlDbType.DateTime );
                    paramWeekTime.Direction = ParameterDirection.Input;
                    paramWeekTime.Value = new DateTime( _nWeekTime );

                    // Output
                    SqlParameter paramResultPoint = cmd.Parameters.Add( "@ResultPoint", SqlDbType.Float );
                    paramResultPoint.Direction = ParameterDirection.Output;
                    paramResultPoint.Value = _nElementValue;

                    SqlParameter paramState = cmd.Parameters.Add( "@State", SqlDbType.Int );
                    paramState.Direction = ParameterDirection.Output;
                    paramState.Value = _nRetCode;

                    SqlParameter paramNickName = cmd.Parameters.Add( "@NickName", SqlDbType.NVarChar, ( int )MAXLENGTH.NICKNAME );
                    paramNickName.Direction = ParameterDirection.Output;
                    paramNickName.Value = _strNickName;

                    SqlParameter paramUserLevel = cmd.Parameters.Add( "@UserLevel", SqlDbType.Int );
                    paramUserLevel.Direction = ParameterDirection.Output;
                    paramUserLevel.Value = _nUserLevel;

                    SqlParameter paramVIPGrade = cmd.Parameters.Add( "@VIPGrade", SqlDbType.Int );
                    paramVIPGrade.Direction = ParameterDirection.Output;
                    paramVIPGrade.Value = _nVIPGrade;

                    SqlParameter paramProfileURL = cmd.Parameters.Add( "@ProfileURL", SqlDbType.NVarChar, ( int )MAXLENGTH.PROFILEURL );
                    paramProfileURL.Direction = ParameterDirection.Output;
                    paramProfileURL.Value = _strProfileURL;

                    SqlParameter paramProfileNumber = cmd.Parameters.Add( "@ProfileNumber", SqlDbType.Int );
                    paramProfileNumber.Direction = ParameterDirection.Output;
                    paramProfileNumber.Value = _nProfileNumber;

                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "RankRecordUpdate_Weekly Fail codeResult = {0}, UserUID = {1}, ElementType = {2}, ElementPoint = {3}, WeekTime = {4}, ResultCode = {5}", ( Int32 )result_code, _nUserUid, _nElementType, _nElementPoint, new DateTime( _nWeekTime ), result_code );
                        return result_code;
                    }

                    _nElementValue = (float)Convert.ToDouble( paramResultPoint.Value );
                    _nRetCode = Convert.ToInt32( paramState.Value );
                    if ( _nRetCode == ( int )DbResultCode.DB_RANK_INSERT )
                    {
                        _strNickName = Convert.ToString( paramNickName.Value );
                        _nUserLevel = Convert.ToByte( paramUserLevel.Value );
                        _nVIPGrade = Convert.ToByte( paramVIPGrade.Value );
                        _strProfileURL = Convert.ToString( paramProfileURL.Value );
                        _nProfileNumber = Convert.ToInt32( paramProfileNumber.Value );
                    }
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode RankElementPointUpdateMonthly( Int64 _nUserUid, Int32 _nElementType, float _nElementPoint, Int32 _nMonthNumber,
            ref float _nElementValue, ref String _strNickName, ref Byte _nUserLevel, ref Byte _nVIPGrade, ref String _strProfileURL, ref int _nProfileNumber, ref int _nRetCode )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.RankRecordUpdate_Monthly", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Input
                    SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    SqlParameter paramUnitUID = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                    paramUnitUID.Direction = ParameterDirection.Input;
                    paramUnitUID.Value = _nUserUid;

                    SqlParameter paramElementType = cmd.Parameters.Add( "@RankElementType", SqlDbType.Int );
                    paramElementType.Direction = ParameterDirection.Input;
                    paramElementType.Value = _nElementType;

                    SqlParameter paramElementPoint = cmd.Parameters.Add( "@RankElementPoint", SqlDbType.Float );
                    paramElementPoint.Direction = ParameterDirection.Input;
                    paramElementPoint.Value = _nElementPoint;

                    SqlParameter paramMonthNumber = cmd.Parameters.Add( "@MonthNumber", SqlDbType.Int );
                    paramMonthNumber.Direction = ParameterDirection.Input;
                    paramMonthNumber.Value = _nMonthNumber;

                    // Output
                    SqlParameter paramResultPoint = cmd.Parameters.Add( "@ResultPoint", SqlDbType.Float );
                    paramResultPoint.Direction = ParameterDirection.Output;
                    paramResultPoint.Value = _nElementValue;

                    SqlParameter paramState = cmd.Parameters.Add( "@State", SqlDbType.Int );
                    paramState.Direction = ParameterDirection.Output;
                    paramState.Value = _nRetCode;

                    SqlParameter paramNickName = cmd.Parameters.Add( "@NickName", SqlDbType.NVarChar, (int)MAXLENGTH.NICKNAME );
                    paramNickName.Direction = ParameterDirection.Output;
                    paramNickName.Value = _strNickName;

                    SqlParameter paramUserLevel = cmd.Parameters.Add( "@UserLevel", SqlDbType.Int );
                    paramUserLevel.Direction = ParameterDirection.Output;
                    paramUserLevel.Value = _nUserLevel;

                    SqlParameter paramVIPGrade = cmd.Parameters.Add( "@VIPGrade", SqlDbType.Int );
                    paramVIPGrade.Direction = ParameterDirection.Output;
                    paramVIPGrade.Value = _nVIPGrade;

                    SqlParameter paramProfileURL = cmd.Parameters.Add( "@ProfileURL", SqlDbType.NVarChar, (int)MAXLENGTH.PROFILEURL );
                    paramProfileURL.Direction = ParameterDirection.Output;
                    paramProfileURL.Value = _strProfileURL;

                    SqlParameter paramProfileNumber = cmd.Parameters.Add( "@ProfileNumber", SqlDbType.Int );
                    paramProfileNumber.Direction = ParameterDirection.Output;
                    paramProfileNumber.Value = _nProfileNumber;

                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "RankRecordUpdate_Monthly Fail codeResult = {0}, UserUID = {1}, ElementType = {2}, ElementPoint = {3}, MonthNumber = {4}, ResultCode = {5}", ( Int32 )result_code, _nUserUid, _nElementType, _nElementPoint, _nMonthNumber, result_code );
                        return result_code;
                    }

                    _nElementValue = (float)Convert.ToDouble( paramResultPoint.Value );
                    _nRetCode = Convert.ToInt32( paramState.Value );
                    if ( _nRetCode == ( int )DbResultCode.DB_RANK_INSERT )
                    {
                        _strNickName = Convert.ToString( paramNickName.Value );
                        _nUserLevel = Convert.ToByte( paramUserLevel.Value );
                        _nVIPGrade = Convert.ToByte( paramVIPGrade.Value );
                        _strProfileURL = Convert.ToString( paramProfileURL.Value );
                        _nProfileNumber = Convert.ToInt32( paramProfileNumber.Value );
                    }
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode GetTotalRankRecord_Weekly( Int64 _lResetTime, ref List<UserRankInfo> RankList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd           = new SqlCommand( "dbo.GetTotalRankRecord_WeeKly", dbConn );
                sqlCmd.CommandType          = CommandType.StoredProcedure;

                SqlParameter paramResult    = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction       = ParameterDirection.ReturnValue;
                paramResult.Value           = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramResetTime = sqlCmd.Parameters.Add( "@WeekResetTime", SqlDbType.DateTime );
                paramResetTime.Direction    = ParameterDirection.Input;
                paramResetTime.Value        = new DateTime( _lResetTime );

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            RankList.Add( new UserRankInfo(
                                ( Int64 )reader["UserID"]
                                , ( Int32 )reader["RankElementType"]
                                , ( String )reader["Nickname"]
                                , ( Byte )reader["Level"]
                                , ( Byte )reader["VipGrade"]
                                , ( String )reader["PhotoImageURL"]
                                , ( Int32 )reader["BaseProfileNumber"]
                                , (float)( double )reader["RankElementPoint"]
                                ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "GetTotalRankRecord_Weekly Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

        public DbResultCode GetTotalRankRecord_Monthly( Int32 _nResetNumber, ref List<UserRankInfo> RankList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd           = new SqlCommand( "dbo.GetTotalRankRecord_Monthly", dbConn );
                sqlCmd.CommandType          = CommandType.StoredProcedure;

                SqlParameter paramResult    = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction       = ParameterDirection.ReturnValue;
                paramResult.Value           = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramResetNumber   = sqlCmd.Parameters.Add( "@MonthNumber", SqlDbType.Int );
                paramResetNumber.Direction      = ParameterDirection.Input;
                paramResetNumber.Value          = _nResetNumber;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            RankList.Add( new UserRankInfo(
                                ( Int64 )reader["UserID"]
                                , ( Int32 )reader["RankElementType"]
                                , ( String )reader["Nickname"]
                                , ( Byte )reader["Level"]
                                , ( Byte )reader["VipGrade"]
                                , ( String )reader["PhotoImageURL"]
                                , ( Int32 )reader["BaseProfileNumber"]
                                , ( float )( double )reader["RankElementPoint"]
                                ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "GetTotalRankRecord_Weekly Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

        public DbResultCode LoadSocialItemRaceRank( List<LoadSocialItemRaceRank> RankList, out Int32 _nNowWeekNumber )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            _nNowWeekNumber = 0;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadSocialItemRaceRank", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramNowWeekNumber = sqlCmd.Parameters.Add( "@NowWeekNumber", SqlDbType.Int );
                paramNowWeekNumber.Direction = ParameterDirection.Output;
                paramNowWeekNumber.Value = _nNowWeekNumber;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            RankList.Add( new LoadSocialItemRaceRank(
                                ( Int32 )reader["WeekNumber"]
                                , ( Int64 )reader["UserUID"]
                                , ( Int32 )reader["WinCount"]
                                , ( Int32 )reader["RewardID"]
                                ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                        _nNowWeekNumber = Convert.ToInt32( paramNowWeekNumber.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "LoadSocialItemRaceRank Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

        public DbResultCode LoadSocialSpeedRaceRank( List<LoadSocialSpeedRaceRank> RankList, out Int32 _nNowWeekNumber )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            _nNowWeekNumber = 0;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadSocialSpeedRaceRank", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramNowWeeksNumber = sqlCmd.Parameters.Add( "@NowWeekNumber", SqlDbType.Int );
                paramNowWeeksNumber.Direction = ParameterDirection.Output;
                paramNowWeeksNumber.Value = _nNowWeekNumber;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            RankList.Add( new LoadSocialSpeedRaceRank(
                                ( Int32 )reader["WeekNumber"]
                                , ( Int64 )reader["UserUID"]
                                , ( Int32 )reader["TrackID"]
                                , ( float )( Double )reader["RecordTime"]
                                , ( Int32 )reader["RewardID"]
                                ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                        _nNowWeekNumber = Convert.ToInt32( paramNowWeeksNumber.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "LoadSocialSpeedRaceRank Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

		public DbResultCode LoadSocialSpeedPastRaceRank( List<LoadSocialSpeedRaceRank> RankList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadSocialSpeedPastRaceRank", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( reader.Read() )
						{
							RankList.Add( new LoadSocialSpeedRaceRank(
								( Int32 )reader["WeekNumber"]
								, ( Int64 )reader["UserUID"]
								, ( Int32 )reader["TrackID"]
								, ( float )( Double )reader["RecordTime"]
								, ( Int32 )reader["RewardID"]
								) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					if ( reader != null )
					{
						reader.Close();
						result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					}
					else
					{
						Logger.ErrLog( "LoadSocialSpeedRaceRank Fail" );
						result_code = DbResultCode.DB_ERROR;
					}
				}
			}
			return result_code;
		}

        public DbResultCode LoadSocialRankTrackID( List<S2SSocialSpeedRaceTrack> listTrackInfo, out Int32 _IsPast )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            _IsPast = 0;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadSocialRankTrackID", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramIsPast = sqlCmd.Parameters.Add( "@IsPast", SqlDbType.Int );
                paramIsPast.Direction = ParameterDirection.Output;
                paramIsPast.Value = 0;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            Int32 nTrackID = ( Int32 )reader["TrackID"];
                            Int32 nTXTCursorNumber = ( Int32 )reader["CursorNumber"];
							String TrackName = ( String )reader["TrackName"];
                            if ( null != listTrackInfo.Find( delegate( S2SSocialSpeedRaceTrack info ) { return info.TrackID == nTrackID; } ) )
                            {
                                Logger.ErrLog( "LoadSocialRankTrackID Error. SameTrackID = {0}", nTrackID );
                                continue;
                            }

							listTrackInfo.Add( new S2SSocialSpeedRaceTrack( nTrackID, nTXTCursorNumber, TrackName ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                        _IsPast = Convert.ToInt32( paramIsPast.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "LoadSocialRankTrackID Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

        public DbResultCode LoadSocialRankPastTrackID( List<S2SSocialSpeedRaceTrack> listTrackInfo )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadSocialRankPastTrackID", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            Int32 nTrackID = ( Int32 )reader["TrackID"];
                            Int32 nTXTCursorNumber = ( Int32 )reader["CursorNumber"];
							String TrackName = (String)reader["TrackName"];
                            if ( null != listTrackInfo.Find( delegate( S2SSocialSpeedRaceTrack info ) { return info.TrackID == nTrackID; } ) )
                            {
                                Logger.ErrLog( "LoadSocialRankPastTrackID Error. SameTrackID = {0}", nTrackID );
                                continue;
                            }

							listTrackInfo.Add( new S2SSocialSpeedRaceTrack( nTrackID, nTXTCursorNumber, TrackName ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "LoadSocialRankPastTrackID Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

		public DbResultCode ReLoadSocialRankResetTime( out Int64 _lResetTime, out bool _bResetNotyflag )
		{
			_lResetTime = 0;
			_bResetNotyflag = false;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				try
				{
					SqlCommand cmd = new SqlCommand( "dbo.P_ReLoadSocialRankResetTime", dbConn );
					cmd.CommandType = CommandType.StoredProcedure;

					// Input
					SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
					paramResult.Direction = ParameterDirection.ReturnValue;
					paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

					SqlParameter paramNewResetTime = cmd.Parameters.Add( "@ResetTime", SqlDbType.DateTime );
					paramNewResetTime.Direction = ParameterDirection.Output;
					paramNewResetTime.Value = DateTime.Now;

					SqlParameter paramRankResetNotify = cmd.Parameters.Add( "@RankResetNotify", SqlDbType.Bit );
					paramRankResetNotify.Direction = ParameterDirection.Output;
					paramRankResetNotify.Value = false;

					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = cmd.ExecuteNonQuery();
					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "ReLoadSocialRankResetTime Fail codeResult = {0}", result_code );
						return result_code;
					}
					DateTime dResetTime = Convert.ToDateTime( paramNewResetTime.Value );

					_lResetTime = dResetTime.Ticks;
					_bResetNotyflag = Convert.ToBoolean( paramRankResetNotify.Value );

				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}
			return result_code;
		}

        public DbResultCode GetSocialRankResetTime( out Int64 _lResetTime, out bool _bRankResetNotify )
        {
            _lResetTime         = 0;
            _bRankResetNotify   = false;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.P_GetSocialRankResetTime", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Input
                    SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    SqlParameter paramNowTime = cmd.Parameters.Add( "@NowTime", SqlDbType.DateTime );
                    paramNowTime.Direction = ParameterDirection.Input;
                    paramNowTime.Value = DateTime.Now;

                    SqlParameter paramNewResetTime = cmd.Parameters.Add( "@ResetTime", SqlDbType.DateTime );
                    paramNewResetTime.Direction = ParameterDirection.Output;
                    paramNewResetTime.Value = DateTime.Now;

                    SqlParameter paramRankResetNotify = cmd.Parameters.Add("@RankResetNotify", SqlDbType.Bit);
                    paramRankResetNotify.Direction = ParameterDirection.Output;
                    paramRankResetNotify.Value = false;


                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "GetSocialRankResetTime Fail codeResult = {0}", result_code );
                        return result_code;
                    }
                    DateTime dResetTime = Convert.ToDateTime( paramNewResetTime.Value );

                    _lResetTime         = dResetTime.Ticks;
                    _bRankResetNotify   = Convert.ToBoolean(paramRankResetNotify.Value);
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode UpdateSocialRankTrack( DataTable kSocialRankTrackList, out DateTime _dNewResetTime )
        {
            _dNewResetTime = DateTime.Now;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.P_UpdateSocialRankTrack", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Input
                    SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    SqlParameter paramTrackList = cmd.Parameters.Add( "@SocialRankTrackList", SqlDbType.Structured );
                    paramTrackList.Direction = ParameterDirection.Input;
                    paramTrackList.Value = kSocialRankTrackList;

                    SqlParameter paramNowTime = cmd.Parameters.Add( "@NowTime", SqlDbType.DateTime );
                    paramNowTime.Direction = ParameterDirection.Input;
                    paramNowTime.Value = DateTime.Now;

                    SqlParameter paramNewResetTime = cmd.Parameters.Add( "@NewResetTime", SqlDbType.DateTime );
                    paramNewResetTime.Direction = ParameterDirection.Output;
                    paramNewResetTime.Value = DateTime.Now;

                    
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					_dNewResetTime = Convert.ToDateTime( paramNewResetTime.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "UpdateSocialRankTrack Fail codeResult = {0}", result_code );
                        return result_code;
                    }
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode UpdateSocialItemRaceRank( Int64 _lUserUID, Int32 _nWinCount, Int64 _lResetTime, out bool _bInsert )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            _bInsert        = false;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.P_UpdateSocialItemRaceRank", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Input
                    SqlParameter paramResult = cmd.Parameters.Add( "@Result", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    SqlParameter paramUserUID = cmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                    paramUserUID.Direction = ParameterDirection.Input;
                    paramUserUID.Value = _lUserUID;

                    SqlParameter paramWinCount = cmd.Parameters.Add( "@WinCount", SqlDbType.Int );
                    paramWinCount.Direction = ParameterDirection.Input;
                    paramWinCount.Value = _nWinCount;

                    SqlParameter paramResetTime = cmd.Parameters.Add( "@ResetTime", SqlDbType.DateTime );
                    paramResetTime.Direction = ParameterDirection.Input;
                    paramResetTime.Value = new DateTime( _lResetTime );

                    SqlParameter paramIsInsert = cmd.Parameters.Add( "@IsInsert", SqlDbType.Bit );
                    paramIsInsert.Direction = ParameterDirection.Output;
                    paramIsInsert.Value = _bInsert;


                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "UpdateSocialItemRaceRank Fail codeResult = {0}", result_code );
                        return result_code;
                    }
                    _bInsert = ( bool )Convert.ToBoolean( paramIsInsert.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode UpdateSocialSpeedRaceRank( Int64 _lUserUID, Int32 _nTrackID, float _lTimeRecord, Int64 _lResetTime, out bool _bInsert )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            _bInsert = false;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                try
                {
                    SqlCommand cmd = new SqlCommand( "dbo.P_UpdateSocialSpeedRaceRank", dbConn );
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Input
                    SqlParameter paramResult = cmd.Parameters.Add( "@Result", SqlDbType.Int );
                    paramResult.Direction = ParameterDirection.ReturnValue;
                    paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                    SqlParameter paramUserUID = cmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                    paramUserUID.Direction = ParameterDirection.Input;
                    paramUserUID.Value = _lUserUID;

                    SqlParameter paramTrackID = cmd.Parameters.Add( "@TrackID", SqlDbType.Int );
                    paramTrackID.Direction = ParameterDirection.Input;
                    paramTrackID.Value = _nTrackID;

                    SqlParameter paramTimeRecord = cmd.Parameters.Add( "@TimeRecord", SqlDbType.Float );
                    paramTimeRecord.Direction = ParameterDirection.Input;
                    paramTimeRecord.Value = _lTimeRecord;

                    SqlParameter paramResetTime = cmd.Parameters.Add( "@ResetTime", SqlDbType.DateTime );
                    paramResetTime.Direction = ParameterDirection.Input;
                    paramResetTime.Value = new DateTime( _lResetTime );

                    SqlParameter paramIsInsert = cmd.Parameters.Add( "@IsInsert", SqlDbType.Bit );
                    paramIsInsert.Direction = ParameterDirection.Output;
                    paramIsInsert.Value = _bInsert;


                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "UpdateSocialSpeedRaceRank Fail codeResult = {0}", result_code );
                        return result_code;
                    }
                    _bInsert = ( bool )Convert.ToBoolean( paramIsInsert.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode GetRankClassInfo( ref List<RankClassInfo> _RankClassInfoList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.GetRankClassInfo", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if ( reader.HasRows )
                    {
                        while ( reader.Read() )
                        {
                            _RankClassInfoList.Add( new RankClassInfo(
                                ( Int32 )reader["ElementType"]
                                , ( Int32 )reader["CollectPeriodType"]
                                , ( Int32 )reader["RankingClass"]
                                , ( Int32 )reader["ClassDivideType"]
                                , ( Int32 )reader["DivideValueTop"]
                                , ( Int32 )reader["DivideValueBottom"]
                                , ( Int32 )reader["RewardGroupID"]
                                ) );
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    if ( reader != null )
                    {
                        reader.Close();
                        result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    }
                    else
                    {
                        Logger.ErrLog( "GetRankClassInfo Fail" );
                        result_code = DbResultCode.DB_ERROR;
                    }
                }
            }
            return result_code;
        }

        public DbResultCode GetRankingResetTime( int _nCollectionPeriodType, ref Int64 _nCollectResetData, ref Int64 _nRankUpdatePeriod )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = new SqlCommand( "dbo.GetRankingResetTime", dbConn );
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramCollectionPeriodType = cmd.Parameters.Add( "@CollectionPeriodType", SqlDbType.Int );
                paramCollectionPeriodType.Direction = ParameterDirection.Input;
                paramCollectionPeriodType.Value = _nCollectionPeriodType;

                SqlParameter paramNowTime = cmd.Parameters.Add( "@NowTime", SqlDbType.DateTime );
                paramNowTime.Direction = ParameterDirection.Input;
                paramNowTime.Value = DateTime.Now;

                SqlParameter paramCollectResetDate = cmd.Parameters.Add( "@CollectResetDate", SqlDbType.DateTime );
                paramCollectResetDate.Direction = ParameterDirection.Output;
                paramCollectResetDate.Value = DateTime.Now;

                SqlParameter paramRankUpdatePeriod = cmd.Parameters.Add( "@RankUpdatePeriod", SqlDbType.DateTime );
                paramRankUpdatePeriod.Direction = ParameterDirection.Output;
                paramRankUpdatePeriod.Value = DateTime.Now;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code == DbResultCode.DB_ERROR_COMMON_CAN_NOT_FIND_ROW_DATA )
                    {
                        Logger.ErrLog( "GetRankingResetTime Fail CollectionPeriodType = {0}", _nCollectionPeriodType );
                        return result_code;
                    }
                    else if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "GetRankingResetTime Error : Result = {0}, CollectionPeriodType = {2}", ( Int32 )result_code, _nCollectionPeriodType );
                        return result_code;
                    }

                    _nCollectResetData = ( ( DateTime )paramCollectResetDate.Value ).Ticks;
                    _nRankUpdatePeriod = ( ( DateTime )paramRankUpdatePeriod.Value ).Ticks;
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

        public DbResultCode GetRankingUpdateTime( int _nCollectionPeriodType, ref Int64 _nRankUpdatePeriod )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = new SqlCommand( "dbo.GetRankingUpdateTime", dbConn );
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramCollectionPeriodType = cmd.Parameters.Add( "@CollectionPeriodType", SqlDbType.Int );
                paramCollectionPeriodType.Direction = ParameterDirection.Input;
                paramCollectionPeriodType.Value = _nCollectionPeriodType;

                SqlParameter paramRankUpdatePeriod = cmd.Parameters.Add( "@RankUpdatePeriod", SqlDbType.DateTime );
                paramRankUpdatePeriod.Direction = ParameterDirection.Output;
                paramRankUpdatePeriod.Value = DateTime.Now;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "GetRankingUpdateTime Fail CollectionPeriodType = {0}", _nCollectionPeriodType );
                        return result_code;
                    }
                    _nRankUpdatePeriod = ( ( DateTime )paramRankUpdatePeriod.Value ).Ticks;
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }
            return result_code;
        }

		public DbResultCode GetSocialProfileByPlatformID( Int64 _nUserUID, String _strPlatformIDList, ref List<KMPlatformSocialProfile> _listSocialProfile )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}
			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetSocialProfileByPlatformID", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _nUserUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformIDList", SqlDbType.VarChar );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = _strPlatformIDList;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if( reader.HasRows )
					{
						while( reader.Read() )
						{
                            KMSocialProfile socialProfile = new KMSocialProfile( (Int64)reader["UserUID"]
                                                        , (String)reader["Nickname"]
                                                        , (String)reader["PhotoImageURL"]
                                                        , (Int32)reader["BaseProfileNumber"]
                                                        , (Byte)reader["Level"]
                                                        , (Byte)reader["VIPGrade"]
                                                        , ( (DateTime)reader["LogoutTime"] ).Ticks
                                                        , ( (DateTime)reader["LastRefreshFatigueGoodsSentTime"] ).Ticks
                                                        , (String)reader["PlatformID"]
                                                        , (bool)reader["MsgOptionKakao"] );


							KMPlatformSocialProfile platformSocialProfile = new KMPlatformSocialProfile( (String)reader["PlatformID"], socialProfile );
								
							_listSocialProfile.Add( platformSocialProfile );
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					if( reader != null )
					{
						reader.Close();
						result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					}
					else
					{
						Logger.ErrLog( "GetSocialProfileByPlatformID Fail" );
						result_code = DbResultCode.DB_ERROR;
					}
				}
			}
			return result_code;
		}

        public DbResultCode GetUserCompleteCareer( Int64 _lUserUID, List<DBCareerCompleteStageInfo> _listStageInfo, Int32 _nConditionCount, Int32 _nDailyDefulatCount )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = new SqlCommand( "dbo.GetUserCompleteCareer", dbConn );
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _lUserUID;

                SqlParameter paramConditionCount = cmd.Parameters.Add( "@ConditionCount", SqlDbType.Int );
                paramConditionCount.Direction = ParameterDirection.Input;
                paramConditionCount.Value = _nConditionCount;

				SqlParameter paramDefaultCount = cmd.Parameters.Add( "@Default_Count", SqlDbType.Int );
				paramDefaultCount.Direction = ParameterDirection.Input;
				paramDefaultCount.Value = _nDailyDefulatCount;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                    if ( result.HasRows )
                    {
                        Int32 nConditionNumber = 0;
                        while ( result.Read() )
                        {
                            Int16 sStageNumber  = ( Int16 )result["StageNumber"];
                            Int16 sComplete     = ( Int16 )result["Complete"];
                            Int32 nConditionCount = ( Int32 )result["ConditionCount"];

                            if ( false == CheckAndInsertUserConditionResult( _listStageInfo, sStageNumber, sComplete, nConditionNumber ) )
                            {
                                DBCareerCompleteStageInfo tempInfo = new DBCareerCompleteStageInfo( sStageNumber, new Int16[nConditionCount] );
                                if ( nConditionCount > 0 )
                                    tempInfo.Complete[0] = sComplete;
                                _listStageInfo.Add( tempInfo );
                                nConditionNumber = 0;
                            }
                            ++nConditionNumber;
                        }
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    result.Close();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
            }

            return result_code;
        }

		public DbResultCode GetUserDailyStage( Int64 _lUserUID, Int32 DefaultDailyCount, out DBDailyStageInfo[] DailyList )
		{
			List<DBDailyStageInfo> tempList = new List<DBDailyStageInfo>();
			DailyList = null;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand cmd = new SqlCommand( "dbo.CAREER_GetUserDailyStage", dbConn );
				cmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = cmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUID = cmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUID.Direction = ParameterDirection.Input;
				paramUID.Value = _lUserUID;

				SqlParameter paramDefaultCount = cmd.Parameters.Add( "@DefaultCount", SqlDbType.Int );
				paramDefaultCount.Direction = ParameterDirection.Input;
				paramDefaultCount.Value = DefaultDailyCount;

				SqlDataReader result = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					result = cmd.ExecuteReader();
					if ( result.HasRows )
					{
						Int32 nConditionNumber = 0;
						while ( result.Read() )
						{
							Int16 StageNumber		= ( Int16 )result["StageNumber"];
							Int32 Complete			= ( Int32 )result["RemainCount"];

							tempList.Add( new DBDailyStageInfo( StageNumber, Complete ) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();
					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "DailyStage Is No Data. retCode = {0}, UserUID = {1}", result_code, _lUserUID );
				}
			}

			DailyList = tempList.ToArray();

			return result_code;
		}

		public DbResultCode GetUserDailyHiddenStage( Int64 _lUserUID, Int32 DefaultHiddenCount, out DBDailyStageInfo[] DailyHiddenList )
		{
			List<DBDailyStageInfo> tempList = new List<DBDailyStageInfo>();
			DailyHiddenList = null;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand cmd = new SqlCommand( "dbo.CAREER_GetUserDailyHiddenStage", dbConn );
				cmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = cmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUID = cmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUID.Direction = ParameterDirection.Input;
				paramUID.Value = _lUserUID;

				SqlParameter paramDefaultCount = cmd.Parameters.Add( "@DefaultHiddenCount", SqlDbType.Int );
				paramDefaultCount.Direction = ParameterDirection.Input;
				paramDefaultCount.Value = DefaultHiddenCount;

				SqlDataReader result = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					result = cmd.ExecuteReader();
					if ( result.HasRows )
					{
						Int32 nConditionNumber = 0;
						while ( result.Read() )
						{
							Int16 StageNumber = ( Int16 )result["StageNumber"];
							Int32 Complete = ( Int32 )result["RemainCount"];

							tempList.Add( new DBDailyStageInfo( StageNumber, Complete ) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();
					result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "DailyHiddenStage Is No Data. retCode = {0}, UserUID = {1}", result_code, _lUserUID );
				}
			}

			DailyHiddenList = tempList.ToArray();

			return result_code;
		}

        private bool CheckAndInsertUserConditionResult( List<DBCareerCompleteStageInfo> _listStageInfo, Int16 _sStageNumber, Int16 _sComplete, Int32 _nConditionNumber )
        {
            for ( int i = 0; i < _listStageInfo.Count; ++i )
            {
                if ( _listStageInfo.ElementAt( i ).StageNumber == _sStageNumber )
                {
                    _listStageInfo.ElementAt( i ).Complete[_nConditionNumber] = _sComplete;
                    return true;
                }
            }
            return false;
        }

		public DbResultCode ForcedOpenCareerStage( Int64 _lUserUID, Int16 _sStageNumber, Int32 _nConditionCount, byte GoodsType, Int32 GoodsQuantity )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = new SqlCommand( "dbo.ForcedOpenCareerStage", dbConn );
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _lUserUID;

                SqlParameter paramStageNumber = cmd.Parameters.Add( "@StageNumber", SqlDbType.SmallInt );
                paramStageNumber.Direction = ParameterDirection.Input;
                paramStageNumber.Value = _sStageNumber;

                SqlParameter paramConditionCount = cmd.Parameters.Add( "@ConditionCount", SqlDbType.Int );
                paramConditionCount.Direction = ParameterDirection.Input;
                paramConditionCount.Value = _nConditionCount;


				SqlParameter paramGoodsType = cmd.Parameters.Add( "@GoodsType", SqlDbType.TinyInt );
				paramGoodsType.Direction = ParameterDirection.Input;
				paramGoodsType.Value = GoodsType;

				SqlParameter paramGoodsQuantity = cmd.Parameters.Add( "@Quantity", SqlDbType.Int );
				paramGoodsQuantity.Direction = ParameterDirection.Input;
				paramGoodsQuantity.Value = GoodsQuantity;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );

                    if ( result_code == DbResultCode.DB_ERROR )
                    {
                        Logger.ErrLog( "ForcedOpenCareerStage Fail : UserUID = {0}, StageNumber = {1}", _lUserUID, _sStageNumber );
                    }
                    else if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "ForcedOpenCareerStage Error : Result = {0}, UserUID = {1}, StageNumber = {2}", ( Int32 )result_code, _lUserUID, _sStageNumber );
                    }
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public KMCareerFriendStageInfo[] GetFriendsCareerStageInfo(Int64 lUserUID, DataTable _kFriendUIDList)
		{
			List<KMCareerFriendStageInfo> kFriendsStageInfo = new List<KMCareerFriendStageInfo>();
			try
			{
                if (Manager.IsOpen() == false)
					Manager.Open();
			}
            catch (System.Exception ex)
			{
                Logger.ExceptionLog(ex);
				return kFriendsStageInfo.ToArray();
			}
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
                SqlCommand cmd = new SqlCommand("dbo.CAREER_GetFriendsStageInfo", dbConn);
				cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID           = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction              = ParameterDirection.Input;
				paramUserUID.Value                  = lUserUID;

                SqlParameter kParamFriendUIDList    = cmd.Parameters.Add( "@FriendUIDList", SqlDbType.Structured );
                kParamFriendUIDList.Direction       = ParameterDirection.Input;
                kParamFriendUIDList.Value           = _kFriendUIDList;

				SqlDataReader kResultData = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					kResultData = cmd.ExecuteReader();
                    if (kResultData.HasRows)
					{
                        Int32 nConditionNumber = 0;
                        while (kResultData.Read())
						{
                            Int64 lGetUserUID       = ( Int64 )kResultData["UserUID"];
                            Int16 sStageNumber      = (Int16)kResultData["StageNumber"];
                            Int16 sComplete         = (Int16)kResultData["Complete"];
                            Int32 nConditionCount   = (Int32)kResultData["ConditionCount"];

                            bool bComplete = false;
                            if ( sComplete > 0 )
                                bComplete = true;

                            if ( false == CheckAndInsertFirendConditionResult( kFriendsStageInfo, lGetUserUID, bComplete, nConditionNumber ) )
                            {
                                KMCareerFriendStageInfo kFriendStageInfo = new KMCareerFriendStageInfo( lGetUserUID,
                                    new KMCareerStageInfo( sStageNumber, new bool[nConditionCount] ) );
                                kFriendStageInfo.stageInfo.Complete[0] = bComplete;
                                kFriendsStageInfo.Add(kFriendStageInfo);
                                nConditionNumber = 0;
                            }
                            ++nConditionNumber;
						}
					}
                    //
				}
                catch (Exception ex)
				{
                    Logger.ExceptionLog(ex);
					kFriendsStageInfo.Clear();
				}
			}

			return kFriendsStageInfo.ToArray();
		}

        private bool CheckAndInsertFirendConditionResult( List<KMCareerFriendStageInfo> _listFriendsStageInfo, Int64 _lUserUID, bool _bComplete, Int32 _nConditionNumber )
        {
            for ( int i = 0; i < _listFriendsStageInfo.Count; ++i )
            {
                if ( _listFriendsStageInfo.ElementAt( i ).lUserUID == _lUserUID )
                {
                    _listFriendsStageInfo.ElementAt( i ).stageInfo.Complete[_nConditionNumber] = _bComplete;
                    return true;
                }
            }
            return false;
        }

        public DbResultCode GetFriendsCareerProfileInfo( Int64 IUserUID, ref List<KMCareerFriendProfileInfo> kFriendsCareerProfileInfoList, DataTable _kFriendUIDList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.CAREER_GetFriendsProfileInfo", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = IUserUID;

                SqlParameter kParamFriendUIDList    = cmd.Parameters.Add( "@FriendUIDList", SqlDbType.Structured );
                kParamFriendUIDList.Direction       = ParameterDirection.Input;
                kParamFriendUIDList.Value           = _kFriendUIDList;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            kFriendsCareerProfileInfoList.Add(new KMCareerFriendProfileInfo(
                                (Int64)result["UserUID"],
                                (Byte)result["BaseProfileNumber"],
								(String)result["PhotoImageURL"],
                                "")
                                );
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("GetFriendsCareerProfileInfo Fail : UserUID = {0}", IUserUID);
                    }
                }
            }

            return result_code;
        }
        public DbResultCode FriendAskFavor(Int64 _UserUID, Int16 _EpisodeNumber, Byte _byMaxRequestCount, DateTime _ExpirationDate, DataTable _kFriendUIDList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CAREER_AskFavorFriends", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _UserUID;

                SqlParameter paramEpisodeNumber = sqlCmd.Parameters.Add("@EpisodeNumber", SqlDbType.SmallInt);
                paramEpisodeNumber.Direction = ParameterDirection.Input;
                paramEpisodeNumber.Value = _EpisodeNumber;

                SqlParameter paramMaxRequestCount = sqlCmd.Parameters.Add("@MaxRequestCount", SqlDbType.TinyInt);
                paramMaxRequestCount.Direction = ParameterDirection.Input;
                paramMaxRequestCount.Value = _byMaxRequestCount;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = _ExpirationDate;

                SqlParameter paramFriendUIDList = sqlCmd.Parameters.Add("@FriendUIDList", SqlDbType.Structured);
                paramFriendUIDList.Direction = ParameterDirection.Input;
                paramFriendUIDList.Value = _kFriendUIDList;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("CAREER_AskFavorFriends Fail UserUID = {0} ResultCode = {1}", _UserUID, result_code);
                }
            }

            return result_code;
        }

        public DbResultCode FriendAcceptFavor(Int64 _UserUID, DataTable _kFriendUIDList, Int16 _sNeedFavorCount, ref List<Int64> _kAllFavorReceivedUIDList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CAREER_AcceptFavorFriends", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _UserUID;

                SqlParameter paramFriendUIDList = sqlCmd.Parameters.Add("@FriendUIDList", SqlDbType.Structured);
                paramFriendUIDList.Direction = ParameterDirection.Input;
                paramFriendUIDList.Value = _kFriendUIDList;

                SqlParameter paramNeedFavorCount = sqlCmd.Parameters.Add("@NeedAcceptCount", SqlDbType.SmallInt);
                paramNeedFavorCount.Direction = ParameterDirection.Input;
                paramNeedFavorCount.Value = _sNeedFavorCount;

                SqlDataReader result = null;
                try
                {
                    dbConn.Open();
                    result = sqlCmd.ExecuteReader();
                    if(result.HasRows)
                    {
                        while(true == result.Read())
                        {
                            _kAllFavorReceivedUIDList.Add((Int64)result["UserUID"]);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("CAREER_AcceptFavorFriends is Fail UserUID: {0}, retCode: {1}", _UserUID, result_code);
                    }
                }
            }

            return result_code;
        }

        public DbResultCode FavorAskList(Int64 _UserUID, ref KMCareerFriendProfileInfo[] _FriendProfileInfoArray)
        {
            List<KMCareerFriendProfileInfo> kFriendProfileInfoList = new List<KMCareerFriendProfileInfo>();
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.CAREER_GetAskFavorList", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _UserUID;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            kFriendProfileInfoList.Add(new KMCareerFriendProfileInfo(
                                (Int64)result["SendUserUID"],
                                (Byte)((Int32)(result["BaseProfileNumber"])),
                                (String)result["PhotoImageURL"],
                                (String)result["Nickname"]
                                ));
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("CAREER_GetAskFavorList Fail : UserUID = {0}", _UserUID);
                    }

                    else
                    {
                        _FriendProfileInfoArray = kFriendProfileInfoList.ToArray();
                    }
                }
            }

            return result_code;
        }

        public DbResultCode FavorAcceptList(Int64 _UserUID, Int16 _NextEpisodeNumber, Int16 NeedAcceptCount, ref KMCareerFriendProfileInfo[] _FriendProfileArray)
        {
            List<KMCareerFriendProfileInfo> kFriendProfileInfoList = new List<KMCareerFriendProfileInfo>();
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.CAREER_GetAcceptFavorList", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _UserUID;

                SqlParameter paramEpisodeNumber = cmd.Parameters.Add("@EpisodeNumber", SqlDbType.SmallInt);
                paramEpisodeNumber.Direction = ParameterDirection.Input;
                paramEpisodeNumber.Value    = _NextEpisodeNumber;

				SqlParameter paramAcceptCount = cmd.Parameters.Add( "@NeedAcceptCount", SqlDbType.SmallInt );
				paramAcceptCount.Direction = ParameterDirection.Input;
				paramAcceptCount.Value = NeedAcceptCount;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            kFriendProfileInfoList.Add(new KMCareerFriendProfileInfo(
                                (Int64)result["ReceiveUserUID"],
                                (Byte)((Int32)(result["BaseProfileNumber"])),
                                (String)result["PhotoImageURL"],
                                (String)result["Nickname"]
                                ));
                        }
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("CAREER_GetFavorAcceptList Fail : UserUID = {0}", _UserUID);
                    }

                    else
                    {
                        _FriendProfileArray = kFriendProfileInfoList.ToArray();
                    }
                }
            }

            return result_code;
        }

        public DbResultCode NewFavorAskCheck(Int64 _UserUID, ref bool _IsNewFavorAsk)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.CAREER_CheckNewFavorAsk", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _UserUID;

                SqlParameter paramNewFavorAsk = cmd.Parameters.Add("@IsNewFavorAsk", SqlDbType.Bit);
                paramNewFavorAsk.Direction = ParameterDirection.Output;
                paramNewFavorAsk.Value = _IsNewFavorAsk;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("CAREER_GetFavorAcceptList Fail : UserUID = {0}", _UserUID);
                        _IsNewFavorAsk = false;
                    }

                    else
                    {
                        _IsNewFavorAsk = (bool)paramNewFavorAsk.Value;
                    }
                }
            }

            return result_code;
        }

        public DbResultCode OpenCareerStage(Int64 _UserUID, DataTable _kCareerStageInfoList, DataTable CompleteStage, Int16 HiddenStage, Int32 DefaultHiddenCount )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }   
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CAREER_OpenNextStage", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _UserUID;

                SqlParameter paramOpenList = sqlCmd.Parameters.Add("@CareerStageInfoList", SqlDbType.Structured);
				paramOpenList.Direction = ParameterDirection.Input;
				paramOpenList.Value = _kCareerStageInfoList;

				SqlParameter paramCompleteStage = sqlCmd.Parameters.Add( "@CompleteStage", SqlDbType.Structured );
				paramCompleteStage.Direction = ParameterDirection.Input;
				paramCompleteStage.Value = CompleteStage;

				SqlParameter paramHiddenStageNumber = sqlCmd.Parameters.Add( "@HiddenStageNumber", SqlDbType.SmallInt );
				paramHiddenStageNumber.Direction = ParameterDirection.Input;
				paramHiddenStageNumber.Value = HiddenStage;

				SqlParameter paramDefaultHiddenCount = sqlCmd.Parameters.Add( "@DefaultHiddenCount", SqlDbType.Int );
				paramDefaultHiddenCount.Direction = ParameterDirection.Input;
				paramDefaultHiddenCount.Value = DefaultHiddenCount;


                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("CAREER_OpenNextStage Fail UserUID = {0} ResultCode = {1}", _UserUID, result_code);
                }
            }
            return result_code;
        }

		public DbResultCode AddCareerNextStage( Int64 _UserUID, DataTable _kCareerStageInfoList, Int16 HiddenStage, Int32 DefaultHiddenCount )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_AddNextStage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _UserUID;

				SqlParameter paramOpenList = sqlCmd.Parameters.Add( "@CareerStageInfoList", SqlDbType.Structured );
				paramOpenList.Direction = ParameterDirection.Input;
				paramOpenList.Value = _kCareerStageInfoList;

				SqlParameter paramHiddenStageNumber = sqlCmd.Parameters.Add( "@HiddenStageNumber", SqlDbType.SmallInt );
				paramHiddenStageNumber.Direction = ParameterDirection.Input;
				paramHiddenStageNumber.Value = HiddenStage;

				SqlParameter paramDefaultHiddenCount = sqlCmd.Parameters.Add( "@DefaultHiddenCount", SqlDbType.Int );
				paramDefaultHiddenCount.Direction = ParameterDirection.Input;
				paramDefaultHiddenCount.Value = DefaultHiddenCount;


				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "CAREER_OpenNextStage Fail UserUID = {0} ResultCode = {1}", _UserUID, result_code );
				}
			}
			return result_code;
		}

		public DbResultCode GetCareerFavorOpenTime( Int64 _UserUID, out Int16 _OpenEpisodeNumber, out Int64 _OpenTimeTick)
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			_OpenEpisodeNumber = 0;
			_OpenTimeTick = 0;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_GetCareerFavorOpenTime", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _UserUID;

				SqlParameter paramOpenEpisodeNumber = sqlCmd.Parameters.Add( "@OpenEpisodeNumber", SqlDbType.SmallInt );
				paramOpenEpisodeNumber.Direction = ParameterDirection.Input;
				paramOpenEpisodeNumber.Value = _OpenEpisodeNumber;

				SqlParameter paramOpenTime = sqlCmd.Parameters.Add( "@OpenTime", SqlDbType.DateTime );
				paramOpenTime.Direction = ParameterDirection.Input;
				paramOpenTime.Value = DateTime.Now;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code == DbResultCode.SUCCESS )
				{
					_OpenEpisodeNumber	= Convert.ToInt16( paramOpenEpisodeNumber.Value );
					_OpenTimeTick		= Convert.ToDateTime( paramOpenTime.Value ).Ticks;
				}

				else if ( result_code == DbResultCode.DB_ERROR )
				{
					Logger.ErrLog( "GetCareerFavorOpenTime fail. UserUID = {0}", _UserUID );
				}
			}

			return result_code;
		}

		public DbResultCode StartDailyStage( Int64 UserUID, Int16 StageNumber, Int32 DefaultDailyCount, out Int32 RemainCount )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			RemainCount = 0;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_StartDailyStage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramStageNumber = sqlCmd.Parameters.Add( "@StageNumber", SqlDbType.SmallInt );
				paramStageNumber.Direction = ParameterDirection.Input;
				paramStageNumber.Value = StageNumber;

				SqlParameter paramDefaultCount = sqlCmd.Parameters.Add( "@DefaultCount", SqlDbType.Int );
				paramDefaultCount.Direction = ParameterDirection.Input;
				paramDefaultCount.Value = DefaultDailyCount;

				SqlParameter paramRemainCount = sqlCmd.Parameters.Add( "@RemainCount", SqlDbType.Int );
				paramRemainCount.Direction = ParameterDirection.Output;
				paramRemainCount.Value = RemainCount;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code == DbResultCode.SUCCESS )
				{
					RemainCount = Convert.ToInt32( paramRemainCount.Value );
				}
				else
				{
					Logger.ErrLog( "StartDailyStage fail. retCode = {0}, UserUID = {1}, StageNumber = {2}", result_code, UserUID, StageNumber );
				}
			}

			return result_code;
		}

		public DbResultCode StartDailyHiddenStage( Int64 UserUID, Int16 StageNumber, Int32 DefaultDailyCount, out Int32 RemainCount )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			RemainCount = 0;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_StartDailyHiddenStage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramStageNumber = sqlCmd.Parameters.Add( "@StageNumber", SqlDbType.SmallInt );
				paramStageNumber.Direction = ParameterDirection.Input;
				paramStageNumber.Value = StageNumber;

				SqlParameter paramDefaultCount = sqlCmd.Parameters.Add( "@DefaultCount", SqlDbType.Int );
				paramDefaultCount.Direction = ParameterDirection.Input;
				paramDefaultCount.Value = DefaultDailyCount;

				SqlParameter paramRemainCount = sqlCmd.Parameters.Add( "@RemainCount", SqlDbType.Int );
				paramRemainCount.Direction = ParameterDirection.Output;
				paramRemainCount.Value = RemainCount;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code == DbResultCode.SUCCESS )
				{
					RemainCount = Convert.ToInt32( paramRemainCount.Value );
				}
				else
				{
					Logger.ErrLog( "StartDailyHiddenStage fail. retCode = {0}, UserUID = {1}, StageNumber = {2}", result_code, UserUID, StageNumber );
				}
			}

			return result_code;
		}

		public DbResultCode InsertDailyStage( Int64 UserUID, Int32 DefaultDailyCount, Int16 NextDailyStageNumber )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_InsertDailyStage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramDailyDefaultCount = sqlCmd.Parameters.Add( "@DailyDefaultCount", SqlDbType.Int );
				paramDailyDefaultCount.Direction = ParameterDirection.Input;
				paramDailyDefaultCount.Value = DefaultDailyCount;

				SqlParameter paramNextDailyStageNumber = sqlCmd.Parameters.Add( "@NextDailyStageNumber", SqlDbType.SmallInt );
				paramNextDailyStageNumber.Direction = ParameterDirection.Input;
				paramNextDailyStageNumber.Value = NextDailyStageNumber;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "StartDailyStage fail. retCode = {0}, UserUID = {1}, NextStageNumber = {2}", result_code, UserUID, NextDailyStageNumber );
				}
			}

			return result_code;
		}

		public DbResultCode RefreshDailyStageRemindCount( Int64 UserUID, Int16 StageNumber, Int32 RefreshCount, Int32 UseCash )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_UpdateDailyStageRemindCount", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramStageNumber = sqlCmd.Parameters.Add( "@StageNumber", SqlDbType.SmallInt );
				paramStageNumber.Direction = ParameterDirection.Input;
				paramStageNumber.Value = StageNumber;

				SqlParameter paramRefreshCount = sqlCmd.Parameters.Add( "@RefreshCount", SqlDbType.Int );
				paramRefreshCount.Direction = ParameterDirection.Input;
				paramRefreshCount.Value = RefreshCount;

				SqlParameter paramUseCash = sqlCmd.Parameters.Add( "@UseCash", SqlDbType.Int );
				paramUseCash.Direction = ParameterDirection.Input;
				paramUseCash.Value = UseCash;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "RefreshDailyStageRemindCount fail. retCode = {0}, UserUID = {1}, StageNumber = {2}, RefreshCount = {3}", result_code, UserUID, StageNumber, RefreshCount );
				}
			}

			return result_code;
		}

		public DbResultCode LoadDailyResetTime( out Int64 DailyResetTick )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			DailyResetTick = 0;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_LoadDailyResetTime", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramResetTime = sqlCmd.Parameters.Add( "@ResetTime", SqlDbType.DateTime );
				paramResetTime.Direction = ParameterDirection.Output;
				paramResetTime.Value = DateTime.Now;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "LoadDailyResetTime fail. retCode = {0}", result_code );
				}
				else
					DailyResetTick = Convert.ToDateTime( paramResetTime.Value ).Ticks;
			}

			return result_code;
		}

		public DbResultCode UpdateDailyReset( DataTable UserUIDList, Int32 DefaultCount, Int32 DefaultHiddenCount, out Int64 NextDailyResetTick )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			NextDailyResetTick = 0;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_UpdataeDailyReset", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUIDList = sqlCmd.Parameters.Add( "@UserUIDList", SqlDbType.Structured );
				paramUserUIDList.Direction = ParameterDirection.Input;
				paramUserUIDList.Value = UserUIDList;

				SqlParameter paramDefaultCount = sqlCmd.Parameters.Add( "@DefaultCount", SqlDbType.Int );
				paramDefaultCount.Direction = ParameterDirection.Input;
				paramDefaultCount.Value = DefaultCount;

				SqlParameter paramDefaultHiddenCount = sqlCmd.Parameters.Add( "@DefaultHiddenCount", SqlDbType.Int );
				paramDefaultHiddenCount.Direction = ParameterDirection.Input;
				paramDefaultHiddenCount.Value = DefaultHiddenCount;

				SqlParameter paramResetTime = sqlCmd.Parameters.Add( "@NextResetTime", SqlDbType.DateTime );
				paramResetTime.Direction = ParameterDirection.Output;
				paramResetTime.Value = DateTime.Now;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "UpdateDailyReset fail. retCode = {0}", result_code );
				}
				else
					NextDailyResetTick = Convert.ToDateTime( paramResetTime.Value ).Ticks;
			}

			return result_code;
		}

		public DbResultCode UpdateDisconnectStage( Int64 UserUID, Int32 StageNumber )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_UpdateDisconnectStage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramStageNumber = sqlCmd.Parameters.Add( "@StageNumber", SqlDbType.Int );
				paramStageNumber.Direction = ParameterDirection.Input;
				paramStageNumber.Value = StageNumber;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "UpdateDisconnectStage fail. resultCode = {0}, UserUID = {1}, StageNumber = {2}", result_code, UserUID, StageNumber );
				}
			}

			return result_code;
		}

		public DbResultCode CheckDisconnectStage( Int64 UserUID, Int32 StageNumber )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.CAREER_CheckDisconnectStage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramStageNumber = sqlCmd.Parameters.Add( "@StageNumber", SqlDbType.Int );
				paramStageNumber.Direction = ParameterDirection.Input;
				paramStageNumber.Value = StageNumber;

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
				if ( result_code != DbResultCode.SUCCESS )
				{
					Logger.ErrLog( "CheckDisconnectStage fail. resultCode = {0}, UserUID = {1}, StageNumber = {2}", result_code, UserUID, StageNumber );
				}
			}

			return result_code;
		}

		public DbResultCode OpenCareerFavorStage( Int64 _UserUID, DataTable _kCareerStageInfoList, DataTable CompleteStage, DateTime _kEpisodeOpenDate, Int16 HiddenStage, Int32 DefaultHiddenCount )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.CAREER_OpenNextFavorStage", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _UserUID;

                SqlParameter paramOpenStage = sqlCmd.Parameters.Add("@CareerStageInfoList", SqlDbType.Structured);
				paramOpenStage.Direction = ParameterDirection.Input;
				paramOpenStage.Value = _kCareerStageInfoList;

				SqlParameter paramComplete = sqlCmd.Parameters.Add( "@CompleteStage", SqlDbType.Structured );
				paramComplete.Direction = ParameterDirection.Input;
				paramComplete.Value = CompleteStage;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@EpisodeOpenDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = _kEpisodeOpenDate;

				SqlParameter paramHiddenStageNumber = sqlCmd.Parameters.Add( "@HiddenStageNumber", SqlDbType.SmallInt );
				paramHiddenStageNumber.Direction = ParameterDirection.Input;
				paramHiddenStageNumber.Value = HiddenStage;

				SqlParameter paramHiddenStageCount = sqlCmd.Parameters.Add( "@HiddenStageCount", SqlDbType.Int );
				paramHiddenStageCount.Direction = ParameterDirection.Input;
				paramHiddenStageCount.Value = DefaultHiddenCount;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("CAREER_OpenNextFavorStage Fail UserUID = {0} ResultCode = {1}", _UserUID, result_code);
                }
            }
            return result_code;
        }

        public DbResultCode CheckOpenNextEpisode(Int64 _UserUID, Int16 _EpisodeNumber, Int16 _AcceptCount, Int32 _nConditionCount, Int32 _DailyDafaultCount)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.CAREER_CheckOpenNextEpisode", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _UserUID;

                SqlParameter paramEpisodeNumber = cmd.Parameters.Add("@EpisodeNumber", SqlDbType.SmallInt);
                paramEpisodeNumber.Direction = ParameterDirection.Input;
                paramEpisodeNumber.Value = _EpisodeNumber;

                SqlParameter paramAcceptCount = cmd.Parameters.Add("@NeedAcceptCount", SqlDbType.SmallInt);
                paramAcceptCount.Direction = ParameterDirection.Input;
                paramAcceptCount.Value = _AcceptCount;

                SqlParameter paramConditionCount = cmd.Parameters.Add( "@ConditionCount", SqlDbType.Int );
                paramConditionCount.Direction = ParameterDirection.Input;
                paramConditionCount.Value = _nConditionCount;

				SqlParameter paramDailyDefaultCount = cmd.Parameters.Add( "@DailyDefaultCount", SqlDbType.Int );
				paramDailyDefaultCount.Direction = ParameterDirection.Input;
				paramDailyDefaultCount.Value = _DailyDafaultCount;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR || result_code == DbResultCode.DB_CAREER_DAILY_STAGE_ERROR_DATA)
                    {
                        Logger.ErrLog( "CAREER_CheckOpenNextEpisode Fail : UserUID = {0}, EpisodeNumber = {1}, AcceptCount = {2}, ConditionCount = {3}, DailyDefaultCount = {4}", _UserUID, _EpisodeNumber, _AcceptCount, _nConditionCount, _DailyDafaultCount );
                    }
                }
            }

            return result_code;
        }

        public DbResultCode ChangeDefaultCharacter(Int64 lUserUID, Int64 lUnEquipItemUID, Int32 nSelecetCharacter)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.PERIOD_ExpireCharacter", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = lUserUID;

                SqlParameter paramUnEquipItemUID = cmd.Parameters.Add("@UnEquipItemUID", SqlDbType.BigInt);
                paramUnEquipItemUID.Direction = ParameterDirection.Input;
                paramUnEquipItemUID.Value = lUnEquipItemUID;

                SqlParameter paramSelectCharacter = cmd.Parameters.Add("@SelectCharacter", SqlDbType.Int);
                paramSelectCharacter.Direction = ParameterDirection.Input;
                paramSelectCharacter.Value = nSelecetCharacter;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("ChangeDefaultCharacter Fail : UserUID = {0}", lUserUID);
                    }
                }
            }

            return result_code;
        }

        public DbResultCode ChangeDefaultVehicleParts(Int64 lUserUID, Int64 lUnEquipItemUID, Int64 lEquipItemUID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.PERIOD_ExpireCharacter", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = lUserUID;

                SqlParameter paramUnEquipItemUID = cmd.Parameters.Add("@UnEquipItemUID", SqlDbType.BigInt);
                paramUnEquipItemUID.Direction = ParameterDirection.Input;
                paramUnEquipItemUID.Value = lUnEquipItemUID;

                SqlParameter paramEquipItemUID = cmd.Parameters.Add("@EquipItemUID", SqlDbType.BigInt);
                paramEquipItemUID.Direction = ParameterDirection.Input;
                paramEquipItemUID.Value = lEquipItemUID;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("ChangeDefaultVehicleParts Fail : UserUID = {0}", lUserUID);
                    }
                }
            }

            return result_code;
        }

        public DbResultCode UpdatePropertyInfo(DataTable kPropertyInfoList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.PROPERTY_UpdateMasteryInfo", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramPropertyInfoList = cmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                paramPropertyInfoList.Direction = ParameterDirection.Input;
                paramPropertyInfoList.Value = kPropertyInfoList;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("UpdatePropertyInfo Fail");
                    }
                }
            }

            return result_code;
        }

        public DbResultCode UpdatePropertyMasteryLevel(Int64 lUserUID, Int64 lItemUID, Int32 nPropertyID, Byte byMasteryLevel)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.PROPERTY_UpdateMasteryLevel", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = cmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = lUserUID;

                SqlParameter paramItemUID = cmd.Parameters.Add("@ItemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = lItemUID;

                SqlParameter paramPropertyID = cmd.Parameters.Add("@PropertyID", SqlDbType.Int);
                paramPropertyID.Direction = ParameterDirection.Input;
                paramPropertyID.Value = nPropertyID;

                SqlParameter paramMasteryLevel = cmd.Parameters.Add("@MasteryLevel", SqlDbType.TinyInt);
                paramMasteryLevel.Direction = ParameterDirection.Input;
                paramMasteryLevel.Value = byMasteryLevel;

                SqlDataReader result = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    result = cmd.ExecuteReader();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code == DbResultCode.DB_ERROR)
                    {
                        Logger.ErrLog("UpdatePropertyMasteryLevel Fail : ItemUID = {0}", lItemUID);
                    }
                }
            }

            return result_code;
        }

		public DbResultCode AddQuantitativeItem(Int64 nUserUID, Int32 nItemID, DataTable kRecipeElementList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
   			}
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.USER_GetQuantityItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = nUserUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@ItemID", SqlDbType.BigInt);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = nItemID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@RecipeElementList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = kRecipeElementList;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("USER_GetQuantityItem Fail UserUID = {0} ResultCode = {1}", nUserUID, result_code);
                }
            }
            return result_code;
        }

        public DbResultCode MakeProductWithRecipe(Int64 nUserUID, byte bCostType, int nCostValue, Int64 nRecipeItemUID, Int64 nProductItemUID, Int32 nProductItemID, Int32 nSkillID, DataTable kRecipeElementList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEM_ProduceWithRecipe", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = nUserUID;

                SqlParameter paramCostType = sqlCmd.Parameters.Add("@CostType", SqlDbType.TinyInt);
                paramCostType.Direction = ParameterDirection.Input;
                paramCostType.Value = bCostType;

                SqlParameter paramCostValue = sqlCmd.Parameters.Add("@CostValue", SqlDbType.Int);
                paramCostValue.Direction = ParameterDirection.Input;
                paramCostValue.Value = nCostValue;

                SqlParameter paramRecipeItemUID = sqlCmd.Parameters.Add("@RecipeItemUID", SqlDbType.BigInt);
                paramRecipeItemUID.Direction = ParameterDirection.Input;
                paramRecipeItemUID.Value = nRecipeItemUID;

                SqlParameter paramProductItemUID = sqlCmd.Parameters.Add("@ProductItemUID", SqlDbType.BigInt);
                paramProductItemUID.Direction = ParameterDirection.Input;
                paramProductItemUID.Value = nProductItemUID;

                SqlParameter paramProductItemID = sqlCmd.Parameters.Add("@ProductItemID", SqlDbType.Int);
                paramProductItemID.Direction = ParameterDirection.Input;
                paramProductItemID.Value = nProductItemID;

                SqlParameter paramSkillID = sqlCmd.Parameters.Add( "@SkillID", SqlDbType.Int );
                paramSkillID.Direction = ParameterDirection.Input;
                paramSkillID.Value = nSkillID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@RecipeElementList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = kRecipeElementList;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("ITEM_ProduceWithRecipe Fail UserUID = {0} ResultCode = {1}", nUserUID, result_code);
                }
            }
            return result_code;
        }

        public DbResultCode ElementCompose( Int64 UserUID, DataTable ElementItem )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return result_code;
            }
            using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.ITEM_Compose", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

//                 SqlParameter paramResultItemUID = sqlCmd.Parameters.Add( "@ResultItemUID", SqlDbType.BigInt );
//                 paramResultItemUID.Direction = ParameterDirection.Input;
//                 paramResultItemUID.Value = ResultItem.itemUID;
// 
//                 SqlParameter paramResultItemID = sqlCmd.Parameters.Add( "@ResultItemID", SqlDbType.Int );
//                 paramResultItemID.Direction = ParameterDirection.Input;
//                 paramResultItemID.Value = ResultItem.itemId;
// 
//                 SqlParameter paramResultAttachItemID = sqlCmd.Parameters.Add( "@ResultAttachItemID", SqlDbType.Int );
//                 paramResultAttachItemID.Direction = ParameterDirection.Input;
//                 paramResultAttachItemID.Value = ResultItem.attachItemID;
// 
//                 SqlParameter paramResultItemClass = sqlCmd.Parameters.Add( "@ResultItemClass", SqlDbType.TinyInt );
//                 paramResultItemClass.Direction = ParameterDirection.Input;
//                 paramResultItemClass.Value = ResultItem.itemClass;
// 
//                 SqlParameter paramMainTag = sqlCmd.Parameters.Add( "@MainTag", SqlDbType.TinyInt );
//                 paramMainTag.Direction = ParameterDirection.Input;
//                 paramMainTag.Value = ResultItem.mainTag;
// 
//                 SqlParameter paramSubTag = sqlCmd.Parameters.Add( "@SubTag", SqlDbType.TinyInt );
//                 paramSubTag.Direction = ParameterDirection.Input;
//                 paramSubTag.Value = ResultItem.subTag;
// 
//                 SqlParameter paramItemCount = sqlCmd.Parameters.Add( "@ItemCount", SqlDbType.Int );
//                 paramItemCount.Direction = ParameterDirection.Input;
//                 paramItemCount.Value = ResultItem.subTag;

                SqlParameter paramElementItem = sqlCmd.Parameters.Add( "@ElementItem", SqlDbType.Structured );
                paramElementItem.Direction = ParameterDirection.Input;
                paramElementItem.Value = ElementItem;
                
                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
                if( result_code != DbResultCode.SUCCESS )
                {
                    Logger.ErrLog( "ITEM_Compose Fail UserUID = {0} ResultCode = {1}", UserUID, result_code );
                }
            }

            return result_code;
        }

        public DbResultCode RecipeCompose( Int64 UserUID, DBUserItem ResultItem, DataTable DeleteItemList )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return result_code;
            }
            using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.ITEM_RecipeCompose", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramResultItemUID = sqlCmd.Parameters.Add( "@ResultItemUID", SqlDbType.BigInt );
                paramResultItemUID.Direction = ParameterDirection.Input;
                paramResultItemUID.Value = ResultItem.itemUID;

                SqlParameter paramResultItemID = sqlCmd.Parameters.Add( "@ResultItemID", SqlDbType.Int );
                paramResultItemID.Direction = ParameterDirection.Input;
                paramResultItemID.Value = ResultItem.itemId;

                SqlParameter paramResultAttachItemID = sqlCmd.Parameters.Add( "@ResultAttachItemID", SqlDbType.Int );
                paramResultAttachItemID.Direction = ParameterDirection.Input;
                paramResultAttachItemID.Value = ResultItem.attachItemID;

                SqlParameter paramResultItemClass = sqlCmd.Parameters.Add( "@ResultItemClass", SqlDbType.TinyInt );
                paramResultItemClass.Direction = ParameterDirection.Input;
                paramResultItemClass.Value = ResultItem.itemClass;

                SqlParameter paramItemCount = sqlCmd.Parameters.Add( "@ItemCount", SqlDbType.Int );
                paramItemCount.Direction = ParameterDirection.Input;
                paramItemCount.Value = ResultItem.quantity;

                SqlParameter paramDeleteItemList = sqlCmd.Parameters.Add( "@DeleteItemList", SqlDbType.Structured );
                paramDeleteItemList.Direction = ParameterDirection.Input;
                paramDeleteItemList.Value = DeleteItemList;
                
                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
                if( result_code != DbResultCode.SUCCESS )
                {
                    Logger.ErrLog( "ITEM_RecipeCompose Fail UserUID = {0} ResultCode = {1}", UserUID, result_code );
                }
            }

            return result_code;
        }

        public DbResultCode SetPromotionalInviteCnt(Int64 UserUID, Byte AddAmount, Byte InviteFriendCount)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.USER_SetPromotionalInviteCnt", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramInviteCnt = sqlCmd.Parameters.Add("@PromotionalInviteCnt", SqlDbType.TinyInt);
                paramInviteCnt.Direction = ParameterDirection.Input;
                paramInviteCnt.Value = AddAmount + InviteFriendCount;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("USER_SetPromotionalInviteCnt Fail UserUID = {0} ResultCode = {1}", UserUID, result_code);
                }
            }
            return result_code;
        }

		public DbResultCode RegistCoupon(String strPlatformID, Int64 lCouponID, Byte byPostType, Int32 _nPostAttachItemID, Int32 _nPostAttachItemQuantity, 
            String strPostMessage, Int64 lPostEndTick)
		{
			DbResultCode kDbResult = DbResultCode.DB_ERROR;

			try
			{
				if (Manager.IsOpen() == false)
					Manager.Open();
			}
			catch (System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				return kDbResult;
			}
			using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.USER_RegistCoupon", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@PlatformID", SqlDbType.VarChar);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = strPlatformID;

				SqlParameter paramCouponID = sqlCmd.Parameters.Add("@CouponID", SqlDbType.BigInt);
				paramCouponID.Direction = ParameterDirection.Input;
				paramCouponID.Value = lCouponID;

				SqlParameter paramPostType = sqlCmd.Parameters.Add("@PostType", SqlDbType.TinyInt);
				paramPostType.Direction = ParameterDirection.Input;
				paramPostType.Value = byPostType;

                SqlParameter paramPostAttachmentItemID          = sqlCmd.Parameters.Add("@PostAttachmentItemID", SqlDbType.Int);
                paramPostAttachmentItemID.Direction             = ParameterDirection.Input;
                paramPostAttachmentItemID.Value                 = _nPostAttachItemID;

                SqlParameter paramPostAttachmentItemQuantity    = sqlCmd.Parameters.Add("@PostAttachmentItemQuantity", SqlDbType.Int);
                paramPostAttachmentItemQuantity.Direction       = ParameterDirection.Input;
                paramPostAttachmentItemQuantity.Value           = _nPostAttachItemQuantity;

				SqlParameter paramPostMessage = sqlCmd.Parameters.Add("@PostMessage", SqlDbType.VarChar);
				paramPostMessage.Direction = ParameterDirection.Input;
				paramPostMessage.Value = strPostMessage;

				SqlParameter paramPostEndTime = sqlCmd.Parameters.Add("@PostEndTime", SqlDbType.DateTime);
				paramPostEndTime.Direction = ParameterDirection.Input;
				paramPostEndTime.Value = DateTime.FromBinary(lPostEndTick);

				dbConn.Open();

				Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
				kDbResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
				if (kDbResult != DbResultCode.SUCCESS)
				{
					Logger.ErrLog("USER_RegistCoupon Fail PlatformID = {0} ResultCode = {1}", strPlatformID, kDbResult);
				}
			}
			return kDbResult;
		}

        public DbResultCode CheckCareerOpenStage( Int64 _lUserUID, Int16 _sStageNumber )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand cmd = new SqlCommand( "dbo.CheckCareerOpenStage", dbConn );
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add( "@Ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add( "@UserID", SqlDbType.BigInt );
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _lUserUID;

                SqlParameter paramStageNumber = cmd.Parameters.Add( "@StageNumber", SqlDbType.SmallInt );
                paramStageNumber.Direction = ParameterDirection.Input;
                paramStageNumber.Value = _sStageNumber;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

        public DbResultCode ExpireCharacter(Int64 _lUserUID, Int64 _lItemUID, Int32 _nSelectedAvartar)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand cmd = new SqlCommand("dbo.PERIOD", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = cmd.Parameters.Add("@Ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUID = cmd.Parameters.Add("@UserID", SqlDbType.BigInt);
                paramUID.Direction = ParameterDirection.Input;
                paramUID.Value = _lUserUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = cmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }
        public DbResultCode ChangeNickNameByCoupon(Int64 _lUserUID, String _strNickName, DataTable _kCouponQuantityInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if (Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return result_code;
            }
            using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.COUPON_ChangeNickName", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramNickName = sqlCmd.Parameters.Add("@NickName", SqlDbType.NVarChar, (int)MAXLENGTH.NICKNAME);
                paramNickName.Direction = ParameterDirection.Input;
                paramNickName.Value = _strNickName;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kCouponQuantityInfo;

                dbConn.Open();

                Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                if (result_code != DbResultCode.SUCCESS)
                {
                    Logger.ErrLog("COUPON_ChangeNickName Fail UserUID = {0} ResultCode = {1}", _lUserUID, result_code);
                }
            }
            return result_code;
        }

        public DbResultCode GachaCarPartsByCoupon(Int64 _lUserUID, DBUserItem _kUserItem, DataTable _kCouponQuantityInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.COUPON_InsertItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@userUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@itemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = _kUserItem.itemUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@itemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = _kUserItem.itemId;

                SqlParameter paramAttachItemID = sqlCmd.Parameters.Add("@attachItemID", SqlDbType.Int);
                paramAttachItemID.Direction = ParameterDirection.Input;
                paramAttachItemID.Value = _kUserItem.attachItemID;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add("@upgradeLevel", SqlDbType.TinyInt);
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = (Byte)_kUserItem.upgradeLevel;

                SqlParameter paramIsEquip = sqlCmd.Parameters.Add("@isEquip", SqlDbType.Bit);
                paramIsEquip.Direction = ParameterDirection.Input;
                paramIsEquip.Value = _kUserItem.isEquip;

                SqlParameter paramUnEquipItemUID = sqlCmd.Parameters.Add("@unEquipItemUID", SqlDbType.BigInt);
                paramUnEquipItemUID.Direction = ParameterDirection.Input;
                paramUnEquipItemUID.Value = 0;

                SqlParameter paramCurrentTime = sqlCmd.Parameters.Add("@currTime", SqlDbType.DateTime);
                paramCurrentTime.Direction = ParameterDirection.Input;
                paramCurrentTime.Value = DateTime.Now;

                SqlParameter paramIsPeriod = sqlCmd.Parameters.Add("@isPeriod", SqlDbType.Bit);
                paramIsPeriod.Direction = ParameterDirection.Input;
                paramIsPeriod.Value = _kUserItem.isPeriod;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = new DateTime(_kUserItem.lExpirationDateTick);

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kCouponQuantityInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("COUPON_InsertItem Fail UserUID = {0} ItemID = {1}, Result_Code = {2}", _lUserUID, _kUserItem.itemId, result_code);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GachaPaintByCoupon(Int64 _lUserUID, Int64 _lItemUID, Int32 _nPaintItemID, DataTable _kCouponQuantityInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.COUPON_ApplyPaintToCarParts", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@ItemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = _lItemUID;

                SqlParameter paramPaintItemID = sqlCmd.Parameters.Add("@PaintItemID", SqlDbType.Int);
                paramPaintItemID.Direction = ParameterDirection.Input;
                paramPaintItemID.Value = _nPaintItemID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kCouponQuantityInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("COUPON_ApplyPaintToCarParts Fail UserUID = {0} ItemUID = {1} PaintItemID = {2}, Result_Code: {3}", 
                            _lUserUID, _lItemUID, _nPaintItemID, result_code);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

		public DbResultCode OpenHiddenSkillByCoupon( Int64 lUserUID , Int64 lItemUID , Int32 nSkillID , Int32 nCouponItemID , DataTable kCouponQuantityInfo , out Int32 nPrevSkillID )
		{
			nPrevSkillID = 0;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( lUserUID );
				if ( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.COUPON_OpenHiddenSkill" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = lUserUID;

				SqlParameter paramItemUID = sqlCmd.Parameters.Add( "@ItemUID" , SqlDbType.BigInt );
				paramItemUID.Direction = ParameterDirection.Input;
				paramItemUID.Value = lItemUID;

				SqlParameter paramSkillID = sqlCmd.Parameters.Add( "@SkillID" , SqlDbType.Int );
				paramSkillID.Direction = ParameterDirection.Input;
				paramSkillID.Value = nSkillID;

				SqlParameter paramCouponItemID = sqlCmd.Parameters.Add( "@CouponItemID" , SqlDbType.Int );
				paramCouponItemID.Direction = ParameterDirection.Input;
				paramCouponItemID.Value = nCouponItemID;

				SqlParameter paramCouponItemModifyPlan = sqlCmd.Parameters.Add( "@CouponQuantityInfoList" , SqlDbType.Structured );
				paramCouponItemModifyPlan.Direction = ParameterDirection.Input;
				paramCouponItemModifyPlan.Value = kCouponQuantityInfo;

				SqlParameter paramPrevSkillID = sqlCmd.Parameters.Add("@PrevSkillID", SqlDbType.Int);
				paramPrevSkillID.Direction = ParameterDirection.Output;
				paramPrevSkillID.Value = 0;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					DbResultCode result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "COUPON_OpenHiddenSkill Fail UserUID = {0}, Result_Code: {1}" , lUserUID , result_code );
					}
					else
					{
						nPrevSkillID = (int)paramPrevSkillID.Value;
					}

					return result_code;
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					return DbResultCode.DB_ERROR;
				}
			}
		}

        public DbResultCode UpgradeCarPartsByCoupon(Int64 _lUserUID, Int64 _lTargetItemUID, Int32 _nTargetItemID, Byte _byUpgradeLevel, DataTable _kCouponQuantityInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.COUPON_UpgradeCarParts", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramTargetItemUID = sqlCmd.Parameters.Add("@TargetItemUID", SqlDbType.BigInt);
                paramTargetItemUID.Direction = ParameterDirection.Input;
                paramTargetItemUID.Value = _lTargetItemUID;

                SqlParameter paramTargetItemID = sqlCmd.Parameters.Add("@TargetItemID", SqlDbType.Int);
                paramTargetItemID.Direction = ParameterDirection.Input;
                paramTargetItemID.Value = _nTargetItemID;

                SqlParameter paramTargetItemUpgradeLevel = sqlCmd.Parameters.Add("@TargetItemUpgradeLevel", SqlDbType.TinyInt);
                paramTargetItemUpgradeLevel.Direction = ParameterDirection.Input;
                paramTargetItemUpgradeLevel.Value = _byUpgradeLevel;           

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kCouponQuantityInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("COUPON_UpgradeCarParts Fail UserUID = {0} ItemUID = {1} ItemID = {2}, UpgradeLevel = {3}, Result_Code: {4}",
                            _lUserUID, _lTargetItemUID, _nTargetItemID, _byUpgradeLevel, result_code);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode BuyChannelTicket(Int64 _lUserUID, Byte _byTicketPriceType, Int32 _nTicketPrice, Int32 _nCouponItemID, Byte _byMainTag, Byte _bySubTag, DataTable _kTicketQuantityInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_BuyChannelTicket", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

				SqlParameter paramTicketPriceType = sqlCmd.Parameters.Add("@TicketPriceType", SqlDbType.TinyInt);
                paramTicketPriceType.Direction = ParameterDirection.Input;
                paramTicketPriceType.Value = _byTicketPriceType;

                SqlParameter paramTicketPrice = sqlCmd.Parameters.Add("@TicketPrice", SqlDbType.Int);
                paramTicketPrice.Direction = ParameterDirection.Input;
                paramTicketPrice.Value = -_nTicketPrice;

                SqlParameter paramCouponItemID = sqlCmd.Parameters.Add("@CouponItemID", SqlDbType.Int);
                paramCouponItemID.Direction = ParameterDirection.Input;
                paramCouponItemID.Value = _nCouponItemID;

                SqlParameter paramMainTag = sqlCmd.Parameters.Add("@MainTag", SqlDbType.TinyInt);
                paramMainTag.Direction = ParameterDirection.Input;
                paramMainTag.Value = _byMainTag;

                SqlParameter paramSubTag = sqlCmd.Parameters.Add("@SubTag", SqlDbType.TinyInt);
                paramSubTag.Direction = ParameterDirection.Input;
                paramSubTag.Value = _bySubTag;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kTicketQuantityInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("P_BuyChannelTicket Fail UserUID = {0}, TicketPriceType = {1}, TicketPrice : {2}, Result_Code : {3}",
                            _lUserUID, _byTicketPriceType, _nTicketPrice, result_code);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateTicketQuantity(Int64 _lUserUID, DataTable _kTicketQuantityInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEM_ChangeQuantity", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@RecipeElementList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kTicketQuantityInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("ITEM_ChangeQuantity Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GiveGoodsByCoupon( Int64 lUserUID, Byte byGoodsType, Int32 nIncrGoodsAmount, Int32 nCouponItemID, DataTable kCouponModifyPlan )
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( lUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return DbResultCode.DB_ERROR;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.COUPON_GiveGoods", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = lUserUID;

                SqlParameter paramGoodsType = sqlCmd.Parameters.Add( "@GoodsType", SqlDbType.TinyInt );
                paramGoodsType.Direction = ParameterDirection.Input;
                paramGoodsType.Value = byGoodsType;

                SqlParameter paramQuantijtyVariation = sqlCmd.Parameters.Add( "@QuantityVariation", SqlDbType.Int );
                paramQuantijtyVariation.Direction = ParameterDirection.Input;
                paramQuantijtyVariation.Value = nIncrGoodsAmount;

                SqlParameter paramCouponItemID = sqlCmd.Parameters.Add( "@CouponItemID", SqlDbType.Int );
                paramCouponItemID.Direction = ParameterDirection.Input;
                paramCouponItemID.Value = nCouponItemID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add( "@CouponQuantityInfoList", SqlDbType.Structured );
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = kCouponModifyPlan;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "COUPON_GiveGoods Fail UserUID = {0}, Result_Code: {1}", lUserUID, result_code );
                    }

                    return result_code;
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    return DbResultCode.DB_ERROR;
                }
            }
        }

		public DbResultCode GiveItemByCoupon( Int64 lUserUID , Int64 lItemUID , Int32 nItemID , Byte byItemLevel , Int32 nCouponItemID , DataTable kCouponModifyPlan )
		{
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( lUserUID );
				if ( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.COUPON_GiveItem" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = lUserUID;

				SqlParameter paramItemUID = sqlCmd.Parameters.Add( "@ItemUID" , SqlDbType.BigInt );
				paramItemUID.Direction = ParameterDirection.Input;
				paramItemUID.Value = lItemUID;

				SqlParameter paramItemID = sqlCmd.Parameters.Add( "@ItemID" , SqlDbType.Int );
				paramItemID.Direction = ParameterDirection.Input;
				paramItemID.Value = nItemID;

				SqlParameter paramItemLevel = sqlCmd.Parameters.Add( "@ItemLevel" , SqlDbType.TinyInt );
				paramItemLevel.Direction = ParameterDirection.Input;
				paramItemLevel.Value = lItemUID;

				SqlParameter paramCouponItemID = sqlCmd.Parameters.Add( "@CouponItemID" , SqlDbType.Int );
				paramCouponItemID.Direction = ParameterDirection.Input;
				paramCouponItemID.Value = nCouponItemID;

				SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add( "@CouponQuantityInfoList" , SqlDbType.Structured );
				paramRecipeElementList.Direction = ParameterDirection.Input;
				paramRecipeElementList.Value = kCouponModifyPlan;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					DbResultCode result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "COUPON_GiveItem Fail UserUID = {0}, Result_Code: {1}" , lUserUID , result_code );
					}

					return result_code;
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					return DbResultCode.DB_ERROR;
				}
			}
		}

		public DbResultCode ApplyPaintToBodyPartsByCoupon( Int64 lUserUID , Int64 lItemUID , Int32 nItemID , Int32 nCouponItemID , DataTable kCouponModifyPlan )
		{
			return this.GachaPaintByCoupon( lUserUID , lItemUID , nItemID , kCouponModifyPlan );
		}

        public DbResultCode GiveElementItemByCoupon(Int64 lUserUID, Int32 nItemID, DataTable kElementModifyPlan, Int32 nCouponItemID, DataTable kCouponModifyPlan)
		{
			SqlManager sqlMgr = null;
			try
			{
                sqlMgr = GetDB(lUserUID);
                if (sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}
            catch (System.Exception ex)
			{
                Logger.ExceptionLog(ex);
				return DbResultCode.DB_ERROR;
			}

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
                SqlCommand sqlCmd = new SqlCommand("dbo.COUPON_GiveElementItem", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = lUserUID;

                SqlParameter paramElementItemID = sqlCmd.Parameters.Add("@ElementItemID", SqlDbType.Int);
				paramElementItemID.Direction = ParameterDirection.Input;
				paramElementItemID.Value = nItemID;

                SqlParameter paramElementList = sqlCmd.Parameters.Add("@ElementItemList", SqlDbType.Structured);
				paramElementList.Direction = ParameterDirection.Input;
				paramElementList.Value = kElementModifyPlan;

                SqlParameter paramCouponItemID = sqlCmd.Parameters.Add("@CouponItemID", SqlDbType.Int);
				paramCouponItemID.Direction = ParameterDirection.Input;
				paramCouponItemID.Value = nCouponItemID;

                SqlParameter paramCouponList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
				paramCouponList.Direction = ParameterDirection.Input;
				paramCouponList.Value = kCouponModifyPlan;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
					{
                        Logger.ErrLog("COUPON_GiveElementItem Fail UserUID = {0}, Result_Code: {1}", lUserUID, result_code);
					}

					return result_code;
				}
                catch (Exception ex)
				{
                    Logger.ExceptionLog(ex);
					return DbResultCode.DB_ERROR;
				}
			}
		}

        public DbResultCode BuyMagicBoxByMagicPoint(Int64 _lUserUID, Byte _byGoodsType, Int32 _nGoodsQuantity, DataTable _kGoodsInfoList, Byte _byInCreaseCaues, Byte _byDecreaseCause)
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_BuyMagicBox", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramGoodsType = sqlCmd.Parameters.Add("@CostGoodsType", SqlDbType.TinyInt);
                paramGoodsType.Direction = ParameterDirection.Input;
                paramGoodsType.Value = _byGoodsType;

                SqlParameter paramGoodsQuantity = sqlCmd.Parameters.Add("@CostGoodsQuantity", SqlDbType.Int);
                paramGoodsQuantity.Direction = ParameterDirection.Input;
                paramGoodsQuantity.Value = _nGoodsQuantity;

                SqlParameter paramGoodsInfoList = sqlCmd.Parameters.Add("@GoodsInfoList", SqlDbType.Structured);
                paramGoodsInfoList.Direction = ParameterDirection.Input;
                paramGoodsInfoList.Value = _kGoodsInfoList;

                SqlParameter paramIncreaseCause = sqlCmd.Parameters.Add("@IncreaseChangeCause", SqlDbType.TinyInt);
                paramIncreaseCause.Direction = ParameterDirection.Input;
                paramIncreaseCause.Value = _byInCreaseCaues;

                SqlParameter paramDecreaseCause = sqlCmd.Parameters.Add("@DecreaseChangeCause", SqlDbType.TinyInt);
                paramDecreaseCause.Direction = ParameterDirection.Input;
                paramDecreaseCause.Value = _byDecreaseCause;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("P_BuyMagicBox Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode BuyNonQuantitativeItem(Int64 _lUserUID, DBUserItem _kUserItem, Byte _byCostGoodsType, Int32 _nCostGoodsQuantity, Byte _byDecreaseCause)
        {
            DataTable kPropertyInfoList = this.GetPropertyIDTable(_kUserItem.itemUID, _kUserItem.itemId);
            if(null != kPropertyInfoList)
            {
                return this.BuyNonQuantitativeItemWithProperty(_lUserUID, _kUserItem, _byCostGoodsType, _nCostGoodsQuantity, _byDecreaseCause, kPropertyInfoList);
            }

            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_BuyNonQuantitativeItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramGoodsType = sqlCmd.Parameters.Add("@CostGoodsType", SqlDbType.TinyInt);
                paramGoodsType.Direction = ParameterDirection.Input;
                paramGoodsType.Value = _byCostGoodsType;

                SqlParameter paramGoodsQuantity = sqlCmd.Parameters.Add("@CostGoodsQuantity", SqlDbType.Int);
                paramGoodsQuantity.Direction = ParameterDirection.Input;
                paramGoodsQuantity.Value = _nCostGoodsQuantity;

                SqlParameter paramDecreaseCause = sqlCmd.Parameters.Add("@DecreaseChangeCause", SqlDbType.TinyInt);
                paramDecreaseCause.Direction = ParameterDirection.Input;
                paramDecreaseCause.Value = _byDecreaseCause;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@ItemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = _kUserItem.itemUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@ItemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = _kUserItem.itemId;

                SqlParameter paramAttachItemID = sqlCmd.Parameters.Add("@AttachmentItemID", SqlDbType.Int);
                paramAttachItemID.Direction = ParameterDirection.Input;
                paramAttachItemID.Value = _kUserItem.attachItemID;

                SqlParameter paramIsEquip = sqlCmd.Parameters.Add("@IsEquip", SqlDbType.Bit);
                paramIsEquip.Direction = ParameterDirection.Input;
                paramIsEquip.Value = _kUserItem.isEquip;

                SqlParameter paramQuantity = sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int);
                paramQuantity.Direction = ParameterDirection.Input;
                paramQuantity.Value = _kUserItem.quantity;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add("@UpgradeLevel", SqlDbType.TinyInt);
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = _kUserItem.upgradeLevel;

                SqlParameter paramMainTag = sqlCmd.Parameters.Add("@MainTag", SqlDbType.TinyInt);
                paramMainTag.Direction = ParameterDirection.Input;
                paramMainTag.Value = _kUserItem.mainTag;

                SqlParameter paramSubTag = sqlCmd.Parameters.Add("@SubTag", SqlDbType.TinyInt);
                paramSubTag.Direction = ParameterDirection.Input;
                paramSubTag.Value = _kUserItem.subTag;

                SqlParameter paramIsPeriod = sqlCmd.Parameters.Add("@IsPeriod", SqlDbType.Bit);
                paramIsPeriod.Direction = ParameterDirection.Input;
                paramIsPeriod.Value = _kUserItem.isPeriod;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = new DateTime(_kUserItem.lExpirationDateTick);

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("P_BuyNonQuantitativeItem Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode BuyNonQuantitativeItemWithProperty(Int64 _lUserUID, DBUserItem _kUserItem, Byte _byCostGoodsType, Int32 _nCostGoodsQuantity, Byte _byDecreaseCause, DataTable _kPropertyInfoList)
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_BuyNonQuantitativeItemWithProperty", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramGoodsType = sqlCmd.Parameters.Add("@CostGoodsType", SqlDbType.TinyInt);
                paramGoodsType.Direction = ParameterDirection.Input;
                paramGoodsType.Value = _byCostGoodsType;

                SqlParameter paramGoodsQuantity = sqlCmd.Parameters.Add("@CostGoodsQuantity", SqlDbType.Int);
                paramGoodsQuantity.Direction = ParameterDirection.Input;
                paramGoodsQuantity.Value = _nCostGoodsQuantity;

                SqlParameter paramDecreaseCause = sqlCmd.Parameters.Add("@DecreaseChangeCause", SqlDbType.TinyInt);
                paramDecreaseCause.Direction = ParameterDirection.Input;
                paramDecreaseCause.Value = _byDecreaseCause;

                SqlParameter paramItemUID = sqlCmd.Parameters.Add("@ItemUID", SqlDbType.BigInt);
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = _kUserItem.itemUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@ItemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = _kUserItem.itemId;

                SqlParameter paramAttachItemID = sqlCmd.Parameters.Add("@AttachmentItemID", SqlDbType.Int);
                paramAttachItemID.Direction = ParameterDirection.Input;
                paramAttachItemID.Value = _kUserItem.attachItemID;

                SqlParameter paramIsEquip = sqlCmd.Parameters.Add("@IsEquip", SqlDbType.Bit);
                paramIsEquip.Direction = ParameterDirection.Input;
                paramIsEquip.Value = _kUserItem.isEquip;

                SqlParameter paramQuantity = sqlCmd.Parameters.Add("@Quantity", SqlDbType.Int);
                paramQuantity.Direction = ParameterDirection.Input;
                paramQuantity.Value = _kUserItem.quantity;

                SqlParameter paramUpgradeLevel = sqlCmd.Parameters.Add("@UpgradeLevel", SqlDbType.TinyInt);
                paramUpgradeLevel.Direction = ParameterDirection.Input;
                paramUpgradeLevel.Value = _kUserItem.upgradeLevel;

                SqlParameter paramMainTag = sqlCmd.Parameters.Add("@MainTag", SqlDbType.TinyInt);
                paramMainTag.Direction = ParameterDirection.Input;
                paramMainTag.Value = _kUserItem.mainTag;

                SqlParameter paramSubTag = sqlCmd.Parameters.Add("@SubTag", SqlDbType.TinyInt);
                paramSubTag.Direction = ParameterDirection.Input;
                paramSubTag.Value = _kUserItem.subTag;

                SqlParameter paramIsPeriod = sqlCmd.Parameters.Add("@IsPeriod", SqlDbType.Bit);
                paramIsPeriod.Direction = ParameterDirection.Input;
                paramIsPeriod.Value = _kUserItem.isPeriod;

                SqlParameter paramExpirationDate = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                paramExpirationDate.Direction = ParameterDirection.Input;
                paramExpirationDate.Value = new DateTime(_kUserItem.lExpirationDateTick);

                SqlParameter paramPropertyIDList = sqlCmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                paramPropertyIDList.Direction = ParameterDirection.Input;
                paramPropertyIDList.Value = _kPropertyInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("P_BuyNonQuantitativeItemWithProperty Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    return DbResultCode.DB_ERROR;
                }
            }
        }

		public DbResultCode GetGachaGuage( Int64 _UserUID, out Int32 _GachaGuagePoint, out Int32 _OneLevelCount, out Int32 _TwoLevelCount, out Int32 _TriLevelCount )
		{
			_GachaGuagePoint = 0;
            _OneLevelCount = 0;
            _TwoLevelCount = 0;
            _TriLevelCount = 0;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( _UserUID );
				if ( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetGachaGuage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _UserUID;

				SqlParameter paramGachaGuagePoint = sqlCmd.Parameters.Add( "@GachaGuage", SqlDbType.Int );
				paramGachaGuagePoint.Direction = ParameterDirection.Output;
				paramGachaGuagePoint.Value = _GachaGuagePoint;

                SqlParameter paramOneLevelCount = sqlCmd.Parameters.Add( "@OneLevelCount", SqlDbType.Int );
				paramOneLevelCount.Direction = ParameterDirection.Output;
				paramOneLevelCount.Value = _OneLevelCount;

                SqlParameter paramTwoLevelCount = sqlCmd.Parameters.Add( "@TwoLevelCount", SqlDbType.Int );
				paramTwoLevelCount.Direction = ParameterDirection.Output;
				paramTwoLevelCount.Value = _TwoLevelCount;

                SqlParameter paramTriLevelCount = sqlCmd.Parameters.Add( "@TriLevelCount", SqlDbType.Int );
				paramTriLevelCount.Direction = ParameterDirection.Output;
				paramTriLevelCount.Value = _TriLevelCount;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					DbResultCode result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
					{
						if( result_code != DbResultCode.DB_GACHA_GUAGE_USER_DATA_NOT_FOUND )
							Logger.ErrLog( "P_GetGachaGuage Fail UserUID = {0}, Result_Code: {1}", _UserUID, result_code );
					}
					else
                    {
                        _GachaGuagePoint = (Int32)paramGachaGuagePoint.Value;
                        _OneLevelCount = (Int32)paramOneLevelCount.Value;
                        _TwoLevelCount = (Int32)paramTwoLevelCount.Value;
                        _TriLevelCount = (Int32)paramTriLevelCount.Value;
                    }
					return result_code;
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					return DbResultCode.DB_ERROR;
				}
			}
		}

		public DbResultCode UpdateGachaGuage(Int64 _UserUID, Int32 _GuagePoint, Byte _UpdateGuageType)
		{
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( _UserUID );
				if ( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateGachaGuage", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _UserUID;

				SqlParameter paramGuagePoint = sqlCmd.Parameters.Add( "@GachaGuage", SqlDbType.Int );
				paramGuagePoint.Direction = ParameterDirection.Input;
				paramGuagePoint.Value = _GuagePoint;

                SqlParameter paramUpdateGuageType = sqlCmd.Parameters.Add( "@UpdateGuageType", SqlDbType.TinyInt );
                paramUpdateGuageType.Direction = ParameterDirection.Input;
                paramUpdateGuageType.Value = _UpdateGuageType;
                
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					DbResultCode result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "P_UpdateGachaGuage Fail UserUID = {0}, GuagePoint = {1}, Result_Code: {2}", _UserUID, _GuagePoint, result_code );
					}

					return result_code;
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					return DbResultCode.DB_ERROR;
				}
			}
		}

        public DbResultCode BuyNonQuantitativeItemListWithProperty(Int64 _lUserUID, Byte _byCostGoodsType, Int32 _nCostGoodsQuantity, Byte _byDecreaseCause, DataTable _kItemInfoList, DataTable _kPropertyInfoList)
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return DbResultCode.DB_ERROR;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_BuyNonQuantitativeItemListWithProperty", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramGoodsType = sqlCmd.Parameters.Add("@CostGoodsType", SqlDbType.TinyInt);
                paramGoodsType.Direction = ParameterDirection.Input;
                paramGoodsType.Value = _byCostGoodsType;

                SqlParameter paramGoodsQuantity = sqlCmd.Parameters.Add("@CostGoodsQuantity", SqlDbType.Int);
                paramGoodsQuantity.Direction = ParameterDirection.Input;
                paramGoodsQuantity.Value = -_nCostGoodsQuantity;

                SqlParameter paramDecreaseCause = sqlCmd.Parameters.Add("@DecreaseChangeCause", SqlDbType.TinyInt);
                paramDecreaseCause.Direction = ParameterDirection.Input;
                paramDecreaseCause.Value = _byDecreaseCause;

                SqlParameter paramItemInfoList = sqlCmd.Parameters.Add("@ItemInfoList", SqlDbType.Structured);
                paramItemInfoList.Direction = ParameterDirection.Input;
                paramItemInfoList.Value = _kItemInfoList;
				paramItemInfoList.TypeName = "dbo.ItemInfoList";

                SqlParameter paramPropertyInfoList = sqlCmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                paramPropertyInfoList.Direction = ParameterDirection.Input;
                paramPropertyInfoList.Value = _kPropertyInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("P_BuyNonQuantitativeItemListWithProperty Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode TotalGachaByCoupon(Int64 _lUserUID, DataTable _kItemInfoList, DataTable _kPropertyInfoList, DataTable _kCouponQuantityInfoList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.COUPON_TotalGacha", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramItemInfoList = sqlCmd.Parameters.Add("@ItemInfoList", SqlDbType.Structured);
                paramItemInfoList.Direction = ParameterDirection.Input;
                paramItemInfoList.Value = _kItemInfoList;

                SqlParameter paramPropertyInfoList = sqlCmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                paramPropertyInfoList.Direction = ParameterDirection.Input;
                paramPropertyInfoList.Value = _kPropertyInfoList;

                SqlParameter paramCouponQuantityInfoList = sqlCmd.Parameters.Add("@CouponQuantityInfoList", SqlDbType.Structured);
                paramCouponQuantityInfoList.Direction = ParameterDirection.Input;
                paramCouponQuantityInfoList.Value = _kCouponQuantityInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("COUPON_TotalGacha Fail UserUID = {0}, Result_Code = {1}", _lUserUID, result_code);

                    return result_code;
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode CheckSocialRankReward( Int64 _lUserUID, out bool _bIsReward )
        {
            _bIsReward = false;
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_CheckSocialRankReward", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramIsReward = sqlCmd.Parameters.Add( "@IsReward", SqlDbType.Bit );
                paramIsReward.Direction = ParameterDirection.Output;
                paramIsReward.Value = _bIsReward;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "COUPON_TotalGacha Fail UserUID = {0}, Result_Code = {1}", _lUserUID, result_code );

                    _bIsReward = ( bool )paramIsReward.Value;
                    return result_code;
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateSocialItemReward( Int64 _lUserUID, Int32 _nRewardID )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSocialItemRankReward", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramRewardID = sqlCmd.Parameters.Add( "@RewardID", SqlDbType.Int );
                paramRewardID.Direction = ParameterDirection.Input;
                paramRewardID.Value = _nRewardID;


                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateSocialItemReward Fail UserUID = {0}, Result_Code = {1}, RewardID = {2}", _lUserUID, result_code, _nRewardID );

                    return result_code;
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateSocialSpeedReward( Int64 _lUserUID, Int32 _nTrackID, Int32 _nRewardID )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if ( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSocialSpeedRankReward", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUnitUID.Direction = ParameterDirection.Input;
                paramUnitUID.Value = _lUserUID;

                SqlParameter paramTrackID = sqlCmd.Parameters.Add( "@TrackID", SqlDbType.Int );
                paramTrackID.Direction = ParameterDirection.Input;
                paramTrackID.Value = _nTrackID;

                SqlParameter paramRewardID = sqlCmd.Parameters.Add( "@RewardID", SqlDbType.Int );
                paramRewardID.Direction = ParameterDirection.Input;
                paramRewardID.Value = _nRewardID;


                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    result_code = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                    if ( result_code != DbResultCode.SUCCESS )
                        Logger.ErrLog( "UpdateSocialSpeedReward Fail UserUID = {0}, Result_Code = {1}, TrackID = {2}, RewardID = {3}", _lUserUID, result_code, _nTrackID, _nRewardID );

                    return result_code;
                }
                catch ( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch ( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return DbResultCode.SUCCESS;
        }

		public ClientErrorMessage GetItemBookList( Int64 _lUserUID, ref List<DBItemBook> _liItemBook )
        {
			ClientErrorMessage resulCode	= ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr				= null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return resulCode;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.ITEMBOOK_GetList", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult	= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction		= ParameterDirection.ReturnValue;
 
                SqlParameter paramUserUID	= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction		= ParameterDirection.Input;
                paramUserUID.Value			= _lUserUID;

                SqlDataReader reader = null;
                try
                {
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
							_liItemBook.Add
								(
									new DBItemBook( (Int32)reader["ItemID"], (bool)reader["IsNew"] )
								);
                        }
                    }

                    reader.Close();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
				finally
				{
					resulCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
				}
            }

			return resulCode;
        }

		public ClientErrorMessage OpenItemBook( Int64 _lUserUID, Int32 _nItemID )
        {
			ClientErrorMessage resultCode	= ClientErrorMessage.ERROR_NORMAL;
            SqlManager sqlMgr				= null;
			try
			{
				sqlMgr = GetDB( _lUserUID );
				if( false == sqlMgr.IsOpen() )
					sqlMgr.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return resultCode;
			}

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd	= new SqlCommand("dbo.ITEMBOOK_OpenEntity", dbConn);
                sqlCmd.CommandType	= CommandType.StoredProcedure;

                SqlParameter paramResult	= sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction		= ParameterDirection.ReturnValue;

                SqlParameter paramUserUID	= sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction		= ParameterDirection.Input;
                paramUserUID.Value			= _lUserUID;

                SqlParameter paramItemID	= sqlCmd.Parameters.Add("@ItemID", SqlDbType.Int);
                paramItemID.Direction		= ParameterDirection.Input;
                paramItemID.Value			= _nItemID;

                try
                {
                    dbConn.Open();
					sqlCmd.ExecuteNonQuery();
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
				finally
				{
					resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
				}
            }

			return resultCode;
        }

        public DbResultCode InsertItemBook(Int64 _lUserUID, Int32 _nItemID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.ITEMBOOK_InsertEntity", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramItemID = sqlCmd.Parameters.Add("@ItemID", SqlDbType.Int);
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = _nItemID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("ITEMBOOK_InsertEntity is Fail UserUID: {0}, ItemID: {1}, retCode: {2}", _lUserUID, _nItemID, result_code);
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

		public ClientErrorMessage InsertItemBookArray( Int64 _lUserUID, DataTable _ItemIDList )
		{
			ClientErrorMessage resultCode	= ClientErrorMessage.ERROR_NORMAL;
			SqlManager sqlMgr				= null;
			try
			{
				sqlMgr = GetDB( _lUserUID );
				if( false == sqlMgr.IsOpen() )
					sqlMgr.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return resultCode;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd	= new SqlCommand( "dbo.ITEMBOOK_InsertEntity", dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult		= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction			= ParameterDirection.ReturnValue;

				SqlParameter paramUserUID		= sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction			= ParameterDirection.Input;
				paramUserUID.Value				= _lUserUID;

				SqlParameter paramItemIDList    = sqlCmd.Parameters.Add( "@ItemIDList", SqlDbType.Structured );
				paramItemIDList.Direction		= ParameterDirection.Input;
				paramItemIDList.Value			= _ItemIDList;

                try
                {
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
					resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
                }
            }

			return resultCode;
		}

        public DbResultCode GetSmartPopupInfo(Int64 _lUserUID, out DBSmartPopup _kSmartPopupInfo)
        {
            _kSmartPopupInfo = null;

            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserSmartPopupInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            _kSmartPopupInfo = new DBSmartPopup((Byte)reader["PurchaseCount"], (Byte)reader["ConciliationCount"],
                                (Byte)reader["OpenPopupCount"], ((DateTime)reader["LastUpdateDate"]).Ticks);
                        }
                    }

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_GetUserSmartPopupInfo is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateSmartPopupInfo(Int64 _lUserUID, DBSmartPopup _kSmartPopupInfo)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if (sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using (SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateUserSmartpopInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramPurchaseCount = sqlCmd.Parameters.Add("@PurchaseCount", SqlDbType.TinyInt);
                paramPurchaseCount.Direction = ParameterDirection.Input;
                paramPurchaseCount.Value = _kSmartPopupInfo.PurchaseCount;

                SqlParameter paramConciliationCount = sqlCmd.Parameters.Add("@ConciliationCount", SqlDbType.TinyInt);
                paramConciliationCount.Direction = ParameterDirection.Input;
                paramConciliationCount.Value = _kSmartPopupInfo.ConciliationCount;

                SqlParameter paramOpenPopupCount = sqlCmd.Parameters.Add("@OpenPopupCount", SqlDbType.TinyInt);
                paramOpenPopupCount.Direction = ParameterDirection.Input;
                paramOpenPopupCount.Value = _kSmartPopupInfo.OpenPopupCount;

                SqlParameter paramLastUpdateDate = sqlCmd.Parameters.Add("@LastUpdateDate", SqlDbType.DateTime);
                paramLastUpdateDate.Direction = ParameterDirection.Input;
                paramLastUpdateDate.Value = new DateTime(_kSmartPopupInfo.LastUpdateDateTicks);

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if (DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_UpdateUserSmartpopInfo is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                    }
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

		public DbResultCode Season1UserMoveToSeason2(Int64 _lUserUID, String _strPlatformID, out Int32 TotalRewardStar, out Int32 _nTotalCollectStar, out Byte _byUserLevel)
		{
			_nTotalCollectStar  = 0;
            _byUserLevel        = 0;
            TotalRewardStar     = 0;

			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lUserUID);
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}

			catch( System.Exception ex )
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.P_USER_MoveFromSeason1", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult            = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction               = ParameterDirection.ReturnValue;
				paramResult.Value                   = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID           = sqlCmd.Parameters.Add("@PlatformID", SqlDbType.NVarChar, 40);
				paramUserUID.Direction              = ParameterDirection.Input;
				paramUserUID.Value                  = _strPlatformID;

                SqlParameter paramTotalRewardStar   = sqlCmd.Parameters.Add("@TotalRewardStar", SqlDbType.Int);
                paramTotalRewardStar.Direction      = ParameterDirection.Output;
                paramTotalRewardStar.Value          = TotalRewardStar;

                SqlParameter paramUserLevel         = sqlCmd.Parameters.Add( "@UserLevel", SqlDbType.TinyInt );
                paramUserLevel.Direction            = ParameterDirection.Output;
                paramUserLevel.Value                = _byUserLevel;

                SqlParameter paramTotalCollectStar  = sqlCmd.Parameters.Add( "@TotalCollectStar", SqlDbType.Int );
                paramTotalCollectStar.Direction     = ParameterDirection.Output;
                paramTotalCollectStar.Value         = _nTotalCollectStar;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();

					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS == result_code)
					{
                        _nTotalCollectStar  = Convert.ToInt32( paramTotalCollectStar.Value );
                        _byUserLevel        = Convert.ToByte( paramUserLevel.Value );
                        TotalRewardStar     = Convert.ToInt32(paramTotalRewardStar.Value);
					}
					else if( DbResultCode.DB_ERROR_IS_NOT_SEASON_ONE_USER == result_code)
					{
						_nTotalCollectStar  = 0;
                        _byUserLevel        = 0;
                        TotalRewardStar     = 0;
					}
					else
						Logger.ErrLog("P_USER_MoveFromSeason1 is Fail PlatformID: {0}, retCode: {1}", _strPlatformID, result_code);
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog(ior);
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog(ioe);
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog(ex);
				}
			}

			return result_code;
		}

        public DbResultCode GetTestMoveEventRewardInfo( Int64 _lUserUID, String _strPlatformID, ref Int32 _nStarAmount, ref Byte _byUserLevel )
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lUserUID);
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}

			catch( System.Exception ex )
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetTestMoveEventRewardInfo", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult            = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction               = ParameterDirection.ReturnValue;
				paramResult.Value                   = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID           = sqlCmd.Parameters.Add( "@PlatformID", SqlDbType.NVarChar, 40 );
				paramUserUID.Direction              = ParameterDirection.Input;
				paramUserUID.Value                  = _strPlatformID;

                SqlParameter paramTotalStarAmount   = sqlCmd.Parameters.Add( "@StarAmount", SqlDbType.Int );
				paramTotalStarAmount.Direction      = ParameterDirection.Output;
				paramTotalStarAmount.Value          = _nStarAmount;

                SqlParameter paramUserLevel         = sqlCmd.Parameters.Add( "@UserLevel", SqlDbType.TinyInt );
                paramUserLevel.Direction            = ParameterDirection.Output;
                paramUserLevel.Value                = _byUserLevel;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();

					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS == result_code)
					{
                        _nStarAmount    = Convert.ToInt32( paramTotalStarAmount.Value );
                        _byUserLevel    = Convert.ToByte( paramUserLevel.Value );
					}
					else if( DbResultCode.DB_ERROR_IS_NOT_SEASON_ONE_USER == result_code)
					{
                        _nStarAmount    = 0;
                        _byUserLevel    = 0;
					}
					else
                        Logger.ErrLog( "P_GetTestMoveEventRewardInfo is Fail PlatformID: {0}, retCode: {1}", _strPlatformID, result_code );
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog(ior);
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog(ioe);
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog(ex);
				}
			}

			return result_code;
        }

		public DbResultCode CheckMoveFromSeason1User(Int64 _lUserUID, String _strPlatformID)
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lUserUID);
				if(sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}

			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.P_USER_CheckMoveFromSeason1", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@PlatformID", SqlDbType.NVarChar, 40);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _strPlatformID;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();

					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.DB_ERROR_IS_NOT_SEASON_ONE_USER != result_code && DbResultCode.SUCCESS != result_code)
					{
						Logger.ErrLog("P_USER_CheckMoveFromSeason1 is Fail PlatformID: {0}, retCode: {1}", _strPlatformID, result_code);
					}
				}
				catch(System.IndexOutOfRangeException ior)
				{
					Logger.ExceptionLog(ior);
				}
				catch(System.InvalidOperationException ioe)
				{
					Logger.ExceptionLog(ioe);
				}
				catch(System.Exception ex)
				{
					Logger.ExceptionLog(ex);
				}
			}

			return result_code;
		}

		public DbResultCode CheckFlowerEventReceived(Int64 _lUserUID, out Byte[] _kReceivedFlowerEvent)
		{
			_kReceivedFlowerEvent = new Byte[0];

			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lUserUID);
				if(sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}

			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserFlowerEvent", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;


				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _lUserUID;

				SqlDataReader reader = null;

				List<Byte> kReceivedFlowerEvent = new List<Byte>();
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if(reader.HasRows)
					{
						while(reader.Read())
						{
							kReceivedFlowerEvent.Add((Byte)reader["FlowerEventType"]);
						}
					}

					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS == result_code)
					{
						_kReceivedFlowerEvent = kReceivedFlowerEvent.ToArray();
					}

					else
					{
						Logger.ErrLog("P_GetUserFlowerEvent is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
					}
				}
				catch(System.IndexOutOfRangeException ior)
				{
					Logger.ExceptionLog(ior);
				}
				catch(System.InvalidOperationException ioe)
				{
					Logger.ExceptionLog(ioe);
				}
				catch(System.Exception ex)
				{
					Logger.ExceptionLog(ex);
				}
			}

			return result_code;
		}

		public DbResultCode InsertFlowerEventReceived(Int64 _lUserUID, Byte _byFlowerEventType)
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB(_lUserUID);
				if(sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}

			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.P_InsertUserFlowerEvent", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;


				SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = _lUserUID;

				SqlParameter paramFlowerEventType = sqlCmd.Parameters.Add("@FlowerEventType", SqlDbType.TinyInt);
				paramFlowerEventType.Direction = ParameterDirection.Input;
				paramFlowerEventType.Value = _byFlowerEventType;

				SqlDataReader reader = null;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					
					if(DbResultCode.SUCCESS != result_code)
					{
						Logger.ErrLog("P_InsertUserFlowerEvent is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
					}
				}
				catch(System.IndexOutOfRangeException ior)
				{
					Logger.ExceptionLog(ior);
				}
				catch(System.InvalidOperationException ioe)
				{
					Logger.ExceptionLog(ioe);
				}
				catch(System.Exception ex)
				{
					Logger.ExceptionLog(ex);
				}
			}

			return result_code;
		}

        public DbResultCode GetRecommendPlayerList( Byte _byStartLevel, Byte _byMaxLevel, Int32 _nRecommendFriendCount, DateTime _kCheckDateTime, ref List<DBRecommendPlayerInfo> _kRecommendPlayerList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = Manager;
				if(sqlMgr.IsOpen() == false)
				{
					sqlMgr.Open();
				}
			}

			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.P_GetRecommendFriendList", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction           = ParameterDirection.ReturnValue;
				paramResult.Value               = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramStartLevel    = sqlCmd.Parameters.Add( "@StartLevel", SqlDbType.TinyInt );
                paramStartLevel.Direction       = ParameterDirection.Input;
                paramStartLevel.Value           = _byStartLevel;

                SqlParameter paramMaxLevel      = sqlCmd.Parameters.Add( "@MaxLevel", SqlDbType.TinyInt );
                paramMaxLevel.Direction         = ParameterDirection.Input;
                paramMaxLevel.Value             = _byMaxLevel;

                SqlParameter paramGetUserCount  = sqlCmd.Parameters.Add( "@GetUserCount", SqlDbType.Int );
                paramGetUserCount.Direction     = ParameterDirection.Input;
                paramGetUserCount.Value         = _nRecommendFriendCount;

                SqlParameter paramCheckDateTime = sqlCmd.Parameters.Add( "@CheckDateTime", SqlDbType.DateTime );
                paramCheckDateTime.Direction    = ParameterDirection.Input;
                paramCheckDateTime.Value        = _kCheckDateTime;

				SqlDataReader reader = null;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if(reader.HasRows)
					{
						while(reader.Read())
						{
							DBRecommendPlayerInfo kRecommentPlayerInfo = new DBRecommendPlayerInfo();
							kRecommentPlayerInfo.UserUID			= (Int64)reader["UserUID"];
							kRecommentPlayerInfo.Level				= (Byte)reader["Level"];
							kRecommentPlayerInfo.Nickname			= (String)reader["Nickname"];
                            kRecommentPlayerInfo.ProfileImageURL    = (String)reader["ImageURL"];
                            kRecommentPlayerInfo.ProfileImageIndex  = (Int32)reader["ProfileNumber"];
							kRecommentPlayerInfo.LogoutTick			= ((DateTime)reader["LogoutTime"]).Ticks;
                            kRecommentPlayerInfo.CurrentState       = (Int16)CLIENT_PLAYER_STATE.LOGOUT;

							_kRecommendPlayerList.Add(kRecommentPlayerInfo);
						}
					}

					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS != result_code && DbResultCode.DB_NOT_FOUND_LEVEL_USER_LIST != result_code)
					{
						Logger.ErrLog("P_GetRecommendFriendList is Fail Level: {0}, retCode: {1}", _byStartLevel, result_code);
					}
				}
				catch(System.IndexOutOfRangeException ior)
				{
					Logger.ExceptionLog(ior);
				}
				catch(System.InvalidOperationException ioe)
				{
					Logger.ExceptionLog(ioe);
				}
				catch(System.Exception ex)
				{
					Logger.ExceptionLog(ex);
				}
			}

			return result_code;
		}

		public String DecryptAES128b(String _strEncryptedProfileURL)
		{
			try
			{
				if(null != _strEncryptedProfileURL && _strEncryptedProfileURL.Length > 0)
				{
					return CryptoUtil.INSTANCE.DecryptAES128b(_strEncryptedProfileURL);
				}
				else
				{ 
					return "";
				}
			}
			catch(Exception ex)
			{
                Logger.ExceptionLog(ex);
				return "";
			}
		}

		public DbResultCode UpdateEloPoint( Int64 UserUID, Int32 GameMode, Int32 UpdateEloPoint, out Int32 nDBEloPoint )
		{
			nDBEloPoint = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( UserUID );
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
                }
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpdateEloPoint", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

				DBDecorator.InParam( paramList, "@UserUID", UserUID );
				DBDecorator.InParam( paramList, "@GameMode", GameMode );
				DBDecorator.InParam( paramList, "@AddEloPoint", UpdateEloPoint );
				DBDecorator.InParam( paramList, "@DBEloPoint", 0, ParameterDirection.Output );

				result_code = DBDecorator.Execute( cmd );
				if( result_code != DbResultCode.SUCCESS )
					Logger.ErrLog( "UpdateEloPoint Fail : codeResult = {0}", (Int32)result_code );
				else
					nDBEloPoint = (int)paramList["@DBEloPoint"].Value;
			}

			return result_code;
		}

        public DbResultCode SetProfileImageOption(Int64 _lUserUID, bool _bIsProfileImageOption)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_SetProfileImageOption", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramProfileImageOption = sqlCmd.Parameters.Add("@ProfileImageOption", SqlDbType.Bit);
                paramProfileImageOption.Direction = ParameterDirection.Input;
                paramProfileImageOption.Value = _bIsProfileImageOption;

                SqlDataReader reader = null;

                List<Byte> kReceivedFlowerEvent = new List<Byte>();
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_SetProfileImageOption is Fail UserUID: {0}, ProfileImageOption: {1}, retCode: {2}", _lUserUID, _bIsProfileImageOption, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;

        }

        public DbResultCode SetNotifyingRankResetFlag(bool _bIsNotifyingRankReset)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_SetNotifyingRankReset", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramNotifyRanResetFlag = sqlCmd.Parameters.Add("@NotifyingRankReset", SqlDbType.Bit);
                paramNotifyRanResetFlag.Direction = ParameterDirection.Input;
                paramNotifyRanResetFlag.Value = _bIsNotifyingRankReset;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);

                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_SetNotifyingRankReset is Fail retCode: {0}", result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode GetMagicPointProvideCount(Int64 _lUserUID, ref List<Byte> _kProvideTypeList, ref List<Int16> _kProvideCountList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.MAGICPOINT_GetProvideCount", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlDataReader reader = null;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            _kProvideTypeList.Add((Byte)reader["ProvideType"]);
                            _kProvideCountList.Add((Int16)reader["Count"]);
                        }
                    }

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("MAGICPOINT_GetProvideCount is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode InitMagicPointProvideCount(Int64 _lUserUID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.MAGICPOINT_InitProvideCount", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("MAGICPOINT_InitProvideCount is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode IncreaseMagicPointProvideCount(Int64 _lUserUID, Byte _byProvideType, Int16 _sIncreaseProvideCount, ref Int16 _sResultProvideCount)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.MAGICPOINT_IncreaseProvideCount", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramProvideType = sqlCmd.Parameters.Add("@ProvideType", SqlDbType.TinyInt);
                paramProvideType.Direction = ParameterDirection.Input;
                paramProvideType.Value = _byProvideType;

                SqlParameter paramIncreaseProvideCount = sqlCmd.Parameters.Add("@IncreaseCount", SqlDbType.SmallInt);
                paramIncreaseProvideCount.Direction = ParameterDirection.Input;
                paramIncreaseProvideCount.Value = _sIncreaseProvideCount;

                SqlParameter paramResultProvideCount = sqlCmd.Parameters.Add("@ResultCount", SqlDbType.SmallInt);
                paramResultProvideCount.Direction = ParameterDirection.Output;
                paramResultProvideCount.Value = _sResultProvideCount;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();

                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS == result_code)
                    {
                        _sResultProvideCount = Convert.ToInt16(paramResultProvideCount.Value);
                    }
                    else
                    {
                        Logger.ErrLog("MAGICPOINT_IncreaseProvideCount is Fail UserUID: {0}, ProvideType: {1}, IncreaseProvideCount: {2}, retCode: {3}", 
                            _lUserUID, _byProvideType, _sIncreaseProvideCount, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode CompleteTutorial(Int64 _lUserUID, Byte _byTutorialType)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.TUTORIAL_CompleteTutorial", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramTutorialType = sqlCmd.Parameters.Add("@TutorialType", SqlDbType.SmallInt);
                paramTutorialType.Direction = ParameterDirection.Input;
                paramTutorialType.Value = _byTutorialType;

                try
                {
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("TUTORIAL_CompleteTutorial is Fail UserUID: {0}, TutorialType: {1}, ResultCode: {2}", _lUserUID, _byTutorialType, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    result_code = (DbResultCode)Convert.ToInt32(sqlCmd.Parameters["@Result"].Value);
                }
            }

            return result_code;
        }

        public DbResultCode CompleteTutorialWithRewardItem(Int64 _lUserUID, Byte _byTutorialType, DataTable _kItemInfoList, DataTable _kPropertyInfoList, Int32 _nRewardElementItemID, DataTable _kRecipeElementList)
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return DbResultCode.DB_ERROR;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.TUTORIAL_CompleteTutorialWithRewardItem", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramTutorialType = sqlCmd.Parameters.Add("@TutorialType", SqlDbType.SmallInt);
                paramTutorialType.Direction = ParameterDirection.Input;
                paramTutorialType.Value = _byTutorialType;

                SqlParameter paramItemInfoList = sqlCmd.Parameters.Add("@ItemInfoList", SqlDbType.Structured);
                paramItemInfoList.Direction = ParameterDirection.Input;
                paramItemInfoList.Value = _kItemInfoList;
                paramItemInfoList.TypeName = "dbo.ItemInfoList";

                SqlParameter paramPropertyInfoList = sqlCmd.Parameters.Add("@PropertyInfoList", SqlDbType.Structured);
                paramPropertyInfoList.Direction = ParameterDirection.Input;
                paramPropertyInfoList.Value = _kPropertyInfoList;

                SqlParameter paramRewardElementItemID = sqlCmd.Parameters.Add("@RewardElementItemID", SqlDbType.Int);
                paramRewardElementItemID.Direction = ParameterDirection.Input;
                paramRewardElementItemID.Value = _nRewardElementItemID;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@RecipeElementList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kRecipeElementList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("TUTORIAL_CompleteTutorialWithRewardItem Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode CompleteUpgradeTutorial(Int64 _lUserUID, Byte _byTutorialType, Int64 _lTargetItemUID, Byte _byTargetItemUpgradeLevel, DataTable _kRecipeElementList)
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return DbResultCode.DB_ERROR;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.TUTORIAL_CompleteUpgradeTutorial", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramTutorialType = sqlCmd.Parameters.Add("@TutorialType", SqlDbType.SmallInt);
                paramTutorialType.Direction = ParameterDirection.Input;
                paramTutorialType.Value = _byTutorialType;

                SqlParameter paramTargetItemUID = sqlCmd.Parameters.Add("@TargetItemUID", SqlDbType.BigInt);
                paramTargetItemUID.Direction = ParameterDirection.Input;
                paramTargetItemUID.Value = _lTargetItemUID;

                SqlParameter paramTargetItemUpgradeLevel = sqlCmd.Parameters.Add("@TargetItemUpgradeLevel", SqlDbType.TinyInt);
                paramTargetItemUpgradeLevel.Direction = ParameterDirection.Input;
                paramTargetItemUpgradeLevel.Value = _byTargetItemUpgradeLevel;

                SqlParameter paramRecipeElementList = sqlCmd.Parameters.Add("@RecipeElementList", SqlDbType.Structured);
                paramRecipeElementList.Direction = ParameterDirection.Input;
                paramRecipeElementList.Value = _kRecipeElementList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("TUTORIAL_CompleteUpgradeTutorial Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code);
                    }

                    return result_code;
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode CompleteDecomposeTutorial(Int64 _lUserUID, Byte _byTutorialType, DataTable _kItemInfoList, DataTable _kDeleteItemUIDList, DataTable _kElementItemInfoList)
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return DbResultCode.DB_ERROR;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.TUTORIAL_CompleteDecomposeTutorial", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult                = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction                   = ParameterDirection.ReturnValue;
                paramResult.Value                       = (Int32) DbResultCode.DB_ERROR;

                SqlParameter paramUserUID               = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction                  = ParameterDirection.Input;
                paramUserUID.Value                      = _lUserUID;

                SqlParameter paramTutorialType          = sqlCmd.Parameters.Add( "@TutorialType", SqlDbType.SmallInt );
                paramTutorialType.Direction             = ParameterDirection.Input;
                paramTutorialType.Value                 = _byTutorialType;

                SqlParameter kItemInfoList              = sqlCmd.Parameters.Add( "@ItemInfoList", SqlDbType.Structured );
                kItemInfoList.Direction                 = ParameterDirection.Input;
                kItemInfoList.Value                     = _kItemInfoList;

                SqlParameter kParamItemUIDList          = sqlCmd.Parameters.Add( "@ItemUIDList", SqlDbType.Structured );
                kParamItemUIDList.Direction             = ParameterDirection.Input;
                kParamItemUIDList.Value                 = _kDeleteItemUIDList;

                SqlParameter kParamElementItemInfoList  = sqlCmd.Parameters.Add( "@ElementItemInfoList", SqlDbType.Structured );
                kParamElementItemInfoList.Direction     = ParameterDirection.Input;
                kParamElementItemInfoList.Value         = _kElementItemInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode) Convert.ToInt32( paramResult.Value );
                    if( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "TUTORIAL_CompleteDecomposeTutorial Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code );
                    }

                    return result_code;
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode CompleteMakePartsTutorial( Int64 _lUserUID, Byte _byTutorialType, DataTable _kItemInfoList, DataTable _kDeleteItemUIDList, DataTable _kElementItemInfoList )
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return DbResultCode.DB_ERROR;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.TUTORIAL_CompleteMakePartsTutorial", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult                = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32) DbResultCode.DB_ERROR;

                SqlParameter paramUserUID               = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlParameter paramTutorialType          = sqlCmd.Parameters.Add( "@TutorialType", SqlDbType.SmallInt );
                paramTutorialType.Direction = ParameterDirection.Input;
                paramTutorialType.Value = _byTutorialType;

                SqlParameter kItemInfoList              = sqlCmd.Parameters.Add( "@ItemInfoList", SqlDbType.Structured );
                kItemInfoList.Direction = ParameterDirection.Input;
                kItemInfoList.Value = _kItemInfoList;

                SqlParameter kParamItemUIDList          = sqlCmd.Parameters.Add( "@ItemUIDList", SqlDbType.Structured );
                kParamItemUIDList.Direction = ParameterDirection.Input;
                kParamItemUIDList.Value = _kDeleteItemUIDList;

                SqlParameter kParamElementItemInfoList  = sqlCmd.Parameters.Add( "@ElementItemInfoList", SqlDbType.Structured );
                kParamElementItemInfoList.Direction = ParameterDirection.Input;
                kParamElementItemInfoList.Value = _kElementItemInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode) Convert.ToInt32( paramResult.Value );
                    if( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "TUTORIAL_CompleteMakePartsTutorial Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code );
                    }

                    return result_code;
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    return DbResultCode.DB_ERROR;
                }
            }
        }

        public DbResultCode LoadCompleteTutorialList(Int64 _lUserUID, ref List<Byte> _kCompleteTutorialList)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }

            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.TUTORIAL_LoadCompleteList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;


                SqlParameter paramUserUID = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _lUserUID;

                SqlDataReader reader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if(reader.HasRows)
                    {
                        while(reader.Read())
                        {
                            Byte byTutorialType = (Byte)reader["TutorialType"];
                            _kCompleteTutorialList.Add(byTutorialType);
                        }
                    }

                    reader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(result_code != DbResultCode.SUCCESS)
                    {
                        Logger.ErrLog("TUTORIAL_LoadCompleteList is Fail UserUID: {0}, retCode: {1}", _lUserUID, result_code);
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return result_code;
        }

        public DbResultCode UpdateMessageBlockingState(Int64 UserUID, bool MsgBlock)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( UserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }

            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }

		public DbResultCode UpdateCareerPlayInfo( Int64 UserUID, Int32 CareerRecentlyStage, bool CareerRecentlyStageClear, Int32 ArriveStageNum )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( UserUID );
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand cmd = DBDecorator.CreateCommand( "dbo.P_UpdateCareerPlayInfo", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( cmd, "@Result" );

				DBDecorator.InParam( paramList, "@UserUID", UserUID );
				DBDecorator.InParam( paramList, "@CareerRecentlyStage", CareerRecentlyStage );
				DBDecorator.InParam( paramList, "@CareerRecentlyStageClear", CareerRecentlyStageClear );
				DBDecorator.InParam( paramList, "@ArriveStageNum", ArriveStageNum );

				result_code = DBDecorator.Execute( cmd );
				if( result_code != DbResultCode.SUCCESS )
					Logger.ErrLog( "UpdateCareerPlayInfo Fail : codeResult = {0}", (Int32)result_code );
			}

			return result_code;
		}

		public DBMissionInfo[] UpdateMissionInfo( Int64 UserUID, DBMissionInfo[] _kMissionArray )
        {
			List<DBMissionInfo>	liUserMissionInfo = new List<DBMissionInfo>();
            SqlManager sqlMgr = null;
            try
            {
				sqlMgr = GetDB( UserUID );
				if( false == sqlMgr.IsOpen() )
                    sqlMgr.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return new DBMissionInfo[0];
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_UpdateMission", dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult			= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction				= ParameterDirection.ReturnValue;

				SqlParameter paramUnitUID			= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
				paramUnitUID.Direction				= ParameterDirection.Input;
				paramUnitUID.Value					= UserUID;

				SqlParameter paramMissionID			= sqlCmd.Parameters.Add( "@in_MissionID", SqlDbType.SmallInt );
				paramMissionID.Direction			= ParameterDirection.Input;

				SqlParameter paramValue				= sqlCmd.Parameters.Add( "@in_ResetValue", SqlDbType.Int );
				paramValue.Direction				= ParameterDirection.Input;

				dbConn.Open();
				for( Int32 i = 0; i < _kMissionArray.Length; ++i )
				{
					paramMissionID.Value	= _kMissionArray[i].MissionID;
					paramValue.Value		= _kMissionArray[i].GainedCount;
					try
					{
						sqlCmd.ExecuteNonQuery();
						ClientErrorMessage resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
						if( resultCode != ClientErrorMessage.SUCCESS )
						{
							Logger.ErrLog( "P_UpdateMission is Update Fail UserUID:{0}, MissionID:{1}, result_code:{2}", UserUID, _kMissionArray[i].MissionID, resultCode );
						}
						else
						{
							_kMissionArray[i].IsComplete = false;
							liUserMissionInfo.Add( new DBMissionInfo( _kMissionArray[i] ) );
						}
					}
					catch( Exception ex )
					{
						Logger.ExceptionLog( ex );
						return new DBMissionInfo[0];
					}
				}
			}

			return liUserMissionInfo.ToArray();
        }

		public DBMissionInfo[] AcquireUserMissionInfo( Int64 UserUID, DBMissionInfo[] _kAcquireUserMissionInfo )
		{
			List<DBMissionInfo>	liUserMissionInfo = new List<DBMissionInfo>();
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( UserUID );
				if ( false == sqlMgr.IsOpen() )
					sqlMgr.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return new DBMissionInfo[0];
			}

			using ( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd	= new SqlCommand( "dbo.P_AcquireMission", dbConn );
				sqlCmd.CommandType	= CommandType.StoredProcedure;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				paramResult.Direction		= ParameterDirection.ReturnValue;

				SqlParameter paramUnitUID	= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
				paramUnitUID.Direction		= ParameterDirection.Input;
				paramUnitUID.Value			= UserUID;

				SqlParameter paramMissionID	= sqlCmd.Parameters.Add( "@in_MissionID", SqlDbType.SmallInt );
				paramMissionID.Direction	= ParameterDirection.Input;

				SqlParameter paramValue		= sqlCmd.Parameters.Add( "@in_AcquireValue", SqlDbType.Int );
				paramValue.Direction		= ParameterDirection.Input;

				SqlParameter paramCondition	= sqlCmd.Parameters.Add( "@in_Condition", SqlDbType.Int );
				paramCondition.Direction	= ParameterDirection.Input;

				SqlParameter paramResetDate	= sqlCmd.Parameters.Add( "@in_ResetDate", SqlDbType.DateTime );
				paramResetDate.Direction	= ParameterDirection.Input;

				SqlDataReader reader = null;
				dbConn.Open();
				for ( Int32 i = 0; i < _kAcquireUserMissionInfo.Length; ++i )
				{
					MissionInfo kMissionInfo;
					if ( false == ConfigDataGlobal.INSTANCE.MissionDataManager.TryGetMissionInfoByMissionID( _kAcquireUserMissionInfo[i].MissionID, out kMissionInfo ) )
						continue;

					paramMissionID.Value	= kMissionInfo.MissionID;
					paramValue.Value		= _kAcquireUserMissionInfo[i].GainedCount;
					paramCondition.Value	= kMissionInfo.Condition;
					paramResetDate.Value = new DateTime( _kAcquireUserMissionInfo[i].ResetDateTick );
					try
					{
						reader = sqlCmd.ExecuteReader();
						ClientErrorMessage resultCode = ( ClientErrorMessage )Convert.ToInt32( paramResult.Value );
						if ( resultCode != ClientErrorMessage.SUCCESS )
						{
							Logger.ErrLog( "P_AcquireMission is Update Fail UserUID:{0}, MissionID:{1}, result_code:{2}", UserUID, _kAcquireUserMissionInfo[i].MissionID, resultCode );
						}
						else
						{
							if ( reader.HasRows )
							{
								while ( reader.Read() )
								{
									liUserMissionInfo.Add( new DBMissionInfo
										(
											_kAcquireUserMissionInfo[i].MissionID,
											Convert.ToInt32( reader["GainedCount"] ),
											Convert.ToBoolean( reader["IsComplete"] ),
											( ( DateTime )reader["ResetDate"] ).Ticks
										) );
								}
							}
						}

						reader.Close();
					}
					catch ( Exception ex )
					{
						Logger.ExceptionLog( ex );
						return new DBMissionInfo[0];
					}
				}
			}

			return liUserMissionInfo.ToArray();
		}

        public DbResultCode GetFriendPlatformID(Int64 _lUserUID, Int64 _lFriendUserUID, ref String _strFriendPlatformID)
        {
            DbResultCode result_code = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendPlatformID", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult   = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID  = sqlCmd.Parameters.Add("@FriendUserUID", SqlDbType.BigInt);
                kParamUserUID.Direction = ParameterDirection.Input;
                kParamUserUID.Value = _lFriendUserUID;

                SqlDataReader kReader       = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    kReader = sqlCmd.ExecuteReader();
                    if(kReader.HasRows)
                    {
                        while(kReader.Read())
                            _strFriendPlatformID = (String)kReader["PlatformID"];
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    kReader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(kParamResult.Value);
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_GetFriendPlatformID is Fail UserUID: {0}", _lUserUID);
                    }
                }
            }

            return DbResultCode.SUCCESS;
        }

		public ClientErrorMessage LoadRandomMissionList( DataTable _kWeeklyMissionIDList, DataTable _kMonthlyMissionIDList, DateTime _WeeklyResetDate, DateTime _MonthlyResetDate,
			out Dictionary<Byte, List<Int16>> _kLoadMissionIDDic )
        {
			ClientErrorMessage resultCode	= ClientErrorMessage.ERROR_NORMAL;
            _kLoadMissionIDDic				= new Dictionary<Byte, List<Int16>>();

			try
			{
				if( false == Manager.IsOpen() )
					Manager.Open();

            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				return resultCode;
            }

			using( SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand kSqlCmd = new SqlCommand( "dbo.P_LoadRandomMissionIDList", kDbConn );
				kSqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter kParamResult				= kSqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				kParamResult.Direction					= ParameterDirection.ReturnValue;
				kParamResult.Value						= (Int32)DbResultCode.SUCCESS;

				SqlParameter kParamWeeklyMissionIDList	= kSqlCmd.Parameters.Add( "@in_WeeklyMissionInfoList", SqlDbType.Structured );
				kParamWeeklyMissionIDList.Direction		= ParameterDirection.Input;
				kParamWeeklyMissionIDList.Value			= _kWeeklyMissionIDList;

				SqlParameter kParamMonthlyMissionIDList	= kSqlCmd.Parameters.Add( "@in_MonthlyMissionInfoList", SqlDbType.Structured );
				kParamMonthlyMissionIDList.Direction	= ParameterDirection.Input;
				kParamMonthlyMissionIDList.Value		= _kMonthlyMissionIDList;

				SqlParameter kParamWeeklyResetDate		= kSqlCmd.Parameters.Add( "@in_WeeklyResetDate", SqlDbType.DateTime );
				kParamWeeklyResetDate.Direction			= ParameterDirection.Input;
				kParamWeeklyResetDate.Value				= _WeeklyResetDate;

				SqlParameter kParamMonthlyResetDate		= kSqlCmd.Parameters.Add( "@in_MonthlyResetDate", SqlDbType.DateTime );
				kParamMonthlyResetDate.Direction		= ParameterDirection.Input;
				kParamMonthlyResetDate.Value			= _MonthlyResetDate;

				SqlDataReader kReader = null;
                try
                {
					kDbConn.Open();
                    kReader = kSqlCmd.ExecuteReader();
                    if(kReader.HasRows)
                    {
                        while(kReader.Read())
                        {
                            Byte byScheduleType	= (Byte)kReader["ScheduleType"];
							Int16 byMissionID	= (Int16)kReader["MissionID"];

                            if(false == _kLoadMissionIDDic.ContainsKey(byScheduleType))
                                _kLoadMissionIDDic.Add(byScheduleType, new List<Int16>());

                            _kLoadMissionIDDic[byScheduleType].Add((Int16)byMissionID);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
					resultCode = (ClientErrorMessage)Convert.ToInt32( kParamResult.Value );

					if( ClientErrorMessage.SUCCESS != resultCode )
						Logger.ErrLog( "P_LoadRandomMissionIDList is Fail ResultCode: {0}", resultCode );
                }
            }

			return resultCode;
        }

        public DbResultCode AcquireElementItem(Int64 _lUserUID, DataTable _kElementItemList, DataTable _kQuantitiveItemInfoList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd = new SqlCommand("dbo.P_AcquireElementItem", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult                   = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction                      = ParameterDirection.ReturnValue;
                kParamResult.Value                          = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID                  = kSqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction                     = ParameterDirection.Input;
                kParamUserUID.Value                         = _lUserUID;

                SqlParameter kParamItemInfoList             = kSqlCmd.Parameters.Add("@ItemInfoList", SqlDbType.Structured);
                kParamItemInfoList.Direction                = ParameterDirection.Input;
                kParamItemInfoList.Value                    = _kElementItemList;

                SqlParameter kParamQuantitiveItemInfoList   = kSqlCmd.Parameters.Add("@ElementInfoList", SqlDbType.Structured);
                kParamQuantitiveItemInfoList.Direction      = ParameterDirection.Input;
                kParamQuantitiveItemInfoList.Value          = _kQuantitiveItemInfoList;

                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    kSqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kParamResult.Value);

                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_AcquireElementItem is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode ItemCompose( Int64 UserUID, Int64 ItemUID, Int32 ItemID, Int32 AttachItemID, Int32 SkillID, DataTable DeleteItemList )
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using( SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand kSqlCmd = new SqlCommand( "dbo.P_PartsCompose", kDbConn );
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult = kSqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = kSqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramItemUID = kSqlCmd.Parameters.Add( "@ItemUID", SqlDbType.BigInt );
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = ItemUID;

                SqlParameter paramItemID = kSqlCmd.Parameters.Add( "@ItemID", SqlDbType.Int );
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = ItemID;

                SqlParameter paramAttachItemID = kSqlCmd.Parameters.Add( "@AttachItemID", SqlDbType.Int );
                paramAttachItemID.Direction = ParameterDirection.Input;
                paramAttachItemID.Value = AttachItemID;

                SqlParameter paramSkillID = kSqlCmd.Parameters.Add( "@SkillID", SqlDbType.Int );
                paramSkillID.Direction = ParameterDirection.Input;
                paramSkillID.Value = SkillID;

                SqlParameter paramDeleteTargetItem = kSqlCmd.Parameters.Add( "@DeleteItemUIDList", SqlDbType.Structured );
                paramDeleteTargetItem.Direction = ParameterDirection.Input;
                paramDeleteTargetItem.Value = DeleteItemList;
                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    kSqlCmd.ExecuteNonQuery();
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32( kParamResult.Value );

                    if( DbResultCode.SUCCESS != eDbResultCode )
                        Logger.ErrLog( "P_PartsCompose is Fail ResultCode: {0}", eDbResultCode );
                }
            }

            return eDbResultCode;
        }

		public DbResultCode ItemEvolution( Int64 UserUID, Int64 ItemUID, Int32 ItemID, Int32 AttachItemID, bool IsEquip, Int32 SkillID, Int32 SkillLevel, DataTable DeleteItemList )
		{
			DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eDbResultCode = DbResultCode.DB_ERROR;
				return eDbResultCode;
			}

			using ( SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand kSqlCmd = new SqlCommand( "dbo.P_PartsEvolution", kDbConn );
				kSqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter kParamResult = kSqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				kParamResult.Direction = ParameterDirection.ReturnValue;
				kParamResult.Value = ( Int32 )DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = kSqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				SqlParameter paramItemUID = kSqlCmd.Parameters.Add( "@ItemUID", SqlDbType.BigInt );
				paramItemUID.Direction = ParameterDirection.Input;
				paramItemUID.Value = ItemUID;

				SqlParameter paramItemID = kSqlCmd.Parameters.Add( "@ItemID", SqlDbType.Int );
				paramItemID.Direction = ParameterDirection.Input;
				paramItemID.Value = ItemID;

				SqlParameter paramAttachItemID = kSqlCmd.Parameters.Add( "@AttachItemID", SqlDbType.Int );
				paramAttachItemID.Direction = ParameterDirection.Input;
				paramAttachItemID.Value = AttachItemID;

				SqlParameter paramIsEquip = kSqlCmd.Parameters.Add( "@IsEquip", SqlDbType.Bit );
				paramIsEquip.Direction = ParameterDirection.Input;
				paramIsEquip.Value = IsEquip;

				SqlParameter paramSkillID = kSqlCmd.Parameters.Add( "@SkillID", SqlDbType.Int );
				paramSkillID.Direction = ParameterDirection.Input;
				paramSkillID.Value = SkillID;

				SqlParameter paramSkillLevel = kSqlCmd.Parameters.Add( "@SkillLevel", SqlDbType.Int );
				paramSkillLevel.Direction = ParameterDirection.Input;
				paramSkillLevel.Value = SkillLevel;

				SqlParameter paramDeleteTargetItem = kSqlCmd.Parameters.Add( "@DeleteItemUIDList", SqlDbType.Structured );
				paramDeleteTargetItem.Direction = ParameterDirection.Input;
				paramDeleteTargetItem.Value = DeleteItemList;
				try
				{
					// 실행직전 오픈
					kDbConn.Open();
					kSqlCmd.ExecuteNonQuery();
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					eDbResultCode = ( DbResultCode )Convert.ToInt32( kParamResult.Value );

					if ( DbResultCode.SUCCESS != eDbResultCode )
						Logger.ErrLog( "P_PartsCompose is Fail ResultCode: {0}", eDbResultCode );
				}
			}

			return eDbResultCode;
		}

        public DbResultCode ItemEnhance(Int64 UserUID, Int64 ItemUID, Int32 ItemID, Int32 ItemLevel, Int32 ExpPoint, Int32 NeedPrice, DataTable DeleteItems)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using( SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand kSqlCmd = new SqlCommand( "dbo.P_ItemEnhance", kDbConn );
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult = kSqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = kSqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserUID;

                SqlParameter paramItemUID = kSqlCmd.Parameters.Add( "@ItemUID", SqlDbType.BigInt );
                paramItemUID.Direction = ParameterDirection.Input;
                paramItemUID.Value = ItemUID;

                SqlParameter paramItemID = kSqlCmd.Parameters.Add( "@ItemID", SqlDbType.Int );
                paramItemID.Direction = ParameterDirection.Input;
                paramItemID.Value = ItemID;

                SqlParameter paramItemLevel = kSqlCmd.Parameters.Add( "@ItemLevel", SqlDbType.Int );
                paramItemLevel.Direction = ParameterDirection.Input;
                paramItemLevel.Value = ItemLevel;

                SqlParameter paramExpPoint = kSqlCmd.Parameters.Add( "@ExpPoint", SqlDbType.Int );
                paramExpPoint.Direction = ParameterDirection.Input;
                paramExpPoint.Value = ExpPoint;

                SqlParameter paramNeedPrice = kSqlCmd.Parameters.Add( "@NeedPrice", SqlDbType.Int );
                paramNeedPrice.Direction = ParameterDirection.Input;
                paramNeedPrice.Value = NeedPrice;

                SqlParameter paramDeleteTargetItem = kSqlCmd.Parameters.Add( "@DeleteItemUIDList", SqlDbType.Structured );
                paramDeleteTargetItem.Direction = ParameterDirection.Input;
                paramDeleteTargetItem.Value = DeleteItems;
                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    kSqlCmd.ExecuteNonQuery();
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32( kParamResult.Value );

                    if( DbResultCode.SUCCESS != eDbResultCode )
                        Logger.ErrLog( "P_ItemEnhance is Fail ResultCode: {0}", eDbResultCode );
                }
            }

            return eDbResultCode;
        }

        public DbResultCode SellItems( Int64 _userUID, DataTable _sellItemList )
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using( SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand kSqlCmd = new SqlCommand( "dbo.P_SellItems", kDbConn );
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult = kSqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = kSqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _userUID;

                SqlParameter paramDeleteTargetItem = kSqlCmd.Parameters.Add( "@DeleteItemUIDList", SqlDbType.Structured );
                paramDeleteTargetItem.Direction = ParameterDirection.Input;
                paramDeleteTargetItem.Value = _sellItemList;
                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    kSqlCmd.ExecuteNonQuery();
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32( kParamResult.Value );

                    if( DbResultCode.SUCCESS != eDbResultCode )
                        Logger.ErrLog( "P_SellItems is Fail ResultCode: {0}", eDbResultCode );
                }
            }

            return eDbResultCode;
        }

        public DbResultCode RewardByChannelPlay( Int64 _lUserUID, DataTable _kRewardItemList, DataTable _kQuantitiveItemModifyPlatList )
        {
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _lUserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return DbResultCode.DB_ERROR;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_ProvideChannelPlayReward", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult           = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID          = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                kParamUserUID.Direction = ParameterDirection.Input;
                kParamUserUID.Value = _lUserUID;

                SqlParameter kItemInfoList          = sqlCmd.Parameters.Add( "@ItemInfoList", SqlDbType.Structured );
                kItemInfoList.Direction = ParameterDirection.Input;
                kItemInfoList.Value = _kRewardItemList;

                SqlParameter kElementItemModifyList = sqlCmd.Parameters.Add( "@QuantitiveItemModifyPlanList", SqlDbType.Structured );
                kElementItemModifyList.Direction = ParameterDirection.Input;
                kElementItemModifyList.Value = _kQuantitiveItemModifyPlatList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    DbResultCode result_code = (DbResultCode)Convert.ToInt32( kParamResult.Value );
                    if( result_code != DbResultCode.SUCCESS )
                    {
                        Logger.ErrLog( "P_ProvideChannelPlayReward Fail UserUID = {0}, Result_Code: {1}", _lUserUID, result_code );
                    }

                    return result_code;
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    return DbResultCode.DB_ERROR;
                }
            }
        }

		public DbResultCode ExchangeItem( Int64 _lUserUID, DataTable _kItemInfoList, DataTable _kPropertyInfoList, DataTable _kCouponQuantityInfoList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			SqlManager sqlMgr = null;
			try
			{
				sqlMgr = GetDB( _lUserUID );
				if( sqlMgr.IsOpen() == false )
				{
					sqlMgr.Open();
				}
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_ExchangeItem", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUnitUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				paramUnitUID.Direction = ParameterDirection.Input;
				paramUnitUID.Value = _lUserUID;

				SqlParameter paramItemInfoList = sqlCmd.Parameters.Add( "@ItemInfoList", SqlDbType.Structured );
				paramItemInfoList.Direction = ParameterDirection.Input;
				paramItemInfoList.Value = _kItemInfoList;

				SqlParameter paramPropertyInfoList = sqlCmd.Parameters.Add( "@PropertyInfoList", SqlDbType.Structured );
				paramPropertyInfoList.Direction = ParameterDirection.Input;
				paramPropertyInfoList.Value = _kPropertyInfoList;

				SqlParameter paramCouponQuantityInfoList = sqlCmd.Parameters.Add( "@ModifyQuantityInfoList", SqlDbType.Structured );
				paramCouponQuantityInfoList.Direction = ParameterDirection.Input;
				paramCouponQuantityInfoList.Value = _kCouponQuantityInfoList;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "ExchangeItem Fail UserUID = {0}, Result_Code = {1}", _lUserUID, result_code );

					return result_code;
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return DbResultCode.SUCCESS;
		}

        public DbResultCode GetSendNotifyData(Int64 _lUserUID, ref List<DBSendNotifyData> SendNotifyDataList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(_lUserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendSendNotifyData", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult    = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction       = ParameterDirection.ReturnValue;
                ParamResult.Value           = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID   = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction      = ParameterDirection.Input;
                ParamUserUID.Value          = _lUserUID;

                SqlDataReader DataReader = null;
                try
                {
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            DBSendNotifyData SendNotifyData = new DBSendNotifyData();

                            SendNotifyData.FriendPlatformID         = (String)DataReader["ReceiveUserPlatformID"];
                            SendNotifyData.RewardReceiveDateTicks   = ((DateTime)DataReader["RewardReceiveDate"]).Ticks;
                            SendNotifyData.LastSendDateTicks        = ((DateTime)DataReader["LastSendDate"]).Ticks;
                            SendNotifyData.IsRewardReceive          = (bool)DataReader["IsRewardReceive"];

                            SendNotifyDataList.Add(SendNotifyData);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetSendNotifyData Fail UserUID = {0}, Result_Code = {1}", _lUserUID, eDbResultCode);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode SendNotify(Int64 _lUserUID, String FriendPlatformID, bool IsRewardReceive)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd = new SqlCommand("dbo.P_FriendSendNotify", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction               = ParameterDirection.ReturnValue;
                ParamResult.Value                   = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID           = kSqlCmd.Parameters.Add("@SendUserUID", SqlDbType.BigInt);
                ParamUserUID.Direction              = ParameterDirection.Input;
                ParamUserUID.Value                  = _lUserUID;

                SqlParameter ParamFriendPlatformID  = kSqlCmd.Parameters.Add("@ReceivedUserPlatformID", SqlDbType.NVarChar, 40);
                ParamFriendPlatformID.Direction     = ParameterDirection.Input;
                ParamFriendPlatformID.Value         = FriendPlatformID;

                SqlParameter ParamIsRewardReceive  = kSqlCmd.Parameters.Add("@IsRewardReceive", SqlDbType.Bit);
                ParamIsRewardReceive.Direction      = ParameterDirection.Input;
                ParamIsRewardReceive.Value          = IsRewardReceive;

                try
                {
                    // 실행직전 오픈
                    kDbConn.Open();
                    kSqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);

                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_SendNotify is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode GetAllUserRankInfo(ref List<RankInfo> RankInfoList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetAllUserRankInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult    = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlDataReader DataReader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            RankInfo kRankInfo              = new RankInfo();
                            kRankInfo.UserUID = (Int64)DataReader["UserUID"];
                            kRankInfo.Nickname = (String)DataReader["Nickname"];
                            kRankInfo.Level                 = (Byte)DataReader["Level"];

                            kRankInfo.GameMode = (Byte)DataReader["GameMode"];
                            kRankInfo.RatingPoint           = (Int32)DataReader["EloPoint"];
                            kRankInfo.HighRatingPoint       = (Int32)DataReader["HighEloPoint"];
                            kRankInfo.PlayCount = (Int32)DataReader["PlayCount"];
                            kRankInfo.WinCount = (Int32)DataReader["WinCount"];
                            kRankInfo.NextPenaltyDateTicks = ((DateTime)DataReader["NextPenaltyDate"]).Ticks;
                            kRankInfo.RaceIndex = (Int64)DataReader["RaceIndex"];
                            RankInfoList.Add(kRankInfo);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetAllUserRankInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GetUserProfileInfo(Int64 UserUID, ref String NickName, ref Byte Level, DBSocialProfileInfo ProfileInfo)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserProfileInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult   = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID  = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction = ParameterDirection.Input;
                kParamUserUID.Value = UserUID;

                SqlDataReader DataReader       = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            NickName = (String)DataReader["Nickname"];
                            Level = (Byte)DataReader["Level"];
                            ProfileInfo.profileImgURL = (String)DataReader["PhotoImageURL"];
                            ProfileInfo.profileImgIndex = (Int32)DataReader["BaseProfileNumber"];
                            ProfileInfo.Notice = (String)DataReader["Notice"];
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetUserProfileInfo is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GetUserEquipInfo(Int64 UserUID, List<DBEquipItemInfo> EquipItemInfoList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserEquipInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult           = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                kParamResult.Direction = ParameterDirection.ReturnValue;
                kParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamUserUID          = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                kParamUserUID.Direction = ParameterDirection.Input;
                kParamUserUID.Value = UserUID;

                SqlParameter kParamCharacterItemID  = sqlCmd.Parameters.Add("@CharacterID", SqlDbType.Int);
                kParamCharacterItemID.Direction = ParameterDirection.Output;
                kParamCharacterItemID.Value = 0;

                SqlParameter kParamMasteryLevel     = sqlCmd.Parameters.Add("@MasteryLevel", SqlDbType.TinyInt);
                kParamMasteryLevel.Direction = ParameterDirection.Output;
                kParamMasteryLevel.Value = 0;

                SqlDataReader DataReader            = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                            EquipItemInfoList.Add(new DBEquipItemInfo((Int32)DataReader["ItemID"], (Byte)DataReader["UpgradeLevel"]));
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetUserEquipInfo is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                    else
                        EquipItemInfoList.Add(new DBEquipItemInfo((Int32)kParamCharacterItemID.Value, (Byte)kParamMasteryLevel.Value));
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode InsertRankInfo(DataTable InsertRankInfoList, ref List<RankInfo> kRankInfoList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_InsertUserRankInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRankInfoList      = sqlCmd.Parameters.Add("@RankInfoList", SqlDbType.Structured);
                ParamRankInfoList.Direction = ParameterDirection.Input;
                ParamRankInfoList.Value = InsertRankInfoList;

                SqlDataReader DataReader            = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            RankInfo kRankInfo = new RankInfo();
                            kRankInfo.UserUID = (Int64)DataReader["UserUID"];
                            kRankInfo.Nickname = (String)DataReader["Nickname"];
                            kRankInfo.GameMode = (Byte)DataReader["GameMode"];
                            kRankInfo.RatingPoint = (Int32)DataReader["RatingPoint"];
                            kRankInfo.PlayCount = (Int32)DataReader["PlayCount"];
                            kRankInfo.WinCount = (Int32)DataReader["WinCount"];
                            kRankInfo.NextPenaltyDateTicks = ((DateTime)DataReader["UserUID"]).Ticks;

                            kRankInfoList.Add(kRankInfo);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_InsertUserRankInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateRankInfo(DataTable UpdateRankInfoList, ref List<DBUpdateRankInfo> ResultRankInfoList)
        {
            DbResultCode eDbResultCode = DbResultCode.SUCCESS;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateUserRankInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRankInfoList      = sqlCmd.Parameters.Add("@RankInfoList", SqlDbType.Structured);
                ParamRankInfoList.Direction = ParameterDirection.Input;
                ParamRankInfoList.Value = UpdateRankInfoList;

                SqlDataReader DataReader            = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            DBUpdateRankInfo ResultRankInfo = new DBUpdateRankInfo();
                            ResultRankInfo.UserUID = (Int64)DataReader["UserUID"];
                            ResultRankInfo.GameMode = (Byte)DataReader["GameMode"];
                            ResultRankInfo.RatingPoint = (Int32)DataReader["RatingPoint"];
                            ResultRankInfo.HighRatingPoint = (Int32)DataReader["HighRatingPoint"];
                            ResultRankInfo.StressPoint = (Byte)DataReader["StressPoint"];
                            ResultRankInfo.NextPenaltyDateTicks = ((DateTime)DataReader["NextPenaltyDate"]).Ticks;

                            ResultRankInfoList.Add(ResultRankInfo);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdateUserRankInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode InsertRaceInfo(Int64 UserUID, Byte GameMode, DataTable RacePlayInfo, DataTable RaceEloInfo, ref Int64 RaceIndex)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_InsertRaceInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID       = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction = ParameterDirection.Input;
                ParamUserUID.Value = UserUID;

                SqlParameter ParamGameMode      = sqlCmd.Parameters.Add("@GameMode", SqlDbType.TinyInt);
                ParamGameMode.Direction = ParameterDirection.Input;
                ParamGameMode.Value = GameMode;

                SqlParameter ParamRacePlayInfo  = sqlCmd.Parameters.Add("@RacePlayInfo", SqlDbType.Structured);
                ParamRacePlayInfo.Direction = ParameterDirection.Input;
                ParamRacePlayInfo.Value = RacePlayInfo;

                SqlParameter ParamRaceEloInfo   = sqlCmd.Parameters.Add("@RaceEloInfo", SqlDbType.Structured);
                ParamRaceEloInfo.Direction = ParameterDirection.Input;
                ParamRaceEloInfo.Value = RaceEloInfo;

                SqlParameter ParamRaceIndex     = sqlCmd.Parameters.Add("@RaceIndex", SqlDbType.BigInt);
                ParamRaceIndex.Direction = ParameterDirection.Output;
                ParamRaceIndex.Value = RaceIndex;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_InsertRaceInfo is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                    else
                        RaceIndex = (Int64)ParamRaceIndex.Value;
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode InsertRacePlayInfo(Int64 UserUID, Byte GameMode, DataTable RacePlayInfo, ref Int64 RaceIndex)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB(UserUID);
                if(sqlMgr.IsOpen() == false)
                {
                    sqlMgr.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_InsertRacePlayInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID       = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction = ParameterDirection.Input;
                ParamUserUID.Value = UserUID;

                SqlParameter ParamGameMode      = sqlCmd.Parameters.Add("@GameMode", SqlDbType.TinyInt);
                ParamGameMode.Direction = ParameterDirection.Input;
                ParamGameMode.Value = GameMode;

                SqlParameter ParamRacePlayInfo  = sqlCmd.Parameters.Add("@RacePlayInfo", SqlDbType.Structured);
                ParamRacePlayInfo.Direction = ParameterDirection.Input;
                ParamRacePlayInfo.Value = RacePlayInfo;

                SqlParameter ParamRaceIndex     = sqlCmd.Parameters.Add("@RaceIndex", SqlDbType.BigInt);
                ParamRaceIndex.Direction = ParameterDirection.Output;
                ParamRaceIndex.Value = RaceIndex;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_InsertRacePlayInfo is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                    else
                        RaceIndex = (Int64)ParamRaceIndex.Value;
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateRacePlayInfo(DataTable RacePlayInfo)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateRacePlayInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRacePlayInfo  = sqlCmd.Parameters.Add("@RacePlayInfo", SqlDbType.Structured);
                ParamRacePlayInfo.Direction = ParameterDirection.Input;
                ParamRacePlayInfo.Value = RacePlayInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdateRacePlayInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateRaceEloInfo(DataTable RaceEloInfo)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateRaceEloInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRaceEloInfo = sqlCmd.Parameters.Add("@RaceEloInfo", SqlDbType.Structured);
                ParamRaceEloInfo.Direction = ParameterDirection.Input;
                ParamRaceEloInfo.Value = RaceEloInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdateRaceEloInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateRaceInfo(DataTable RacePlayInfo, DataTable RaceEloInfo)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateRaceInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRacePlayInfo  = sqlCmd.Parameters.Add("@RacePlayInfo", SqlDbType.Structured);
                ParamRacePlayInfo.Direction = ParameterDirection.Input;
                ParamRacePlayInfo.Value = RacePlayInfo;

                SqlParameter ParamRaceEloInfo   = sqlCmd.Parameters.Add("@RaceEloInfo", SqlDbType.Structured);
                ParamRaceEloInfo.Direction = ParameterDirection.Input;
                ParamRaceEloInfo.Value = RaceEloInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdateRacePlayInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode LoadChallengerRegTime(ref Int64 RegTime)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlDataReader DataReader = null;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadChallengerRegTime", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                            RegTime = ((DateTime)DataReader["RegDate"]).Ticks;
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode InsertChallengerUserList(DataTable ItemModeUserUIDList, DataTable SpeedModeUserUIDList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_InsertChallengerUserList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamItemModeUserUIDList = sqlCmd.Parameters.Add("@ItemModeUserUIDList", SqlDbType.Structured);
                ParamItemModeUserUIDList.Direction = ParameterDirection.Input;
                ParamItemModeUserUIDList.Value = ItemModeUserUIDList;

                SqlParameter ParamSpeedModeUserUIDList = sqlCmd.Parameters.Add("@SpeedModeUserUIDList", SqlDbType.Structured);
                ParamSpeedModeUserUIDList.Direction = ParameterDirection.Input;
                ParamSpeedModeUserUIDList.Value = SpeedModeUserUIDList;

                SqlParameter ParamRegTime = sqlCmd.Parameters.Add("@RegTime", SqlDbType.DateTime);
                ParamRegTime.Direction = ParameterDirection.Input;
                ParamRegTime.Value = DateTime.Now;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_InsertChallengerUserList is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode LoadChallengerUserList(List<Int64> UserUIDlist, List<Byte> GameModeList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlDataReader DataReader = null;
            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadChallengerUserList", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            UserUIDlist.Add((Int64)DataReader["UserUID"]);
                            GameModeList.Add((Byte)DataReader["GameMode"]);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode LoadRewardBox(Int64 UserUID, List<DBRewardBox> RewardBoxList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB(UserUID);
                if(kManager.IsOpen() == false)
                    kManager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_LoadRewardBox", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult    = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction       = ParameterDirection.ReturnValue;
                ParamResult.Value           = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID   = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction      = ParameterDirection.Input;
                ParamUserUID.Value          = UserUID;

                SqlDataReader DataReader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            DBRewardBox kRewardBox = new DBRewardBox();

                            kRewardBox.RewardBoxIndex       = (Int64)DataReader["RewardBoxIndex"];
                            kRewardBox.ContentType          = (Byte)DataReader["ContentType"];
                            kRewardBox.RewardID             = (Int32)DataReader["RewardID"];
                            kRewardBox.ExpirationDateTicks  = ((DateTime)DataReader["ExpirationDate"]).Ticks;

                            RewardBoxList.Add(kRewardBox);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_LoadRewardBox is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode TakeRewardBox(Int64 UserUID, Int64 RewardBoxIndex)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB(UserUID);
                if(kManager.IsOpen() == false)
                    kManager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_TakeRewardBox", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction               = ParameterDirection.ReturnValue;
                ParamResult.Value                   = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRewardBoxIndex    = sqlCmd.Parameters.Add("@RewardBoxIndex", SqlDbType.BigInt);
                ParamRewardBoxIndex.Direction       = ParameterDirection.Input;
                ParamRewardBoxIndex.Value           = RewardBoxIndex;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_TakeRewardBox is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode InsertRewardBox(Int64 UserUID, Byte ContentType, Int32 RewardID, DateTime ExpirationDate, out Int64 RewardBoxIndex)
        {
            RewardBoxIndex = 0;

            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB(UserUID);
                if(kManager.IsOpen() == false)
                    kManager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_InsertRewardBox", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction               = ParameterDirection.ReturnValue;
                ParamResult.Value                   = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID           = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction              = ParameterDirection.Input;
                ParamUserUID.Value                  = UserUID;

                SqlParameter ParamContentType       = sqlCmd.Parameters.Add("@ContentType", SqlDbType.TinyInt);
                ParamContentType.Direction          = ParameterDirection.Input;
                ParamContentType.Value              = ContentType;

                SqlParameter ParamRewardID          = sqlCmd.Parameters.Add("@RewardID", SqlDbType.Int);
                ParamRewardID.Direction             = ParameterDirection.Input;
                ParamRewardID.Value                 = RewardID;

                SqlParameter ParamExpirationDate    = sqlCmd.Parameters.Add("@ExpirationDate", SqlDbType.DateTime);
                ParamExpirationDate.Direction       = ParameterDirection.Input;
                ParamExpirationDate.Value           = ExpirationDate;

                SqlParameter ParamRewardBoxIndex    = sqlCmd.Parameters.Add("@RewardBoxIndex", SqlDbType.BigInt);
                ParamRewardBoxIndex.Direction       = ParameterDirection.Output;
                ParamRewardBoxIndex.Value           = RewardBoxIndex;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_TakeRewardBox is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode);
                    else
                        RewardBoxIndex = Convert.ToInt64(ParamRewardBoxIndex.Value);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdatePurchaseCount(Int64 UserUID, Int32 PackageSerialNumber, DateTime _LastPurchaseDate)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB( UserUID );
                if( kManager.IsOpen() == false )
                    kManager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using( SqlConnection dbConn = kManager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdatePurchaseCount", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                ParamResult.Direction               = ParameterDirection.ReturnValue;
                ParamResult.Value                   = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID           = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                ParamUserUID.Direction              = ParameterDirection.Input;
                ParamUserUID.Value                  = UserUID;

                SqlParameter ParamSerialNumber      = sqlCmd.Parameters.Add( "@SerialNumber", SqlDbType.Int );
                ParamSerialNumber.Direction         = ParameterDirection.Input;
                ParamSerialNumber.Value             = PackageSerialNumber;

                SqlParameter ParamLastPurchaseDate  = sqlCmd.Parameters.Add("@LastPurchaseDate", SqlDbType.DateTime);
                ParamLastPurchaseDate.Direction     = ParameterDirection.Input;
                ParamLastPurchaseDate.Value         = _LastPurchaseDate;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32( ParamResult.Value );
                    if( DbResultCode.SUCCESS != eDbResultCode )
                        Logger.ErrLog( "P_UpdatePurchaseCount is Fail UserUID: {0}, ResultCode: {1}", UserUID, eDbResultCode );
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode LoadPurchaseCount( Int64 _UserUID, out List<DBMonthlyPackageInfo> _MonthlyPackageInfoList )
        {
            _MonthlyPackageInfoList = new List<DBMonthlyPackageInfo>();

            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            SqlManager sqlMgr = null;
            try
            {
                sqlMgr = GetDB( _UserUID );
                if( sqlMgr.IsOpen() == false )
                {
                    sqlMgr.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                return DbResultCode.DB_ERROR;
            }

            using( SqlConnection dbConn = sqlMgr.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadPurchaseCount", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = _UserUID;

                SqlDataReader DataReader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            DBMonthlyPackageInfo kPackageInfo = new DBMonthlyPackageInfo();
                            kPackageInfo.SerialNumber           = (Int32)DataReader["PackageSerialNumber"];
                            kPackageInfo.PurchaseCount          = (Int32)DataReader["PurchaseCount"];
                            kPackageInfo.LastPurchaseDateTicks  = ((DateTime)DataReader["LastPurchaseDate"]).Ticks;

                            _MonthlyPackageInfoList.Add(kPackageInfo);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_LoadPurchaseCount is Fail UserUID: {0}, ResultCode: {1}", _UserUID, eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GetUserLeagueInfo(Int64 _UserUID, out Int64 _WeeklyRewardReceivedDateTicks, out List<DBPenaltyInfo> _PenaltyInfoList)
        {
            _WeeklyRewardReceivedDateTicks  = 0;
            _PenaltyInfoList                = new List<DBPenaltyInfo>();

            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB(_UserUID);
                if(kManager.IsOpen() == false)
                    kManager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserLeagueInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult                        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction                           = ParameterDirection.ReturnValue;
                ParamResult.Value                               = (Int32)DbResultCode.SUCCESS;

                SqlParameter ParamUserUID                       = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction                          = ParameterDirection.Input;
                ParamUserUID.Value                              = _UserUID;

                SqlParameter ParamWeeklyRewardReceiveDateTicks  = sqlCmd.Parameters.Add("@WeeklyRewardReceiveDate", SqlDbType.DateTime);
                ParamWeeklyRewardReceiveDateTicks.Direction     = ParameterDirection.Output;
                ParamWeeklyRewardReceiveDateTicks.Value         = DateTime.Now;

                SqlDataReader DataReader = null;
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    DataReader = sqlCmd.ExecuteReader();
                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            DBPenaltyInfo kPenaltyInfo  = new DBPenaltyInfo();
                            kPenaltyInfo.RaceIndex      = (Int64)DataReader["RaceIndex"];
                            kPenaltyInfo.PenaltyPoint   = (Int32)DataReader["PenaltyPoint"];

                            _PenaltyInfoList.Add(kPenaltyInfo);
                        }
                    }
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetUserLeagueInfo is Fail UserUID: {0}, ResultCode: {1}", _UserUID, eDbResultCode);
                    else
                    {
                        _WeeklyRewardReceivedDateTicks  = Convert.ToDateTime(ParamWeeklyRewardReceiveDateTicks.Value).Ticks;
                    }
                        
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateUserLeagueInfo(Int64 _UserUID, DateTime _WeeklyRewardReceivedDate)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB(_UserUID);
                if(kManager.IsOpen() == false)
                    kManager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateUserLeagueInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult                        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction                           = ParameterDirection.ReturnValue;
                ParamResult.Value                               = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID                       = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction                          = ParameterDirection.Input;
                ParamUserUID.Value                              = _UserUID;

                SqlParameter ParamWeeklyRewardReceiveDateTicks  = sqlCmd.Parameters.Add("@WeeklyRewardReceiveDate", SqlDbType.DateTime);
                ParamWeeklyRewardReceiveDateTicks.Direction     = ParameterDirection.Input;
                ParamWeeklyRewardReceiveDateTicks.Value         = _WeeklyRewardReceivedDate;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdateUserLeagueInfo is Fail UserUID: {0}, ResultCode: {1}", _UserUID, eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdatePenaltyInfo(DataTable _RaceEloInfo, DataTable _PenaltyInfoList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdatePenaltyInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamRaceEloInfo       = sqlCmd.Parameters.Add("@RaceEloInfo", SqlDbType.Structured);
                ParamRaceEloInfo.Direction          = ParameterDirection.Input;
                ParamRaceEloInfo.Value              = _RaceEloInfo;

                SqlParameter ParamPenaltyInfoList   = sqlCmd.Parameters.Add("@PenaltyInfo", SqlDbType.Structured);
                ParamPenaltyInfoList.Direction      = ParameterDirection.Input;
                ParamPenaltyInfoList.Value          = _PenaltyInfoList;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdatePenaltyInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode ResetPenaltyInfo(Int64 _UserUID)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_ResetPenaltyInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult    = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction       = ParameterDirection.ReturnValue;
                ParamResult.Value           = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID   = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction      = ParameterDirection.Input;
                ParamUserUID.Value          = _UserUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_ResetPenaltyInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode UpdateDisconnectUserPlayInfo(Byte _GameMode, DataTable _DisconnectUserPlayInfo)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_UpdateDisconnectUserPlayInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult                    = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction                       = ParameterDirection.ReturnValue;
                ParamResult.Value                           = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamGameMode                  = sqlCmd.Parameters.Add("@GameMode", SqlDbType.TinyInt);
                ParamGameMode.Direction                     = ParameterDirection.Input;
                ParamGameMode.Value                         = _GameMode;

                SqlParameter ParamDisconnectUserPlayInfo    = sqlCmd.Parameters.Add("@DisconnectUserPlayInfo", SqlDbType.Structured);
                ParamDisconnectUserPlayInfo.Direction       = ParameterDirection.Input;
                ParamDisconnectUserPlayInfo.Value           = _DisconnectUserPlayInfo;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_UpdateDisconnectUserPlayInfo is Fail ResultCode: {0}", eDbResultCode);
                }
            }

            return DbResultCode.SUCCESS;
        }

        public DbResultCode GetUserChallengerInfo(Int64 _UserUID, out bool _IsItemModeChallenger, out bool _IsSpeedModeChallenger)
        {
            _IsItemModeChallenger   = false;
            _IsSpeedModeChallenger  = false;

            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            SqlManager kManager = null;
            try
            {
                kManager = GetDB(_UserUID);
                if(kManager.IsOpen() == false)
                    kManager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetUserChallengerInfo", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult                        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction = ParameterDirection.ReturnValue;
                ParamResult.Value = (Int32)DbResultCode.SUCCESS;

                SqlParameter ParamUserUID                       = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction = ParameterDirection.Input;
                ParamUserUID.Value = _UserUID;

                SqlParameter ParamIsItemModeChallenger      = sqlCmd.Parameters.Add("@IsItemModeChallenger", SqlDbType.Bit);
                ParamIsItemModeChallenger.Direction         = ParameterDirection.Output;
                ParamIsItemModeChallenger.Value             = _IsItemModeChallenger;

                SqlParameter ParamIsSpeedModeChallenger     = sqlCmd.Parameters.Add("@IsSpeedModeChallenger", SqlDbType.Bit);
                ParamIsSpeedModeChallenger.Direction        = ParameterDirection.Output;
                ParamIsSpeedModeChallenger.Value            = _IsSpeedModeChallenger;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS != eDbResultCode)
                        Logger.ErrLog("P_GetUserChallengerInfo is Fail UserUID: {0}, ResultCode: {1}", _UserUID, eDbResultCode);
                    else
                    {
                        _IsItemModeChallenger   = Convert.ToBoolean(ParamIsItemModeChallenger.Value);
                        _IsSpeedModeChallenger  = Convert.ToBoolean(ParamIsSpeedModeChallenger.Value);
                    }

                }
            }

            return DbResultCode.SUCCESS;
        }

		public Int32 UseFixedCharge( long _lUserUID, out FixedChargeData _kFixedCharge )
		{
			Int64 defualtTicks = DateTime.MinValue.Ticks;
			_kFixedCharge = new FixedChargeData( false, 0, defualtTicks, defualtTicks, defualtTicks, defualtTicks );

			ClientErrorMessage nResult = ClientErrorMessage.SUCCESS;

			SqlManager kManager = null;
			try
			{
				kManager = GetDB( _lUserUID );
				if( kManager.IsOpen() == false )
					kManager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return (Int32)ClientErrorMessage.ERROR_NORMAL;
			}

			using( SqlConnection dbConn = kManager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UseFixedCharge", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter ParamResult            = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				ParamResult.Direction = ParameterDirection.ReturnValue;
				ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter ParamUserUID       = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				ParamUserUID.Direction			= ParameterDirection.Input;
				ParamUserUID.Value				= _lUserUID;


				SqlDataReader DataReader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					DataReader = sqlCmd.ExecuteReader();
					if( DataReader.HasRows )
					{
						while( DataReader.Read() )
						{
							_kFixedCharge.bUsed = (bool)DataReader["Used"];
							_kFixedCharge.ItemSerialNum = (Int32)DataReader["ItemSerialNum"];
							_kFixedCharge.StartDateTick = ( (DateTime)DataReader["StartDate"] ).Date.Ticks;
							_kFixedCharge.EndDateTick = ( (DateTime)DataReader["EndDate"] ).Date.Ticks;
							_kFixedCharge.ExpirationDateTick = ( (DateTime)DataReader["ExpirationDate"] ).Date.Ticks;
							_kFixedCharge.LastUpdateTimeTick = ( (DateTime)DataReader["LastUpdateDate"] ).Ticks;
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					DataReader.Close();

					nResult = (ClientErrorMessage)Convert.ToInt32( ParamResult.Value );
					if( ClientErrorMessage.SUCCESS != (ClientErrorMessage)nResult &&
						ClientErrorMessage.DB_ERROR_NOT_USED_FIXED_CHARGE != (ClientErrorMessage)nResult )
					{
						_kFixedCharge.bUsed = false;
						Logger.ErrLog( "UseFixedCharge DB Process Fail UserUID: {0}, ResultCode: {1}", _lUserUID, nResult );
					}
					
				}
			}

			return (Int32)nResult;
		}

		public Int32 PaymentFixedCharge( Int64 UserUID, Int32 ItemSerialNum, out FixedChargeData _kFixedCharge )
		{
			Int64 defualtTicks = DateTime.MinValue.Ticks;
			_kFixedCharge = new FixedChargeData( false, 0, defualtTicks, defualtTicks, defualtTicks, defualtTicks );

			ClientErrorMessage nResult = ClientErrorMessage.SUCCESS;

			SqlManager kManager = null;
			try
			{
				kManager = GetDB( UserUID );
				if( kManager.IsOpen() == false )
					kManager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return (Int32)ClientErrorMessage.ERROR_NORMAL;
				
			}

			using( SqlConnection dbConn = kManager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_PaymentFixedCharge", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter ParamResult            = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				ParamResult.Direction = ParameterDirection.ReturnValue;
				ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter ParamUserUID       = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				ParamUserUID.Direction = ParameterDirection.Input;
				ParamUserUID.Value = UserUID;

				SqlParameter ParamType      = sqlCmd.Parameters.Add( "@ItemSerialNum", SqlDbType.Int );
				ParamType.Direction			= ParameterDirection.Input;
				ParamType.Value				= ItemSerialNum;

				SqlDataReader DataReader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					DataReader = sqlCmd.ExecuteReader();
					if( DataReader.HasRows )
					{
						while( DataReader.Read() )
						{
							_kFixedCharge.bUsed = (bool)DataReader["Used"];
							_kFixedCharge.ItemSerialNum = (Int32)DataReader["ItemSerialNum"];
							_kFixedCharge.StartDateTick = ( (DateTime)DataReader["StartDate"] ).Date.Ticks;
							_kFixedCharge.EndDateTick = ( (DateTime)DataReader["EndDate"] ).Date.Ticks;
							_kFixedCharge.ExpirationDateTick = ( (DateTime)DataReader["ExpirationDate"] ).Date.Ticks;
							_kFixedCharge.LastUpdateTimeTick = ( (DateTime)DataReader["LastUpdateDate"] ).Ticks;
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					DataReader.Close();

					nResult = (ClientErrorMessage)Convert.ToInt32( ParamResult.Value );
					if( ClientErrorMessage.SUCCESS == nResult )
					{
					}
					else
					{
						Logger.ErrLog( "PaymentFixedCharge DB Process Fail UserUID: {0}, ResultCode: {1}", UserUID, nResult );
					}

				}
			}

			return (Int32)nResult;
		}

		public int GiveFixedChargeCompensation( Int64 UserUID, Int32 ItemSerialNum, Int32 nDay, DataTable compensationTable, out List<DBNewPostBox> _liPostbox )
		{
			ClientErrorMessage nResult = ClientErrorMessage.SUCCESS;
			_liPostbox = new List<DBNewPostBox>();

			SqlManager kManager = null;
			try
			{
				kManager = GetDB( UserUID );
				if( kManager.IsOpen() == false )
					kManager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return (Int32)ClientErrorMessage.ERROR_NORMAL;

			}

			using( SqlConnection dbConn = kManager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GiveFixedChargeCompensation", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter ParamResult = sqlCmd.Parameters.Add( "@Result", SqlDbType.Int );
				ParamResult.Direction = ParameterDirection.ReturnValue;
				ParamResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter ParamUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
				ParamUserUID.Direction = ParameterDirection.Input;
				ParamUserUID.Value = UserUID;

				SqlParameter ParamType = sqlCmd.Parameters.Add( "@ItemSerialNum", SqlDbType.Int );
				ParamType.Direction = ParameterDirection.Input;
				ParamType.Value = ItemSerialNum;

				SqlParameter ParamDay = sqlCmd.Parameters.Add( "@Day", SqlDbType.Int );
				ParamDay.Direction = ParameterDirection.Input;
				ParamDay.Value = nDay;

				SqlParameter paramItemList = sqlCmd.Parameters.Add( "@CompensationTable", SqlDbType.Structured );
				paramItemList.Direction = ParameterDirection.Input;
				paramItemList.Value = compensationTable;

				SqlDataReader DataReader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					DataReader = sqlCmd.ExecuteReader();
					if( DataReader.HasRows )
					{
						while( DataReader.Read() )
						{
							Int64           lPostUID        = (Int64)DataReader["PostUID"];
							Int64           lSendUserUID    = (Int64)DataReader["SendUserUID"];
							Int32           nAttachItemID   = (Int32)DataReader["AttachItemID"];
							Int32           nAttachQuantity = (Int32)DataReader["AttachItemQuantity"];
							Byte            byPostType      = (Byte)DataReader["PostType"];
							Byte            byMessageType   = (Byte)DataReader["MessageType"];
							String          strPostMessage  = (String)DataReader["PostMessage"];
							bool			IsDel			= (bool)DataReader["IsDel"];
							DateTime        kExpirationDate = (DateTime)DataReader["ExpirationDate"];
							DBNewPostBox    kDbPostBox      = new DBNewPostBox( lPostUID, byPostType, byMessageType, lSendUserUID, nAttachItemID, nAttachQuantity, strPostMessage, IsDel, false, kExpirationDate.Ticks );
							_liPostbox.Add( kDbPostBox );
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					DataReader.Close();

					nResult = (ClientErrorMessage)Convert.ToInt32( ParamResult.Value );
					if( ClientErrorMessage.SUCCESS == nResult )
					{
					}
					else
					{
						Logger.ErrLog( "PaymentFixedCharge DB Process Fail UserUID: {0}, ResultCode: {1}", UserUID, nResult );
					}

				}
			}

			return (Int32)nResult;
		}

		public Int32 DeleteFixedCharge( Int64 UserUID )
        {
			ClientErrorMessage nResult = ClientErrorMessage.SUCCESS;

			SqlManager kManager = null;
			try
			{
				kManager = GetDB( UserUID );
				if( kManager.IsOpen() == false )
					kManager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return (Int32)ClientErrorMessage.ERROR_NORMAL;

			}


            using(SqlConnection dbConn = kManager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_DeleteFixedCharge", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult            = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction               = ParameterDirection.ReturnValue;
                ParamResult.Value                   = (Int32)DbResultCode.DB_ERROR;

                SqlParameter ParamUserUID           = sqlCmd.Parameters.Add("@UserUID", SqlDbType.BigInt);
                ParamUserUID.Direction              = ParameterDirection.Input;
                ParamUserUID.Value                  = UserUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
					nResult = (ClientErrorMessage)Convert.ToInt32( ParamResult.Value );
					if( ClientErrorMessage.SUCCESS == nResult )
						Logger.ErrLog( "DeleteFixedCharge is Fail UserUID: {0}, ResultCode: {1}", UserUID, nResult );
                }
            }

			return (Int32)nResult;
        }

		public GuerrillaData[] AcquireUserGuerrillaData( Int64 _lUserUID, Int64 _EventID, DataTable _UpdateTable )
		{
			List<GuerrillaData> liUserInfo = new List<GuerrillaData>();
			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return liUserInfo.ToArray();
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.P_AcquireGuerrillaInfoList", dbConn			);

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@Result",			SqlDbType.Int			);
				paramResult.Direction		= ParameterDirection.ReturnValue;

				SqlParameter paramUnitUID	= sqlCmd.Parameters.Add( "@in_UserUID",		SqlDbType.BigInt		);
				paramUnitUID.Direction		= ParameterDirection.Input;
				paramUnitUID.Value			= _lUserUID;

				SqlParameter paramEventID	= sqlCmd.Parameters.Add( "@in_EventID",		SqlDbType.BigInt		);
				paramEventID.Direction		= ParameterDirection.Input;
				paramEventID.Value			= _EventID;

				SqlParameter paramInfoList	= sqlCmd.Parameters.Add( "@in_InfoList",	SqlDbType.Structured	);
				paramInfoList.Direction		= ParameterDirection.Input;
				paramInfoList.Value			= _UpdateTable;

				SqlDataReader reader = null;
				dbConn.Open();
				try
				{
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( reader.Read() )
						{
							liUserInfo.Add( new GuerrillaData
								(
									Convert.ToInt32( reader["Type"]			),
									Convert.ToInt32( reader["Action"]		),
									Convert.ToInt32( reader["DetailNum"]	),
									Convert.ToInt32( reader["Value"]		)
								) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					ClientErrorMessage resultCode = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
					if ( resultCode != ClientErrorMessage.SUCCESS )
					{
						Logger.ErrLog( "P_AcquireGuerrilla is Update Fail UserUID:{0}, EventID:{1}, result_code:{2}", _lUserUID, _EventID, resultCode );
					}
				}
			}

			return liUserInfo.ToArray();
		}

		public GuerrillaData[] LoadGuerrillaList( Int64 _lUserUID, Int64 _lEventID )
		{
			List<GuerrillaData> _kGuerrillaInfoList = new List<GuerrillaData>();
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return _kGuerrillaInfoList.ToArray();
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.P_LoadGuerrilla", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID", _lUserUID );
				DBDecorator.InParam( paramList, "@in_EventID", _lEventID );

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( reader.Read() )
						{
							_kGuerrillaInfoList.Add(new GuerrillaData
								(
									Convert.ToInt32( reader["Type"] ),
									Convert.ToInt32( reader["Action"] ),
									Convert.ToInt32( reader["DetailNum"] ),
									Convert.ToInt32( reader["Value"] )
								) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();

					ClientErrorMessage eResultCode = (ClientErrorMessage)Convert.ToInt32( sqlCmd.Parameters["@Result"].Value );
					if ( ClientErrorMessage.SUCCESS != eResultCode )
						Logger.ErrLog( "P_LoadGuerrilla is Fail UserUID: {0}, ResultCode: {1}", _lUserUID, eResultCode );
				}
			}

			return _kGuerrillaInfoList.ToArray();
		}

        public DbResultCode SearchUserUID(Byte bySearchType, String strSearhString, out Int64 lSearchUserUID)
        {
            lSearchUserUID = 0;

            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_SearchUserUID", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParamResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                ParamResult.Direction           = ParameterDirection.ReturnValue;
                ParamResult.Value               = (Int32)DbResultCode.SUCCESS;

                SqlParameter ParamSearchType    = sqlCmd.Parameters.Add("@SearchType", SqlDbType.TinyInt);
                ParamSearchType.Direction       = ParameterDirection.Input;
                ParamSearchType.Value           = bySearchType;

                SqlParameter ParamSearchString  = sqlCmd.Parameters.Add("@SearchString", SqlDbType.NVarChar, 50);
                ParamSearchString.Direction     = ParameterDirection.Input;
                ParamSearchString.Value         = strSearhString;

                SqlParameter ParamSearchUserUID = sqlCmd.Parameters.Add("@SearchUserUID", SqlDbType.BigInt);
                ParamSearchUserUID.Direction    = ParameterDirection.Output;
                ParamSearchUserUID.Value        = lSearchUserUID;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(ParamResult.Value);
                    if(DbResultCode.SUCCESS == eDbResultCode)
                        lSearchUserUID = Convert.ToInt64(ParamSearchUserUID.Value);
                }
            }

            return eDbResultCode;
        }
	}
}