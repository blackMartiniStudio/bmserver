﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;

namespace SCommonLib.DB.Handler
{
    public class NonQueryHandler : DBCmd
    {
        public NonQueryHandler(SqlManager mgr)
            : base(mgr)
        {

        }

        protected override void Bind() { throw new NotImplementedException(); }
        private DbCommand BeginExecute()
        {
            // 열려 있는지 확인
            m_result = DbResultCode.SUCCESS;
            if (m_sql_mgr.IsOpen() == false)
            {
                m_result = DbResultCode.DB_ERROR;
                return null;
            }

            Bind();
            // 커맨드 생성
            return m_sql_mgr.CreateCommand(m_sp_name, m_inParams);
        }

        public virtual void EndExecute()
        {

        }

        public override void Execute()
        {
            DbCommand cmd = BeginExecute();
            if (cmd == null)
                throw new InvalidOperationException();

            using (DbConnection conn = m_sql_mgr.CreateConnection())
            {
                try
                {
                    cmd.Connection = conn;
                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    m_result = (DbResultCode)cmd.Parameters[DBCmd.paramResultName].Value;
                    if (m_result != DbResultCode.SUCCESS)
                        throw new InvalidOperationException("Fail!!");

                    EndExecute();
                }
                catch (Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }
        }
    }
}
