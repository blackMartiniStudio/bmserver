﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCommonLib.DB
{
    public abstract class SqlHandler
    {
        public Dictionary<Int16, SqlManager> m_dicSqlManager = new Dictionary<Int16, SqlManager>();

        public void Add(Int16 id, SqlManager dbManager)
        {
            m_dicSqlManager.Add(id, dbManager);
        }

        public bool Add(DBConnectionInfo _kDBConnectionInfo)
        {
            try
            {
                MsSqlGenerator kSqlGenerator = new MsSqlGenerator(_kDBConnectionInfo.ip, _kDBConnectionInfo.port, _kDBConnectionInfo.dbName,
                _kDBConnectionInfo.user, _kDBConnectionInfo.pw);

                kSqlGenerator.Init();

                SqlManager kSqlManager = new SqlManager(kSqlGenerator);

                Add(_kDBConnectionInfo.id, kSqlManager);
            }

            catch(Exception ex)
            {
                Logger.ErrLog("SqlHandler Add Fail");
                Logger.ExceptionLog(ex);

                return false;
            }

            return true;   
        }

        public SqlManager Manager
        {
            get
            {
                foreach(var mgr in m_dicSqlManager.Values)
                {
                    return mgr;
                }
                return null;
            }
        }
    }
}
