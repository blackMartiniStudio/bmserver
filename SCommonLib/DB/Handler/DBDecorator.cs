﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SCommonLib
{
	static class DBDecorator
	{
		static private Int32 ReturnParameterIndex = 0;

		public static SqlCommand CreateCommand( String spName , SqlConnection dbConn , CommandType type = CommandType.StoredProcedure )
		{
			SqlCommand cmd = new SqlCommand( spName , dbConn );
			cmd.CommandType = type;
			return cmd;
		}

		//  [1/9/2014 seedyoon]
		// return을 명시적으로 해줘야 paramList가 나오게 함. 이래야 오류를 줄일수 있을 것 같음.
		static public SqlParameterCollection Return( SqlCommand cmd , String paramName = "@ret" )
		{
			SqlParameter param  = new SqlParameter( paramName , SqlDbType.Int );
			param.ParameterName = paramName;
			param.Direction = ParameterDirection.ReturnValue;
			param.Value = (Int32)( DbResultCode.DB_ERROR );

			cmd.Parameters.Add( param );
			return cmd.Parameters;
		}


		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , bool value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.Bit );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList, String paramName, float value, ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName, SqlDbType.Real );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , Byte value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.TinyInt );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , Int16 value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.SmallInt );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , Int32 value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.Int );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , Int64 value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.BigInt );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , DateTime value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.DateTime );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}

		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , String value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param  = new SqlParameter( paramName , SqlDbType.VarChar , value.Length );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}


		static public SqlParameter InParam( SqlParameterCollection paramList , String paramName , Byte[] value , ParameterDirection direction = ParameterDirection.Input )
		{
			SqlParameter param = new SqlParameter( paramName , SqlDbType.Binary , value.Length );
			param.ParameterName = paramName;
			param.Direction = direction;
			param.Value = value;

			paramList.Add( param );
			return param;
		}



		internal static DbResultCode Execute( SqlCommand cmd )
		{
			try
			{
				cmd.Connection.Open();
				cmd.ExecuteNonQuery();
				return (DbResultCode)cmd.Parameters[ReturnParameterIndex].Value;
			}
			catch ( System.IndexOutOfRangeException ior )
			{
				Logger.ExceptionLog( ior );
			}
			catch ( System.InvalidOperationException ioe )
			{
				Logger.ExceptionLog( ioe );
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}

			return DbResultCode.DB_ERROR;

		}
	}
}
