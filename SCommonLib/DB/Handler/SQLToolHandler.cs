﻿using KMNetClass;
using SCommonLib.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SCommonLib
{
	public class SQLToolHandler : SqlHandler
	{
		public DbResultCode ToolMoneyLog( String platformID , String typeCd , Int32 changeCnt , Int32 remainCnt , String changeDesc , String transactionID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.UP_TOOL_MONEY_LOG" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramChannelPlatformID = sqlCmd.Parameters.Add( "@I_CHANNEL_USER_ID" , SqlDbType.VarChar , 40 );
				paramChannelPlatformID.Direction = ParameterDirection.Input;
				paramChannelPlatformID.Value = platformID;

				SqlParameter paramMoneyTypeCD = sqlCmd.Parameters.Add( "@I_MONEY_TYPE_CD" , SqlDbType.Char , 3 );
				paramMoneyTypeCD.Direction = ParameterDirection.Input;
				paramMoneyTypeCD.Value = typeCd;

				SqlParameter paramTransactionID = sqlCmd.Parameters.Add( "@I_TRANSACTION_ID" , SqlDbType.VarChar , 19 );
				paramTransactionID.Direction = ParameterDirection.Input;
				paramTransactionID.Value = transactionID;

				SqlParameter paramChangeCnt = sqlCmd.Parameters.Add( "@I_CHANGE_CNT" , SqlDbType.Int );
				paramChangeCnt.Direction = ParameterDirection.Input;
				paramChangeCnt.Value = changeCnt;

				SqlParameter paramRemainCnt = sqlCmd.Parameters.Add( "@I_REMAIN_CNT" , SqlDbType.Int );
				paramRemainCnt.Direction = ParameterDirection.Input;
				paramRemainCnt.Value = remainCnt;

				SqlParameter paramChangeDesc = sqlCmd.Parameters.Add( "@I_CHANGE_DESC" , SqlDbType.NVarChar , (int)MAXLENGTH.MONEYLOG_DESC );
				paramChangeDesc.Direction = ParameterDirection.Input;
				paramChangeDesc.Value = changeDesc;

				SqlParameter paramRegDateTime = sqlCmd.Parameters.Add( "@I_REG_DATETIME" , SqlDbType.DateTime );
				paramRegDateTime.Direction = ParameterDirection.Input;
				paramRegDateTime.Value = DateTime.Now;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateHackUser( Int64 UserUID, Int32 HackType, out bool Block )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			Block = false;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{

				SqlCommand cmd = DBDecorator.CreateCommand( "dbo.HACK_USER_Update", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( cmd );

				DBDecorator.InParam( paramList, "@UserUID", UserUID );
				DBDecorator.InParam( paramList, "@HackType", HackType );
				DBDecorator.InParam( paramList, "@Block", Block, ParameterDirection.Output );

				result_code = DBDecorator.Execute( cmd );
				if ( result_code == DbResultCode.SUCCESS )
				{
					Block = Convert.ToBoolean( paramList["@Block"].Value );
				}
				else
				{
					Logger.ErrLog( "ToolDB.UpdateHackUser Fail. Result = {0}, UserUID = {1}, HackType = {2}", result_code, UserUID, HackType );
				}
			}

			return result_code;
		}

		public DbResultCode ToolHackLog( String platformID , Int32 trackID , Int16 lapTimeSec , Int32 characterType , Byte characterLevel , Int32 carBodyType , Byte carBodyLevel , Int32 carSpoilerType , Byte carSpoilerLevel , Int32 carWheelType , Byte carWheelLevel )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{

				SqlCommand cmd= DBDecorator.CreateCommand( "dbo.UP_TOOL_HACK_LOG" , dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( cmd );

				DBDecorator.InParam( paramList , "@I_CHANNEL_USER_ID" , platformID );
				DBDecorator.InParam( paramList , "@I_TRACK_ID" , trackID );
				DBDecorator.InParam( paramList , "@I_LAP_TIME_SECOND" , lapTimeSec );
				DBDecorator.InParam( paramList , "@I_CHARACTER_TYPE" , characterType );
				DBDecorator.InParam( paramList , "@I_CHARACTER_LEVEL" , characterLevel );
				DBDecorator.InParam( paramList , "@I_CAR_BODY_TYPE" , carBodyType );
				DBDecorator.InParam( paramList , "@I_CAR_BODY_LEVEL" , carBodyLevel );
				DBDecorator.InParam( paramList , "@I_CAR_SPOILER_TYPE" , carSpoilerType );
				DBDecorator.InParam( paramList , "@I_CAR_SPOILER_LEVEL" , carSpoilerLevel );
				DBDecorator.InParam( paramList , "@I_CAR_WHEEL_TYPE" , carWheelType );
				DBDecorator.InParam( paramList , "@I_CAR_WHEEL_LEVEL" , carWheelLevel );

				result_code = DBDecorator.Execute( cmd );
			}

			return result_code;
		}
		public DbResultCode ToolPackageBuyLog( String platformID , Int32 packageID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}
			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.UP_TOOL_PACKAGEBUY_LOG" , dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd , "@ret" );

				DBDecorator.InParam( paramList , "@PLATFORMID" , platformID );
				DBDecorator.InParam( paramList , "@PACKAGEID" , packageID );
				DBDecorator.InParam( paramList , "@REG_DATETIME" , DateTime.Now );

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
		}

		public DbResultCode AddStarFriendShip( String platformID , String transactionID , Int32 itemID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_AddStarFriendShip" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramTransactionID = sqlCmd.Parameters.Add( "@transactionID" , SqlDbType.VarChar , 19 );
				paramTransactionID.Direction = ParameterDirection.Input;
				paramTransactionID.Value = transactionID;

				SqlParameter paramItemID = sqlCmd.Parameters.Add( "@itemID" , SqlDbType.Int );
				paramItemID.Direction = ParameterDirection.Input;
				paramItemID.Value = itemID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateStarFriendShip( String platformID , String transactionID , Int64 friendUserUID , bool isFriendshipFlag , out Int32 itemID )
		{
			itemID = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateStarFriendShip" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramTransactionID = sqlCmd.Parameters.Add( "@transactionID" , SqlDbType.VarChar , 19 );
				paramTransactionID.Direction = ParameterDirection.Input;
				paramTransactionID.Value = transactionID;

				SqlParameter paramFriendUserUID = sqlCmd.Parameters.Add( "@friendUserUID" , SqlDbType.BigInt );
				paramFriendUserUID.Direction = ParameterDirection.Input;
				paramFriendUserUID.Value = friendUserUID;

				SqlParameter paramIsFriendshipFlag = sqlCmd.Parameters.Add( "@isFriendshipFlag" , SqlDbType.Bit );
				paramIsFriendshipFlag.Direction = ParameterDirection.Input;
				paramIsFriendshipFlag.Value = isFriendshipFlag;

				SqlParameter paramItemID = sqlCmd.Parameters.Add( "@itemID" , SqlDbType.Int );
				paramItemID.Direction = ParameterDirection.Output;
				paramItemID.Value = itemID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code == DbResultCode.SUCCESS )
						itemID = (Int32)paramItemID.Value;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode SchoolTransferLog( String platformID , Int16 prevClanCode , Int16 transferClanCode )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.UP_TOOL_SCHOOL_TRANSFER" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@I_CHANNEL_USER_ID" , SqlDbType.NVarChar , (int)MAXLENGTH.SCHOOL_CHANNEL_USER_ID );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramPrevClanCode = sqlCmd.Parameters.Add( "@I_PREV_CLAN_CODE" , SqlDbType.SmallInt );
				paramPrevClanCode.Direction = ParameterDirection.Input;
				paramPrevClanCode.Value = prevClanCode;

				SqlParameter paramTransferClanCode = sqlCmd.Parameters.Add( "@I_TRANSFER_CLAN_CODE" , SqlDbType.SmallInt );
				paramTransferClanCode.Direction = ParameterDirection.Input;
				paramTransferClanCode.Value = transferClanCode;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code != DbResultCode.SUCCESS )
						Logger.ErrLog( "School Transfer Log Insert Fail : platformID={0}, prevClanClde={1}, transferClanCode={2}" , platformID , prevClanCode , transferClanCode );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode LoadNoti( List<DBNotiInfo> notiList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoadNoti" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( reader.Read() )
						{
							DBNotiInfo notiInfo = new DBNotiInfo();
							notiInfo = new DBNotiInfo();
							notiInfo.notiSeq = (Int32)reader["NOTI_SEQ"];
							notiInfo.type = (Byte)reader["TYPE"];
							notiInfo.cycle = (Byte)reader["CYCLE"];
							notiInfo.interval = (Byte)reader["INTERVAL"];
							notiInfo.startTicks = ( (DateTime)reader["START_TIME"] ).Ticks;
							notiInfo.endTicks = ( (DateTime)reader["END_TIME"] ).Ticks;
							notiInfo.notiMsgs = String.Format( "{0}/{1}/{2}/{3}/{4}" , (String)reader["MSG_1"] , (String)reader["MSG_2"] , (String)reader["MSG_3"] , (String)reader["MSG_4"] , (String)reader["MSG_5"] );
							notiInfo.isOpen = (bool)reader["ISOPEN"];

							notiList.Add( notiInfo );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GetNotiInfo( Int32 notiSeq , out DBNotiInfo notiInfo )
		{
			notiInfo = null;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetNotiInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramNotiSeq= sqlCmd.Parameters.Add( "@notiSeq" , SqlDbType.Int );
				paramNotiSeq.Direction = ParameterDirection.Input;
				paramNotiSeq.Value = notiSeq;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows ) // if 주의해서
					{
						if ( reader.Read() ) // if를 주의하자.
						{
							notiInfo = new DBNotiInfo();
							notiInfo.notiSeq = (Int32)reader["NOTI_SEQ"];
							notiInfo.type = (Byte)reader["TYPE"];
							notiInfo.cycle = (Byte)reader["CYCLE"];
							notiInfo.interval = (Byte)reader["INTERVAL"];
							notiInfo.startTicks = ( (DateTime)reader["START_TIME"] ).Ticks;
							notiInfo.endTicks = ( (DateTime)reader["END_TIME"] ).Ticks;
							notiInfo.notiMsgs = String.Format( "{0}/{1}/{2}/{3}/{4}" , (String)reader["MSG_1"] , (String)reader["MSG_2"] , (String)reader["MSG_3"] , (String)reader["MSG_4"] , (String)reader["MSG_5"] );
							notiInfo.isOpen = (bool)reader["ISOPEN"];
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateNotiIsOpen( Int32 notiSeq , bool isOpen )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateNotiOpen" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramNotiSeq = sqlCmd.Parameters.Add( "@notiSeq" , SqlDbType.Int );
				paramNotiSeq.Direction = ParameterDirection.Input;
				paramNotiSeq.Value = notiSeq;

				SqlParameter paramIsOpen = sqlCmd.Parameters.Add( "@isOpen" , SqlDbType.Bit );
				paramIsOpen.Direction = ParameterDirection.Input;
				paramIsOpen.Value = isOpen;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode LoggingAU( Int64 userUID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoggingAU" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramStep = sqlCmd.Parameters.Add( "@step" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)0;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "LoggingAU() fail, UserUID = {0}, MSSQL ERROR_NUMBER = {1}, STEP = {2}" , userUID , paramResult.Value , paramStep.Value );
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode LoggingRevenue( Int64 userUID , String marketName , Int64 sale )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoggingRevenue" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramStep = sqlCmd.Parameters.Add( "@step" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)( -1 );

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramMarketName = sqlCmd.Parameters.Add( "@marketName" , SqlDbType.NVarChar , 16 );
				paramMarketName.Direction = ParameterDirection.Input;
				if ( marketName.Length > 16 )
				{
					marketName = marketName.Substring( 0 , 16 );
				}
				paramMarketName.Value = marketName.ToUpper().Trim();

				SqlParameter paramSale = sqlCmd.Parameters.Add( "@sale" , SqlDbType.Money );
				paramSale.Direction = ParameterDirection.Input;
				paramSale.Value = sale;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "LoggingRevenue() fail, UserUID = {0}, marketName = {1}, sale = {2}" , userUID , marketName , sale );
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode LoggingRU( Int64 userUID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_LoggingRU" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramStep = sqlCmd.Parameters.Add( "@step" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)0;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "LoggingRU() fail, UserUID = {0}, MSSQL ERROR_NUMBER = {1}, STEP = {2}" , userUID , paramResult.Value , paramStep.Value );
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode LogPeriodItemUpdateExpiration( Int64 nUserUID , Int64 nItemUID , Int32 nItemID , Int16 nIncreaseDay ,
			Byte MethodType , ref DateTime dtExpirationDate , ref DateTime dtUpdateItemDate )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.p_LogPeriodItemUpdateExpiration" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = nUserUID;

				SqlParameter paramItemUID = sqlCmd.Parameters.Add( "@ItemUID" , SqlDbType.BigInt );
				paramItemUID.Direction = ParameterDirection.Input;
				paramItemUID.Value = nItemUID;

				SqlParameter paramItemID = sqlCmd.Parameters.Add( "@ItemID" , SqlDbType.Int );
				paramItemID.Direction = ParameterDirection.Input;
				paramItemID.Value = nItemID;

				SqlParameter paramIncreaseDay = sqlCmd.Parameters.Add( "@IncreaseDay" , SqlDbType.SmallInt );
				paramIncreaseDay.Direction = ParameterDirection.Input;
				paramIncreaseDay.Value = nIncreaseDay;

				SqlParameter paramMethodType = sqlCmd.Parameters.Add( "@MethodType" , SqlDbType.TinyInt );
				paramMethodType.Direction = ParameterDirection.Input;
				paramMethodType.Value = MethodType;

				SqlParameter paramExpirationDate = sqlCmd.Parameters.Add( "@ExpirationDate" , SqlDbType.DateTime );
				paramExpirationDate.Direction = ParameterDirection.Input;
				paramExpirationDate.Value = dtExpirationDate;

				SqlParameter paramUpdatedItemDate = sqlCmd.Parameters.Add( "@UpdatedItemDate" , SqlDbType.DateTime );
				paramUpdatedItemDate.Direction = ParameterDirection.Input;
				paramUpdatedItemDate.Value = dtUpdateItemDate;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					if ( result_code != DbResultCode.SUCCESS )
					{
						Logger.ErrLog( "LogPeriodItemUpdateExpiration() fail, UserUID = {0}, ItemUID = {1}, ItemID = {2}, IncreaseDay = {3}, ExpirationDate = {4}, UpdateItemDate = {5}" ,
							nUserUID , nItemUID , nItemID , nIncreaseDay , dtExpirationDate , dtUpdateItemDate );
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}


		public DbResultCode LogUserRegist( UserRegistLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserRegist" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = UserRegistLogData.ConvertToTable( new UserRegistLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

        public DbResultCode LogUserLogin( UserLoginLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserLogin", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserLoginLogData.ConvertToTable( new UserLoginLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserLogout( UserLogoutLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserLogout", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserLogoutLogData.ConvertToTable( new UserLogoutLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserReturn( UserReturnLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserReturn", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserReturnLogData.ConvertToTable( new UserReturnLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserWithdraw( UserWithdrawLogData kLogData, Int64 _lUserUID )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserWithdraw", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID", SqlDbType.BigInt );
                paramUserUID.Direction  = ParameterDirection.Input;
                paramUserUID.Value      = _lUserUID;

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserWithdrawCancel( UserWithdrawCancelLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserWithdrawCancel", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserWithdrawCancelLogData.ConvertToTable( new UserWithdrawCancelLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserAttendance( UserAttendanceLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserAttendance", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserAttendanceLogData.ConvertToTable( new UserAttendanceLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public ClientErrorMessage LogUserSevenAttendance( Int64 _UserUID, Int16 _AttendanceDay )
		{
			ClientErrorMessage eResult = ClientErrorMessage.ERROR_NORMAL;

			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return eResult;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserSevenAttendance", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction		= ParameterDirection.ReturnValue;
				paramResult.Value			= (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID	= sqlCmd.Parameters.Add( "@in_UserUID", SqlDbType.BigInt );
				paramUserUID.Direction		= ParameterDirection.Input;
				paramUserUID.Value			= _UserUID;

				SqlParameter paramDay		= sqlCmd.Parameters.Add( "@in_Day", SqlDbType.SmallInt );
				paramDay.Direction			= ParameterDirection.Input;
				paramDay.Value				= _AttendanceDay;

				try
				{
					dbConn.Open();
					sqlCmd.ExecuteNonQuery();
					eResult = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return eResult;
		}
        public DbResultCode LogUserDailyMissionRefresh( UserDailyMissionRefreshLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserDailyMissionRefresh", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserDailyMissionRefreshLogData.ConvertToTable( new UserDailyMissionRefreshLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserDailyMissionComplete( Int64 _UserUID, Int32 _MissionID )
        {
			DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
				return result_code;
            }

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_UserDailyMissionComplete", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",		_UserUID	);
				DBDecorator.InParam( paramList, "@in_MissionID",	_MissionID	);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
        }

        public DbResultCode LogUserAchievementComplete( UserAchievementCompleteLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserAchievementComplete", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserAchievementCompleteLogData.ConvertToTable( new UserAchievementCompleteLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserMagicBoxUse( UserMagicBoxUseLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserMagicBoxUse", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserMagicBoxUseLogData.ConvertToTable( new UserMagicBoxUseLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogGetGoodsGameMoney( IncrGameMoneyLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsGameMoney", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrGameMoneyLogData.ConvertToTable( new IncrGameMoneyLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseGoodsGameMoney( DecrGameMoneyLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsGameMoney" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
                paramLogList.Value = DecrGameMoneyLogData.ConvertToTable( new DecrGameMoneyLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogGoodsHistory( Int64 _UserUID, AttachmentType _eGoodsType, Int32 _Before, Int32 _Variance, Int32 _After, Int32 _Cause )
        {
			DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
				return result_code;
            }

			using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_GoodsHistory", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",		_UserUID			);
				DBDecorator.InParam( paramList, "@in_GoodsType",	(Int16)_eGoodsType	);
				DBDecorator.InParam( paramList, "@in_Before",		_Before				);
				DBDecorator.InParam( paramList, "@in_Variance",		_Variance			);
				DBDecorator.InParam( paramList, "@in_After",		_After				);
				DBDecorator.InParam( paramList, "@in_Cause",		_Cause				);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
        }

		public DbResultCode LogGuerrillaEventGiveReward( PacketDBRqGuerrillaEventGiveReward _kRecv )
        {
			DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
				return result_code;
            }

			using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_GuerrillaGiveReward", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",				_kRecv.UserUID				);
				DBDecorator.InParam( paramList, "@in_EventID",				_kRecv.EventID				);
				DBDecorator.InParam( paramList, "@in_Type",					_kRecv.Type					);
				DBDecorator.InParam( paramList, "@in_Action",				_kRecv.Action				);
				DBDecorator.InParam( paramList, "@in_DetailNum",			_kRecv.DetailNum			);
				DBDecorator.InParam( paramList, "@in_Value",				_kRecv.Value				);
				DBDecorator.InParam( paramList, "@in_RewardItemID",			_kRecv.RewardItemID			);
				DBDecorator.InParam( paramList, "@in_RewardItemQuantity",	_kRecv.RewardItemQuantity	);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
        }

        public DbResultCode LogGetGoodsCash( IncrCashLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsCash", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrCashLogData.ConvertToTable( new IncrCashLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseGoodsCash( DecrCashLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsCash", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = DecrCashLogData.ConvertToTable( new DecrCashLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogGetGoodsEnergy( IncrEnergyLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsEnergy", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrEnergyLogData.ConvertToTable( new IncrEnergyLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseGoodsEnergy( DecrEnergyLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsEnergy", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = DecrEnergyLogData.ConvertToTable( new DecrEnergyLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogGetGoodsMagicPoint( IncrMagicPointLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsMagicPoint", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrMagicPointLogData.ConvertToTable( new IncrMagicPointLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseGoodsMagicPoint( DecrMagicPointLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GoodsMagicPoint", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = DecrMagicPointLogData.ConvertToTable( new DecrMagicPointLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUserRankPoint( UserRankPointLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_UserRankPoint", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = UserRankPointLogData.ConvertToTable( new UserRankPointLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public DbResultCode LogUserEXP( PacketDBRqUserEXPLog _kPacket )
        {
			DbResultCode result_code = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
				return result_code;
            }

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_UserEXP", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",		_kPacket.UserUID		);
				DBDecorator.InParam( paramList, "@in_Before",		_kPacket.BeforeAmount	);
				DBDecorator.InParam( paramList, "@in_Variance",		_kPacket.Variance		);
				DBDecorator.InParam( paramList, "@in_After",		_kPacket.AfterAmount	);
				DBDecorator.InParam( paramList, "@in_Cause",		_kPacket.eCause			);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
        }

        public DbResultCode LogUserLevel( PacketDBRqUserLevelLog _kPacket )
        {
			DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
				return result_code;
            }

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_UserLevel", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",		_kPacket.UserUID		);
				DBDecorator.InParam( paramList, "@in_Before",		_kPacket.BeforeAmount	);
				DBDecorator.InParam( paramList, "@in_Variance",		_kPacket.Variance		);
				DBDecorator.InParam( paramList, "@in_After",		_kPacket.AfterAmount	);
				DBDecorator.InParam( paramList, "@in_Cause",		_kPacket.eCause			);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
        }

        public DbResultCode LogGetItemCharacter( IncrItemCharacterLogData[] kLogDataArray )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCharacter", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrItemCharacterLogData.ConvertToTable( kLogDataArray );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogItemCharacterMastery( IncrItemCharacterMasteryLogData[] kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCharacterMastery", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrItemCharacterMasteryLogData.ConvertToTable( kLogData );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseItemCharacter( DecrItemCharacterLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCharacter", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = DecrItemCharacterLogData.ConvertToTable( new DecrItemCharacterLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogGetItemCarParts( IncrItemCarPartsLogData[] kLogDataArray )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCarParts", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrItemCarPartsLogData.ConvertToTable( kLogDataArray );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogItemCarPartsUpgradeStart( ItemCarPartsUpgradeStartLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCarPartsUpgradeStart", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = ItemCarPartsUpgradeStartLogData.ConvertToTable( new ItemCarPartsUpgradeStartLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogItemCarPartsUpgradeComplete( ItemCarPartsUpgradeCompleteLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCarPartsUpgradeComplete", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = ItemCarPartsUpgradeCompleteLogData.ConvertToTable( new ItemCarPartsUpgradeCompleteLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseItemCarParts( DecrItemCarPartsLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCarParts", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = DecrItemCarPartsLogData.ConvertToTable( new DecrItemCarPartsLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public DbResultCode LogItemCarPartsPaint( ItemCarPartsPaintLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCarPartsPaint" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = ItemCarPartsPaintLogData.ConvertToTable( new ItemCarPartsPaintLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

        public DbResultCode LogItemCarPartsHiddenSkill( ItemCarPartsHiddenSkillLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCarPartsHiddenSkill", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = ItemCarPartsHiddenSkillLogData.ConvertToTable( new ItemCarPartsHiddenSkillLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogGetItemRecipe( IncrItemRecipeLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemRecipe", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = IncrItemRecipeLogData.ConvertToTable( new IncrItemRecipeLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogUseItemRecipe( DecrItemRecipeLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if ( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemRecipe", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = ( Int32 )DbResultCode.DB_ERROR;

                SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramUserUID.Direction = ParameterDirection.Input;
                paramUserUID.Value = DecrItemRecipeLogData.ConvertToTable( new DecrItemRecipeLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = ( DbResultCode )Convert.ToInt32( paramResult.Value );
                }
                catch ( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public DbResultCode LogGetItemElement( IncrItemElementLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemElement" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = IncrItemElementLogData.ConvertToTable( new IncrItemElementLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogUseItemElement( DecrItemElementLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemElement" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = DecrItemElementLogData.ConvertToTable( new DecrItemElementLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogGetItemCoupon( IncrItemCouponLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCoupon" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = IncrItemCouponLogData.ConvertToTable( new IncrItemCouponLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogUseItemCoupon( DecrItemCouponLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCoupon" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = DecrItemCouponLogData.ConvertToTable( new DecrItemCouponLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogInAppShopPurchaseEnergy( InAppShopPurchaseEnergyLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_InAppShopPurchaseEnergy" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = InAppShopPurchaseEnergyLogData.ConvertToTable( new InAppShopPurchaseEnergyLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogInAppShopPurchaseGameMoney( InAppShopPurchaseGameMoneyLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_InAppShopPurchaseGameMoney" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = InAppShopPurchaseGameMoneyLogData.ConvertToTable( new InAppShopPurchaseGameMoneyLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogPostSend( DataTable _kPostLogList )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_PostSend" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult	= sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction		= ParameterDirection.ReturnValue;
				paramResult.Value			= (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramLogList	= sqlCmd.Parameters.Add( "@in_LogList", SqlDbType.Structured );
				paramLogList.Direction		= ParameterDirection.Input;
				paramLogList.Value			= _kPostLogList;

				try
				{
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogPostDelete( PostDeleteLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_PostDelete" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = PostDeleteLogData.ConvertToTable( new PostDeleteLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

        public DbResultCode LogPostArrayDelete(PostDeleteLogData[] kLogDataArray)
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.LOG_PostDelete", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramLogList = sqlCmd.Parameters.Add("@LogList", SqlDbType.Structured);
                paramLogList.Direction = ParameterDirection.Input;
                paramLogList.Value = PostDeleteLogData.ConvertToTable(kLogDataArray);

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public DbResultCode LogPostAttachmentAcquire( PostAttachmentAcquireLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_PostAttachmentAcquire" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = PostAttachmentAcquireLogData.ConvertToTable( new PostAttachmentAcquireLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogCareerStageComplete( CareerStageOpenLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_CareerStageComplete" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = CareerStageOpenLogData.ConvertToTable( new CareerStageOpenLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogCareerStageClear( NetmarbleLogCareerStageClear kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_CareerStageClear", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = NetmarbleLogCareerStageClear.ConvertToTable( new NetmarbleLogCareerStageClear[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogCareerEpisodeComplete( CareerEpisodeCompleteLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_CareerEpisodeComplete" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = CareerEpisodeCompleteLogData.ConvertToTable( new CareerEpisodeCompleteLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogCareerFavorAsk( CareerFavorAskLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_CareerFavorAsk" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = CareerFavorAskLogData.ConvertToTable( new CareerFavorAskLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogCareerFavorAccept( CareerFavorAcceptLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_CareerFavorAccept" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = CareerFavorAcceptLogData.ConvertToTable( new CareerFavorAcceptLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogInGameFriendRequest( InGameFriendRequestLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_InGameFriendRequest" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = InGameFriendRequestLogData.ConvertToTable( new InGameFriendRequestLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogInGameFriendAccept( InGameFriendAcceptLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_InGameFriendAccept" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = InGameFriendAcceptLogData.ConvertToTable( new InGameFriendAcceptLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogInGameFriendDelete( InGameFriendDeleteLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_InGameFriendDelete" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = InGameFriendDeleteLogData.ConvertToTable( new InGameFriendDeleteLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogSocialEnergySend( SocialEnergySendLogData[] kLogDataArray )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_SocialEnergySend" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
                paramLogList.Value = SocialEnergySendLogData.ConvertToTable( kLogDataArray );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogGachaRaceReward( GachaRaceRewardLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GachaRaceReward" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = GachaRaceRewardLogData.ConvertToTable( new GachaRaceRewardLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogGachaTotal(GachaTotalLogData kLogData)
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if (Manager.IsOpen() == false)
					Manager.Open();
			}
			catch (System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.LOG_GachaTotal", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add("@LogList", SqlDbType.Structured);
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.TypeName = "dbo.GachaTotalList";
				paramLogList.Value = GachaTotalLogData.ConvertToTable(new GachaTotalLogData[1] { kLogData });

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
				}
				catch (Exception ex)
				{
					Logger.ExceptionLog(ex);
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

        public DbResultCode LogGachaTotalTenPull(List<GachaTotalLogData> _kLogDataList)
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if(Manager.IsOpen() == false)
                    Manager.Open();
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.LOG_GachaTotalTenPull", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@ret", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramLogList = sqlCmd.Parameters.Add("@LogList", SqlDbType.Structured);
                paramLogList.Direction = ParameterDirection.Input;
                paramLogList.TypeName = "dbo.GachaTotalList";
                paramLogList.Value = GachaTotalLogData.ConvertToTable(_kLogDataList);

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public DbResultCode LogGachaItemCarPartsHiddenSkill( GachaItemCarPartsHiddenSkillLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GachaItemCarPartsHiddenSkill" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = GachaItemCarPartsHiddenSkillLogData.ConvertToTable( new GachaItemCarPartsHiddenSkillLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogGachaItemCarParts( GachaItemCarPartsLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GachaItemCarParts" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = GachaItemCarPartsLogData.ConvertToTable( new GachaItemCarPartsLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogGachaItemPaint( GachaItemPaintLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_GachaItemPaint" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = GachaItemPaintLogData.ConvertToTable( new GachaItemPaintLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogDecomposeItem( DecomposeItemLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_DecomposeItem" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = DecomposeItemLogData.ConvertToTable( new DecomposeItemLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

        public DbResultCode LogComposeItem( ComposeItemLogData kLogData )
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }
            using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ComposeItem", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramLogList.Direction = ParameterDirection.Input;
                paramLogList.Value = ComposeItemLogData.ConvertToTable( new ComposeItemLogData[1] { kLogData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogItemEnhance(ItemEnhanceLogData _logData)
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }
            using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemEnhance", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramLogList.Direction = ParameterDirection.Input;
                paramLogList.Value = ItemEnhanceLogData.ConvertToTable( new ItemEnhanceLogData[1] { _logData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

        public DbResultCode LogItemCompose(ItemComposeLogData _logData)
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;

            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }
            using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ItemCompose", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
                paramLogList.Direction = ParameterDirection.Input;
                paramLogList.Value = ItemComposeLogData.ConvertToTable( new ItemComposeLogData[1] { _logData } );

                try
                {
                    // 실행직전 Open
                    dbConn.Open();
                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
                    eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
                }
                catch( Exception ex )
                {
                    Logger.ExceptionLog( ex );
                    eResult = DbResultCode.DB_ERROR;
                }
            }

            return eResult;
        }

		public DbResultCode LogProductionViaRecipe( ProductionViaRecipeLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.LOG_ProductionVIARecipe", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.TypeName = "dbo.ProductionVIARecipeList";
				paramLogList.Value = ProductionViaRecipeLogData.ConvertToTable( new ProductionViaRecipeLogData[1] { kLogData } );

				SqlParameter paramElementLogList = sqlCmd.Parameters.Add( "@ElementLogList" , SqlDbType.Structured );
				paramElementLogList.Direction = ParameterDirection.Input;
				paramElementLogList.TypeName = "dbo.ProductionVIARecipeConsumeElementList";
				paramElementLogList.Value = ProductionViaRecipeLogData.ConvertToConsumeElementTable( new ProductionViaRecipeLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogRaceTrack( PacketDBRqRaceTrackLog _kPacket )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_RaceTrack", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_RoomUID",	(Int64)_kPacket.GameRoomUID		);
				DBDecorator.InParam( paramList, "@in_Mode",		_kPacket.GameMode				);
				DBDecorator.InParam( paramList, "@in_Random",	_kPacket.IsRandom				);
				DBDecorator.InParam( paramList, "@in_TrackID",	_kPacket.TrackID				);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
		}

		public DbResultCode LogRaceTrack( RaceTrackLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_RaceTrack" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = RaceTrackLogData.ConvertToTable( new RaceTrackLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogRaceLapTime( PacketDBRqRaceLapTimeLog _kPacket )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				return result_code;
			}
			using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_RaceLapTime", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",		_kPacket .UserUID									);
				DBDecorator.InParam( paramList, "@in_GameRoomUID",	Convert.ToString(_kPacket.GameRoomUID)				);
				DBDecorator.InParam( paramList, "@in_GameMode",		( (KMGameMode)_kPacket.GameMode ).ToString( "F" )	);
				DBDecorator.InParam( paramList, "@in_TrackID",		_kPacket.TrackID									);
				DBDecorator.InParam( paramList, "@in_MaxLapCount",	_kPacket.MaxLabCount								);
				DBDecorator.InParam( paramList, "@in_IsRetire",		_kPacket.IsRetire									);
				DBDecorator.InParam( paramList, "@in_LapTime",		_kPacket.LapTime									);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
		}

		public DbResultCode LogRaceReward( PacketDBRqRaceRewardLog _kPacket )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if (Manager.IsOpen() == false)
					Manager.Open();
			}
			catch (System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				return result_code;
			}

			using (SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = DBDecorator.CreateCommand( "dbo.LOG_RaceReward", dbConn );
				SqlParameterCollection paramList = DBDecorator.Return( sqlCmd, "@Result" );

				DBDecorator.InParam( paramList, "@in_UserUID",		_kPacket.UserUID		);
				DBDecorator.InParam( paramList, "@in_GameChannel",	(Int32)_kPacket.GameChannel );
				DBDecorator.InParam( paramList, "@in_GameMode",		(Int32)_kPacket.GameMode );
				DBDecorator.InParam( paramList, "@in_TrackID",		_kPacket.TrackID		);
				DBDecorator.InParam( paramList, "@in_GetSilver",	_kPacket.GetSilver		);
				DBDecorator.InParam( paramList, "@in_MySilver",		_kPacket.MySilver		);
				DBDecorator.InParam( paramList, "@in_GetExp",		_kPacket.GetExp			);
				DBDecorator.InParam( paramList, "@in_MyExp",		_kPacket.MyExp			);
				DBDecorator.InParam( paramList, "@in_MyLevel",		_kPacket.MyLevel		);
				DBDecorator.InParam( paramList, "@in_GetEloPoint",	_kPacket.GetEloPoint	);
				DBDecorator.InParam( paramList, "@in_MyEloPoint",	_kPacket.MyEloPoint		);

				result_code = DBDecorator.Execute( sqlCmd );
			}

			return result_code;
		}

		public DbResultCode LogInAppPurchase( InAppPurchaseLogData kLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_InAppPurchase" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList" , SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = InAppPurchaseLogData.ConvertToTable( new InAppPurchaseLogData[1] { kLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode RemoveUserInfo(String _strPlatformID)
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if(Manager.IsOpen() == false)
					Manager.Open();
			}
			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
                SqlCommand sqlCmd = new SqlCommand("dbo.RESET_SIMPLE_UserByNickname", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add("@PlatformID", SqlDbType.VarChar, 40);
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = _strPlatformID;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS != eResult)
					{
						Logger.ErrLog("RESET_UserByPlatformID is Fail PlatformID: {0}, Result_code: {1}", _strPlatformID, eResult);
					}
				}
				catch(Exception ex)
				{
					Logger.ExceptionLog(ex);
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode FlowerEventReceived(String _strPlatformID, Byte _byFlowerEventType, Int32 _nFlowerCount)
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if(Manager.IsOpen() == false)
					Manager.Open();
			}
			catch(System.Exception ex)
			{
				Logger.ExceptionLog(ex);
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
			{
				SqlCommand sqlCmd = new SqlCommand("dbo.LOG_FlowerEventReceived", dbConn);
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add("@PlatformID", SqlDbType.VarChar, 40);
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = _strPlatformID;

				SqlParameter paramFlowerEventType = sqlCmd.Parameters.Add("@FlowerEventType", SqlDbType.TinyInt);
				paramFlowerEventType.Direction = ParameterDirection.Input;
				paramFlowerEventType.Value = _byFlowerEventType;

				SqlParameter paramFlowerCount = sqlCmd.Parameters.Add("@FlowerCount", SqlDbType.Int);
				paramFlowerCount.Direction = ParameterDirection.Input;
				paramFlowerCount.Value = _nFlowerCount;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32(paramResult.Value);
					if(DbResultCode.SUCCESS != eResult)
					{
						Logger.ErrLog("LOG_FlowerEventReceived is Fail PlatformID: {0}, Result_code: {1}", _strPlatformID, eResult);
					}
				}
				catch(Exception ex)
				{
					Logger.ExceptionLog(ex);
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LogBuyConsumeItem( ConsumeItemBuyLogData _kConsumeItemBuyLogData )
		{
			DbResultCode eResult = DbResultCode.DB_ERROR;

			try
			{
				if( Manager.IsOpen() == false )
					Manager.Open();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				eResult = DbResultCode.DB_ERROR;
				return eResult;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.LOG_ConsumeItemBuy", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramLogList = sqlCmd.Parameters.Add( "@LogList", SqlDbType.Structured );
				paramLogList.Direction = ParameterDirection.Input;
				paramLogList.Value = ConsumeItemBuyLogData.ConvertToTable( new ConsumeItemBuyLogData[1] { _kConsumeItemBuyLogData } );

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					eResult = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch( Exception ex )
				{
					Logger.ExceptionLog( ex );
					eResult = DbResultCode.DB_ERROR;
				}
			}

			return eResult;
		}

		public DbResultCode LoadToolEventInfo( ref List<TD_TOOL_EVENT_DATA> liEventData )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.UP_TOOL_EVENT_LOAD_ACTIVATED", dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret", SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if( reader.HasRows )
					{
						while( reader.Read() )
						{
							TD_TOOL_EVENT_DATA kEventData = new TD_TOOL_EVENT_DATA();
							kEventData.SeqID		= (Int64)reader["EVENT_SEQ"];
							kEventData.nStartTicks	= ( (DateTime)reader["RESERVE_DATE"] ).Ticks;
							kEventData.nState		= (Byte)reader["STATE"];
							kEventData.nCmdType		= (Int32)reader["CMD"];
							kEventData.strJsonData	= (String)reader["JSON_STRING"];
							kEventData.Comment		= (String)reader["COMMENT"];
							liEventData.Add( kEventData );
						}
					}
				}
				catch( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

        public DbResultCode ResetUserInfo(String _strNickname)
        {
            DbResultCode eResult = DbResultCode.DB_ERROR;
            try
            {
                if( Manager.IsOpen() == false )
                    Manager.Open();
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                eResult = DbResultCode.DB_ERROR;
                return eResult;
            }

            using( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand sqlCmd = new SqlCommand( "dbo.RESET_SIMPLE_UserByNickname", dbConn );
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@Nickname", SqlDbType.VarChar, 128 );
                paramPlatformID.Direction = ParameterDirection.Input;
                paramPlatformID.Value = _strNickname;

                SqlDataReader   reader = null;
                String          strMeesage = "";
                try
                {
                    // 실행직전 오픈
                    dbConn.Open();
                    reader = sqlCmd.ExecuteReader();
                    if( reader.HasRows )
                    {
                        while( reader.Read() )
                            strMeesage = (String)reader["ErrorMessage"];
                    }
                }
                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
                finally
                {
                    reader.Close();
                    
                    if(false == strMeesage.Equals("SUCCESS"))
                    {
                        Logger.ErrLog( "RESET_SIMPLE_UserByNickname is Fail Nickname: {0}, ErrorMessage: {1}", _strNickname, strMeesage );
                    }
                }
            }

            return eResult;
        }

        public DbResultCode CheckAdvanceUser(Int64 UserUID, String PlatformID, ref Int32 RewardStar)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_CheckAdvanceUser", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult        = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction           = ParameterDirection.ReturnValue;
                paramResult.Value               = (Int32)DbResultCode.DB_ERROR;

                SqlParameter paramPlatformID    = sqlCmd.Parameters.Add("@PlatformID", SqlDbType.NVarChar, 40);
                paramPlatformID.Direction       = ParameterDirection.Input;
                paramPlatformID.Value           = PlatformID;

                SqlParameter paramRewardStar    = sqlCmd.Parameters.Add("@RewardStar", SqlDbType.Int);
                paramRewardStar.Direction       = ParameterDirection.Output;
                paramRewardStar.Value           = RewardStar;

                try
                {
                    // 실행직전 오픈
                    dbConn.Open();

                    Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

                    eDbResultCode = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(eDbResultCode != DbResultCode.SUCCESS && eDbResultCode != DbResultCode.DB_ERROR_IS_NOT_ADVANCE_USER)
                        Logger.ErrLog("P_CheckAdvanceUser Fail UserUID: {0}, PlatformID: {1}, ResultCode: {2}", UserUID, PlatformID, eDbResultCode);
                    else
                        RewardStar = (Int32)paramRewardStar.Value;
                }
                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
            }

            return eDbResultCode;
        }		
	}
}
