﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;

namespace SCommonLib.DB.Handler
{
    abstract public class ReadQueryHandler : DBCmd
    {
        public ReadQueryHandler(SqlManager mgr)
            : base(mgr)
        {

        }

        protected override void Bind() { throw new NotImplementedException(); }
        private DbCommand BeginExecute()
        {
            // 열려 있는지 확인
            m_result = DbResultCode.SUCCESS;
            if (m_sql_mgr.IsOpen() == false)
            {
                m_result = DbResultCode.DB_ERROR;
                return null;
            }

            Bind();
            // 커맨드 생성
            return m_sql_mgr.CreateCommand(m_sp_name, m_inParams);
        }

        public override void Execute()
        {
            Batch();
        }

        protected abstract void OnRead(IDataReader reader);
        public void Batch()
        {

            DbCommand cmd = BeginExecute();
            if (cmd == null)
                throw new InvalidOperationException();


            using (DbConnection conn = m_sql_mgr.CreateConnection())
            {
                DbDataReader reader = null;
                try
                {
                    cmd.Connection = conn;
                    cmd.Connection.Open();

                    reader = cmd.ExecuteReader() as DbDataReader;
                    if (reader == null)
                        throw new InvalidOperationException();

                    OnRead(reader);
                }
                catch (System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch (System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch (System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    reader.Close();
                    m_result = (DbResultCode)cmd.Parameters[DBCmd.paramResultName].Value;
                    if (m_result != DbResultCode.SUCCESS)
                        throw new InvalidOperationException("Fail!!");
                }
            }
        }
    }
}
