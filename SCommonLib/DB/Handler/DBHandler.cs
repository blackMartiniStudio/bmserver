﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace SCommonLib.DB.Handler
{
    public delegate void DBWorker( DBCmd cmd);
    abstract public class DBCmd
    {
        
        public static readonly string paramResultName = "out_Result";
        public DBWorker m_dbWorker;
        protected String m_sp_name;
        protected DbResultCode m_result;
        protected String m_err_msg;
        protected SqlManager m_sql_mgr;
        protected List<DbParameter> m_inParams = new List<DbParameter>();

        public bool m_isComplete;


        public DBCmd(SqlManager mgr)
        {
            m_sql_mgr = mgr;
        }

        #region [property]
        public String ProceduerName
        {
            get { return m_sp_name; }
        }

        public DbResultCode DBResult
        {
            get { return m_result; }
        }

        public String ErrMsg
        {
            get { return m_err_msg; }
        }
        #endregion

        #region [In]
        protected void In(String paramName, bool value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }
        protected void In(String paramName, Byte value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }

        protected void In(String paramName, Int16 value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }

        protected void In(String paramName, Int32 value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }

        protected void In(String paramName, Int64 value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }

        protected void In(String paramName, DateTime value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }

        protected void In(String paramName, String value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }

        protected void In(String paramName, Byte[] value, ParameterDirection direction = ParameterDirection.Input)
        {
            m_sql_mgr.InParam(m_inParams, paramName, value, direction);
        }
        #endregion

        protected abstract void Bind();

        public abstract void Execute();
        

    }
    
}
