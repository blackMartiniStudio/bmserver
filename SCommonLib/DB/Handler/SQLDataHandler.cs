﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using SCommonLib.DB;
using System.Text;
using System.Data.Common;
using KMNetClass;

namespace SCommonLib
{
    public class SQLDataHandler : SqlHandler
    {
        public DbResultCode LoadTableColumn(String _strTableName, ref List<String> _kTableColumnList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            try
            {
                if(false == Manager.IsOpen())
                {   
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd   = new SqlCommand("P_LoadTableColumn", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult       = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction          = ParameterDirection.ReturnValue;
                kParamResult.Value              = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamTableName    = kSqlCmd.Parameters.Add("@TableName", SqlDbType.NVarChar, 50);
                kParamTableName.Direction       = ParameterDirection.Input;
                kParamTableName.Value           = _strTableName;

                SqlDataReader DataReader        = null;
                try
                {
                    kDbConn.Open();
                    DataReader = kSqlCmd.ExecuteReader();

                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                            _kTableColumnList.Add((String)DataReader["COLUMN_NAME"]);
                    }
                }

                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    DataReader.Close();
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kParamResult.Value);

                    if(eDbResultCode != DbResultCode.SUCCESS)
                        Logger.ErrLog("P_LoadTableColumn is Fail TableName: {0}", _strTableName);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode LoadTableData(String _strTableName, List<String> _kTableColumnList, ref List<String[]> _KTableDataList)
        {
            DbResultCode eDbResultCode = DbResultCode.DB_ERROR;

            try
            {
                if(false == Manager.IsOpen())
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                eDbResultCode = DbResultCode.DB_ERROR;
                return eDbResultCode;
            }

            using(SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand kSqlCmd   = new SqlCommand("P_LoadTableData", kDbConn);
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamResult       = kSqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                kParamResult.Direction          = ParameterDirection.ReturnValue;
                kParamResult.Value              = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamTableName    = kSqlCmd.Parameters.Add("@TableName", SqlDbType.NVarChar, 50);
                kParamTableName.Direction       = ParameterDirection.Input;
                kParamTableName.Value           = _strTableName;

                SqlDataReader DataReader        = null;
                try
                {
                    kDbConn.Open();
                    DataReader = kSqlCmd.ExecuteReader();

                    if(DataReader.HasRows)
                    {
                        while(DataReader.Read())
                        {
                            List<String> kTableDataList = new List<String>();

                            for(Int32 i = 0; i < _kTableColumnList.Count; ++i)
                                kTableDataList.Add((String)DataReader[_kTableColumnList[i]]);

                            _KTableDataList.Add(kTableDataList.ToArray());
                        }
                    }

                    DataReader.Close();
                }

                catch(System.IndexOutOfRangeException ior)
                {
                    Logger.ExceptionLog(ior);
                }
                catch(System.InvalidOperationException ioe)
                {
                    Logger.ExceptionLog(ioe);
                }
                catch(System.Exception ex)
                {
                    Logger.ExceptionLog(ex);
                }
                finally
                {
                    eDbResultCode = (DbResultCode)Convert.ToInt32(kParamResult.Value);

                    if(eDbResultCode != DbResultCode.SUCCESS)
                        Logger.ErrLog("P_LoadTableData is Fail TableName: {0}", _strTableName);
                }
            }

            return eDbResultCode;
        }

        public DbResultCode GetUpdateConfigDataTableList(DateTime _kCheckDateTime, out List<String> _kUpdateConfigDataTableList)
        {
            _kUpdateConfigDataTableList = new List<String>();

            DbResultCode result_code = DbResultCode.SUCCESS;

            try
            {
                if( false == Manager.IsOpen() )
                {
                    Manager.Open();
                }
            }
            catch( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using( SqlConnection kDbConn = Manager.CreateConnection() as SqlConnection )
            {
                SqlCommand kSqlCmd   = new SqlCommand( "P_GetUpdateConfigDataTableList", kDbConn );
                kSqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter kParamTableName    = kSqlCmd.Parameters.Add( "@CheckDateTime", SqlDbType.DateTime );
                kParamTableName.Direction       = ParameterDirection.Input;
                kParamTableName.Value           = _kCheckDateTime;

                SqlDataReader DataReader = null;
                try
                {
                    kDbConn.Open();
                    DataReader = kSqlCmd.ExecuteReader();

                    if( DataReader.HasRows )
                    {
                        while( DataReader.Read() )
                            _kUpdateConfigDataTableList.Add( (String) DataReader["TableName"] );
                    }

                    DataReader.Close();
                }

                catch( System.IndexOutOfRangeException ior )
                {
                    Logger.ExceptionLog( ior );
                }
                catch( System.InvalidOperationException ioe )
                {
                    Logger.ExceptionLog( ioe );
                }
                catch( System.Exception ex )
                {
                    Logger.ExceptionLog( ex );
                }
            }

            return result_code;
        }
    }
}
