﻿using SCommonLib.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SCommonLib
{
	public class SQLSocialHandler : SqlHandler
	{
		public ClientErrorMessage RegistUser( Int64 userUID , String platformID , UInt32 hashKey )
		{
			ClientErrorMessage result_code = ClientErrorMessage.ERROR_BASE;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_RegistUser" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramHashKey = sqlCmd.Parameters.Add( "@HashKey" , SqlDbType.Int );
				paramHashKey.Direction = ParameterDirection.Input;
				paramHashKey.Value = (Int32)hashKey;


				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (ClientErrorMessage)Convert.ToInt32( paramResult.Value );

					if( result_code == ClientErrorMessage.SUCCESS )
					{
						userUID = (Int64)paramUserUID.Value;
						result_code = (ClientErrorMessage)paramResult.Value;
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetRankScoreByUID( Int64 p_userUid , Byte week , out DBRankScoreInfo p_info )
		{
			p_info = null;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetRankScoreByUserUID" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = p_userUid;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				SqlDataReader result = null;

				try
				{
					// 실행직전 Open
					dbConn.Open();

					result = sqlCmd.ExecuteReader();

					if ( result.HasRows )
					{
						while ( result.Read() )
						{
							p_info = new DBRankScoreInfo( p_userUid
														, (Int32)result["RankScore"]
														, (Int32)result["CarBodyItemID"]
														);
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();

					// 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GetAppFriendRankScoreByUID( String platformIDs , Byte week , List<DBAppFriendsScoreInfo> scoreList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetAppFriendInfoByUserUID" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramAppFriendUID = sqlCmd.Parameters.Add( "@platformIDs" , SqlDbType.VarChar , -1 );
				paramAppFriendUID.Direction = ParameterDirection.Input;
				paramAppFriendUID.Value = platformIDs;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@Week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				SqlDataReader result = null;
				try
				{
					// 실행직전 Open
					dbConn.Open();

					result = sqlCmd.ExecuteReader();
					if ( result.HasRows )
					{
						while ( result.Read() )
						{
							scoreList.Add( new DBAppFriendsScoreInfo( (Int64)result["UserUID"]
															, (Int32)result["RankScore"]
															, (Int32)result["CarBodyItemID"]
															) );
						}
					}

					// 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GetAppFriendInfoByPlatformID( String platformID , Byte week , out DBAppFriendsScoreData data )
		{
			data = null;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetAppFriendInfoByPlatformID" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 30 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@Week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				SqlDataReader result = null;
				try
				{
					// 실행직전 Open
					dbConn.Open();

					result = sqlCmd.ExecuteReader();
					if ( result.HasRows )
					{
						if ( result.Read() == true )
						{
							data = new DBAppFriendsScoreData(
																(Int64)result["UserUID"]
																, (String)result["PlatformID"]
																, (Int32)result["RankScore"]
																, (Int32)result["CarBodyItemID"]
																, (bool)result["MsgOptionFlower"]
																, (bool)result["MsgOptionAlarm"]
																, (bool)result["MsgOptionPicture"]
																, (Byte)result["Level"]
																, ( (DateTime)result["LoginTime"] ).Ticks
															);
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GetAppFriendInfo( String strAppFriends , Byte week , List<DBAppFriendsScoreData> data )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetAppFriendInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramStrAppFriends = sqlCmd.Parameters.Add( "@strAppFriends" , SqlDbType.VarChar , -1 );
				paramStrAppFriends.Direction = ParameterDirection.Input;
				paramStrAppFriends.Value = strAppFriends;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@Week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( reader.Read() )
						{
							data.Add( new DBAppFriendsScoreData(
											(Int64)reader["UserUID"]
											, (String)reader["PlatformID"]
											, (Int32)reader["RankScore"]
											, (Int32)reader["CarBodyItemID"]
											, (bool)reader["MsgOptionFlower"]
											, (bool)reader["MsgOptionAlarm"]
											, (bool)reader["MsgOptionPicture"]
											, (Byte)reader["Level"]
											, ( (DateTime)reader["LoginTime"] ).Ticks
							) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode GetAppDummyFriendInfoByPlatformID( UInt32 hashKey , String platformID , out DBAppFriendsScoreData data )
		{
			data = null;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{

				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetAppDummyFriendInfoByPlatformID" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;


				SqlParameter paramAppHashKey = sqlCmd.Parameters.Add( "@HashKey" , SqlDbType.Int );
				paramAppHashKey.Direction = ParameterDirection.Input;
				paramAppHashKey.Value = (Int32)hashKey;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 30 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;



				SqlDataReader result = null;
				try
				{
					dbConn.Open();

					result = sqlCmd.ExecuteReader();
					if ( result.HasRows )
					{
						if ( result.Read() == true )
						{
							data = new DBAppFriendsScoreData(
																(Int64)result["UserUID"]
																, (String)result["PlatformID"]
																, (Int32)result["RankScore"]
																, (Int32)result["CarBodyItemID"]
																, false
																, false
																, false
																, 1
																, 0
															);
						}
					}

				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode InsertDummyScoreData( Int64 userUID , String platformID , UInt32 hashKey )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_InsertDummyScore" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@PlatformID" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramAppHashKey = sqlCmd.Parameters.Add( "@HashKey" , SqlDbType.Int );
				paramAppHashKey.Direction = ParameterDirection.Input;
				paramAppHashKey.Value = (Int32)hashKey;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );

					return result_code;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}

			}

			return result_code;
		}


		public DbResultCode GetAppFriendRankScoreOnly( Int64 appFriendUID , Byte week , out Int32 rankScore )
		{
			rankScore = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetAppFriendRankScoreOnly" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramAppFriendUID = sqlCmd.Parameters.Add( "@AppFriendUID" , SqlDbType.BigInt );
				paramAppFriendUID.Direction = ParameterDirection.Input;
				paramAppFriendUID.Value = appFriendUID;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@Week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				SqlParameter paramRankScore = sqlCmd.Parameters.Add( "@RankScore" , SqlDbType.Int );
				paramRankScore.Direction = ParameterDirection.Output;
				paramRankScore.Value = rankScore;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
						rankScore = (Int32)paramRankScore.Value;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetUserRankInfo( Int64 userUID , Byte week , out Int32 highScore , out Int32 currentScore , out Int64 nextWeeklyUpdateDate )
		{
			highScore = 0;
			currentScore = 0;
			nextWeeklyUpdateDate = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetUserRankInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;


				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@Week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				SqlParameter paramRankScore = sqlCmd.Parameters.Add( "@RankScore" , SqlDbType.Int );
				paramRankScore.Direction = ParameterDirection.Output;
				paramRankScore.Value = currentScore;

				SqlParameter paramHighScore = sqlCmd.Parameters.Add( "@HighScore" , SqlDbType.Int );
				paramHighScore.Direction = ParameterDirection.Output;
				paramHighScore.Value = highScore;

				SqlParameter paramNextWeeklyUpdateDate = sqlCmd.Parameters.Add( "@NextWeeklyUpdateDate" , SqlDbType.DateTime );
				paramNextWeeklyUpdateDate.Direction = ParameterDirection.Output;
				paramNextWeeklyUpdateDate.Value = new DateTime( DateTime.Now.Ticks );

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();

					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
					{
						currentScore = Convert.ToInt32( paramRankScore.Value );
						highScore = Convert.ToInt32( paramHighScore.Value );
					}

					// 실패를 하더라도 다음 갱신일은 가져오게 된다. wish
					nextWeeklyUpdateDate = Convert.ToDateTime( paramNextWeeklyUpdateDate.Value ).Ticks;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetSocialUserInfo( Int64 userUID , ref bool msgGiftFlower , ref bool msgAlarm , ref bool msgPicture , ref Int64 lastLoginTime )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;

			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetSocialUserInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;


				SqlDataReader result = null;

				try
				{
					// 실행직전 Open
					dbConn.Open();

					result = sqlCmd.ExecuteReader();

					if ( result.HasRows )
					{
						if ( result.Read() )
						{

							msgGiftFlower = (bool)result["MsgOptionFlower"];
							msgAlarm = (bool)result["MsgOptionAlarm"];
							msgPicture = (bool)result["MsgOptionPicture"];
							lastLoginTime = ( (DateTime)result["LoginTime"] ).Ticks;
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result.Close();

					// 출력 및 반환 값 매개 변수의 경우 SqlCommand가 완료될 때와 SqlDataReader가 닫힌 후에 값이 설정된다.
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateRankScore( Int64 userUID , String platformID , Byte week , Int32 rankScore , Int32 hightScore , Int32 carBodyID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateRankScore" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramPlatformID = sqlCmd.Parameters.Add( "@platformId" , SqlDbType.VarChar , 40 );
				paramPlatformID.Direction = ParameterDirection.Input;
				paramPlatformID.Value = platformID;

				SqlParameter paramScore = sqlCmd.Parameters.Add( "@rankScore" , SqlDbType.Int );
				paramScore.Direction = ParameterDirection.Input;
				paramScore.Value = rankScore;

				SqlParameter paramHightScore = sqlCmd.Parameters.Add( "@hightScore" , SqlDbType.Int );
				paramHightScore.Direction = ParameterDirection.Input;
				paramHightScore.Value = hightScore;

				SqlParameter paramCarBody = sqlCmd.Parameters.Add( "@carBodyID" , SqlDbType.Int );
				paramCarBody.Direction = ParameterDirection.Input;
				paramCarBody.Value = carBodyID;

				SqlParameter paramWeek = sqlCmd.Parameters.Add( "@week" , SqlDbType.TinyInt );
				paramWeek.Direction = ParameterDirection.Input;
				paramWeek.Value = week;

				try
				{
					// 실행직전 Open
					dbConn.Open();
					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetPrevWeeklyFriendScoreInfo( Int64 userUID , Byte prevWeek , Int64 prevStartTicks , out Int32 myScore , List<WeeklyRankObject> prevRankList )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			myScore = 0;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetPrevWeeklyRankInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramPrevWeek = sqlCmd.Parameters.Add( "@prevWeek" , SqlDbType.TinyInt );
				paramPrevWeek.Direction = ParameterDirection.Input;
				paramPrevWeek.Value = prevWeek;

				SqlParameter paramPrevStartTime = sqlCmd.Parameters.Add( "@prevStartTime" , SqlDbType.DateTime );
				paramPrevStartTime.Direction = ParameterDirection.Input;
				paramPrevStartTime.Value = new DateTime( prevStartTicks );

				SqlParameter paramPrevMyScore = sqlCmd.Parameters.Add( "@myScore" , SqlDbType.Int );
				paramPrevMyScore.Direction = ParameterDirection.Output;
				paramPrevMyScore.Value = myScore;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows )
					{
						while ( reader.Read() )
						{
							prevRankList.Add( new WeeklyRankObject(
								(Int64)reader["UserUID"]
								, (Int32)reader["RankScore"]
								, (Int32)reader["CarBodyItemID"]
								) );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					// 실패해도 가져온다.
					myScore = (Int32)paramPrevMyScore.Value;
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateMsgOption( Int64 userUID , Int32 optionID , bool enable )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateMsgOption" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramOptionID = sqlCmd.Parameters.Add( "@OptionID" , SqlDbType.Int );
				paramOptionID.Direction = ParameterDirection.Input;
				paramOptionID.Value = optionID;

				SqlParameter paramMsgFlag = sqlCmd.Parameters.Add( "@MsgFlag" , SqlDbType.Bit );
				paramMsgFlag.Direction = ParameterDirection.Input;
				paramMsgFlag.Value = Convert.ToInt32( enable );

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateRecoveryRankScore( Int64 userUID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}


			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_RecoveryRankScore" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetSchoolCompetitionInfo( Int64 userUID , out DBSchoolCompetitionInfo dbSchoolCompetition )
		{
			dbSchoolCompetition = null;

			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_GetSchoolCompetitionInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows ) // if 주의해서
					{
						if ( reader.Read() ) // if를 주의하자.
						{
							dbSchoolCompetition = new DBSchoolCompetitionInfo(
								(Int16)reader["ClanCode"]
								, ( (DateTime)reader["AdmissionSchoolTime"] ).Ticks
								, (Int16)reader["TransferClanCode"]
								, ( (DateTime)reader["TransferSchoolTime"] ).Ticks
								, ( (DateTime)reader["NextUpdateTime"] ).Ticks );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode AddSchoolAdmission( Int64 userUID , Int16 clanCode , out Int64 admissionTime , out Int64 nextUpdateTime )
		{
			admissionTime = nextUpdateTime = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_AddSchoolAdmission" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramClanCode = sqlCmd.Parameters.Add( "@clanCode" , SqlDbType.SmallInt );
				paramClanCode.Direction = ParameterDirection.Input;
				paramClanCode.Value = clanCode;

				SqlParameter paramAdmissionTime = sqlCmd.Parameters.Add( "@admissionTime" , SqlDbType.DateTime );
				paramAdmissionTime.Direction = ParameterDirection.Output;
				paramAdmissionTime.Value = DateTime.Now;

				SqlParameter paramNextUpdateTime = sqlCmd.Parameters.Add( "@nextUpdateTime" , SqlDbType.DateTime );
				paramNextUpdateTime.Direction = ParameterDirection.Output;
				paramNextUpdateTime.Value = DateTime.Now;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
					{
						admissionTime = ( (DateTime)paramAdmissionTime.Value ).Ticks;
						nextUpdateTime = ( (DateTime)paramNextUpdateTime.Value ).Ticks;
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateSchoolTransfer( Int64 userUID , Int16 transferClanCode , out Int64 transferTime )
		{
			transferTime = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSchoolTransfer" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramTransferClanCode = sqlCmd.Parameters.Add( "@transferClanCode" , SqlDbType.SmallInt );
				paramTransferClanCode.Direction = ParameterDirection.Input;
				paramTransferClanCode.Value = transferClanCode;

				SqlParameter paramTransferTime = sqlCmd.Parameters.Add( "@transferTime" , SqlDbType.DateTime );
				paramTransferTime.Direction = ParameterDirection.Output;
				paramTransferTime.Value = DateTime.Now;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
						transferTime = ( (DateTime)paramTransferTime.Value ).Ticks;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateWeeklySchoolTransfer( Int64 userUID , Int16 transferClanCode , out DBSchoolCompetitionInfo dbSchoolCompetition , out Int16 clanCode )
		{
			clanCode = 0;
			dbSchoolCompetition = null;

			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateWeeklySchoolTransfer" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramUserTransferClanCode = sqlCmd.Parameters.Add( "@userTransferclanCode" , SqlDbType.SmallInt );
				paramUserTransferClanCode.Direction = ParameterDirection.Input;
				paramUserTransferClanCode.Value = transferClanCode;

				SqlParameter paramClanCode = sqlCmd.Parameters.Add( "@clanCode" , SqlDbType.SmallInt );
				paramClanCode.Direction = ParameterDirection.Output;
				paramClanCode.Value = clanCode;

				SqlDataReader reader = null;
				try
				{
					// 실행직전 오픈
					dbConn.Open();
					reader = sqlCmd.ExecuteReader();
					if ( reader.HasRows ) // if 주의해서
					{
						if ( reader.Read() ) // if를 주의하자.
						{
							dbSchoolCompetition = new DBSchoolCompetitionInfo(
								(Int16)reader["ClanCode"]
								, ( (DateTime)reader["AdmissionSchoolTime"] ).Ticks
								, (Int16)reader["TransferClanCode"]
								, ( (DateTime)reader["TransferSchoolTime"] ).Ticks
								, ( (DateTime)reader["NextUpdateTime"] ).Ticks );
						}
					}
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					reader.Close();
					clanCode = (Int16)paramClanCode.Value;
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateSchoolNextUpdateTime( Int64 userUID , out Int64 nextUpdateTime )
		{
			nextUpdateTime = 0;
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSchoolNextUpdate" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramNextUpdateTime = sqlCmd.Parameters.Add( "@nextUpdateTime" , SqlDbType.DateTime );
				paramNextUpdateTime.Direction = ParameterDirection.Output;
				paramNextUpdateTime.Value = DateTime.Now;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
					if ( result_code == DbResultCode.SUCCESS )
						nextUpdateTime = ( (DateTime)paramNextUpdateTime.Value ).Ticks;
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode CheckJoinSchoolCompetition( Int16 clanCode )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_CheckJoinSchoolCompetition" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramClanCode = sqlCmd.Parameters.Add( "@clanCode" , SqlDbType.SmallInt );
				paramClanCode.Direction = ParameterDirection.Input;
				paramClanCode.Value = clanCode;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateSocialUserLevel( Int64 userUID , Byte level )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSocialUserLevel" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramLevel = sqlCmd.Parameters.Add( "@level" , SqlDbType.TinyInt );
				paramLevel.Direction = ParameterDirection.Input;
				paramLevel.Value = level;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateSocialUserLoginTime( Int64 userUID , Int64 loginTicks )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateSocialUserLoginTime" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = userUID;

				SqlParameter paramLoginTime = sqlCmd.Parameters.Add( "@loginTime" , SqlDbType.DateTime );
				paramLoginTime.Direction = ParameterDirection.Input;
				paramLoginTime.Value = new DateTime( loginTicks );

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode UpdateUserLogoutTime( Int64 UserUID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.P_UpdateUserLogoutTime" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@ret" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@userUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = UserUID;

				try
				{
					// 실행직전 오픈
					dbConn.Open();
                    sqlCmd.ExecuteNonQuery();

				}
				catch ( System.IndexOutOfRangeException ior )
				{
					Logger.ExceptionLog( ior );
				}
				catch ( System.InvalidOperationException ioe )
				{
					Logger.ExceptionLog( ioe );
				}
				catch ( System.Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
				finally
				{
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
                    if(DbResultCode.SUCCESS != result_code)
                    {
                        Logger.ErrLog("P_UpdateUserLogoutTime is Fail ResultCode: {0}, UserUID: {1}", result_code, UserUID);
                    }
				}
			}

			return result_code;
		}

		public DbResultCode UpdatePlatformProvidedUID( Int64 nUserUID , String PlatformProvideUID )
		{
			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.USER_UpdatePlatformProvideUID" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = nUserUID;

				SqlParameter paramPlatformProvideUID = sqlCmd.Parameters.Add( "@PlatformProvideUID" , SqlDbType.VarChar );
				paramPlatformProvideUID.Direction = ParameterDirection.Input;
				paramPlatformProvideUID.Value = PlatformProvideUID;

				try
				{
					// 실행직전 오픈
					dbConn.Open();

					Int32 execute_result_line = sqlCmd.ExecuteNonQuery();
					result_code = (DbResultCode)Convert.ToInt32( paramResult.Value );
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
				}
			}

			return result_code;
		}

		public DbResultCode GetPlatformProvidedUID( Int64 nUserUID , out DBPlatformProvideInfo kProvideInfo )
		{
			kProvideInfo = new DBPlatformProvideInfo( 0 , "" );

			DbResultCode result_code = DbResultCode.DB_ERROR;
			try
			{
				if ( Manager.IsOpen() == false )
				{
					Manager.Open();
				}
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
				result_code = DbResultCode.DB_ERROR;
				return result_code;
			}

			using ( SqlConnection dbConn = Manager.CreateConnection() as SqlConnection )
			{
				SqlCommand sqlCmd = new SqlCommand( "dbo.USER_GetPlatformProvideInfo" , dbConn );
				sqlCmd.CommandType = CommandType.StoredProcedure;

				SqlParameter paramResult = sqlCmd.Parameters.Add( "@Result" , SqlDbType.Int );
				paramResult.Direction = ParameterDirection.ReturnValue;
				paramResult.Value = (Int32)DbResultCode.DB_ERROR;

				SqlParameter paramUserUID = sqlCmd.Parameters.Add( "@UserUID" , SqlDbType.BigInt );
				paramUserUID.Direction = ParameterDirection.Input;
				paramUserUID.Value = nUserUID;

				SqlDataReader kDataReader = null;
				try
				{
					dbConn.Open();
					kDataReader = sqlCmd.ExecuteReader();
					if ( kDataReader.HasRows )
					{
						if ( kDataReader.Read() )
						{
							kProvideInfo = new DBPlatformProvideInfo(
								(Int64)kDataReader["UserUID"] ,
								(String)kDataReader["ProvideUID"] );
						}
					}
				}
				catch ( Exception ex )
				{
					Logger.ExceptionLog( ex );
					result_code = DbResultCode.DB_ERROR;
				}
			}

			return result_code;
		}

        public DbResultCode GetFriendLogoutTime(Int64 _lUserUID, DataTable _kFriendUIDLIst, out List<Int64> _kFriendLogoutTimeList)
        {
            _kFriendLogoutTimeList = new List<Int64>();

            DbResultCode result_code = DbResultCode.DB_ERROR;
            try
            {
                if(Manager.IsOpen() == false)
                {
                    Manager.Open();
                }
            }
            catch(System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                result_code = DbResultCode.DB_ERROR;
                return result_code;
            }

            using(SqlConnection dbConn = Manager.CreateConnection() as SqlConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("dbo.P_GetFriendLogoutTime", dbConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter paramResult = sqlCmd.Parameters.Add("@Result", SqlDbType.Int);
                paramResult.Direction = ParameterDirection.ReturnValue;
                paramResult.Value = (Int32)DbResultCode.DB_ERROR;

                SqlParameter kParamFriendUIDList = sqlCmd.Parameters.Add("@FriendUIDList", SqlDbType.Structured);
                kParamFriendUIDList.Direction = ParameterDirection.Input;
                kParamFriendUIDList.Value = _kFriendUIDLIst;

                SqlDataReader kDataReader = null;
                try
                {
                    dbConn.Open();
                    kDataReader = sqlCmd.ExecuteReader();
                    if(kDataReader.HasRows)
                    {
                        while(kDataReader.Read())
                            _kFriendLogoutTimeList.Add(((DateTime)kDataReader["LogoutTime"]).Ticks);
                    }
                }
                catch(Exception ex)
                {
                    Logger.ExceptionLog(ex);
                    result_code = DbResultCode.DB_ERROR;
                }
                finally
                {
                    kDataReader.Close();
                    result_code = (DbResultCode)Convert.ToInt32(paramResult.Value);
                    if(result_code != DbResultCode.SUCCESS)
                        Logger.ErrLog("P_GetFriendLogoutTime UserUID = {0}, Result_Code = {1}", _lUserUID, result_code);
                }
            }

            return result_code;
        }


	}
}
