﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;

namespace SCommonLib
{
    public class KMDBWorker
    {
        private Thread                                  m_thread;
        private KMDBScheduler                           m_scheduler;
        private BlockingCollection<ActExecuteRequest>   m_queue;
        
        public Int32 ThreadID
        {
            get { return m_thread.ManagedThreadId; }
        }

        public KMDBWorker(KMDBScheduler scheduler, Int32 capacity)
        {
            m_scheduler     =   scheduler;
            m_queue         =   new BlockingCollection<ActExecuteRequest>(capacity);
            m_thread        =   new Thread(this.OnWork);
        }

        public void Activate()
        {
            if (m_thread != null)
                m_thread.Start();
        }

        public void Deactivate()
        {
            m_queue.CompleteAdding();
            m_scheduler = null;
            m_thread    = null;
        }


        public void Enque(ActExecuteRequest request)
        {
            m_queue.TryAdd(request);
        }


        private void OnWork()
        {
#if  SERVERQA
            DBWorkerHistory history = new DBWorkerHistory(ThreadID);
            DBTracer.INSTANCE.Add(history);
#endif

            // IsCompleted == (IsAddingCompleted && Count == 0)
            while (m_queue.IsCompleted == false)
            {
                ActExecuteRequest request = null;
                try
                {
                    if (m_queue.TryTake(out request, System.Threading.Timeout.Infinite) == false)
                    {
                        if (m_queue.IsCompleted) // thread terminate
                            return;
                    }

                    DBRequest dbRq = request as DBRequest;

                    Int32 cmd = dbRq.OnExecute();

#if SERVERQA
                    Int64 elapsed = DateTime.Now.Ticks - dbRq.m_makeTime;
                    Interlocked.Increment(ref history.m_count);
                    DBTracer.INSTANCE.UpdateCmd((S2SPacketEnum)cmd, elapsed);
#endif
                    m_scheduler.ReturnToWorker(this, request);

                }
                catch (OperationCanceledException)
                {
                    Logger.InfoLog("Taking canceled. tid {0}", m_thread.ManagedThreadId);
                    break;
                }
                
            }
        }
    }
}
