﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace SCommonLib
{
    public class KMDBScheduler : IDisposable
    {
        private readonly int MaxRequest = 120000;
        private Object m_lockObj = new Object();

#region [Requester Delegate]
        private DelegateDBRequest m_dbReqCreator;
#endregion
        

        private ConcurrentQueue<ActExecuteRequest>      m_dbRequestQueue;
        private BlockingCollection<ActExecuteRequest>   m_jobTask;
        private BlockingCollection<KMDBWorker>          m_workerPool;
        private Thread                                  m_loop;

        private bool m_disposed = false; // set to true when disposing queue and no more tasks are pending
    

        
        public KMDBScheduler(DelegateDBRequest dbReqCreator, Int32 workerSize, Int32 workerPerThrougput)
        {
            m_dbReqCreator = dbReqCreator;
            m_dbRequestQueue = new ConcurrentQueue<ActExecuteRequest>();
            m_jobTask = new BlockingCollection<ActExecuteRequest>(MaxRequest);
            m_workerPool = new BlockingCollection<KMDBWorker>(workerSize);

            for (Int32 i = 0; i < workerSize; ++i)
            {
                var dbWorker = new KMDBWorker(this, workerPerThrougput);
                while (m_workerPool.TryAdd(dbWorker, System.Threading.Timeout.Infinite) == false)
                {
                    Thread.SpinWait(10);
                }

                dbWorker.Activate();
            }

            m_loop = new Thread(this.OnWork);
        }

        public void Activate()
        {

            if (m_loop != null)
                m_loop.Start();
        }

        internal void Deactivate()
        {
            Dispose();
            m_jobTask.CompleteAdding();
            m_workerPool.CompleteAdding();
            
        }

        protected KMDBWorker GetDBWorker()
        {

            KMDBWorker worker = null;
            m_workerPool.TryTake(out worker, System.Threading.Timeout.Infinite);

            return worker;
        }

        private void OnWork()
        {
            while (m_jobTask.IsCompleted == false)
            {

                ActExecuteRequest req = null;
                if (m_jobTask.TryTake(out req, System.Threading.Timeout.Infinite) == false)
                    continue;

                KMDBWorker worker = null;
                m_workerPool.TryTake(out worker, System.Threading.Timeout.Infinite);

                worker.Enque(req);
            }

            while (m_workerPool.IsCompleted == false)
            {
                KMDBWorker worker = null;
                m_workerPool.TryTake(out worker, System.Threading.Timeout.Infinite);

                worker = null;
            }

            m_jobTask = null;
            m_workerPool = null;
        }

        public void Enque(ActExecuteRequest req)
        {
            req.m_makeTime = DateTime.Now.Ticks;
            bool success = false;
            do
            {
                try
                {
                    success = m_jobTask.TryAdd(req, 1);
                }
                catch (OperationCanceledException)
                {
                    m_jobTask.CompleteAdding();
                }

                if (success == true)
                {
                    break;
                }
                else
                {
                    Thread.SpinWait(10);
                }

            } while (true);


        }
        
        public void Dispose()
        {
            bool waitForThreads = false;
            lock (m_lockObj)
            {
                if (m_disposed == false)
                {
                    GC.SuppressFinalize(this);

                    m_disposed = true;
                    waitForThreads = true;
                }
            }

            if (waitForThreads)
            {
                foreach (var worker in this.m_workerPool)
                {
                    worker.Deactivate();
                    Thread.Sleep(10);
                }
            }
        }
         

        internal void ReturnToWorker(KMDBWorker worker, ActExecuteRequest request)
        {
            m_workerPool.TryAdd(worker);
            m_dbRequestQueue.Enqueue(request);

        }

        internal DBRequest GetDBRequest()
        {
            ActExecuteRequest actReq = null;
            if (m_dbRequestQueue.Count != 0)
            {
                if (m_dbRequestQueue.TryDequeue(out actReq) == true)
                    return (DBRequest)actReq;
            }

            return m_dbReqCreator();
        }

        internal bool RecallRequest(ActExecuteRequest actReq)
        {

            m_dbRequestQueue.Enqueue(actReq);
            return true;
        }
    }
}
