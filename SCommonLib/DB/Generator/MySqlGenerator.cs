﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Data;

namespace KMServerCommon.DB
{
    public class MySqlGenerator : ISqlGenerator
    {
        private MySqlConnectionStringBuilder m_dbConnStringBuilder;
        private MySqlConnection m_mainConnection;

        public string ConnectionString
        {
            get { return m_dbConnStringBuilder.ConnectionString; }
        }

        public DbConnection MainConnection
        {
            get { return m_mainConnection; }
        }

        public DbConnection CreateConnection()
        {
            return new MySqlConnection( m_dbConnStringBuilder.ConnectionString);
        }

        public DbCommand CreateCommand(string spName, DbConnection dbConn)
        {
            return new MySqlCommand(spName, (MySqlConnection)dbConn);
        }

        public DbCommand CreateCommand(string spName)
        {
            return new MySqlCommand(spName);
        }

        public DbCommand CreateCommand(string spName, List<DbParameter> paramList)
        {
            MySqlCommand cmd = new MySqlCommand(spName);
            cmd.Parameters.AddRange(paramList.ToArray());
            return cmd;
        }


        public MySqlGenerator(string dns, Int16 port, string dbName, string uid, string pw)
        {
            m_dbConnStringBuilder = new MySqlConnectionStringBuilder();
            m_dbConnStringBuilder["Server"] = dns;
            m_dbConnStringBuilder["Port"] = port;
            m_dbConnStringBuilder["Database"] = dbName;
            m_dbConnStringBuilder["uid"] = uid;
            m_dbConnStringBuilder["pwd"] = pw;
        }

        public void Init()
        {
            m_mainConnection = new MySqlConnection(m_dbConnStringBuilder.ConnectionString);
            m_mainConnection.Open();
        }

        public void CleanUp()
        {
            if (m_mainConnection != null)
                m_mainConnection.Close();

            m_mainConnection = null;
        }

        

        public DbParameter InParam(List<DbParameter> paramList, String paramName, bool value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.Bit);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }
        public DbParameter InParam(List<DbParameter> paramList, String paramName, Byte value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.UByte);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, Int16 value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.Int16);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, Int32 value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.Int32);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, Int64 value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.Int64);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, DateTime value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.DateTime);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, String value, ParameterDirection direction)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.VarChar, value.Length);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }


        public DbParameter InParam(List<DbParameter> paramList, String paramName, Byte[] value, ParameterDirection direction = ParameterDirection.Input)
        {
            MySqlParameter param = new MySqlParameter(paramName, MySqlDbType.Binary, value.Length);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }
    }
}
