﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace KMServerCommon.DB
{
    public interface ISqlGenerator
    {
        void Init();
        void CleanUp();
        DbConnection CreateConnection();
        string ConnectionString
        {
            get;
        }

        DbConnection MainConnection
        {
            get;
        }

        DbCommand CreateCommand(string spName);
        DbCommand CreateCommand(string spName, List<DbParameter> paramList);
        DbCommand CreateCommand(string spName, DbConnection dbConn);

        #region [In Param]
        DbParameter InParam(List<DbParameter> paramList, String paramName, bool value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, Byte value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, Int16 value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, Int32 value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, Int64 value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, DateTime value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, String value, ParameterDirection direction);
        DbParameter InParam(List<DbParameter> paramList, String paramName, Byte[] value, ParameterDirection direction);
        #endregion
    }

    
}
