﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;

namespace KMServerCommon.DB
{
    public class MsSqlGenerator : ISqlGenerator
    {
        private SqlConnectionStringBuilder m_dbConnStringBuilder;
        private SqlConnection m_mainConnection;

        public string ConnectionString
        {
            get { return m_dbConnStringBuilder.ConnectionString; }
        }

        public DbConnection MainConnection
        {
            get { return m_mainConnection; }
        }

        public DbConnection CreateConnection()
        {
            return new SqlConnection( m_dbConnStringBuilder.ConnectionString);
        }

        public DbCommand CreateCommand(string spName, DbConnection dbConn)
        {
            return new SqlCommand(spName, (SqlConnection)dbConn);
        }

        public DbCommand CreateCommand(string spName)
        {
            return new SqlCommand(spName);
        }

        public DbCommand CreateCommand(string spName, List<DbParameter> paramList)
        {
            SqlCommand cmd = new SqlCommand(spName);
            cmd.Parameters.AddRange(paramList.ToArray());
            return cmd;
        }


        public MsSqlGenerator(string dns, Int32 port, string dbName, string uid, string pw)
        {
            m_dbConnStringBuilder = new SqlConnectionStringBuilder();
            m_dbConnStringBuilder["Server"] = string.Format("{0},{1}", dns, port);
            m_dbConnStringBuilder["Database"] = dbName;
            m_dbConnStringBuilder["uid"] = uid;
            m_dbConnStringBuilder["pwd"] = pw;
            m_dbConnStringBuilder.Enlist = false;

            

        }

        public void Init()
        {
            try
            {   
                m_mainConnection = new SqlConnection(m_dbConnStringBuilder.ConnectionString);
                m_mainConnection.Open();
            }
            catch (System.Exception ex)
            {   
                Logger.ErrLog("DB Open Error... connectionString:{0}", m_dbConnStringBuilder.ConnectionString);
                Logger.ExceptionLog(ex);
            }
        }

        public void CleanUp()
        {
            if (m_mainConnection != null)
                m_mainConnection.Close();

            m_mainConnection = null;
        }

        

        public DbParameter InParam(List<DbParameter> paramList, String paramName, bool value, ParameterDirection direction)
        {
            SqlParameter param  = new SqlParameter(paramName, SqlDbType.Bit);
            param.ParameterName = paramName;
            param.Direction     = direction;
            param.Value         = value;

            paramList.Add(param);
            return param;
        }
        public DbParameter InParam(List<DbParameter> paramList, String paramName, Byte value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.TinyInt);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, Int16 value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.SmallInt);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, Int32 value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.Int);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, Int64 value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.BigInt);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, DateTime value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.DateTime);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }

        public DbParameter InParam(List<DbParameter> paramList, String paramName, String value, ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.VarChar, value.Length);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }


        public DbParameter InParam(List<DbParameter> paramList, String paramName, Byte[] value, ParameterDirection direction = ParameterDirection.Input)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.Binary, value.Length);
            param.ParameterName = paramName;
            param.Direction = direction;
            param.Value = value;

            paramList.Add(param);
            return param;
        }
    }
}
