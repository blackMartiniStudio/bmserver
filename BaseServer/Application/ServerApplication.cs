﻿using System;
using System.Threading;
using BMDK;

namespace BMServerCommonLib
{
	public abstract class ServerApplication
	{
		private Thread m_kMainProcess;
		private Int64  m_ElapsedTimeTick;
		private Int64  m_LastUpdateTimeTick;

		private bool m_IsRun = false;

		protected bool IsRun
		{
			get { return m_IsRun; }
		}

		public string ProcessName { get; private set; }

		public ServerApplication(  )
		{
			m_ElapsedTimeTick = DateTime.Now.Ticks;
			m_LastUpdateTimeTick = DateTime.Now.Ticks;
		}

		protected abstract bool OnCreate();

		protected abstract bool OnUpdate( Int64 _elapsed );

		protected abstract void OnDestroy();

		public bool Create( string _processName )
		{
			ProcessName = _processName;

			BMLogger.InfoLog( "== {0} Create == ", ProcessName );

			m_kMainProcess = new Thread( new ThreadStart( MainUpdate ) );

			if( false == OnCreate() )
				return false;

			return true;
		}

		public bool Update( Int64 _elapsed )
		{
			if( false == OnUpdate( _elapsed ) )
				return false;

			Thread.Sleep( 1 );

			return true;
		}

		public void Destroy()
		{
			OnDestroy();
			Stop();
			BMLogger.End();
		}

		public bool Start()
		{
			try
			{
				BMLogger.InfoLog( "== {0} Start == ", ProcessName );

				AppDomain currentDomain = AppDomain.CurrentDomain;
				currentDomain.UnhandledException += new UnhandledExceptionEventHandler( OnUnhandleException );

				Run();			
			}
			catch( Exception ex )
			{
				// 로그
				BMLogger.ExceptionLog( ex );

				return false;
			}

			return true;
		}

		private void OnUnhandleException( object sender, UnhandledExceptionEventArgs args )
		{
			Exception e = (Exception)args.ExceptionObject;

			// 로그
			BMLogger.ExceptionLog( e );
		}

		protected virtual bool Run()
		{
			m_IsRun = true;
			m_kMainProcess.Start();
			return true;
		}

		public void Stop()
		{
			if( m_kMainProcess.IsAlive )
				m_kMainProcess.Join( 2000 );

			m_IsRun = false;
		}

		private void MainUpdate()
		{
			while( m_IsRun )
			{
				Int64 elapsed = UpdateElapsed();
				if( false == Update( elapsed ) )
				{
					Stop();
				}
			}
		}

		public Int64 UpdateElapsed()
		{
			Int64 nCurrentTimeTick = DateTime.Now.Ticks;
			m_ElapsedTimeTick = nCurrentTimeTick - m_LastUpdateTimeTick;
			m_LastUpdateTimeTick = nCurrentTimeTick;
			return m_ElapsedTimeTick;
		}
	}
}