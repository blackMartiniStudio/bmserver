﻿using System;
using System.Threading;
using BMDK;

namespace BMServerCommonLib
{
	public abstract class ConsoleApplication : IDisposable
	{
		private Int64  m_ElapsedTimeTick;
		private Int64  m_LastUpdateTimeTick;

		public string ProcessName { get; private set; }


		public ConsoleApplication()
		{
			m_ElapsedTimeTick = DateTime.Now.Ticks;
			m_LastUpdateTimeTick = DateTime.Now.Ticks;
		}

		protected abstract bool OnCreate();

		protected abstract bool OnUpdate( Int64 _elapsed );

		protected abstract void OnDestroy();

		public bool Create( string _processName )
		{
			ProcessName = _processName;

			BMLogger.InfoLog( "== {0} Create == ", ProcessName );

			if( false == OnCreate() )
				return false;

			return true;
		}

		public bool Update( Int64 _elapsed )
		{
			if( false == OnUpdate( _elapsed ) )
				return false;

			Thread.Sleep( 1 );

			return true;
		}

		public void Destroy()
		{
			OnDestroy();
		}

		public bool Start()
		{
			BMLogger.InfoLog( "== {0} Start == ", ProcessName );

			MainUpdate();
			return true;
		}

		private void MainUpdate()
		{
			while( true )
			{
				Int64 elapsed = UpdateElapsed();
				if( false == Update( elapsed ) )
				{
					break;
				}
			}
		}

		public Int64 UpdateElapsed()
		{
			Int64 nCurrentTimeTick = DateTime.Now.Ticks;
			m_ElapsedTimeTick = nCurrentTimeTick - m_LastUpdateTimeTick;
			m_LastUpdateTimeTick = nCurrentTimeTick;
			return m_ElapsedTimeTick;
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}