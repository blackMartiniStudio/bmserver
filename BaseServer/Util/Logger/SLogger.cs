﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using log4net;
using log4net.Config;

namespace BMServerCommonLib
{
	public class SLogger : IBMLogger
	{
		public ILog log;
		public ILogViewer mViewer = new EmptyLogViewer();

		public SLogger( String processName )
		{
			String log4netXmlLogFileName = processName + ".exe.config";
			try
			{
				System.IO.FileInfo file_info = new System.IO.FileInfo( log4netXmlLogFileName );
				XmlConfigurator.Configure( file_info );

#if DEBUG
				log = LogManager.GetLogger( "DebugLog" );
#else
				log = LogManager.GetLogger( "DebugLog" );
#endif
			}
			catch( System.Exception )
			{
				System.Diagnostics.Debug.Assert( false );
			}
		}

		public void SetLogWindow( ILogViewer viewWindow )
		{
			mViewer = viewWindow;
		}

		public void Start()
		{
			Debug( "======================= Log Start ======================= " );
		}


		public void End()
		{
			Debug( "=======================  Log End  ======================= " );
			LogManager.Shutdown();
		}

		public void Debug( String text )
		{
			if( false == log.IsDebugEnabled )
				return;

			log.Debug( text );
			mViewer.AddToLogView( text );
		}
		public void Info( String text )
		{
			if( false == log.IsInfoEnabled )
				return;

			log.Info( text );
			mViewer.AddToLogView( text );
		}

		public void Warning( String text )
		{
			if( false == log.IsWarnEnabled )
				return;

			StringBuilder format_builder = new StringBuilder();
			BMLogger.CallerInfoWrite( ref format_builder );
			format_builder.Append( text );

			log.Warn( format_builder.ToString() );

			mViewer.AddToLogView( text );
		}

		public void Error( String text )
		{
			if( false == log.IsErrorEnabled )
				return;

			StringBuilder format_builder = new StringBuilder();
			BMLogger.CallerInfoWrite( ref format_builder );
			format_builder.Append( text );

			log.Error( format_builder.ToString() );

			mViewer.AddToLogView( text );
		}

		public void Fatal( String text )
		{
			if( false == log.IsFatalEnabled )
				return;

			StringBuilder format_builder = new StringBuilder();
			BMLogger.FullStackWrite( format_builder );
			format_builder.Append( text );

			log.Fatal( format_builder.ToString() );

			mViewer.AddToLogView( text );
		}


		static private log4net.Appender.IAppender FindAppender( String appendName )
		{
			foreach( var obj in log4net.LogManager.GetRepository().GetAppenders() )
			{
				if( obj.Name == appendName )
				{
					return obj;
				}
			}
			return null;
		}
	}
}
