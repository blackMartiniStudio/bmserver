﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMServerCommonLib
{
public sealed partial class SPacketCommandCoder
{
	static public object Decode(int cmd, byte[] bodyBuffer)
	{
		switch (cmd)
		{
			case (int)SPacketCommandEnum.CMD_PacketS2SSendTest:
				return (object)SPacketCommandDecoder.DecodePacketS2SSendTest(bodyBuffer);
			case (int)SPacketCommandEnum.CMD_PacketS2SRecvTest:
				return (object)SPacketCommandDecoder.DecodePacketS2SRecvTest(bodyBuffer);
			case (int)SPacketCommandEnum.CMD_PacketS2SReqInitialize:
				return (object)SPacketCommandDecoder.DecodePacketS2SReqInitialize(bodyBuffer);
			case (int)SPacketCommandEnum.CMD_PacketS2SResInitialize:
				return (object)SPacketCommandDecoder.DecodePacketS2SResInitialize(bodyBuffer);
			case (int)SPacketCommandEnum.CMD_PacketM2GCreateRoom:
				return (object)SPacketCommandDecoder.DecodePacketM2GCreateRoom(bodyBuffer);
			case (int)SPacketCommandEnum.CMD_PacketG2MCreateRoom:
				return (object)SPacketCommandDecoder.DecodePacketG2MCreateRoom(bodyBuffer);
			default:
				break;
		}
		return null;
	}
}
}
