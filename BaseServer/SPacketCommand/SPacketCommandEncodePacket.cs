﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMServerCommonLib
{
public sealed partial class SPacketCommandCoder
{
	static public byte[] Encode(int cmd, object packetClass, out bool isCrypt)
	{
		switch (cmd)
		{
			case (int)SPacketCommandEnum.CMD_PacketS2SSendTest:
				return SPacketCommandEncoder.EncodePacketS2SSendTest((PacketS2SSendTest)packetClass, out isCrypt);
			case (int)SPacketCommandEnum.CMD_PacketS2SRecvTest:
				return SPacketCommandEncoder.EncodePacketS2SRecvTest((PacketS2SRecvTest)packetClass, out isCrypt);
			case (int)SPacketCommandEnum.CMD_PacketS2SReqInitialize:
				return SPacketCommandEncoder.EncodePacketS2SReqInitialize((PacketS2SReqInitialize)packetClass, out isCrypt);
			case (int)SPacketCommandEnum.CMD_PacketS2SResInitialize:
				return SPacketCommandEncoder.EncodePacketS2SResInitialize((PacketS2SResInitialize)packetClass, out isCrypt);
			case (int)SPacketCommandEnum.CMD_PacketM2GCreateRoom:
				return SPacketCommandEncoder.EncodePacketM2GCreateRoom((PacketM2GCreateRoom)packetClass, out isCrypt);
			case (int)SPacketCommandEnum.CMD_PacketG2MCreateRoom:
				return SPacketCommandEncoder.EncodePacketG2MCreateRoom((PacketG2MCreateRoom)packetClass, out isCrypt);
			default:
				break;
		}
		isCrypt = false;
		return null;
	}
}
}
