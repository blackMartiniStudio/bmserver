﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMServerCommonLib
{
public enum SPacketCommandEnum
{
	CMD_NONE,
	CMD_PacketS2SSendTest = 5001,
	CMD_PacketS2SRecvTest,
	CMD_PacketS2SReqInitialize,
	CMD_PacketS2SResInitialize,
	CMD_PacketM2GCreateRoom = 10001,
	CMD_PacketG2MCreateRoom,
	CMD_MAX
}
}
