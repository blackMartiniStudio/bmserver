﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BMServerCommonLib
{
	public sealed partial class SPacketCommandEncoder
	{

		static public byte[] EncodePacketS2SSendTest(PacketS2SSendTest pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketS2SRecvTest(PacketS2SRecvTest pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketS2SReqInitialize(PacketS2SReqInitialize pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketS2SResInitialize(PacketS2SResInitialize pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketM2GCreateRoom(PacketM2GCreateRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketG2MCreateRoom(PacketG2MCreateRoom pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}
	}
}
