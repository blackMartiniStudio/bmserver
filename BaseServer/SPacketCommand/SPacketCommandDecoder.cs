﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BMServerCommonLib
{
	public sealed partial class SPacketCommandDecoder
	{

		static public PacketS2SSendTest DecodePacketS2SSendTest(byte[] bodyBuffer)
		{
			PacketS2SSendTest retValue = new PacketS2SSendTest();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketS2SRecvTest DecodePacketS2SRecvTest(byte[] bodyBuffer)
		{
			PacketS2SRecvTest retValue = new PacketS2SRecvTest();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketS2SReqInitialize DecodePacketS2SReqInitialize(byte[] bodyBuffer)
		{
			PacketS2SReqInitialize retValue = new PacketS2SReqInitialize();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketS2SResInitialize DecodePacketS2SResInitialize(byte[] bodyBuffer)
		{
			PacketS2SResInitialize retValue = new PacketS2SResInitialize();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketM2GCreateRoom DecodePacketM2GCreateRoom(byte[] bodyBuffer)
		{
			PacketM2GCreateRoom retValue = new PacketM2GCreateRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketG2MCreateRoom DecodePacketG2MCreateRoom(byte[] bodyBuffer)
		{
			PacketG2MCreateRoom retValue = new PacketG2MCreateRoom();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}
	}
}
