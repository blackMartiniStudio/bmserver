﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using CSCommonLib;

namespace BMServerCommonLib
{

public class PacketS2SSendTest
{
	public string ServerType;
	public Int32 SendCount;
	public PacketS2SSendTest() {}
	public PacketS2SSendTest(string p_ServerType, Int32 p_SendCount)
	{
		this.ServerType = p_ServerType;
		this.SendCount = p_SendCount;
	}
	public PacketS2SSendTest(PacketS2SSendTest p_PacketS2SSendTest)
	{
		this.ServerType = p_PacketS2SSendTest.ServerType;
		this.SendCount = p_PacketS2SSendTest.SendCount;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (ServerType == null)
			ServerType = "";
		byte[] sServerType = Encoding.UTF8.GetBytes(ServerType);
		output.Write(sServerType.Length);
		output.Write(sServerType);
		output.Write(SendCount);
	}

	public void StreamDecoding(BinaryReader src)
	{
		int lServerType = src.ReadInt32();
		byte[] sServerType = new byte[lServerType];
		src.Read(sServerType, 0, lServerType);
		ServerType = Encoding.UTF8.GetString(sServerType);
		SendCount = src.ReadInt32();
	}
}

public class PacketS2SRecvTest
{
	public Int32 Result;
	public string ServerType;
	public Int32 RecvCount;
	public PacketS2SRecvTest() {}
	public PacketS2SRecvTest(Int32 p_Result, string p_ServerType, Int32 p_RecvCount)
	{
		this.Result = p_Result;
		this.ServerType = p_ServerType;
		this.RecvCount = p_RecvCount;
	}
	public PacketS2SRecvTest(PacketS2SRecvTest p_PacketS2SRecvTest)
	{
		this.Result = p_PacketS2SRecvTest.Result;
		this.ServerType = p_PacketS2SRecvTest.ServerType;
		this.RecvCount = p_PacketS2SRecvTest.RecvCount;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		if (ServerType == null)
			ServerType = "";
		byte[] sServerType = Encoding.UTF8.GetBytes(ServerType);
		output.Write(sServerType.Length);
		output.Write(sServerType);
		output.Write(RecvCount);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		int lServerType = src.ReadInt32();
		byte[] sServerType = new byte[lServerType];
		src.Read(sServerType, 0, lServerType);
		ServerType = Encoding.UTF8.GetString(sServerType);
		RecvCount = src.ReadInt32();
	}
}

public class PacketS2SReqInitialize
{
	public string ServerType;
	public PacketS2SReqInitialize() {}
	public PacketS2SReqInitialize(string p_ServerType)
	{
		this.ServerType = p_ServerType;
	}
	public PacketS2SReqInitialize(PacketS2SReqInitialize p_PacketS2SReqInitialize)
	{
		this.ServerType = p_PacketS2SReqInitialize.ServerType;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (ServerType == null)
			ServerType = "";
		byte[] sServerType = Encoding.UTF8.GetBytes(ServerType);
		output.Write(sServerType.Length);
		output.Write(sServerType);
	}

	public void StreamDecoding(BinaryReader src)
	{
		int lServerType = src.ReadInt32();
		byte[] sServerType = new byte[lServerType];
		src.Read(sServerType, 0, lServerType);
		ServerType = Encoding.UTF8.GetString(sServerType);
	}
}

public class PacketS2SResInitialize
{
	public Int32 Result;
	public PacketS2SResInitialize() {}
	public PacketS2SResInitialize(Int32 p_Result)
	{
		this.Result = p_Result;
	}
	public PacketS2SResInitialize(PacketS2SResInitialize p_PacketS2SResInitialize)
	{
		this.Result = p_PacketS2SResInitialize.Result;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
	}
}

public class PacketM2GCreateRoom
{
	public TDCreateRoomInfo CreateRoomInfo;
	public PacketM2GCreateRoom() {}
	public PacketM2GCreateRoom(TDCreateRoomInfo p_CreateRoomInfo)
	{
		this.CreateRoomInfo = new TDCreateRoomInfo(p_CreateRoomInfo);
	}
	public PacketM2GCreateRoom(PacketM2GCreateRoom p_PacketM2GCreateRoom)
	{
		this.CreateRoomInfo = new TDCreateRoomInfo(p_PacketM2GCreateRoom.CreateRoomInfo);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (CreateRoomInfo == null)
			CreateRoomInfo = new TDCreateRoomInfo();
		CreateRoomInfo.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		CreateRoomInfo = new TDCreateRoomInfo();
		CreateRoomInfo.StreamDecoding(src);
	}
}

public class PacketG2MCreateRoom
{
	public Int32 Result;
	public TDRoomInfo RoomInfo;
	public PacketG2MCreateRoom() {}
	public PacketG2MCreateRoom(Int32 p_Result, TDRoomInfo p_RoomInfo)
	{
		this.Result = p_Result;
		this.RoomInfo = new TDRoomInfo(p_RoomInfo);
	}
	public PacketG2MCreateRoom(PacketG2MCreateRoom p_PacketG2MCreateRoom)
	{
		this.Result = p_PacketG2MCreateRoom.Result;
		this.RoomInfo = new TDRoomInfo(p_PacketG2MCreateRoom.RoomInfo);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(Result);
		if (RoomInfo == null)
			RoomInfo = new TDRoomInfo();
		RoomInfo.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		Result = src.ReadInt32();
		RoomInfo = new TDRoomInfo();
		RoomInfo.StreamDecoding(src);
	}
}
}
