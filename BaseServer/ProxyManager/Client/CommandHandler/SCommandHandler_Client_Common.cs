﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;

namespace BMServerCommonLib
{
	public class SCommandHandler_Client_Common : SCommandHandler_Client
	{
		public SCommandHandler_Client_Common( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetConnect, OnNetConnect );

			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetRelayConnect, OnNetRelayConnect );
			SetCmdHandler( (Int32)SPacketCommandEnum.CMD_PacketS2SRecvTest, OnS2SRecvTest );
			SetCmdHandler( (Int32)SPacketCommandEnum.CMD_PacketS2SResInitialize, OnS2SResInitialize );
		}

		private BMCommandResult OnNetConnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			BMLogger.InfoLog( " OnNetConnect " );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnNetRelayConnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			NetClientProxy kClient = ToNetClient( _kHandler );

			PacketNetRelayConnect kPkt = _kCommand.Packet as PacketNetRelayConnect;
			kClient.OnReplyConnect( kPkt.tdNetRelayConnect.uidHost, kPkt.tdNetRelayConnect.uidAlloc, kPkt.tdNetRelayConnect.timeTick );
			kClient.ReqInitializeToServer();

			BMLogger.InfoLog( " OnNetRelayConnect : uidHost = {0}, uidAlloc = {1} ", kPkt.tdNetRelayConnect.uidHost, kPkt.tdNetRelayConnect.uidAlloc );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnS2SResInitialize( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			NetClientProxy kClient = ToNetClient( _kHandler );

			PacketS2SResInitialize kPkt = _kCommand.Packet as PacketS2SResInitialize;
			kClient.OnResInitializeToServer( kPkt );

			return BMCommandResult.SUCCESS;
		}


		private BMCommandResult OnS2SRecvTest( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			NetClientProxy kClient = ToNetClient( _kHandler );

			PacketS2SRecvTest kCmd = _kCommand.Packet as PacketS2SRecvTest;

			BMLogger.InfoLog( "[{0}:{1}] S2S Recv Test CommandID[{2}], Server[{3}], SenderUID[{4}], RecvCount = {4}",
				kClient.Desc.ClientName, kClient.MyUID, _kCommand.ID, kCmd.ServerType, _kCommand.SendererUID, kCmd.RecvCount );

			return BMCommandResult.SUCCESS;
		}
	}
}
