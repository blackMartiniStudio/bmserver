﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;

namespace BMServerCommonLib
{
	public class SCommandHandler_Client : BMCommandHandler
	{
		public SCommandHandler_Client( BMCommandCommunicator _CommandCommunicator ) 
			: base( _CommandCommunicator )
		{
		}

		protected virtual NetClientProxy ToNetClient( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as NetClientProxy;
		}
	}
}
