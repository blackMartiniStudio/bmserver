﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMDK;
using BMNet;

namespace BMServerCommonLib
{
	public class NetClientProxy : BMNetClient
	{
		public bool OnInitialize { get; protected set; }
		private SCommandHandler_Client_Common CommonHandler;

		public NetClientProxy( BMNetClientDesc _kDesc ) : base( _kDesc )
		{
			m_bASyncSending = true;
			OnInitialize = false;

			CommonHandler = new SCommandHandler_Client_Common( this );
		}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new SCommandBuilder();
		}

		public bool IsConnected()
		{
			return m_kSocket.IsActive;
		}


		public override void Disconnect( string reasen = "" )
		{
			base.Disconnect( reasen );

			OnInitialize = false;
		}

		public void ReqInitializeToServer()
		{
			BMCommand kNewCommand = NewCommand( (Int32)SPacketCommandEnum.CMD_PacketS2SReqInitialize );
			PacketS2SReqInitialize kSendCmd = new PacketS2SReqInitialize( Desc.ClientName );

			kNewCommand.SetPacket( kSendCmd );

			Post( kNewCommand );
		}

		public virtual void OnResInitializeToServer( PacketS2SResInitialize kPkt )
		{
			ResultTable onResult = (ResultTable)kPkt.Result;
			BMLogger.InfoLog( " OnS2SResInitialize {0}", ( (ResultTable)kPkt.Result ).ToString() );

			if( onResult != ResultTable.SUCCESS )
				return;

			OnInitialize = true;
		}
	}
}
