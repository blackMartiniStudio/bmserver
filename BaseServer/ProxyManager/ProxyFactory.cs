﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;

namespace BMServerCommonLib
{
	public interface IProxyServer
	{
	}

	public class EmptyServerProxy : IProxyServer
	{
		public EmptyServerProxy()
		{

		}
	}

	public class GameServerProxy : IProxyServer
	{
		public GameServerProxy()
		{
			
		}
	}

	public interface IProxyFactory
	{
		NetServerProxyManager CreateProxyServer( BMNetServerDesc kDesc, string ownerServerName);

		BMNetClient CreateProxyClient( BMNetClientDesc kDesc );
		
	}
}
