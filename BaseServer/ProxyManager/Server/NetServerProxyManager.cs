﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;

namespace BMServerCommonLib
{
	public abstract class NetServerProxyManager : BMNetServer
	{
		public string OwnerServerName { get; protected set; }
		private ICommandGroup kCommandGroup;

		public NetServerProxyManager( BMNetServerDesc _kDesc, string ownerServerName ) : base( _kDesc )
		{
			OwnerServerName = ownerServerName;
			kCommandGroup = new SCommandHandler_Server( this );
	}

		protected override void CreateCommandBuilder()
		{
			m_kCommandBuilder = new SCommandBuilder();
		}

		public abstract IProxyServer CreateProxyServer( UInt64 uid );
	}
}
