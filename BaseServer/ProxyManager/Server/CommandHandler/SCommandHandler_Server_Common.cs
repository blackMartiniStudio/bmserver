﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMDK;
using BMNet;

namespace BMServerCommonLib
{
	public class CommandHandler_Server_Common : BMCommandHandler 
	{
		private static Int32 nRecvCount = 0;

		public CommandHandler_Server_Common( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)SPacketCommandEnum.CMD_PacketS2SSendTest, OnS2SSendTest );
			SetCmdHandler( (Int32)SPacketCommandEnum.CMD_PacketS2SReqInitialize, OnS2SReqInitialize );
		}

	
		private BMCommandResult OnS2SSendTest( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			NetServerProxyManager kServer = SCommandHandler_Server.ToNetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			PacketS2SSendTest kCmd = _kCommand.Packet as PacketS2SSendTest;

			BMCommand kNewCommand = kServer.NewCommand( (Int32)SPacketCommandEnum.CMD_PacketS2SRecvTest, _kCommand.SendererUID );
			PacketS2SRecvTest kResCmd = new PacketS2SRecvTest( (int)BMCommandResult.SUCCESS, kServer.OwnerServerName, ++nRecvCount );

			BMLogger.InfoLog( "[{3}:{4}] CommandID = {0}, SendCount = {1}, RecvCount = {2}", _kCommand.ID, kCmd.SendCount, nRecvCount, kServer.Desc.ServerName, _kCommand.SendererUID );

			kNewCommand.SetPacket( kResCmd );

			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnS2SReqInitialize( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			NetServerProxyManager kServer = SCommandHandler_Server.ToNetServer( _kHandler );
			if( kServer == null )
				return BMCommandResult.FAIL;

			PacketS2SReqInitialize kCmd = _kCommand.Packet as PacketS2SReqInitialize;
			BMCommand kNewCommand = kServer.NewCommand( (Int32)SPacketCommandEnum.CMD_PacketS2SResInitialize, _kCommand.SendererUID );

			PacketS2SResInitialize kResCmd;
			if( kServer.Desc.ServerName == kCmd.ServerType )
			{
				kResCmd  = new PacketS2SResInitialize( (int)BMCommandResult.SUCCESS );
				IProxyServer serverProxy = kServer.CreateProxyServer( _kCommand.SendererUID );
				if( serverProxy == null )
				{
					BMLogger.ErrLog( "OnS2SReqInitialize CreateProxyServer Error!" );
				}
			}
			else
			{
				kResCmd = new PacketS2SResInitialize( (int)BMCommandResult.FAIL );
			}

			BMLogger.InfoLog( " ServerProxy Initialize - ServerType[{0}], CreateProxyServerUID[{1}]", kServer.Desc.ServerName, _kCommand.SendererUID );

			kNewCommand.SetPacket( kResCmd );

			kServer.Post( kNewCommand );
			return BMCommandResult.SUCCESS;
		}
	}
}
