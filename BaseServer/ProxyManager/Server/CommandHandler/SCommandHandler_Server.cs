﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMNet;

namespace BMServerCommonLib
{
	public class SCommandHandler_Server : ICommandGroup
	{
		public SCommandHandler_Server( BMCommandCommunicator _kCommunicator )
		{
			CommandHandler_Server_Common kCommon = new CommandHandler_Server_Common( _kCommunicator );
		}

		static public NetServerProxyManager ToNetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as NetServerProxyManager;
		}
	}
}
