﻿using System;
using System.Threading;

namespace BMServerCommonLib
{
	public abstract class GBaseServer
	{
		private Thread m_kMainProcess;
		private Int64  m_ElapsedTimeTick;
		private Int64  m_LastUpdateTimeTick;

		private bool m_IsRun = false;

		protected bool IsRun
		{
			get { return m_IsRun; }
		}

		public string ProcessName { get; private set; }

		protected abstract bool OnCreate();

		protected abstract bool OnUpdate( Int64 _elapsed );

		protected abstract void OnDestroy();

		public bool Create( string processName )
		{
			ProcessName = processName;

			if( false == OnCreate() )
				return false;

			return true;
		}

		public bool Update( Int64 _elapsed )
		{
			if( false == OnUpdate( _elapsed ) )
				return false;

			return true;
		}

		public void Destroy()
		{
			OnDestroy();
		}
	}
}