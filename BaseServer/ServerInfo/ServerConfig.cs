﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BMServerCommonLib
{
	public abstract class ServerConfig
	{
		public abstract bool Load( string filePath );
	}
}
