﻿using System;

namespace BMNet
{
	public class SendPacketNode
	{
		private Int32 cmdId;
		private Byte[] rowData;

		public SendPacketNode( Int32 _cmdId, Byte[] _rowData )
		{
			cmdId = _cmdId;
			rowData = _rowData;
		}

		public Byte[] Data
		{
			get { return rowData; }
		}

		public Int32 Offset
		{
			get;
			set;
		}

		public Int32 RemainData
		{
			get { return rowData.Length - Offset; }
		}
	}
}