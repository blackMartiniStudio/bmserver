﻿using System;

namespace BMNet
{
	public class SendBuffer
	{
		private static readonly Int32 DEFAULT_BUFFER_SIZE = (Int32)NetCommon.MAX_PACKET_SIZE;
		public Byte[]   m_Buf;              // 버퍼
		public Int32    m_DataSize;         // 버퍼에 존재하는 데이터 크기
		public Int32    m_BufOffset;        // 읽어들인 버퍼의 위치

		public bool     m_bLock;

		public SendBuffer()
		{
			m_Buf = new Byte[DEFAULT_BUFFER_SIZE];
		}

		public SendBuffer( Int32 nSize )
		{
			m_Buf = new Byte[nSize];
		}

		public Byte[] Buff
		{
			get { return m_Buf; }
		}

		public Int32 Size
		{
			get { return m_Buf.Length; }
		}

		public Int32 DataSize
		{
			get { return m_DataSize; }
		}

		public Int32 RemainSize
		{
			get { return m_DataSize - m_BufOffset; }
		}

		public Int32 SpareSize
		{
			get { return m_Buf.Length - m_DataSize; }
		}

		public bool Lock
		{
			get { return m_bLock; }
		}

		public void Reset()
		{
			Array.Clear( m_Buf, 0, m_Buf.Length );
			m_DataSize = 0;
			m_BufOffset = 0;
			m_bLock = false;
		}

		public bool Push( Byte[] addData )
		{
			Int32 nAddSize = addData.Length;

			if( SpareSize < nAddSize )
				return false;

			Buffer.BlockCopy( addData, 0, m_Buf, m_DataSize, nAddSize );
			m_DataSize += nAddSize;

			return true;
		}

		public void Pop( Int32 nSize )
		{
			m_BufOffset += nSize;
		}

		public void Locking()
		{
			m_bLock = true;
		}
	}
}