﻿//using System.Collections.Concurrent;
using BMDK;
using System;

namespace BMNet
{
	using DelayCommandList = BMConcurrentQueue<DelayCommand>;

	public class DelayCommand
	{
		public Int64        Tick;
		public BMCommand    kCommand;
	}

	public class BMDelayTester
	{
		private Int64 m_nDelayTime;
		private DelayCommandList m_liCommand = new DelayCommandList();

		public Int64 DelayTime
		{
			get { return m_nDelayTime; }
		}

		public DelayCommandList CommandList
		{
			get { return m_liCommand; }
		}

		public void SetDelay( Int64 nDelayTime )
		{
			m_nDelayTime = nDelayTime;
		}

		public void Post( BMCommand kCommand, Int64 nNowTime )
		{
			DelayCommand kDelayCmd = new DelayCommand();
			kDelayCmd.Tick = nNowTime;
			kDelayCmd.kCommand = kCommand;

			m_liCommand.Enqueue( kDelayCmd );
		}

		public void OnRun( BMCommandCommunicator cm, Int64 nNowTime )
		{
			while( true )
			{
				DelayCommand kCmd;
				if( false == m_liCommand.TryDequeue( out kCmd ) )
					break;

				if( nNowTime - kCmd.Tick >= m_nDelayTime )
				{
					cm.SendCommand( kCmd.kCommand );
				}
			}

			return;
		}
	}
}