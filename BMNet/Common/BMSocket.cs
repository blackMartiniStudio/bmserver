﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using BMDK;

namespace BMNet
{
	public class BMSocket
	{
		protected Socket        m_kSocket;
		protected bool          m_bNoDelay;
		protected bool          m_bReuse;

		public Socket Sock
		{
			get { return m_kSocket; }
		}

		public bool IsInvalid
		{
			get { return m_kSocket != null; }
		}

		public bool NoDelay
		{
			get { return m_bNoDelay; }
		}

		public bool Reuse
		{
			get { return m_bReuse; }
		}

		public virtual bool CreateSocket( AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType, bool bNoDelay, bool bReuse )
		{
			try
			{
				m_bNoDelay = bNoDelay;
				m_bReuse = bReuse;

				m_kSocket = new Socket( addressFamily, socketType, protocolType );
				m_kSocket.Blocking = false;
				m_kSocket.LingerState = new LingerOption( true, 10 );
				m_kSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.NoDelay, bNoDelay );
				m_kSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, bReuse );
			}
			catch( Exception ex )
			{
				// 로그
				BMLogger.ExceptionLog( ex );
				return false;
			}

			return true;
		}

		public virtual void CloseSocket( String where )
		{
			// 로그
			BMLogger.DebugLog( "Close Socket - {0}", where );

			m_kSocket.Close();
		}

		protected String ConvertIP( String remoteAddr )
		{
			return "";
		}

		public virtual bool IsSocketOpened()
		{
			return m_kSocket.Connected;
		}
	}
}