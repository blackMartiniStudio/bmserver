﻿using System;

namespace BMNet
{
	public class BMKeepingAliveData
	{
		private Int64 m_LastRecvTick;

		public bool CheckAlive( Int64 nNowTick, Int64 nAllowedTick )
		{
			if( m_LastRecvTick > nNowTick )
				return true;

			if( m_LastRecvTick == 0 )
				return true;

			if( nNowTick - m_LastRecvTick < nAllowedTick )
				return true;

			return false;
		}

		public void SetRecvTime( Int64 nTick )
		{
			m_LastRecvTick = nTick;
		}

		public Int64 GetRecvTime()
		{
			return m_LastRecvTick;
		}
	}
}