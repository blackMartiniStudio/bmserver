﻿using System;

namespace BMNet
{
	public class BMNetClientDesc
	{
		public string ClientName { get; set; }
		public string ServerName { get; set; }
		public string ServerIp { get; set; }
		public int ServerPort { get; set; }

		public bool KeepAlive { get; set; }
		public Int64 KeepAliveTick { get; set; }

		public BMNetClientDesc()
		{
			ClientName = string.Empty;
			ServerName = string.Empty;
			ServerIp = string.Empty;
			ServerPort = 0;

			KeepAlive = false;
			KeepAliveTick = TimeSpan.TicksPerSecond * 60;
		}

	}
}