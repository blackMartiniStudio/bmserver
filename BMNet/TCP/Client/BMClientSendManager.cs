﻿using System;
using System.Collections.Generic;

namespace BMNet
{
	using liSendBuffer = LinkedList<SendBuffer>;

	public class BMClientSendManager
	{
		private BMClientTcpSocket m_kClientSocket;
		private SendBufferObject m_SendBufferObj = new SendBufferObject();
		private liSendBuffer m_liSendBuffer = new liSendBuffer();

		private Object m_SendBufferLock = new object();

		public BMClientSendManager( BMClientTcpSocket _kClientSocket )
		{
			m_kClientSocket = _kClientSocket;
		}

		internal void LazySend( Int32 nID, byte[] header, byte[] body )
		{
			Int32 nPacketSize = header.Length + body.Length;

			SendBuffer kBuffer = GetBuffer( nPacketSize );

			kBuffer.Push( header );
			kBuffer.Push( body );
		}

		private SendBuffer GetBuffer( int nPacketSize )
		{
			SendBuffer kBuff;

			lock ( m_SendBufferLock )
			{
				if( null == m_liSendBuffer.Last )
				{
					m_liSendBuffer.AddLast( m_SendBufferObj.Get() );
				}

				kBuff = m_liSendBuffer.Last.Value;

				if( kBuff.Lock == true || kBuff.SpareSize < nPacketSize )
				{
					kBuff = m_SendBufferObj.Get();
					m_liSendBuffer.AddLast( kBuff );
				}
			}

			return kBuff;
		}

		internal void AllSend()
		{
			Send();
		}

		private void Send()
		{
			lock ( m_SendBufferLock )
			{
				if( m_liSendBuffer.Count == 0 )
					return;

				LockSendBuffer( m_liSendBuffer );

				if( false == m_kClientSocket.Send( m_liSendBuffer ) )
				{
					foreach( SendBuffer kBuffer in m_liSendBuffer )
					{
						RestoreSendBuffer( kBuffer );
					}
				}

				m_liSendBuffer.Clear();
			}
		}

		private void LockSendBuffer( liSendBuffer _liBuff )
		{
			foreach( SendBuffer kBuffer in _liBuff )
			{
				kBuffer.Locking();
			}
		}

		internal void RestoreSendBuffer( SendBuffer _kSendBuffer )
		{
			m_SendBufferObj.Restore( _kSendBuffer );
		}

		internal void Clear()
		{
			Send();
		}
	}
}