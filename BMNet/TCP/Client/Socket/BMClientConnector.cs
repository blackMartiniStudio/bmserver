﻿using BMDK;
using System;
using System.Net;
using System.Net.Sockets;

namespace BMNet
{
	public interface IClientConnectListener
	{
		void OnConnect( BMClientTcpSocket socket );
	};

	public class TryConnectInfo
	{
		public String addr;
		public Int32  port;
		public BMClientTcpSocket socket;
		public Int32  tryCount;

		public string ToDebugString()
		{
			return string.Format( "{0}:{1} retry[{2}] ", addr, port, tryCount );
		}
	}

	public class BMClientConnector
	{
		private IClientConnectListener m_kListner;
		private Int32 MaxRetryConnect = -1;

		public void SetListener( IClientConnectListener _kListner )
		{
			m_kListner = _kListner;
		}

		public bool Connect( BMClientTcpSocket _kSocket )
		{
			String localAddr = NetworkHelper.RESERVED_IPv4_LOOPBACK;
			if( _kSocket.Host.ToUpper() == "LOCALHOST" )
			{
				_kSocket.SetHost( localAddr );
			}

			TryConnectInfo kTryConnetInfo = new TryConnectInfo();
			kTryConnetInfo.addr = _kSocket.Host;
			kTryConnetInfo.port = _kSocket.Port;
			kTryConnetInfo.socket = _kSocket;
			kTryConnetInfo.tryCount = 0;

			Connect( kTryConnetInfo );

			return true;
		}

		protected virtual void Connect( TryConnectInfo connInfo )
		{
			SocketAsyncEventArgs kEventArgs;
			if( false == NewSocketAsyncEventArgs( connInfo, out kEventArgs ) )
				return;

			BMClientTcpSocket socket = connInfo.socket;
			bool willRaiseEvent = socket.Sock.ConnectAsync( kEventArgs );
			if( false == willRaiseEvent )
			{
				BMLogger.ErrLog( "Connect willRaiseEvent false ConnectInfo : {0}", connInfo.ToDebugString() );
				ProcessConnect( kEventArgs );
			}
		}

		private bool NewSocketAsyncEventArgs( TryConnectInfo kTryConnetInfo, out SocketAsyncEventArgs kEventArgs )
		{
			IPEndPoint endPoint = new IPEndPoint( IPAddress.Parse( kTryConnetInfo.addr ), kTryConnetInfo.port );

			kEventArgs = new SocketAsyncEventArgs();
			kEventArgs.RemoteEndPoint = endPoint;
			kEventArgs.DisconnectReuseSocket = true;
			kEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>( ConnectEventArgCompleted );

			kEventArgs.UserToken = kTryConnetInfo;

			return true;
		}

		private bool IsRetryAbleSocketError( SocketAsyncEventArgs kEventArgs )
		{
			return kEventArgs.SocketError == SocketError.TimedOut ||
					kEventArgs.SocketError == SocketError.ConnectionRefused ||
					kEventArgs.SocketError == SocketError.HostDown;
		}

		private void ProcessConnect( SocketAsyncEventArgs kEventArgs )
		{
			TryConnectInfo kInfo = kEventArgs.UserToken as TryConnectInfo;

			if( kEventArgs.SocketError == SocketError.Success )
			{
				BMClientTcpSocket kSocket = kInfo.socket;
				OnConnect( kSocket );
				return;
			}

			if( IsRetryAbleSocketError( kEventArgs ) )
			{
				if( MaxRetryConnect == -1 || kInfo.tryCount < MaxRetryConnect )
				{
					BMLogger.DebugLog( "ProcessConnect ReTryConnect Error. ConnectInfo : {0}", kInfo.ToDebugString() );

					kInfo.tryCount++;
					ReTryConnect( kInfo );
					return;
				}

				ConnectError( kInfo.socket );

				return;
			}

		
		}

		private void ReTryConnect( TryConnectInfo connInfo )
		{
			Connect( connInfo );
		}

		protected void ConnectError( BMClientTcpSocket kSocket )
		{
			kSocket.Fini( FileFunction.__Function() );
		}

		private void ConnectEventArgCompleted( object sender, SocketAsyncEventArgs kEventArgs )
		{
			ProcessConnect( kEventArgs );
		}

		protected void OnConnect( BMClientTcpSocket kSocket )
		{
			kSocket.Recv();

			if( m_kListner != null )
				m_kListner.OnConnect( kSocket );
		}
	}
}