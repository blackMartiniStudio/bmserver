﻿using System;
using System.Net.Sockets;

namespace BMNet
{
	public interface IClientSendListener
	{
		void OnSend( SendBuffer _kSendBuffer );

		void OnHardDisconnect( String reason );
	};

	public class ClientSendAsyncStateObject
	{
		public BMClientTcpSocket    m_kSocket;
		public SendBuffer   m_kBuffer;

		public ClientSendAsyncStateObject( BMClientTcpSocket kSocket, SendBuffer kBuffer )
		{
			m_kSocket = kSocket;
			m_kBuffer = kBuffer;
		}
	}

	public class BMClientSender
	{
		private IClientSendListener m_kListener;

		public bool Send( BMClientTcpSocket kSocket )
		{
			SendBuffer kBuffer;
			if( false == kSocket.DequeueSendBuffer( out kBuffer ) )
				return true;

			ClientSendAsyncStateObject kStateObj = new ClientSendAsyncStateObject( kSocket, kBuffer );
			return BeginSend( kSocket, kBuffer );
		}

		private bool BeginSend( BMClientTcpSocket kSocket, SendBuffer kBuffer )
		{
			ClientSendAsyncStateObject kStateObj = new ClientSendAsyncStateObject( kSocket, kBuffer );

			SocketError eSocketError = SocketError.Success;
			kSocket.Sock.BeginSend( kBuffer.Buff, kBuffer.m_BufOffset, kBuffer.RemainSize, SocketFlags.None, out eSocketError, new AsyncCallback( SendCallback ), kStateObj );
			if( eSocketError != SocketError.Success )
			{
				kSocket.Disconnect();
				return false;
			}

			return true;
		}

		private void SendCallback( IAsyncResult ar )
		{
			ClientSendAsyncStateObject kStateObj = (ClientSendAsyncStateObject)ar.AsyncState;
			BMClientTcpSocket kTcpSocket = kStateObj.m_kSocket;
			SendBuffer kBuffer = kStateObj.m_kBuffer;
			Socket socket = kTcpSocket.Sock;

			SocketError eSocketError = SocketError.Success;
			Int32 sendByte = socket.EndSend( ar, out eSocketError );

			if( sendByte <= 0 || eSocketError != SocketError.Success )
			{
				kTcpSocket.Disconnect();

				return;
			}

			kBuffer.Pop( sendByte );
			if( kBuffer.RemainSize > 0 )
			{
				BeginSend( kTcpSocket, kBuffer );
				return;
			}

			OnSend( kTcpSocket, kBuffer );
			Send( kTcpSocket );
		}

		private void OnSend( BMClientTcpSocket socket, SendBuffer buff )
		{
			if( null == m_kListener )
				return;

			m_kListener.OnSend( buff );
		}

		internal void SetListener( IClientSendListener kListner )
		{
			m_kListener = kListner;
		}
	}
}