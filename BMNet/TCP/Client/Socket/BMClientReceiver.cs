﻿using System;
using System.Net.Sockets;

namespace BMNet
{
	public interface IClientRecvListener
	{
		void OnDisconnect( String reason );

		void OnHardDisconnect( String reason );

		void OnRecv( BMClientTcpSocket tcpsocket, byte[] arData, Int32 nPacketLen );
	};

	public class BMClientReceiver
	{
		private IClientRecvListener m_kListener;

		internal bool Init()
		{
			return true;
		}

		internal void SetListener( IClientRecvListener kListner )
		{
			m_kListener = kListner;
		}

		public bool Recv( BMClientTcpSocket _kTcpSocket )
		{
			SocketError eSocketError = SocketError.Success;

			_kTcpSocket.Sock.BeginReceive( _kTcpSocket.RecvBuffer, 0, (Int32)NetCommon.MAX_PACKET_SIZE, SocketFlags.None, out eSocketError,
										 new AsyncCallback( ReadCallback ), _kTcpSocket );

			return true;
		}

		public void ReadCallback( IAsyncResult ar )
		{
			BMClientTcpSocket kTcpSocket = (BMClientTcpSocket)ar.AsyncState;
			Socket socket = kTcpSocket.Sock;

			SocketError eSocketError = SocketError.Success;
			Int32 bytesTransferred = socket.EndReceive( ar, out eSocketError );

			// Passive 연결 끊김
			if( bytesTransferred == 0 )
			{
				if( null != m_kListener )
					m_kListener.OnDisconnect( "Passive 연결 끊김" );

				return;
			}

			OnRecv( kTcpSocket, kTcpSocket.RecvBuffer, bytesTransferred );

			// Async Recv 포스트
			socket.BeginReceive( kTcpSocket.RecvBuffer, 0, (Int32)NetCommon.MAX_PACKET_SIZE, SocketFlags.None, out eSocketError,
									 new AsyncCallback( ReadCallback ), kTcpSocket );

			if( eSocketError != SocketError.Success )
			{
				kTcpSocket.Disconnect();
			}
		}

		private void OnRecv( BMClientTcpSocket kTcpSocket, byte[] arRecvBuffer, Int32 bytesTransferred )
		{
			if( null == m_kListener )
				return;

			m_kListener.OnRecv( kTcpSocket, arRecvBuffer, bytesTransferred );
		}
	}
}