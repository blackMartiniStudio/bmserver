﻿using BMDK;
using System;
using System.Collections.Generic;
using System.Net.Sockets;

//using System.Collections.Concurrent;

namespace BMNet
{
	public class BMClientTcpSocket : BMSocket
	{
		protected SocketStatus  m_eStatus;
		protected String        m_strLastFunc;
		protected List<String>  m_liFuncStack;

		protected bool          m_bInitialized;
		protected String        m_strHost;
		protected Int32         m_nPort;

		protected bool          m_bSocketReuse;

		protected Byte[]        m_RecvBuffer;

		protected BMConcurrentQueue<SendBuffer> m_quSendBuffer;

		protected BMClientConnector             m_kConnector;
		protected BMClientSender                m_kSender;
		protected BMClientReceiver              m_kReceiver;

		public Int32 Port
		{
			get { return m_nPort; }
		}

		public String Host
		{
			get { return m_strHost; }
		}

		public bool IsActive
		{
			get
			{
				if( m_kSocket == null )
					return false;

				return m_kSocket.Connected;
			}
		}

		public Byte[] RecvBuffer
		{
			get { return m_RecvBuffer; }
		}

		public BMClientTcpSocket( BMClientConnector _kConnector, BMClientSender _kSender, BMClientReceiver _kReceiver )
		{
			m_kConnector = _kConnector;
			m_kSender = _kSender;
			m_kReceiver = _kReceiver;

			m_liFuncStack = new List<string>();
			m_RecvBuffer = new Byte[(Int32)NetCommon.MAX_PACKET_SIZE];
			m_quSendBuffer = new BMConcurrentQueue<SendBuffer>();
		}

		public bool CreateSocket( AddressFamily addressFamily, String _strRemoteIP, Int32 _nPort, bool _bNoDelay, bool _bReuse )
		{
			m_strHost = _strRemoteIP;
			m_nPort = _nPort;

			m_bNoDelay = _bNoDelay;
			m_bReuse = _bReuse;

			m_kSocket = new Socket( addressFamily, SocketType.Stream, ProtocolType.Tcp );
			m_kSocket.LingerState = new LingerOption( true, 10 );
			m_kSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.NoDelay, _bNoDelay );
			m_kSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, _bReuse );

			return true;
		}

		public bool Recv()
		{
			if( false == IsSocketOpened() )
				return false;

			if( false == m_kReceiver.Recv( this ) )
			{
				return false;
			}

			return true;
		}

		internal virtual bool Send( LinkedList<SendBuffer> liBuffer )
		{
			if( false == IsSocketOpened() )
			{
				//  로그
				BMLogger.WarnLog( "Send Error. Client Tcp Socket Not Opened " );
				return false;
			}

			EnqueueSendBuffer( liBuffer );

			if( false == m_kSender.Send( this ) )
				return false;

			return true;
		}

		protected void CloseSocket()
		{
			if( false == IsSocketOpened() )
			{
				return;
			}

			Shotdown( SocketShutdown.Both );
			base.CloseSocket( FileFunction.__Function() );
		}

		public void Fini( String _strWhere )
		{
			CloseSocket();
		}

		public bool Disconnect()
		{
			CloseSocket();
			return true;
		}

		private void Shotdown( SocketShutdown _eShutdown = SocketShutdown.Both )
		{
			m_kSocket.Shutdown( _eShutdown );
		}

		public void GetTraffic( out Int32 _nSendTraffic, out Int32 _nRecvTraffic )
		{
			_nSendTraffic = 0;
			_nRecvTraffic = 0;
		}

		public void EnqueueSendBuffer( LinkedList<SendBuffer> _liBuff )
		{
			foreach( SendBuffer kBuffer in _liBuff )
			{
				m_quSendBuffer.Enqueue( kBuffer );
			}
		}

		internal bool DequeueSendBuffer( out SendBuffer kBuffer )
		{
			return m_quSendBuffer.TryDequeue( out kBuffer );
		}

		internal void SetHost( String _strHost )
		{
			m_strHost = _strHost;
		}
	}
}