﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using BMDK;

namespace BMNet
{
	public class BMNetClient : BMCommandCommunicator
	{
		public BMNetClientDesc Desc { get; protected set; }
		protected UInt64                m_uidServer;
		protected BMCommandStream       m_kCommandStream;
		protected BMCommandHandler      m_kCmdHandler;

		protected BMClientTcpSocket     m_kSocket;

		protected Queue<SendPacketNode> m_quSendPacket;

		protected BMClientConnector                 m_kConnector;
		protected BMCommandClientConnectListener    m_kConnectListener;

		protected BMClientSendManager               m_kSendMgr;
		protected BMClientSender                    m_kSender;
		protected BMCommandClientSendListener       m_kSendListener;

		protected BMClientReceiver                  m_kReceiver;
		protected BMCommandClientRecvListener       m_kRecvListener;

		protected bool                              m_bASyncSending = true;

		protected bool                              m_IsConnectionKeep;
		protected Regulator                         m_kReqlatorKeeper;

		public BMClientSendManager SendMgr
		{
			get { return m_kSendMgr; }
		}

		public BMNetClient( BMNetClientDesc _kDesc )
		{
			Desc = _kDesc;

			MyUID = 0;
			m_uidServer = 0;

			m_kCommandStream = new BMCommandStream( CommandMgr, m_kCommandBuilder );
			m_kCmdHandler = new BMCommandHandlerNetClient( this );

			m_quSendPacket = new Queue<SendPacketNode>();

			m_kConnector = new BMClientConnector();
			m_kConnectListener = new BMCommandClientConnectListener( this );

			m_kSender = new BMClientSender();
			m_kSendListener = new BMCommandClientSendListener( this );

			m_kReceiver = new BMClientReceiver();
			m_kRecvListener = new BMCommandClientRecvListener( this );

			m_kSocket = new BMClientTcpSocket( m_kConnector, m_kSender, m_kReceiver );
			m_kSendMgr = new BMClientSendManager( m_kSocket );

			m_kConnector.SetListener( m_kConnectListener );
			m_kSender.SetListener( m_kSendListener );
			m_kReceiver.SetListener( m_kRecvListener );

			m_IsConnectionKeep = false;
			m_kReqlatorKeeper = new Regulator( 1000 );
			m_kReqlatorKeeper.Stop();
		}

		public ResultTable CreateSocket( AddressFamily addressFamily, String _strRemoteIP, Int32 _nPort, bool _bNodelay, bool _bReuse )
		{
			if( false == m_kSocket.CreateSocket( addressFamily, _strRemoteIP, _nPort, _bNodelay, _bReuse ) )
				return ResultTable.UNKNOWN;

			return ResultTable.SUCCESS;
		}

		public void OnReplyConnect( UInt64 uidHost, UInt64 uidAlloc, Int64 nTimeTick )
		{
			m_uidServer = uidHost;
			MyUID = uidAlloc;
		}

		public ResultTable Connect( bool isConnectionKeep = true )
		{
			m_IsConnectionKeep = isConnectionKeep;

			// 로그
			BMLogger.InfoLog( " BMNetClient Connet : Keep[{0}]", isConnectionKeep );

			if( m_kSocket.Sock == null )
				return ResultTable.UNKNOWN;

			if( m_kSocket.IsSocketOpened() )
				return ResultTable.ALREADY_SOCKET_CONNECTED;

			if( false == m_kConnector.Connect( m_kSocket ) )
				return ResultTable.SOCKET_CONNECT_ERROR;
			
			return ResultTable.SUCCESS;
		}

		public virtual void OnConnect()
		{

		}

		public virtual void Disconnect( string rease = "" )
		{
			//Logger.DebugLog("Disconnect - {0}", rease );

			m_kSocket.Disconnect();

			if( m_IsConnectionKeep )
			{
				m_kReqlatorKeeper.Start();
			}
		}

		public override void SendCommand( BMCommand kCommand )
		{
			Byte[] header;
			Byte[] body;
			if( false == m_kCommandBuilder.BuildToStream( kCommand, out header, out body ) )
			{
				// 로그
				BMLogger.ErrLog( "SendCommand BuildToStream Error. CommandID[{0}]", kCommand.ID );
				return;
			}

			if( true == m_bASyncSending )
			{
				//  비동기 Send
				m_kSendMgr.LazySend( kCommand.ID, header, body );
			}
			else
			{
				// 동기 Send
				Byte[] rowData = new Byte[header.Length + body.Length];
				Buffer.BlockCopy( header, 0, rowData, 0, header.Length );
				Buffer.BlockCopy( body, 0, rowData, header.Length, body.Length );

				m_quSendPacket.Enqueue( new SendPacketNode( kCommand.ID, rowData ) );
			}
		}

		protected override void OnPrepareUpdate()
		{
			base.OnPrepareUpdate();
		}

		protected override void OnUpdate()
		{
			m_kSendMgr.AllSend();

			ConnectionKeeper();
		}

		private void ConnectionKeeper()
		{
			if( m_kReqlatorKeeper.IsUpdate() )
			{
				if( false == m_kSocket.CreateSocket( m_kSocket.Sock.AddressFamily, m_kSocket.Host, m_kSocket.Port, m_kSocket.NoDelay, m_kSocket.Reuse ) )
				{
					//Logger.ErrLog( "ConnectionKeeper CreateSocket Fail!" );
					return;
				}

				Connect( true );
				m_kReqlatorKeeper.Stop();
			}
		}

		protected void OnRun()
		{
		}

		public void OnRecv( Byte[] PacketData, Int32 _recvSize )
		{
			UInt64 uidReceiver = MyUID;
			UInt64 uidSender = m_uidServer;

			if( false == m_kCommandStream.Read( PacketData, _recvSize ) )
				return;

			while( true )
			{
				BMCommand kCmd;
				if( false == m_kCommandStream.TryPopCommand( out kCmd ) )
					break;

				kCmd.ReceiverUID = uidReceiver;
				kCmd.SendererUID = uidSender;

				RecvCommand( kCmd );
			}
		}

		public BMCommand NewCommand( Int32 nCmdId )
		{
			return new BMCommand( nCmdId, MyUID, m_uidServer );
		}
	}
}