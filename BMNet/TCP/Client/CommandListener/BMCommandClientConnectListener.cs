﻿using System;

namespace BMNet
{
	public class BMCommandClientConnectListener : IClientConnectListener
	{
		private BMNetClient m_kCommunicator;

		public BMCommandClientConnectListener( BMNetClient _kNetClient )
		{
			m_kCommunicator = _kNetClient;
		}

		public virtual void OnConnect( BMClientTcpSocket _kSocket )
		{
			// Connect 되었다는 패킷을 보낼 수 있도록 한다.
			BMCommand kNewCommand = m_kCommunicator.NewLocalCommand( (Int32)PacketCommandEnum.CMD_PacketNetConnect );
			m_kCommunicator.AsyncPost( kNewCommand );
		}
	}
}