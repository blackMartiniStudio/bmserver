﻿using System;

namespace BMNet
{
	public class BMCommandClientSendListener : IClientSendListener
	{
		private BMNetClient m_kNetClient;

		public BMCommandClientSendListener( BMNetClient _kClient )
		{
			m_kNetClient = _kClient;
		}

		public virtual void OnSend( SendBuffer _kSendBuffer )
		{
			//m_kNetClient.TrafficMonitor.RecordSend( _kSendBuffer.DataSize );
			m_kNetClient.SendMgr.RestoreSendBuffer( _kSendBuffer );
		}

		public virtual void OnHardDisconnect( String reason )
		{
		}
	}
}