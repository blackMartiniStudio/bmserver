﻿using System;

namespace BMNet
{
	public class BMCommandClientRecvListener : IClientRecvListener
	{
		private BMNetClient m_kNetClient;

		public BMCommandClientRecvListener( BMNetClient _kClient )
		{
			m_kNetClient = _kClient;
		}

		public virtual void OnDisconnect( String reason )
		{
			m_kNetClient.Disconnect( reason );
		}

		public virtual void OnHardDisconnect( String reason )
		{
			m_kNetClient.Disconnect( reason );
		}

		public virtual void OnRecv( BMClientTcpSocket _kSocket, byte[] arData, Int32 nPacketLen )
		{
			// m_kNetClient.TrafficMonitor.RecordRecv( nPacketLen );

			m_kNetClient.OnRecv( arData, nPacketLen );
		}
	}
}