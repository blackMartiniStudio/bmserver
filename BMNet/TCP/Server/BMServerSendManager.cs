﻿using System;
using System.Collections.Generic;

namespace BMNet
{
	using liSendBuffer = LinkedList<SendBuffer>;

	public class BMServerSendManager
	{
		private SendBufferObject m_SendBufferObj = new SendBufferObject();
		private Dictionary<UInt64, liSendBuffer> m_dicSendBuffer = new Dictionary<UInt64, liSendBuffer>();
		private BMNetServer m_kNetServer;

		public BMServerSendManager( BMNetServer _kNetServer )
		{
			m_kNetServer = _kNetServer;
		}

		internal void LazySend( Int32 nID, UInt64 uidReceiver, byte[] header, byte[] body )
		{
			Int32 nPacketSize = header.Length + body.Length;

			SendBuffer kBuffer = GetBuffer( uidReceiver, nPacketSize );

			kBuffer.Push( header );
			kBuffer.Push( body );
		}

		private SendBuffer GetBuffer( ulong uidReceiver, int nPacketSize )
		{
			liSendBuffer liBuff;
			if( false == m_dicSendBuffer.TryGetValue( uidReceiver, out liBuff ) )
			{
				liBuff = new liSendBuffer();
				m_dicSendBuffer.Add( uidReceiver, liBuff );
				liBuff.AddLast( m_SendBufferObj.Get() );
			}

			SendBuffer kBuff;
			if( null == liBuff.Last )
			{
				kBuff = m_SendBufferObj.Get();
				liBuff.AddLast( kBuff );
			}
			else
			{
				kBuff = liBuff.Last.Value;
				if( kBuff.Lock == true || kBuff.SpareSize < nPacketSize )
				{
					kBuff = m_SendBufferObj.Get();
					liBuff.AddLast( kBuff );
				}
			}

			return kBuff;
		}

		internal void AllSend()
		{
			foreach( var keyValue in m_dicSendBuffer )
			{
				Send( keyValue.Key, keyValue.Value );
			}
		}

		private void Send( UInt64 uidReceiver, liSendBuffer liBuffer )
		{
			if( liBuffer.Count == 0 )
				return;

			LockSendBuffer( liBuffer );

			if( false == m_kNetServer.Send( uidReceiver, liBuffer ) )
			{
				foreach( SendBuffer kBuffer in liBuffer )
				{
					RestoreSendBuffer( kBuffer );
				}
			}

			liBuffer.Clear();
		}

		internal void RestoreSendBuffer( SendBuffer kBuffer )
		{
			m_SendBufferObj.Restore( kBuffer );
		}

		internal void Clear( ulong uid )
		{
			liSendBuffer liBuffer;
			if( false == m_dicSendBuffer.TryGetValue( uid, out liBuffer ) )
				return;

			Send( uid, liBuffer );

			m_dicSendBuffer.Remove( uid );
		}

		internal void ReallocUID( ulong uidOriginal, ulong uidReAlloc )
		{
			Clear( uidOriginal );
		}

		private void LockSendBuffer( liSendBuffer _liBuff )
		{
			foreach( SendBuffer kBuffer in _liBuff )
			{
				kBuffer.Locking();
			}
		}
	}
}