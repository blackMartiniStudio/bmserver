﻿using BMDK;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace BMNet
{
	public class BMNetServer : BMCommandCommunicator, ILinkReleaseListener
	{
		public BMNetServerDesc		Desc { get; protected set; }
		protected BMTCPServer       m_kTcpServer;
		protected BMTrafficMonitor  m_kTrafficMonitor;
		protected BMDelayTester     m_kDelayTester;

		protected BMConnector       m_kConnector;

		protected BMCommandHandler					m_kCommandNetHandler;
		protected BMCommandServerAcceptListener     m_kAcceptListener;
		protected BMCommandServerDisconnectListner  m_kDisconnectListner;
		protected BMCommandServerSendListner        m_kSendListener;
		protected BMCommandServerRecvListner        m_kRecvListner;

		protected BMSocketLinkManager               m_kSocketLinkMgr;
		protected BMServerSendManager               m_kPacketSendMgr;

		protected TimeTick                          m_kTimer;

		protected Int64                             m_nHeartBeatLastTime;
		protected Int64                             m_nDisconnCountByHeartBeat;
		protected Int64                             m_nDisconnCountByWrongPacket;

		public BMSocketLinkManager LinkMgr
		{
			get { return m_kSocketLinkMgr; }
		}

		public BMServerSendManager SendMgr
		{
			get { return m_kPacketSendMgr; }
		}

		public BMTrafficMonitor TrafficMonitor
		{
			get { return m_kTrafficMonitor; }
		}

		public TimeTick Timer
		{
			get { return m_kTimer; }
		}

		public BMNetServer( BMNetServerDesc _kDesc )
		{
			Desc = _kDesc;
			CommunicatorType = _kDesc.ServerName;

			m_kTimer = new TimeTick();
			m_kTcpServer = new BMTCPServer( m_kTimer );
			m_kTrafficMonitor = new BMTrafficMonitor();

			m_kCommandNetHandler = new BMCommandHandler_Server_Network( this );
			m_kAcceptListener = new BMCommandServerAcceptListener( this );
			m_kDisconnectListner = new BMCommandServerDisconnectListner( this );
			m_kSendListener = new BMCommandServerSendListner( this );
			m_kRecvListner = new BMCommandServerRecvListner( this );

			m_kSocketLinkMgr = new BMSocketLinkManager();
			m_kPacketSendMgr = new BMServerSendManager( this );

			m_kTcpServer.SetAcceptorListener( m_kAcceptListener );
			m_kTcpServer.SetDisconnectorListener( m_kDisconnectListner );
			m_kTcpServer.SetSenderListener( m_kSendListener );
			m_kTcpServer.SetReceiverListener( m_kRecvListner );

			m_kConnector = new BMConnector();
		}

		public virtual bool Start( AddressFamily addressFamily, Int32 nListenPort, bool bNoDelay,
			Int32 nSocketPoolSize = 3000, Int32 nSendPendingLimitCount = Int32.MaxValue, String strMyNetworkCardIP = "" )
		{
			SetUID( NewUID() );

			return m_kTcpServer.Start( addressFamily, nListenPort, bNoDelay, this, nSocketPoolSize, nSendPendingLimitCount, strMyNetworkCardIP );
		}

		public void Connect( List<ConnectSocketInfo> conSockInfos )
		{
			m_kConnector.Connect( conSockInfos );
		}

		public void SetConnectListener( IConnectListener kListner )
		{
			m_kConnector.SetListener( kListner );
		}

		protected override void OnPrepareUpdate()
		{
			base.OnPrepareUpdate();
			UpdateKeepingAlive();

			if( true == Desc.DelayTest )
			{
				Int64 nowTick = m_kTimer.GetNowTick();
				m_kDelayTester.OnRun( this, nowTick );
			}
		}

		protected override void OnUpdate()
		{
			m_kPacketSendMgr.AllSend();
			m_kTcpServer.Update();
		}

		private void UpdateKeepingAlive()
		{
			if( false == Desc.HeartBeat )
				return;

			Int64 nNowTick = m_kTimer.GetNowTick();

			if( nNowTick - m_nHeartBeatLastTime < Desc.HeartBeatTick )
				return;

			m_nHeartBeatLastTime = nNowTick;

			List<BMLinkSocket> LinkSockets = m_kSocketLinkMgr.LinkSockets;
			foreach( BMLinkSocket kSocket in LinkSockets )
			{
				if( true == kSocket.IsDisconnected() )
				{
					// 로그 : Link Already Disconnected
					BMLogger.WarnLog( "Link Already Disconnected" );
					continue;
				}

				if( false == kSocket.CheckAlive( nNowTick, Desc.HeartBeatTimeoutTick ) )
				{
					// 로그
					kSocket.SetRecvTime( nNowTick );
					kSocket.Disconnect( "Heartbeat failed" );

					IncreaseDisconnCountByHearBeat();
				}
			}
		}

		private void SetUID( UInt64 uid )
		{
			MyUID = uid;
		}

		public override bool Post( BMCommand kCommand )
		{
			if( Desc.DelayTest )
			{
				if( false == kCommand.CheckRule() )
					return false;

				Int64 nNowTick = m_kTimer.GetNowTick();
				m_kDelayTester.Post( kCommand, nNowTick );

				return true;
			}

			return base.Post( kCommand );
		}

		public override void SendCommand( BMCommand kCommand )
		{
			byte[] header;
			byte[] body;
			if( false == BuildStream( kCommand, out header, out body ) )
			{
				// 로그
				BMLogger.ErrLog( "SendCommand BuildToStream Error. CommandID[{0}]", kCommand.ID );
				return;
			}

			m_kPacketSendMgr.LazySend( kCommand.ID, kCommand.ReceiverUID, header, body );
		}

		private bool BuildStream( BMCommand kCommand, out byte[] header, out byte[] body )
		{
			return m_kCommandBuilder.BuildToStream( kCommand, out header, out body );
		}

		public bool Send( UInt64 uidReceiver, LinkedList<SendBuffer> liBuffer )
		{
			BMLinkSocket socket;
			if( false == LinkMgr.TryGetSocket( uidReceiver, out socket ) )
			{
				// 로그
				BMLogger.ErrLog( "Send Fail. Get Socket Error. uidReceiver[{0}]", uidReceiver );
				return false;
			}

			if( false == socket.IsLinkValid() )
			{
				// 로그
				BMLogger.ErrLog( "Send Fail. Link Valid Error. uidReceiver[{0}]", uidReceiver );
				return false;
			}

			socket.Send( liBuffer );

			return true;
		}

		public virtual void Disconnect( String reason, UInt64 uidLink )
		{
			BMLinkSocket kSocket;
			if( false == m_kSocketLinkMgr.TryGetSocket( uidLink, out kSocket ) )
			{
				if( false == m_kSocketLinkMgr.TryGetLinkFromReAllocTable( uidLink, out kSocket ) )
					return;
			}

			FlushSend( uidLink );
			kSocket.Disconnect( reason );
		}

		public void HardDisconnect( UInt64 uidLink )
		{
			BMLinkSocket kSocket;
			if( false == m_kSocketLinkMgr.TryGetSocket( uidLink, out kSocket ) )
			{
				if( false == m_kSocketLinkMgr.TryGetLinkFromReAllocTable( uidLink, out kSocket ) )
					return;
			}

			FlushSend( uidLink );
			kSocket.HardDisconnect();
		}

		public void OnReleaseLink( UInt64 uid )
		{
			m_kPacketSendMgr.Clear( uid );
		}

		public void FlushSend( UInt64 uid )
		{
			m_kPacketSendMgr.Clear( uid );
		}

		public bool ReAllocLinkUID( UInt64 uidOriginal, UInt64 uidReAlloc )
		{
			m_kPacketSendMgr.ReallocUID( uidOriginal, uidReAlloc );

			if( false == m_kSocketLinkMgr.ReallockUID( uidOriginal, uidReAlloc ) )
				return false;

			BMCommand kCmd = NewCommand( (Int32)PacketCommandEnum.CMD_PacketNetReallocUID, uidReAlloc );
			kCmd.SetPacket( new TDNetReallocUID( uidReAlloc ) );
			Post( kCmd );

			return true;
		}

		public void ResetDisconnCount()
		{
			Interlocked.Exchange( ref m_nDisconnCountByHeartBeat, 0 );
			Interlocked.Exchange( ref m_nDisconnCountByWrongPacket, 0 );
		}

		internal void IncreaseDisconnCountByHearBeat()
		{
			Interlocked.Increment( ref m_nDisconnCountByHeartBeat );
		}

		internal void IncreaseDisconnCountByWrongPacket()
		{
			Interlocked.Increment( ref m_nDisconnCountByWrongPacket );
		}

		public Int64 GetDisconnCountByHearBeat()
		{
			return m_nDisconnCountByHeartBeat;
		}

		public Int64 GetDisconnCountByWrongPacket()
		{
			return m_nDisconnCountByWrongPacket;
		}

		public String GetIPAddr( UInt64 uid )
		{
			BMLinkSocket kSocket;
			if( false == m_kSocketLinkMgr.TryGetSocket( uid, out kSocket ) )
				return "N/A";

			return kSocket.GetRemoteIP();
		}

		public void GetTotalTraffic( out Int64 outRecvSize, out Int64 outSendSize )
		{
			m_kTrafficMonitor.GetTotalTraffic( out outRecvSize, out outSendSize );
		}

		public void GetSecondTraffic( out Int64 outRecvSize, out Int64 outSendSize )
		{
			m_kTrafficMonitor.GetSecondTraffic( out outRecvSize, out outSendSize );
		}

		public Int32 GetLinkSize()
		{
			return m_kSocketLinkMgr.LinkCount;
		}
	}
}