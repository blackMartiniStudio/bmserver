﻿using BMDK;
using System;
using System.Net.Sockets;

namespace BMNet
{
	public interface ISendListener
	{
		void OnSend( UInt64 uid, SendBuffer _kSendBuffer );

		void OnHardDisconnect( UInt64 uidLink, String reason );
	};

	public class SendAsyncStateObject
	{
		public BMServerTcpSocket m_kTcpSocket;
		public SendBuffer        m_kBuffer;

		public SendAsyncStateObject( BMServerTcpSocket kSocket, SendBuffer kBuffer )
		{
			m_kTcpSocket = kSocket;
			m_kBuffer = kBuffer;
		}
	}

	public class BMSender : ISender
	{
		private ISendListener m_kListener;

		internal bool Init()
		{
			return true;
		}

		public void SetListener( ISendListener kListner )
		{
			m_kListener = kListner;
		}

		public bool Send( BMServerTcpSocket socket )
		{
			SendBuffer kBuffer;
			if( false == socket.DequeueSendBuffer( out kBuffer ) )
				return true;

			socket.GetIO().OnStartSend();

			return BeginSend( socket, kBuffer );
		}

		private bool BeginSend( BMServerTcpSocket socket, SendBuffer kBuffer )
		{
			SendAsyncStateObject kStateObj = new SendAsyncStateObject( socket, kBuffer );

			SocketError eSocketError = SocketError.Success;

			socket.Sock.BeginSend( kBuffer.Buff, kBuffer.m_BufOffset, kBuffer.RemainSize, SocketFlags.None, out eSocketError, new AsyncCallback( SendCallback ), kStateObj );
			if( eSocketError != SocketError.Success )
			{
				socket.DecreaseSendPendingCounter();
				socket.Disconnect( FileFunction.__Function() );

				return false;
			}

			return true;
		}

		private void SendCallback( IAsyncResult ar )
		{
			SendAsyncStateObject kStateObj = (SendAsyncStateObject)ar.AsyncState;
			BMServerTcpSocket kTcpSocket = kStateObj.m_kTcpSocket;

			if( kTcpSocket == null || kTcpSocket.Sock == null )
				return;

			if( kTcpSocket.GetIO().IsIOState( IOState.IOState_Closed ) == true ||
				kTcpSocket.GetIO().IsIOState( IOState.IOState_TryingTransmitFile ) == true )
				return;


			if( kTcpSocket.IsSocketOpened() == false )
				return;

			Socket socket = kTcpSocket.Sock;

			SendBuffer kBuffer = kStateObj.m_kBuffer;

			SocketError eSocketError = SocketError.Success;
			Int32 sendByte = socket.EndSend( ar, out eSocketError );

			if( sendByte <= 0 || eSocketError != SocketError.Success )
			{
				kTcpSocket.DecreaseSendPendingCounter();
				kTcpSocket.Disconnect( FileFunction.__Function() );

				return;
			}

			kBuffer.Pop( sendByte );
			if( kBuffer.RemainSize > 0 )
			{
				BeginSend( kTcpSocket, kBuffer );
				return;
			}

			OnSend( kTcpSocket, kBuffer );
			kTcpSocket.GetIO().OnSend();

			Send( kTcpSocket );

		}

		private void OnSend( BMServerTcpSocket socket, SendBuffer buff )
		{
			if( null == m_kListener )
				return;

			m_kListener.OnSend( socket.GetLink(), buff );
		}
	}
}