﻿using BMDK;
using System;
using System.Net.Sockets;

namespace BMNet
{
	public interface IRecvListener
	{
		void OnDisconnect( UInt64 uidLink, String reason );

		void OnHardDisconnect( UInt64 uidLink, String reason );

		void OnRecv( BMServerTcpSocket tcpsocket, UInt64 uidLink, byte[] arData, Int32 nPacketLen );
	};

	public class BMReceiver : IReceiver
	{
		private IRecvListener m_kListener;

		internal bool Init()
		{
			return true;
		}

		public void SetListener( IRecvListener kListner )
		{
			m_kListener = kListner;
		}

		public bool Recv( BMServerTcpSocket _kTcpSocket )
		{
			_kTcpSocket.Sock.BeginReceive( _kTcpSocket.RecvBuffer, 0, (Int32)NetCommon.MAX_PACKET_SIZE, 0,
										 new AsyncCallback( ReadCallback ), _kTcpSocket );

			return true;
		}

		public void ReadCallback( IAsyncResult ar )
		{

			BMServerTcpSocket kTcpSocket = (BMServerTcpSocket)ar.AsyncState;
			if( kTcpSocket == null || kTcpSocket.Sock == null )
				return;

			if( kTcpSocket.GetIO().IsIOState( IOState.IOState_Closed ) == true ||
				kTcpSocket.GetIO().IsIOState( IOState.IOState_TryingTransmitFile ) == true )
				return;

			if( kTcpSocket.IsSocketOpened() == false )
				return;

			Socket socket = kTcpSocket.Sock;

			SocketError eSocketError = SocketError.Success;
			Int32 bytesTransferred = socket.EndReceive( ar, out eSocketError );

			// Passive 연결 끊김
			if( bytesTransferred == 0 )
			{
				if( null != m_kListener )
					m_kListener.OnDisconnect( kTcpSocket.GetLink(), "Passive 연결 끊김" );

				return;
			}

			OnRecv( kTcpSocket, kTcpSocket.GetLink(), kTcpSocket.RecvBuffer, bytesTransferred );

			// Async Recv 포스트
			socket.BeginReceive( kTcpSocket.RecvBuffer, 0, (Int32)NetCommon.MAX_PACKET_SIZE, 0, out eSocketError,
									 new AsyncCallback( ReadCallback ), kTcpSocket );

			if( eSocketError != SocketError.Success )
			{
				kTcpSocket.Disconnect( FileFunction.__Function() );
			}


		}

		private void OnRecv( BMServerTcpSocket kTcpSocket, UInt64 uidLink, byte[] arRecvBuffer, Int32 bytesTransferred )
		{
			if( null == m_kListener )
				return;

			m_kListener.OnRecv( kTcpSocket, uidLink, arRecvBuffer, bytesTransferred );
		}
	}
}