﻿using System;
using System.Net;
using System.Net.Sockets;

namespace BMNet
{
	public class BMTCPListenSocket : BMSocket
	{
		private IPEndPoint m_RemoteEndPoint;
		private Int32 m_nListenBacklog = 100;     // 연결 요청 대기큐의 크기

		internal bool Init( AddressFamily addressFamily, int nListenPort, bool bNoDelay, bool bReuse, String strMyNetworkCardIP )
		{
			if( strMyNetworkCardIP == "" )
			{
				m_RemoteEndPoint = new IPEndPoint( IPAddress.Any, nListenPort );
			}
			else
			{
				m_RemoteEndPoint = new IPEndPoint( IPAddress.Parse( strMyNetworkCardIP ), nListenPort );
			}

			if( false == base.CreateSocket( addressFamily, SocketType.Stream, ProtocolType.Tcp, bNoDelay, bReuse ) )
				return false;

			return true;
		}

		internal bool Listen( BMAcceptor _kAcceptor )
		{
			m_kSocket.Bind( m_RemoteEndPoint );
			m_kSocket.Listen( m_nListenBacklog );

			return true;
		}
	}
}