﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Diagnostics;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

using BMNetwork.Managed;
using System.Collections.Concurrent;

namespace KMServerCommon
{
    public delegate bool EncodePacketDelegate(Int32 protocol, Object pkt, out Byte[] header, out Byte[] body);
    public delegate BatchResult DecodePacketDelegate(PerSocketContext sock_ctx, ISocket client);
    public delegate bool RecvPacketDelegate( PerSocketContext sock_ctx, Int32 bytesTransferred );


    public class PacketDelegateManager
    {
        Stopwatch updateStop = new Stopwatch();
        List<Int32> liTestCallCnt = new List<Int32>();

        // encode packet delegate
        private EncodePacketDelegate m_c2s_encode_packet_delegate = null;
        private EncodePacketDelegate m_s2s_encode_packet_delegate = null;
        private EncodePacketDelegate m_policy_encode_packet_delegate = null;
        private EncodePacketDelegate m_tooni_encode_packet_delegate = null;

        // decode packet delegate
        private DecodePacketDelegate m_c2s_decode_packet_delegate = null;
        private DecodePacketDelegate m_s2s_decode_packet_delegate = null;
        private DecodePacketDelegate m_policy_decode_packet_delegate = null;
        private DecodePacketDelegate m_tooni_decode_packet_delegate = null;


        // recv packet delegate
        private RecvPacketDelegate m_c2s_recv_packet_delegate = null;
        private RecvPacketDelegate m_s2s_recv_packet_delegate = null;
        private RecvPacketDelegate m_policy_recv_packet_delegate = null;
        private RecvPacketDelegate m_tooni_recv_packet_delegate = null;

		protected S2SCommandCommunicator m_kS2SCommandCommunicator = new S2SCommandCommunicatorNull();
		protected C2SCommandCommunicator m_kC2SCommandCommunicator = new C2SCommandCommunicatorNull();

        public PacketDelegateManager()
        {
            //updateStop.Start();

            m_c2s_encode_packet_delegate = new EncodePacketDelegate(this.C2SEncodePacketProcess);
            m_s2s_encode_packet_delegate = new EncodePacketDelegate(this.S2SEncodePacketProcess);

            // decode packet delegate
            m_c2s_decode_packet_delegate = new DecodePacketDelegate(this.C2SDecodePacketProcess);
            m_s2s_decode_packet_delegate = new DecodePacketDelegate(this.S2SDecodePacketProcess);

            // recv packet delegate
            m_c2s_recv_packet_delegate = new RecvPacketDelegate(this.C2SRecvPacketProcess);
            m_s2s_recv_packet_delegate = new RecvPacketDelegate(this.S2SRecvPacketProcess);
        }


        #region [Property]
        public EncodePacketDelegate C2SEncoder
        {
            get { return this.m_c2s_encode_packet_delegate; }
        }

        public EncodePacketDelegate S2SEncoder
        {
            get { return this.m_s2s_encode_packet_delegate; }
        }

        public EncodePacketDelegate PolicyEncoder
        {
            get { return this.m_policy_encode_packet_delegate; }
        }

        public EncodePacketDelegate TooniEncoder
        {
            get { return this.m_tooni_encode_packet_delegate; }
        }

        public DecodePacketDelegate C2SDecoder
        {
            get { return this.m_c2s_decode_packet_delegate; }
        }

        public DecodePacketDelegate S2SDecoder
        {
            get { return this.m_s2s_decode_packet_delegate; }
        }

        public DecodePacketDelegate PolicyDecoder
        {
            get { return this.m_policy_decode_packet_delegate; }
        }

        public DecodePacketDelegate TooniDecoder
        {
            get { return this.m_tooni_decode_packet_delegate; }
        }

        public RecvPacketDelegate C2SReceiver
        {
            get { return this.m_c2s_recv_packet_delegate; }
        }

        public RecvPacketDelegate S2SReceiver
        {
            get { return this.m_s2s_recv_packet_delegate; }
        }

        public RecvPacketDelegate PolicyReceiver
        {
            get { return this.m_policy_recv_packet_delegate; }
        }

        public RecvPacketDelegate TooniReceiver
        {
            get { return this.m_tooni_recv_packet_delegate; }
        }

        #endregion

        #region [Delegate Function]
        private bool C2SEncodePacketProcess(Int32 protocol, Object pkt, out Byte[] header, out Byte[] body)
        {
            
            header = null;
            body = null;
            Int32 bodyLength = 0;

            bool isCrypt = false;
            UInt32 dataCRC = 0;
            UInt16 packetOption = PacketConstant.PACKET_CRYPT_OPTION;

#if SERVERQA
            packetOption = PacketConstant.PACKET_OPTION_NONE;
#endif
            
            if ( pkt == null)
            {
                body = new Byte[0];
                bodyLength = 0;

                packetOption = PacketConstant.PACKET_OPTION_NONE;
            }
            else
            {
                body = C2SPacketCoder.Encode(protocol, pkt, out isCrypt);
                bodyLength = body.Length;

                if (isCrypt == true && PacketConstant.IsEncrypt(packetOption))
                {
                    if (Encryptor.INSTANCE.Encrypt(ref body, bodyLength) == false)
                        throw new InvalidOperationException("Encrypt Fail !!! pkt " + protocol);
                }
                else
                {
                    packetOption = (UInt16)(packetOption & (~PacketConstant.PACKET_OPTION_ENCRYPTION));
                }

                if ( PacketConstant.IsDataCRC( packetOption ) ==true)
                {
                    if (CRCChecker.INSTANCE.GetCRC(body, bodyLength, ref dataCRC) == false)
                        throw new InvalidOperationException("Make CRC FAil protocol " + (C2SPacketEnum)protocol);
                }
                else
                {
                    packetOption = (UInt16)(packetOption & (~PacketConstant.PACKET_OPTION_DATA_CRC));
                }

#if DEBUG
//                 if (body != null)
//                     Logger.DebugLog("Body Stream = {0} Pkt {1}", BitConverter.ToString(body), pkt.ToString());
#endif
            }

            {
                bool notUsingCrypt = false;
                header = C2SPacketEncoder.EncodeC2SPacketHeader(    new C2SPacketHeader(protocol, packetOption, dataCRC, bodyLength), 
                                                                    out notUsingCrypt);

#if DEBUG
//                 if (header != null)
//                     Logger.DebugLog("Header Stream = {0}, protocol {1} option {2} bodylenght {3}", BitConverter.ToString(header), protocol, packetOption, bodyLength);
#endif
            }

            if (header == null) return false;

            return true;
        }

        private bool S2SEncodePacketProcess(Int32 protocol, Object pkt, out Byte[] header, out Byte[] body)
        {

            bool isCrypt = false;
            Byte[] encodeBody = null;
            Int32 streamLength = 0;

            header = null;
            body = null;

            if (pkt != null)
            {
                encodeBody = S2SPacketCoder.Encode(protocol, pkt, out isCrypt);
                streamLength = encodeBody.Length;
            }
            else
            {
                encodeBody = new Byte[0];
                streamLength = 0;
            }

            if (encodeBody.Length > ManagedServer.COMPRESS_PACKET_LENGTH)
            {
                MiniLZO.Compress(encodeBody, out body);
#if DEBUG
                Logger.DebugLog("Compress protocol {0} src length {1} dest {2}", (S2SPacketEnum)protocol, encodeBody.Length, body.Length);
#endif
            }
            else
            {
                body = encodeBody;
            }

            streamLength = body.Length;

            
            S2SPacketHeader pktHeader = new S2SPacketHeader(protocol, streamLength, encodeBody.Length);
            {
                // TODO: seedyoon [10/29/2013]
                // isCrypt 변수는 해더에서 사용하지 않는다. 현재 idl 구조상 모든 패킷이 동일하게 적용되서 이렇게 사용
                // 차후 시간날 때 수정 요망, 급하다.
                bool notUsingFlag = false;
                header = S2SPacketEncoder.EncodeS2SPacketHeader(pktHeader, out notUsingFlag);
            }

            if (header == null) return false;

            return true;
        }

        private BatchResult C2SDecodePacketProcess(PerSocketContext sock_ctx, ISocket client)
        {
            try
            {
                C2SPacketHeader pkt_header = null;
                Int32 hLen = (Int32)PacketVariables.C2S_PKT_HDR_SIZE;
                Byte[] WriteHeadBuffer = new Byte[hLen];

                Int32 nPacketCount = sock_ctx.ReadablePacketCount;
                //for ( int i = 0; i < nPacketCount; ++i )
				Byte[] SocketBuffer;
                while ( sock_ctx.PopPacketByPool( out SocketBuffer ) )
                {
                    sock_ctx.ReadHeader( SocketBuffer, WriteHeadBuffer, hLen );
                    pkt_header = C2SPacketDecoder.DecodeC2SPacketHeader(WriteHeadBuffer);

#if DEBUG
//                     Logger.DebugLog("RecvPkt() :: code = {0}  option {1} crc {2} length {3}", pkt_header.mCode, pkt_header.mOption, pkt_header.mCRC, pkt_header.mBodySize);
//                     Logger.DebugLog("Header Packet Data = {0}", BitConverter.ToString(header_stream));
#endif
                    

                    // 잘못된 패킷, 이 클라이언트 연결을 끊는다.
                    if (pkt_header.mCode <= 0)
                    {
                        System.Diagnostics.Debug.Assert(false);
                        sock_ctx.BufferClear();
                        return BatchResult.Fail;
                    }

                    UInt16 packetOption = pkt_header.mOption;
                    Byte[] body_stream = new Byte[pkt_header.mBodySize];
					sock_ctx.ReadBody( SocketBuffer, body_stream, hLen, pkt_header.mBodySize );

                    if (PacketConstant.IsDataCRC(packetOption) == true)
                    {
                        UInt32 dataCRC = 0;

                        if (CRCChecker.INSTANCE.GetCRC(body_stream, body_stream.Length, ref dataCRC) == false)
                            throw new InvalidOperationException("Decode CRC Function Error cmd " + pkt_header.mCode);

                        if (pkt_header.mCRC != dataCRC)
                            throw new InvalidOperationException("Incorrect CRC Data cmd " + pkt_header.mCode);
                    }

                    if (PacketConstant.IsEncrypt(packetOption) == true)
                    {
                        if (Encryptor.INSTANCE.Decrypt(ref body_stream, body_stream.Length) == false)
                            throw new InvalidOperationException("Decode Decrypt Fail Error cmd " + pkt_header.mCode);
                    }

                    Object pkt_body = C2SPacketCoder.Decode(pkt_header.mCode, body_stream);
#if DEBUG
//                     if(pkt_body !=null)
//                         Logger.DebugLog("Body Stream = {0} Pkt {1}", BitConverter.ToString(body_stream), pkt_body.ToString());
#endif

                    client.MsgDispatch(pkt_header.mCode, pkt_body);

					C2SPacketEnum kPacketCode = C2SCommandCommunicator.ParsePacketCode(pkt_header.mCode);
					if (C2SPacketEnum.CMD_NONE != kPacketCode)
					{
						this.m_kC2SCommandCommunicator.PushCommand( new GCommand<C2SPacketEnum>( client , kPacketCode , pkt_body ) );
                }
                }
                return BatchResult.Success;
            }
            catch (InvalidOperationException ioe)
            {
                Logger.ExceptionLog(ioe);
                return BatchResult.Fail;
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return BatchResult.Fail;
            }
        }

        private BatchResult S2SDecodePacketProcess(PerSocketContext sock_ctx, ISocket client)
        {
            try
            {
                S2SPacketHeader pkt_header = null;
                Int32 hLen = (Int32)S2SPacketVariables.S2S_PKT_HDR_SIZE;
                Byte[] header_stream = new Byte[hLen];
                Int32 nPacketCount = sock_ctx.ReadablePacketCount;

                //liTestCallCnt.Add( nPacketCount );

                //if ( updateStop.Elapsed.Ticks  > TimeSpan.TicksPerSecond )
                //{
                //    StringBuilder testSTR = new StringBuilder();
                //    testSTR.Append( "====== Start =======\n" );
                //    foreach ( var info in liTestCallCnt )
                //    {
                //        testSTR.AppendFormat( "{0}, ", info.ToString() );
                //    }
                //    testSTR.Append( "\n====== End =======\n" );

                //    Logger.InfoLog( "{0}", testSTR.ToString() );

                //    liTestCallCnt.Clear();
                //    updateStop.Restart();
                //}
				Byte[] SocketBuffer;
                while ( sock_ctx.PopPacketByPool( out SocketBuffer ) )
                {
                    sock_ctx.ReadHeader( SocketBuffer, header_stream, hLen );
                    pkt_header = S2SPacketDecoder.DecodeS2SPacketHeader( header_stream );

                    // 잘못된 패킷, 이 클라이언트 연결을 끊는다.
                    if ( pkt_header.code == 0 )
                    {
                        System.Diagnostics.Debug.Assert( false );
                        sock_ctx.BufferClear();
                        Logger.ErrLog( "S2SDecodePacketProcess MemoryDump {0}", BitConverter.ToString( header_stream ) );
                        return BatchResult.Fail;
                    }

                    // 압축되어 있는 패킷인지 아닌지 판단. decrypt 를 위해서는 패킷 데이터의 오리지널 사이즈 필요
                    // pkt_header.mBodySize 는 압축된 패킷의 사이즈. 오리지널 사이즈를 따로 기입해야 함

                    Byte[] decodeBody = new Byte[pkt_header.streamLength];
					sock_ctx.ReadBody( SocketBuffer, decodeBody, hLen, pkt_header.streamLength );

                    if ( pkt_header.bodyLength > ManagedServer.COMPRESS_PACKET_LENGTH )
                    {
                        Byte[] decompressBody = new Byte[pkt_header.bodyLength];
                        MiniLZO.Decompress( decodeBody, decompressBody );
                        decodeBody = decompressBody;
                    }

                    Object pkt_body = S2SPacketCoder.Decode( pkt_header.code, decodeBody );
                    client.MsgDispatch( pkt_header.code, pkt_body );

					S2SPacketEnum kPacketCode = S2SCommandCommunicator.ParsePacketCode( pkt_header.code );
					if ( S2SPacketEnum.CMD_NONE != kPacketCode )
					{
						this.m_kS2SCommandCommunicator.PushCommand( new GCommand<S2SPacketEnum>( client , kPacketCode , pkt_body ) );
                }

                }
                return BatchResult.Success;
            }
            catch (InvalidOperationException ioe)
            {
                Logger.ExceptionLog(ioe);
                return BatchResult.Fail;
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return BatchResult.Fail;
            }
        }

        private bool C2SRecvPacketProcess(PerSocketContext sock_ctx, Int32 bytesTransferred )
        {
            try
            {
				sock_ctx.IOCtx.m_offset += bytesTransferred;

                Int32 hLen = (Int32)PacketVariables.C2S_PKT_HDR_SIZE;

				Int32 accumulate_read_buf_size = sock_ctx.IOCtx.m_offset; // 수신된 누적 버퍼의 크기
                Int32 offset = 0;             //   복사된 바이트 수

                // 수신된 버퍼의 길이가 해더 보다 크다면 파싱
                Byte[] headerBuf = new Byte[hLen];
                while (accumulate_read_buf_size >= hLen)
                {
					Buffer.BlockCopy( sock_ctx.IOCtx.m_buffer, offset, headerBuf, 0, hLen );

                    // 바디 사이즈를 알 수 없기에 디코딩 해야 한다..
                    C2SPacketHeader header = C2SPacketDecoder.DecodeC2SPacketHeader(headerBuf);
                    if (header == null)
                        throw new InvalidOperationException("Incorrect header type");
					if ( header.mCode <= 0 )
						throw new InvalidOperationException( "Packet Is Error mCode" );

                    Int32 bodyLen = 0;
                    bodyLen = header.mBodySize;
                    Int32 complete_pkt_length = (hLen + bodyLen);

                    // 구성된 패킷의 길이가 해더보다 작다면 오류.. 
                    if (complete_pkt_length < hLen)
                        throw new InvalidOperationException("Incorrect packet length ");

                    //  패킷이 너무 큰 경우 데이터가 완전해 질 때 까지 받아야 하므로 break해서 이어받을 수 잇게.
                    if (complete_pkt_length > accumulate_read_buf_size)
                    {
                        //Logger.DebugLog("Check: ClientConn.RecvComplete() break; complete_pkt_length = {0} > accumulate_read_buf_size = {1}", complete_pkt_length, accumulate_read_buf_size);
                        break;
                    }

                    Byte[] byComplete_Packet = new byte[complete_pkt_length];
                    // 정상적으로 처리가 되었다면. while을 나가기 위해 recvbuflne 줄이기, 처리한 offset값을 설정한다.
					Buffer.BlockCopy( sock_ctx.IOCtx.m_buffer, offset, byComplete_Packet, 0, complete_pkt_length );
                    sock_ctx.AddPacketByPool( byComplete_Packet );

                    accumulate_read_buf_size -= complete_pkt_length;
                    offset += complete_pkt_length;
                }

                if ( accumulate_read_buf_size > 0 )
					Buffer.BlockCopy( sock_ctx.IOCtx.m_buffer, offset, sock_ctx.IOCtx.m_buffer, 0, accumulate_read_buf_size );

				Array.Clear( sock_ctx.IOCtx.m_buffer, accumulate_read_buf_size, sock_ctx.IOCtx.m_buffer.Length - accumulate_read_buf_size );
				sock_ctx.IOCtx.m_offset = accumulate_read_buf_size;

                return true;
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }
        }

        private bool S2SRecvPacketProcess(PerSocketContext sock_ctx, Int32 bytesTransferred )
        {
            try
            {
				sock_ctx.IOCtx.m_offset += bytesTransferred;

                Int32 hLen = (Int32)S2SPacketVariables.S2S_PKT_HDR_SIZE;

				Int32 accumulate_read_buf_size = sock_ctx.IOCtx.m_offset; // 수신된 누적 버퍼의 크기
                Int32 offset = 0;             //   복사된 바이트 수

                // 수신된 버퍼의 길이가 해더 보다 크다면 파싱
                byte[] headerBuf = new byte[hLen];
                while (accumulate_read_buf_size >= hLen)
                {
					Buffer.BlockCopy( sock_ctx.IOCtx.m_buffer, offset, headerBuf, 0, hLen );

                    // 바디 사이즈를 알 수 없기에 디코딩 해야 한다..
                    S2SPacketHeader header = (S2SPacketHeader)S2SPacketDecoder.DecodeS2SPacketHeader(headerBuf);
                    if (header == null)
                        throw new InvalidOperationException("Incorrect header type");

                    int complete_pkt_length = (hLen + header.streamLength);

                    // 구성된 패킷의 길이가 해더보다 작다면 오류.. 
                    if (complete_pkt_length < hLen)
                        throw new InvalidOperationException("Incorrect packet length ");

                    //  패킷이 너무 큰 경우 데이터가 완전해 질 때 까지 받아야 하므로 break해서 이어받을 수 잇게.
                    if (complete_pkt_length > accumulate_read_buf_size)
                    {
						Logger.DebugLog( "Check: ClientConn.RecvCompleteServerCmd() break; complete_pkt_length = {0} > accumulate_read_buf_size = {1}, bufferLength = {2}", complete_pkt_length, accumulate_read_buf_size, sock_ctx.IOCtx.m_buffer.Length );
						if( complete_pkt_length > sock_ctx.IOCtx.m_buffer.Length )
							Logger.ErrLog( "Over PacketSize. Complete_pkt_length = {0}, buffer_length = {1}", complete_pkt_length, sock_ctx.IOCtx.m_buffer.Length );
                        break;
                    }

                    Byte[] byComplete_Packet = new byte[complete_pkt_length];
                    // 정상적으로 처리가 되었다면. while을 나가기 위해 recvbuflne 줄이기, 처리한 offset값을 설정한다.
					Buffer.BlockCopy( sock_ctx.IOCtx.m_buffer, offset, byComplete_Packet, 0, complete_pkt_length );
                    sock_ctx.AddPacketByPool( byComplete_Packet );

                    accumulate_read_buf_size -= complete_pkt_length;
                    offset += complete_pkt_length;
                }

                if ( accumulate_read_buf_size > 0 )
					Buffer.BlockCopy( sock_ctx.IOCtx.m_buffer, offset, sock_ctx.IOCtx.m_buffer, 0, accumulate_read_buf_size );

				Array.Clear( sock_ctx.IOCtx.m_buffer, accumulate_read_buf_size, sock_ctx.IOCtx.m_buffer.Length - accumulate_read_buf_size );
				sock_ctx.IOCtx.m_offset = accumulate_read_buf_size;

                return true;
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }
        }
        #endregion

		public void SetC2SCommandCommunicator(C2SCommandCommunicator kCommunicator)
		{
			this.m_kC2SCommandCommunicator = kCommunicator;
		}

		public C2SCommandCommunicator GetC2SCommandCommunicator()
		{
			return this.m_kC2SCommandCommunicator;
		}

		public void SetS2SCommandCommunicator(S2SCommandCommunicator kCommunicator)
		{
			this.m_kS2SCommandCommunicator = kCommunicator;
    }

		public S2SCommandCommunicator GetS2SCommandCommunicator()
		{
			return this.m_kS2SCommandCommunicator;
		}
    }

    #region [Define ClientEvent]
    public delegate void ClientConnectionEvent(UInt32 id, bool success, EndPoint address, Object token);
    #endregion
    

    public abstract class ManagedServer
    {
        static public readonly Int32 COMPRESS_PACKET_LENGTH = 1024;

        private Int32 m_serverGroupID;
        public Int32 ServerGroupID
        {
            get { return m_serverGroupID; }
            set { m_serverGroupID = value; }
        }
        // 게임서버 고유아이디
        private UInt16 m_serverId;
        public UInt16 ServerID
        {
            get { return m_serverId; }
            set { m_serverId = value; }
        }

        private Int32 m_serialID = 0;
        
        protected Object m_monitorLock = new Object();
        private Monitor m_monitor = null;
        private EventWaitHandle m_ewh = new EventWaitHandle(false, EventResetMode.AutoReset);

        private BMServerType m_serverType = BMServerType.None;
        private ConnectionManager m_clientConnManager = null;
        private ConnectionManager m_proxyConnManager = null;

		private ClientContextManager m_kClientContenxtManager;
		private ServerContextManager m_kServerContenxtManager;     

        protected bool m_isRun = true;
        protected AtomicInt m_disconnectAll = new AtomicInt(0);

        protected Object m_callbackObj = new Object();
        protected List<KMCommand> m_frontCallbackCmd = new List<KMCommand>();
        protected List<KMCommand> m_backEndCallbackCmd = new List<KMCommand>();

        protected UInt16 m_seedClientId = 0;
        protected IoAcceptor m_listner = null;

        private PacketDelegateManager m_packet_delegate_manager = null;
        private Thread m_mainProcessThread = null;

        private Int64 m_elapsed_time_ns = DateTime.Now.Ticks;
        private Int64 m_current_time_ns = DateTime.Now.Ticks;
        private Int64 m_lastIoPresentTime = DateTime.Now.Ticks;
        private readonly Int64 IO_PRESENT_INTERVAL = TimeSpan.TicksPerMillisecond * 50;

        private Int32 m_readSockBufCapacity = 0;
        private Int32 m_writeSockBufCapacity = 0;
        private Int32 m_ioCtxBufCapacity = 0;

		private readonly Int32 UpdateFrameCheckMS = 5000;

		Regulator m_DBQueueProcessTime;
		Regulator m_reqMainUpdater;
		Regulator m_reqIOPresent;
		Regulator m_reqMainUpdateSleep;
		Regulator m_reqProfileUpdater;
		
		List<Int64>	m_liDBQueryTick = new List<long>();
		List<Int64>	m_liIORecvTick = new List<long>();
		List<Int64>	m_liPacketCmdTick = new List<long>();
        List<Int64>	m_liHttpCmdTick = new List<long>();
		List<Int64>	m_liGameUpdateTick = new List<long>();
		List<Int64>	m_liIOSendTick = new List<long>();

		public Int32 GameUpdateCnt
		{
			get;	set;
		}

		public Int32 MainProcessUpdateCnt
		{
			get;
			set;
		}

		public Regulator MainUpdater
		{
			get { return m_reqMainUpdater; }
		}

		public HttpSocketManager HttpManager { get; private set; }

        protected LogicEntry m_logicEntry;
        public LogicEntry Entry
        {
            get { return m_logicEntry; }
        }


        public event ClientConnectionEvent ConnectionEvent;

        private object m_kMainProcessLock       = new object();
        private object m_kClientSockCtxPoolLock = new object();
        private object m_kServerSockCtxPoolLock = new object();


        #region [Property]
        public BMServerType ServerType
        {
            get { return m_serverType; }
        }
        public ConnectionManager ClientConnManager
        {
            get { return m_clientConnManager; }
        }

        public ConnectionManager ProxyConnManager
        {
            get { return m_proxyConnManager; }
        }

		protected ClientContextManager ClientCtxMgr
		{
			get { return m_kClientContenxtManager;  }
		}

		protected ServerContextManager ServerCtxMgr
		{
			get { return m_kServerContenxtManager; }
		}


        protected Int64 ElapsedTickNS
        {
            get { return this.m_elapsed_time_ns; }
        }

        public PacketDelegateManager PktDelegateMgr
        {
            get { return this.m_packet_delegate_manager; }
        }
        #endregion  // [Property]


        public static void SafeClose(Socket socket)
        {
            if (socket == null)
                return;

            if (!socket.Connected)
                return;

            try
            {
				socket.Shutdown( SocketShutdown.Send );
            }
            catch
            {
            }

            try
            {
				//socket.Close();
            }
            catch
            {
            }
        }

        public ManagedServer(BMServerType type, Int32 readSockBufCapacity, Int32 writeSockBufCapacity, Int32 ioCtxBufCapacity)
        {
            m_serverType = type;
            m_readSockBufCapacity = readSockBufCapacity;
            m_writeSockBufCapacity = writeSockBufCapacity;
            m_ioCtxBufCapacity = ioCtxBufCapacity;

            m_packet_delegate_manager = new PacketDelegateManager();

            m_monitor = new Monitor(this);
            HttpManager = new HttpSocketManager(this, LuaScriptManager.INSTANCE.m_HttpWorkerPoolSize, LuaScriptManager.INSTANCE.m_bHttpProfile);

			//SetThreads( 8, 8 );
            KMLogicGateway.INSTANCE.Init(this);

			m_reqMainUpdater = new Regulator( 50 );
			m_reqIOPresent = new Regulator( 20 );
			m_reqMainUpdateSleep = new Regulator( 10 );
			m_reqProfileUpdater = new Regulator( LuaScriptManager.INSTANCE.m_nUpdateFrameCheckMS );
			m_DBQueueProcessTime = new Regulator( LuaScriptManager.INSTANCE.m_nDBQueueMSTick );

			Logger.InfoLog( "Server {0} : [Sock ReadBufSize] {1}\n[WriteBufSize] {2}\n[IoBufSize] {3}\n",
			  type,
			  readSockBufCapacity,
			  writeSockBufCapacity,
			  ioCtxBufCapacity );

            m_mainProcessThread = new Thread(new ThreadStart(this.Run));

			m_kClientContenxtManager = new ClientContextManager( this, ioCtxBufCapacity );
			m_kServerContenxtManager = new ServerContextManager( this, ioCtxBufCapacity );
        }


        public void InitProxyPool()
        {
            if (m_proxyConnManager != null)
                m_proxyConnManager = new ConnectionManager();
        }

        public void InitClientPool()
        {
            if (m_clientConnManager == null)
                m_clientConnManager = new ConnectionManager();
        }


        public abstract bool Init();
        public abstract void CleanUp();
        public abstract void Dump();


        public bool ProcessThreadStart()
        {
            try
            {
                m_mainProcessThread.Start();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

            return true;
        }

        public virtual bool ListenStart(IPEndPoint endPoint, Int32 backlog)
        {
            Logger.InfoLog("ListenStart {0} endPoint {1}:{2} backlog{3}", this.ToString(), endPoint.Address, endPoint.Port, backlog);
            return true;
        }

        // 메인쓰레드 외에 다른 스레드에서 호출 될 수 있다.
        public void DisconnectAll()
        {
            // seed 수정, atomic으로
            m_disconnectAll.CasOn();
        }

        protected virtual void OnBegin()
        {
            this.UserIoPresent();
        }

        protected virtual void OnExecute()
        {
            this.MainProcessThread();
        }

        protected virtual void OnEnd()
        {
            this.MainProcessThreadEnd();
        }

        protected virtual void Run()
        {
            try
            {
                this.OnBegin();
                this.OnExecute();
                this.OnEnd();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

        }

        public UInt32 CreateSerialKey()
        {
            Int32 frontMask = m_serverId << 24;
            return (UInt32)(frontMask | Interlocked.Increment(ref m_serialID));
        }

        // 소켓 버퍼에 저장
        protected void RecvPost( PerSocketContext sock_ctx )
        {
            try
            {
                if (sock_ctx == null)
                    throw new InvalidOperationException("sock ctx is Null");

                AsyncCallback rc = readResult =>
                {
					SocketError eSocketError = SocketError.Success;
                    Socket rawSock = null;
					PerSocketContext kSocketCtx = null;
                    try
                    {
						kSocketCtx = (PerSocketContext)readResult.AsyncState;
                        rawSock = kSocketCtx.Sock;

						Int32 bytesTransferred = rawSock.EndReceive( readResult, out eSocketError );
                        if (bytesTransferred > 0)
                        {
                            ISocket client = null;
                            lock (sock_ctx)
                            {
                                client = sock_ctx.Client;
                            }

                            if (client == null)
                                throw new InvalidOperationException("Not Found Client");

                            if (false == client.RecvPacket( sock_ctx, bytesTransferred ))
                                throw new InvalidOperationException("Fail Recv Complete ");


                            sock_ctx.ReleaseVar(LifeTimeID.RECV);

                            RecvPost( sock_ctx );
                        }
                        else
                        {
							if ( null != sock_ctx.Client && BMServerType.None < sock_ctx.Client.DestServerType )
							{
								Logger.ErrLog( "SocketClose. ErrorCode = {0}", eSocketError.ToString() );
								if ( null != kSocketCtx )
									Logger.InfoLog( "Check: EndReceive() bytesTransferred = 0 : ServerType = {0}, Offset = {1}, RemainBufLength = {2} ", sock_ctx.Client.DestServerType, kSocketCtx.IOCtx.m_offset, kSocketCtx.IOCtx.RemainBufLength );
							}

                            // 연결이 종료된 소켓 close 처리.
                            ManagedServer.SafeClose(rawSock);

                            sock_ctx.ReleaseVar(LifeTimeID.RECV);
                            OnClose(sock_ctx);
                            return;
                        }

                    }
                    catch (ObjectDisposedException ode)
                    {
						// 서버에서 클라이언트 소켓 강제 종료 했을때 들어옴.
						// 로그 찍을 필요가 있나?
                        ManagedServer.SafeClose(rawSock);
                        if ( sock_ctx != null)
                        {
                            sock_ctx.ReleaseVar(LifeTimeID.RECV);
                            OnClose(sock_ctx);
                        }
                    }
                    catch (System.Net.Sockets.SocketException se)
                    {
#if _NATRACELOG
                        //Logger.InfoLog("Check: System.Net.Sockets.SocketException :: RecvPost() 1");
#endif
						if ( ( SocketError )se.ErrorCode == SocketError.ConnectionAborted ||
							( SocketError )se.ErrorCode == SocketError.ConnectionReset ||
							( SocketError )se.ErrorCode == SocketError.TimedOut )
						{
							// disconnect process
						}
						else
						{
							Logger.SocketExceptionLog( se );
							Logger.ErrLog( "Socket Error Number = {0}", eSocketError );
						}

                        ManagedServer.SafeClose(rawSock);
                        sock_ctx.ReleaseVar(LifeTimeID.RECV);
                        OnClose(sock_ctx, "EndReceive SE " + se.Message);
                    }
					catch ( System.ArgumentNullException ane )
					{
						Logger.ExceptionLog( ane );
						Logger.ErrLog( "Socket Error Number = {0}", eSocketError + " : ErrorParam : " + ane.ParamName );

						ManagedServer.SafeClose( rawSock );
						sock_ctx.ReleaseVar( LifeTimeID.RECV );
						OnClose( sock_ctx, "EndReceive ANE " + ane.Message + " : ErrorParam : " + ane.ParamName );
					}
                    catch (System.ArgumentException ae)
                    {
						// 현재 콜백 인자로 넘어온 io_ctx가 널이다!!!!
                        Logger.ExceptionLog(ae);
						Logger.ErrLog( "Socket Error Number = {0}", eSocketError + " : ErrorParam : " + ae.ParamName );

                        ManagedServer.SafeClose(rawSock);
                        sock_ctx.ReleaseVar(LifeTimeID.RECV);
						OnClose( sock_ctx, "EndReceive AE " + ae.Message + " : ErrorParam : " + ae.ParamName);
                    }
                    catch (InvalidOperationException ioe)
                    {
                        Logger.ExceptionLog(ioe);
						Logger.ErrLog( "Socket Error Number = {0}", eSocketError );

                        ManagedServer.SafeClose(rawSock);
                        sock_ctx.ReleaseVar(LifeTimeID.RECV);
                        OnClose(sock_ctx, "EndReceive IOE " + ioe.Message);
                    }
                    catch (System.Exception ex)
                    {
                        Logger.ExceptionLog(ex);
						Logger.ErrLog( "Socket Error Number = {0}", eSocketError );

                        ManagedServer.SafeClose(rawSock);
                        sock_ctx.ReleaseVar(LifeTimeID.RECV);
                        OnClose(sock_ctx, "EndReceive EX " + ex.Message);
                    }
                };

				if( sock_ctx.IOCtx.RemainBufLength <= 0 )
					Logger.ErrLog( "Error Buffer RemainSize = {0}, ServerType = {1}", sock_ctx.IOCtx.RemainBufLength, sock_ctx.Client.DestServerType );

				sock_ctx.Sock.BeginReceive( sock_ctx.IOCtx.m_buffer,
											sock_ctx.IOCtx.m_offset,
											sock_ctx.IOCtx.RemainBufLength,
											SocketFlags.None,
											rc,
											sock_ctx );

				sock_ctx.AddRefVar( LifeTimeID.RECV );

				//if ( sock_ctx.SOCK.Connected == false )
				//{
				//	ManagedServer.SafeClose( sock_ctx.SOCK );
				//	sock_ctx.ReleaseVar( LifeTimeID.RECV );
				//	OnClose( sock_ctx, io_ctx, "Connect Fail" );
				//}

            }
            catch (System.Net.Sockets.SocketException se)
            {
                //WSAECONNRESET, the other side closed impolitely 
                if (se.ErrorCode == (Int32)SocketError.ConnectionReset ||
                   ((se.ErrorCode != (Int32)SocketError.Interrupted) &&
                   (se.ErrorCode != (Int32)SocketError.ConnectionAborted)))
                {
                    // Complete the disconnect request.
                    //                     String remoteIP =
                    //                       ((IPEndPoint)sock_ctx.SOCK.RemoteEndPoint).Address.ToString();
                    //                     String remotePort =
                    //                       ((IPEndPoint)sock_ctx.SOCK.RemoteEndPoint).Port.ToString();
                    //                     this.owner.DisconnectClient(remoteIP, remotePort);
                    //                     handler.Close();
                    //                     handler = null;
                }
                else
                {
                    Logger.SocketExceptionLog(se);
                }

                ManagedServer.SafeClose(sock_ctx.Sock);
                sock_ctx.ReleaseVar(LifeTimeID.RECV);
                OnClose(sock_ctx, "BeginReceive SE " + se.Message);
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
                ManagedServer.SafeClose(sock_ctx.Sock);
                sock_ctx.ReleaseVar(LifeTimeID.RECV);
                OnClose(sock_ctx, "EndReceive EX " + ex.Message);
            }
        }

        protected virtual void OnClose(PerSocketContext sock_ctx, String reason = "")
        {
            try
            {
                if (sock_ctx == null)
                    return;

                if (sock_ctx.IsWaitClose.IsOn())
                    return;

                if (sock_ctx.Sock == null)
                    return;

                sock_ctx.IsWaitClose.CasOn();

				if ( reason != "" )
					Logger.ErrLog( reason );

            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        private void MainProcessThreadFPS()
        {
            m_monitor.Update();
            m_ewh.WaitOne(1);
        }

        private void MainProcessThreadEnd()
        {
			ClientCtxMgr.MainProcessEnd();
			ServerCtxMgr.MainProcessEnd();
        }

        #region [EventHandler]
        internal void OnConnectionEvent(UInt32 id, bool success, EndPoint address, Object token)
        {
            if (this.ConnectionEvent != null)
                this.OnConnectionEvent(id, success, address, token);
        }
        #endregion



		private void SystemIoReceiver()
		{
			try
			{
				ServerCtxMgr.UpdateIoReceive();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}
		}

        protected void UserIoReceiver()
        {
            try
            {
				ClientCtxMgr.UpdateIoReceive();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }


		private void SystemIoPresent()
		{
			try
			{
				ServerCtxMgr.UpdateIoPresent();
			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}
		}

        protected void UserIoPresent()
        {
            try
            {
				ClientCtxMgr.UpdateIoPresent();
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        private void OnTimer()
        {
            Int64 current_time_ns = DateTime.Now.Ticks;
            m_elapsed_time_ns = current_time_ns - m_current_time_ns;
            m_current_time_ns = current_time_ns;
        }

        protected virtual bool OnGameUpdate()
        {
			GameUpdateCnt++;
			UpdateServerSockContext( this.m_elapsed_time_ns );
			UpdateClientSockContext( this.m_elapsed_time_ns );

			return true;
        }

        // 비동기 결과 처리를 작업할 객체를 가져온다.
        public virtual bool GetUsingAsyncWorker(UInt32 ownSocketID, out ISocket socket)
        {
            if (ClientConnManager.Find(ownSocketID, out socket) == false)
            {
                //Logger.WarnLog("Invalid Client ID {0} Work is Skip", ownSocketID);
                return false;
            }

            return true;
        }



        protected virtual void UpdateServerSockContext(Int64 elapsedTick)
        {
            try
            {
				ServerCtxMgr.UpdateLogic( elapsedTick );
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

            try
            {
                if (m_proxyConnManager != null)
                    m_proxyConnManager.Update(elapsedTick);
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        protected virtual void UpdateClientSockContext(Int64 elapsedTick)
        {
            try
            {
				if( m_disconnectAll.IsOn() )
				{
					ClientCtxMgr.DisconnectAll();

					m_disconnectAll.CasOff();
				}
				else
				{
					ClientCtxMgr.UpdateLogic( elapsedTick );
				}
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

            try
            {
                if (m_clientConnManager != null)
                    m_clientConnManager.Update(elapsedTick);
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }

        private void MainProcessThread()
        {
            try
            {
                while (m_isRun)
                {
                    OnTimer();

					if( false == m_reqMainUpdateSleep.IsUpdate() )
					{
						//Thread.Sleep( 1 );
						continue;
					}

					MainProcessUpdateCnt++;
					
					Int64 nBeginTick = DateTime.Now.Ticks;
					ProcessCallback();
					Int64 nEndTick = DateTime.Now.Ticks;
					m_liDBQueryTick.Add( nEndTick - nBeginTick );

					nBeginTick = DateTime.Now.Ticks;
                    // receive
                    SystemIoReceiver();
                    UserIoReceiver();
					nEndTick = DateTime.Now.Ticks;
					m_liIORecvTick.Add( nEndTick - nBeginTick );


					nBeginTick = DateTime.Now.Ticks;
					ExecuteCommand();
					nEndTick = DateTime.Now.Ticks;
					m_liPacketCmdTick.Add( nEndTick - nBeginTick );

					nBeginTick = DateTime.Now.Ticks;
					nBeginTick = DateTime.Now.Ticks;
					HttpManager.Update();
					nEndTick = DateTime.Now.Ticks;
					m_liHttpCmdTick.Add( nEndTick - nBeginTick );

					nBeginTick = DateTime.Now.Ticks;
					bool IsGameUpdate = OnGameUpdate();
					nEndTick = DateTime.Now.Ticks;
					m_liGameUpdateTick.Add( nEndTick - nBeginTick );

                    // send
					//if( true == IsGameUpdate || m_reqIOPresent.IsUpdate() )
                    {
						nBeginTick = DateTime.Now.Ticks;
                        UserIoPresent();
                        SystemIoPresent();
						nEndTick = DateTime.Now.Ticks;
						m_liIOSendTick.Add( nEndTick - nBeginTick );

                        // 제거 예약된 클라이언트 처리
                        // 이곳에 둔 이유는 보내야 할 패킷은 모두 보내고 그 이후에 제거 처리
                        DeleteClientReserved();
                    }

                    MainProcessThreadFPS();

					if(m_reqProfileUpdater.IsUpdate())
					{
						Int32 nFrameCheckSec = LuaScriptManager.INSTANCE.m_nUpdateFrameCheckMS / 1000;

						Logger.InfoLog( " => FrameCheck : UpdateTick = {0} Sec, GameUpdateCnt = {1} Frame, MainProcessUpdateCnt = {2} Frame, SocketCount : {3}"
							, nFrameCheckSec
							, GameUpdateCnt / nFrameCheckSec
							, MainProcessUpdateCnt / nFrameCheckSec
							, ClientCtxMgr.GetUsedContextCount() );
						
						Logger.InfoLog( " => DBQuery = {0}/{1}, IORecv = {2}/{3}, CmdPacket = {4}/{5}, HttpCmd = {6}/{7}, GameUpdate = {8}/{9}, IOSend = {10}/{11}"
							, m_liDBQueryTick.Count == 0 ? 0 : ( m_liDBQueryTick.Sum() / m_liDBQueryTick.Count ) / TimeSpan.TicksPerMillisecond, m_liDBQueryTick.Sum() / TimeSpan.TicksPerMillisecond
							, m_liIORecvTick.Count == 0 ? 0 : ( m_liIORecvTick.Sum() / m_liIORecvTick.Count ) / TimeSpan.TicksPerMillisecond, m_liIORecvTick.Sum() / TimeSpan.TicksPerMillisecond
							, m_liPacketCmdTick.Count == 0 ? 0 : ( m_liPacketCmdTick.Sum() / m_liPacketCmdTick.Count ) / TimeSpan.TicksPerMillisecond, m_liPacketCmdTick.Sum() / TimeSpan.TicksPerMillisecond
							, m_liHttpCmdTick.Count == 0 ? 0 : ( m_liHttpCmdTick.Sum() / m_liHttpCmdTick.Count ) / TimeSpan.TicksPerMillisecond, m_liHttpCmdTick.Sum() / TimeSpan.TicksPerMillisecond
							, m_liGameUpdateTick.Count == 0 ? 0 : ( m_liGameUpdateTick.Sum() / m_liGameUpdateTick.Count ) / TimeSpan.TicksPerMillisecond, m_liGameUpdateTick.Sum() / TimeSpan.TicksPerMillisecond
							, m_liIOSendTick.Count == 0 ? 0 : ( m_liIOSendTick.Sum() / m_liIOSendTick.Count ) / TimeSpan.TicksPerMillisecond, m_liIOSendTick.Sum() / TimeSpan.TicksPerMillisecond
							);

						if( LuaScriptManager.INSTANCE.m_bCommandProfile )
						{
							CommandProfile();
						}

						HttpManager.ProfilePrint();

						m_liDBQueryTick.Clear();
						m_liIORecvTick.Clear();
						m_liPacketCmdTick.Clear();
						m_liHttpCmdTick.Clear();
						m_liGameUpdateTick.Clear();
						m_liIOSendTick.Clear();

						GameUpdateCnt = 0;
						MainProcessUpdateCnt = 0;
					}
                }
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
				Logger.FatalLog( "ManagedServer.MainProcessThread @ main thread stopped!! you must restart this process!!!" );
            }
        }

        internal void AddCallback(KMCommand cmd)
        {
            lock (m_callbackObj)
            {
                m_frontCallbackCmd.Add(cmd);
            }
        }

        private void ProcessCallback()
        {
// 			if( false == m_DBQueueProcessTime.IsUpdate() )
// 				return;

            lock (m_callbackObj)
            {
                if (m_frontCallbackCmd.Count == 0)
                    return;

                m_backEndCallbackCmd.AddRange(m_frontCallbackCmd);
                m_frontCallbackCmd.Clear();
            }

            foreach (var cmd in m_backEndCallbackCmd)
            {
                CallbackComplete(cmd);
            }
            m_backEndCallbackCmd.Clear();
        }

        private void CallbackComplete(KMCommand cmd)
        {
            KMLogic.INSTANCE.GetLogicEntry.OnProcess(cmd);
            switch (cmd.TypeID)
            {
                case KMCommand.Type.DB:
                    KMLogic.INSTANCE.GetLogicEntry.RecallDBCmd(cmd as KMDBCommand);
                    break;

            }
        }

        public ISocket OnClientAccept(Socket allocSock, CreateSockeDelegate creator, Object token)
        {
            PerSocketContext sock_ctx = null;
            PerIoContext io_ctx = null;

            try
            {
                // 1. 접속 허용 인원 체크.
                // 2. sock check
                if (allocSock == null)
                {
                    return null;
                }

                allocSock.NoDelay = true;
                allocSock.LingerState = new System.Net.Sockets.LingerOption(true, 0);
				allocSock.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );

                // clinet 할당. (only instance creation)
                ISocket allocClient = creator();
                if (allocClient == null)
                    throw new InvalidOperationException("Create Fail Client");

				sock_ctx = ClientCtxMgr.GetSocketContext( allocSock );
                if (sock_ctx == null)
                    throw new InvalidOperationException("sock pool is empty");

                if (false == allocClient.Init(this, sock_ctx))
                    throw new InvalidOperationException(allocClient.ToString() + " Init Fail, sock Destroy");

                //Logger.InfoLog("Allocate Handle {0}", allocSock.Handle);
                //OnConnectionEvent(allocClient.ID, true, allocClient.RemoteEndPoint, token);

                RecvPost(sock_ctx);
                return allocClient;
            }
            catch (SocketException ske)
            {
                // accept 중 소켓이 강제 종료 된 경우.
                if (ske.ErrorCode != (Int32)SocketError.ConnectionReset)
                {
                    Logger.SocketExceptionLog(ske);
                    OnClose(sock_ctx, "OnAccept Socket Exception ");
                }
            }
            catch (InvalidOperationException ie)
            {
                Logger.ExceptionLog(ie);
                OnClose(sock_ctx, "OnAccept IE " + ie.Message);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                OnClose(sock_ctx, "OnAccept EX " + ex.Message);

            }
            return null;
        }

        public ISocket OnProxyAccept(Socket allocSock, CreateSockeDelegate creator, Object token )
        {
            PerSocketContext sock_ctx = null;
            PerIoContext io_ctx = null;

            try
            {
                allocSock.NoDelay = true;

                // 1. 접속 허용 인원 체크.
                // 2. sock check
                if (allocSock == null)
                {
                    return null;
                }
          
                // clinet 할당. (only instance creation)
                ISocket allocClient = creator();
                if (allocClient == null)
                    throw new InvalidOperationException("Create Fail Client");

				sock_ctx = ServerCtxMgr.GetSocketContext( allocSock );
                if (sock_ctx == null)
                    throw new InvalidOperationException("sock pool is empty");

                // sock에 data set 을 위해 lock을 걸어준다.
                if (false == allocClient.Init(this, sock_ctx))
                    throw new InvalidOperationException(allocClient.ToString() + " Init Fail, sock Destroy");

                

                //OnConnectionEvent(allocClient.ID, true, allocClient.RemoteEndPoint, token);

                RecvPost(sock_ctx);
                return allocClient;
            }
            catch (SocketException ske)
            {
                // accept 중 소켓이 강제 종료 된 경우.
                if (ske.ErrorCode != (Int32)SocketError.ConnectionReset)
                {
                    Logger.SocketExceptionLog(ske);
                    OnClose(sock_ctx, "OnAccept Socket Exception ");
                }
            }
            catch (InvalidOperationException ie)
            {
                Logger.ExceptionLog(ie);
                OnClose(sock_ctx, "OnAccept IE " + ie.Message);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                OnClose(sock_ctx, "OnAccept EX " + ex.Message);

            }
            return null;
        }


        public void GetProxy(UInt32 requestClientID, BMServerType type, out ISocket proxy)
        {
            ServerConnManager.INSTANCE.FindServerConnectionByType(requestClientID, type, out proxy);
        }

        public bool MakeServerConnection(IConnector connector, Int32 makeCount )
        {
            try
            {
                for (Int32 i = 0; i < makeCount; ++i)
                {
                    Socket sock = null;
                    while (true)
                    {
                        sock = connector.CreateAndConnectSocket();

                        if (sock.Connected)
                            break;

                        sock = null;
                        System.Threading.Thread.Sleep(100);
                        continue;
                    }

                    ISocket proxy = connector.MakeConnection();
                    if (proxy == null)
                        throw new InvalidOperationException("Fail Make proxy !!!!");

					PerSocketContext sock_ctx = ServerCtxMgr.GetSocketContext( sock );
					if( sock_ctx == null )
						throw new InvalidOperationException( "Fail sock ctx is null" );

                    lock (sock_ctx)
                    {
                        if (proxy.Init(this, sock_ctx) == false)
                            throw new InvalidOperationException("Fail Proxy Init ");

                    }

                    RecvPost(sock_ctx);

                }
                return true;
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return false;
        }

        public bool RegistProxy(Socket allocateSock, IConnector connector, out ISocket proxy)
        {
            try
            {
                proxy = connector.MakeConnection();
                if (proxy == null)
                    throw new InvalidOperationException("Fail Make proxy !!!!");

				PerSocketContext sock_ctx = ServerCtxMgr.GetSocketContext( allocateSock );
				if( sock_ctx == null )
					throw new InvalidOperationException( "Fail sock ctx is null" );

                lock (sock_ctx)
                {
                    if (proxy.Init(this, sock_ctx) == false)
                        throw new InvalidOperationException("Fail Proxy Init ");
                }

                RecvPost(sock_ctx);

				if ( m_clientConnManager != null )
					if ( false == m_clientConnManager.Add( proxy.ClientID, proxy ) )
						Logger.ErrLog( "ManagedServer.RegistProxy() Fail." );

                return true;
            }
            catch (System.Exception ex)
            {
                proxy = null;
                Logger.ExceptionLog(ex);
            }
            return false;
        }

        protected ISocket MakeServerConnection(IConnector connector)
        {
            try
            {
                Socket sock = null;
                while (true)
                {
                    sock = connector.CreateAndConnectSocket();

                    if (sock.Connected)
                        break;

                    sock = null;
                    System.Threading.Thread.Sleep(100);
                    continue;
                }

                ISocket proxy = connector.MakeConnection();
                if (proxy == null)
                    throw new InvalidOperationException("Fail Make proxy !!!!");

				PerSocketContext sock_ctx = ServerCtxMgr.GetSocketContext( sock );
				if( sock_ctx == null )
					throw new InvalidOperationException( "Fail sock ctx is null" );

                lock (sock_ctx)
                {
                    if (proxy.Init(this, sock_ctx) == false)
                        throw new InvalidOperationException("Fail Proxy Init ");

                }
                RecvPost(sock_ctx);
                return proxy;
            }
            catch (InvalidOperationException ioe)
            {
                Logger.ErrLog("Make Server Connection {0}" + ioe.Message);
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return null;
        }

		public virtual void CreateDB( DBConnectionInfo[] kDBConnectionInfoArray, Int32 WorkerPoolSize, Int32 WorkerPerThroughput )
        {
            Logger.InfoLog("DB::Cehck Verify");
			Logger.InfoLog( "CreateDB - WorkerPoolSize : {0}, WorkerPerThroughput : {1}", WorkerPoolSize, WorkerPerThroughput );
            if(0 == kDBConnectionInfoArray.Length)
            {
                Logger.ErrLog("{0} is Not Found DBConnection", this.ToString());
                return;
            }


            Logger.InfoLog("DB::Cehck Infomation");
            foreach(var info in kDBConnectionInfoArray)
            {
                Logger.InfoLog("Make DB name={0}, id={1}, ip={2}", info.dbName, info.id, info.ip);
            }

            Logger.InfoLog("DB::Create");

            if(null != KMLogic.INSTANCE.GetLogicEntry)
            {
				if( KMLogic.INSTANCE.GetLogicEntry.MakeDB( kDBConnectionInfoArray, WorkerPoolSize, WorkerPerThroughput ) == false )
                {
                    Logger.ErrLog("{0} Make DB Fail", this.ToString());
                    return;
                }
            }

            Logger.InfoLog("DB Complete");
        }

		public void CreateServerConfig( ListenPort[] _aListenPort, ConnectAddress[] _aConnectAddress )
		{
            Logger.InfoLog( "Config::Cehck Verify" );
            Logger.InfoLog( "Server IP {0}", NetworkHelper.GetIPv4(ServerType) );

			if( null != _aListenPort )
                TryOpenListen( _aListenPort );
            if( null != _aConnectAddress)
                TryConnectToServer( _aConnectAddress );
        }

		public void SetC2SCommandCommunicator(C2SCommandCommunicator kCommunicator)
		{
			if (null == this.m_packet_delegate_manager)
			{
				return;
			}

			this.m_packet_delegate_manager.SetC2SCommandCommunicator( kCommunicator );
		}

		public void SetS2SCommandCommunicator(S2SCommandCommunicator kCommunicator)
		{
			if (null == this.m_packet_delegate_manager)
			{
				return;
			}

			this.m_packet_delegate_manager.SetS2SCommandCommunicator( kCommunicator );
		}

		protected void ExecuteCommand()
		{
			if ( null == this.m_packet_delegate_manager )
			{
				return;
			}

			this.m_packet_delegate_manager.GetS2SCommandCommunicator().Run();
			this.m_packet_delegate_manager.GetC2SCommandCommunicator().Run();
		}

        public virtual void FirstProcess()
        { 
        }

        protected abstract void TryOpenListen( ListenPort[] _aListenPort );
        protected abstract void TryConnectToServer( ConnectAddress[] _aConnectAddress );

        public virtual bool Open()
        {
            m_listner.Open();
            return true;
        }

        protected virtual void DeleteClientReserved()
        {
        }

		public void SetThreads( Int32 nMaxThreads, Int32 nMinThreads )
		{
			// 두 값중에 하나라도 0이라면 CLR에서 자동으로 설정되도록 한다.
			if( nMaxThreads == 0 || nMinThreads == 0 )
				return;

			int workerThreads, cmpThreads;

			ThreadPool.GetMaxThreads( out workerThreads, out cmpThreads );
			if( workerThreads < nMaxThreads )
			{
				workerThreads = nMaxThreads;
			}
			ThreadPool.SetMaxThreads( workerThreads, cmpThreads );

			ThreadPool.GetMinThreads( out workerThreads, out cmpThreads );
			if( workerThreads < nMinThreads )
			{
				workerThreads = nMinThreads;
			}
			ThreadPool.SetMinThreads( workerThreads, cmpThreads );

		}

		public void CommandProfile()
		{
			m_packet_delegate_manager.GetC2SCommandCommunicator().PrintProfile();
		}
    }

}
