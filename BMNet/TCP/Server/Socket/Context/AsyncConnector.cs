﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMNetwork.Managed;
using System.Net.Sockets;
using System.Net;

namespace BMNetwork
{
    public abstract class AsyncConnector : ServerConnector
    {
        
        
        private SocketAsyncEventArgs m_connectedEvent = new SocketAsyncEventArgs();
        public AsyncConnector(ManagedServer ownerServer)
            : base(ownerServer)
        {

        }

        public override bool Init(string ip, int port)
        {
            if (base.Init(ip, port) == false)
                return false;

            m_connectedEvent.RemoteEndPoint = m_endPoint;
            m_connectedEvent.Completed += new EventHandler<SocketAsyncEventArgs>(OnConnectEvent);
            return true;
        }

        public override bool TryConnect(Socket allocatSock, ProxyController controller)
        {
            m_connectedEvent.UserToken = (Object)controller;
            return allocatSock.ConnectAsync(m_connectedEvent);
        }

        public void OnConnectEvent(object sender, SocketAsyncEventArgs e)
        {
            ProxyController controller = (ProxyController)e.UserToken;
            lock ( controller.ConnectLock )
            {
                if ( e.SocketError != SocketError.Success )
                {
                    controller.ReTryConnect();
					Logger.ErrLog( "OnConnectEvent SocketError = {0}. So, Controller Try ReConnect", e.SocketError );
                    return;
                }

                ISocket proxy = null;
                if ( Owner.RegistProxy( e.ConnectSocket, this, out proxy ) == false )
                {
                    Logger.ErrLog( "OnConnectEvent ReigstProxy Fail" );
                    return;
                }
                controller.Activate( proxy );
            }
        }

        public override ISocket MakeConnection()
        {
            throw new NotImplementedException();
        }

//                 try
//                 {
//                     return new GameProxy(m_owner.CreateSerialKey(), BMServerType.GAME);
//                 }
//                 catch (Exception ex)
//                 {
//                     Logger.ExceptionLog(ex);
//                     return null;
//                 }
    }
}
