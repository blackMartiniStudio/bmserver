﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Collections.Concurrent;

namespace BMNetwork
{
    public enum IO_CTX_TYPE
    {
        IO_CTX_NONE,
        IO_CTX_READ,
        IO_CTX_WRITE,
        IO_CTX_DB,
        IO_CTX_S2S
    }

    public enum IO_CTX_ENUM
    {
        NONE = 0,
        MAX_TCP_PKT_LEN = 8192 * 2,//65536,
        MAX_BUFFER_LEN = 8192 * 2, //131072,

    }


    public enum LifeTimeID
    {
        NULL = 0,
        OBJECT = 1,
        POOL,
        SOCKET,
        ACCEPT,

        RECV,
        SEND,

        CONNECTION,

        CONN_MGR,

        MAX,
    };

    public class RefObject
    {
        public RefObject()
        {
            AddRefVar(LifeTimeID.OBJECT);
        }

        private Int32 m_refCount = 0;
        public Int32 RefCount
        {
            get { return m_refCount; }
        }

#if DEBUG
        public List<LifeTimeID> m_refCountList = new List<LifeTimeID>((Int32)LifeTimeID.MAX);

        public void AddRefVar(LifeTimeID id)
        {
			return;
			try
			{
				m_refCountList.Add( id );
				AddRef();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}


        }

        public void ReleaseVar(LifeTimeID id)
        {
			return;
			try
			{
				if ( m_refCountList.Remove( id ) == true )
					Release();
			}
			catch ( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}
        }
#else
        public void AddRefVar(LifeTimeID id)
        {
			return;
            AddRef();
        }

        public void ReleaseVar(LifeTimeID id)
        {
			return;
            Release();
        }
#endif

        private void AddRef()
        {
            Interlocked.Increment(ref m_refCount);
        }

        private void Release()
        {
            if (Interlocked.Decrement(ref m_refCount) == 0)
                OnFree();

            if ( m_refCount < 1)
                m_refCount = 1;
        }



        protected virtual void OnFree()
        {

        }
    }


    public class PerContext : RefObject
    {
     
    }

	public abstract class ContextPool<T> where T : class, new()
    {
		protected ObjectPool<T> m_CtxPool = new ObjectPool<T>( () => new T() );

		public void InitPool( Int32 nPoolSize )
		{
			m_CtxPool.MinimumSize = nPoolSize;
			for( Int32 nId = 0; nId < nPoolSize; ++nId )
			{
				m_CtxPool.Push( new T() );
			}
		}

		protected void ReleaseContext( T socketCtx, bool isDelete = false )
		{
			if( socketCtx == null )
				return;

			m_CtxPool.Push( socketCtx );
		}

		protected T GetContext()
		{
			return m_CtxPool.Pop();
		}
    }
}
