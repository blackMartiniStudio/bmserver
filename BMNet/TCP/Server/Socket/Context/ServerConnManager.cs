﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

namespace BMNetwork.Managed
{
    public class ServerConnManager : ConnectionManager
    {
        #region [Simple Singleton]
        private static ServerConnManager _inst = new ServerConnManager();
        public static ServerConnManager INSTANCE
        {
            get { return _inst; }
        }
        private ServerConnManager()
        {
        }
        #endregion  // [Simple Singleton]


        public bool FindServerConnectionByType(UInt32 requestClientID, BMServerType type, out ISocket active_client)
        {
            active_client = null;
            try
            {
                lock (this)
                {
                    foreach (var obj in this.Clients.Values)
                    {
                        if (type == obj.DestServerType && (requestClientID % 10) == (obj.ClientID % 10))
                        {
                            active_client = obj;
                            return true;
                        }
                    }

                    foreach (var obj in this.Clients.Values)
                    {
                        if (type == obj.DestServerType)
                        {
                            active_client = obj;
                            return true;
                        }
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }
        }

        public bool AddServerConnection(UInt32 clientID, ISocket alloc_client)
        {
            return this.Add(clientID, alloc_client);
        }

        public void RemoveServerConnection(UInt32 clientID)
        {
            this.Remove(clientID);
        }


    }
}
