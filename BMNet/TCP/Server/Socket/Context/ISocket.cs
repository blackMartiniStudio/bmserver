﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using BMNetwork.Managed;


namespace BMNetwork
{
    public enum CONNECTION_STATE
    {
        NONE = 0,
		PROGRESS,			// 폰 만들기 전(첫 패킷 받은 후) or 서버이동 후 폰 만든후
        CONNECTED,			// 커넥트 된후(첫 패킷 받기 전)
        AUTHORIZE,			// 일반적인 유저정도가 어느정도 갖춰진 후(로그인 완료라고 봐도됨)
        ACTIVATE,			// 안쓰임
        CLOSING,			// 안쓰임
        DISCONNECT,			// 안쓰임
        FIN					// 안쓰임
    }

    // 인덱스 규칙을 정해서 프록시들을 잘 관리할 수 있게 변경 필요[4/29/2012 seedyoon]
    public enum CONNECTION_TYPE
    {
        NONE = 0,
        SESSION_PROXY = 1000,
        DBAGENT_PROXY = 2000,
        GAME_PROXY = 3000,
        MATCH_PROXY = 4000,
    }

    public enum BatchResult
    {
        Success = 0,
        Fail,
        Close,
    }

    public class SendBuffer
    {
		static private readonly Int32 MaxSendBufLength = /*8192;*/ 32768;
        public Byte[] buffer;
        public Int32 bufferOffset = 0;
        private Int32 bufferedLength = 0;
        public Int32 sendedOffset = 0;

		public SendBuffer()
		{
			buffer = new Byte[SendBuffer.MaxSendBufLength];
		}
        public Int32 BufferedLength
        {
            get { return bufferedLength; }
            set
            {
                if (value > SendBuffer.MaxSendBufLength)
                {
#if DEBUG
                    Debug.Assert(false);
#endif
                    bufferedLength = SendBuffer.MaxSendBufLength;
                }
                else
                {
                    bufferedLength = value;
                }
            }
        }
        public Int32 BufferCapacity
        {
            get { return SendBuffer.MaxSendBufLength - bufferedLength; }
        }

        public Int32 MaxBufSize
        {
            get { return MaxSendBufLength; }
        }

		public void Reset()
		{
			bufferOffset = 0;
			bufferedLength = 0;
			sendedOffset = 0;

			Array.Clear( buffer, 0, buffer.Length );  
		}
    }

    public interface ISocket
    {
        ManagedServer OwnServer { get; }
        CONNECTION_STATE ConnState { get; set; }
        BMServerType DestServerType { get; set; }
        PerSocketContext SockCtx { get; set; }
        UInt32 ClientID { get; }

        bool IsSendable { get; }

        AtomicInt WirtePendingBytes { get; set; }
        AtomicInt ReadBytePkt { get; set; }
        AtomicInt WriteBytePkt { get; set; }


        bool Init(ManagedServer server, PerSocketContext sock_ctx);
        void Update(Int64 elapsedTick);
        void OnClose();
        bool SendPacket();
        bool Send(C2SPacketEnum cmd, object pkt);    // 클라이언트와의 통신
        bool Send(S2SPacketEnum cmd, object pkt);                      // 서버간 통신
        bool SendBytes(Byte[] buffer);

        BatchResult Batch();
        bool RecvPacket(PerSocketContext sock_ctx, Int32 byteTransferred);
        void ReserveDisconnect(String reason);
        bool IsCloseComplete();

        void MsgDispatch(Int32 cmd, object pkt);

        System.Net.EndPoint RemoteEndPoint { get; }
    }



}
