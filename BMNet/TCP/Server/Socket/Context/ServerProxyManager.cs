﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMNetwork.Managed;

namespace BMNetwork
{
    public class ServerProxyManager
    {
        protected Object lockManager = new Object();
        protected Dictionary<UInt32, ServerConnection> m_dicProxy = new Dictionary<UInt32, ServerConnection>();
        protected Dictionary<UInt32, bool>              m_kDicProxyConfigDataReloadComplete = new Dictionary<UInt32, bool>();
        private ServerConnection  m_curProxy;

		#region [Property]
		public List<ServerConnection> GetProxyInfoList()
		{
			return m_dicProxy.Values.ToList();
		}
		#endregion [Property]
		
		public virtual void Add( ServerConnection p_proxy )
        {
            lock (lockManager)
            {
                if (m_curProxy == null)
                {
                    m_curProxy = p_proxy;
                }

                if (m_dicProxy.ContainsKey(p_proxy.ClientID) == false)
                {
                    m_dicProxy.Add(p_proxy.ClientID, p_proxy);
					Logger.InfoLog( "{0}.Add(), ProxyType:{1}, Key = {2}", this.ToString(), p_proxy.DestServerType, p_proxy.ClientID );
                }
                else
                {
					Logger.ErrLog( "{0}.Add contains fail, ProxyType:{1}, Key = {2}", this.ToString(), p_proxy.DestServerType, p_proxy.ClientID );
                }
            }
        }

        public bool Find( UInt32 _unClientID, out ServerConnection _kServerConnection )
        {
            lock ( lockManager )
            {
                if ( false == m_dicProxy.TryGetValue( _unClientID, out _kServerConnection ) )
                    return false;
                return true;
            }
        }

        public virtual void Remove( ServerConnection _kProxy )
		{
			lock( lockManager )
			{
				ServerConnection removeProxy = _kProxy;
				if( true == m_dicProxy.ContainsKey( _kProxy.ClientID ) )
				{
					Logger.InfoLog( "{0}.Remove() ProxyType:{1}, Key = {2}", this.ToString(), _kProxy.DestServerType, _kProxy.ClientID );
					m_dicProxy.Remove( _kProxy.ClientID );
				}

				if( 0 == m_dicProxy.Count )
				{
					m_curProxy = null;
					Logger.WarnLog( "{0} is zero!!", _kProxy.ToString() );
					return;
				}

				if( null != m_curProxy )
				{
					// 현재 선택되어진 proxy인 경우. 현재 것을 제거 후, 가장 앞의 것을 선택
					if( m_curProxy.ClientID == removeProxy.ClientID )
						m_curProxy = m_dicProxy.Values.First();
				}
			}
		}

        public void SendPacketToAll( S2SPacketEnum cmd, Object sendPacket )
        {
            for ( int i = 0; i < m_dicProxy.Count; ++i )
            {
                lock ( lockManager )
                {
                    m_dicProxy.ElementAt( i ).Value.Send( cmd, sendPacket );
                }
            }
        }

        public void InitReloadConfigData()
        {
            m_kDicProxyConfigDataReloadComplete.Clear();

            List<UInt32> kServerClientIDList = m_dicProxy.Keys.ToList();
            for(Int32 i = 0; i < kServerClientIDList.Count; ++i)
            {
                m_kDicProxyConfigDataReloadComplete.Add(kServerClientIDList[i], false);
    }
}

        public void CompleteReloadConfigData(UInt32 _kServerClientID)
        {
            if(false == m_kDicProxyConfigDataReloadComplete.ContainsKey(_kServerClientID))
            {
                Logger.ErrLog("CompleteReloadConfigData is Fail Not Found ClientID", _kServerClientID);
                return;
            }

            m_kDicProxyConfigDataReloadComplete[_kServerClientID] = true;

            if(false == CheckAllServerReloadComplete())
                return;

            Logger.DebugLog("Complete All Server Reload");
            NotifyApplyReloadConfigData();
        }

        private bool CheckAllServerReloadComplete()
        {
            return m_kDicProxyConfigDataReloadComplete.Where(item => item.Value == false).Count() == 0;
        }

        private void NotifyApplyReloadConfigData()
        {
            SendPacketToAll(S2SPacketEnum.CMD_PacketMMNtApplyReloadConfigData, null);
        }
    }
}
