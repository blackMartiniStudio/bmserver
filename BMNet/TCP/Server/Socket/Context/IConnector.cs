﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;


//  커넥터.. 현재 서버 전체에서 같은 형식의 커넥션 방법을 취하고 있으므로
//  인터페이스 이외에 기본 형식을 서버 라이브러리에 정의해서 상속 받아 쓰게 하자.[4/29/2012 seedyoon]
namespace BMNetwork.Managed
{
    public interface IConnector
    {
        bool Init(String ip, int port);
        void CleanUp();

        Socket CreateAndConnectSocket();
        ISocket MakeConnection();
    }
}
