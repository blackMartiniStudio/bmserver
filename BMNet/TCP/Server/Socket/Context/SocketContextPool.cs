﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMNetwork
{
	public class SocketContextPool : ContextPool<PerSocketContext>
	{
		private Int32 m_nIOCtxBuffSize = 8192;

		public SocketContextPool( Int32 _nIOCtxBuffSize )
		{
			m_nIOCtxBuffSize = _nIOCtxBuffSize;
		}


		public void Restore( PerSocketContext socketCtx )
		{
			if( socketCtx == null )
				return;

			base.ReleaseContext( socketCtx );
		}

		public PerSocketContext Get()
		{
			PerSocketContext ctx = base.GetContext();
			if( ctx == null )
				return null;

			ctx.Reset();
			ctx.IOCtx.Initialize( m_nIOCtxBuffSize );

			return ctx;
		}

	}
}
