﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMNetwork
{
    public delegate void RecvProxyDelegate();
    public delegate void SendProxyDelegate();

    public class Procedure
    {
        public Procedure( RecvProxyDelegate _RecvProxy, SendProxyDelegate _SendProxy )
        {
            RecvProxy = _RecvProxy;
            SendProxy = _SendProxy;
        }
        RecvProxyDelegate RecvProxy = null;
        SendProxyDelegate SendProxy = null;
    }

    public class PacketProcedureHelper
    {
        public static void Add( Dictionary<Int32, Procedure> dicPacketProcedure, Int32 cmd, RecvProxyDelegate recv, SendProxyDelegate send )
        {
            Procedure tempProcedure = new Procedure( recv, send );
            dicPacketProcedure.Add( cmd, tempProcedure );
        }
    }
}
