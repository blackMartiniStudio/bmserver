﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
namespace BMNetwork.Managed
{
    public class ConnectionManager
    {
        private Object _lockObj = new Object();
        protected Dictionary<UInt32, ISocket> m_dicClient = new Dictionary<UInt32, ISocket>();

        #region [Property]
        public Dictionary<UInt32, ISocket> Clients
        {
            get { return m_dicClient; }
        }
        #endregion  // [Property]

        public Int32 Count
        {
            get 
            {
                lock (_lockObj)
                {
                    return m_dicClient.Count; 
                }
            }
        }

        public Int32 ClientCount
        {
            get
            {
                lock ( _lockObj )
                {
                    Int32 nCount = 0;
                    foreach ( var Client in m_dicClient )
                    {
                        if ( Client.Value.DestServerType == BMServerType.CLIENT )
                            ++nCount;
                    }
                    return nCount;
                }
            }
        }

        public bool Add(UInt32 clientID, ISocket alloc_client)
        {
            try
            {
                lock (_lockObj)
                {
                    m_dicClient.Add(clientID, alloc_client);

                    Logger.DebugLog("ConnectionManager.Add(), cID = {0}", clientID);
                }
                return true;
            }
            catch (System.ArgumentException aex )
            {
				if ( alloc_client.DestServerType > BMServerType.None )
					Logger.ExceptionLog( aex );
                ISocket tempClient = null;
                if( m_dicClient.TryGetValue( clientID,  out tempClient) == true)
                {
                    var conn = tempClient as Connection;
                    if ( conn != null)
                        conn.ReserveDisconnect("ConnectionManager duplication id Fail cID " + clientID );

					Logger.ErrLog( "Duplication ClientID. OldServerType = {0}, NewServerType = {1}, ClientID = {2}", tempClient.DestServerType, alloc_client.DestServerType, clientID );
                    m_dicClient.Remove(clientID);
                }
                // 제거 후 추가.
                m_dicClient.Add(clientID, alloc_client);
                return true;
            }
            catch (System.Exception ex)
            {
                Logger.ErrLog("ConnectionManager Exception Fail cID {0}", clientID);
                Logger.ExceptionLog(ex);


                return false;
            }
        }

        public void Remove(UInt32 clientID)
        {
            lock (_lockObj)
            {
#if _NATRACELOG
                Logger.DebugLog("ConnectionManager.Remove(), cID = {0}", clientID);
#endif
                this.m_dicClient.Remove(clientID);
            }
        }

        public bool Find(UInt32 clientID, out ISocket active_client)
        {
            lock (_lockObj)
            {
                return this.m_dicClient.TryGetValue(clientID, out active_client);
            }
        }

        internal void GetWritePending(out Int32 readbytes, out Int32 writebytes, out Int32 writePending)
        {
            writePending = 0;
            readbytes = 0;
            writebytes = 0;
            lock (_lockObj)
            {
                foreach (var conn in m_dicClient.Values)
                {
                    readbytes += conn.ReadBytePkt.Value;
                    conn.ReadBytePkt.Value = 0;

                    writebytes += conn.WriteBytePkt.Value;
                    conn.WriteBytePkt.Value = 0;

                    writePending += conn.WirtePendingBytes.Value;
                }
            }

        }

        internal void Update(Int64 elapsedTick)
        {
        }

        public ISocket GetFirstSocket()
        {
            return m_dicClient.First().Value;
        }
    }
}
