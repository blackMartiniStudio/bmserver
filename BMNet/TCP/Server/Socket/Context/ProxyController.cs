﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMUtility;
using System.Net.Sockets;

namespace BMNetwork.Managed
{
    public class ProxyController
    {
        private List<ISocket> m_proxyList = new List<ISocket>();
        private Object m_LockConnect = new object();
        public Object ConnectLock
        {
            get { return m_LockConnect; }
        }
        
        protected Int32 CurrentProxyCount
        {
            get { return m_proxyList.Count; }
        }

        protected ISocket Proxy
        {
            get { return m_proxyList[0]; }
        }
        protected AtomicInt         m_isRunnable = new AtomicInt(0);
        protected AtomicInt         m_tryConnected = new AtomicInt(0);
        private ServerConnector     m_connector = null;
        public ServerConnector   Connector
        {
            get {return m_connector;}
        }

        public ProxyController(ServerConnector connector)
        {
			m_connector = connector;
		}

        public virtual bool Activate(ISocket proxy)
        {
            m_tryConnected.Off();
            m_isRunnable.CasOn();

            m_proxyList.Add(proxy);

            return true;
        }

        public virtual void Reset()
        {
            if (m_isRunnable.IsOn() == false)
                return;


            List<ISocket> removeList = new List<ISocket>();
            foreach ( var proxy in m_proxyList )
            {
                if ( proxy.ConnState == CONNECTION_STATE.FIN)
                    removeList.Add(proxy);
            }

            foreach ( var proxy in removeList)
            {
                m_proxyList.Remove(proxy);
            }

            if (m_proxyList.Count == 0)
            {
                m_isRunnable.CasOff();
                Logger.WarnLog("Proxy list is zero runnable false");
            }
        }

        public void Send(S2SPacketEnum cmd, Object pkt)
        {
            if (m_isRunnable.IsOn() == false)
                return;

            if (Proxy == null)
                return;

            if (Proxy.SockCtx == null)
                return;

            if (Proxy.SockCtx.Sock == null)
                return;

            if (Proxy.SockCtx.Sock.Connected == false)
                return;

            Proxy.Send(cmd, pkt);
        }

        public void SendChoice(S2SPacketEnum cmd, Object pkt, String ip)
        {
            if (m_isRunnable.IsOn() == false)
                return;

            if (Proxy == null)
                return;

            if (Proxy.SockCtx == null)
                return;

            if (Proxy.SockCtx.Sock == null)
                return;

            if (Proxy.SockCtx.Sock.Connected == false)
                return;

            if (((System.Net.IPEndPoint)Proxy.RemoteEndPoint).Address.ToString().Equals(ip) == false)
                return;

            Proxy.Send(cmd, pkt);
        }

        public void TryConnect()
        {
            // 연결 시도중이면 답을 받을 때 까지 pass
            if (m_tryConnected.IsOn() == false )
            {
                m_tryConnected.CasOn();

                Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				sock.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
                m_connector.TryConnect(sock, this);
            }
            
        }

        public void ReTryConnect()
        {
#if DEBUG
            Logger.InfoLog("ReTryConnect.");
#endif
            m_tryConnected.CasOff();
        }

        public bool IsRunable()
        {
            if (m_isRunnable.IsOn() == false)
                return false;

            if (Proxy == null)
                return false;

            if (Proxy.SockCtx == null)
                return false;

            if (Proxy.SockCtx.Sock == null)
                return false;

            if (Proxy.SockCtx.Sock.Connected == false)
                return false;

            Proxy.Send(S2SPacketEnum.CMD_PacketS2SRqPing, null);

            return true;
        }
        
    }
}
