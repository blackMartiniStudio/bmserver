﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMNetwork.Managed;

namespace BMNetwork
{
    abstract public class ServerNode
    {
        private ServerConnection m_proxy;
        protected ServerConnection Proxy
        {
            get { return m_proxy; }
        }
        
        public ServerNode(ServerConnection proxy)
        {
            m_proxy = proxy;
        }

        public abstract bool Init(Object authorizePacket);
    }
}
