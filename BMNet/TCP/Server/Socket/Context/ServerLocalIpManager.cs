﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMNetwork
{
    public class ServerLocalIpManager : TSingleton<ServerLocalIpManager>
    {
        private String m_localIP;

        public String LocalIP
        {
            get { return m_localIP; }
        }

        public bool Init(String file_name)
        {   
            CSVReader reader = new CSVReader(file_name, this.OnProcess);
            if (reader.OnParser() == false)
                return false;

            return true;
        }

        private void OnProcess(String[] value)
        {
            try
            {
                m_localIP = value[0].Trim();

                Logger.InfoLog("## LocalIP : {0}", m_localIP);
            }
            catch (System.ArgumentException ae)
            {
                Logger.PrintLog(ae.ParamName + " : " + ae.Message);
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
        }
    }
}
