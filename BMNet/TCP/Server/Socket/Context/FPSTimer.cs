﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BMNetwork
{
    class FPSTimer
    {
        static Int64 FrameCnt = 0;
        static Int64 TimeElapsed = 0;
        static Int64 lastTime = DateTime.Now.Ticks;
        public float m_fFPS = 0.0f;
        
        
        public Int64 m_minElaspdTick = 0;
        public Int64 m_maxElapsedTick = 0;

        public Int64 RATE = 10;
        public void UpdateFPS()
        {
            Int64 currTime = DateTime.Now.Ticks;
            if (lastTime < currTime)
            {
                m_fFPS = (float)FrameCnt / (float)(currTime - (lastTime - TimeSpan.TicksPerSecond)) * TimeSpan.TicksPerSecond;

                lastTime = currTime + TimeSpan.TicksPerSecond;
                FrameCnt = 0;
            }
            else
                ++FrameCnt;

            TimeElapsed = DateTime.Now.Ticks - currTime;

            m_minElaspdTick = Math.Max(m_minElaspdTick, TimeElapsed);
            if (m_minElaspdTick > TimeSpan.TicksPerSecond)
                Logger.WarnLog(" 지연이 발생할 수  있음 {0}", TimeElapsed);


            if (TimeElapsed < (RATE * TimeSpan.TicksPerMillisecond))
            {
                Int32 delay = (Int32)(((RATE * TimeSpan.TicksPerMillisecond) - TimeElapsed) / TimeSpan.TicksPerMillisecond);
                Thread.Sleep(delay);
            }
            else
                Thread.Sleep(5);

            // 최대 지연시간 - Sleep 지연시간 기록.
            m_minElaspdTick = Math.Max(m_maxElapsedTick, (DateTime.Now.Ticks - currTime));
        } 
    }
}
