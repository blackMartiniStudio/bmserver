﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;
using System.Threading;

namespace BMNetwork
{
    public class PerIoContext : PerContext
    {
        public Byte[] m_buffer = null;
        public Int32 m_offset;        // 사용한 offset

        public Int32 m_internal_offset;
        public Int32 m_internalHigh_offset;

        public IO_CTX_TYPE m_type;    // read write type

        // ioctx buf에 남은 양 반환
        public Int32 RemainBufLength
        {
            get
            {
                return this.m_buffer.Length - this.m_offset;
            }
        }


        
        public void Initialize(Int32 bufCapacity)
        {
            if (m_buffer == null)
                this.m_buffer = new Byte[bufCapacity];

			this.Reset();
        }

        private void Reset()
        {
			Array.Clear( this.m_buffer, 0, this.m_buffer.Length );

            this.m_offset = 0;
            this.m_internal_offset = 0;
            this.m_internalHigh_offset = 0;
            this.m_type = IO_CTX_TYPE.IO_CTX_READ;
        }
    }
}
