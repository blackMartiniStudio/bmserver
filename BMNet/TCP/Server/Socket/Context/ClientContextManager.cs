﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BMNetwork
{
	public class ClientContextManager : SocketContextManager
	{
		public ClientContextManager( ManagedServer _kOwner, Int32 _nIOCtxBufferSize )
			: base( _kOwner, _nIOCtxBufferSize )
		{
			
		}

		protected override void Initialize()
		{
			m_kContextPool.InitPool( 3000 );
		}

	}
}
