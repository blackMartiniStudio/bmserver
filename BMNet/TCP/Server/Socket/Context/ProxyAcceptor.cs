﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace BMNetwork.Managed
{
    abstract public class ProxyAcceptor : IoAcceptor
    {
        public ProxyAcceptor(ManagedServer ownServer)
            : base(ownServer)
        {
            ownServer.InitClientPool();
        }

        protected override bool Filtering(Socket allocSock)
        {
            // Using the RemoteEndPoint property.
            return base.Filtering(allocSock);
        }

        protected override ISocket OnAccept(bool success, Socket sock)
        {
            try
            {
                if (IsRunning == false)
                    return null;
                
                bool isCorrectSocket = Filtering(sock);
                if (isCorrectSocket == false)
                {
                    Logger.WarnLog("isCorrectSocket Fail Filter!!");
                    return null;
                }

                ISocket allocClient = OwnServer.OnProxyAccept(sock, CreateSock, null);
                if (allocClient != null)
                {

                    EndPoint endPoint = null;
                    try
                    {
                        endPoint = sock.RemoteEndPoint;
                        return allocClient;
                    }
                    catch (System.Exception ex)
                    {
                        Logger.ExceptionLog(ex);
                    }
                    ManagedServer.SafeClose(sock);
                    Logger.ErrLog("Accept Sock Fail {0}", this.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return null;
        }
        
    }
}
