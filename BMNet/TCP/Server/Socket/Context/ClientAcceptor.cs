﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace BMNetwork.Managed
{

    public abstract class ClientAcceptor : IoAcceptor
    {

        public ClientAcceptor(ManagedServer ownServer)
            : base(ownServer)
        {
            ownServer.InitClientPool();
        }

        protected override bool Filtering(System.Net.Sockets.Socket allocSock)
        {
            return base.Filtering(allocSock);
        }

        protected override ISocket OnAccept(bool success, Socket sock)
        {
            try
            {
                if (IsRunning == false)
                    return null;

                bool isCorrectSocket = Filtering(sock);
                if (isCorrectSocket == false)
                {
                    Logger.WarnLog("Blocking Socket");
                    return null;
                }

                ISocket allocSock = OwnServer.OnClientAccept(sock, CreateSock, null);
                if ( allocSock == null)
                {
                    
                    EndPoint endPoint = null;
                    try
                    {
                        endPoint = sock.RemoteEndPoint;
                        return allocSock;
                    }
                    catch (System.Exception ex)
                    {
                        Logger.ExceptionLog(ex);
                    }
                    ManagedServer.SafeClose(sock);
                    Logger.ErrLog("Accept Sock Fail {0}", this.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
            }
            return null;
        }

    }
}
