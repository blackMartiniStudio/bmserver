﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace BMNetwork
{
    public delegate ISocket CreateSockeDelegate();
    abstract public class IoAcceptor : IDisposable
    {
        public bool IsRunning { get; private set; }
        /// <summary>
        /// 중지 중인가?
        /// </summary>
        public bool IsStopping { get; private set; }
        private Thread m_thread = null;
        private AutoResetEvent m_connected = null;
        private AutoResetEvent m_startupfinished = null;
        private AtomicInt m_suspended = new AtomicInt();

        private IPEndPoint m_endPoint;
        public IPEndPoint RemoteEndPoint
        {
            get { return m_endPoint; }
            private set { m_endPoint = value; }
        }
        private Socket m_listenSock = null;
        public Socket ListenSock
        {
            get { return m_listenSock; }
        }
        private ManagedServer m_ownServer;
        protected ManagedServer OwnServer
        {
            get { return m_ownServer; }
        }

        private Int32 m_backlog = 0;
        public Int32 BackLog
        {
            get { return m_backlog; }
        }

        public IoAcceptor(ManagedServer server)
        {
            this.m_ownServer = server;
        }


        public IoAcceptor(ManagedServer owner, IPEndPoint endPoint, int backlog)
        {
            this.m_ownServer    = owner;
            this.m_endPoint = endPoint;
            this.m_backlog  = backlog;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (IsRunning)
                    Stop();

                m_connected.Close();
                m_connected = null;
            }

            m_ownServer = null;
            m_thread    = null;
        }

        public void Suspend(bool suspending)
        {
            if (suspending)
            {
                m_suspended.On();
            }
            else
            {
                m_suspended.Off();
            }
        }

        public void Stop()
        {
            IsStopping = true;

            if (m_listenSock != null)
            {
                m_listenSock.Close();
                m_listenSock = null;
            }

            if (m_thread != null)
            {
                m_thread.Join();
                m_thread = null;
            }

            // waiting for shutdown
            while (IsRunning) { const int TIMEOUT = 10; Thread.Sleep(TIMEOUT); }
        }

        private void ListenThreadEntry()
        {
            try
            {
                m_listenSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                m_listenSock.Bind(RemoteEndPoint);
                m_listenSock.Listen(BackLog);

            }
            catch (Exception e)
            {
                Logger.ExceptionLog(e);
                m_startupfinished.Set();

                return;
            }

            SocketAsyncEventArgs eventArgs  =   new SocketAsyncEventArgs();
            eventArgs.Completed             +=  new EventHandler<SocketAsyncEventArgs>(CompletedAcceptClient);

            IsRunning = true;

            // 시작 완료되었어요~
            m_startupfinished.Set();
            while (!IsStopping)
            {
                bool pending = false;
                try
                {
                    eventArgs.AcceptSocket = null;
                    pending = m_listenSock.AcceptAsync(eventArgs);
                }
                catch (Exception e)
                {
                    Logger.ExceptionLog(e);
                    break;
                }

                if (!pending)
                {
                    CompletedAcceptClient(null, eventArgs);
                }

                // 접속이 하나 완료될때까지는 대기~ 완료된후 다시~ 
                m_connected.WaitOne();
            }

            IsRunning = false;
        }

        private void CompletedAcceptClient(object obj, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                Socket client = e.AcceptSocket;
                m_connected.Set();

                if (m_suspended.IsOn())
                {
                    client.Close();
                    return;
                }

                OnAccept(false, client);
            }
            else
            {
                m_connected.Set();
            }
        }


        public bool Init(IPEndPoint endPoint, Int32 backlog)
        {
            try
            {
                Logger.InfoLog("======================================");
                Logger.InfoLog("IP [{0}] Port [{1}] BackLog [{2}]", endPoint.Address.ToString(), endPoint.Port, backlog);
                
                IsRunning           = false;
                IsStopping          = false;

                m_thread            = new Thread(ListenThreadEntry);
                
                m_endPoint = endPoint; // for log

                m_listenSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                m_listenSock.Blocking = false;
                m_listenSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, true);
                m_listenSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, true);

                m_backlog = backlog;
            }
            catch (System.Exception ex)
            {
                m_listenSock.Close();
                m_listenSock.Dispose();

                Logger.ExceptionLog(ex);

                return false;
            }
            return true;
        }

        public void Open()
        {
            Logger.InfoLog("======================================");
            Logger.InfoLog("ListenSock Open");
            Logger.InfoLog("======================================");
            try
            {
                m_connected         = new AutoResetEvent(false);
                m_startupfinished   = new AutoResetEvent(false);
                    
                m_thread.Start();
            }
                
            catch (Exception e)
            {
                Logger.ExceptionLog(e);
            }
            m_startupfinished.WaitOne();
        }

        

        public void CleanUp()
        {
            ManagedServer.SafeClose(m_listenSock);
            m_listenSock.Dispose();
        }


        // TODO: seedyoon [8/1/2013]
        // 이런곳에서 filterring도 가능 할 듯.
        protected abstract ISocket OnAccept(bool success, Socket sock);
        protected virtual bool Filtering(Socket allocSock)
        {
            // Using the RemoteEndPoint property.
            // filter ip counting 후 지나치케 많은 카운팅이 존재시 접속 불가~
//             String ip_name = ((IPEndPoint)allocSock.RemoteEndPoint).Address.ToString();
//             Int32 index1 = ip_name.IndexOf("10.199.250.");
//             if (index1 != -1)
//                 return false;

            //Logger.DebugLog("connected to " + IPAddress.Parse(((IPEndPoint)allocSock.RemoteEndPoint).Address.ToString()) + " on port number " + ((IPEndPoint)allocSock.RemoteEndPoint).Port.ToString());
            return true;
        }

        public bool Standby(String ip, Int32 port, Int32 backlog)
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            return Init(endPoint, backlog);
        }

        protected abstract ISocket CreateSock();

    }
}
