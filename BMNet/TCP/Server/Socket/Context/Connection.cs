﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;

using BMNetwork;
using System.Net;
using System.Diagnostics;
using System.Collections.Concurrent;

namespace BMNetwork.Managed
{
    public abstract class Connection : ISocket
    {
        //Stopwatch updateStop = new Stopwatch();
        //List<Int32> liTestCallCnt = new List<Int32>();

        // const value
        static private readonly Int32 MAX_SEND_BUFFER_COUNT = 100;
        static private readonly Int32 NOTI_SEND_BUFFER_COUNT = 10;

        //========================================================================================
#if SERVERQA
        protected bool m_isWatch = false; // x테스트 변수.
#endif

        protected ManagedServer m_ownServer = null;
        protected PerSocketContext m_sockCtx = null;
        protected UInt32 m_clientId = 0;                // 이 게임서버에서만 유니크한 값
        protected CONNECTION_STATE m_connState;

        protected ConnectionManager m_conn_manger = null;

		protected ConcurrentQueue<SendBuffer> m_queSendBuffer = new ConcurrentQueue<SendBuffer>();
        protected Object lockSendQue = new Object();
        protected bool m_isSending = false;
        protected SendBuffer m_sendedBuffer;
        protected SendBuffer m_WriteBuffer;

        private EncodePacketDelegate m_encode_packet_delegate = null;
        private DecodePacketDelegate m_decode_packet_delegate = null;
        private RecvPacketDelegate m_recv_packet_delegate = null;


        private AtomicInt m_writePendingBytes = new AtomicInt(0);
        private AtomicInt m_readBytePacket = new AtomicInt(0);
        private AtomicInt m_writeBytePacket = new AtomicInt(0);



        public AtomicInt WirtePendingBytes
        {
            get
            {
                return m_writePendingBytes;
            }
            set
            {
                m_writePendingBytes = value;
            }
        }
        public AtomicInt ReadBytePkt
        {
            get
            {
                return m_readBytePacket;
            }
            set
            {
                m_readBytePacket = value;
            }
        }
        public AtomicInt WriteBytePkt
        {
            get
            {
                return m_writeBytePacket;
            }
            set
            {
                m_writeBytePacket = value;
            }
        }

        public static AtomicInt m_begin_sendCount = new AtomicInt(0);
        public static AtomicInt m_end_sendCount = new AtomicInt(0);


        //========================================================================================
        #region [Property]
        public ManagedServer OwnServer
        {
            get { return this.m_ownServer; }
        }
        public PerSocketContext SockCtx
        {
            get { return this.m_sockCtx; }
            set { this.m_sockCtx = value; }
        }
        public UInt32 ClientID
        {
            get { return this.m_clientId; }
        }

        protected BMServerType m_destServer = BMServerType.None;
        private EndPoint m_remoteEndPoint;
        public BMServerType DestServerType
        {
            get { return m_destServer; }
            set { m_destServer = value; }
        }
        virtual public CONNECTION_STATE ConnState   // NARO: TEMP: 디버깅을 위해 임시로 virtual 선언
        {
            get { return m_connState; }
            set { m_connState = value; }
        }

        public bool IsSendable
        {
            get
            {
                if (this.m_queSendBuffer == null) return false;

                if (this.m_queSendBuffer.Count == 0) return false;

                return true;
            }
        }

        public EncodePacketDelegate Encoding
        {
            get { return this.m_encode_packet_delegate; }
        }

        public DecodePacketDelegate Decoding
        {
            get { return this.m_decode_packet_delegate; }
        }

        public RecvPacketDelegate Receiver
        {
            get { return this.m_recv_packet_delegate; }
        }

        public EndPoint RemoteEndPoint
        {
            get { return this.m_remoteEndPoint; }
        }


        #endregion  // [Property]

        //========================================================================================
        // Constructor
        public Connection(UInt32 p_clientId)
        {
            //updateStop.Start();
            this.m_clientId = p_clientId;
        }

        //========================================================================================
        #region [Basement Function]
        public virtual bool Init(ManagedServer server, PerSocketContext new_sock_ctx)
        {
            MakePacketDelegate(server);

			m_WriteBuffer = SendBufferObject.INSTANCE.Get();
			EnqueSendStreamNoLock( m_WriteBuffer );

            m_isSending = false;

            m_ownServer = server;
            new_sock_ctx.Client = this;

            this.m_sockCtx = new_sock_ctx;
            this.m_remoteEndPoint = new_sock_ctx.Sock.RemoteEndPoint;
            this.m_connState = CONNECTION_STATE.CONNECTED;

            return this.m_conn_manger.Add(this.m_clientId, this);
        }

        public virtual void Update(Int64 elapsedTick)
        {
        }

        public virtual void OnClose()
        {
			RestoreSendBufferByOnClose();

            if (m_ownServer != null && m_conn_manger != null)
                m_conn_manger.Remove(m_clientId);

            if (m_sockCtx != null)
            {
				m_sockCtx.Sock.Shutdown( SocketShutdown.Both );
				m_sockCtx.Sock.Close();
                m_sockCtx.Client = null;
                m_sockCtx = null;
            }

            this.m_ownServer = null;

            this.m_isSending = false;

#if SERVERQA

            if (m_isWatch)
                Logger.InfoLog("Connection::OnClose Complete ID : {0} state {1}", this.m_clientId, this.m_connState);

            
#endif
            m_connState = CONNECTION_STATE.FIN;

        }

        #endregion

        protected void RegistDelegate(EncodePacketDelegate encoder, DecodePacketDelegate decoder, RecvPacketDelegate recver)
        {
            this.m_encode_packet_delegate = encoder;
            this.m_decode_packet_delegate = decoder;
            this.m_recv_packet_delegate = recver;

            bool isValid = (this.m_encode_packet_delegate != null && this.m_decode_packet_delegate != null && this.m_recv_packet_delegate != null);
            if (isValid == false)
                throw new InvalidOperationException("Not Found Packet Delegate " + this.ToString());
        }

        public BatchResult Batch()
        {
            if (m_sockCtx.IsWaitClose.IsOn() == false)
            {
                if (m_sockCtx.IsReading())
                    return this.m_decode_packet_delegate(m_sockCtx, this);
            }
            else
            {
                bool isComplete = this.IsCloseComplete();
                if (isComplete)
                    return BatchResult.Close;
            }
            return BatchResult.Success;
        }

        public bool RecvPacket(PerSocketContext sock_ctx, Int32 bytesTransferred)
        {
            ReadBytePkt.Value += bytesTransferred;
            return this.m_recv_packet_delegate(sock_ctx, bytesTransferred );
        }

        public bool IsCloseComplete()
        {
            //  종료 대기 상태
            if (m_sockCtx == null)
            {
                Logger.ErrLog("sock ctx is Null, ClientID {0} State {1}", this.m_clientId, this.m_connState);
                return true;
            }

            // 소켓이 활성화 되어 있는 상태라면
            if (m_sockCtx.Sock.Connected)
            {
                if (this.IsSendable)
                {
#if DEBUG
                    Logger.DebugLog("IsWaitCloseComplete Client Wait Close sendable..");
#endif
                    return false;
                }

                try
                {
                    Logger.ErrLog( "IsCloseComplete() - shutdown" );
                    m_sockCtx.Sock.Shutdown(SocketShutdown.Both);
                }
                catch (System.Exception)
                {

                }

                try
                {
                    m_sockCtx.Sock.Close();
                }
                catch (System.Exception)
                {

                }

                return false;
            }

            return true;
        }


        #region [for SendPacket Member]
        protected SendBuffer DequeSendSteamNoLock()
        {
			SendBuffer retBuffer;
			if( false == m_queSendBuffer.TryDequeue( out retBuffer ) || m_WriteBuffer == retBuffer )
			{
				m_WriteBuffer = null;
            }

            return retBuffer;
        }

        protected void EnqueSendStreamNoLock(SendBuffer buf)
        {
            this.m_queSendBuffer.Enqueue(buf);
        }

        public void ReserveDisconnect(String reason)
        {
			Flush();

            if (m_sockCtx != null )
            {
                if (m_sockCtx.Sock != null && m_sockCtx.Sock.Connected)
				{
					ManagedServer.SafeClose( m_sockCtx.Sock );
				}
				
                m_sockCtx.ReleaseVar(LifeTimeID.RECV);
                m_sockCtx.IsWaitClose.CasOn();

				if ( m_sockCtx.Sock != null )
					Logger.ErrLog( "ReserveDisconnect WaitClose, reason = {0} Handle = {1}, refCount {2}", reason, m_sockCtx.Sock.Handle, m_sockCtx.RefCount );
				else
					Logger.ErrLog( "ReserveDisconnect WaitClose, reason = {0}", reason );
#if DEBUG
                Logger.DebugLog("ReserveDisconnect WaitClose, reason = {0} Handle = {1}, refCount {2}", reason, m_sockCtx.Sock.Handle, m_sockCtx.RefCount);
#endif
                
            }
            else
            {
                Logger.InfoLog("ReserveDisconnect(), m_sockCtx is null, reason = {0}", reason);
            }
        }

		private void Flush()
		{
			SendPacket();
		}

        #endregion [for SendPacket Member]


        #region [Send Interface]
        public bool SendPacket()
        {
            try
            {
                AsyncCallback sc = null;
                sc = sendResult =>
                {
                    Socket rawSock = null;
                    PerSocketContext using_send_io_ctx = null;
                    try
                    {
                        using_send_io_ctx = (PerSocketContext)sendResult.AsyncState;
                        rawSock = using_send_io_ctx.Sock;

                        if (using_send_io_ctx.Sock.Connected == false)
                        {
                            using_send_io_ctx.ReleaseVar(LifeTimeID.SEND);

                            return;
						}

						Int32 sent_length = rawSock.EndSend( sendResult );

#if SERVERQA
                        Connection.m_end_sendCount.Value++;
                        WriteBytePkt.Value += sent_length;
                        WirtePendingBytes.Value -= sent_length;
#endif

						lock( this.lockSendQue )
						{
							using_send_io_ctx.ReleaseVar( LifeTimeID.SEND );
							if( sent_length != m_sendedBuffer.BufferedLength )
							{
								m_sendedBuffer.BufferedLength -= sent_length;
								m_sendedBuffer.sendedOffset += sent_length;

								if( m_sockCtx != null && m_sockCtx.Sock.Connected )
								{
									lock( rawSock )
									{
										using_send_io_ctx.AddRefVar( LifeTimeID.SEND );
										rawSock.BeginSend( m_sendedBuffer.buffer, m_sendedBuffer.sendedOffset, m_sendedBuffer.BufferedLength, SocketFlags.None, sc, using_send_io_ctx );
									}
#if SERVERQA
                                     Connection.m_begin_sendCount.Value++;
#endif
								}

							}
							else if( m_queSendBuffer.Count > 0 )
							{
								RestoreSendBuffer();

								m_sendedBuffer = DequeSendSteamNoLock();
								if( m_sockCtx != null && m_sockCtx.Sock.Connected )
								{
									lock( rawSock )
									{
										using_send_io_ctx.AddRefVar( LifeTimeID.SEND );
										rawSock.BeginSend( m_sendedBuffer.buffer, 0, m_sendedBuffer.BufferedLength, SocketFlags.None, sc, using_send_io_ctx );
									}
#if SERVERQA
                                     Connection.m_begin_sendCount.Value++;
#endif
								}
							}
							else
							{
								this.m_isSending = false;
							}
						}

                    }
                    catch (ObjectDisposedException ode)
                    {
                        this.m_isSending = false;
                        Logger.InfoLog("Already Send Socket is Dispose" + ode.Message);

                    }
                    catch (System.Exception ex)
                    {
                        this.m_isSending = false;

                        // NARO: TODO: disconnect 된 애들은 더 이상 Send 시도 안되게 수정

                        Logger.ExceptionLog(ex);
                    }
                };


				lock( this.lockSendQue )
				{
					if( m_isSending == false )
					{
						RestoreSendBuffer();

						m_sendedBuffer = DequeSendSteamNoLock();

						if( m_sendedBuffer == null )
							return true;

						if( m_sockCtx.IsWaitClose.IsOn() == true )
							return true;

						Socket rawSocket = m_sockCtx.Sock;
						if( m_sockCtx != null )
						{
							m_isSending = true;
							lock( rawSocket )
							{
								if( rawSocket.Connected == true )
								{
									m_sockCtx.AddRefVar( LifeTimeID.SEND );
									rawSocket.BeginSend( m_sendedBuffer.buffer, 0, m_sendedBuffer.BufferedLength, SocketFlags.None, sc, m_sockCtx );
								}
							}
#if SERVERQA
                             Connection.m_begin_sendCount.Value++;
#endif
						}
						else
						{
							return false;
						}
					}

				}

			}
            catch (SocketException se)
            {
                bool isAbortedReset = ((SocketError)se.ErrorCode == SocketError.ConnectionReset || (SocketError)se.ErrorCode == SocketError.Shutdown);
				if ( isAbortedReset == false || DestServerType > BMServerType.None )
                    Logger.SocketExceptionLog(se);    

                
                return false;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

            return true;
        }

        public virtual bool Send(C2SPacketEnum cmd, Object pkt)
        {
#if _NATRACELOG
            if (cmd != C2SPacketEnum.CMD_PacketCSRsChangeTrack  // 너무 많이 발생한다.. 줄일 방법은 없을까.. by naro
                && cmd != C2SPacketEnum.CMD_PacketCSRqPing
                && cmd != C2SPacketEnum.CMD_PacketCSRsPing
                )
                Logger.DebugLog("++++ Send Packet in ClientConnection cID = {0} : command :: {1}", m_clientId, cmd);
#endif
            try
            {
                Int32 protocol = (Int32)cmd;
                Byte[] header = null;
                Byte[] body = null;

                bool isEncode = m_encode_packet_delegate(protocol, pkt, out header, out body);
                if (isEncode == false)
                {
                    if (pkt != null)
                    {
                        Logger.ErrLog("Encode Packet Error pkt {0} owner ={1}", pkt.ToString(), this.ToString());
                    }
                    else
                    {
                        Logger.ErrLog("Encode Packet Error pkt cmd = {0}, owner = {1}", cmd, this.ToString());
                    }
                    return false;
                }

                SendStream(header, body);
                pkt = null;

            }
            catch (SocketException se)
            {
                Logger.ExceptionLog(se);
                return false;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

            return true;
        }

        public virtual bool Send(S2SPacketEnum cmd, object pkt)
        {
            try
            {
                Int32 protocol = (Int32)cmd;
                Byte[] header = null;
                Byte[] body = null;

                bool isEncode = this.m_encode_packet_delegate(protocol, pkt, out header, out body);
                if (isEncode == false)
                {
                    if (pkt != null)
                    {
                        Logger.ErrLog("Encode Packet Error pkt {0} owner ={1}", pkt.ToString(), this.ToString());
                    }
                    else
                    {
                        Logger.ErrLog("Encode Packet Error pkt cmd = {0}, owner = {1}", cmd, this.ToString());
                    }
                    return false;
                }
                SendStream(header, body);
                pkt = null;

            }
            catch (SocketException se)
            {
                Logger.ExceptionLog(se);
                return false;
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

            return true;
        }

        public bool SendStream(Byte[] header, Byte[] body)
        {
            try
            {
				lock( lockSendQue )
				{

					if( m_WriteBuffer == null )
					{
						m_WriteBuffer = SendBufferObject.INSTANCE.Get();
						EnqueSendStreamNoLock( m_WriteBuffer );
					}

					if( m_WriteBuffer.BufferCapacity > header.Length + body.Length )
					{
						Buffer.BlockCopy( header, 0, m_WriteBuffer.buffer, m_WriteBuffer.bufferOffset, header.Length );
						m_WriteBuffer.BufferedLength += header.Length;
						m_WriteBuffer.bufferOffset += header.Length;
						Buffer.BlockCopy( body, 0, m_WriteBuffer.buffer, m_WriteBuffer.bufferOffset, body.Length );
						m_WriteBuffer.BufferedLength += body.Length;
						m_WriteBuffer.bufferOffset += body.Length;
					}
					else
					{
#if DEBUG
						// NARO: TODO: 이런 경우가 발생하면 안되게 패킷을 설계해야 한다.
						//            가변크기의 패킷일 경우 이런 경우가 예상 되면 BeginPacket 과 EndPacket 사이에
						//            일정사이즈의 배열을 보내는 패킷을 생성하는 구조로 패킷통신 을 설계하도록 하자
						if( m_WriteBuffer.MaxBufSize <= header.Length + body.Length )
						{
							Logger.ErrLog( "!!!!!!!!! 패킷 버퍼가 엄청 큼~ 일단은 65536 으로 해놨지만... 추후 수정 고민해보기... header.Length + body.Length = {0}", header.Length + body.Length );
						}
#endif

						m_WriteBuffer = SendBufferObject.INSTANCE.Get();
						EnqueSendStreamNoLock( m_WriteBuffer );

						Buffer.BlockCopy( header, 0, m_WriteBuffer.buffer, m_WriteBuffer.bufferOffset, header.Length );
						m_WriteBuffer.BufferedLength += header.Length;
						m_WriteBuffer.bufferOffset += header.Length;
						Buffer.BlockCopy( body, 0, m_WriteBuffer.buffer, m_WriteBuffer.bufferOffset, body.Length );
						m_WriteBuffer.BufferedLength += body.Length;
						m_WriteBuffer.bufferOffset += body.Length;
					}

					WirtePendingBytes.Value += ( header.Length + body.Length );
				}
			}
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

            return true;
        }

        public bool SendBytes(Byte[] buffer)
        {
            try
            {
                if (buffer.Length <= 0) return true;

                Int32 bufferLength = buffer.Length;

				lock( lockSendQue )
				{

					if( m_WriteBuffer == null )
					{
						m_WriteBuffer = SendBufferObject.INSTANCE.Get();
						EnqueSendStreamNoLock( m_WriteBuffer );
					}

					if( m_WriteBuffer.BufferCapacity > bufferLength )
					{
						Buffer.BlockCopy( buffer, 0, m_WriteBuffer.buffer, m_WriteBuffer.bufferOffset, bufferLength );
						m_WriteBuffer.BufferedLength += bufferLength;
						m_WriteBuffer.bufferOffset += bufferLength;
					}
					else
					{
						m_WriteBuffer = SendBufferObject.INSTANCE.Get();
						EnqueSendStreamNoLock( m_WriteBuffer );

						Buffer.BlockCopy( buffer, 0, m_WriteBuffer.buffer, m_WriteBuffer.bufferOffset, bufferLength );
						m_WriteBuffer.BufferedLength += bufferLength;
						m_WriteBuffer.bufferOffset += bufferLength;
					}
				}
               
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return false;
            }

            return true;
        }
        #endregion

        #region [abstract function]
        // SEED: Packet 처리 루틴 
        protected abstract void MakePacketDelegate(ManagedServer server);

        // 일반 패킷 처리 by naro
        public abstract void MsgDispatch(Int32 cmd, object pkt);
        #endregion

		public void RestoreSendBuffer()
		{
			if( m_sendedBuffer != null )
			{
				SendBufferObject.INSTANCE.Restore( m_sendedBuffer );
    }
		}

		public void RestoreSendBufferByOnClose()
		{
			RestoreSendBuffer();

			int nCount = m_queSendBuffer.Count;
			for( int nID = 0; nID < nCount; ++nID )
			{
				SendBuffer buffer;
				if( false == m_queSendBuffer.TryDequeue( out buffer ) )
				{
					continue;
				}

				SendBufferObject.INSTANCE.Restore( buffer );
			}
		}
    }
}
