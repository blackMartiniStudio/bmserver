﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
namespace BMNetwork.Managed
{
    public abstract class ServerConnector : IConnector
    {
        protected   IPEndPoint m_endPoint;
        private     ManagedServer m_owner;
        protected   ManagedServer Owner
        {
            get { return m_owner; }
        }

        public IPEndPoint RemoteEndPoint
        {
            get { return m_endPoint; }
        }

        protected ServerConnector( ManagedServer owner )
        {
            m_owner = owner;
        }

        public abstract ISocket MakeConnection();
        public virtual bool TryConnect(Socket allocSock, ProxyController controller)
        {
            throw new NotImplementedException();
        }

        public virtual bool Init(String ip, Int32 port)
        {
            m_endPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            return true;
        }

        public void CleanUp()
        {

        }

        public virtual Socket CreateAndConnectSocket()
        {
            Socket sock = null;
            bool bWroteLog = false;
            while (sock == null)
            {
                try
                {
                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sock.Connect(m_endPoint);
                }
                catch (SocketException se)
                {
                    if ( false == bWroteLog )
                    {
                        Logger.ErrLog( "ServerConnector.CreateSock() SocketException, endPoint = {0} {1}", m_endPoint, se.Message );
                        bWroteLog = true;
                    }
                    sock.Close();
                    sock = null;
                }
                catch (System.Exception ex)
                {
                    if ( false == bWroteLog )
                    {
                        Logger.ErrLog( "ServerConnector.CreateSock() Exception, endPoint = {0} {1}", m_endPoint, ex.Message );
                        bWroteLog = true;
                    }
                    sock.Close();
                    sock = null;
                }

                System.Threading.Thread.Sleep(100);
            }

            return sock;
        }

        
    }
}
