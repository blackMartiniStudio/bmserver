﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMNetwork
{
	public class SendBufferObject : TSingleton<SendBufferObject>
	{
		readonly Int32 m_nPoolSize = 10000;
		private ObjectPool<SendBuffer> m_BufPool = new ObjectPool<SendBuffer>( () => new SendBuffer() );

		public SendBufferObject()
		{
			Int32 nInitPool = (Int32)( m_nPoolSize * 0.3 );//Math.Min( m_BufPool.MinimumSize, (Int32)(m_nPoolSize * 0.5));
			m_BufPool.MinimumSize = m_nPoolSize;

			for( Int32 nId = 0; nId < nInitPool; ++nId )
			{
				m_BufPool.Push( new SendBuffer() );
			}
		}

		public SendBuffer Get()
		{
			SendBuffer kBuffer = m_BufPool.Pop();
			kBuffer.Reset();

			return kBuffer;
		}

		public void Restore( SendBuffer _Buffer )
		{
			m_BufPool.Push( _Buffer );
		}

		public Int32 GetAvailableCount()
		{
			return m_BufPool.AvailableCount;
		}
	}
}
