﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMNetwork.Managed;

namespace BMNetwork
{
    public class ManagementConnector : ServerConnector
    {
        public ManagementConnector(ManagedServer owner)
            : base(owner)
        {
            
        }

		public BMServerType ServerType
		{
			get { return Owner.ServerType; }
		}

        public override ISocket MakeConnection()
        {
            try
            {
                return new ManagementProxy( Owner.CreateSerialKey(), BMServerType.MANAGEMENT);
            }
            catch (Exception ex)
            {
                Logger.ExceptionLog(ex);
                return null;
            }
        }
    }
}
