﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BMNetwork
{
	interface ISocketContextManager
	{
		PerSocketContext GetSocketContext( Socket socket );
		void ReleaseContext( PerSocketContext context );
	}

	public class SocketContextManager : ISocketContextManager
	{
		protected ManagedServer				m_kOwnerServer;
		protected SocketContextPool			m_kContextPool;
		protected HashSet<PerSocketContext>	m_setUsedContext	= new HashSet<PerSocketContext>();
		protected List<PerSocketContext>	m_liRestoreContext	= new List<PerSocketContext>();

		protected Object ContextUpdateLock = new Object();

		public SocketContextManager( ManagedServer _kOwner, Int32 _nIOCtxBufferSize )
		{
			m_kOwnerServer		= _kOwner;
			m_kContextPool		= new SocketContextPool( _nIOCtxBufferSize );

			Initialize();
		}

		protected virtual void Initialize()
		{
		}

		public PerSocketContext GetSocketContext( Socket sock )
		{
			PerSocketContext sock_ctx = m_kContextPool.Get();
			if( sock_ctx == null )
				return null;

			sock_ctx.Sock = sock;

			lock( ContextUpdateLock )
			{
				m_setUsedContext.Add( sock_ctx );
			}
			

			return sock_ctx;
		}

		private void ReleaseContext()
		{
			foreach( PerSocketContext context in m_liRestoreContext )
			{
				ReleaseContext( context );
			}

			m_liRestoreContext.Clear();
		}

		public void ReleaseContext( PerSocketContext context )
		{
			lock( ContextUpdateLock )
			{
				context.ReleaseSocket();
				m_setUsedContext.Remove( context );
			}

			m_kContextPool.Restore( context );
		}

		public void UpdateLogic( Int64 elapsedTick )
		{
			lock( ContextUpdateLock )
			{
				foreach( PerSocketContext context in m_setUsedContext )
				{
					if( context.Client == null )
						continue;

					context.Client.Update( elapsedTick );
				}
			}
		}

		internal void UpdateIoReceive()
		{
			bool isSuccess = false;

			lock( ContextUpdateLock )
			{
				foreach( PerSocketContext context in m_setUsedContext )
				{
					ISocket client = context.Client;
					if( context.IsWaitClose.IsOn() == true )
					{
						if( client != null )
							client.OnClose();

						context.Client = null;
					}

					if( context.Client != null )
					{
						if( context.Client.ConnState != CONNECTION_STATE.FIN )
						{
							if( context.ReadablePacketCount != 0 )
								isSuccess = ClientProcess( context, client );
						}
					}

					if( isSuccess == false && context.IsRelease() )
					{
						m_liRestoreContext.Add( context );
						//ReleaseContext( context );
					}
				}
			}

			ReleaseContext();
		}
	

		internal void UpdateIoPresent()
		{
			lock( ContextUpdateLock )
			{
				foreach( PerSocketContext context in m_setUsedContext )
				{
					if( context.Client == null )
						continue;

					if( context.Client.SendPacket() == false )
						OnClose( context, "UserIOPresent SendPacket Fail" );

				}
			}
		}

		public void DisconnectAll()
		{
			lock( ContextUpdateLock )
			{
				foreach( PerSocketContext context in m_setUsedContext )
				{
					context.IsWaitClose.CasOn();
					if( context.Client == null )
						continue;

					context.Client.ReserveDisconnect( "Disconnected All" );
				}
			}
		}

		internal void MainProcessEnd()
		{
			lock( ContextUpdateLock )
			{
				foreach( PerSocketContext context in m_setUsedContext )
				{
					OnClose( context, "MainProcessEnd" );
				}
			}
		}

		public void OnClose( PerSocketContext sock_ctx, String reason = "" )
		{
			try
			{
				if( sock_ctx == null )
					return;

				if( sock_ctx.IsWaitClose.IsOn() )
					return;

				if( sock_ctx.Sock == null )
					return;

				sock_ctx.IsWaitClose.CasOn();

				if( reason != "" )
					Logger.ErrLog( reason );

			}
			catch( System.Exception ex )
			{
				Logger.ExceptionLog( ex );
			}
		}

		private bool ClientProcess( PerSocketContext sock_ctx, ISocket client )
		{
			BatchResult result = client.Batch();
			if( result == BatchResult.Success )
				return true;

			if( result == BatchResult.Fail )
			{
				OnClose( sock_ctx, result.ToString() );
				return true;
			}

			return false;
		}

		public Int32 GetUsedContextCount()
		{
			return m_setUsedContext.Count;
		}
	}
}
