﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;
using BMNetwork;
using System.Threading;
using BMNetwork.Managed;

namespace BMNetwork
{
    public abstract class ClientConnection : Connection
    {

        static public Int32 AllocateCount = 0;
        static public Int32 PacketThroughput = 0;
        //========================================================================================
        // Constructor
        public ClientConnection(UInt32 p_clientId)
            : base(p_clientId)
        {
            Interlocked.Increment(ref AllocateCount);
        }

        //========================================================================================
        public override bool Init(ManagedServer server, PerSocketContext new_sock_ctx)
        {
            m_conn_manger = server.ClientConnManager;

            return base.Init(server, new_sock_ctx);
        }

        public override void Update(Int64 elapsedTick)
        {
        }

        public override void OnClose()
        {
            base.OnClose();
        }

        protected override void MakePacketDelegate(ManagedServer server)
        {
            base.RegistDelegate(server.PktDelegateMgr.C2SEncoder,
                                   server.PktDelegateMgr.C2SDecoder,
                                   server.PktDelegateMgr.C2SReceiver);
        }


        public override void MsgDispatch(int cmd, object pkt)
        {
            Interlocked.Increment(ref ClientConnection.PacketThroughput);
        }

        protected void SendErrorPkt(C2SPacketEnum protocol, ClientErrorMessage detail_code)
        {
#if DEBUG
            Logger.DebugLog("{0} SendErrorPkt protocol = {1},  detail code = {2}", this.ToString(), (C2SPacketEnum)protocol, detail_code);
#endif
            //this.Send(C2SPacketEnum.CMD_PktS2CClientErrorMessage, new PktS2CClientErrorMessage((Int32)protocol, (Int32)detail_code));
        }


        public override bool Send(S2SPacketEnum cmd, Object pkt)
        {
            throw new NotImplementedException("S2SPacket Not Implement class is " + this.ToString());
        }

        public override bool Send(C2SPacketEnum cmd, Object pkt)
        {
#if DEBUG
			if( LuaScriptManager.INSTANCE.m_bPrintPacketLog )
			{
				if( cmd != C2SPacketEnum.CMD_PacketCSRqPing
					   && cmd != C2SPacketEnum.CMD_PacketCSRsPing
					   && cmd != C2SPacketEnum.CMD_PacketCSRqChangeTrack
					   && cmd != C2SPacketEnum.CMD_PacketCSRqRoomBroadcastPing
					   && cmd != C2SPacketEnum.CMD_PacketCSRsPing
					   && cmd != C2SPacketEnum.CMD_PacketCSRsChangeTrack
					   && cmd != C2SPacketEnum.CMD_PacketCSRsRoomBroadcastPing
					   )
				{
					Logger.DebugLog( "s->c send {0} {1}", this.ClientID, cmd );
				}
			}
            
#endif
            if ( m_sockCtx != null && m_sockCtx.Sock != null)
            {
                return base.Send(cmd, pkt);
            }
            return false;
        }


    }
}
