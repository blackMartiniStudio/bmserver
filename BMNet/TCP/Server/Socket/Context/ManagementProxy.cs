﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMNetwork.Managed
{
    public class ManagementProxy : ServerConnection
    {
        public ManagementProxy(UInt32 clientID, BMServerType dest)
            :base( clientID )
        {
            m_clientId   =  clientID;
            m_destServer =  dest;
        }

        public override bool Init(ManagedServer server, PerSocketContext sock_ctx)
        {
            return base.Init(server, sock_ctx);
        }

        public override void OnClose()
        {
            Logger.ErrLog("{0}.OnClose() clientID = {0}", this.ToString(), m_clientId);
            base.OnClose();
        }

        public override void MsgDispatch(int cmd, object pkt)
        {
            S2SPacketEnum protocol = (S2SPacketEnum)cmd;
            switch (protocol)
            {
                case S2SPacketEnum.CMD_PacketS2SRqPing:
                case S2SPacketEnum.CMD_PacketS2SRsPing:
                    break;

                case S2SPacketEnum.CMD_PacketMMRsAuthrize:
                    OnRecvPacketMMRsAuthrize(pkt);
                    break;

                case S2SPacketEnum.CMD_PacketMMNtConnectAddress:
                    OnRecvPacketMMNtConnectAddress( pkt );
                    break;

                case S2SPacketEnum.CMD_PacketMMRqReadConfigData:
                    OnRecvPacketMMRqReadConfigData(pkt);
                    break;

                case S2SPacketEnum.CMD_PacketMMNtApplyReloadConfigData:
                    OnRecvPacketMMNtApplyReloadConfigData(pkt);
                    break;

                default:
                    Logger.ErrLog("Not Define Protocol {0}", protocol);
                    break;
            }
        }

        

        //========================================================================================
        //
        // 패킷 처리 함수들
        //
        //========================================================================================
        #region [OnRecv 패킷 처리]
        private void OnRecvClientErrorMessage(Object pkt)
        {
            //---------------------------------------------------------------
            // 기본 에러 검사
            PacketS2SCommonErrorMSG recvPacket = pkt as PacketS2SCommonErrorMSG;
            if (recvPacket == null)
            {
                Logger.ErrLog("DBAgentProxy Invalid Packet Recved : PacketS2SCommonErrorMSG");
                return;
            }
        }

        private void OnRecvPacketMMRsAuthrize(object pkt)
        {
            //---------------------------------------------------------------
            // 기본 에러 검사
            PacketMMRsAuthrize recvPacket = pkt as PacketMMRsAuthrize;
            if (recvPacket == null)
            {
                Logger.ErrLog("DBAgentProxy Invalid Packet Recved : PacketMMRsAuthrize");
                return;
            }

            if ((ServerErrorMessage)recvPacket.retCode != ServerErrorMessage.SUCCESS)
            {
                Logger.ErrLog("OnRecvPacketMMRsAuthrize error {0}", (ServerErrorMessage)recvPacket.retCode);
                return;
            }

            // 아직 로딩이 되지 않은 경우. 다시 받아야 한다.
            if (recvPacket.listenPort.Count() == 0 )
            {
                Logger.DebugLog("OnRecvPacketMMRsAuthrize load delay... try request");
                System.Threading.Thread.Sleep(100);
                ManagementManager.INSTANCE.RequsetLoadConfig();
                return;
            }

            OwnServer.ServerGroupID = recvPacket.ServerGroupID;
            OwnServer.ServerID = recvPacket.ServerID;
            OwnServer.CreateDB( recvPacket.dbConnInfo, LuaScriptManager.INSTANCE.m_DBWorkerPoolSize, LuaScriptManager.INSTANCE.m_DBWorkerPerThroughput );
            OwnServer.CreateServerConfig( recvPacket.listenPort, recvPacket.ConnectAddressList );
            OwnServer.FirstProcess();

            Logger.InfoLog( "Given : ServerGroupID = {0}, ServerID = {1}", OwnServer.ServerGroupID, OwnServer.ServerID );
        }

        private void OnRecvPacketMMNtConnectAddress(object pkt)
        {
            //---------------------------------------------------------------
            // 기본 에러 검사
            PacketMMNtConnectAddress recvPacket = pkt as PacketMMNtConnectAddress;
            if ( recvPacket == null )
            {
                Logger.ErrLog( "Invalid Packet Recved : PacketMMNtConnectAddress" );
                return;
            }

            if ( 0 == recvPacket.ConnectAddressList.Length )
            {
                Logger.ErrLog( "OnRecvPacketMMNtConnectAddress fail : Length = 0" );
                return;
            }

            OwnServer.CreateServerConfig( null, recvPacket.ConnectAddressList );
        }

        private void OnRecvPacketMMRqReadConfigData(Object pkt)
        {
            if( false == LuaScriptManager.INSTANCE.m_bRealTimeConfigDataReloadSwitch || null == KMLogic.INSTANCE.GetLogicEntry )
            {
                PacketMMRsReadConfigData kRsPacket = new PacketMMRsReadConfigData(true);
                ManagementManager.INSTANCE.SendPacketToManagementServer(S2SPacketEnum.CMD_PacketMMRsReadConfigData, kRsPacket);
                return;
            }

            ConfigDataDisposer.INSTANCE.RequestRealTimeReadConfigData();
        }

        private void OnRecvPacketMMNtApplyReloadConfigData(Object pkt)
        {
            Logger.DebugLog("All Server ReloadConfigData Complete");
            ConfigDataGlobal.INSTANCE.Init();
            ConfigDataDisposer.INSTANCE.ApplyReloadConfigData();
        }
        #endregion
    }
}
