﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Net.Sockets;
using BMUtility;
using System.Collections.Concurrent;

namespace BMNetwork
{
    public class PerSocketContext : PerContext
    {
#if SERVERQA
        private AtomicInt m_isWatch = new AtomicInt(0);
        public AtomicInt IsWatch
        {
            get { return m_isWatch; }
        }
#endif

        private AtomicInt m_isWaitClose = new AtomicInt(0);

		private PerIoContext m_kIOCtx = new PerIoContext();


        public AtomicInt IsWaitClose
        {
            get { return m_isWaitClose; }
            set { m_isWaitClose = value; }
        }

		public PerIoContext IOCtx
		{
			get { return m_kIOCtx; }
		}


		private ConcurrentQueue<byte[]> m_quePoolReadBuffer = new ConcurrentQueue<byte[]>();
        private object m_lockPool = new object();

        public Socket Sock { get; set; }
        public ISocket Client { get; set; }

        public Int32 ReadablePacketCount
        {
            get	{	return m_quePoolReadBuffer.Count;	}
        }


        public bool IsReading()
        {
            if ( this.Client == null )
                return false;

			return false == this.m_quePoolReadBuffer.IsEmpty;
        }

        public bool IsRelease()
        {
			if( Sock == null )
				return true;

			if( Sock.Connected == false && IsWaitClose.IsOn() && CheckReleaseRefCount() )
                return true;

            return false;
        }

		private bool CheckReleaseRefCount()
		{
			return RefCount <= 1;
		}

        public void AddPacketByPool( Byte[] _byPacket )
        {
			m_quePoolReadBuffer.Enqueue( _byPacket );
			Interlocked.Increment( ref Monitor.ClientSendPacketCount );
        }

		public bool PopPacketByPool( out Byte[] readBuffer )
		{
			return m_quePoolReadBuffer.TryDequeue( out readBuffer );
		}

        public void ReadHeader( Byte[] SocketBuffer, Byte[] WriteBuffer, Int32 length )
        {
            try
            {
				Buffer.BlockCopy( SocketBuffer, 0, WriteBuffer, 0, length );
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
            }
        }
		public void ReadBody( Byte[] SocketBuffer, Byte[] WriteBuffer, Int32 srcOffset, Int32 length )
        {
            try
            {
				Buffer.BlockCopy( SocketBuffer, srcOffset, WriteBuffer, 0, length );
            }
            catch ( System.Exception ex )
            {
                Logger.ExceptionLog( ex );
            }
        }

        public void BufferClear()
        {
			Byte[] buffer;
			while( m_quePoolReadBuffer.TryDequeue( out buffer ) )
			{

			}
            
        }

		public PerSocketContext()
		{
			this.Sock = null;
			BufferClear();
			this.m_isWaitClose.CasOff();

#if SERVERQA
            this.m_isWatch.Off();
#endif

		}
        public void Reset()
        {
#if SERVERQA
            if (this.m_isWatch.IsOn())
            {
                if (this.Sock != null)
                    Logger.InfoLog("Sock Handle {0}", this.Sock.Handle);
            }

            this.m_isWatch.Off();
#endif

			ReleaseSocket();
			BufferClear();

            this.m_isWaitClose.CasOff();

        }

		internal void ReleaseSocket()
		{
			if( this.Sock != null )
			{
				this.Sock = null;
			}
		}
	}
}
