﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BMNetwork.Managed
{
    public class ManagementConfig
    {
        public String   Ip;
        public Int32    Port;
        public Int32    UpdateInterval;        
    }
}
