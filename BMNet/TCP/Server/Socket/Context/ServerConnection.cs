﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;
using System.Threading;
using BMClientSocketLIB;
using BMNetwork;

namespace BMNetwork.Managed
{
    public abstract class ServerConnection : Connection
    {
        static public readonly Int64 KEEPALIVE_INTERVAL = TimeSpan.TicksPerMinute;

        static public Int32 AllocateCount = 0;
        static public Int32 PacketThroughput = 0;
        private Int64 m_keepAliveTick;

		public bool SendServerAuth { get; private set; }
        //========================================================================================
        // Constructor
        public ServerConnection(UInt32 p_clientId)
            : base(p_clientId)
        {
            Interlocked.Increment(ref AllocateCount);
        }

        //========================================================================================
        public override bool Init(ManagedServer server, PerSocketContext sock_ctx)
        {
            m_conn_manger = (ConnectionManager)ServerConnManager.INSTANCE;

            // NARO: TEMP: CBT 전까지만 확인 하기
            Logger.InfoLog("Connection.Init(), ConnectType = {0}, m_remoteEndPoint = {1}", (BMServerType)DestServerType, sock_ctx.Sock.RemoteEndPoint);

            return base.Init(server, sock_ctx);
        }

        public override void Update(Int64 elapsedTick)
        {
            m_keepAliveTick += elapsedTick;
            if (m_keepAliveTick > KEEPALIVE_INTERVAL)
            {
                Send(S2SPacketEnum.CMD_PacketS2SRqPing, null);
                m_keepAliveTick = 0;
            }

            base.Update(elapsedTick);
        }

        public override void OnClose()
        {
            Logger.DebugLog("ServerConn::OnClose ID : {0} Handle {1} State {2}", ClientID, m_sockCtx.Sock.Handle, m_connState);

            Logger.InfoLog( "ServerConn::OnClose() : Type = {0}, EndPoint = {1}", DestServerType, RemoteEndPoint );

            base.OnClose();
        }

        protected void SendErrorPkt(S2SPacketEnum protocol, ClientErrorMessage detail_code)
        {
#if DEBUG
            Logger.DebugLog("SendErrorPkt protocol = {0},  detail code = {1}", (S2SPacketEnum)protocol, detail_code);
#endif
            //this.Send(C2SPacketEnum.CMD_PacketRelayPacket, new PktS2CClientErrorMessage((Int32)protocol, (Int32)detail_code));
        }

        protected override void MakePacketDelegate(ManagedServer server)
        {
            base.RegistDelegate(server.PktDelegateMgr.S2SEncoder,
                                    server.PktDelegateMgr.S2SDecoder,
                                    server.PktDelegateMgr.S2SReceiver);
        }

        public override void MsgDispatch(int cmd, object pkt)
        {
            Interlocked.Increment(ref ServerConnection.PacketThroughput);
        }

        public override bool Send(S2SPacketEnum cmd, Object pkt)
        {

            if (m_sockCtx != null && m_sockCtx.Sock != null)
            {
                if (m_sockCtx.Sock.Connected == false)
                    return false;

                return base.Send(cmd, pkt);
            }

            return false;
        }


        public override bool Send(C2SPacketEnum cmd, Object pkt)
        {
            throw new NotImplementedException("Check: FAIL ServerConn is Not Using C2S Send");
        }

        public bool Connect(String ip, int port)
        {
            try
            {
                this.m_sockCtx.Sock.Connect(ip, port);
            }
            catch (System.Exception ex)
            {
                Logger.ExceptionLog(ex);
            }

            return this.m_sockCtx.Sock.Connected;
        }


		public virtual void OnConnect()
		{
			SendToBeginning();
		}

		public virtual void SendToBeginning()
		{
			SendServerAuth = true;
		}
	}
}
