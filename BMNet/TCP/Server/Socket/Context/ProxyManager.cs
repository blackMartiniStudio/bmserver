﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMNetwork.Managed;

namespace BMNetwork
{
    abstract public class ProxyManager<T>
    {
        private bool m_isInitialized = false;

        private List<T> m_listClient = new List<T>();
        protected Object lockDicId2Client = new Object();

        public bool Initialize()
        {
            if (m_isInitialized) return false;

            m_isInitialized = true;


            return true;
        }

        public void Terminate()
        {
            if (!m_isInitialized) return;

            m_listClient.Clear();
            m_isInitialized = false;
        }

        public void Update(Int64 elapsedTime, Int64 accumTime)
        {
            //foreach (var obj in this.m_dicId2Client.Values)
            //{
            //    obj.Update(elapsedTime, accumTime);
            //}
        }

        

        protected abstract T CreateNode(Object authorizePkt, ServerConnection connection);

        private void Add(T proxy)
        {
            lock (lockDicId2Client)
            {
                m_listClient.Add(proxy);
            }
        }

        public bool OnAuthorize(Object authorizePkt, ServerConnection connection)
        {
            T node =CreateNode(authorizePkt, connection);
            if (node == null)
                return false;

            Add(node);
            return true;
        }
    }
}
