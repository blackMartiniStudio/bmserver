﻿using BMDK;
using System;

//using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;

namespace BMNet
{
	// socket 디버그용, 소켓의 상태 변화 히스토리
	public enum DBG_STATE
	{
		DBG_CREATING,
		DBG_CREATED,
		DBG_CLOSING,
		DBG_CLOSED,
		DBG_REUSING,
		DBG_ACCEPTING,
		DBG_ACCEPTED,
		DBG_ACCEPTING_FAILED,
		DBG_DISCONNECTING,
		DBG_TRY_TRANSMITFILE,
		DBG_TRANSMITFILE,
		DBG_TRANSMITEDFILE,
		DBG_TRANSMITEDFILE_FAILED,
		DBG_FINI,
		DBG_REINIT,
		DBG_SETLINK,

		DBG_DISCON_FROM_eIOState_Accepted,
		DBG_DISCON_FROM_eIOState_AcceptFailed,
		DBG_DISCON_FROM_eIOState_FirstRecvTried,
		DBG_DISCON_FROM_eIOState_PossibleSend,
	};

	public class BMServerTcpSocket : BMSocket
	{
		private UInt64 m_uidLink;
		private AddressFamily m_addressFamily;

		private bool m_IsAccepting;
		private bool m_IsNodelayOption;

		private ILinkReleaseListener m_kLinkReleaseListner;
		private IAcceptor           m_kAcceptor;
		private IDisconnector       m_kDisconnector;
		private ISender             m_kSender;
		private IReceiver           m_kReceiver;

		private ITcpSocketIO        m_kIO;

		private List<DBG_STATE>     m_liStateHistory;
		private UInt64              m_linkHistory;

		private byte[]              m_RecvBuffer;
		private BMConcurrentQueue<SendBuffer>   m_quSendBuffer;

		private Int32               m_nSendPendingCounter;
		private Int32               m_nSendPendingLimitCount;

		public IDisconnector Disconnector
		{
			get { return m_kDisconnector; }
		}

		public Int32 SendPendingCount
		{
			get { return m_nSendPendingCounter; }
		}

		public Byte[] RecvBuffer
		{
			get { return m_RecvBuffer; }
		}

		public AddressFamily Address
		{
			get { return m_addressFamily; }
		}

		public BMServerTcpSocket( IAcceptor _kAcceptor, IDisconnector _kDisconnector, ISender _kSender, IReceiver _kReceiver )
		{
			// TODO: Complete member initialization
			this.m_kAcceptor = _kAcceptor;
			this.m_kDisconnector = _kDisconnector;
			this.m_kSender = _kSender;
			this.m_kReceiver = _kReceiver;
			m_kIO = new BMTcpSocketIO( this );
			m_liStateHistory = new List<DBG_STATE>();

			m_RecvBuffer = new byte[(Int32)NetCommon.MAX_PACKET_SIZE];
			m_quSendBuffer = new BMConcurrentQueue<SendBuffer>();
		}

		internal bool Init( AddressFamily addressFamily, bool bAccept, bool nodelay )
		{
			//if( false == CreateSocket( addressFamily, SocketType.Stream, ProtocolType.Tcp, nodelay, true ) )
			//	return false;

			InitContext();
			return true;
		}

		internal bool ReInit( String strMember )
		{
			// 로그
			BMLogger.InfoLog( "ReInit Member[{0}]", strMember );

			if( true == IsSocketOpened() )
			{
				// 로그
				BMLogger.ErrLog( "ReInit Fail. Already Socket Opened. Member[{0}] ", strMember );

				return false;
			}

			AddStateHistory( DBG_STATE.DBG_REINIT );
			bool result = Init( m_addressFamily, m_IsAccepting, m_IsNodelayOption );
			DumpStateHistory();

			return result;
		}

		public void Fini( String where )
		{
			// 로그
			BMLogger.DebugLog( " Socket Finish. : {0}", where );

			AddStateHistory( DBG_STATE.DBG_FINI );
			ResetIOPendingCounter();
			ReleaseLink();
			Shotdown( SocketShutdown.Both );
			CloseSocket( where );
			DumpStateHistory();
		}

		public override bool CreateSocket( AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType, bool bNoDelay, bool bReuse )
		{
			AddStateHistory( DBG_STATE.DBG_CREATING );
			bool result = base.CreateSocket( addressFamily, socketType, protocolType, bNoDelay, bReuse );
			AddStateHistory( DBG_STATE.DBG_CREATED );

			m_addressFamily = addressFamily;
			m_IsNodelayOption = bNoDelay;

			return result;
		}

		public override void CloseSocket( string strWhere )
		{
			AddStateHistory( DBG_STATE.DBG_CLOSING );
			base.CloseSocket( strWhere );
			AddStateHistory( DBG_STATE.DBG_CLOSED );
		}

		private void InitContext()
		{
		}

		internal void SetSocket( Socket socket )
		{
			m_kSocket = socket;
		}

		internal void SetLink( UInt64 uid )
		{
			m_uidLink = uid;

			// 필요에 따라서 아래 부분  lock 할 필요가 생길 수 있다.

			AddStateHistory( DBG_STATE.DBG_SETLINK );
			m_linkHistory = m_uidLink;
			DumpStateHistory();
		}

		internal UInt64 GetLink()
		{
			return m_uidLink;
		}

		internal void ReleaseLink()
		{
			if( m_kLinkReleaseListner != null )
			{
				m_kLinkReleaseListner.OnReleaseLink( m_uidLink );
			}

			m_uidLink = 0;
		}

		public void SetSocketIO( ITcpSocketIO _kSocketIO )
		{
			m_kIO = _kSocketIO;
		}

		internal ITcpSocketIO GetIO()
		{
			return m_kIO;
		}

		public void SetLinkListener( ILinkReleaseListener _kLinkReleaseListener )
		{
			m_kLinkReleaseListner = _kLinkReleaseListener;
		}

		public void SetSendPendingLimitCount( int _nSendPendingLimitCount )
		{
			if( _nSendPendingLimitCount < 0 )
				return;

			m_nSendPendingLimitCount = _nSendPendingLimitCount;
		}

		public bool IncreaseSendPendingCounter()
		{
			if( m_nSendPendingLimitCount <= m_nSendPendingCounter )
				return false;

			Interlocked.Increment( ref m_nSendPendingCounter );

			return true;
		}

		public void DecreaseSendPendingCounter()
		{
			if( m_nSendPendingCounter <= 0 )
			{
				// 로그
				BMLogger.WarnLog( "DecreaseSendPendingCounter Warning. : SendPendingCount[{0}]", m_nSendPendingCounter );
				return;
			}

			Interlocked.Decrement( ref m_nSendPendingCounter );

			return;
		}

		private void ResetIOPendingCounter()
		{
			m_nSendPendingCounter = 0;
		}

		public bool IsNeedAcceptingSocket()
		{
			return m_IsAccepting;
		}

		public virtual bool Accept()
		{
			if( false == GetIO().OnStartAccept() )
				return false;

			m_IsAccepting = true;

			return m_kAcceptor.Accept( this );
		}

		public virtual bool Recv()
		{
			if( false == IsSocketOpened() )
				return false;

			if( false == GetIO().OnStartRecv() )
				return false;

			if( false == m_kReceiver.Recv( this ) )
			{
				GetIO().OnCancelRecv();
				return false;
			}

			return true;
		}

		public virtual bool Send( LinkedList<SendBuffer> _liBuff )
		{
			if( false == IsSocketOpened() )
			{
				//  로그
				return false;
			}

			EnqueueSendBuffer( _liBuff );

			if( false == m_kSender.Send( this ) )
			{
				GetIO().OnCancelSend();
				return false;
			}

			return true;
		}

		internal bool Disconnect( String reason )
		{
			// 로그
			BMLogger.DebugLog( "BMServerTcpSocket Disconnect LinkUID[{0}] : {1}", m_uidLink, reason );

			if( false == GetIO().OnDisconnect() )
				return false;

			Shotdown( SocketShutdown.Both );

			return true;
		}

		private void Shotdown( SocketShutdown _eShutdown = SocketShutdown.Both )
		{
			Sock.Shutdown( _eShutdown );
		}

		public void HardDisconnect( UInt64 uidLink )
		{
			Disconnect( MethodBase.GetCurrentMethod().Name );
		}

		public void OnPossibleSend()
		{
			if( false == GetIO().OnPossibleSend() )
			{
				// 로그
				BMSocketErrorHandler.HandleError( this, FileFunction.__Function() );
				return;
			}

			// accept되자마자 send를 할 수 있는 구조가 있을 수 있음
			//m_kAcceptor.OnAccept(this);
		}

		public bool Reuse( String where )
		{
			return true;
		}

		internal void AddStateHistory( DBG_STATE eState )
		{
#if DEBUG
			m_liStateHistory.Add( eState );
#endif
		}

		public void DumpStateHistory()
		{
#if DEBUG
			// 로그

			foreach( DBG_STATE state in m_liStateHistory )
			{
				// 로그 남기기
			}
#endif
		}

		private void EnqueueSendBuffer( LinkedList<SendBuffer> _liBuff )
		{
			foreach( SendBuffer kBuffer in _liBuff )
			{
				m_quSendBuffer.Enqueue( kBuffer );
			}
		}

		internal bool DequeueSendBuffer( out SendBuffer kBuffer )
		{
			return m_quSendBuffer.TryDequeue( out kBuffer );
		}
	}
}