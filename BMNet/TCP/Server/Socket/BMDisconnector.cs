﻿using BMDK;
using System;
using System.Net.Sockets;

namespace BMNet
{
	public interface IDisconnectListener
	{
		void OnDisconnect( UInt64 uidLink );
	};

	public class BMDisconnector : IDisconnector
	{
		private IDisconnectListener m_kListener;

		internal bool Init()
		{
			return true;
		}

		public void SetListener( IDisconnectListener kListner )
		{
			m_kListener = kListner;
		}

		public bool Disconnect( BMServerTcpSocket _kSocket )
		{
			if( false == _kSocket.GetIO().OnStartTransmitFile() )
			{
				// 로그
				BMLogger.WarnLog( "Disconnect OnStartTransmitFile Fail." );

				return false;
			}

			_kSocket.Sock.Shutdown( SocketShutdown.Both );
			if( _kSocket.GetLink() == 0 )
			{
				BMSocketErrorHandler.HandleError( _kSocket );
				return false;
			}

			_kSocket.AddStateHistory( DBG_STATE.DBG_TRY_TRANSMITFILE );

			// TRANSMITFILE 처리를 한다.

			_kSocket.AddStateHistory( DBG_STATE.DBG_TRANSMITFILE );
			_kSocket.DumpStateHistory();

			_kSocket.CloseSocket( FileFunction.__Function() );

			OnDisconnect( _kSocket.GetLink() );

			return true;
		}

		public void OnDisconnect( UInt64 link )
		{
			if( null == m_kListener )
				return;

			m_kListener.OnDisconnect( link );
		}
	}
}