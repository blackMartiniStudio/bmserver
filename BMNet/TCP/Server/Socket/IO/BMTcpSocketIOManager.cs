﻿using BMDK;
using System;

//using System.Collections.Concurrent;
using System.Collections.Generic;

namespace BMNet
{
	using TRY_ACCEPTINGS = BMConcurrentQueue<SocketTrying>;

	using TRY_DISCONNECTINGS = BMConcurrentQueue<SocketTrying>;
	using TRY_RECVINGS = BMConcurrentQueue<SocketTrying>;

	public class BMTcpSocketIOMgr : ITcpSocketIOMgr
	{
		private ITcpSockFactory m_kTcpSockFactory;
		private BMDisconnector  m_kDisconnector;
		private ITick           m_kTick;

		private List<BMServerTcpSocket> m_liSocket = new List<BMServerTcpSocket>();

		private TRY_RECVINGS m_TryRecvings = new TRY_RECVINGS();
		private TRY_DISCONNECTINGS m_TryDisconnectings = new TRY_DISCONNECTINGS();
		private TRY_ACCEPTINGS m_TryAcceptings = new TRY_ACCEPTINGS();

		private static readonly Int64 FIRST_RECV_DELAY = TimeSpan.TicksPerSecond / 2;
		private static readonly Int64 DISCONNECT_TICK = TimeSpan.TicksPerSecond / 2;

		public BMTcpSocketIOMgr( ITcpSockFactory _kTcpSockFactory, BMDisconnector _kDisconnector, ITick timeTick )
		{
			m_kTcpSockFactory = _kTcpSockFactory;
			m_kDisconnector = _kDisconnector;
			m_kTick = timeTick;
		}

		public bool Init( Int32 nSocketPoolSize, bool bNoDelay )
		{
			if( false == CreateSocketPool( bNoDelay, nSocketPoolSize ) )
				return false;

			return true;
		}

		private bool CreateSocketPool( bool bNoDelay, int nSocketPoolSize )
		{
			if( m_liSocket.Count > 0 )
				return false;

			if( nSocketPoolSize <= 0 )
				return false;

			for( UInt32 nCnt = 0; nCnt < nSocketPoolSize; ++nCnt )
			{
				BMServerTcpSocket kSocket;
				if( false == m_kTcpSockFactory.NewTcpSocket( bNoDelay, out kSocket ) )
				{
					ClearSockets();
					return false;
				}

				ITcpSocketIO socketIO = kSocket.GetIO();
				socketIO.SetMgr( this );
				socketIO.SetSN( nCnt );

				m_liSocket.Add( kSocket );
			}

			return true;
		}

		private void ClearSockets()
		{
			foreach( BMServerTcpSocket socket in m_liSocket )
			{
				m_kTcpSockFactory.DelTcpSocket( socket );
			}

			m_liSocket.Clear();
		}

		internal bool StartAccept()
		{
			foreach( BMServerTcpSocket kSocket in m_liSocket )
			{
				if( false == kSocket.Accept() )
				{
					ClearSockets();
					return false;
				}
			}

			return true;
		}

		public void FiniListen()
		{
			ClearSockets();
		}

		public Int32 GetListenSocketCount()
		{
			return m_liSocket.Count;
		}

		public Int32 GetFirstRecvTryingsCount()
		{
			return m_TryRecvings.Count;
		}

		public bool TryGetSocket( Int32 nID, out BMServerTcpSocket _kSocket )
		{
			_kSocket = null;
			if( nID < 0 && m_liSocket.Count <= nID )
				return false;

			_kSocket = m_liSocket[nID];
			return true;
		}

		public virtual void OnFirstRecvTried( BMServerTcpSocket socket )
		{
			m_TryRecvings.Enqueue( new SocketTrying( socket, m_kTick ) );
		}

		public virtual void Disconnect( BMServerTcpSocket socket )
		{
			m_TryDisconnectings.Enqueue( new SocketTrying( socket, m_kTick ) );
		}

		public virtual void Accept( BMServerTcpSocket socket )
		{
			m_TryAcceptings.Enqueue( new SocketTrying( socket, m_kTick ) );
		}

		internal void Update()
		{
			Int64 nowTicks = m_kTick.GetNowTick();

			UpdateRecvTryings( nowTicks );
			UpdateDisconnectings( nowTicks );
			UpdateAcceptings( nowTicks );
		}

		private void UpdateRecvTryings( long nowTicks )
		{
			Int32 nCount = m_TryRecvings.Count;

			for( Int32 nID = 0; nID < nCount; ++nID )
			{
				SocketTrying recvTrying;
				if( false == m_TryRecvings.TryPeek( out recvTrying ) )
					break;

				Int64 elapsed = nowTicks - recvTrying.Tick;
				if( elapsed < FIRST_RECV_DELAY )
					break;

				SocketTrying dequeueTrying;
				if( recvTrying.DisconCount == recvTrying.Socket.GetIO().GetDisconnectCount() )
				{
					recvTrying.Socket.OnPossibleSend();
				}

				m_TryRecvings.TryDequeue( out dequeueTrying );
			}
		}

		private void UpdateDisconnectings( long nowTicks )
		{
			Int32 nCount = m_TryDisconnectings.Count;

			for( Int32 nID = 0; nID < nCount; ++nID )
			{
				SocketTrying desconTrying;
				if( false == m_TryDisconnectings.TryPeek( out desconTrying ) )
					break;

				ITcpSocketIO socketIO = desconTrying.Socket.GetIO();

				Int64 elapsed = nowTicks - desconTrying.Tick;
				if( elapsed < DISCONNECT_TICK )
					break;

				SocketTrying dequeueTrying;
				if( false == socketIO.IsSendFinished() )
				{
					m_TryDisconnectings.Enqueue( new SocketTrying( desconTrying.Socket, m_kTick ) );
					m_TryDisconnectings.TryDequeue( out dequeueTrying );
					continue;
				}

				m_kDisconnector.Disconnect( desconTrying.Socket );
				m_TryDisconnectings.TryDequeue( out dequeueTrying );

				Accept( dequeueTrying.Socket );
			}
		}

		private void UpdateAcceptings( long nowTicks )
		{
			Int32 nCount = m_TryAcceptings.Count;

			for( Int32 nID = 0; nID < nCount; ++nID )
			{
				SocketTrying acceptTrying;
				if( false == m_TryAcceptings.TryPeek( out acceptTrying ) )
					break;

				ITcpSocketIO socketIO = acceptTrying.Socket.GetIO();

				Int64 elapsed = nowTicks - acceptTrying.Tick;
				if( elapsed < DISCONNECT_TICK )
					break;

				SocketTrying dequeueTrying;
				if( false == socketIO.IsIOFinished() )
				{
					m_TryAcceptings.Enqueue( new SocketTrying( acceptTrying.Socket, m_kTick ) );
					m_TryAcceptings.TryDequeue( out dequeueTrying );
					continue;
				}

				acceptTrying.Socket.Accept();
				m_TryAcceptings.TryDequeue( out dequeueTrying );
			}
		}
	}
}