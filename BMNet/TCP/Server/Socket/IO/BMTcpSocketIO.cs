﻿using System;
using System.Threading;

namespace BMNet
{
	public class BMTcpSocketIO : ITcpSocketIO
	{
		private BMServerTcpSocket   m_kTcpSocket;
		private ITcpSocketIOMgr     m_kSocketIOMgr;

		private Int32               m_IOState;
		private Int32               m_SendCount;
		private Int32               m_DisconCount;
		private Int32               m_IsRecv;
		private UInt32              m_SN;

		public BMTcpSocketIO( BMServerTcpSocket _kServerTcpSocket )
		{
			this.m_kTcpSocket = _kServerTcpSocket;
			m_IOState = (Int32)IOState.IOState_None;
		}

		public bool OnStartAccept()
		{
			if( true == TransToIOState( IOState.IOState_None, IOState.IOState_Accepting ) )
				return true;

			if( true == TransToIOState( IOState.IOState_Closed, IOState.IOState_Accepting ) )
				return true;

			if( true == TransToIOState( IOState.IOState_TryingTransmitFile, IOState.IOState_Accepting ) )
				return true;

			// 로그

			return false;
		}

		public bool OnStartSend()
		{
			if( false == IsIOState( IOState.IOState_PossibleSend ) )
			{
				// 로그
				return false;
			}

			IncreaseSendCount();

			return true;
		}

		public void OnCancelSend()
		{
			DecreaseSendCount();
		}

		public bool OnStartRecv()
		{
			if( true == IsIOState( IOState.IOState_Closed ) )
				return false;

			if( true == IsIOState( IOState.IOState_TryingTransmitFile ) )
				return false;

			if( false == SetRecv() )
			{
				// 로그
				return false;
			}

			if( true == IsIOState( IOState.IOState_PossibleSend ) )
			{
				return true;
			}

			if( true == IsIOState( IOState.IOState_Accepted ) )
			{
				return true;
			}

			if( true == IsIOState( IOState.IOState_FirstRecvTried ) )
			{
				return true;
			}

			SetNoRecv();

			return false;
		}

		public void OnCancelRecv()
		{
			SetNoRecv();
		}

		public bool OnDisconnect()
		{
			Interlocked.Increment( ref m_DisconCount );

			if( true == TransToIOState( IOState.IOState_Accepted, IOState.IOState_Closed ) )
			{
				m_kTcpSocket.AddStateHistory( DBG_STATE.DBG_DISCON_FROM_eIOState_AcceptFailed );
				m_kSocketIOMgr.Disconnect( m_kTcpSocket );
				return true;
			}

			if( true == TransToIOState( IOState.IOState_AcceptFailed, IOState.IOState_Closed ) )
			{
				m_kTcpSocket.AddStateHistory( DBG_STATE.DBG_DISCON_FROM_eIOState_AcceptFailed );
				m_kSocketIOMgr.Disconnect( m_kTcpSocket );
				return true;
			}

			if( true == TransToIOState( IOState.IOState_FirstRecvTried, IOState.IOState_Closed ) )
			{
				m_kTcpSocket.AddStateHistory( DBG_STATE.DBG_DISCON_FROM_eIOState_FirstRecvTried );
				m_kSocketIOMgr.Disconnect( m_kTcpSocket );
				return true;
			}

			if( true == TransToIOState( IOState.IOState_PossibleSend, IOState.IOState_Closed ) )
			{
				m_kTcpSocket.AddStateHistory( DBG_STATE.DBG_DISCON_FROM_eIOState_PossibleSend );
				m_kSocketIOMgr.Disconnect( m_kTcpSocket );
				return true;
			}

			// 로그

			return false;
		}

		public bool OnAccept()
		{
			if( false == TransToIOState( IOState.IOState_Accepting, IOState.IOState_Accepted ) )
			{
				// 로그
				return false;
			}

			return true;
		}

		public void OnAcceptFailed()
		{
			if( false == TransToIOState( IOState.IOState_Accepting, IOState.IOState_AcceptFailed ) )
			{
				// 로그
			}
		}

		public bool OnFirstRecvTried()
		{
			if( false == TransToIOState( IOState.IOState_Accepted, IOState.IOState_FirstRecvTried ) )
			{
				// 로그
				return false;
			}

			m_kSocketIOMgr.OnFirstRecvTried( m_kTcpSocket );

			return true;
		}

		public bool OnPossibleSend()
		{
			return TransToIOState( IOState.IOState_FirstRecvTried, IOState.IOState_PossibleSend );
		}

		public void OnSend()
		{
			DecreaseSendCount();
		}

		public bool OnRecv()
		{
			return SetNoRecv();
		}

		public bool OnStartTransmitFile()
		{
			return TransToIOState( IOState.IOState_Closed, IOState.IOState_TryingTransmitFile );
		}

		public Int32 GetDisconnectCount()
		{
			return Interlocked.Exchange( ref m_DisconCount, 0 );
		}

		public void OnError()
		{
			Interlocked.Increment( ref m_DisconCount );

			if( false == TransToIOState( (IOState)m_IOState, IOState.IOState_None ) )
			{
				// 로그
				return;
			}

			m_kSocketIOMgr.Accept( m_kTcpSocket );
		}

		public bool IsRecvFinished()
		{
			return 0 == Interlocked.Exchange( ref m_IsRecv, 0 );
		}

		public bool IsSendFinished()
		{
			return 0 == Interlocked.Exchange( ref m_SendCount, 0 );
		}

		public bool IsIOFinished()
		{
			return IsRecvFinished() && IsSendFinished();
		}

		public void SetSN( UInt32 sn )
		{
			m_SN = sn;
		}

		public UInt32 GetSN()
		{
			return m_SN;
		}

		public void SetMgr( ITcpSocketIOMgr _kMgr )
		{
			m_kSocketIOMgr = _kMgr;
		}

		public bool IsIOState( IOState state )
		{
			return state == (IOState)Interlocked.Add( ref m_IOState, (Int32)IOState.IOState_None );
		}

		private bool TransToIOState( IOState fromState, IOState toState )
		{
			return Interlocked.CompareExchange( ref m_IOState, (Int32)toState, (Int32)fromState ) == (Int32)fromState;
		}

		private bool SetRecv()
		{
			return 0 == Interlocked.CompareExchange( ref m_IsRecv, 1, 0 );
		}

		private bool SetNoRecv()
		{
			return 1 == Interlocked.CompareExchange( ref m_IsRecv, 0, 1 );
		}

		private void IncreaseSendCount()
		{
			Interlocked.Increment( ref m_SendCount );
		}

		private void DecreaseSendCount()
		{
			if( true == IsSendFinished() )
			{
				// 로그
				return;
			}

			Interlocked.Decrement( ref m_SendCount );
		}
	}
}