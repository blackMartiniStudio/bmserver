﻿using System;
using System.Collections.Generic;

namespace BMNet
{
	public class BMLinkSocket
	{
		protected UInt64                m_UID;
		protected BMServerTcpSocket     m_kSocket;
		protected BMCommandStream       m_kCommandStream;
		protected BMCommandManager      m_kCommandManager;

		protected BMKeepingAliveData    m_kKeepingAliveData;

		private bool                m_bAllowed      = false;
		private bool                m_bDisconnect   = false;

		public BMLinkSocket( UInt64 uidServer, UInt64 newUID, BMServerTcpSocket _kSocket, BMCommandManager _kCommandManager, ICommandBuilder _kCmdBuilder )
		{
			this.m_UID = newUID;
			this.m_kSocket = _kSocket;
			this.m_kCommandManager = _kCommandManager;

			m_kCommandStream = new BMCommandStream( _kCommandManager, _kCmdBuilder );
			m_kKeepingAliveData = new BMKeepingAliveData();

			SetAllowed( true );
			SetRecvTime( DateTime.Now.Ticks );
		}

		internal UInt64 GetUID()
		{
			return m_UID;
		}

		public void Send( LinkedList<SendBuffer> _liBuff )
		{
			m_kSocket.Send( _liBuff );
		}

		public bool Disconnect( String reason )
		{
			m_bDisconnect = true;
			return m_kSocket.Disconnect( reason );
		}

		internal void HardDisconnect()
		{
			m_kSocket.HardDisconnect( m_UID );
		}

		public void SetAllowed( bool bAllowed )
		{
			m_bAllowed = bAllowed;
		}

		internal bool IsAllowed()
		{
			return m_bAllowed;
		}

		internal bool IsLinkValid()
		{
			return m_kSocket.IsSocketOpened();
		}

		internal BMCommandStream GetCommandStream()
		{
			return m_kCommandStream;
		}

		internal string GetRemoteIP()
		{
			return m_kSocket.Address.ToString();
		}

		internal void ReallocUID( ulong uidReAlloc )
		{
			m_UID = uidReAlloc;
			m_kSocket.SetLink( uidReAlloc );
		}

		internal bool IsDisconnected()
		{
			return m_bDisconnect;
		}

		internal bool CheckAlive( long nNowTick, long nAllowedTime )
		{
			return m_kKeepingAliveData.CheckAlive( nNowTick, nAllowedTime );
		}

		internal void SetRecvTime( Int64 nowTick )
		{
			m_kKeepingAliveData.SetRecvTime( nowTick );
		}

		internal Int64 GetRecvTime()
		{
			return m_kKeepingAliveData.GetRecvTime();
		}
	}
}