﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace BMNet
{
	public class BMSocketLinkManager
	{
		private ConcurrentDictionary<UInt64, BMLinkSocket> m_LinkMap = new ConcurrentDictionary<UInt64, BMLinkSocket>();
		private ConcurrentDictionary<UInt64, UInt64> m_ReAllocUIDMap = new ConcurrentDictionary<UInt64, UInt64>();

		public List<BMLinkSocket> LinkSockets
		{
			get { return m_LinkMap.Values.ToList(); }
		}

		public Int32 LinkCount
		{
			get { return m_LinkMap.Count; }
		}

		public void Add( BMLinkSocket _kLinkSocket )
		{
			m_LinkMap.TryAdd( _kLinkSocket.GetUID(), _kLinkSocket );
		}

		public bool Delete( UInt64 uidLink, out UInt64 outUIDReAlloc )
		{
			outUIDReAlloc = uidLink;

			BMLinkSocket kLinkSocket;
			if( false == m_LinkMap.TryGetValue( uidLink, out kLinkSocket ) )
			{
				UInt64 uidReAlloc = 0;
				if( false == m_ReAllocUIDMap.TryGetValue( uidLink, out uidReAlloc ) )
					return false;

				if( false == m_LinkMap.TryGetValue( uidReAlloc, out kLinkSocket ) )
					return false;

				outUIDReAlloc = uidReAlloc;
			}

			m_LinkMap.TryRemove( outUIDReAlloc, out kLinkSocket );

			UInt64 removeValue;
			m_ReAllocUIDMap.TryRemove( uidLink, out removeValue );

			return true;
		}

		public bool TryGetSocket( UInt64 uidLink, out BMLinkSocket kLinkSocket )
		{
			return m_LinkMap.TryGetValue( uidLink, out kLinkSocket );
		}

		internal bool TryGetLinkFromReAllocTable( UInt64 uidLink, out BMLinkSocket kLinkSocket )
		{
			kLinkSocket = null;

			UInt64 uidReAlloc;
			if( false == m_ReAllocUIDMap.TryRemove( uidLink, out uidReAlloc ) )
			{
				return false;
			}

			return m_LinkMap.TryGetValue( uidReAlloc, out kLinkSocket );
		}

		internal bool ReallockUID( ulong uidOriginal, ulong uidReAlloc )
		{
			BMLinkSocket kSocket;
			if( false == m_LinkMap.TryRemove( uidOriginal, out kSocket ) )
				return false;

			kSocket.ReallocUID( uidReAlloc );

			m_LinkMap.TryAdd( uidReAlloc, kSocket );
			m_ReAllocUIDMap.TryAdd( uidOriginal, uidReAlloc );

			return true;
		}
	}
}