﻿using BMDK;
using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

namespace BMNet
{
	public interface IConnectListener
	{
		void OnConnect( BMServerTcpSocket socket );
	};

	public interface BMConnectSocketReceiver
	{
		void SetConnectSocket( BMServerTcpSocket conSocket );
	};

	public class ConnectSocketInfo
	{
		public String addr;
		public Int32  port;
		public BMServerTcpSocket socket;
		public BMConnectSocketReceiver conSocketReciever;
		public Int32  tryCount;

		public string ToDebugString()
		{
			return string.Format( "{0}:{1} retry[{2}] ", addr, port, tryCount );
		}
	}

	public class BMConnector
	{
		private IAcceptor           m_kAcceptor;
		private IDisconnector       m_kDisconnector;
		private ISender             m_kSender;
		private IReceiver           m_kReceiver;
		private IConnectListener    m_kConnectListener;
		private Int32 MaxRetryConnect = -1;

		public BMConnector()
		{
		}

		public BMConnector( IAcceptor m_kAcceptor, IDisconnector m_kDisconnector, ISender m_kSender, IReceiver m_kReceiver )
		{
			this.m_kAcceptor = m_kAcceptor;
			this.m_kDisconnector = m_kDisconnector;
			this.m_kSender = m_kSender;
			this.m_kReceiver = m_kReceiver;
		}

		public void Connect( String addr, String port, String localNicAddr )
		{
			if( addr.Length == 0 )
				return;

			if( port.Length == 0 )
				return;

			if( localNicAddr.Length == 0 )
				return;

			String conAddr = NetworkHelper.RESERVED_IPv4_LOOPBACK;
			if( addr.ToUpper() != "LOCALHOST" )
			{
				conAddr = addr;
			}
		}

		public void Connect( List<ConnectSocketInfo> conSockInfos )
		{
			if( conSockInfos.Count == 0 )
				return;

			String localAddr = NetworkHelper.RESERVED_IPv4_LOOPBACK;
			foreach( ConnectSocketInfo kConnectInfo in conSockInfos )
			{
				if( kConnectInfo.addr.ToUpper() == "LOCALHOST" )
				{
					kConnectInfo.addr = localAddr;
				}

				kConnectInfo.socket = new BMServerTcpSocket( m_kAcceptor, m_kDisconnector, m_kSender, m_kReceiver );
				kConnectInfo.socket.CreateSocket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp, false, true );
				Connect( kConnectInfo );
			}
		}

		private void Connect( ConnectSocketInfo connInfo )
		{
			SocketAsyncEventArgs kEventArgs;
			if( false == NewSocketAsyncEventArgs( connInfo, out kEventArgs ) )
				return;

			bool willRaiseEvent = connInfo.socket.Sock.ConnectAsync( kEventArgs );
			if( false == willRaiseEvent )
			{
				ProcessConnect( kEventArgs );
			}
		}

		private void ReTryConnect( ConnectSocketInfo connInfo )
		{
			Connect( connInfo );
		}

		private void ProcessConnect( SocketAsyncEventArgs kEventArgs )
		{
			if( kEventArgs.SocketError == SocketError.Success )
			{
				ConnectSocketInfo kSocketInfo = kEventArgs.UserToken as ConnectSocketInfo;
				OnConnect( kSocketInfo );
				return;
			}

			if( IsRetryAbleSocketError( kEventArgs ) )
			{
				ConnectSocketInfo kInfo = kEventArgs.UserToken as ConnectSocketInfo;
				if( MaxRetryConnect == -1 || kInfo.tryCount < MaxRetryConnect )
				{
					kInfo.tryCount++;
					BMLogger.DebugLog( "ProcessConnect ReTryConnect - {0}", kInfo.ToDebugString() );
					ReTryConnect( kInfo );
					return;
				}

				ConnectError( kInfo );
				return;
			}
		}

		private void ConnectEventArgCompleted( object sender, SocketAsyncEventArgs kEventArgs )
		{
			ProcessConnect( kEventArgs );
		}

		private void OnConnect( ConnectSocketInfo kSocketInfo )
		{
			kSocketInfo.socket.Recv();

			if( null != kSocketInfo.conSocketReciever )
				kSocketInfo.conSocketReciever.SetConnectSocket( kSocketInfo.socket );
		}

		private bool NewSocketAsyncEventArgs( ConnectSocketInfo connInfo, out SocketAsyncEventArgs kEventArgs )
		{
			IPEndPoint endPoint = new IPEndPoint( IPAddress.Parse( connInfo.addr ), connInfo.port );

			kEventArgs = new SocketAsyncEventArgs();
			kEventArgs.RemoteEndPoint = endPoint;
			kEventArgs.DisconnectReuseSocket = true;
			kEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>( ConnectEventArgCompleted );

			kEventArgs.UserToken = connInfo;

			return true;
		}

		internal void SetListener( IConnectListener kListner )
		{
			m_kConnectListener = kListner;
		}

		private void ConnectError( ConnectSocketInfo kInfo )
		{
			kInfo.socket.Fini( FileFunction.__Function() );
		}

		private bool IsRetryAbleSocketError( SocketAsyncEventArgs kEventArgs )
		{
			return kEventArgs.SocketError == SocketError.TimedOut ||
					kEventArgs.SocketError == SocketError.ConnectionRefused ||
					kEventArgs.SocketError == SocketError.HostDown;
		}
	}
}