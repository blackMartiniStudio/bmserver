﻿using BMDK;
using System;

namespace BMNet
{
	public class SocketTrying
	{
		public SocketTrying( BMServerTcpSocket _kSocket, ITick _kTick )
		{
			m_kSocket = _kSocket;
			m_tryTick = _kTick.GetNowTick();
			m_sockDisconCount = _kSocket.GetIO().GetDisconnectCount();
		}

		private Int64 m_tryTick;
		private BMServerTcpSocket m_kSocket;
		private Int32 m_sockDisconCount;

		public Int64 Tick
		{
			get { return m_tryTick; }
		}

		public BMServerTcpSocket Socket
		{
			get { return m_kSocket; }
		}

		public Int32 DisconCount
		{
			get { return m_sockDisconCount; }
		}
	};
}