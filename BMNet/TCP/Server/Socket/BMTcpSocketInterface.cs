﻿using System;

namespace BMNet
{
	public enum IOState
	{
		IOState_None,
		IOState_Accepting,
		IOState_Accepted,
		IOState_AcceptFailed,
		IOState_FirstRecvTried,
		IOState_PossibleSend,
		IOState_Closed,
		IOState_TryingTransmitFile,
	};

	public interface ILinkReleaseListener
	{
		void OnReleaseLink( UInt64 uid );
	};

	public interface ITcpSocketIOMgr
	{
		void OnFirstRecvTried( BMServerTcpSocket socket );

		void Disconnect( BMServerTcpSocket socket );

		void Accept( BMServerTcpSocket socket );
	}

	public interface ITcpSockFactory
	{
		bool NewTcpSocket( bool nodelay, out BMServerTcpSocket _kSocket );

		void DelTcpSocket( BMServerTcpSocket socket );
	};

	public interface ITcpSocketIO
	{
		bool OnAccept();

		void OnAcceptFailed();

		bool OnFirstRecvTried();

		bool OnPossibleSend();

		void OnSend();

		bool OnRecv();

		bool OnStartTransmitFile();

		bool IsIOState( IOState state );

		UInt32 GetSN();

		Int32 GetDisconnectCount();

		void OnError();

		bool OnStartAccept();

		bool OnStartSend();

		void OnCancelSend();

		bool OnStartRecv();

		void OnCancelRecv();

		bool OnDisconnect();

		void SetMgr( ITcpSocketIOMgr _kMgr );

		bool IsRecvFinished();

		bool IsSendFinished();

		bool IsIOFinished();

		void SetSN( UInt32 sn );
	}

	public interface IAcceptor
	{
		bool Accept( BMServerTcpSocket socket );

		void OnAccept( BMServerTcpSocket socket );
	}

	public interface IDisconnector
	{
		bool Disconnect( BMServerTcpSocket socket );

		void OnDisconnect( UInt64 uidLink );
	}

	public interface ISender
	{
		bool Send( BMServerTcpSocket socket );
	}

	public interface IReceiver
	{
		bool Recv( BMServerTcpSocket socket );
	};
}