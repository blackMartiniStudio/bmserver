﻿using System;
using System.Runtime.CompilerServices;

namespace BMNet
{
	public class BMSocketErrorHandler
	{
		public static void HandleError( BMServerTcpSocket socket,
			[CallerMemberName] String strMember = "",
			[CallerFilePath] string sourceFilePath = "",
			[CallerLineNumber] int sourceLineNumber = 0 )
		{
			// 로그
			UInt64 link = socket.GetLink();
			socket.GetIO().OnError();
			socket.ReleaseLink();
			socket.CloseSocket( strMember );
			socket.ReInit( strMember );
			socket.Disconnector.OnDisconnect( link );
		}
	}
}