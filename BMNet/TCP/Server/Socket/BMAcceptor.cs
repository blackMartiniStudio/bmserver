﻿using System;
using System.Net.Sockets;
using System.Reflection;

namespace BMNet
{
	public interface IAcceptListener
	{
		void PreRecv( BMServerTcpSocket socket );

		void OnAccept( BMServerTcpSocket socket );
	};

	public class BMAcceptListener : IAcceptListener
	{
		public virtual void PreRecv( BMServerTcpSocket tcp_socket )
		{
		}

		public virtual void OnAccept( BMServerTcpSocket tcp_socket )
		{
		}
	};

	public class BMAcceptor : IAcceptor
	{
		private BMTCPListenSocket m_kTcpListenSocket;
		private IAcceptListener m_kListener;

		public BMAcceptor()
		{
			m_kListener = new BMAcceptListener();
		}

		internal bool Init( BMTCPListenSocket m_kListenSocket )
		{
			m_kTcpListenSocket = m_kListenSocket;

			return true;
		}

		public bool Accept( BMServerTcpSocket _kSocket )
		{
			SocketAsyncEventArgs kEventArgs;
			if( false == NewSocketAsyncEventArgs( _kSocket, out kEventArgs ) )
				return false;

			bool willRaiseEvent = m_kTcpListenSocket.Sock.AcceptAsync( kEventArgs );
			if( false == willRaiseEvent )
			{
				ProcessAccept( kEventArgs );
			}

			return true;
		}

		private void ListenEntry()
		{
		}

		private void AcceptEventArgCompleted( object sender, SocketAsyncEventArgs eventArgs )
		{
			ProcessAccept( eventArgs );
		}

		private void ProcessAccept( SocketAsyncEventArgs eventArgs )
		{
			BMServerTcpSocket kSocket = eventArgs.UserToken as BMServerTcpSocket;

			InheritAcceptSocketProperty( eventArgs.AcceptSocket, kSocket );

			if( eventArgs.SocketError != SocketError.Success )
			{
				kSocket.GetIO().OnAcceptFailed();
				OnRemoteConnectionClosed( kSocket );
				return;
			}

			OnAccept( kSocket );
		}

		public void OnAccept( BMServerTcpSocket _kSocket )
		{
			_kSocket.GetIO().OnAccept();
			m_kListener.PreRecv( _kSocket );

			if( false == _kSocket.Recv() )
			{
				OnRemoteConnectionClosed( _kSocket );
				return;
			}

			_kSocket.GetIO().OnFirstRecvTried();

			m_kListener.OnAccept( _kSocket );
		}

		internal bool NewSocketAsyncEventArgs( BMServerTcpSocket _kSocket, out SocketAsyncEventArgs _kEventArgs )
		{
			_kEventArgs = new SocketAsyncEventArgs();
			_kEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>( AcceptEventArgCompleted );
			_kEventArgs.DisconnectReuseSocket = true;

			_kEventArgs.UserToken = _kSocket;

			return true;
		}

		public void SetListener( BMAcceptListener kListner )
		{
			m_kListener = kListner;
		}

		private void OnRemoteConnectionClosed( BMServerTcpSocket kSocket )
		{
			kSocket.Disconnect( MethodBase.GetCurrentMethod().Name );
		}

		private void InheritAcceptSocketProperty( Socket kAcceptSocket, BMServerTcpSocket _kTcpSocket )
		{
			_kTcpSocket.SetSocket( kAcceptSocket );
		}
	}
}