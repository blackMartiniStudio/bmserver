﻿using BMDK;
using System;

//using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;

namespace BMNet
{
	public class SocketAsyncEventArgsPool
	{
		private Int32 m_nNextTokenID = 0;
		private BMConcurrentQueue<SocketAsyncEventArgs> m_quePool;

		public Int32 Count
		{
			get { return m_quePool.Count; }
		}

		public SocketAsyncEventArgsPool()
		{
			m_quePool = new BMConcurrentQueue<SocketAsyncEventArgs>();
		}

		internal Int32 AssignTokenId()
		{
			Int32 tokenId = Interlocked.Increment( ref m_nNextTokenID );
			return tokenId;
		}

		public SocketAsyncEventArgs Pop()
		{
			SocketAsyncEventArgs eventArgs;
			if( false == m_quePool.TryDequeue( out eventArgs ) )
				return null;

			return eventArgs;
		}

		public void Push( SocketAsyncEventArgs eventArgs )
		{
			if( eventArgs == null )
				return;

			m_quePool.Enqueue( eventArgs );
		}
	}
}