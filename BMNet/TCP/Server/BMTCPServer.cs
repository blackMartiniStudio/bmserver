﻿using BMDK;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;

namespace BMNet
{
	public class BMTCPServer : ITcpSockFactory
	{
		private BMAcceptor          m_kAcceptor;        // Disconnector와 협력해서 소켓풀을 관리한다.
		private BMConnector         m_kConnector;
		private BMDisconnector      m_kDisconnector;    // Acceptor와 협력해서 소켓풀을 관리한다
		private BMReceiver          m_kReceiver;        // 유저풀을 관리한다. 데이터송수신, 파싱처리
		private BMSender            m_kSender;
		private BMTCPListenSocket   m_kListenSocket;
		private BMTcpSocketIOMgr    m_kTcpSockIOMgr;

		private Int32   m_nListenPort               = 6000;
		private Int32   m_nSocketPoolSize           = 3000;
		private Int32   m_nSendPendingLimitCount    = Int32.MaxValue;

		private ILinkReleaseListener    m_kLinkReleaseListener;

		public BMTCPServer( TimeTick _kTimer )
		{
			m_kAcceptor = new BMAcceptor();     // Disconnector와 협력해서 소켓풀을 관리한다.
			m_kDisconnector = new BMDisconnector(); // Acceptor와 협력해서 소켓풀을 관리한다
			m_kReceiver = new BMReceiver();     // 유저풀을 관리한다. 데이터송수신, 파싱처리
			m_kSender = new BMSender();
			m_kListenSocket = new BMTCPListenSocket();

			m_kConnector = new BMConnector( m_kAcceptor, m_kDisconnector, m_kSender, m_kReceiver );

			m_kTcpSockIOMgr = new BMTcpSocketIOMgr( this, m_kDisconnector, _kTimer );
		}

		public bool Start( AddressFamily addressFamily, Int32 nListenPort, bool bNoDelay, ILinkReleaseListener kLinkReleaseListener
			, Int32 nSocketPoolSize = 3000, Int32 nSendPendingLimitCount = Int32.MaxValue, String strMyNetworkCardIP = "" )
		{
			m_nListenPort = nListenPort;
			m_nSocketPoolSize = nSocketPoolSize;
			m_nSendPendingLimitCount = nSendPendingLimitCount;
			m_kLinkReleaseListener = kLinkReleaseListener;

			if( false == Init() )
				return false;

			if( false == m_kListenSocket.Init( addressFamily, nListenPort, bNoDelay, true, strMyNetworkCardIP ) )
				return false;

			if( false == m_kListenSocket.Listen( m_kAcceptor ) )
				return false;

			if( false == m_kAcceptor.Init( m_kListenSocket ) )
				return false;

			if( false == m_kTcpSockIOMgr.Init( nSocketPoolSize, bNoDelay ) )
				return false;

			return m_kTcpSockIOMgr.StartAccept();
		}

		public void Stop()
		{
		}

		public bool Init(  )
		{
// 			Int32 nMaxWorkerThreads;
// 			Int32 nMinWorkerThreads;
// 			Int32 nCompletionPortThreads;
// 
// 			SetThreads( nMaxThreads, nMinThreads );
// 
// 			ThreadPool.GetMaxThreads( out nMaxWorkerThreads, out nCompletionPortThreads );
// 			ThreadPool.GetMinThreads( out nMinWorkerThreads, out nCompletionPortThreads );

			if( false == m_kDisconnector.Init() )
				return false;

			if( false == m_kReceiver.Init() )
				return false;

			if( false == m_kSender.Init() )
				return false;

			return true;
		}

		private void SetThreads( Int32 nMaxThreads, Int32 nMinThreads )
		{
			// 두 값중에 하나라도 0이라면 CLR에서 자동으로 설정되도록 한다.
			if( nMaxThreads == 0 || nMinThreads == 0 )
				return;

			int workerThreads, cmpThreads;

			ThreadPool.GetMaxThreads( out workerThreads, out cmpThreads );
			ThreadPool.SetMaxThreads( Math.Min( workerThreads, nMaxThreads ), cmpThreads );

			ThreadPool.GetMinThreads( out workerThreads, out cmpThreads );
			ThreadPool.SetMinThreads( Math.Max( workerThreads, nMinThreads ), cmpThreads );
		}

		public void Connect( List<ConnectSocketInfo> conSockInfos )
		{
			m_kConnector.Connect( conSockInfos );
		}

		public void SetAcceptorListener( BMAcceptListener kListner )
		{
			m_kAcceptor.SetListener( kListner );
		}

		public void SetConnectListener( IConnectListener kListner )
		{
			m_kConnector.SetListener( kListner );
		}

		public void SetDisconnectorListener( IDisconnectListener kListner )
		{
			m_kDisconnector.SetListener( kListner );
		}

		public void SetSenderListener( ISendListener kListner )
		{
			m_kSender.SetListener( kListner );
		}

		public void SetReceiverListener( IRecvListener kListner )
		{
			m_kReceiver.SetListener( kListner );
		}

		public void Update()
		{
			m_kTcpSockIOMgr.Update();
		}

		public bool NewTcpSocket( bool nodelay, out BMServerTcpSocket _kSocket )
		{
			_kSocket = new BMServerTcpSocket( m_kAcceptor, m_kDisconnector, m_kSender, m_kReceiver );
			if( false == _kSocket.Init( AddressFamily.InterNetwork, true, nodelay ) )
			{
				_kSocket = null;
				return false;
			}

			_kSocket.SetSendPendingLimitCount( m_nSendPendingLimitCount );
			_kSocket.SetLinkListener( m_kLinkReleaseListener );

			return true;
		}

		public void DelTcpSocket( BMServerTcpSocket socket )
		{
			socket = null;
		}
	}
}