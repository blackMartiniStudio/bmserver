﻿using System;
using BMDK;

namespace BMNet
{
	public class BMCommandServerSendListner : ISendListener
	{
		private BMNetServer m_kCommandServer;

		public BMCommandServerSendListner( BMNetServer _kNetServer )
		{
			m_kCommandServer = _kNetServer;
		}

		public void OnSend( UInt64 uid, SendBuffer _kSendBuffer )
		{
			m_kCommandServer.TrafficMonitor.RecordSend( _kSendBuffer.DataSize );
			m_kCommandServer.SendMgr.RestoreSendBuffer( _kSendBuffer );
		}

		public void OnHardDisconnect( UInt64 uidLink, String reason )
		{
			// 로그 : 강제 종료 통보 받음
			BMLogger.WarnLog( "OnHardDisconnect : 강제 종료 통보 받음" );
			new HardDisconnectImpl( m_kCommandServer, uidLink );
		}
	}
}