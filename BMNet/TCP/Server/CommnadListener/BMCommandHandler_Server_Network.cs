﻿using System;

namespace BMNet
{
	public class BMCommandHandler_Server_Network : BMCommandHandler
	{
		public BMCommandHandler_Server_Network( BMCommandCommunicator _kCommunicator )
			: base( _kCommunicator )
		{
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetDisconnect, OnNetworkDisconnect );
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetHardDisconnect, OnNetworkHardDisconnect );
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetAccept, OnNetworkAccept );
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetClear, OnLocal_NetClear );
			SetCmdHandler( (Int32)PacketCommandEnum.CMD_PacketNetReallocUID, OnNet_ReallocUID );
		}

		private BMCommandResult OnNetworkDisconnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketNetDisconnect kCmd = _kCommand.Packet as PacketNetDisconnect;

			NetServer( _kHandler ).Disconnect( "연결 끊어짐", kCmd.tdNetDisconnect.uidLink );
			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnNetworkHardDisconnect( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketNetHardDisconnect kCmd = _kCommand.Packet as PacketNetHardDisconnect;

			NetServer( _kHandler ).HardDisconnect( kCmd.tdNetHardDisconnect.uidLink );
			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnNetworkAccept( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			PacketNetAccept kCmd = _kCommand.Packet as PacketNetAccept;

			BMNetServer kServer = NetServer( _kHandler );

			UInt64 uidAlloc = kCmd.tdNetAccept.uidLink;
			UInt64 uidHost = kServer.GetUID();

			BMCommand kSendCmd = kServer.NewCommand( (Int32)PacketCommandEnum.CMD_PacketNetRelayConnect, uidAlloc );
			kSendCmd.SetPacket( new PacketNetRelayConnect( new TDNetRelayConnect( uidAlloc, uidHost, kServer.Timer.GetNowTick() ) ) );
			kServer.Post( kSendCmd );

			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnLocal_NetClear( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			return BMCommandResult.SUCCESS;
		}

		private BMCommandResult OnNet_ReallocUID( BMCommand _kCommand, BMCommandHandler _kHandler )
		{
			return BMCommandResult.SUCCESS;
		}

		protected virtual BMNetServer NetServer( BMCommandHandler _kHandler )
		{
			return _kHandler.GetCommandCommunicator() as BMNetServer;
		}
	}
}