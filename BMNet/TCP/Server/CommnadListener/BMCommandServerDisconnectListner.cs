﻿using System;

namespace BMNet
{
	public class BMCommandServerDisconnectListner : IDisconnectListener
	{
		private BMNetServer m_kCommandServer;

		public BMCommandServerDisconnectListner( BMNetServer _kNetServer )
		{
			m_kCommandServer = _kNetServer;
		}

		public virtual void OnDisconnect( UInt64 uidLink )
		{
			if( uidLink == 0 )
				return;

			m_kCommandServer.SendMgr.Clear( uidLink );

			// UID 값이 ReAlloc 되었을 경우를 대비하여
			UInt64 uidReAlloc = 0;
			if( false == m_kCommandServer.LinkMgr.Delete( uidLink, out uidReAlloc ) )
				return;

			uidLink = uidReAlloc == 0 ? uidLink : uidReAlloc;

			BMCommand kNewCommand = m_kCommandServer.NewLocalCommand( (Int32)PacketCommandEnum.CMD_PacketNetClear );
			kNewCommand.SetPacket( new PacketNetClear( new TDNetClear( uidLink ) ) );
			m_kCommandServer.AsyncPost( kNewCommand );
		}
	}
}