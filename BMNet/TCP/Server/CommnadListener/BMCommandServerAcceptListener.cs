﻿using System;

namespace BMNet
{
	public class BMCommandServerAcceptListener : BMAcceptListener
	{
		private BMNetServer m_kCommandServer;

		public BMCommandServerAcceptListener( BMNetServer _kNetServer )
		{
			m_kCommandServer = _kNetServer;
		}

		public override void PreRecv( BMServerTcpSocket _kSocket )
		{
			UInt64 newUID = m_kCommandServer.NewUID();
			BMLinkSocket m_kLinkSocket = CreateLinkSocket( newUID, _kSocket );

			_kSocket.SetLink( newUID );

			m_kCommandServer.LinkMgr.Add( m_kLinkSocket );
		}

		public override void OnAccept( BMServerTcpSocket _kSocket )
		{
			// Accept 되었다는 패킷을 보낼 수 있도록 한다.
			BMCommand kNewCommand = m_kCommandServer.NewLocalCommand( (Int32)PacketCommandEnum.CMD_PacketNetAccept );
			kNewCommand.SetPacket( new PacketNetAccept( new TDNetAccecpt( _kSocket.GetLink() ) ) );
			m_kCommandServer.AsyncPost( kNewCommand );
		}

		protected virtual BMLinkSocket CreateLinkSocket( ulong newUID, BMServerTcpSocket _kSocket )
		{
			return new BMLinkSocket( m_kCommandServer.GetUID(), newUID, _kSocket, m_kCommandServer.CommandMgr, m_kCommandServer.CommandBuilder );
		}
	}
}