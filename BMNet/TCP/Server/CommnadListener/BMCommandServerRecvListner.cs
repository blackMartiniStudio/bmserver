﻿using System;

namespace BMNet
{
	public class BMCommandServerRecvListner : IRecvListener
	{
		private BMNetServer m_kCommandServer;

		public BMCommandServerRecvListner( BMNetServer _kNetServer )
		{
			m_kCommandServer = _kNetServer;
		}

		public void OnDisconnect( UInt64 uidLink, String reason )
		{
			new DisconnectImpl( m_kCommandServer, uidLink );
		}

		public void OnHardDisconnect( UInt64 uidLink, String reason )
		{
			new HardDisconnectImpl( m_kCommandServer, uidLink );
		}

		public void OnRecv( BMServerTcpSocket tcpsocket, UInt64 uidLink, byte[] arData, Int32 nPacketLen )
		{
			m_kCommandServer.TrafficMonitor.RecordRecv( nPacketLen );

			BMLinkSocket kLinkSocket;
			if( false == m_kCommandServer.LinkMgr.TryGetSocket( uidLink, out kLinkSocket ) )
			{
				if( false == m_kCommandServer.LinkMgr.TryGetLinkFromReAllocTable( uidLink, out kLinkSocket ) )
					return;
			}

			if( false == kLinkSocket.IsAllowed() )
				return;

			BMCommandStream kCmdStream = kLinkSocket.GetCommandStream();

			if( false == kCmdStream.Read( arData, nPacketLen ) )
			{
				// 로그
				kLinkSocket.SetAllowed( false );
				kLinkSocket.Disconnect( "패킷이 정상적으로 들어오지 않아서 Disconnet" );
				m_kCommandServer.IncreaseDisconnCountByWrongPacket();
			}

			kLinkSocket.SetRecvTime( m_kCommandServer.Timer.GetNowTick() );

			UInt64 uidReceiver = m_kCommandServer.GetUID();
			UInt64 uidSender = kLinkSocket.GetUID();

			while( true )
			{
				BMCommand kCmd;
				if( false == kCmdStream.TryPopCommand( out kCmd ) )
					break;

				kCmd.ReceiverUID = uidReceiver;
				kCmd.SendererUID = uidSender;

				m_kCommandServer.RecvCommand( kCmd );
			}
		}
	}
}