﻿using BMDK;
using System;
using System.Threading;

namespace BMNet
{
	public class BMTrafficMonitor
	{
		private Int64       m_nRecvSizePerSec;
		private Int64       m_nSendSizePerSec;
		private Int64       m_nTotalRecvSize;
		private Int64       m_nTotalSendSize;
		private Int64       m_nLastTrafficCheckTime;

		private TimeTick    m_Tick  = new TimeTick();
		private Object      m_lock  = new Object();

		public void Reset()
		{
			Interlocked.Exchange( ref m_nRecvSizePerSec, 0 );
			Interlocked.Exchange( ref m_nSendSizePerSec, 0 );
			Interlocked.Exchange( ref m_nTotalRecvSize, 0 );
			Interlocked.Exchange( ref m_nTotalSendSize, 0 );
			Interlocked.Exchange( ref m_nLastTrafficCheckTime, 0 );
		}

		public void RecordRecv( Int64 nPacketSize )
		{
			Monitor.Enter( m_lock );

			if( true == IsElapsedPerSec() )
			{
				ResetPerSecTraffic();
			}

			m_nTotalRecvSize += nPacketSize;
			m_nRecvSizePerSec += nPacketSize;

			Monitor.Exit( m_lock );
		}

		public void RecordSend( Int64 nPacketSize )
		{
			Monitor.Enter( m_lock );

			if( true == IsElapsedPerSec() )
			{
				ResetPerSecTraffic();
			}

			m_nTotalSendSize += nPacketSize;
			m_nSendSizePerSec += nPacketSize;

			Monitor.Exit( m_lock );
		}

		public void GetTotalTraffic( out Int64 outRecvSize, out Int64 outSendSize )
		{
			outRecvSize = m_nTotalRecvSize;
			outSendSize = m_nTotalSendSize;
		}

		public void GetSecondTraffic( out Int64 outRecvSize, out Int64 outSendSize )
		{
			//< 마지막 갱신 시간에서 1초가 경과했다면 0을 돌려줌
			if( true == IsElapsedPerSec() )
			{
				ResetPerSecTraffic();
			}

			outRecvSize = m_nRecvSizePerSec;
			outSendSize = m_nSendSizePerSec;
		}

		private bool IsElapsedPerSec()
		{
			Int64 nowTick = m_Tick.GetNowTick();
			bool bElapsedPerSec = nowTick - m_nLastTrafficCheckTime >= TimeSpan.TicksPerSecond;

			if( true == bElapsedPerSec )
			{
				m_nLastTrafficCheckTime = nowTick;
			}

			return bElapsedPerSec;
		}

		private void ResetPerSecTraffic()
		{
			m_nRecvSizePerSec = 0;
			m_nSendSizePerSec = 0;
		}
	}
}