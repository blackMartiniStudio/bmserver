﻿using System;
using BMDK;

namespace BMNet
{
	public struct DisconnectImpl
	{
		public DisconnectImpl( BMNetServer _kServer, UInt64 uidLink )
		{
			BMLinkSocket kSocket;
			if( false == _kServer.LinkMgr.TryGetSocket( uidLink, out kSocket ) )
			{
				if( false == _kServer.LinkMgr.TryGetLinkFromReAllocTable( uidLink, out kSocket ) )
				{
					// 로그 : 연결 종료를 하여했으나 해당 링크가 없다
					BMLogger.WarnLog( "DisconnectImpl : 연결 종료를 하여했으나 해당 링크가 없다" );
					return;
				}
			}

			BMCommand kNewCommand = _kServer.NewLocalCommand( (Int32)PacketCommandEnum.CMD_PacketNetDisconnect );
			kNewCommand.SetPacket( new PacketNetDisconnect( new TDNetDisconnect( uidLink ) ) );
			_kServer.AsyncPost( kNewCommand );
		}
	}

	public struct HardDisconnectImpl
	{
		public HardDisconnectImpl( BMNetServer _kServer, UInt64 uidLink )
		{
			BMLinkSocket kSocket;
			if( false == _kServer.LinkMgr.TryGetSocket( uidLink, out kSocket ) )
			{
				if( false == _kServer.LinkMgr.TryGetLinkFromReAllocTable( uidLink, out kSocket ) )
				{
					// 로그 : 강제 연결 종료를 하여했으나 해당 링크가 없다
					BMLogger.WarnLog( "HardDisconnectImpl : 강제 연결 종료를 하여했으나 해당 링크가 없다" );
					return;
				}
			}

			BMCommand kNewCommand = _kServer.NewLocalCommand( (Int32)PacketCommandEnum.CMD_PacketNetHardDisconnect );
			kNewCommand.SetPacket( new PacketNetHardDisconnect( new TDNetHardDisconnect( uidLink ) ) );
			_kServer.AsyncPost( kNewCommand );
		}
	}
}