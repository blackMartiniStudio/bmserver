﻿using System;

namespace BMNet
{
	public class BMNetServerDesc
	{
		// 일정 시간마다  클라이언트에 패킷을 보내 Ping, 커넥션 체크할 지 여부
		private bool m_bHeartBeat = true;

		// HeartBeat 주기 타임
		private Int64 m_nHeartBeatTick = TimeSpan.TicksPerSecond * 60;

		// HeartBeat 타임 아웃(msec)
		private Int64 m_nHeartBeatTimeoutTick = TimeSpan.TicksPerSecond * 300;

		// 지연 테스트할 지 여부
		private bool m_bDelayTest = false;

		// 지연 테스트시 딜레이값(msec)
		//Int64	m_nTestDelayTime			= 0;

		public string	ServerName { get; set; }
		public string	PublicIP { get; set; }
		public int		ListenPort { get; set; }
		public bool NoDelay { get; set; }
		public int SocketPoolSize { get; set; }
		public int SendPendingLimitCount { get; set; }

		public string ClientName { get; set; }
		public string ClientType { get; set; }

		public bool DelayTest
		{
			get { return m_bDelayTest; }
		}

		public bool HeartBeat
		{
			get { return m_bHeartBeat; }
		}

		public Int64 HeartBeatTick
		{
			get { return m_nHeartBeatTick; }
		}

		public Int64 HeartBeatTimeoutTick
		{
			get { return m_nHeartBeatTimeoutTick; }
		}

		public BMNetServerDesc()
		{
		}
	}
}