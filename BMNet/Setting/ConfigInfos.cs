﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace BMNet
{
	[JsonObject]
	public class ServerInfo
	{
		public static string Key = "ServerInfo";

		[JsonProperty("ServerName")]
		public string ServerName;
		[JsonProperty("PublicIp")]
		public string PublicIp;
		[JsonProperty("PrivateIp")]
		public string PrivateIp;
		[JsonProperty("ListenPort")]
		public int    ListenPort;
		[JsonProperty("NoDelay")]
		public bool     NoDelay;
		[JsonProperty( "SocketPoolSize" )]
		public int      SocketPoolSize;
		[JsonProperty( "SendPendingLimitCount" )]
		public int     SendPendingLimitCount;
	}

	[JsonObject]
	public class ProxyServerInfo
	{
		public static string Key = "ProxyServerInfo";

		[JsonProperty("ServerName")]
		public string ServerName;
		[JsonProperty("PublicIp")]
		public string PublicIp;
		[JsonProperty("ListenPort")]
		public int    ListenPort;
		[JsonProperty("NoDelay")]
		public bool     NoDelay;
		[JsonProperty( "SocketPoolSize" )]
		public int      SocketPoolSize;
		[JsonProperty( "SendPendingLimitCount" )]
		public int     SendPendingLimitCount;
	}


	[JsonObject]
	public class ServerConnectionInfo
	{
		public static string Key = "ServerConnectionInfo";

		[JsonProperty("ServerName")]
		public string ServerName;
		[JsonProperty("Ip")]
		public string Ip;
		[JsonProperty("Port")]
		public int    Port;
	}
}