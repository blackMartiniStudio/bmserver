﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMNet
{
	public static class NetSettingHelper
	{
		public static BMNetServerDesc CreateNetServerDesc( ServerInfo info )
		{
			BMNetServerDesc kDesc = new BMNetServerDesc();
			kDesc.ServerName = info.ServerName;
			kDesc.PublicIP = info.PublicIp;
			kDesc.ListenPort = info.ListenPort;
			kDesc.NoDelay = info.NoDelay;
			kDesc.SocketPoolSize = info.SocketPoolSize;
			kDesc.SendPendingLimitCount = info.SendPendingLimitCount;

			return kDesc;
		}

		public static BMNetClientDesc CreateNetClientDesc( ServerConnectionInfo info, string clientName )
		{
			BMNetClientDesc kDesc = new BMNetClientDesc();
			kDesc.ClientName = clientName;
			kDesc.ServerName = info.ServerName;
			kDesc.ServerIp = info.Ip;
			kDesc.ServerPort = info.Port;

			return kDesc;
		}
	}
}
