﻿namespace BMNet
{
	public enum ResultTable
	{
		UNKNOWN = -1,
		SUCCESS = 0,
		SOCKET_CONNECT_ERROR = 1,		// 소켓 연결에 실패하였습니다.
		ALREADY_SOCKET_CONNECTED = 2,   // 이미 소켓이 연결된 상태입니다.

		ROOM_ERROR_INDEX = 30000,
		ROOM_CREATE_ERROR_DOES_NOT_EXIST_GAMESERVER,
		ROOM_CREATE_ERROR,

		DB_ERROR_INDEX = 100000,
		DB_ERROR,
	}
}