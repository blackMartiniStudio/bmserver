﻿namespace BMNet
{
	public enum BMCommandResult
	{
		SUCCESS = 0,    // 처됨
		FAIL,           // 처리안됨
		ERROR,          // 에러
		MOVED,          // 다른 큐로 이동
	}

	public enum CommandBuildError
	{
		BUILD_CMD_ERR_INCORRECT_HEADER_TYPE = -1,
		BUILD_CMD_ERR_BIG_PACKET_SIZE = -2,
		BUILD_CMD_ERR_CMD_ID = -3,
		BUILD_CMD_ERR_CHECK_CRC = -4,
		BUILD_CMD_ERR_SERIAL_NO = -5,
		BUILD_CMD_ERR_DECODE = -6,
		BUILD_CMD_ERR_SET_DATA = -7,
	}

	public enum NetCommon
	{
		DEFAULT_QUEUE_MAX_SIZE = 500000,        // 50만개 이상 큐에 쌓이면 커맨드 추가하지 않음
		MAX_PACKET_SIZE = 8192,
		UDP_BASE_PORT = 20000,
	}

	public enum SocketStatus
	{
		INITIALIZED = 0,
		OPENED,
		DISCONNECTED,
		CLOSED
	}

	public enum PacketVariables
	{
		PACKET_HEADER_SIZE = 14,
	}
}