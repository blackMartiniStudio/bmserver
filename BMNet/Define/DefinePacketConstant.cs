﻿using System;

namespace BMNet
{
	public class PacketConstant
	{
		public static readonly UInt16 PACKET_OPTION_NONE        = 0x0000;
		public static readonly UInt16 PACKET_OPTION_DATA_CRC    = 0x0001;
		public static readonly UInt16 PACKET_OPTION_COMPRESS    = 0x0002;
		public static readonly UInt16 PACKET_OPTION_ENCRYPTION  = 0x0004;
		public static readonly UInt16 PACKET_OPTION_CRC_DEFAULT = 0x0008;
		public static readonly UInt16 PACKET_OPTION_CRC_MD5     = 0x0010;

#if SERVERQA
        public static readonly UInt16 PACKET_CRYPT_OPTION = PACKET_OPTION_NONE;
#else
		public static readonly UInt16 PACKET_CRYPT_OPTION = (UInt16)(PACKET_OPTION_DATA_CRC | PACKET_OPTION_ENCRYPTION);
#endif

		public static readonly Int32  PACKET_COMPRESS_LIMIT =   4096;

		static public bool IsDataCRC( UInt32 option )
		{
			return ( option & PACKET_OPTION_DATA_CRC ) != 0;
		}

		static public bool IsEncrypt( UInt32 option )
		{
			return ( option & PACKET_OPTION_ENCRYPTION ) != 0;
		}

		static public bool IsCompress( UInt32 option )
		{
			return ( option & PACKET_OPTION_COMPRESS ) != 0;
		}
	}
}