﻿using BMDK;
using System;

namespace BMNet
{
	using BMCommandList = BMConcurrentQueue<BMCommand>;

	/// MCommandStream : 완결되지않은 Stream을 버퍼링한뒤 Command로 복원한다.
	///
	public class BMCommandStream
	{
		private BMCommandManager    m_kCommandManager;
		private ICommandBuilder     m_kCommandBuilder;
		private BMCommandList       m_CommandList;

		private Byte[]              m_Buffer;
		private Int32               m_nBufferCursor;

		public BMCommandStream( BMCommandManager _kCommandManager, ICommandBuilder _kCmdBuilder )
		{
			m_kCommandManager = _kCommandManager;
			m_kCommandBuilder = _kCmdBuilder;
			m_CommandList = new BMCommandList();
			m_Buffer = new Byte[(Int32)NetCommon.MAX_PACKET_SIZE];
		}

		public bool Read( Byte[] _arBuffer, Int32 nBufferLen )
		{
			if( true == IsBufferEmpty() )
			{
				if( false == ReadBuffer( _arBuffer, nBufferLen ) )
				{
					// 로그
					BMLogger.ErrLog( "ReadBuffer Fail! - _arBuffer[{0}], nBufferLen[{1}]", _arBuffer.Length, nBufferLen );
					return false;
				}
			}
			else
			{
				if( false == AddBuffer( _arBuffer, nBufferLen ) )
					return false;

				ReadBuffer( m_Buffer, m_nBufferCursor );
			}

			return true;
		}

		private bool ReadBuffer( Byte[] _arBuffer, Int32 nBufferLen )
		{
			// Build Command
			Int32 nSpareDataLen = MakeCommand( _arBuffer, nBufferLen );
			if( nSpareDataLen > 0 )
			{
				Buffer.BlockCopy( _arBuffer, nBufferLen - nSpareDataLen, m_Buffer, 0, nSpareDataLen );
				m_nBufferCursor = nSpareDataLen;

				Array.Clear( m_Buffer, m_nBufferCursor, m_Buffer.Length - m_nBufferCursor );
			}
			else if( nSpareDataLen < 0 )
			{
				// 로그
				BMLogger.ErrLog( "ReadBuffer SpareData Len Error : SpareDataLen[{0}],  _arBuffer[{1}], nBufferLen[{2}]",  nSpareDataLen, _arBuffer.Length, nBufferLen );
				return false;
			}

			// m_nBufferCursor = 0;

			return true;
		}

		private bool AddBuffer( Byte[] _arBuffer, Int32 nBufferLen )
		{
			if( nBufferLen <= 0 )
			{
				// 로그
				BMLogger.ErrLog( "BMCommandStream::AddBuffer() 실패! - 크기가 0 또는 음수" );
				return false;
			}

			if( ( m_nBufferCursor + nBufferLen ) >= m_Buffer.Length )
			{
				// 메모리 크기 재 할당
				Int32 nReallocSize = (Int32)( ( m_nBufferCursor + nBufferLen ) * 1.5 );
				Byte[] arUpgradeBuffer = new Byte[nReallocSize];

				Buffer.BlockCopy( m_Buffer, 0, arUpgradeBuffer, 0, m_nBufferCursor );
				m_Buffer = arUpgradeBuffer;
			}

			Buffer.BlockCopy( _arBuffer, 0, m_Buffer, m_nBufferCursor, nBufferLen );
			m_nBufferCursor += nBufferLen;

			return true;
		}

		public bool TryPopCommand( out BMCommand _kCmd )
		{
			return m_CommandList.TryDequeue( out _kCmd );
		}

		private bool IsBufferEmpty()
		{
			if( m_nBufferCursor == 0 )
				return true;

			return false;
		}

		private bool EstimateBufferToCmd()
		{
			throw new NotImplementedException();
		}

		private int MakeCommand( byte[] _arBuffer, int nBufferLen )
		{
			return m_kCommandBuilder.BuildToCommand( _arBuffer, nBufferLen, ref m_CommandList );
		}

		public void SetCommandStream( ICommandBuilder _kCommandBuilder )
		{
			m_kCommandBuilder = _kCommandBuilder;
		}
	}
}