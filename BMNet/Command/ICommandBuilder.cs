﻿//using System.Collections.Concurrent;
using BMDK;
using System;

namespace BMNet
{
	public interface ICommandBuilder
	{
		bool BuildToStream( BMCommand _kCmd, out Byte[] header, out Byte[] body );

		Int32 BuildToCommand( byte[] _arBuffer, int _nBufferLen, ref BMConcurrentQueue<BMCommand> _refCommandList );
	}
}