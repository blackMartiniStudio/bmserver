﻿using System;
using System.Collections.Generic;
using System.Net;

namespace BMNet
{
	public delegate BMCommandResult BMudpCommandHanderFunc( BMUdpCommand _kCommand, EndPoint _kPeerAddress, IUdpCommandCommunicator _kHandler );

	public class BMUdpCommandHandler
	{
		private IUdpCommandCommunicator m_kCoummunicator;
		private Dictionary<Int32, BMudpCommandHanderFunc>   m_dicCommandHandler = new Dictionary<Int32, BMudpCommandHanderFunc>();

		public BMUdpCommandHandler( IUdpCommandCommunicator kCoummunicator )
		{
			m_kCoummunicator = kCoummunicator;
		}

		public virtual void OnCommand( BMUdpCommand kCmd, EndPoint _PeerAddress )
		{
			if( null == kCmd )
				return;

			BMudpCommandHanderFunc func;
			if( false == m_dicCommandHandler.TryGetValue( kCmd.ID, out func ) )
				return;

			func( kCmd, _PeerAddress, m_kCoummunicator );
		}

		public void AddHandler( Int32 nCommandID, BMudpCommandHanderFunc _func )
		{
			if( true == m_dicCommandHandler.ContainsKey( nCommandID ) )
				return;

			m_dicCommandHandler.Add( nCommandID, _func );
		}
	}
}