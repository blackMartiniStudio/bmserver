﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace BMNet
{

public class TDNetAccecpt
{
	public UInt64 uidLink;
	public TDNetAccecpt() {}
	public TDNetAccecpt(UInt64 p_uidLink)
	{
		this.uidLink = p_uidLink;
	}
	public TDNetAccecpt(TDNetAccecpt p_TDNetAccecpt)
	{
		this.uidLink = p_TDNetAccecpt.uidLink;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(uidLink);
	}

	public void StreamDecoding(BinaryReader src)
	{
		uidLink = src.ReadUInt64();
	}
}

public class TDNetDisconnect
{
	public UInt64 uidLink;
	public TDNetDisconnect() {}
	public TDNetDisconnect(UInt64 p_uidLink)
	{
		this.uidLink = p_uidLink;
	}
	public TDNetDisconnect(TDNetDisconnect p_TDNetDisconnect)
	{
		this.uidLink = p_TDNetDisconnect.uidLink;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(uidLink);
	}

	public void StreamDecoding(BinaryReader src)
	{
		uidLink = src.ReadUInt64();
	}
}

public class TDNetHardDisconnect
{
	public UInt64 uidLink;
	public TDNetHardDisconnect() {}
	public TDNetHardDisconnect(UInt64 p_uidLink)
	{
		this.uidLink = p_uidLink;
	}
	public TDNetHardDisconnect(TDNetHardDisconnect p_TDNetHardDisconnect)
	{
		this.uidLink = p_TDNetHardDisconnect.uidLink;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(uidLink);
	}

	public void StreamDecoding(BinaryReader src)
	{
		uidLink = src.ReadUInt64();
	}
}

public class TDNetClear
{
	public UInt64 uidLink;
	public TDNetClear() {}
	public TDNetClear(UInt64 p_uidLink)
	{
		this.uidLink = p_uidLink;
	}
	public TDNetClear(TDNetClear p_TDNetClear)
	{
		this.uidLink = p_TDNetClear.uidLink;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(uidLink);
	}

	public void StreamDecoding(BinaryReader src)
	{
		uidLink = src.ReadUInt64();
	}
}

public class TDNetRelayConnect
{
	public UInt64 uidAlloc;
	public UInt64 uidHost;
	public Int64 timeTick;
	public TDNetRelayConnect() {}
	public TDNetRelayConnect(UInt64 p_uidAlloc, UInt64 p_uidHost, Int64 p_timeTick)
	{
		this.uidAlloc = p_uidAlloc;
		this.uidHost = p_uidHost;
		this.timeTick = p_timeTick;
	}
	public TDNetRelayConnect(TDNetRelayConnect p_TDNetRelayConnect)
	{
		this.uidAlloc = p_TDNetRelayConnect.uidAlloc;
		this.uidHost = p_TDNetRelayConnect.uidHost;
		this.timeTick = p_TDNetRelayConnect.timeTick;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(uidAlloc);
		output.Write(uidHost);
		output.Write(timeTick);
	}

	public void StreamDecoding(BinaryReader src)
	{
		uidAlloc = src.ReadUInt64();
		uidHost = src.ReadUInt64();
		timeTick = src.ReadInt64();
	}
}

public class TDNetReallocUID
{
	public UInt64 uidLink;
	public TDNetReallocUID() {}
	public TDNetReallocUID(UInt64 p_uidLink)
	{
		this.uidLink = p_uidLink;
	}
	public TDNetReallocUID(TDNetReallocUID p_TDNetReallocUID)
	{
		this.uidLink = p_TDNetReallocUID.uidLink;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(uidLink);
	}

	public void StreamDecoding(BinaryReader src)
	{
		uidLink = src.ReadUInt64();
	}
}
}
