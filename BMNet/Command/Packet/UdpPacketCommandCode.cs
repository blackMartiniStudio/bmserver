﻿namespace BMNet
{
	public enum UdpPacketEnum
	{
		CMD_NONE,
		CMD_UdpPacketHeader,
		CMD_UdpPacketPing,
		CMD_UdpPacketSync,
	}
}