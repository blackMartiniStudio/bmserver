﻿using System;
using System.IO;

namespace BMNet
{
	public class UdpPacketHeader
	{
		public Byte fromSlotId;
		public Byte toSlotId;
		public Int32 cmdID;
		public Int32 bodySize;

		public UdpPacketHeader()
		{
		}

		public UdpPacketHeader( Byte p_fromSlotId, Byte p_toSlotId, Int32 p_CmdID, Int32 p_bodySize )
		{
			fromSlotId = p_fromSlotId;
			toSlotId = p_toSlotId;
			cmdID = p_CmdID;
			bodySize = p_bodySize;
		}

		public void StreamEncoding( BinaryWriter output )
		{
			output.Write( fromSlotId );
			output.Write( toSlotId );
			output.Write( cmdID );
			output.Write( bodySize );
		}

		public void StreamDecoding( BinaryReader src )
		{
			fromSlotId = src.ReadByte();
			toSlotId = src.ReadByte();
			cmdID = src.ReadInt32();
			bodySize = src.ReadInt32();
		}
	}

	public class UdpPacketPing
	{
		public Int32 reserved;

		public UdpPacketPing()
		{
		}

		public UdpPacketPing( Int32 p_reserved )
		{
			reserved = p_reserved;
		}

		public void StreamEncoding( BinaryWriter output )
		{
			output.Write( reserved );
		}

		public void StreamDecoding( BinaryReader src )
		{
			reserved = src.ReadInt32();
		}
	}

	public class UdpPacketSync
	{
		public float m_Time;
		public float m_PositionX, m_PositionY, m_PositionZ;
		public float m_RotationX, m_RotationY, m_RotationZ, m_RotationW;
		public float m_VelocityX, m_VelocityY, m_VelocityZ;
		public float m_AccelationX, m_AccelationY, m_AccelationZ;
		public float m_AngularVelX, m_AngularVelY, m_AngularVelZ;
		public float m_SendRate;

		public Int16 m_AnimVehicleID;
		public Int16 m_AnimCharacterID;
		public Byte m_CurrentLap;
		public float m_CurrentDistance;
		public float m_CurrentSteer;
		public Int16 m_DriftAngle;
		public bool m_OnBoostState;
		public bool m_OnSlipStream;
		public bool m_OnDriftAction;

		private byte[] m_ArrayBuffer;
		private byte[] m_ArrayBufferCompressed;

		public UdpPacketSync()
		{
		}

		public UdpPacketSync( float p_Time,
			float p_PositionX, float p_PositionY, float p_PositionZ,
			float p_RotationX, float p_RotationY, float p_RotationZ,
			float p_RotationW, float p_VelocityX, float p_VelocityY, float p_VelocityZ,
			float p_AccelationX, float p_AccelationY, float p_AccelationZ,
			float p_AngularVelX, float p_AngularVelY, float p_AngularVelZ,
			float p_SendRate, Int16 p_AnimVehicleID, Int16 p_AnimCharacterID, Byte p_CurrentLap,
			float p_CurrentDistance, float p_CurrentSteer, Int16 p_DriftAngle,
			bool p_OnBoostState, bool p_OnSlipStream, bool p_OnDriftAction )
		{
			MemoryStream AMemStream = new MemoryStream();
			BinaryWriter ABinaryWriter = new BinaryWriter( AMemStream );

			ABinaryWriter.Write( p_Time );

			ABinaryWriter.Write( p_PositionX );
			ABinaryWriter.Write( p_PositionY );
			ABinaryWriter.Write( p_PositionZ );

			ABinaryWriter.Write( ToShortFromFloat( p_RotationX ) );
			ABinaryWriter.Write( ToShortFromFloat( p_RotationY ) );
			ABinaryWriter.Write( ToShortFromFloat( p_RotationZ ) );
			ABinaryWriter.Write( ToShortFromFloat( p_RotationW ) );

			ABinaryWriter.Write( ToShortFromFloat( p_VelocityX ) );
			ABinaryWriter.Write( ToShortFromFloat( p_VelocityY ) );
			ABinaryWriter.Write( ToShortFromFloat( p_VelocityZ ) );

			ABinaryWriter.Write( ToShortFromFloat( p_AccelationX ) );
			ABinaryWriter.Write( ToShortFromFloat( p_AccelationY ) );
			ABinaryWriter.Write( ToShortFromFloat( p_AccelationZ ) );

			ABinaryWriter.Write( ToShortFromFloat( p_AngularVelX ) );
			ABinaryWriter.Write( ToShortFromFloat( p_AngularVelY ) );
			ABinaryWriter.Write( ToShortFromFloat( p_AngularVelZ ) );

			ABinaryWriter.Write( ToShortFromFloat( p_SendRate ) );
			ABinaryWriter.Write( p_AnimVehicleID );
			ABinaryWriter.Write( p_AnimCharacterID );
			ABinaryWriter.Write( p_CurrentLap );
			ABinaryWriter.Write( p_CurrentDistance );
			ABinaryWriter.Write( ToShortFromFloat( p_CurrentSteer ) );
			ABinaryWriter.Write( p_DriftAngle );
			ABinaryWriter.Write( p_OnBoostState );
			ABinaryWriter.Write( p_OnSlipStream );
			ABinaryWriter.Write( p_OnDriftAction );

			m_ArrayBuffer = AMemStream.GetBuffer();

			m_ArrayBufferCompressed = CLZF2.Compress( m_ArrayBuffer );
		}

		public void StreamEncoding( BinaryWriter output )
		{
			output.Write( m_ArrayBufferCompressed.Length );
			output.Write( m_ArrayBufferCompressed );
		}

		public void StreamDecoding( BinaryReader src )
		{
			int AByteCount = src.ReadInt32();
			m_ArrayBufferCompressed = src.ReadBytes( AByteCount );
			m_ArrayBuffer = CLZF2.Decompress( m_ArrayBufferCompressed );
			MemoryStream AMemStream = new MemoryStream( m_ArrayBuffer );
			BinaryReader ABinaryReader = new BinaryReader( AMemStream );

			m_Time = ABinaryReader.ReadSingle();
			m_PositionX = ABinaryReader.ReadSingle();
			m_PositionY = ABinaryReader.ReadSingle();
			m_PositionZ = ABinaryReader.ReadSingle();

			m_RotationX = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_RotationY = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_RotationZ = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_RotationW = ToFloatFromShort( ABinaryReader.ReadInt16() );

			m_VelocityX = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_VelocityY = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_VelocityZ = ToFloatFromShort( ABinaryReader.ReadInt16() );

			m_AccelationX = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_AccelationY = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_AccelationZ = ToFloatFromShort( ABinaryReader.ReadInt16() );

			m_AngularVelX = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_AngularVelY = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_AngularVelZ = ToFloatFromShort( ABinaryReader.ReadInt16() );

			m_SendRate = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_AnimVehicleID = ABinaryReader.ReadInt16();
			m_AnimCharacterID = ABinaryReader.ReadInt16();
			m_CurrentLap = ABinaryReader.ReadByte();
			m_CurrentDistance = ABinaryReader.ReadSingle();
			m_CurrentSteer = ToFloatFromShort( ABinaryReader.ReadInt16() );
			m_DriftAngle = ABinaryReader.ReadInt16();
			m_OnBoostState = ABinaryReader.ReadBoolean();
			m_OnSlipStream = ABinaryReader.ReadBoolean();
			m_OnDriftAction = ABinaryReader.ReadBoolean();
		}

		private short ToShortFromFloat( float InFloat )
		{
			const float ConstConverScale = 100.0f;
			const float ConstFloatMaxValue = (float)short.MaxValue;
			const float ConstFloatMinValue = (float)short.MinValue;
			float AFloatValue = ( InFloat * ConstConverScale );

			return
				AFloatValue > ConstFloatMaxValue ? short.MaxValue :
				AFloatValue < ConstFloatMinValue ? short.MinValue : (short)AFloatValue;
		}

		private float ToFloatFromShort( short InShort )
		{
			const float ConstConverScale = 100.0f;
			return (float)( ( (float)InShort ) / ConstConverScale );
		}
	}
}