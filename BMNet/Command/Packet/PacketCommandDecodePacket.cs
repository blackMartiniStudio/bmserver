﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMNet
{
public sealed partial class PacketCommandCoder
{
	static public object Decode(int cmd, byte[] bodyBuffer)
	{
		switch (cmd)
		{
			case (int)PacketCommandEnum.CMD_C2SPacketHeader:
				return (object)PacketCommandDecoder.DecodeC2SPacketHeader(bodyBuffer);
			case (int)PacketCommandEnum.CMD_PacketNetConnect:
				return null;
			case (int)PacketCommandEnum.CMD_PacketNetDisconnect:
				return (object)PacketCommandDecoder.DecodePacketNetDisconnect(bodyBuffer);
			case (int)PacketCommandEnum.CMD_PacketNetHardDisconnect:
				return (object)PacketCommandDecoder.DecodePacketNetHardDisconnect(bodyBuffer);
			case (int)PacketCommandEnum.CMD_PacketNetAccept:
				return (object)PacketCommandDecoder.DecodePacketNetAccept(bodyBuffer);
			case (int)PacketCommandEnum.CMD_PacketNetClear:
				return (object)PacketCommandDecoder.DecodePacketNetClear(bodyBuffer);
			case (int)PacketCommandEnum.CMD_PacketNetRelayConnect:
				return (object)PacketCommandDecoder.DecodePacketNetRelayConnect(bodyBuffer);
			case (int)PacketCommandEnum.CMD_PacketNetReallocUID:
				return (object)PacketCommandDecoder.DecodePacketNetReallocUID(bodyBuffer);
			default:
				break;
		}
		return null;
	}
}
}
