﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BMNet
{
	public sealed partial class PacketCommandEncoder
	{

		static public byte[] EncodeC2SPacketHeader(C2SPacketHeader pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetConnect(PacketNetConnect pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetDisconnect(PacketNetDisconnect pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetHardDisconnect(PacketNetHardDisconnect pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetAccept(PacketNetAccept pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetClear(PacketNetClear pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetRelayConnect(PacketNetRelayConnect pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}

		static public byte[] EncodePacketNetReallocUID(PacketNetReallocUID pktBody, out bool isCrypt)
		{
			Byte[] retVal;
			using (MemoryStream mem = new MemoryStream())
			{
				BinaryWriter output = new BinaryWriter(mem);
				pktBody.StreamEncoding(output);
				output.Flush();
				retVal = mem.ToArray();
			}

			isCrypt = false;
			return retVal;
		}
	}
}
