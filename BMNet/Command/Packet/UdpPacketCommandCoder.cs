﻿using System;
using System.IO;

namespace BMNet
{
	public sealed partial class UdpPacketEncoder
	{
		static public byte[] EncodeUdpPacketHeader( UdpPacketHeader pktBody )
		{
			Byte[] retVal;
			using( MemoryStream mem = new MemoryStream() )
			{
				BinaryWriter output = new BinaryWriter( mem );
				pktBody.StreamEncoding( output );
				output.Flush();
				retVal = mem.ToArray();
			}

			return retVal;
		}

		static public byte[] EncodeUdpPacketPing( UdpPacketPing pktBody )
		{
			Byte[] retVal;
			using( MemoryStream mem = new MemoryStream() )
			{
				BinaryWriter output = new BinaryWriter( mem );
				pktBody.StreamEncoding( output );
				output.Flush();
				retVal = mem.ToArray();
			}

			return retVal;
		}

		static public byte[] EncodeUdpPacketSync( UdpPacketSync pktBody )
		{
			Byte[] retVal;
			using( MemoryStream mem = new MemoryStream() )
			{
				BinaryWriter output = new BinaryWriter( mem );
				pktBody.StreamEncoding( output );
				output.Flush();
				retVal = mem.ToArray();
			}

			return retVal;
		}
	}

	public sealed partial class UdpPacketDecoder
	{
		static public UdpPacketHeader DecodeUdpPacketHeader( byte[] bodyBuffer )
		{
			UdpPacketHeader retValue = new UdpPacketHeader();
			using( Stream streamBuffer = new MemoryStream( bodyBuffer, 0, bodyBuffer.Length, false ) )
			{
				BinaryReader src = new BinaryReader( streamBuffer );
				retValue.StreamDecoding( src );
			}

			return retValue;
		}

		static public UdpPacketPing DecodeUdpPacketPing( byte[] bodyBuffer )
		{
			UdpPacketPing retValue = new UdpPacketPing();
			using( Stream streamBuffer = new MemoryStream( bodyBuffer, 0, bodyBuffer.Length, false ) )
			{
				BinaryReader src = new BinaryReader( streamBuffer );
				retValue.StreamDecoding( src );
			}

			return retValue;
		}

		static public UdpPacketSync DecodeUdpPacketSync( byte[] bodyBuffer )
		{
			UdpPacketSync retValue = new UdpPacketSync();
			using( Stream streamBuffer = new MemoryStream( bodyBuffer, 0, bodyBuffer.Length, false ) )
			{
				BinaryReader src = new BinaryReader( streamBuffer );
				retValue.StreamDecoding( src );
			}

			return retValue;
		}
	}

	public sealed class UdpPacketCoder
	{
		static public byte[] Encode( int cmd, object packetClass )
		{
			switch( cmd )
			{
				case (int)UdpPacketEnum.CMD_UdpPacketHeader:
					return ( UdpPacketEncoder.EncodeUdpPacketHeader( (UdpPacketHeader)packetClass ) );

				case (int)UdpPacketEnum.CMD_UdpPacketPing:
					return ( UdpPacketEncoder.EncodeUdpPacketPing( (UdpPacketPing)packetClass ) );

				case (int)UdpPacketEnum.CMD_UdpPacketSync:
					return ( UdpPacketEncoder.EncodeUdpPacketSync( (UdpPacketSync)packetClass ) );

				default:
					break;
			}

			return ( null );
		}

		static public object Decode( int cmd, byte[] bodyBuffer )
		{
			switch( cmd )
			{
				case (int)UdpPacketEnum.CMD_UdpPacketHeader:
					return (object)UdpPacketDecoder.DecodeUdpPacketHeader( bodyBuffer );

				case (int)UdpPacketEnum.CMD_UdpPacketPing:
					return (object)UdpPacketDecoder.DecodeUdpPacketPing( bodyBuffer );

				case (int)UdpPacketEnum.CMD_UdpPacketSync:
					return (object)UdpPacketDecoder.DecodeUdpPacketSync( bodyBuffer );

				default:
					break;
			}

			return ( null );
		}
	}
}