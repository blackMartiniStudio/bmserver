﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMNet
{
public enum PacketCommandEnum
{
	CMD_NONE,
	CMD_C2SPacketHeader = 1,
	CMD_PacketNetConnect = 104,
	CMD_PacketNetDisconnect = 105,
	CMD_PacketNetHardDisconnect = 106,
	CMD_PacketNetAccept = 109,
	CMD_PacketNetClear = 110,
	CMD_PacketNetRelayConnect = 111,
	CMD_PacketNetReallocUID = 112,
	CMD_MAX
}
}
