﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMNet
{
public sealed partial class PacketCommandCoder
{
	static public byte[] Encode(int cmd, object packetClass, out bool isCrypt)
	{
		switch (cmd)
		{
			case (int)PacketCommandEnum.CMD_C2SPacketHeader:
				return PacketCommandEncoder.EncodeC2SPacketHeader((C2SPacketHeader)packetClass, out isCrypt);
			case (int)PacketCommandEnum.CMD_PacketNetConnect:
				isCrypt = false;
				return null;
			case (int)PacketCommandEnum.CMD_PacketNetDisconnect:
				return PacketCommandEncoder.EncodePacketNetDisconnect((PacketNetDisconnect)packetClass, out isCrypt);
			case (int)PacketCommandEnum.CMD_PacketNetHardDisconnect:
				return PacketCommandEncoder.EncodePacketNetHardDisconnect((PacketNetHardDisconnect)packetClass, out isCrypt);
			case (int)PacketCommandEnum.CMD_PacketNetAccept:
				return PacketCommandEncoder.EncodePacketNetAccept((PacketNetAccept)packetClass, out isCrypt);
			case (int)PacketCommandEnum.CMD_PacketNetClear:
				return PacketCommandEncoder.EncodePacketNetClear((PacketNetClear)packetClass, out isCrypt);
			case (int)PacketCommandEnum.CMD_PacketNetRelayConnect:
				return PacketCommandEncoder.EncodePacketNetRelayConnect((PacketNetRelayConnect)packetClass, out isCrypt);
			case (int)PacketCommandEnum.CMD_PacketNetReallocUID:
				return PacketCommandEncoder.EncodePacketNetReallocUID((PacketNetReallocUID)packetClass, out isCrypt);
			default:
				break;
		}
		isCrypt = false;
		return null;
	}
}
}
