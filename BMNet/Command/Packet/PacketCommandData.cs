﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace BMNet
{
// 클라 <-> 서버간의 TCP 패킷 해더

public class C2SPacketHeader
{
	public int mCode;
	public UInt16 mOption;
	public UInt32 mCRC;
	public int mBodySize;
	public C2SPacketHeader() {}
	public C2SPacketHeader(int p_mCode, UInt16 p_mOption, UInt32 p_mCRC, int p_mBodySize)
	{
		this.mCode = p_mCode;
		this.mOption = p_mOption;
		this.mCRC = p_mCRC;
		this.mBodySize = p_mBodySize;
	}
	public C2SPacketHeader(C2SPacketHeader p_C2SPacketHeader)
	{
		this.mCode = p_C2SPacketHeader.mCode;
		this.mOption = p_C2SPacketHeader.mOption;
		this.mCRC = p_C2SPacketHeader.mCRC;
		this.mBodySize = p_C2SPacketHeader.mBodySize;
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		output.Write(mCode);
		output.Write(mOption);
		output.Write(mCRC);
		output.Write(mBodySize);
	}

	public void StreamDecoding(BinaryReader src)
	{
		mCode = src.ReadInt32();
		mOption = src.ReadUInt16();
		mCRC = src.ReadUInt32();
		mBodySize = src.ReadInt32();
	}
}

public class PacketNetConnect
{
	public PacketNetConnect() {}
	public PacketNetConnect(PacketNetConnect p_PacketNetConnect)
	{
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
	}

	public void StreamDecoding(BinaryReader src)
	{
	}
}

public class PacketNetDisconnect
{
	public TDNetDisconnect tdNetDisconnect;
	public PacketNetDisconnect() {}
	public PacketNetDisconnect(TDNetDisconnect p_tdNetDisconnect)
	{
		this.tdNetDisconnect = new TDNetDisconnect(p_tdNetDisconnect);
	}
	public PacketNetDisconnect(PacketNetDisconnect p_PacketNetDisconnect)
	{
		this.tdNetDisconnect = new TDNetDisconnect(p_PacketNetDisconnect.tdNetDisconnect);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (tdNetDisconnect == null)
			tdNetDisconnect = new TDNetDisconnect();
		tdNetDisconnect.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		tdNetDisconnect = new TDNetDisconnect();
		tdNetDisconnect.StreamDecoding(src);
	}
}

public class PacketNetHardDisconnect
{
	public TDNetHardDisconnect tdNetHardDisconnect;
	public PacketNetHardDisconnect() {}
	public PacketNetHardDisconnect(TDNetHardDisconnect p_tdNetHardDisconnect)
	{
		this.tdNetHardDisconnect = new TDNetHardDisconnect(p_tdNetHardDisconnect);
	}
	public PacketNetHardDisconnect(PacketNetHardDisconnect p_PacketNetHardDisconnect)
	{
		this.tdNetHardDisconnect = new TDNetHardDisconnect(p_PacketNetHardDisconnect.tdNetHardDisconnect);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (tdNetHardDisconnect == null)
			tdNetHardDisconnect = new TDNetHardDisconnect();
		tdNetHardDisconnect.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		tdNetHardDisconnect = new TDNetHardDisconnect();
		tdNetHardDisconnect.StreamDecoding(src);
	}
}

public class PacketNetAccept
{
	public TDNetAccecpt tdNetAccept;
	public PacketNetAccept() {}
	public PacketNetAccept(TDNetAccecpt p_tdNetAccept)
	{
		this.tdNetAccept = new TDNetAccecpt(p_tdNetAccept);
	}
	public PacketNetAccept(PacketNetAccept p_PacketNetAccept)
	{
		this.tdNetAccept = new TDNetAccecpt(p_PacketNetAccept.tdNetAccept);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (tdNetAccept == null)
			tdNetAccept = new TDNetAccecpt();
		tdNetAccept.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		tdNetAccept = new TDNetAccecpt();
		tdNetAccept.StreamDecoding(src);
	}
}

public class PacketNetClear
{
	public TDNetClear tdNetClear;
	public PacketNetClear() {}
	public PacketNetClear(TDNetClear p_tdNetClear)
	{
		this.tdNetClear = new TDNetClear(p_tdNetClear);
	}
	public PacketNetClear(PacketNetClear p_PacketNetClear)
	{
		this.tdNetClear = new TDNetClear(p_PacketNetClear.tdNetClear);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (tdNetClear == null)
			tdNetClear = new TDNetClear();
		tdNetClear.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		tdNetClear = new TDNetClear();
		tdNetClear.StreamDecoding(src);
	}
}

public class PacketNetRelayConnect
{
	public TDNetRelayConnect tdNetRelayConnect;
	public PacketNetRelayConnect() {}
	public PacketNetRelayConnect(TDNetRelayConnect p_tdNetRelayConnect)
	{
		this.tdNetRelayConnect = new TDNetRelayConnect(p_tdNetRelayConnect);
	}
	public PacketNetRelayConnect(PacketNetRelayConnect p_PacketNetRelayConnect)
	{
		this.tdNetRelayConnect = new TDNetRelayConnect(p_PacketNetRelayConnect.tdNetRelayConnect);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (tdNetRelayConnect == null)
			tdNetRelayConnect = new TDNetRelayConnect();
		tdNetRelayConnect.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		tdNetRelayConnect = new TDNetRelayConnect();
		tdNetRelayConnect.StreamDecoding(src);
	}
}

public class PacketNetReallocUID
{
	public TDNetReallocUID tdNetReallocUID;
	public PacketNetReallocUID() {}
	public PacketNetReallocUID(TDNetReallocUID p_tdNetReallocUID)
	{
		this.tdNetReallocUID = new TDNetReallocUID(p_tdNetReallocUID);
	}
	public PacketNetReallocUID(PacketNetReallocUID p_PacketNetReallocUID)
	{
		this.tdNetReallocUID = new TDNetReallocUID(p_PacketNetReallocUID.tdNetReallocUID);
	}

	public object Clone()
	{
		return this.MemberwiseClone();
	}

	public void StreamEncoding(BinaryWriter output)
	{
		if (tdNetReallocUID == null)
			tdNetReallocUID = new TDNetReallocUID();
		tdNetReallocUID.StreamEncoding(output);
	}

	public void StreamDecoding(BinaryReader src)
	{
		tdNetReallocUID = new TDNetReallocUID();
		tdNetReallocUID.StreamDecoding(src);
	}
}
}
