﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BMNet
{
	public sealed partial class PacketCommandDecoder
	{

		static public C2SPacketHeader DecodeC2SPacketHeader(byte[] bodyBuffer)
		{
			C2SPacketHeader retValue = new C2SPacketHeader();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetConnect DecodePacketNetConnect(byte[] bodyBuffer)
		{
			PacketNetConnect retValue = new PacketNetConnect();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetDisconnect DecodePacketNetDisconnect(byte[] bodyBuffer)
		{
			PacketNetDisconnect retValue = new PacketNetDisconnect();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetHardDisconnect DecodePacketNetHardDisconnect(byte[] bodyBuffer)
		{
			PacketNetHardDisconnect retValue = new PacketNetHardDisconnect();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetAccept DecodePacketNetAccept(byte[] bodyBuffer)
		{
			PacketNetAccept retValue = new PacketNetAccept();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetClear DecodePacketNetClear(byte[] bodyBuffer)
		{
			PacketNetClear retValue = new PacketNetClear();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetRelayConnect DecodePacketNetRelayConnect(byte[] bodyBuffer)
		{
			PacketNetRelayConnect retValue = new PacketNetRelayConnect();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}

		static public PacketNetReallocUID DecodePacketNetReallocUID(byte[] bodyBuffer)
		{
			PacketNetReallocUID retValue = new PacketNetReallocUID();
			using (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))
			{
				BinaryReader src = new BinaryReader(streamBuffer);
				retValue.StreamDecoding(src);
			}

			return retValue;
		}
	}
}
