﻿using BMDK;
using System;

namespace BMNet
{
	using BMCommandList = BMConcurrentQueue<BMCommand>;

	public class BMCommandManager
	{
		private BMCommandList   m_CommandRecvQueue = new BMCommandList();
		private BMCommandList   m_CommandReserveQueue = new BMCommandList();
		private Int32           m_nMaxQueueSize = (Int32)NetCommon.DEFAULT_QUEUE_MAX_SIZE;

		public Int32 MaxQueueSize
		{
			get { return m_nMaxQueueSize; }
			set { m_nMaxQueueSize = value; }
		}

		public Int32 Count
		{
			get { return m_CommandRecvQueue.Count; }
		}

		public bool Push( BMCommand _kCommand )
		{
			m_CommandRecvQueue.Enqueue( _kCommand );
			return true;
		}

		public bool TryGetCommand( out BMCommand _kCmd )
		{
			return m_CommandRecvQueue.TryDequeue( out _kCmd );
		}

		public void Clear()
		{
			BMCommand kCmd;

			Int32 nSize = m_CommandRecvQueue.Count;
			for( Int32 nID = 0; nID < nSize; ++nID )
			{
				if( false == m_CommandRecvQueue.TryDequeue( out kCmd ) )
					break;
			}

			nSize = m_CommandReserveQueue.Count;
			for( Int32 nID = 0; nID < nSize; ++nID )
			{
				if( false == m_CommandReserveQueue.TryDequeue( out kCmd ) )
					break;
			}
		}

		internal void ResevedCommand( BMCommand kNewCommand )
		{
			m_CommandReserveQueue.Enqueue( kNewCommand );
		}

		public bool TryGetResevedCommand( out BMCommand _kCmd )
		{
			return m_CommandReserveQueue.TryDequeue( out _kCmd );
		}
	}
}