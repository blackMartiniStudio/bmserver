﻿using System;
using System.Collections.Generic;

namespace BMNet
{
	public class BMCommandProxy
	{
		public BMCommandProxy( Int32 _nID, BMCommandHandler _kHandler, BMCommandHanderFunc _Function )
		{
			m_nID = _nID;
			m_kHandler = _kHandler;
			m_Function = _Function;
		}

		public Int32                m_nID       = 0;        // Command ID
		public BMCommandHandler     m_kHandler  = null;     // Command 핸들러
		public BMCommandHanderFunc  m_Function  = null;     // Command 핸들러 델리게이터 함수
	}

	public class BMCommandMap
	{
		private Dictionary<Int32, BMCommandProxy>   m_dicCommandMap = new Dictionary<Int32, BMCommandProxy>();

		public bool ContainsKey( Int32 _CmdID )
		{
			return m_dicCommandMap.ContainsKey( _CmdID );
		}

		public bool TryGetValue( Int32 key, out BMCommandProxy value )
		{
			return m_dicCommandMap.TryGetValue( key, out value );
		}

		public void AddCommnad( BMCommandProxy cmd )
		{
			m_dicCommandMap.Add( cmd.m_nID, cmd );
		}
	}
}