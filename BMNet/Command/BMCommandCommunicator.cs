﻿using BMDK;
using System;

////using System.Collections.Concurrent;
using System.Collections.Generic;

namespace BMNet
{
	using BMCommandList = BMConcurrentQueue<BMCommand>;

	public enum CrowdState
	{
		COMFORTABLE = 0,
		CROWDED,
		OVERFULL,
	}

	public abstract class BMCommandCommunicator
	{
		public string CommunicatorType { get; protected set; }
		public UInt64 MyUID { get;  protected set; }
		protected Queue<BMCommand>      m_CommandQueue  = new Queue<BMCommand>();
		
		protected BMCommandMap          m_CommandMap    = new BMCommandMap();
		protected BMCommandManager      m_CommandMgr    = new BMCommandManager();

		protected BMUIDGenerator        m_kUIDGenerator = new BMUIDGenerator();

		protected ICommandBuilder       m_kCommandBuilder;

		public BMCommandManager CommandMgr
		{
			get { return m_CommandMgr; }
		}

		public ICommandBuilder CommandBuilder
		{
			get { return m_kCommandBuilder; }
			set { m_kCommandBuilder = value; }
		}

		public BMCommandCommunicator()
		{
			CreateCommandBuilder();
		}

		protected virtual void CreateCommandBuilder()
		{
			m_kCommandBuilder = new BMCommandBuilder();
		}

		public abstract void SendCommand( BMCommand _kCommand );

		//public abstract bool Send(UInt64 uidReceiver, SendBuffer buff);
		protected abstract void OnUpdate();

		internal UInt64 NewUID()
		{
			return m_kUIDGenerator.Generate();
		}

		internal UInt64 GetUID()
		{
			return MyUID;
		}

		public virtual void SetCommandHandler( int nCmdID, BMCommandHandler _kHandler, BMCommandHanderFunc fnFunc )
		{
			if( true == m_CommandMap.ContainsKey( nCmdID ) )
			{
				// 로그
				BMLogger.WarnLog( "SetCommandHandler Duplication CmdID : {0} ", nCmdID );
				return;
			}

			BMCommandProxy cmd = new BMCommandProxy( nCmdID, _kHandler, fnFunc );

			m_CommandMap.AddCommnad( cmd );
		}

		public virtual bool Post( BMCommand kCommand )
		{
			if( false == OnCheckCommandFloodingSend( kCommand ) )
				return true;

			if( false == kCommand.CheckRule() )
				return false;

			// 자기 자신에게 보내는 패킷이므로 Recv로 처리할 수 있도록 한다.
			if( kCommand.ReceiverUID == MyUID )
			{
				RecvCommand( kCommand );
				return true;
			}

			SendCommand( kCommand );

			return true;
		}

		public void AsyncPost( BMCommand kCommand )
		{   // 별도의 스레드에서 호출할 경우에 사용

			// 자기 자신에게 보내는 패킷이므로 Recv로 처리할 수 있도록 한다.
			if( kCommand.ReceiverUID == MyUID )
			{
				RecvCommand( kCommand );
				return;
			}

			ReservedPost( kCommand );
		}

		private void ReservedPost( BMCommand kNewCommand )
		{
			m_CommandMgr.ResevedCommand( kNewCommand );
		}

		public void Update()
		{
			OnPrepareUpdate();

			while( true )
			{
				BMCommand cmd;
				if( false == PopCommand( out cmd ) )
					break;

				OnPreCommand( cmd );
				OnCommand( cmd );
				OnPostCommand( cmd );
			}

			OnUpdate();
		}

		protected virtual void OnPrepareUpdate()
		{
			while( true )
			{
				BMCommand cmd;
				if( false == m_CommandMgr.TryGetResevedCommand( out cmd ) )
					break;

				Post( cmd );
			}

			int popCount = 0;
			int cmdCount = m_CommandMgr.Count;
			while( popCount < cmdCount )
			{
				BMCommand cmd;
				if( false == TryGetCommandFromManager( out cmd ) )
					break;

				m_CommandQueue.Enqueue( cmd );

				popCount++;
			}
		}


		private bool PopCommand( out BMCommand _kCmd )
		{
			_kCmd = default( BMCommand );
			if( m_CommandQueue.Count == 0 )
				return false;

			_kCmd = m_CommandQueue.Dequeue();
			return _kCmd != null;
		}

		private bool TryGetCommandFromManager( out BMCommand _kCmd )
		{
			return m_CommandMgr.TryGetCommand( out _kCmd );
		}

		protected virtual void OnPreCommand( BMCommand _Cmd )
		{
		}

		protected virtual void OnPostCommand( BMCommand _Cmd )
		{
		}

		public virtual void RecvCommand( BMCommand _kCommand )
		{
			m_CommandMgr.Push( _kCommand );
		}

		protected virtual BMCommandResult OnCommand( BMCommand _kCommand )
		{
			try
			{
				BMCommandProxy kProxy;
				if( false == m_CommandMap.TryGetValue( _kCommand.ID, out kProxy ) )
				{
					// 로그
					BMLogger.WarnLog( "OnCommand Not Found CmdID : {0} ", _kCommand.ID );
					return BMCommandResult.FAIL;
				}

				kProxy.m_Function( _kCommand, kProxy.m_kHandler );
			}
			catch( System.Exception ex )
			{
				// 로그
				BMLogger.ExceptionLog( ex );
				return BMCommandResult.ERROR;
			}

			return BMCommandResult.SUCCESS;
		}

		public BMCommand NewCommand( Int32 nCmdID, UInt64 uidTarget )
		{
			return new BMCommand( nCmdID, MyUID, uidTarget );
		}

		internal BMCommand NewLocalCommand( Int32 nCmdID )
		{
			return new BMCommand( nCmdID, MyUID, MyUID );
		}

		private bool OnCheckCommandFloodingSend( BMCommand _kCommand )
		{
			return true;
		}
	}
}