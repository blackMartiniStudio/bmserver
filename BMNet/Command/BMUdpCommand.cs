﻿using System;

namespace BMNet
{
	public class BMUdpCommand
	{
		private Byte    m_nFromID;
		private Byte    m_nToID;
		private Int32   m_CmdId;
		private Object  m_kBodyObj;

		public Int32 ID
		{
			get { return m_CmdId; }
		}

		public Byte FromID
		{
			get { return m_nFromID; }
			set { m_nFromID = value; }
		}

		public Byte ToID
		{
			get { return m_nToID; }
			set { m_nToID = value; }
		}

		public Object Body
		{
			get { return m_kBodyObj; }
		}

		private void Reset()
		{
		}

		public BMUdpCommand( Int32 _CmdId, Byte _fromID, Byte _toID )
		{
			m_CmdId = _CmdId;
			m_nFromID = _fromID;
			m_nToID = _toID;
		}

		internal bool CheckRule()
		{
			// Commad에 대한 검증

			return true;
		}

		public void SetPacket( Object _kPkt )
		{
			m_kBodyObj = _kPkt;
		}
	}
}