﻿using System;

namespace BMNet
{
	public interface IUdpCommandBuilder
	{
		UdpPacketHeader BuildToHeader( byte[] _arBuffer, int _nBufferLen );

		BMUdpCommand BuildToCommand( byte[] _arBuffer, int _nBufferLen );

		bool BuildToStream( BMUdpCommand _kCmd, out Byte[] headerBuffer, out Byte[] bodyBuffer );
	}
}