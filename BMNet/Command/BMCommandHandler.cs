﻿using System;
using System.Collections.Generic;

namespace BMNet
{
	public delegate BMCommandResult BMCommandHanderFunc( BMCommand _kCommand, BMCommandHandler _kHandler );

	public class BMCommandHandler
	{
		protected BMCommandCommunicator m_CommandCommunicator;
		private List<Int32> m_liCmdID = new List<int>();

		public BMCommandHandler( BMCommandCommunicator _CommandCommunicator )
		{
			m_CommandCommunicator = _CommandCommunicator;
		}

		public void SetCmdHandler( Int32 nID, BMCommandHanderFunc fnFunc )
		{
			m_CommandCommunicator.SetCommandHandler( nID, this, fnFunc );
		}

		public BMCommandCommunicator GetCommandCommunicator()
		{
			return m_CommandCommunicator;
		}
// 
// 		protected virtual BMNetClient ToNetClient( BMCommandHandler _kHandler )
// 		{
// 			return _kHandler.GetCommandCommunicator() as BMNetClient;
// 		}
	}
}