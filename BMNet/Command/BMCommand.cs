﻿using System;

namespace BMNet
{
	public class BMCommand
	{
		private UInt64  m_uidSenderer;
		private UInt64  m_uidReceiver;
		private Int32   m_CmdId;
		private Object  m_kPacket;

		//private	List<UInt64>	m_liAdditiveReceiver;	// 메세지를 받는 머신이 여러대일 경우 여기에 추가해서 사용

		public Int32 ID
		{
			get { return m_CmdId; }
		}

		public UInt64 SendererUID
		{
			get { return m_uidSenderer; }
			set { m_uidSenderer = value; }
		}

		public UInt64 ReceiverUID
		{
			get { return m_uidReceiver; }
			set { m_uidReceiver = value; }
		}

		public Object Packet
		{
			get { return m_kPacket; }
		}

		private void Reset()
		{
		}

		public BMCommand( Int32 _CmdId, UInt64 _uidSender, UInt64 _uidReceiver )
		{
			m_CmdId = _CmdId;
			m_uidSenderer = _uidSender;
			m_uidReceiver = _uidReceiver;
		}

		internal bool CheckRule()
		{
			// Commad에 대한 검증

			return true;
		}

		public void SetPacket( Object _kPkt )
		{
			m_kPacket = _kPkt;
		}
	}
}