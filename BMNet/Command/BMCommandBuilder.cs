﻿//using System.Collections.Concurrent;
using BMDK;
using System;

namespace BMNet
{
	public class BMCommandBuilder : ICommandBuilder
	{
		public bool BuildToStream( BMCommand _kCmd, out Byte[] header, out Byte[] body )
		{
			header = null;
			body = null;
			Int32 bodyLength = 0;

			bool isCrypt = false;
			UInt32 dataCRC = 0;
			UInt16 packetOption = PacketConstant.PACKET_CRYPT_OPTION;

			if( _kCmd.Packet == null )
			{
				body = new Byte[0];
				bodyLength = 0;
				packetOption = PacketConstant.PACKET_OPTION_NONE;
			}
			else
			{
				body = BodyEncode( _kCmd, out isCrypt );
				bodyLength = body.Length;

				if( isCrypt == true && PacketConstant.IsEncrypt( packetOption ) )
				{
					if( Encryptor.INSTANCE.Encrypt( ref body, bodyLength ) == false )
						throw new InvalidOperationException( "Encrypt Fail !!! pkt " + _kCmd.ID );
				}
				else
				{
					packetOption = (UInt16)( packetOption & ( ~PacketConstant.PACKET_OPTION_ENCRYPTION ) );
				}

				if( PacketConstant.IsDataCRC( packetOption ) == true )
				{
					if( CRCChecker.INSTANCE.GetCRC( body, bodyLength, ref dataCRC ) == false )
						throw new InvalidOperationException( "Make CRC FAil protocol " + (PacketCommandEnum)_kCmd.ID );
				}
				else
				{
					packetOption = (UInt16)( packetOption & ( ~PacketConstant.PACKET_OPTION_DATA_CRC ) );
				}
			}

			bool notUsingCrypt = false;
			header = HeaderEncode( _kCmd.ID, packetOption, dataCRC, bodyLength, out notUsingCrypt );

			if( header == null )
				return false;

			return true;
		}

		protected virtual byte[] HeaderEncode( Int32 _cmdID, UInt16 packetOption, UInt32 dataCRC, Int32 bodyLength, out bool notUsingCrypt )
		{
			return PacketCommandEncoder.EncodeC2SPacketHeader( new C2SPacketHeader( _cmdID, packetOption, dataCRC, bodyLength ), out notUsingCrypt );
		}

		protected virtual byte[] BodyEncode( BMCommand _kCmd, out bool isCrypt )
		{
			return PacketCommandCoder.Encode( _kCmd.ID, _kCmd.Packet, out isCrypt );
		}

		public Int32 BuildToCommand( byte[] _arBuffer, int _nBufferLen, ref BMConcurrentQueue<BMCommand> _refCommandList )
		{
			Int32 nOffset = 0;
			Int32 nCurrentBufferLen = _nBufferLen;
			Int32 nBuildCmdCount = 0;

			try
			{
				Int32 nHeaderSize = (Int32)PacketVariables.PACKET_HEADER_SIZE;
				while( nCurrentBufferLen >= nHeaderSize )
				{
					C2SPacketHeader kHeader = HeaderDecode( _arBuffer, nOffset );

					if( kHeader == null )
						throw new InvalidOperationException( CommandBuildError.BUILD_CMD_ERR_INCORRECT_HEADER_TYPE.ToString() );

					if( kHeader.mCode <= 0 )
						throw new InvalidOperationException( CommandBuildError.BUILD_CMD_ERR_CMD_ID.ToString() );

					Int32 nBodySize = kHeader.mBodySize;
					Int32 nPacketSize = nHeaderSize + nBodySize;

					// 데이터의 크기가 패킷의 크기보다 작다면 데이터를 덜 받았다.
					// 다음에 조합하도록 한다.
					if( nCurrentBufferLen < nPacketSize )
						break;

					Object kBody = BodyDecode( _arBuffer, kHeader, nOffset );
					BMCommand kCmd = new BMCommand( kHeader.mCode, 0, 0 );
					kCmd.SetPacket( kBody );

					_refCommandList.Enqueue( kCmd );

					nOffset += nPacketSize;
					nCurrentBufferLen -= nPacketSize;
					nBuildCmdCount++;
				}
			}
			catch( Exception ex )
			{
				//  로그
				BMLogger.ExceptionLog( ex );
			}

			return nCurrentBufferLen;
		}

		protected virtual C2SPacketHeader HeaderDecode( byte[] _arBuffer, Int32 _nOffset )
		{
			Int32 nHeaderSize = (Int32)PacketVariables.PACKET_HEADER_SIZE;

			Byte[] headerStream = new Byte[nHeaderSize];
			Buffer.BlockCopy( _arBuffer, _nOffset, headerStream, 0, nHeaderSize );

			// 해드 부분을 디코더 해서 바디 사이즈 확인
			return PacketCommandDecoder.DecodeC2SPacketHeader( headerStream );
		}

		protected Object BodyDecode( byte[] _arBuffer, C2SPacketHeader _kHeader, int _nOffset )
		{
			Int32 nHeaderSize = (Int32)PacketVariables.PACKET_HEADER_SIZE;
			Int32 nBodySize = _kHeader.mBodySize;

			Byte[] bodyStream = new Byte[nBodySize];
			Buffer.BlockCopy( _arBuffer, _nOffset + nHeaderSize, bodyStream, 0, nBodySize );

			UInt32 nOption = _kHeader.mOption;

			if( PacketConstant.IsDataCRC( nOption ) == true )
			{
				UInt32 dataCRC = 0;

				if( CRCChecker.INSTANCE.GetCRC( bodyStream, bodyStream.Length, ref dataCRC ) == false )
					throw new InvalidOperationException( String.Format( "{0} : CmdID = {1}", CommandBuildError.BUILD_CMD_ERR_CMD_ID.ToString(), _kHeader.mCode ) );

				if( _kHeader.mCRC != dataCRC )
					throw new InvalidOperationException( String.Format( "{0} : CmdID = {1}", CommandBuildError.BUILD_CMD_ERR_CHECK_CRC.ToString(), _kHeader.mCode ) );
			}

			if( PacketConstant.IsEncrypt( nOption ) == true )
			{
				if( Encryptor.INSTANCE.Decrypt( ref bodyStream, bodyStream.Length ) == false )
					throw new InvalidOperationException( String.Format( "{0} : CmdID = {1}", CommandBuildError.BUILD_CMD_ERR_DECODE.ToString(), _kHeader.mCode ) );
			}

			// 해드 부분을 디코더 해서 바디 사이즈 확인
			return BodyDecod( _kHeader.mCode, bodyStream );
		}

		protected virtual Object BodyDecod( int cmd, byte[] bodyBuffer )
		{
			return PacketCommandCoder.Decode( cmd, bodyBuffer );
		}
	}
}