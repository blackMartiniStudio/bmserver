﻿using System;

namespace BMNet
{
	internal class BMUdpCommandBuilder : IUdpCommandBuilder
	{
		public UdpPacketHeader BuildToHeader( Byte[] _arBuffer, Int32 _nBufferLen )
		{
			UdpPacketHeader kHeader = HeaderDecode( _arBuffer );
			return kHeader;
		}

		public BMUdpCommand BuildToCommand( byte[] _arBuffer, int _nBufferLen )
		{
			UdpPacketHeader kHeader = HeaderDecode( _arBuffer );
			BMUdpCommand kCmd = new BMUdpCommand( kHeader.cmdID, kHeader.fromSlotId, kHeader.toSlotId );

			kCmd.SetPacket( BodyDecode( kHeader.cmdID, _arBuffer ) );

			return kCmd;
		}

		protected virtual UdpPacketHeader HeaderDecode( byte[] _arBuffer )
		{
			// 해드 부분을 디코더 해서 바디 사이즈 확인
			return UdpPacketDecoder.DecodeUdpPacketHeader( _arBuffer );
		}

		protected virtual Object BodyDecode( Int32 nCmdID, byte[] _arBuffer )
		{
			return UdpPacketCoder.Decode( nCmdID, _arBuffer );
		}

		public bool BuildToStream( BMUdpCommand _kCmd, out Byte[] headerBuffer, out Byte[] bodyBuffer )
		{
			headerBuffer = null;
			bodyBuffer = null;

			if( false == TryBodyEncode( _kCmd.ID, _kCmd.Body, out bodyBuffer ) )
				return false;

			headerBuffer = HeaderEncode( _kCmd.ID, _kCmd.FromID, _kCmd.ToID, bodyBuffer.Length );

			return true;
		}

		private bool TryBodyEncode( Int32 _cmdPacket, Object _kBodyObj, out Byte[] bodyBuff )
		{
			if( _kBodyObj == null )
			{
				bodyBuff = new Byte[0];
				return true;
			}

			bodyBuff = UdpPacketCoder.Encode( _cmdPacket, _kBodyObj );
			if( bodyBuff == null )
				return false;

			return true;
		}

		private Byte[] HeaderEncode( Int32 _cmdPacket, Byte _nFromSlotID, Byte _nToSlotID, Int32 nBodyBufferSize )
		{
			UdpPacketHeader kHeader = new UdpPacketHeader( _nFromSlotID, _nToSlotID, _cmdPacket, nBodyBufferSize );
			return UdpPacketCoder.Encode( (Int32)UdpPacketEnum.CMD_UdpPacketHeader, kHeader );
		}
	}
}