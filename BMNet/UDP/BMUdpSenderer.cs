﻿using System;
using System.Net;
using System.Net.Sockets;
using BMDK;

namespace BMNet
{
	public class BMUdpSenderer
	{
		private BMSocket            m_kSocket;
		private IUdpSendListener    m_kSendListener;

		public BMUdpSenderer( BMSocket m_kSocket )
		{
			this.m_kSocket = m_kSocket;
		}

		internal void Send( Byte[] m_Buff, Int32 _nSize, EndPoint m_kDestAddress )
		{
			m_kSocket.Sock.BeginSendTo( m_Buff, 0, _nSize, SocketFlags.None, m_kDestAddress, CallBackSend, m_kSocket );
		}

		private void CallBackSend( IAsyncResult ar )
		{
			try
			{
				BMSocket kTcpSocket = (BMSocket)ar.AsyncState;
				Socket socket = kTcpSocket.Sock;

				Int32 bytesTransferred = socket.EndSendTo( ar );

				OnSend( bytesTransferred );
			}
			catch( Exception ex )
			{
				// 로그
				BMLogger.ExceptionLog( ex );
			}
		}

		private void OnSend( Int32 bytesTransferred )
		{
			if( null == m_kSendListener )
				return;

			m_kSendListener.OnSend( bytesTransferred );
		}

		internal void SetListener( IUdpSendListener _kRecvListener )
		{
			m_kSendListener = _kRecvListener;
		}
	}
}