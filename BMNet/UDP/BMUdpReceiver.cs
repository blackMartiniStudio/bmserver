﻿using System;
using System.Net;
using System.Net.Sockets;
using BMDK;

namespace BMNet
{
	public class BMUdpReceiver
	{
		private BMSocket            m_kSocket;
		private Byte[]              m_Buff;
		private EndPoint            m_kPeerAddress;
		private IUdpRecvListener    m_kListener;

		public BMUdpReceiver( BMSocket _kSocket )
		{
			m_kSocket = _kSocket;
			m_Buff = new Byte[(Int32)NetCommon.MAX_PACKET_SIZE];
			m_kPeerAddress = new IPEndPoint( IPAddress.Any, 0 );
		}

		public void Recv()
		{
			m_kSocket.Sock.BeginReceiveFrom( m_Buff, 0, (Int32)NetCommon.MAX_PACKET_SIZE, SocketFlags.None, ref m_kPeerAddress, CallBackRecv, m_kSocket );
		}

		public void CallBackRecv( IAsyncResult ar )
		{
			try
			{
				BMSocket kTcpSocket = (BMSocket)ar.AsyncState;
				Socket socket = kTcpSocket.Sock;

				Int32 bytesTransferred = socket.EndReceiveFrom( ar, ref m_kPeerAddress );

				OnRecv( bytesTransferred, m_Buff, m_kPeerAddress );
			}
			catch( Exception ex )
			{
				// 로그
				BMLogger.ExceptionLog( ex );
			}
			finally
			{
				try
				{
					BMSocket kTcpSocket = (BMSocket)ar.AsyncState;
					Socket socket = kTcpSocket.Sock;

					if( socket != null )
					{
						EndPoint tempPeerAddress = new IPEndPoint( IPAddress.Any, 0 );

						Array.Clear( m_Buff, 0, m_Buff.Length );
						socket.BeginReceiveFrom( m_Buff, 0, (Int32)NetCommon.MAX_PACKET_SIZE, SocketFlags.None, ref m_kPeerAddress, CallBackRecv, m_kSocket );
					}
				}
				catch( Exception ex )
				{
					// 로그
					BMLogger.ExceptionLog( ex );
				}
			}
		}

		private void OnRecv( Int32 bytesTransferred, Byte[] m_Buff, EndPoint m_kPeerAddress )
		{
			if( m_kListener == null )
				return;

			m_kListener.OnRecv( bytesTransferred, m_Buff, m_kPeerAddress );
		}

		internal void SetListener( IUdpRecvListener _kRecvListener )
		{
			m_kListener = _kRecvListener;
		}
	}
}