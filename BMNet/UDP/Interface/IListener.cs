﻿using System;
using System.Net;

namespace BMNet
{
	public interface IUdpRecvListener
	{
		void OnRecv( Int32 bytesTransferred, Byte[] m_Buff, EndPoint m_kPeerAddress );
	}

	public interface IUdpSendListener
	{
		void OnSend( Int32 bytesTransferred );
	}
}