﻿using System;
using System.Net;

namespace BMNet
{
	public interface ICommandFilter
	{
		bool OnRecv( EndPoint peerAddress, Byte[] m_Buff, Int32 bytesTransferred );
	}
}