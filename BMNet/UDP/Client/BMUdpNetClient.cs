﻿using System;
using System.Net;

namespace BMNet
{
	public class BMUdpNetClient : IUdpCommandCommunicator
	{
		protected IPEndPoint            m_kLocalAddress;
		protected BMUDPReactor          m_kReactor;

		protected IPEndPoint            m_kRemoteAddress;
		protected IUdpCommandBuilder    m_kCmdBuilder;

		protected IUdpRecvListener      m_kRecvListener;
		protected IUdpSendListener      m_kSendListener;

		protected ICommandFilter        m_kFilter;
		protected BMUdpCommandHandler   m_kCommandHandler;

		public BMUdpNetClient()
		{
			m_kReactor = new BMUDPReactor();
			m_kCmdBuilder = new BMUdpCommandBuilder();
			m_kRecvListener = new BMUdpClientRecvListener( this );
			m_kSendListener = new BMUdpClientSendListener( this );
			m_kCommandHandler = new BMUdpCommandHandler( this );

			m_kReactor.SetRecvListener( m_kRecvListener );
			m_kReactor.SetSendListener( m_kSendListener );
		}

		public bool Create( Int32 _LocalPort, String _strServerIP, Int32 _nServerPort )
		{
			m_kLocalAddress = new IPEndPoint( IPAddress.Any, _LocalPort );
			m_kRemoteAddress = new IPEndPoint( IPAddress.Parse( _strServerIP ), _nServerPort );

			return m_kReactor.Create( m_kLocalAddress );
		}

		public void Run()
		{
			m_kReactor.Start();
		}

		public IUdpCommandBuilder GetCommandBuilder()
		{
			return m_kCmdBuilder;
		}

		internal void RecvCommand( BMUdpCommand kCmd, EndPoint m_kPeerAddress )
		{
			m_kCommandHandler.OnCommand( kCmd, m_kPeerAddress );
		}

		public void PostCommand( BMUdpCommand _kCmd )
		{
			Byte[] header;
			Byte[] body;
			if( false == m_kCmdBuilder.BuildToStream( _kCmd, out header, out body ) )
				return;

			Byte[] rowData = new Byte[header.Length + body.Length];
			Buffer.BlockCopy( header, 0, rowData, 0, header.Length );
			Buffer.BlockCopy( body, 0, rowData, header.Length, body.Length );

			m_kReactor.Send( rowData, rowData.Length, m_kRemoteAddress );
		}

		public void PostCommand( BMUdpCommand _kCmd, EndPoint peerAddress )
		{
			Byte[] header;
			Byte[] body;
			if( false == m_kCmdBuilder.BuildToStream( _kCmd, out header, out body ) )
				return;

			Byte[] rowData = new Byte[header.Length + body.Length];
			Buffer.BlockCopy( header, 0, rowData, 0, header.Length );
			Buffer.BlockCopy( body, 0, rowData, header.Length, body.Length );

			m_kReactor.Send( rowData, rowData.Length, peerAddress );
		}

		public BMUdpCommand NewCommand( UdpPacketEnum _cmdPacket, Byte _nFromSlotID, Byte _nToSlotID )
		{
			return new BMUdpCommand( (Int32)_cmdPacket, _nFromSlotID, _nToSlotID );
		}

		public void SetFilter( ICommandFilter _kFilter )
		{
			m_kFilter = _kFilter;
		}

		public ICommandFilter GetFilter()
		{
			return m_kFilter;
		}

		public void SetCmdHandler( Int32 _nCmdID, BMudpCommandHanderFunc _func )
		{
			m_kCommandHandler.AddHandler( _nCmdID, _func );
		}
	}
}