﻿using System;

namespace BMNet
{
	public class BMUdpClientSendListener : IUdpSendListener
	{
		private BMUdpNetClient m_kCommunicator;

		public BMUdpClientSendListener( BMUdpNetClient _kCommunicator )
		{
			m_kCommunicator = _kCommunicator;
		}

		public void OnSend( Int32 bytesTransferred )
		{
		}
	}
}