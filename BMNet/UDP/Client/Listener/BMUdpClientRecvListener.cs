﻿using System;
using System.Net;

namespace BMNet
{
	public class BMUdpClientRecvListener : IUdpRecvListener
	{
		private BMUdpNetClient m_kCommunicator;

		public BMUdpClientRecvListener( BMUdpNetClient _kCommunicator )
		{
			m_kCommunicator = _kCommunicator;
		}

		public void OnRecv( Int32 bytesTransferred, Byte[] m_Buff, EndPoint m_kPeerAddress )
		{
			ICommandFilter kFilter = m_kCommunicator.GetFilter();
			if( null != kFilter && false == kFilter.OnRecv( m_kPeerAddress, m_Buff, bytesTransferred ) )
				return;

			IUdpCommandBuilder kBuilder = m_kCommunicator.GetCommandBuilder();
			BMUdpCommand kCmd = kBuilder.BuildToCommand( m_Buff, bytesTransferred );

			m_kCommunicator.RecvCommand( kCmd, m_kPeerAddress );
		}
	}
}