﻿using System;
using System.Net;
using System.Net.Sockets;

namespace BMNet
{
	public class BMUDPReactor
	{
		static private int SIO_UDP_CONNRESET = -1744830452;
		private BMUdpSocket     m_kSocket;
		private BMUdpReceiver   m_Receiver;
		private BMUdpSenderer   m_Senderer;

		public BMUDPReactor()
		{
			m_kSocket = new BMUdpSocket();
			m_Receiver = new BMUdpReceiver( m_kSocket );
			m_Senderer = new BMUdpSenderer( m_kSocket );
		}

		internal bool Create( IPEndPoint _kLocalAddress )
		{
			if( false == m_kSocket.CreateSocket( AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp, false, true ) )
			{
				return false;
			}

			// Unit 에서는 동작하지 않음
			// m_kSocket.Sock.IOControl( (IOControlCode)SIO_UDP_CONNRESET, new Byte[] { 0, 0, 0, 0 }, null );
			m_kSocket.Sock.Bind( _kLocalAddress );

			return true;
		}

		internal void Start()
		{
			m_Receiver.Recv();
		}

		public void Terminate()
		{
			m_kSocket.Sock.Shutdown( SocketShutdown.Both );
			m_kSocket.Sock.Close();
		}

		public void SetRecvListener( IUdpRecvListener _kRecvListener )
		{
			m_Receiver.SetListener( _kRecvListener );
		}

		public void SetSendListener( IUdpSendListener _kRecvListener )
		{
			m_Senderer.SetListener( _kRecvListener );
		}

		public void Send( Byte[] _Buff, Int32 _nSize, EndPoint m_kDestAddress )
		{
			m_Senderer.Send( _Buff, _nSize, m_kDestAddress );
		}
	}
}