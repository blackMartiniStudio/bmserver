﻿using System;
using System.Net.Sockets;
using BMDK;

namespace BMNet
{
	public class BMUdpSocket : BMSocket
	{
		public override bool CreateSocket( AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType, bool bNoDelay, bool bReuse )
		{
			try
			{
				m_bNoDelay = bNoDelay;
				m_bReuse = bReuse;

				m_kSocket = new Socket( addressFamily, socketType, protocolType );
				m_kSocket.Blocking = false;
				m_kSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.NoDelay, bNoDelay );
				m_kSocket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, bReuse );
			}
			catch( Exception ex )
			{
				// 로그
				BMLogger.ExceptionLog( ex );

				return false;
			}

			return true;
		}
	}
}