﻿using System;
using System.Collections.Generic;
using System.Net;

namespace BMNet
{
	public class BMUdpNetServer : IUdpCommandCommunicator
	{
		protected IPEndPoint            m_kLocalAddress;
		protected BMUDPReactor          m_kReactor;

		protected IUdpCommandBuilder    m_kCmdBuilder;
		protected BMUdpRecvListener     m_kRecvListener;
		protected BMUdpSendListener     m_kSendListener;

		protected HashSet<EndPoint>     m_setPeerAddress;

		protected ICommandFilter        m_kFilter;

		public BMUdpNetServer()
		{
			m_kReactor = new BMUDPReactor();
			m_kCmdBuilder = new BMUdpCommandBuilder();
			m_kRecvListener = new BMUdpRecvListener( this );
			m_kSendListener = new BMUdpSendListener( this );
			m_setPeerAddress = new HashSet<EndPoint>();

			m_kReactor.SetRecvListener( m_kRecvListener );
			m_kReactor.SetSendListener( m_kSendListener );
		}

		public bool Create( Int32 _Port )
		{
			m_kLocalAddress = new IPEndPoint( IPAddress.Any, _Port );
			return m_kReactor.Create( m_kLocalAddress );
		}

		public void Run()
		{
			m_kReactor.Start();
		}

		public IUdpCommandBuilder GetCommandBuilder()
		{
			return m_kCmdBuilder;
		}

		public void Terminate()
		{
			m_setPeerAddress.Clear();
			m_kReactor.Terminate();
		}

		public virtual void RecvCommand( Int32 bytesTransferred, Byte[] m_Buff, UdpPacketHeader kHaeder, EndPoint m_kPeerAddress )
		{
		}

		public virtual void Broadcast( Byte[] m_Buff, Int32 _nSize, EndPoint m_kPeerAddress )
		{
		}

		public void AddPeerAddressObject( EndPoint _kPeerAddress )
		{
			if( true == m_setPeerAddress.Contains( _kPeerAddress ) )
				return;

			m_setPeerAddress.Add( _kPeerAddress );
		}

		public void SetFilter( ICommandFilter _kFilter )
		{
			m_kFilter = _kFilter;
		}

		public ICommandFilter GetFilter()
		{
			return m_kFilter;
		}
	}
}