﻿using System;
using System.Net;

namespace BMNet
{
	public class BMUdpRecvListener : IUdpRecvListener
	{
		private BMUdpNetServer m_kCommunicator;

		public BMUdpRecvListener( BMUdpNetServer _kCommunicator )
		{
			m_kCommunicator = _kCommunicator;
		}

		public void OnRecv( Int32 bytesTransferred, Byte[] m_Buff, EndPoint m_kPeerAddress )
		{
			ICommandFilter kFilter = m_kCommunicator.GetFilter();
			if( null != kFilter && false == kFilter.OnRecv( m_kPeerAddress, m_Buff, bytesTransferred ) )
				return;

			IUdpCommandBuilder kBuilder = m_kCommunicator.GetCommandBuilder();
			UdpPacketHeader kHeader = kBuilder.BuildToHeader( m_Buff, bytesTransferred );

			m_kCommunicator.RecvCommand( bytesTransferred, m_Buff, kHeader, m_kPeerAddress );
		}
	}
}