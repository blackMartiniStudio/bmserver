﻿using System;

namespace BMNet
{
	public class BMUdpSendListener : IUdpSendListener
	{
		private BMUdpNetServer m_kCommunicator;

		public BMUdpSendListener( BMUdpNetServer _kCommunicator )
		{
			m_kCommunicator = _kCommunicator;
		}

		public void OnSend( Int32 bytesTransferred )
		{
		}
	}
}