﻿using NUnit.Framework;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;
using WebServer.Domain.Entities;
using Moq;
using Couchbase.Core;
using WebServer.Test.CouchBase.Helper;
using System;
using Couchbase;
using WebServer.Domain.CouchBase.Transaction;
using Couchbase.IO;
using WebServer.Domain.CouchBase.StroageHelper;
using WebServer.Domain.CouchBase;
using Newtonsoft.Json;

namespace WebServer.CouchBase.Transaction
{
	[TestFixture]
	[Category( "WebServer.CouchBase.Transaction" )]
	public class TransactionTest
	{
		[Test]
		public async Task Transaction_TradeMoney_Success()
		{

			TradeMoney kTradeMoeny = new TradeMoney();
			kTradeMoeny.fromAID = 100000000001;
			kTradeMoeny.toAID = 100000000002;
			kTradeMoeny.money = 1500;

			UInt64 nTransactionID = 1;

			TSTradeMoney kTSTrade = new TSTradeMoney( kTradeMoeny );
			PlayerData kPlayerFrom = EntityHelper.GetFromPlayerObj();
			PlayerData kPlayerTo = EntityHelper.GetToPlayerObj();

			Mock<IBucket> mockBucket = new Mock<IBucket>();
			FakeStorage kFakeStorage = new FakeStorage( mockBucket );

			kTSTrade.id = nTransactionID;

			kFakeStorage.Storage.Add( kTSTrade.FromKey(), EntityHelper.GetObjToJson( kPlayerFrom ) );
			kFakeStorage.Storage.Add( kTSTrade.ToKey(), EntityHelper.GetObjToJson( kPlayerTo ) );

			var kTransaction = new Transaction<FakeStorage, TSTradeMoney, PlayerData, PlayerData>( kFakeStorage, kTSTrade );
			await kTransaction.Execute();

			Assert.AreNotEqual( null, kTransaction.TransactionDoc );
			Assert.AreEqual( kTransaction.TransactionDoc.state, "success" );

			Object playerFrom;
			Object playerTo;
			kFakeStorage.GetStorageData( kTSTrade.FromKey(), out playerFrom );
			kFakeStorage.GetStorageData( kTSTrade.ToKey(), out playerTo );
			Assert.AreEqual( kPlayerFrom.money - kTradeMoeny.money, EntityHelper.GetJosnToObj<PlayerData>( playerFrom ).money );
			Assert.AreEqual( kPlayerTo.money + kTradeMoeny.money, EntityHelper.GetJosnToObj<PlayerData>( playerTo ).money );
		}

		[Test]
		public async Task Transaction_TradeMoney_Fail_RollBack()
		{

			TradeMoney kTradeMoeny = new TradeMoney();
			kTradeMoeny.fromAID = 100000000001;
			kTradeMoeny.toAID = 100000000002;
			kTradeMoeny.money = 1500;

			UInt64 nTransactionID = 1;

			TSTradeMoney kTSTrade = new TSTradeMoney( kTradeMoeny );
			PlayerData kPlayerFrom = EntityHelper.GetFromPlayerObj();
			PlayerData kPlayerTo = EntityHelper.GetToPlayerObj();

			Mock<IBucket> mockBucket = new Mock<IBucket>();
			FakeStorage kFakeStorage = new FakeStorage( mockBucket );

			kTSTrade.id = nTransactionID;

			kFakeStorage.Storage.Add( kTSTrade.FromKey(), EntityHelper.GetObjToJson( kPlayerFrom ) );
			kFakeStorage.Storage.Add( kTSTrade.ToKey(), EntityHelper.GetObjToJson( kPlayerTo ) );

			kFakeStorage.Cas.Add( kTSTrade.FromKey(), 10000 );
			kFakeStorage.Cas.Add( kTSTrade.ToKey(), 10001 );

			var kTransaction = new Transaction<FakeStorage, TSTradeMoney, PlayerData, PlayerData>( kFakeStorage, kTSTrade );
			await kTransaction.Execute();

			Assert.AreNotEqual( null, kTransaction.TransactionDoc );
			Assert.AreEqual( kTransaction.TransactionDoc.state, "fail" );

			Object playerFrom;
			Object playerTo;
			kFakeStorage.GetStorageData( kTSTrade.FromKey(), out playerFrom );
			kFakeStorage.GetStorageData( kTSTrade.ToKey(), out playerTo );
			Assert.AreEqual( kPlayerFrom.money, EntityHelper.GetJosnToObj<PlayerData>( playerFrom ).money );
			Assert.AreEqual( kPlayerTo.money, EntityHelper.GetJosnToObj<PlayerData>( playerTo ).money );
		}
	}
}