﻿using Couchbase;
using Couchbase.Core;
using System;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase;
using WebServer.Domain.CouchBase.StroageHelper;
using Couchbase.N1QL;
using Newtonsoft.Json;
using System.Collections.Generic;
using Moq;
using Couchbase.IO;

namespace WebServer.Test.CouchBase.Helper
{
	
	public class FakeStorage : IStorageHelper
	{
		protected Mock<IBucket> m_mockBucket;
		Dictionary<String, Object> m_dicStore	= new Dictionary<string, Object>();
		Dictionary<String, UInt64> m_dicCas		= new Dictionary<string, UInt64>();

		public FakeStorage( Mock<IBucket> _mockBucket )
		{
			m_mockBucket = _mockBucket;
		}

		public Dictionary<String, Object> Storage
		{
			get { return m_dicStore; }
			set { m_dicStore = value; }
		}

		public Dictionary<String, UInt64> Cas
		{
			get { return m_dicCas; }
			set { m_dicCas = value; }
		}

		public IBucket Bucket
		{
			get { return m_mockBucket.Object; }
		}

		public virtual Task<IQueryResult<dynamic>> ExecuteQuery( IQueryRequest query )
		{
			return Bucket.QueryAsync<dynamic>( query );
		}

		public virtual Task<IQueryResult<dynamic>> ExecuteQuery( string query )
		{
			return ExecuteQuery( new QueryRequest( query ) );
		}

		public virtual string ExecuteQuery( string query, Formatting format )
		{
			return JsonConvert.SerializeObject( ExecuteQuery( query ), format );
		}

		public virtual string ExecuteQuery( IQueryRequest query, Formatting format )
		{
			return JsonConvert.SerializeObject( ExecuteQuery( query ), format );
		}

		Task<IOperationResult<dynamic>> IStorageHelper.GetAsync( string id )
		{
			Object doc = null;
			bool bGetSuccess = GetStorageData( id, out doc );
			
			var docGet = new Mock<IOperationResult<dynamic>>();
			docGet.SetupGet( m => m.Success ).Returns( bGetSuccess );
			docGet.SetupGet( m => m.Status ).Returns( ResponseStatus.Success );
			docGet.SetupGet( m => m.Cas ).Returns( 10000 );
			docGet.SetupGet( m => m.Value ).Returns( doc );
			m_mockBucket.Setup( m => m.GetAsync<dynamic>(id) ).Returns( Task.FromResult( docGet.Object ) );

			return Bucket.GetAsync<dynamic>( id );
		}

		public virtual Task<IOperationResult<dynamic>> UpsertAsync( string id, object model )
		{
			Object jsonModel = EntityHelper.GetObjToJson( model );

			Object doc = null;
			if( true == GetStorageData( id, out doc ) )
			{
				m_dicStore.Remove( id );
			}

			m_dicStore.Add( id, jsonModel );

			var docUpsert = new Mock<IOperationResult<dynamic>>();
			docUpsert.SetupGet( m => m.Success ).Returns( true );
			docUpsert.SetupGet( m => m.Status ).Returns( ResponseStatus.Success );
			m_mockBucket.Setup( m => m.UpsertAsync<dynamic>( id, model ) ).Returns( Task.FromResult( docUpsert.Object ) );

			return Bucket.UpsertAsync<dynamic>( id, model );
		}

		public virtual Task<IOperationResult<dynamic>> ReplaceAsync( string key, object value )
		{
			Object jsonModel = EntityHelper.GetObjToJson( value );

			Object doc = null;
			bool bGetSuccess = GetStorageData( key, out doc );
			if( true == bGetSuccess )
			{
				m_dicStore.Remove( key );
				m_dicStore.Add( key, jsonModel );
			}

			var docReplace = new Mock<IOperationResult<dynamic>>();
			docReplace.SetupGet( m => m.Success ).Returns( bGetSuccess );
			docReplace.SetupGet( m => m.Status ).Returns( ResponseStatus.Success );
			m_mockBucket.Setup( m => m.ReplaceAsync<dynamic>( key, value ) ).Returns( Task.FromResult( docReplace.Object ) );

			return Bucket.ReplaceAsync<dynamic>( key, value );
		}

		public virtual Task<IOperationResult<dynamic>> ReplaceAsync( string key, object value, UInt64 cas )
		{
			Object jsonModel = EntityHelper.GetObjToJson( value );

			Object doc = null;
			bool bGetSuccess = GetStorageData( key, cas, out doc );
			if( true == bGetSuccess )
			{
				m_dicStore.Remove( key );
				m_dicStore.Add( key, jsonModel );
			}

			var docReplace = new Mock<IOperationResult<dynamic>>();
			docReplace.SetupGet( m => m.Success ).Returns( bGetSuccess );
			docReplace.SetupGet( m => m.Status ).Returns( ResponseStatus.Success );
			m_mockBucket.Setup( m => m.ReplaceAsync<dynamic>( key, value, cas ) ).Returns( Task.FromResult( docReplace.Object ) );

			return Bucket.ReplaceAsync<dynamic>( key, value, cas );
		}

		public virtual Task<bool> ExistsAsync( string id )
		{
			return Bucket.ExistsAsync( id );
		}

		public virtual Task<IOperationResult<UInt64>> IncrementAsync( String v1, UInt64 v2, UInt64 v3 )
		{
			var orTransID = new Mock<IOperationResult<ulong>>();
			orTransID.SetupGet( m => m.Success ).Returns( true );
			orTransID.SetupGet( m => m.Status ).Returns( ResponseStatus.Success );
			orTransID.SetupGet( m => m.Value ).Returns( 1 );
			m_mockBucket.Setup( m => m.IncrementAsync( v1, v2, v3 ) ).Returns( Task.FromResult( orTransID.Object ) );

			return Bucket.IncrementAsync( v1, v2, v3 );
		}

		public String GetStorageName()
		{
			return "FakeStorage";
		}

		public bool GetStorageData( String key, out Object value )
		{
			return m_dicStore.TryGetValue( key, out value );
		}

		public bool GetStorageData( String key, UInt64 cas, out Object value )
		{
			if( false == m_dicStore.TryGetValue( key, out value ) )
				return false;

			UInt64 storageCas = 0;
			if( false == m_dicCas.TryGetValue( key, out storageCas ) )
				return true;

			if( storageCas != cas )
				return false;

			return true;
			
		}
	}
}
