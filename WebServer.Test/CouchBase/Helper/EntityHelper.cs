﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer.Domain.CouchBase.Transaction;
using WebServer.Domain.Entities;

namespace WebServer.Test.CouchBase.Helper
{
	public class EntityHelper
	{
		public static String GetObjToJson<Type>( Type obj )
		{
			String _strTSTradeMoney = JsonConvert.SerializeObject( obj );
			return _strTSTradeMoney;
		}

		public static Type GetJosnToObj<Type>( Object obj )
		{
			Type Obj = JsonConvert.DeserializeObject<Type>( obj.ToString() );
			return Obj;
		}

		public static TSTradeMoney GetTransDocToObj()
		{
			String kTransDoc = "{\"tradeData\":{\"fromAID\": 100000000001,\"toAID\": 100000000002,\"money\": 1500},\"id\": 1,\"type\": \"trade\",\"state\": \"init\",\"retry\": 0	}";
			TSTradeMoney _kTSTradeMoney = JsonConvert.DeserializeObject<TSTradeMoney>( kTransDoc.ToString() );
			return _kTSTradeMoney;
		}


		public static PlayerData GetFromPlayerObj()
		{
			String kPlayerDoc = "{ \"aid\": \"100000000001\", \"id\": \"kimjungmin\", \"money\": 10000, \"transactionList\": []	}";
			PlayerData _kPlayerData = JsonConvert.DeserializeObject<PlayerData>( kPlayerDoc.ToString() );
			return _kPlayerData;
		}

		public static PlayerData GetToPlayerObj()
		{
			String kPlayerDoc = "{ \"aid\": \"100000000002\", \"id\": \"chahangun\", \"money\": 5000, \"transactionList\": []	}";
			PlayerData _kPlayerData = JsonConvert.DeserializeObject<PlayerData>( kPlayerDoc.ToString() );
			return _kPlayerData;
		}
	}
}
