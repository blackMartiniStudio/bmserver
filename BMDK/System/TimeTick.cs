﻿using System;

namespace BMDK
{
	public interface ITick
	{
		Int64 GetNowTick();
	}

	public class TimeTick : ITick
	{
		public Int64 GetNowTick()
		{
			return DateTime.Now.Ticks;
		}
	}
}