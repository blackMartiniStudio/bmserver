﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;

namespace BMDK
{
	
	public sealed class BMLogger
	{
		public static IBMLogger logger = new EmptyLogger();

		static public void Initialize( IBMLogger _logger )
		{
			logger = _logger;
		}

		static public void Start()
		{
			logger.Start();
		}

		static public void End()
		{
			logger.End();
		}

		static public void CallerInfoWrite( ref StringBuilder builder )
		{
			StackTrace st = new StackTrace( new StackFrame( 2, true ) );
			StackFrame sf = st.GetFrame( 0 );
			builder.AppendFormat( "\t\t{0}({1},{2}) : {3}\n", sf.GetFileName(), sf.GetFileLineNumber(), sf.GetFileColumnNumber(), sf.GetMethod().Name );
		}

		static private void CallerInfoWrite( StringBuilder builder )
		{
			StackTrace st = new StackTrace( new StackFrame( 2, true ) );
			StackFrame sf = st.GetFrame( 0 );
			builder = new StringBuilder();
			builder.AppendFormat( "\t\t{0}({1},{2}) : {3}\n", sf.GetFileName(), sf.GetFileLineNumber(), sf.GetFileColumnNumber(), sf.GetMethod().Name );
		}

		static public void FullStackWrite( StringBuilder format_builder )
		{
			StackTrace stackTrace = new StackTrace( true );
			StackFrame[] stackFrames = stackTrace.GetFrames();

			const Int32 max_stack_trace_count = 20;
			Int32 current_writing_stack_count = 0;

			format_builder.AppendFormat( "\n==================Stack Trace========================" );
			foreach( StackFrame sf in stackFrames )
			{
				if( current_writing_stack_count > max_stack_trace_count )
					break;

				Int32 file_line = sf.GetFileLineNumber();
				Int32 col_line = sf.GetFileColumnNumber();

				if( file_line != 0 && col_line != 0 )
					format_builder.AppendFormat( "\n\t{0}({1},{2})\n\t{3}.{4}\n",
						sf.GetFileName(), file_line, col_line, sf.GetMethod().Module.Name, sf.GetMethod().Name );

				++current_writing_stack_count;
			}
			format_builder.AppendFormat( "===================Stack End========================\n\t[Message] :" );
		}

		static public void InfoLog( string format, params object[] args )
		{
			string text = string.Format( format, args );
			logger.Info( text );


		}

		static public void DebugLog( string format, params object[] args )
		{
			string text = string.Format( format, args );
			logger.Info( text );
		}

		static public void WarnLog( string format, params object[] args )
		{
			string text = string.Format( format, args );
			logger.Warning( text );
		}

		static public void ErrLog( string format, params object[] args )
		{
			string text = string.Format( format, args );
			logger.Error( text );
		}

		static public void FatalLog( string format, params object[] args )
		{
			string text = string.Format( format, args );
			logger.Fatal( text );
		}

		static public void ExceptionLog( Exception ex )
		{
			ErrLog( "[Exception]\nStack : {0}\nMsg : {1}\nSrc : {2}\nTarget : {3}", ex.StackTrace, ex.Message, ex.Source, ex.TargetSite );
		}

		static public void SocketExceptionLog( SocketException ex )
		{
			ErrLog( "[Exception]\n\nError {0}\nStack : {1}\nMsg : {2}\nSrc : {3}\nTarget : {4}", ex.ErrorCode, ex.StackTrace, ex.Message, ex.Source, ex.TargetSite );

		}
	}
}
