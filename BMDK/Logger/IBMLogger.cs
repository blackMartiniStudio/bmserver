﻿using System;

namespace BMDK
{
	//==============================================================================
	// Window Form 로그창에 남기기 위해
	//==============================================================================
	public interface ILogViewer
	{
		void AddToLogView( String logString );
	}

	public class EmptyLogViewer : ILogViewer
	{
		public void AddToLogView( String logString )
		{
		}
	}


	public interface IBMLogger
	{
		void Start();
		void End();
		void Debug( string text );
		void Info( string text );
		void Warning( string text );
		void Error( string text );
		void Fatal( string text );
	}

	public class EmptyLogger : IBMLogger
	{
		public void End()
		{
		}

		public void Start()
		{
		}

		public void Debug( String text )
		{
		}

		public void Error( String text )
		{
		}

		public void Info( String text )
		{
		}

		public void Warning( String text )
		{
		}

		public void Fatal( String text )
		{
			
		}
	}
}
