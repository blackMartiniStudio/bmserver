﻿using System;
using System.Diagnostics;

namespace BMDK
{
	public class ProcessHelper
	{
		public static string GetProcessName() 
		{
			Process currentProcess = Process.GetCurrentProcess();
			string appName = currentProcess.ProcessName;

			Int32 idx = 0;
			String processName = "";

			idx = appName.IndexOf( "." );
			if( idx > 0 )
				processName = appName.Remove( idx );
			else
				processName = appName;

			return processName;
		}
	}
}
