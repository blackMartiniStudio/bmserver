﻿using System;

namespace BMDK
{
	public class Encryptor
	{
		static public Encryptor INSTANCE = new Encryptor();
		private ICryptor m_cryptor = null;

		public Encryptor()
		{
			if( m_cryptor == null )
				m_cryptor = new XORCryptor();
		}

		public bool Encrypt( ref Byte[] data, int length )
		{
			return m_cryptor.Encrypt( ref data, length );
		}

		public bool Decrypt( ref Byte[] data, int length )
		{
			return m_cryptor.Decrypt( ref data, length );
		}
	}
}