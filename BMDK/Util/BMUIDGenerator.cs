﻿using System;
using System.Threading;

namespace BMDK
{
	public class BMUIDGenerator
	{
		private Int64 m_nCurrentUID;

		public UInt64 Generate()
		{
			UInt64 nNextUID = (UInt64)Interlocked.Increment( ref m_nCurrentUID );
			return nNextUID;
		}
	}
}