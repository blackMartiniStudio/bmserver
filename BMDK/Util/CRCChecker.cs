﻿using System;

namespace BMDK
{
	public class CRCChecker
	{
		private FastCRC m_crc = null;
		static public CRCChecker INSTANCE = new CRCChecker();

		public CRCChecker()
		{
			// 인터페이스를 이용하여 차후 template 처럼 변경하게 만들자.
			if( m_crc == null )
			{
				m_crc = new FastCRC();
				m_crc.Init();
			}
		}

		public bool GetCRC( Byte[] buffer, int length, ref UInt32 CRC )
		{
			return m_crc.GetCRC( buffer, length, ref CRC );
		}
	}
}