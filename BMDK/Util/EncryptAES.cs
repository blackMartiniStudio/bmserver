﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BMDK
{
	public class EncryptAES
	{
		public static String Encrypt( String data, String key )
		{
			byte[] keyArray = UTF8Encoding.UTF8.GetBytes( key );
			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes( data );

			using( RijndaelManaged rDel = new RijndaelManaged() )
			{
				rDel.Key = keyArray;
				rDel.Mode = CipherMode.ECB;
				rDel.Padding = PaddingMode.PKCS7;

				using( ICryptoTransform cTransform = rDel.CreateEncryptor() )
				{
					byte[] resultArray = cTransform.TransformFinalBlock( toEncryptArray, 0, toEncryptArray.Length );
					return Convert.ToBase64String( resultArray, 0, resultArray.Length );
				}
			}
		}

		//AE_S128 복호화
		public static String Decrypt( String data, String key )
		{
			byte[] keyArray = UTF8Encoding.UTF8.GetBytes( key );
			byte[] toEncryptArray = Convert.FromBase64String( data );

			using( RijndaelManaged rDel = new RijndaelManaged() )
			{
				rDel.Key = keyArray;
				rDel.Mode = CipherMode.ECB;
				rDel.Padding = PaddingMode.PKCS7;
				using( ICryptoTransform cTransform = rDel.CreateDecryptor() )
				{
					byte[] resultArray = cTransform.TransformFinalBlock( toEncryptArray, 0, toEncryptArray.Length );
					return UTF8Encoding.UTF8.GetString( resultArray );
				}
			}
		}

		public static String EncryptArray( String[] arrData, String key )
		{
			byte[] keyArray = UTF8Encoding.UTF8.GetBytes( key );

			StringBuilder strData = new StringBuilder();
			foreach( String data in arrData )
			{
				strData.Append( data );
			}

			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes( strData.ToString() );

			using( RijndaelManaged rDel = new RijndaelManaged() )
			{
				rDel.Key = keyArray;
				rDel.Mode = CipherMode.ECB;
				rDel.Padding = PaddingMode.PKCS7;

				using( ICryptoTransform cTransform = rDel.CreateEncryptor() )
				{
					byte[] resultArray = cTransform.TransformFinalBlock( toEncryptArray, 0, toEncryptArray.Length );
					return Convert.ToBase64String( resultArray, 0, resultArray.Length );
				}
			}
		}

		public static String[] DecryptArray( String data, String key, Int32 nOneObjLen )
		{
			List<String> liDecrypString = new List<String>();
			String strDecrypt = Decrypt( data, key );

			if( nOneObjLen <= 0 )
			{
				liDecrypString.Add( strDecrypt );
				return liDecrypString.ToArray();
			}

			Int32 Offset = 0;
			while( strDecrypt.Length > Offset )
			{
				nOneObjLen = Math.Min( strDecrypt.Length - Offset, nOneObjLen );
				liDecrypString.Add( strDecrypt.Substring( Offset, nOneObjLen ) );
				Offset += nOneObjLen;
			}

			return liDecrypString.ToArray();
		}
	}
}