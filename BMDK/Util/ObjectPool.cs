﻿using System;

//using System.Collections.Concurrent;
using System.Threading;

namespace BMDK
{
#pragma warning disable 0420

	public class ObjectPool<T> where T : class
	{
		private readonly BMConcurrentQueue<object> m_objectQueue = new BMConcurrentQueue<object>();

		// object pool에 존재해야 되는 최소한의 하드레퍼런스 객체 수
		private int m_minHardRefSize = 1000;

		// 현재 큐에 들어있는 하드 레퍼런스 객체 수
		private int m_currentHardRefCount = 0;

		// 오브젝트 생성 함수 포인터
		private readonly Func<T> m_createObjectFunctor;

		public int MinimumSize
		{
			get { return m_minHardRefSize; }
			set { m_minHardRefSize = value; }
		}

		public int AvailableCount
		{
			get { return m_objectQueue.Count; }
		}

		// ctor
		public ObjectPool( Func<T> func )
		{
			m_createObjectFunctor = func;
		}

		public void Push( T obj )
		{
			if( m_currentHardRefCount >= m_minHardRefSize )
			{
				m_objectQueue.Enqueue( new WeakReference( obj ) );
				return;
			}

			m_objectQueue.Enqueue( obj );
			Interlocked.Increment( ref m_currentHardRefCount );
		}

		public T Pop()
		{
			object obj;
			if( m_objectQueue.TryDequeue( out obj ) == false )
				return m_createObjectFunctor();

			if( obj is WeakReference )
			{
				var hardObject = ( (WeakReference)obj ).Target;
				if( hardObject == null )    //  gc 당한 경우, recursive 하게 될 때까지 계속 진행
					return Pop();

				return hardObject as T; //  hard 래퍼런스로 객체를 바꿔서 제공.
			}

			// hard ref 인 경우.
			Interlocked.Decrement( ref m_currentHardRefCount );
			return obj as T;
		}

#pragma warning restore 0420
	}
}