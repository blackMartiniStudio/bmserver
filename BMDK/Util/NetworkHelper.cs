﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace BMDK
{
	public enum IPAddressSelectMode
	{
		MANUAL,
		DB,
		PUBLIC,
		PRIVATE
	}

	public class NetworkHelper
	{
		public static readonly String RESERVED_IPv4_LOOPBACK = "127.0.0.1";
		public static short nLocalServerID = 1;
		public static String m_ExternalIP = "";

		public static readonly int      nManagementServerTcpPort = 21000;

		public static readonly int      nBillingPublicPort      = 0;
		public static readonly int      nBillingPrivatePort     = 17000;
		public static readonly int      nBillingConnectPort     = 0;

		public static readonly int      nRankingPublicPort      = 18000;
		public static readonly int      nRankingPrivatePort     = 18000;
		public static readonly int      nRankingConnectPort     = 0;

		public static readonly int      nCLRPublicPort          = 11000;
		public static readonly int      nCLRPrivatePort         = 11000;
		public static readonly int      nCLRConnectPort         = 0;

		public static readonly int      nGamePublicPort         = 12000;
		public static readonly int      nGamePrivatePort        = 12100;
		public static readonly int      nGameConnectPort        = 16000;

		public static readonly int      nLoginPublicPort        = 10000;
		public static readonly int      nLoginPrivatePort       = 20100;
		public static readonly int      nLoginConnectPort       = 16000;

		public static readonly int      nCenterPublicPort       = 0;
		public static readonly int      nCenterPrivatePort      = 20000;
		public static readonly int      nCenterConnectPort      = 0;

		public static readonly int      nRelayPublicPort        = 15100;
		public static readonly int      nRelayPrivatePort       = 0;
		public static readonly int      nRelayConnectPort       = 12100;

		public static readonly int      nSessionPublicPort      = 0;
		public static readonly int      nSessionPrivatePort     = 16000;
		public static readonly int      nSessionConnectPort     = 0;

		public static readonly int      nServicePublicPort      = 19000;
		public static readonly int      nServicePrivatePort     = 19000;
		public static readonly int      nServiceConnectPort     = 0;

		public static UInt32 ConvertIPAddressByteToUInt( IPAddress kIpAddr )
		{
			UInt32 unIpAddr = 0;
			try
			{
				Byte[] byAddrBytes = kIpAddr.GetAddressBytes();
				if( byAddrBytes.Length != 4 )
				{
					throw new Exception();
				}
				unIpAddr += byAddrBytes[3] * 0x01U;
				unIpAddr += byAddrBytes[2] * 0x0100U;
				unIpAddr += byAddrBytes[1] * 0x010000U;
				unIpAddr += byAddrBytes[0] * 0x01000000U;
			}
			catch
			{
				unIpAddr = 0;
			}
			return unIpAddr;
		}

		public static bool IsPrivateIPv4Address( IPAddress kIpAddr )
		{
			if( AddressFamily.InterNetwork != kIpAddr.AddressFamily )
			{
				// IPv4가 아니면 이 연산을 할 수 없음
				throw new ArgumentException();
			}

			UInt32 unIpAddr = NetworkHelper.ConvertIPAddressByteToUInt( kIpAddr );
			if( unIpAddr < 1 )
			{
				// IPv4의 어드레스 값을 uint로 변환하지 못함
				throw new ArgumentException();
			}

			// C class : 192.168.0.0 ~ 192.168.255.255
			if( unIpAddr >= 0xC0A80000U && unIpAddr <= 0xC0A8FFFFU )
			{
				return true;
			}

			// B class : 172.16.0.0 ~ 172.31.255.255
			if( unIpAddr >= 0xAC100000U && unIpAddr <= 0xAC1FFFFFU )
			{
				return true;
			}

			// A class : 10.0.0.0 ~ 10.255.255.255
			if( unIpAddr >= 0x0A000000U && unIpAddr <= 0x0AFFFFFFU )
			{
				return true;
			}

			return false;
		}

		public static List<IPAddress> GetIPv4AddressList()
		{
			List<IPAddress> kIPv4AddressList = new List<IPAddress>();
			IPHostEntry kHostEntries = Dns.GetHostEntry( Dns.GetHostName() );
			foreach( IPAddress kHostEntry in kHostEntries.AddressList )
			{
				if( IPAddress.IsLoopback( kHostEntry ) )
				{
					// 일단 루프백은 허용하지 않음
					continue;
				}
				if( AddressFamily.InterNetwork == kHostEntry.AddressFamily ) // ipv4 only
				{
					kIPv4AddressList.Add( kHostEntry );
				}
			}
			return kIPv4AddressList;
		}

		public static List<IPAddress> GetPrivateIPv4AddressList()
		{
			List<IPAddress> kPrivateIPv4AddressList = new List<IPAddress>();
			List<IPAddress> kIPv4AddressList = NetworkHelper.GetIPv4AddressList();
			foreach( IPAddress kIpAddress in kIPv4AddressList )
			{
				if( true == NetworkHelper.IsPrivateIPv4Address( kIpAddress ) )
				{
					kPrivateIPv4AddressList.Add( kIpAddress );
				}
			}
			return kPrivateIPv4AddressList;
		}

		public static List<IPAddress> GetPublicIPv4AddressList()
		{
			List<IPAddress> kPublicIPv4AddressList = new List<IPAddress>();
			List<IPAddress> kIPv4AddressList = NetworkHelper.GetIPv4AddressList();
			foreach( IPAddress kIpAddress in kIPv4AddressList )
			{
				if( false == NetworkHelper.IsPrivateIPv4Address( kIpAddress ) )
				{
					kPublicIPv4AddressList.Add( kIpAddress );
				}
			}
			return kPublicIPv4AddressList;
		}

		public static List<IPAddress> GetIPAddressList( IPAddressSelectMode kSelectMode )
		{
			List<IPAddress> kIPv4AddressList = new List<IPAddress>();
			switch( kSelectMode )
			{
				case IPAddressSelectMode.DB:
					return NetworkHelper.GetIPv4AddressList();

				case IPAddressSelectMode.PUBLIC:
					return NetworkHelper.GetPublicIPv4AddressList();

				case IPAddressSelectMode.PRIVATE:
					return NetworkHelper.GetPrivateIPv4AddressList();

				default:
					throw new ArgumentException();
			}
		}
	}
}