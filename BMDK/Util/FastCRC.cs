﻿using System;

namespace BMDK
{
	internal class FastCRC
	{
		public bool Init()
		{
			generate();
			return true;
		}

		public static uint[] crc_table = new UInt32[256];
		public readonly uint POLYNOMIAL = (UInt32)0x04c11db7; //  일반적으로 가장 많이 사용하는 crc 시드 값
		public readonly uint MASK = 0x80000000;

		public void generate()
		{
			int i, j;
			uint crc_accum;

			for( i = 0; i < 256; i++ )
			{
				crc_accum = ( (uint)i << 24 );
				for( j = 0; j < 8; j++ )
				{
					if( ( crc_accum & MASK ) == 0 )
						crc_accum = ( crc_accum << 1 );
					else
						crc_accum = ( crc_accum << 1 ) ^ POLYNOMIAL;
				}
				crc_table[i] = crc_accum;
			}
		}

		public bool GetCRC( Byte[] buffer, int length, ref UInt32 crc )
		{
			int i = 0;
			int k = 0;
			uint crc_accum = 0;
			for( int j = 0; j < length; j++ )
			{
				i = ( (int)( crc_accum >> 24 ) ^ buffer[k++] ) & 0xff;
				crc_accum = ( crc_accum << 8 ) ^ crc_table[i];
			}
			crc = crc_accum;
			return true;
		}
	}
}