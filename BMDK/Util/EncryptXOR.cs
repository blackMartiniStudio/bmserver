﻿using System;

namespace BMDK
{
	public interface ICryptor
	{
		bool Encrypt( ref Byte[] data, int length );

		bool Decrypt( ref Byte[] data, int lentgth );
	}

	public class XORCryptor : ICryptor
	{
		private static UInt32 ENCRYPTION_KEY = 0xAFB7E3D9;

		public bool Encrypt( ref Byte[] data, int length )
		{
			UInt32 remain = ( (UInt32)length % sizeof( UInt32 ) );
			length -= (int)remain;

			for( UInt32 i = 0; i < length; i = i + sizeof( UInt32 ) )
			{
				data[i] = (Byte)( data[i] ^ ENCRYPTION_KEY );
			}

			return true;
		}

		public bool Decrypt( ref Byte[] data, int length )
		{
			UInt32 remain = ( (UInt32)length % sizeof( UInt32 ) );
			length -= (int)remain;

			for( UInt32 i = 0; i < length; i = i + sizeof( UInt32 ) )
			{
				data[i] = (Byte)( data[i] ^ ENCRYPTION_KEY );
			}

			return true;
		}
	}
}