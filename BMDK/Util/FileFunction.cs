﻿using System;
using System.Diagnostics;

namespace BMDK
{
	public class FileFunction
	{
		public static String __File()
		{
			StackTrace stackTrace = new StackTrace();
			return stackTrace.GetFrame( 1 ).GetFileName();
		}

		public static String __Function()
		{
			StackTrace stackTrace = new StackTrace();
			return stackTrace.GetFrame( 1 ).GetMethod().Name;
		}

		public static Int32 __Line()
		{
			StackTrace stackTrace = new StackTrace();
			return stackTrace.GetFrame( 1 ).GetFileLineNumber();
		}
	}
}