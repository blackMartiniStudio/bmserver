﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace BMDK
{
#if UNITY_EDITOR
	[Serializable]
	[ComVisible( false )]
	[DebuggerDisplay( "Count = {Count}" )]
	public class BMConcurrentQueue<T> : Queue<T>, IEnumerable<T>, ICollection, IEnumerable
	{
		private Object		m_lock	= new Object();

		public BMConcurrentQueue()
		{
		}

		public BMConcurrentQueue( IEnumerable<T> collection ) : base(collection)
		{
		}

		public bool IsEmpty { get { return Count == 0; } }

		new public void CopyTo( T[] array, int index )
		{
			lock( m_lock )
			{
				base.CopyTo( array, index );
			}
		}

		new public void Enqueue( T item )
		{
			lock( m_lock )
			{
				base.Enqueue( item );
			}
		}

		new public T[] ToArray()
		{
			T[] Temp = default(T[]);
			lock( m_lock )
			{
				Temp = base.ToArray();
				return Temp;
			}
		}

		public bool TryDequeue( out T result )
		{
			lock( m_lock )
			{
				result = default( T );
				if( 0 == Count )
					return false;

				result = base.Dequeue();
			}

			return true;
		}

		public bool TryPeek( out T result )
		{
			lock( m_lock )
			{
				result = default( T );
				if( 0 == Count )
					return false;

				result = base.Peek();
				if( null == result )
					return false;
			}

			return true;
		}
	}
#else

	public class BMConcurrentQueue<T> : System.Collections.Concurrent.ConcurrentQueue<T>
	{
	}

#endif
}