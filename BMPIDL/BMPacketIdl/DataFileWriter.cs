﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class DataFileWriter
    {
        String mFileName;
        StreamWriter mDataFileWriter;
        bool m_useNameSpace = false;

        public void Initialize(String fileName)
        {
            char[] charsToSplit = { '.' };
            String[] splitStirng = fileName.Split(charsToSplit);

            //------------------------------------------------
            // 생성할 파일 이름 저장
            mFileName = splitStirng[0];

            //------------------------------------------------
            // StreamWriter 로 파일 생성
            BeginWriteToFile();
        }

        //==========================================================================
        // 처음 파일에 쓰기 시작할때
        //==========================================================================
        public void BeginWriteToFile()
        {
            // 인코딩 관련 테스트
            //mDataFileWriter = new StreamWriter(mFileName + "Data.cs", false, Encoding.GetEncoding(949));
            //mDataFileWriter = new StreamWriter(mFileName + "Data.cs");
            mDataFileWriter = new StreamWriter(mFileName + "Data.cs", false, Encoding.GetEncoding("UTF-8"));

            mDataFileWriter.WriteLine("using System;");
            mDataFileWriter.WriteLine("using System.Collections.Generic;");
            //mDataFileWriter.WriteLine("using System.Linq;");
            mDataFileWriter.WriteLine("using System.Text;");
            mDataFileWriter.WriteLine("using System.IO;");
            mDataFileWriter.WriteLine("");
        }

        //==========================================================================
        // 파일에 다 쓰고 저장할 때
        //==========================================================================
        public void EndWriteToFile()
        {
            if (m_useNameSpace)
            {
                mDataFileWriter.WriteLine("}");
            }

            mDataFileWriter.Flush();
            mDataFileWriter.Close();
            mDataFileWriter.Dispose();
        }
        
        // 넘어온 스트링 그대로 출력한다.
        public void WriteLineToFile(String lineString)
        {
            mDataFileWriter.WriteLine(lineString);
        }

        public void WriteToFile(String lineString)
        {
            mDataFileWriter.Write(lineString);
        }

        // 예약된 키워드 처리
        public void WriteHeaderToFile(String[] param)
        {
            if (param[0] == "using")
            {
                mDataFileWriter.WriteLine(param[0] + " " + param[1]);
            }
            else if (param[0] == "namespace")
            {
                // 스트링 마지막 ';' 문자 없애기
                String namespaceStirng;
                namespaceStirng = param[1].TrimEnd(new char[] { ' ', ';' });

                mDataFileWriter.WriteLine(param[0] + " " + namespaceStirng);
                mDataFileWriter.WriteLine("{");
                m_useNameSpace = true;
            }
        }

        // 본격적인 클래스 파일 처리
        public void WriteClassToFile(String[] param)
        {
            bool isPartial = false;
            int checkPartialNum = 0;
            int startMemberCnt = 0;
            String className;

            //----------------------------------------------------------------------
            // 예외처리를 어떻게 할까나... 일단 콘솔창에 쓰고 리턴
            if (param[0].ToUpper() == "PACKET")
            {
                checkPartialNum = 1;
            }
            else if (param[0].ToUpper() == "CRYPT")
            {
                checkPartialNum = 2;
            }

            //----------------------------------------------------------------------
            // partial, class 등등 클래스 선언부분 전 구문 분석
            if (param[checkPartialNum] == "partial")
            {
                isPartial = true;
                Console.WriteLine("exist partial class!!! Please make '.cs' file for partial class");
            }

            if (isPartial)
            {
                if (param[checkPartialNum+1] != "class")
                {
                    Console.WriteLine("this line is not class! param[1] = " + param[checkPartialNum+1]);
                    return;
                }

                Console.WriteLine("partial class name = " + param[checkPartialNum+2]);
            }
            else
            {
                if (param[checkPartialNum] != "class")
                {
                    Console.WriteLine("this line is not class! param[0] = " + param[0]);
                    return;
                }
            }
            mDataFileWriter.WriteLine("");

            //----------------------------------------------------------------------
            // 클래스 선언을 한다.
            if (isPartial)
            {
                className = param[checkPartialNum + 2];
                mDataFileWriter.WriteLine("public partial class " + className);
                startMemberCnt = 3 + checkPartialNum;
            }
            else
            {
                className = param[checkPartialNum + 1];
                mDataFileWriter.WriteLine("public class " + className);
                startMemberCnt = 2 + checkPartialNum;
            }
            mDataFileWriter.WriteLine("{");

            //----------------------------------------------------------------------
            // 멤버변수들을 읽어오고 파일에 쓴다.
            // 멤버변수 중에 유저제작 클래스가 존재할 경우 
            //  기본생성자를 외부에서 사용하는것을 막기 위하여 internal로 기본 생성자를 제한 해주자.
            MemberDefine[] arrMember = new MemberDefine[256];
            int memberCount = 0;
            
            bool hasCustomMember = false;

            for (int i = startMemberCnt; i < param.Length; )
            {
                if (param[i] == ";") break;

                mDataFileWriter.WriteLine("\tpublic " + param[i] +" "+ param[i+1] + ";");
                arrMember[memberCount] = new MemberDefine();
                arrMember[memberCount].stringMemberName = param[i+1];

                String typeString;
                if (param[i].Contains("[]"))
                {
                    arrMember[memberCount].isArray = true;
                    typeString = param[i].TrimEnd(new char[] { '[', ']' });
                }
                else
                {
                    typeString = param[i];
                }

                // 유저정의클래스의 이름에 기본타입 이름이 일부 들어갈수 있으므로 
                // Contains() 함수를 사용하여서는 안된다.
                if (typeString == "bool")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_BOOL;
                }
                else if (typeString == "byte" || typeString == "Byte")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_BYTE;
                }
                else if (typeString == "SByte")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_SBYTE;
                }
                else if (typeString == "short")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_SHORT;
                }
                else if (typeString == "UInt16")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_UINT16;
                }
                else if (typeString == "UInt32")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_UINT32;
                }
                else if (typeString == "UInt64")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_UINT64;
                }
                else if (typeString == "Int16")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_INT16;
                }
                else if (typeString == "int" || typeString == "Int32")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_INT32;
                }
                else if (typeString == "Int64")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_INT64;
                }
                else if (typeString == "float")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_FLOAT;
                }
                else if (typeString == "String" || typeString == "string")
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_STRING;
                }
                // 기본타입형이 아닌경우 (유저제작 클래스 혹은 스트럭쳐)
                else
                {
                    arrMember[memberCount].eMemberType = enumDefaultType.EDTYPE_CUSTOM;
                    hasCustomMember = true;
                }

                arrMember[memberCount].stringMemberType = typeString;

                memberCount++;
                i++;    // 이제 멤버이름을 가리킨다.
                i++;    // 이제 다음 멤버의 타입을 가리킨다.
            }

            //----------------------------------------------------------------------
            // 기본 생성자 정의
            //if (hasCustomMember)
            //    mDataFileWriter.WriteLine("\tinternal " + className + "() {}");
            //else
                mDataFileWriter.WriteLine("\tpublic " + className + "() {}");

            //----------------------------------------------------------------------
            // 멤버변수 넘기는 생성자 정의 (멤버변수가 하나도 없을때는 기본생성자와 같으므로 스킵)
            if (memberCount > 0)
            {
                mDataFileWriter.Write("\tpublic " + className + "(");

                for (int i = 0; i < memberCount; i++)
                {
                    if (arrMember[i].isArray)
                    {
                        mDataFileWriter.Write(arrMember[i].stringMemberType + "[] " + "p_" + arrMember[i].stringMemberName);
                    }
                    else
                    {
                        mDataFileWriter.Write(arrMember[i].stringMemberType + " " + "p_" + arrMember[i].stringMemberName);
                    }

                    if (i + 1 < memberCount)
                    {
                        mDataFileWriter.Write(", ");
                    }
                }

                mDataFileWriter.WriteLine(")");
                mDataFileWriter.WriteLine("\t{");

                for (int i = 0; i < memberCount; i++)
                {
                    if (arrMember[i].isArray)
                    {
                        // 레퍼런스로 넘겨지는 유저클래스 등은 new 로 생성후 값을 생성해야 된다.
                        if (arrMember[i].eMemberType == enumDefaultType.EDTYPE_CUSTOM)
                        {
                            mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = new " + arrMember[i].stringMemberType + "[p_" + arrMember[i].stringMemberName + ".Length];");

                            mDataFileWriter.WriteLine("\t\tfor (int i = 0; i < (int)p_" + arrMember[i].stringMemberName + ".Length; i++)");
                            mDataFileWriter.WriteLine("\t\t{");
                            mDataFileWriter.WriteLine("\t\t\tthis." + arrMember[i].stringMemberName + "[i] = new " + arrMember[i].stringMemberType+"(p_"+arrMember[i].stringMemberName+"[i]);");
                            mDataFileWriter.WriteLine("\t\t}");
                        }
                        // 기본타입은 값으로 전달되므로 상관없음.
                        else
                        {
                            mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = new " + arrMember[i].stringMemberType + "[p_" + arrMember[i].stringMemberName + ".Length];");

                            mDataFileWriter.WriteLine("\t\tfor (int i = 0; i < (int)p_" + arrMember[i].stringMemberName + ".Length; i++)");
                            mDataFileWriter.WriteLine("\t\t{");
                            mDataFileWriter.WriteLine("\t\t\tthis." + arrMember[i].stringMemberName + "[i] = p_" + arrMember[i].stringMemberName + "[i];");
                            mDataFileWriter.WriteLine("\t\t}");
                        }
                    }
                    else
                    {
                        // 레퍼런스로 넘겨지는 유저클래스 등은 new 로 생성후 값을 생성해야 된다.
                        if (arrMember[i].eMemberType == enumDefaultType.EDTYPE_CUSTOM)
                        {
                            //this.mAccount = new AccountInfo(account.sessionID, account.accountID, account.accountPW);
                            mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = new " + arrMember[i].stringMemberType + "(p_" + arrMember[i].stringMemberName + ");");
                        }
                        // 기본타입은 값으로 전달되므로 상관없음.
                        else
                        {
                            mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = " + "p_" + arrMember[i].stringMemberName + ";");
                        }
                    }
                }

                mDataFileWriter.WriteLine("\t}");
            }

            //----------------------------------------------------------------------
            // 자신의 클래스 넘기는 생성자 정의
            mDataFileWriter.WriteLine("\tpublic " + className + "(" + className + " p_" + className + ")");
            mDataFileWriter.WriteLine("\t{");

            for (int i = 0; i < memberCount; i++)
            {
                if (arrMember[i].isArray)
                {
                    // 레퍼런스로 넘겨지는 유저클래스 등은 new 로 생성후 값을 생성해야 된다.
                    if (arrMember[i].eMemberType == enumDefaultType.EDTYPE_CUSTOM)
                    {
                        mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = new " + arrMember[i].stringMemberType + "[p_" + className + "." + arrMember[i].stringMemberName + ".Length];");

                        mDataFileWriter.WriteLine("\t\tfor (int i = 0; i < (int)p_" + className + "." + arrMember[i].stringMemberName + ".Length; i++)");
                        mDataFileWriter.WriteLine("\t\t{");
                        mDataFileWriter.WriteLine("\t\t\tthis." + arrMember[i].stringMemberName + "[i] = new " + arrMember[i].stringMemberType + "(p_" + className + "." + arrMember[i].stringMemberName + "[i]);");
                        mDataFileWriter.WriteLine("\t\t}");
                    }
                    // 기본타입은 값으로 전달되므로 상관없음.
                    else
                    {
                        mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = new " + arrMember[i].stringMemberType + "[p_" + className + "." + arrMember[i].stringMemberName + ".Length];");

                        mDataFileWriter.WriteLine("\t\tfor (int i = 0; i < (int)p_" + className + "." + arrMember[i].stringMemberName + ".Length; i++)");
                        mDataFileWriter.WriteLine("\t\t{");
                        mDataFileWriter.WriteLine("\t\t\tthis." + arrMember[i].stringMemberName + "[i] = p_" + className + "." + arrMember[i].stringMemberName + "[i];");
                        mDataFileWriter.WriteLine("\t\t}");
                    }
                }
                else
                {
                    // 레퍼런스로 넘겨지는 유저클래스 등은 new 로 생성후 값을 생성해야 된다.
                    if (arrMember[i].eMemberType == enumDefaultType.EDTYPE_CUSTOM)
                    {
                        mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = new " + arrMember[i].stringMemberType + "(" + "p_" + className + "." + arrMember[i].stringMemberName + ");");
                    }
                    else
                    {
                        mDataFileWriter.WriteLine("\t\tthis." + arrMember[i].stringMemberName + " = " + "p_" + className + "." + arrMember[i].stringMemberName + ";");
                    }
                }
            }

            mDataFileWriter.WriteLine("\t}");

            //----------------------------------------------------------------------
            // 클론 함수 정의
            mDataFileWriter.WriteLine("");
            mDataFileWriter.WriteLine("\tpublic object Clone()");
            mDataFileWriter.WriteLine("\t{");
            mDataFileWriter.WriteLine("\t\treturn this.MemberwiseClone();");
            mDataFileWriter.WriteLine("\t}");

            //----------------------------------------------------------------------
            // StreamEncoding 함수 정의
            mDataFileWriter.WriteLine("");
            mDataFileWriter.WriteLine("\tpublic void StreamEncoding(BinaryWriter output)");
            mDataFileWriter.WriteLine("\t{");
            for(int j=0; j<memberCount; j++)
            {
                String postMemberName;  // 배열일 경우 저장해 놓은 멤버변수 이름뒤에 '[i]' 가 붙어야 한다.

                if (arrMember[j].isArray)
                {
                    postMemberName = arrMember[j].stringMemberName + "[i]";
                    //output.Write(parts_count);
                    mDataFileWriter.WriteLine("\t\toutput.Write((int)" + arrMember[j].stringMemberName + ".Length);");
                    mDataFileWriter.WriteLine("\t\tfor (int i = 0; i < (int)" + arrMember[j].stringMemberName + ".Length; i++)");
                    mDataFileWriter.WriteLine("\t\t{");

                    if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_STRING )
                    {
                        mDataFileWriter.WriteLine( "\t\t\tif (" + postMemberName + " == null)" );
                        mDataFileWriter.WriteLine( "\t\t\t\t" + postMemberName + " = \"\";" );

                        postMemberName = arrMember[j].stringMemberName;
                        mDataFileWriter.WriteLine( "\t\t\t" + "byte[] s" + postMemberName + " = Encoding.UTF8.GetBytes(" + postMemberName + "[i]" + ");" );
                        mDataFileWriter.WriteLine( "\t\t\toutput.Write(s" + postMemberName + ".Length);" );
                        mDataFileWriter.WriteLine( "\t\t\toutput.Write(s" + postMemberName + ");" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_CUSTOM )
                    {
                        mDataFileWriter.WriteLine( "\t\tif (" + postMemberName + " == null)" );
                        mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + " = new " + arrMember[j].stringMemberType + "();" );
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + ".StreamEncoding(output);" );
                    }
                    else //if EDTYPE_INT, EDTYPE_BYTE
                    {
                        mDataFileWriter.WriteLine( "\t\toutput.Write(" + postMemberName + ");" );
                    }
                }
                else
                {
                    postMemberName = arrMember[j].stringMemberName;

                    if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_STRING )
                    {
                        mDataFileWriter.WriteLine( "\t\tif (" + postMemberName + " == null)" );
                        mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + " = \"\";" );
                        mDataFileWriter.WriteLine( "\t\t" + "byte[] s" + postMemberName + " = Encoding.UTF8.GetBytes(" + postMemberName + ");" );
                        mDataFileWriter.WriteLine( "\t\toutput.Write(s" + postMemberName + ".Length);" );
                        mDataFileWriter.WriteLine( "\t\toutput.Write(s" + postMemberName + ");" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_CUSTOM )
                    {
                        mDataFileWriter.WriteLine( "\t\tif (" + postMemberName + " == null)" );
                        mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + " = new " + arrMember[j].stringMemberType + "();" );
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + ".StreamEncoding(output);" );
                    }
                    else //if EDTYPE_INT, EDTYPE_BYTE
                    {
                        mDataFileWriter.WriteLine( "\t\toutput.Write(" + postMemberName + ");" );
                    }
                }

                if (arrMember[j].isArray)
                {
                    mDataFileWriter.WriteLine("\t\t}");
                }
            }
            mDataFileWriter.WriteLine("\t}");

            //----------------------------------------------------------------------
            // StreamDecoding 함수 정의
            mDataFileWriter.WriteLine("");
            mDataFileWriter.WriteLine("\tpublic void StreamDecoding(BinaryReader src)");
            mDataFileWriter.WriteLine("\t{");
            for (int j = 0; j < memberCount; j++)
            {
                String postMemberName;

                if (arrMember[j].isArray)
                {
                    postMemberName = arrMember[j].stringMemberName + "[i]";

                    mDataFileWriter.WriteLine("\t\tint " + arrMember[j].stringMemberName + "Length = src.ReadInt32();"); 
                    mDataFileWriter.WriteLine("\t\t" + arrMember[j].stringMemberName + " = new " + arrMember[j].stringMemberType + "["+arrMember[j].stringMemberName + "Length];");
                    mDataFileWriter.WriteLine("\t\tfor (int i = 0; i < (int)" + arrMember[j].stringMemberName + ".Length; i++)");
                    mDataFileWriter.WriteLine("\t\t{");

                    if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_STRING )
                    {
                        postMemberName = arrMember[j].stringMemberName;
                        mDataFileWriter.WriteLine( "\t\tint l" + postMemberName + " = src.ReadInt32();" );
                        //mDataFileWriter.WriteLine("\t\tif (l" + postMemberName + " <= 0)");
                        //mDataFileWriter.WriteLine("\t\t{");
                        //mDataFileWriter.WriteLine("\t\t\tthrow new EndOfStreamException();");
                        //mDataFileWriter.WriteLine("\t\t}");
                        mDataFileWriter.WriteLine( "\t\tbyte[] s" + postMemberName + " = new byte[l" + postMemberName + "];" );
                        mDataFileWriter.WriteLine( "\t\tsrc.Read(s" + postMemberName + ", 0, l" + postMemberName + ");" );
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + "[i]" + " = Encoding.UTF8.GetString(s" + postMemberName + ");" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_BOOL )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadBoolean();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_BYTE )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadByte();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_SBYTE )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadSByte();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_SHORT )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt16();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_UINT16 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadUInt16();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_UINT32 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadUInt32();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_UINT64 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadUInt64();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_INT16 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt16();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_INT32 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt32();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_INT64 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt64();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_FLOAT )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadSingle();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_CUSTOM )
                    {
                        if( arrMember[j].isArray )
                        {
                            mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + " = new " + arrMember[j].stringMemberType + "();" );
                            mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + ".StreamDecoding(src);" );
                        }
                        else
                        {
                            mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = new " + arrMember[j].stringMemberType + "();" );
                            mDataFileWriter.WriteLine( "\t\t" + postMemberName + ".StreamDecoding(src);" );
                        }
                    }
                }
                else
                {
                    postMemberName = arrMember[j].stringMemberName;

                    if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_STRING )
                    {
                        mDataFileWriter.WriteLine( "\t\tint l" + postMemberName + " = src.ReadInt32();" );
                        //mDataFileWriter.WriteLine("\t\tif (l" + postMemberName + " <= 0)");
                        //mDataFileWriter.WriteLine("\t\t{");
                        //mDataFileWriter.WriteLine("\t\t\tthrow new EndOfStreamException();");
                        //mDataFileWriter.WriteLine("\t\t}");
                        mDataFileWriter.WriteLine( "\t\tbyte[] s" + postMemberName + " = new byte[l" + postMemberName + "];" );
                        mDataFileWriter.WriteLine( "\t\tsrc.Read(s" + postMemberName + ", 0, l" + postMemberName + ");" );
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = Encoding.UTF8.GetString(s" + postMemberName + ");" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_BOOL )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadBoolean();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_BYTE )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadByte();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_SBYTE )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadSByte();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_SHORT )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt16();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_UINT16 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadUInt16();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_UINT32 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadUInt32();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_UINT64 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadUInt64();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_INT16 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt16();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_INT32 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt32();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_INT64 )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadInt64();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_FLOAT )
                    {
                        mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = src.ReadSingle();" );
                    }
                    else if( arrMember[j].eMemberType == enumDefaultType.EDTYPE_CUSTOM )
                    {
                        if( arrMember[j].isArray )
                        {
                            mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + " = new " + arrMember[j].stringMemberType + "();" );
                            mDataFileWriter.WriteLine( "\t\t\t" + postMemberName + ".StreamDecoding(src);" );
                        }
                        else
                        {
                            mDataFileWriter.WriteLine( "\t\t" + postMemberName + " = new " + arrMember[j].stringMemberType + "();" );
                            mDataFileWriter.WriteLine( "\t\t" + postMemberName + ".StreamDecoding(src);" );
                        }
                    }
                }

                if (arrMember[j].isArray)
                {
                    mDataFileWriter.WriteLine("\t\t}");
                }
            }

            mDataFileWriter.WriteLine("\t}");
            mDataFileWriter.WriteLine("}");
        }
    }
}
