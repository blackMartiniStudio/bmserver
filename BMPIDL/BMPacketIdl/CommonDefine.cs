﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum enumDefaultType
{
    EDTYPE_BOOL,
    EDTYPE_BYTE,
    EDTYPE_SBYTE,
    EDTYPE_SHORT,
    EDTYPE_UINT16,
    EDTYPE_UINT32,
    EDTYPE_UINT64,
    EDTYPE_INT16,
    EDTYPE_INT32,
    EDTYPE_INT64,
    EDTYPE_FLOAT,
    EDTYPE_STRING,
    EDTYPE_CUSTOM,
}


namespace KMPacketIdl
{
    class MemberDefine
    {
        public String stringMemberName;
        public String stringMemberType;
        public enumDefaultType eMemberType = enumDefaultType.EDTYPE_INT32;
        public bool isArray = false;
    }

    public class CommonDefine
    {
        public static String[] Keyword = { "print", "using", "namespace", "packet", "partial", "class" };
    }

}