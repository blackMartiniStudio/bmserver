﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class DecoderFileWriter
    {
        String mFileName;
        StreamWriter mDecoderFileWriter;
        bool useNameSpace = false;

        public void Initialize(String fileName)
        {
            //char[] charsToSplit = { '.', '/', '\\' };
            //String[] splitStirng = fileName.Split(charsToSplit, StringSplitOptions.RemoveEmptyEntries);

            ////----------------------------------------------------------------------
            //// 생성할 파일 이름 저장
            //mFileName = splitStirng[splitStirng.Length - 2];

            char[] charsToSplit = { '.' };
            String[] splitStirng = fileName.Split(charsToSplit);

            //------------------------------------------------
            // 생성할 파일 이름 저장
            mFileName = splitStirng[0];

            //------------------------------------------------
            // StreamWriter 로 파일 생성
            BeginWriteToFile();
        }

        public void BeginWriteToFile()
        {
            // 인코딩 관련 테스트
            //mDecoderFileWriter = new StreamWriter(mFileName + "Decoder.cs", false, Encoding.GetEncoding(949));
            //mDecoderFileWriter = new StreamWriter(mFileName + "Decoder.cs");
            mDecoderFileWriter = new StreamWriter(mFileName + "Decoder.cs", false, Encoding.GetEncoding("UTF-8"));

            mDecoderFileWriter.WriteLine("using System;");
            mDecoderFileWriter.WriteLine("using System.Collections.Generic;");
            //mDecoderFileWriter.WriteLine("using System.Linq;");
            mDecoderFileWriter.WriteLine("using System.Text;");
            mDecoderFileWriter.WriteLine("using System.IO;");
            mDecoderFileWriter.WriteLine("");
        }

        public void EndWriteToFile()
        {
            mDecoderFileWriter.WriteLine("\t}");

            if (useNameSpace)
            {
                mDecoderFileWriter.WriteLine("}");
            }

            mDecoderFileWriter.Flush();
            mDecoderFileWriter.Close();
            mDecoderFileWriter.Dispose();
        }

        public void WriteLineToFile(String lineString)
        {
            mDecoderFileWriter.WriteLine(lineString);
        }

        public void WriteHeaderToFile(String[] param)
        {
            if (param[0] == "using")
            {
                mDecoderFileWriter.WriteLine(param[0] + " " + param[1]);
            }
            else if (param[0] == "namespace")
            {
                // 스트링 마지막 ';' 문자 없애기
                String namespaceStirng;
                namespaceStirng = param[1].TrimEnd(new char[] { ' ', ';' });

                mDecoderFileWriter.WriteLine(param[0] + " " + namespaceStirng);
                mDecoderFileWriter.WriteLine("{");
                useNameSpace = true;
            }
        }

        public void WriteFirstFunctionToFile()
        {
            char[] charsToSplit = { '.', '/', '\\' };
            String[] splitStirng = mFileName.Split(charsToSplit, StringSplitOptions.RemoveEmptyEntries);
            String fileName = splitStirng[splitStirng.Length - 1];

            mDecoderFileWriter.WriteLine("\tpublic sealed partial class " + fileName + "Decoder");
            mDecoderFileWriter.WriteLine("\t{");
        }

        public void writeClassToFile(String[] param)
        {
            String className;
            if (param[0].ToUpper() == "PACKET")
            {
                if (param[1] == "class")
                {
                    className = param[2];
                }
                else
                {
                    className = param[3];
                }
            }
            else if (param[0].ToUpper() == "CRYPT")
            {
                if (param[1].ToUpper() != "PACKET")
                    return;

                if (param[2] == "class")
                {
                    className = param[3];
                }
                else
                {
                    className = param[4];
                }
            }
            else
                return;

            //----------------------------------------------------------------------
            // Write Encode Function
            //----------------------------------------------------------------------
            mDecoderFileWriter.WriteLine("");
            mDecoderFileWriter.WriteLine("\t\tstatic public " + className + " Decode" + className + "(byte[] bodyBuffer)");
            mDecoderFileWriter.WriteLine("\t\t{");
            mDecoderFileWriter.WriteLine("\t\t\t" + className + " retValue = new " + className + "();");
            mDecoderFileWriter.WriteLine("\t\t\tusing (Stream streamBuffer = new MemoryStream(bodyBuffer, 0, bodyBuffer.Length, false))");
            mDecoderFileWriter.WriteLine("\t\t\t{");
            mDecoderFileWriter.WriteLine("\t\t\t\tBinaryReader src = new BinaryReader(streamBuffer);");
            mDecoderFileWriter.WriteLine("\t\t\t\tretValue.StreamDecoding(src);");
            mDecoderFileWriter.WriteLine("\t\t\t}");
            mDecoderFileWriter.WriteLine("");
            mDecoderFileWriter.WriteLine("\t\t\treturn retValue;");
            mDecoderFileWriter.WriteLine("\t\t}");
        }
    }
}
