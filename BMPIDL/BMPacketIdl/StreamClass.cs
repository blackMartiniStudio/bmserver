﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KMPacketIdl
{
    public class StreamParam
    {
        //==========================================================================
        // Public Static MemberVariable
        //==========================================================================
        #region [Static MemberVariable]
        public static String StringArray = "[]";
        #endregion

        //==========================================================================
        // Method
        //==========================================================================
        #region [Method]
        private String mStrName;
        public String StrName
        {
            get { return mStrName; }
            set { mStrName = value; }
        }
        private String mStrType;
        public String StrType
        {
            get { return mStrType; }
            set { mStrType = value; }
        }
        public enumDefaultType EnumType = enumDefaultType.EDTYPE_INT32;
        public bool IsArray;
        #endregion //[Method]
    }

    public class StreamClass
    {
        //==========================================================================
        // Public Static MemberVariable
        //==========================================================================
        #region [Static MemberVariable]
        public static String StringPublic = "public";
        public static String StringPartial = "partial";
        public static String StringClass = "class";
        #endregion

        //==========================================================================
        // Method
        //==========================================================================
        #region [Method]
        private bool mIsPartial = false;
        public bool IsPartial
        {
            get { return mIsPartial; }
            set { mIsPartial = value; }
        }

        private bool mIsPacket = false;
        public bool IsPacket
        {
            get { return mIsPacket; }
            set { mIsPacket = value; }
        }

        private String mClassName;
        public String ClassName
        {
            get { return mClassName; }
            set { mClassName = value; }
        }
        #endregion //[Method]

        //==========================================================================
        // Variable
        //==========================================================================
        #region [Member Variable]
        private List<StreamParam> mListParam;
        #endregion //[Member Variable]

        //==========================================================================
        // Constructor
        //==========================================================================
        #region [Constructor]
        public StreamClass()
        {
            Initializer();
        }
        #endregion //[Constructor]

        //==========================================================================
        // Public Function
        //==========================================================================
        #region [Public Member Function]
        public void Initializer()
        {
            mListParam = new List<StreamParam>();
        }

        public void AddParam(StreamParam p_Param)
        {
            mListParam.Add(p_Param);
        }
        #endregion //[Member Function]

        //==========================================================================
        // Private Function
        //==========================================================================
        #region [Private Member Function]
        //private void 
        #endregion //[Member Function]
    }
}
