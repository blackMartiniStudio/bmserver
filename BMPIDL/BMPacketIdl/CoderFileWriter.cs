﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class CoderFileWriter
    {
        String m_fileName;
        String m_enumString;
        String m_encoderString;
        String m_decoderString;
        StreamWriter m_encoderFileWriter;
        StreamWriter m_decoderFileWriter;

        bool m_useNameSpace = false;

        public void Initialize(String fileName)
        {
            char[] charsToSplit = { '.' };
            String[] splitStirng = fileName.Split(charsToSplit);

            //------------------------------------------------
            // 생성할 파일 이름 저장
            m_fileName = splitStirng[0];

            //------------------------------------------------
            // StreamWriter 로 파일 생성
            BeginWriteToFile();
        }

        // 처음 파일에 쓰기 시작할때
        public void BeginWriteToFile()
        {
            // Encoder
            m_encoderFileWriter = new StreamWriter(m_fileName + "EncodePacket.cs", false, Encoding.GetEncoding("UTF-8"));

            m_encoderFileWriter.WriteLine("using System;");
            m_encoderFileWriter.WriteLine("using System.Collections.Generic;");
            //m_encoderFileWriter.WriteLine("using System.Linq;");
            m_encoderFileWriter.WriteLine("using System.Text;");
            m_encoderFileWriter.WriteLine("");

            // Decoder
            m_decoderFileWriter = new StreamWriter(m_fileName + "DecodePacket.cs", false, Encoding.GetEncoding("UTF-8"));

            m_decoderFileWriter.WriteLine("using System;");
            m_decoderFileWriter.WriteLine("using System.Collections.Generic;");
            //m_decoderFileWriter.WriteLine("using System.Linq;");
            m_decoderFileWriter.WriteLine("using System.Text;");
            m_decoderFileWriter.WriteLine("");
        }

        public void WriteFirstCoderToFile()
        {
            char[] charsToSplit = { '.', '/', '\\' };
            String[] splitStirng = m_fileName.Split(charsToSplit, StringSplitOptions.RemoveEmptyEntries);
            String fileName = splitStirng[splitStirng.Length - 1];

            // 기타 변수 스트링 셋팅
            m_enumString = string.Format("{0}Enum", fileName);
            m_encoderString = string.Format("{0}Encoder", fileName);
            m_decoderString = string.Format("{0}Decoder", fileName);

            // Encoder
            m_encoderFileWriter.WriteLine("public sealed partial class " + fileName + "Coder");
            m_encoderFileWriter.WriteLine("{");
            m_encoderFileWriter.WriteLine("\tstatic public byte[] Encode(int cmd, object packetClass, out bool isCrypt)");
            m_encoderFileWriter.WriteLine("\t{");
            m_encoderFileWriter.WriteLine("\t\tswitch (cmd)");
            m_encoderFileWriter.WriteLine("\t\t{");

            // Decoder
            m_decoderFileWriter.WriteLine("public sealed partial class " + fileName + "Coder");
            m_decoderFileWriter.WriteLine("{");
            m_decoderFileWriter.WriteLine("\tstatic public object Decode(int cmd, byte[] bodyBuffer)");
            m_decoderFileWriter.WriteLine("\t{");
            m_decoderFileWriter.WriteLine("\t\tswitch (cmd)");
            m_decoderFileWriter.WriteLine("\t\t{");
        }

        // 파일에 다 쓰고 저장할 때
        public void EndWriteToFile()
        {
            // Encoder
            m_encoderFileWriter.WriteLine("\t\t\tdefault:");
            m_encoderFileWriter.WriteLine("\t\t\t\tbreak;");
            m_encoderFileWriter.WriteLine("\t\t}");
            m_encoderFileWriter.WriteLine("\t\tisCrypt = false;");
            m_encoderFileWriter.WriteLine("\t\treturn null;");
            m_encoderFileWriter.WriteLine("\t}");
            m_encoderFileWriter.WriteLine("}");

            if (m_useNameSpace)
            {
                m_encoderFileWriter.WriteLine("}");
            }

            m_encoderFileWriter.Flush();
            m_encoderFileWriter.Close();
            m_encoderFileWriter.Dispose();

            // Decoder
            m_decoderFileWriter.WriteLine("\t\t\tdefault:");
            m_decoderFileWriter.WriteLine("\t\t\t\tbreak;");
            m_decoderFileWriter.WriteLine("\t\t}");
            m_decoderFileWriter.WriteLine("\t\treturn null;");
            m_decoderFileWriter.WriteLine("\t}");
            m_decoderFileWriter.WriteLine("}");

            if (m_useNameSpace)
            {
                m_decoderFileWriter.WriteLine("}");
            }

            m_decoderFileWriter.Flush();
            m_decoderFileWriter.Close();
            m_decoderFileWriter.Dispose();
        }

        // 예약된 키워드 처리
        public void WriteHeaderToFile(String[] param)
        {
            if (param[0] == "using")
            {
                m_encoderFileWriter.WriteLine(param[0] + " " + param[1]);
                m_decoderFileWriter.WriteLine(param[0] + " " + param[1]);
            }
            else if (param[0] == "namespace")
            {
                // 스트링 마지막 ';' 문자 없애기
                String namespaceStirng;
                namespaceStirng = param[1].TrimEnd(new char[] { ' ', ';' });

                m_encoderFileWriter.WriteLine(param[0] + " " + namespaceStirng);
                m_encoderFileWriter.WriteLine("{");

                m_decoderFileWriter.WriteLine(param[0] + " " + namespaceStirng);
                m_decoderFileWriter.WriteLine("{");

                m_useNameSpace = true;
            }
        }

        public void writeCoderToFile(String[] param)
        {
            Int32 startMemberCnt = 0;
            String className;

            // 패킷아님! 리턴
            if (param[0].ToUpper() == "PACKET")
            {
                if (param[1] == "class")
                {
                    className = param[2];
                    startMemberCnt = 3;
                }
                else
                {
                    className = param[3];
                    startMemberCnt = 4;
                }
            }
            else if (param[0].ToUpper() == "CRYPT")
            {
                if (param[1].ToUpper() != "PACKET")
                    return;

                if (param[2] == "class")
                {
                    className = param[3];
                    startMemberCnt = 3;
                }
                else
                {
                    className = param[4];
                    startMemberCnt = 4;
                }
            }
            else
                return;

            
            
//             if (param[1] == "class")
//             {
//                 className = param[2];
//                 startMemberCnt = 3;
//             }
//             else
//             {
//                 className = param[3];
//                 startMemberCnt = 4;
//             }

            //----------------------------------------------------------------------
            // 멤버변수의 갯수 파악
            Int32 memberCount = 0;

            for (int i = startMemberCnt; i < param.Length; )
            {
                if (param[i] == ";") break;

                memberCount++;
                i++;    // 이제 멤버이름을 가리킨다.
                i++;    // 이제 다음 멤버의 타입을 가리킨다.
            }


            if (memberCount > 0)
            {
                // Write Encode Function
                m_encoderFileWriter.WriteLine("\t\t\tcase (int){0}.CMD_{1}:", m_enumString, className);
                m_encoderFileWriter.WriteLine("\t\t\t\treturn {0}.Encode{1}(({2})packetClass, out isCrypt);", m_encoderString, className, className);

                // Write Decode Function
                m_decoderFileWriter.WriteLine("\t\t\tcase (int){0}.CMD_{1}:", m_enumString, className);
                m_decoderFileWriter.WriteLine("\t\t\t\treturn (object){0}.Decode{1}(bodyBuffer);", m_decoderString, className);
            }
            else
            {
                // Write Encode Function
                m_encoderFileWriter.WriteLine("\t\t\tcase (int){0}.CMD_{1}:", m_enumString, className);
                m_encoderFileWriter.WriteLine("\t\t\t\tisCrypt = false;");
                m_encoderFileWriter.WriteLine("\t\t\t\treturn null;");

                // Write Decode Function
                m_decoderFileWriter.WriteLine("\t\t\tcase (int){0}.CMD_{1}:", m_enumString, className);
                m_decoderFileWriter.WriteLine("\t\t\t\treturn null;");
            }
        }
    }
}
