﻿//----------------------------------------------------
// ToDo List
//----------------------------------------------------
1. 제너레이트 할 파일을 찾을 수 없을 때 예외처리


//----------------------------------------------------
// Wish List
//----------------------------------------------------
1. 주석문 처리
2. 중간 이후 주석처리
4. 예약어 처리 변경


//----------------------------------------------------
// 사용시 주의사항
//----------------------------------------------------
1. 배열 선언시 반드시 타입형에 공백없이 붙여서 선언해 주어야 한다.
 예) int[] mValue, 

2. 배열이 선언되어 있는 클래스의 경우 추가적인 생성자는 partial 클래스에 직접 정의해야 한다.
  배열의 크기가 0일 경우에도 배열은 0크기로 new 로 할당은 해주어야 한다.

3. 파일경로 중간에 '.'이 있으면 안된다. 클래스 생성시 문제가 된다.

4. namespace 예약어 사용시 파일 상단의 using을 제외한 파일 전체를 감싸게 된다.

5. 예약어를 사용시 항상 마지막에 예약어의 가장 마지막은 'class' 로 해야한다.

6. 라인의 맨 앞에 올 수 있는 예약어
	print
	using
	namespace
	packet
	partial
	class


7. 기본개념
 ; 아무런 인자값이 없는 기본생성자는 오로지 Decoder 에서만 사용되어야 한다. 즉 유저가 임의로 빈 패킷을 그냥 생성해서는 안된다.
  기존 코드와의 통합에서 생기는 사이드이펙트를 최소화 하고자 유저 정의 클래스들을 멤버변수로 가지고 있을 경우 생성되지 않은
  멤버변수에 접근하면서 생기는 문제를 예방 하고자 멤버변수중 유저정의 클래스가 있는 경우 internal 로 기본생성자 정의.




//----------------------------------------------------
// Check List
//----------------------------------------------------
1. 타입형 통일
	String or string
	int or Int32

2. Enum형의 값들은 그냥 int 형으로 넘겨주게 한다.
	IPEndPoint		-> PktS2SNotiAddRelay, PktS2SNotiAddGameRelay, 
	
3. List로 구현되어 있는 패킷들 배열로 모두 수정
	List<Script_Npc> mScriptList			at PktS2SResScriptNpc
	List<VehicleMountItem> mScriptList		at PktS2SResScriptVehicle
	List<ChtrMountItem> mScriptList			at PktS2SResScriptCharItem
	List<Script_GameAI> mScriptList			at PktS2SResScriptGameAI
	List<int> mItemList						at Script_SaleItem
	List<Script_GameAI> mScriptList			at PktS2SResScriptGameAI
	List<Script_SaleItem> mScriptList		at PktS2SResScriptSaleItem
	List<int> mPlayerIdxs					at PktS2SNotiSaveGameReward
	List<UnitInfo> mUnits					at PktS2SNotiSaveGameReward

4. PktS2CNotiSquareNPC class 는 생성자에 List를 넘겨준다. 배열로 넘겨지도록 수정하자.




*. DefineCharacter.cs 파일도 함께 정리
*. ServerCommonData.cs 파일도 함께 정리
*. DefineMoveInfo.cs 파일도 함께 정리
*. DefineCharacterInven.cs 파일도 함께 정리


2013 10 29 추가 사항
c2s 및 패킷 암호화 추가.
packet class XXXX
 =>
crypt packet class XXX 로 암호화 적용