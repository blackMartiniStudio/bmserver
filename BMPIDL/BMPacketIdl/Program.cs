﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Error :: 읽어들일 파일 이름이 설정되지 않았습니다.");
                return;
            }

            BpkParser fileParser = new BpkParser();

            if (args.Length == 2)
            {
                if (!fileParser.Initialize(args[0], args[1]))
                {
                    Console.WriteLine("Error :: BPK 파일 파서 초기화에 실패하였습니다. E_Code=102");
                    return;
                }
            }
            else if (args.Length == 1)
            {
                if (!fileParser.Initialize(args[0]))
                {
                    Console.WriteLine("Error :: BPK 파일 파서 초기화에 실패하였습니다.  E_Code=101");
                    return;
                }
            }

            fileParser.ParsingFile();

            //fileParser.NewVersionParsingFile();
        }
    }
}
