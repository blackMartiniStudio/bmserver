﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class EnumFileWriter
    {
        String m_fileName;
        StreamWriter m_enumFileWriter;
        bool m_useNameSpace = false;

        public bool m_isNewStartIndex = false;
        public Int32 m_newStartIndex = 0;

        public void Initialize(String fileName)
        {
            char[] charsToSplit = { '.' };
            String[] splitStirng = fileName.Split(charsToSplit);

            //------------------------------------------------
            // 생성할 파일 이름 저장
            m_fileName = splitStirng[0];

            //------------------------------------------------
            // StreamWriter 로 파일 생성
            BeginWriteToFile();
        }

        // 처음 파일에 쓰기 시작할때
        public void BeginWriteToFile()
        {
            // 인코딩 관련 테스트
            //mDataFileWriter = new StreamWriter(mFileName + "Data.cs", false, Encoding.GetEncoding(949));
            //mDataFileWriter = new StreamWriter(mFileName + "Data.cs");
            m_enumFileWriter = new StreamWriter(m_fileName + "Enum.cs", false, Encoding.GetEncoding("UTF-8"));

            m_enumFileWriter.WriteLine("using System;");
            m_enumFileWriter.WriteLine("using System.Collections.Generic;");
            //m_enumFileWriter.WriteLine("using System.Linq;");
            m_enumFileWriter.WriteLine("using System.Text;");
            m_enumFileWriter.WriteLine("");
        }

        public void WriteFirstEnumToFile()
        {
            char[] charsToSplit = { '.', '/', '\\' };
            String[] splitStirng = m_fileName.Split(charsToSplit, StringSplitOptions.RemoveEmptyEntries);
            String fileName = splitStirng[splitStirng.Length - 1];

            m_enumFileWriter.WriteLine("public enum " + fileName + "Enum");
            m_enumFileWriter.WriteLine("{");
            m_enumFileWriter.WriteLine("\tCMD_NONE,");
        }

        // 파일에 다 쓰고 저장할 때
        public void EndWriteToFile()
        {
            m_enumFileWriter.WriteLine("\tCMD_MAX");
            m_enumFileWriter.WriteLine("}");
            
            if (m_useNameSpace)
            {
                m_enumFileWriter.WriteLine("}");
            }

            m_enumFileWriter.Flush();
            m_enumFileWriter.Close();
            m_enumFileWriter.Dispose();
        }

        // 예약된 키워드 처리
        public void WriteHeaderToFile(String[] param)
        {
            if (param[0] == "using")
            {
                m_enumFileWriter.WriteLine(param[0] + " " + param[1]);
            }
            else if (param[0] == "namespace")
            {
                // 스트링 마지막 ';' 문자 없애기
                String namespaceStirng;
                namespaceStirng = param[1].TrimEnd(new char[] { ' ', ';' });

                m_enumFileWriter.WriteLine(param[0] + " " + namespaceStirng);
                m_enumFileWriter.WriteLine("{");
                m_useNameSpace = true;
            }
        }

        public void writeEnumToFile(String[] param)
        {
            String className;
            if (param[0].ToUpper() == "PACKET")
            {
                if (param[1] == "class")
                {
                    className = param[2];
                }
                else
                {
                    className = param[3];
                }
            }
            else if (param[0].ToUpper() == "CRYPT")
            {
                if (param[1].ToUpper() != "PACKET")
                    return;

                if (param[2] == "class")
                {
                    className = param[3];
                }
                else
                {
                    className = param[4];
                }
            }
            else
                return;

            //----------------------------------------------------------------------
            // Write Encode Function
            //----------------------------------------------------------------------
            if (m_isNewStartIndex)
            {
                m_isNewStartIndex = false;
                m_enumFileWriter.WriteLine("\tCMD_" + className + " = " + m_newStartIndex + ",");
            }
            else
            {
                m_enumFileWriter.WriteLine("\tCMD_" + className + ",");
            }
        }
    }
}
