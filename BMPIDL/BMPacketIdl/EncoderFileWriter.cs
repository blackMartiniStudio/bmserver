﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class EncoderFileWriter
    {
        String mFileName;
        StreamWriter mEncoderFileWriter;
        bool useNameSpace = false;

        public void Initialize(String fileName)
        {
            //char[] charsToSplit = { '.', '/', '\\' };
            //String[] splitStirng = fileName.Split(charsToSplit, StringSplitOptions.RemoveEmptyEntries);

            ////----------------------------------------------------------------------
            //// 생성할 파일 이름 저장
            //mFileName = splitStirng[splitStirng.Length-2];

            char[] charsToSplit = { '.' };
            String[] splitStirng = fileName.Split(charsToSplit);

            //------------------------------------------------
            // 생성할 파일 이름 저장
            mFileName = splitStirng[0];

            //----------------------------------------------------------------------
            // StreamWriter 로 파일 생성
            BeginWriteToFile();
        }

        public void BeginWriteToFile()
        {
            // 인코딩 관련 테스트
            //mEncoderFileWriter = new StreamWriter(mFileName + "Encoder.cs", false, Encoding.GetEncoding(949));
            //mEncoderFileWriter = new StreamWriter(mFileName + "Encoder.cs");     
            mEncoderFileWriter = new StreamWriter(mFileName + "Encoder.cs", false, Encoding.GetEncoding("UTF-8"));

            mEncoderFileWriter.WriteLine("using System;");
            mEncoderFileWriter.WriteLine("using System.Collections.Generic;");
            //mEncoderFileWriter.WriteLine("using System.Linq;");
            mEncoderFileWriter.WriteLine("using System.Text;");
            mEncoderFileWriter.WriteLine("using System.IO;");
            mEncoderFileWriter.WriteLine("");
        }

        public void EndWriteToFile()
        {
            mEncoderFileWriter.WriteLine("\t}");

            if (useNameSpace)
            {
                mEncoderFileWriter.WriteLine("}");
            }

            mEncoderFileWriter.Flush();
            mEncoderFileWriter.Close();
            mEncoderFileWriter.Dispose();
        }

        public void WriteLineToFile(String lineString)
        {
            mEncoderFileWriter.WriteLine(lineString);
        }

        public void WriteHeaderToFile(String[] param)
        {
            if (param[0] == "using")
            {
                mEncoderFileWriter.WriteLine(param[0] + " " + param[1]);
            }
            else if (param[0] == "namespace")
            {
                // 스트링 마지막 ';' 문자 없애기
                String namespaceStirng;
                namespaceStirng = param[1].TrimEnd(new char[] { ' ', ';' });

                mEncoderFileWriter.WriteLine(param[0] + " " + namespaceStirng);
                mEncoderFileWriter.WriteLine("{");
                useNameSpace = true;
            }
        }

        public void WriteFirstFunctionToFile()
        {
            char[] charsToSplit = { '.', '/', '\\' };
            String[] splitStirng = mFileName.Split(charsToSplit, StringSplitOptions.RemoveEmptyEntries);
            String fileName = splitStirng[splitStirng.Length - 1];

            mEncoderFileWriter.WriteLine("\tpublic sealed partial class " + fileName + "Encoder");
            mEncoderFileWriter.WriteLine("\t{");
        }

        public void writeClassToFile(String[] param)
        {
            bool isCrypt = false;
            String className;
            if (param[0].ToUpper() == "PACKET")
            {
                isCrypt = false;
                if (param[1] == "class")
                {
                    className = param[2];
                }
                else
                {
                    className = param[3];
                }
            }
            else if (param[0].ToUpper() == "CRYPT")
            {
                if (param[1].ToUpper() != "PACKET")
                    return;

                isCrypt = true;
                if (param[2] == "class")
                {
                    className = param[3];
                }
                else
                {
                    className = param[4];
                }
            }
            else
                return;
            


            //----------------------------------------------------------------------
            // Write Encode Function
            mEncoderFileWriter.WriteLine("");
            mEncoderFileWriter.WriteLine("\t\tstatic public byte[] Encode" + className + "(" + className + " pktBody, out bool isCrypt)");
            mEncoderFileWriter.WriteLine("\t\t{");
            mEncoderFileWriter.WriteLine("\t\t\tByte[] retVal;");
            mEncoderFileWriter.WriteLine("\t\t\tusing (MemoryStream mem = new MemoryStream())");
            mEncoderFileWriter.WriteLine("\t\t\t{");
            mEncoderFileWriter.WriteLine("\t\t\t\tBinaryWriter output = new BinaryWriter(mem);");
            mEncoderFileWriter.WriteLine("\t\t\t\tpktBody.StreamEncoding(output);");
            mEncoderFileWriter.WriteLine("\t\t\t\toutput.Flush();");
            mEncoderFileWriter.WriteLine("\t\t\t\tretVal = mem.ToArray();");
            mEncoderFileWriter.WriteLine("\t\t\t}");
            mEncoderFileWriter.WriteLine("");
            
            if ( isCrypt == true)
            {
                mEncoderFileWriter.WriteLine("\t\t\tisCrypt = true;");
            }
            else
            {
                mEncoderFileWriter.WriteLine("\t\t\tisCrypt = false;");
            }
            
            mEncoderFileWriter.WriteLine("\t\t\treturn retVal;");
            mEncoderFileWriter.WriteLine("\t\t}");
        }
    }
}
