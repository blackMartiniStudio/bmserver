﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace KMPacketIdl
{
    class BpkParser
    {
        public static String EndLineMark = ";";

        private String m_fileName;
        private StreamReader m_streamReader;
        private String m_curLineString = null;

        private bool m_isDataFileOnly = false;

        private DataFileWriter m_dataFileWriter;
        private EncoderFileWriter m_encoderFileWriter;
        private DecoderFileWriter m_decoderFileWriter;
        private EnumFileWriter m_enumFileWriter;
        private CoderFileWriter m_coderFileWriter;

        Dictionary<String, KeywordProcess> dicKeywordProcessor;

        public delegate bool KeywordProcess(List<String> listParam);
        

        public bool Initialize(String fileName)
        {
            m_fileName = fileName;

            m_dataFileWriter = new DataFileWriter();
            m_dataFileWriter.Initialize(m_fileName);

            if (m_isDataFileOnly == false)
            {
                m_encoderFileWriter = new EncoderFileWriter();
                m_encoderFileWriter.Initialize(m_fileName);

                m_decoderFileWriter = new DecoderFileWriter();
                m_decoderFileWriter.Initialize(m_fileName);

                m_enumFileWriter = new EnumFileWriter();
                m_enumFileWriter.Initialize(m_fileName);

                m_coderFileWriter = new CoderFileWriter();
                m_coderFileWriter.Initialize(m_fileName);
            }

            try
            {
                m_streamReader = new StreamReader(m_fileName, Encoding.GetEncoding("UTF-8"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

            //----------------------------------------------------------------------
            // 예약어 등록
            dicKeywordProcessor = new Dictionary<string, KeywordProcess>();
            dicKeywordProcessor.Add("PRINT", PrintProcess);
            dicKeywordProcessor.Add("USING", UsingProcess);
            dicKeywordProcessor.Add("NAMESPACE", NamespaceProcess);
            dicKeywordProcessor.Add("PACKET", PacketProcess);
            dicKeywordProcessor.Add("PARTIAL", PartialProcess);
            dicKeywordProcessor.Add("CLASS", ClassProcess);

            return true;
        }

        public bool Initialize(String fileName, String options)
        {
            //Console.WriteLine("fileName = {0}, options = {1}", fileName, options);
            if (options == "-data")
            {
                m_isDataFileOnly = true;
            }

            if (Initialize(fileName) == false)
                return false;

            return true;
        }

        public void ParsingFile()
        {
            try
            {
                bool isFirstWriteEncodeFunction = true; // 처음으로 함수 본문을 쓰기전 클래스 이름 선언
                bool isFirstWriteDecodeFunction = true; // 처음으로 함수 본문을 쓰기전 클래스 이름 선언
                bool isFirstWriteEnumFunction = true;   // 처음으로 Enum 함수 본문을 쓰기전 enum 이름 선언
                bool isFirstWriteCoder = true;   // 처음으로 Enum 함수 본문을 쓰기전 enum 이름 선언

                while (!m_streamReader.EndOfStream)
                {
                    String lineString = m_streamReader.ReadLine();
                    String[] param = lineString.Split(new Char[] { ' ', ',', '(', ')', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                    if (param.Length == 0) continue;

                    //--------------------------------------------------------------
                    // 예약어 처리
                    //--------------------------------------------------------------
                    if (param[0] == "namespace")
                    {
                        m_dataFileWriter.WriteHeaderToFile(param);

                        if (m_isDataFileOnly == false)
                        {
                            m_encoderFileWriter.WriteHeaderToFile(param);
                            m_decoderFileWriter.WriteHeaderToFile(param);
                         
                            m_enumFileWriter.WriteHeaderToFile(param);
                            m_coderFileWriter.WriteHeaderToFile(param);
                        }
                        
                        continue;
                    }

                    if (param[0] == "print")
                    {
                        if (param.Length < 2)
                        {
                            // print할 내용이 없다. 그냥 한줄 띄어주고 넘어가자.
                            m_dataFileWriter.WriteLineToFile("");
                            continue;
                        }

                        int nCutIndex = lineString.IndexOf(' ');
                        String printString = lineString.Substring(nCutIndex + 1);

                        m_dataFileWriter.WriteLineToFile(printString);
                        continue;
                    }

                    if (param[0] == "startIndex")
                    {
                        if (param.Length < 2)
                        {
                            continue;
                        }

                        Int32 nStartIndex = 0;
                        bool isEnable = true;
                        
                        try
                        {
                            nStartIndex = Int32.Parse(param[1]);
                        }
                        catch (Exception ex)
                        {
                            isEnable = false;

                            Console.WriteLine("BpkMsg : startIndex Error!" + ex.ToString());
                        }

                        if (isEnable)
                        {
                            m_enumFileWriter.m_isNewStartIndex = true;
                            m_enumFileWriter.m_newStartIndex = nStartIndex;
                        }
                        
                        continue;
                    }

                    //--------------------------------------------------------------
                    // 예약어 처리 끝! 본격적인 파일 쓰기
                    //--------------------------------------------------------------
                    if (m_isDataFileOnly == false)
                    {
                        if (isFirstWriteEncodeFunction)
                        {
                            m_encoderFileWriter.WriteFirstFunctionToFile();
                            isFirstWriteEncodeFunction = false;
                        }

                        if (isFirstWriteDecodeFunction)
                        {
                            m_decoderFileWriter.WriteFirstFunctionToFile();
                            isFirstWriteDecodeFunction = false;
                        }

                        if (isFirstWriteEnumFunction)
                        {
                            m_enumFileWriter.WriteFirstEnumToFile();
                            isFirstWriteEnumFunction = false;
                        }

                        if (isFirstWriteCoder)
                        {
                            m_coderFileWriter.WriteFirstCoderToFile();
                            isFirstWriteCoder = false;
                        }
                    }

                    m_dataFileWriter.WriteClassToFile(param);

                    if (m_isDataFileOnly == false)
                    {
                        m_encoderFileWriter.writeClassToFile(param);
                        m_decoderFileWriter.writeClassToFile(param);
                        m_enumFileWriter.writeEnumToFile(param);
                        m_coderFileWriter.writeCoderToFile(param);
                    }
                }

                m_dataFileWriter.EndWriteToFile();

                if (m_isDataFileOnly == false)
                {
                    if (!isFirstWriteEncodeFunction)
                    {
                        m_encoderFileWriter.EndWriteToFile();
                    }

                    if (!isFirstWriteDecodeFunction)
                    {
                        m_decoderFileWriter.EndWriteToFile();
                    }

                    if (!isFirstWriteEnumFunction)
                    {
                        m_enumFileWriter.EndWriteToFile();
                    }

                    if (!isFirstWriteCoder)
                    {
                        m_coderFileWriter.EndWriteToFile();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return;
            }
        }

        // NARO: 130425: 현재는 사용안함. 추후 로직을 깔끔하게 바꿀 때 사용하자.
        public void NewVersionParsingFile()
        {
            try
            {
                //bool isFirstWriteEncodeFunction = true; // 처음으로 함수 본문을 쓰기전 클래스 이름 선언
                //bool isFirstWriteDecodeFunction = true; // 처음으로 함수 본문을 쓰기전 클래스 이름 선언

                while (!m_streamReader.EndOfStream)
                {
                    List<String> mParam = new List<String>();
                    
                    // 라인 종료 문자 ';' 가 나올때까지 라인을 읽고
                    bool isEndLine = false;
                    while (!isEndLine && !m_streamReader.EndOfStream)
                    {
                        m_curLineString += " " + m_streamReader.ReadLine();

                        if (m_curLineString.Contains(';'))
                        {
                            isEndLine = true;
                        }
                    }
                        
                    // 읽은 라인을 구분자에 의해 파라미터로 구분한다.
                    String[] param = m_curLineString.Split(new Char[] { ' ', ',', '(', ')', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                        
                    // 가공하여 쓰기 쉽게 List에 집어넣는다.
                    for (int i = 0; i < param.Length; i++)
                    {
                        mParam.Add(param[i]);
                    }

                    bool endLoop = false;
                    while (!endLoop)
                    {
                        int paramCount = 0;
                        if (paramCount >= mParam.Count)
                        {
                            endLoop = true;
                        }
                        
                        // 예약어 처리
                        KeywordProcess processor;
                        String keyword = mParam[0];
                        mParam.RemoveAt(0);
                        dicKeywordProcessor.TryGetValue(keyword.ToUpper(), out processor);
                        if (processor == null)
                        {
                            continue;
                        }

                        if (processor(mParam))
                        {
                            endLoop = true;
                        }

                        paramCount++;
                    }

                    m_curLineString = null;
                }

                m_dataFileWriter.EndWriteToFile();
                m_encoderFileWriter.EndWriteToFile();
                m_decoderFileWriter.EndWriteToFile();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return;
            }
        }

        // delegate
        bool PrintProcess(List<String> param)
        {
            if (param.Count < 1)
            {
                // print할 내용이 없다. 그냥 한줄 공백 써준다.
                m_dataFileWriter.WriteLineToFile("");
                return true;
            }

            // 프린트 문은 문장의 맨앞에서만 사용 되도록 강제한다.
            for(int i=0; i<param.Count; i++)
                m_dataFileWriter.WriteToFile(param[i]);

            m_dataFileWriter.WriteToFile("\n");

            return true;
        }

        bool UsingProcess(List<String> param)
        {
            return true;
        }

        bool NamespaceProcess(List<String> param)
        {
            //mDataFileWriter.WriteHeaderToFile(param);
            //mEncoderFileWriter.WriteHeaderToFile(param);
            //mDecoderFileWriter.WriteHeaderToFile(param);

            return true;
        }

        bool PacketProcess(List<String> param)
        {
            return false;
        }

        bool PartialProcess(List<String> param)
        {
            return false;
        }

        bool ClassProcess(List<String> param)
        {
            return true;
        }
    }
}